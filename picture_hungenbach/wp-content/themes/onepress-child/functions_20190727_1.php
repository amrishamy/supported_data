<?php
if ( ! function_exists( 'onepress_footer_site_info' ) ) {
    /**
     * Add custom copyright to footer
     * @since 1.0
	 *
	 * <span class="sep"> &ndash; </span>
     * <?php printf(esc_html__('%1$s', 'onepress'), '<a href="' . esc_url('https://creative.hungenbach.de/privacy-statement/', 'onepress') . '">Privacy Statement</a>'); ?>
	 *	
     */
    function onepress_footer_site_info()
    {
        ?>
        <?php printf(esc_html__('Copyright %1$s %2$s %3$s', 'Markus Hungenbach'), '&copy;', esc_attr(date('Y')), 'Markus Hungenbach'); ?>
        <?php
    }
	
	/**
	* Child theme stylesheet einbinden in Abhängigkeit vom Original-Stylesheet
	*/

	function child_theme_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'child-theme-css', get_stylesheet_directory_uri() .'/style.css' , array('parent-style'));

	}
	add_action( 'wp_enqueue_scripts', 'child_theme_styles' );
	}
	 
	/**
	* Exclude everything except pages from search results - WordPress
	*/

	function remove_posts_from_wp_search($query) {
	if ($query->is_search) {
	$query->set('post_type', 'page');
	}
	return $query;
	}
	add_filter('pre_get_posts','remove_posts_from_wp_search');

	/**
	* Show Different Menus to Logged in Users in WordPress
	*/
	
	function my_wp_nav_menu_args( $args = '' ) {
	if( is_user_logged_in() ) { 
		$args['menu'] = 'header-logged-in';
	} else { 
		$args['menu'] = 'header-logged-out';
	} 
		return $args;
	}
	add_filter( 'wp_nav_menu_args', 'my_wp_nav_menu_args' );

	// custom user style sheet (e.g. hide login button-div)
	function add_theme_scripts() {
		if( is_user_logged_in() ) { 
			wp_enqueue_style( 'style', get_stylesheet_uri() );
			wp_enqueue_style( 'user_style', get_stylesheet_directory_uri() . '/user_style.css', array(), '1.1', 'all');
		}
	}
	add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

	
	/**
	 * Block wp-admin (dashboard) access for non-admins
	 */
	function ace_block_wp_admin() {
		if ( is_admin() && ! current_user_can( 'administrator' ) && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
			wp_safe_redirect( home_url() );
			exit;
		}
	}
	add_action( 'admin_init', 'ace_block_wp_admin' );

	
/**
        <?php printf(esc_html__('Copyright %1$s %2$s %3$s', 'onepress'), '&copy;', esc_attr(date('Y')), esc_attr(get_bloginfo())); ?>
        <span class="sep"> &ndash; </span>
        <?php printf(esc_html__('%1$s theme by %2$s', 'onepress'), '<a href="' . esc_url('https://www.famethemes.com/themes/onepress', 'onepress') . '">OnePress</a>', 'FameThemes'); ?>
*/
?>