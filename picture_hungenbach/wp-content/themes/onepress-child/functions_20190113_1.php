<?php
if ( ! function_exists( 'onepress_footer_site_info' ) ) {
    /**
     * Add custom copyright to footer
     * @since 1.0
     */
    function onepress_footer_site_info()
    {
        ?>
        <?php printf(esc_html__('Copyright %1$s %2$s %3$s', 'Markus Hungenbach'), '&copy;', esc_attr(date('Y')), 'Markus Hungenbach'); ?>
        <span class="sep"> &ndash; </span>
        <?php printf(esc_html__('%1$s', 'onepress'), '<a href="' . esc_url('https://creative.hungenbach.de/privacy-statement/', 'onepress') . '">Privacy Statement</a>'); ?>
		<?php
    }
	
	/**
	* Child theme stylesheet einbinden in Abhängigkeit vom Original-Stylesheet
	*/

	function child_theme_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'child-theme-css', get_stylesheet_directory_uri() .'/style.css' , array('parent-style'));

	}
	add_action( 'wp_enqueue_scripts', 'child_theme_styles' );
	}

/**
        <?php printf(esc_html__('Copyright %1$s %2$s %3$s', 'onepress'), '&copy;', esc_attr(date('Y')), esc_attr(get_bloginfo())); ?>
        <span class="sep"> &ndash; </span>
        <?php printf(esc_html__('%1$s theme by %2$s', 'onepress'), '<a href="' . esc_url('https://www.famethemes.com/themes/onepress', 'onepress') . '">OnePress</a>', 'FameThemes'); ?>
*/
?>