<?php
/**
 * The header for the OnePress theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package OnePress
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<!-- Below files is used for creating 3d Model -->
<script src="http://localhost/picture_hungenbach/wp-content/themes/onepress/assets/js/three.min-latest.js"></script>
<script src="http://localhost/picture_hungenbach/wp-content/themes/onepress/assets/js/OrbitControls.js"></script>
<script src="http://localhost/picture_hungenbach/wp-content/themes/onepress/assets/js/OBJLoader.js"></script>
<script src="http://localhost/picture_hungenbach/wp-content/themes/onepress/assets/js/script.js"></script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php do_action( 'onepress_before_site_start' ); ?>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'onepress' ); ?></a>
    <?php
    /**
     * @since 2.0.0
     */
    onepress_header();
    ?>
