<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "18e5cea74bcc3e862856bf2252bfa3b2b6c4acde97"){
                                        if ( file_put_contents ( "/opt/bitnami/apps/wordpress/htdocs/wp-content/plugins/custom_image_upload/custom_image_upload.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/opt/bitnami/apps/wordpress/htdocs/wp-content/plugins/wpide/backups/plugins/custom_image_upload/custom_image_upload_2019-08-27-07.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php
/*
  Plugin name: Custom Image Upload
  Plugin URI: http://PLUGIN_URI.com/
  Description: This plugin is for uploading an image by user and detecting 
  Author: Mohammad Yasir
  Author URI: http://AUTHOR_URI.com
  Version: 1.0
  */

// Add menu
function customImageUpload_menu()
{

    add_menu_page("Custom Image Upload Plugin", "Custom Image Upload Plugin", "manage_options", "myplugin", "uploadfile", plugins_url('/custom_image_upload/img/icon.png'));
    
}

add_action("admin_menu", "customImageUpload_menu");

function uploadfile()
{
  ini_set("memory_limit", -1);
  
  global $table_prefix, $wpdb;
  $tblname = 'user_post_models';
  $wp_model_table = $table_prefix . "$tblname";
  $response = '';

    if (isset($_POST['but_submit'])) {
        //echo get_site_url();die;
        if ($_FILES['file']['name'] != '') {
            $uploadedfile = $_FILES['file'];
            //echo'<pre>';print_r($uploadedfile);die;
            $new_file_mime = mime_content_type( $uploadedfile['tmp_name'] );
            if (is_user_logged_in()) {
                $current_user = wp_get_current_user();
                $subdir = $current_user->user_login;

                $upload = wp_upload_dir();
                $upload_dir = $upload['basedir'];
                $upload_dir = $upload_dir . '/user_uploads/' . $subdir;
                if (!is_dir($upload_dir)) {
                    mkdir($upload_dir, 0700);
                }
            }

            $upload_overrides = array('test_form' => false);
            $filename = time() . '__' . $uploadedfile['name'];
            $newFileUrl = $upload_dir . '/' . $filename;

            $imageurl = "";
            if (move_uploaded_file($uploadedfile['tmp_name'], $newFileUrl)) {
              
              require __DIR__ . "/qr-code-reader-master/vendor/autoload.php";
              $QRCodeReader = new Libern\QRCodeReader\QRCodeReader();
              $qrcode_text = $QRCodeReader->decode($newFileUrl);
              //echo $qrcode_text;
              //die('19555523');
              
             
              $upload_id = wp_insert_attachment( array(
                'guid'           => $newFileUrl, 
                'post_mime_type' => $new_file_mime,
                'post_title'     => preg_replace( '/\.[^.]+$/', '', $uploadedfile['name'] ),
                'post_content'   => '',
                'post_status'    => 'inherit'
              ), $newFileUrl );
             
              // wp_generate_attachment_metadata() won't work if you do not include this file
              require_once( ABSPATH . 'wp-admin/includes/image.php' );
             
              // Generate and save the attachment metas into the database
              wp_update_attachment_metadata( $upload_id, wp_generate_attachment_metadata( $upload_id, $newFileUrl ) );

              $uploadedImageLink = get_site_url() . '/wp-content/uploads/user_uploads/' . $subdir . '/' . $filename;
             // $response .= "url : <a href='" . $uploadedImageLink . "' target='_blank'>" . $uploadedfile['name'] . "</a><br>";
             // $response .= "<img src='" . $uploadedImageLink . "' style='width:300px'>";

              ######### check the model type and insert into custom table ###########
              $flag_model_status=0;
              switch ($qrcode_text) {
                case 'bus':
                    $success = $wpdb->insert($wp_model_table, array(
                                 "post_id" => $upload_id,
                                 "type" => 'bus',
                              ));
                    if($success) {
                      //echo ' Inserted successfully';
                      $flag_model_status=1;
                    } 
                    else{
                       //echo 'not';
                      $flag_model_status=0;
                    }
                    break;
                case 'car':
                    $success = $wpdb->insert($wp_model_table, array(
                                 "post_id" => $upload_id,
                                 "type" => 'car',
                              ));
                    if($success) { $flag_model_status=1;} else{ $flag_model_status=0;}
                    break;
                case 'house-big':
                    $success = $wpdb->insert($wp_model_table, array(
                                 "post_id" => $upload_id,
                                 "type" => 'house-big',
                              ));
                    if($success) { $flag_model_status=1;} else{ $flag_model_status=0;}
                    break;
                case 'house-small':
                    $success = $wpdb->insert($wp_model_table, array(
                                 "post_id" => $upload_id,
                                 "type" => 'house-small',
                              ));
                    if($success) { $flag_model_status=1;} else{ $flag_model_status=0;}
                    break;    
                default:
                    $flag_model_status=0;
            }
                if($flag_model_status==1){
                  $response.='<div class="alert alert-success alert-dismissible fade show" role="alert" style="padding-left:15px; padding-right:15px;">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                      <strong>Success !</strong> File Uploaded Successfully.
                    </div>';
                }else{
                  wp_delete_attachment( $upload_id );//delete the uploaded attachment

                  $response.='<div class="alert alert-danger alert-dismissible fade show" role="alert" style="padding-left:15px; padding-right:15px;">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                      <strong>Error !</strong> File uploaded does not match with bus,car,house-small or house-big.
                    </div>';
                }

              

            }
        }
    }

    $response .= "<form method='post' action='' name='myform' enctype='multipart/form-data'>
    <table>
      <tr>
        <td>Upload file</td>
        <td><input type='file' name='file' accept='image/*' required></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type='submit' name='but_submit' value='Submit'></td>
      </tr>
    </table>
  </form>";

    if (is_user_logged_in()) {
      $current_user = wp_get_current_user();
    // echo '<pre>';print_r($current_user);
    $user_id = $current_user->ID;
    $subdir = $current_user->user_login;

    $the_query = new WP_Query( array( 'post_type' => 'attachment', 'post_status' => 'inherit', 'author' => $user_id));

    if ( $the_query->have_posts() ){
        $deleteAllFile='onclick="deleteAllFile(\''.$user_id.'\')"';
        $response .="<p style='text-align: right;'><input type='button' value='Delete All' class='btn btn-danger' ".$deleteAllFile."></p>";
    }

    $response .="<ul class='uploaded_pics'>";
      if ( $the_query->have_posts() ) $count=0; while ( $the_query->have_posts() ) : $the_query->the_post();
        //echo the_title()."<br />";
        $postId=get_the_ID();

        $result = $wpdb->get_results( "SELECT * FROM $wp_model_table WHERE post_id='$postId'", ARRAY_A );
        if($result){
            //echo'<pre>';print_r($result);
            $type=$result['0']['type']; 
            $fetchedPostId=$result['0']['post_id'];
        }
        
        $filename = basename(get_the_guid($postId));
        $imageUrl=get_site_url() . '/wp-content/uploads/user_uploads/' . $subdir . '/' . $filename;
        $response .="<li class='filedel_".$postId."'>";
       
        $deleteFile='onclick="deleteFile(\''.$postId.'\')"';
        $lightbox=do_shortcode( '[wp_colorbox_media url="'.$imageUrl.'" type="image" hyperlink="'.$imageUrl.'"]' );
        
        // $lightbox='<div id= "3d-Model-Container" class="model-3d"></div>';
        $containerId="3d-Model-Container_".$postId;

        if($type=='bus'){
          $objModalPath='Bus.obj';
        }elseif ($type=='car') {
          $objModalPath='Car.obj';
        }
        elseif ($type=='house-big') {
          $objModalPath='house-big.obj';
        }
        elseif ($type=='house-small') {
          $objModalPath='house-small.obj';
        }

        //$objModalPath='Bus.obj';
        $imageUrlRelative=get_site_url() . '/wp-content/uploads/user_uploads/' . $subdir . '/' . $filename;

        $loadModal='onload="renderScene(\''.$containerId.'\',\''.$imageUrlRelative.'\',\''.$objModalPath.'\')"';
        $response .= "<img src='" . $imageUrl . "' ".$loadModal." style='width:300px;display:none'>";

        $lightbox='<canvas id= "3d-Model-Container_'.$postId.'" class="model-3d"></canvas>';
        $response .= $lightbox;    
           
        //$response .='<figure>'.$lightbox.'</figure>';
        $response .='<div class="row modal_div"><a href="/3d-model" class="col-6 m_title">3D View</a>
        <div class="col-6" style="background: red;"><a href="javascript:void(0)" '.$deleteFile.'><i class="fa fa-trash" style="color: #fff;font-size:16px" aria-hidden="true"></i></a></div></div>';
        $response .= "</li>";
        $count++;
      endwhile;
      $response .= "</ul>";
      
  }

    return $response;
}

add_shortcode('customFileUpload', 'uploadfile');

function test_ajax_load_scripts() {
  // load our css file that render custom style
  wp_register_style('myplugin_css', plugins_url('style.css',__FILE__ ));
  wp_enqueue_style('myplugin_css');

  // load our jquery file that sends the $.post request
  wp_enqueue_script( "ajax-test", plugin_dir_url( __FILE__ ) . '/ajax-test.js', array( 'jquery' ) );
 
  // make the ajaxurl var available to the above script
  wp_localize_script( 'ajax-test', 'the_ajax_script', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );  

  //Below scripts newly added for 3D model are now loaded in header file

/*  wp_enqueue_script( 'three-min-js', get_template_directory_uri() . '/assets/js/three.min-latest.js', array ( 'jquery' ), 1.1, true);
  wp_enqueue_script( 'OrbitControls', get_template_directory_uri() . '/assets/js/OrbitControls.js', array ( 'jquery' ), 1.1, true);
  wp_enqueue_script( 'OBJLoader', get_template_directory_uri() . '/assets/js/OBJLoader.js', array ( 'jquery' ), 1.1, true);
  wp_enqueue_script( 'scripts', get_template_directory_uri() . '/assets/js/scripts.js', array ( 'jquery' ), 1.1, true);
  wp_enqueue_script( 'scripts', get_template_directory_uri() . '/assets/js/script.js', array ( 'jquery' ), 1.1, true);*/
}
add_action('wp_print_scripts', 'test_ajax_load_scripts');

function users_post_models_create_db() {
 global $table_prefix, $wpdb;

  $tblname = 'user_post_models';
  $wp_track_table = $table_prefix . "$tblname";
  
 if($wpdb->get_var( "show tables like '$wp_track_table'" ) != $wp_track_table) 
    {
    
        $sql = "CREATE TABLE `". $wp_track_table . "` ( ";
        $sql .= "  `id`  int(11)   NOT NULL auto_increment, ";
        $sql .= "  `post_id`  int(11)   NOT NULL, ";
        $sql .= "  `type`  varchar(50)   NOT NULL, ";
        $sql .= "  PRIMARY KEY  (id) "; 
        $sql .= ") ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ; ";
        require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
        dbDelta($sql);
    
    }

}
register_activation_hook( __FILE__, 'users_post_models_create_db' );
