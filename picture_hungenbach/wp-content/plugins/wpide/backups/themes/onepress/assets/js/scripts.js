var objectRef = null;

var Testurl = 'https://jsonplaceholder.typicode.com/todos/1';

function getTextureFromServer () {
    axios.get(Testurl)
  .then(function (response) {
    // handle success
    console.log(response);
    
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .finally(function () {
    // always executed
  });
};

// getting container where three js renderer will render the scene
var container = document.getElementById('3d-Model-Container');
var containerWidth = container.offsetWidth;
var containerHeight = container.offsetHeight;

// creating scene
var scene = new THREE.Scene();

// setting up cameras
var fov = 45;
var aspectRatio = containerWidth / containerHeight;
var near = 0.1;
var far = 1000; 
var camera = new THREE.PerspectiveCamera(fov, aspectRatio, near, far);
camera.position.z = 60;
scene.add(camera);

// setting up renderer
var renderer = new THREE.WebGLRenderer();
renderer.setSize(containerWidth, containerHeight);
container.appendChild(renderer.domElement);

// setting up Orbit controls for orbiting , zooming and panning
var orbitControls = new THREE.OrbitControls(camera, renderer.domElement);
orbitControls.enableDamping = true;
orbitControls.dampingFactor = 0.25;
orbitControls.enableZoom = true;
orbitControls.zoomSpeed = 1.25;
orbitControls.minDistance = 27;
orbitControls.maxDistance = 35;
orbitControls.rotateSpeed = 0.5;
orbitControls.keyPanSpeed = 7.5;

// setting up lighting
hlight= new THREE.AmbientLight(0x404040,3);
var keyLight = new THREE.DirectionalLight(new THREE.Color('hsl(30, 100%, 80%)'), 0.5);
keyLight.position.set(-100, 0, 100);
keyLight.castShadow = true; 
var fillLight = new THREE.DirectionalLight(new THREE.Color('hsl(240, 100%, 80%)'), 0.5);
fillLight.position.set(100, 0, 100);
fillLight.castShadow = true; 
var backLight = new THREE.DirectionalLight(0xffffff, 1.5);
backLight.position.set(100, 0, -100).normalize();
backLight.castShadow = true; 
scene.add(hlight);
scene.add(keyLight);
scene.add(fillLight);
scene.add(backLight);

// loading manager for showing progress....
var manager = new THREE.LoadingManager();
manager.onStart = function (url, itemsLoaded, itemsTotal) {

	console.log('Started loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' + itemsTotal + ' files.');

};
manager.onLoad = function ( ) {

	console.log('Loading complete!');

};

manager.onProgress = function (url, itemsLoaded, itemsTotal) {

	console.log('Loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' + itemsTotal + ' files.');

};
manager.onError = function (url) {

	console.log('There was an error loading ' + url);

};

// obj loader to load obj files ...
var objLoader = new THREE.OBJLoader(manager);
objLoader.setPath('/assets/');
objLoader.load('Bus.obj', function (object) {
console.log( 'model loaded successfully');
console.log(object);
objectRef = object;
applyTexture();
scene.add(object);
object.position.y -= 5;

});

// applying texture to model loaded
function applyTexture () {
objectRef.traverse( function (child) {
  if(child instanceof THREE.Mesh) {
    child.material.map = THREE.ImageUtils.loadTexture('/assets/template_bus.jpg');
    child.material.needsUpdate = true;
  }
});
};


// animation loop ... it will be called every frame
var animate = function () {
  requestAnimationFrame(animate);
  orbitControls.update();
  console.log("camera position");
  console.log(camera.position);
  renderer.render(scene, camera);
};

animate();


