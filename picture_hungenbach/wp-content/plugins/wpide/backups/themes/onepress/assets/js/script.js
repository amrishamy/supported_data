function renderScene(id, imgPath, modelPath) {
//alert('good');
console.log(id+' '+imgPath+' '+modelPath);
    
    // getting container where three js renderer will render the scene
    var container = document.getElementById(id);
    var containerWidth = container.clientWidth;
    var containerHeight = container.clientHeight;

    // creating scene
    var scene = new THREE.Scene();

    // setting up camera
    var fov = 45;
    var aspectRatio = containerWidth / containerHeight;
    var near = 0.1;
    var far = 1000;
    var camera = new THREE.PerspectiveCamera(fov, aspectRatio, near, far);
    camera.position.z = 50;
    scene.add(camera);

    // setting up renderer
    var renderer = new THREE.WebGLRenderer({
        antialias: true,
        canvas: container
        
    });
    renderer.setSize(containerWidth, containerHeight);
    renderer.setPixelRatio(window.devicePixelRatio);
    // container.appendChild(renderer.domElement);

    //setting up Orbit controls for orbiting , zooming and panning
    var orbitControls = new THREE.OrbitControls(camera, renderer.domElement);
    orbitControls.enableDamping = true;
    orbitControls.dampingFactor = 0.07;
    orbitControls.minPolarAngle = 0.8;
    orbitControls.maxPolarAngle = 2.4;
    orbitControls.rotateSpeed = 0.07;
    
    // setting up lighting
    hlight = new THREE.AmbientLight(0x404040, 3);

    var keyLight = new THREE.DirectionalLight(new THREE.Color('hsl(30, 100%, 80%)'), 0.5);
    keyLight.position.set(-100, 0, 100);
    keyLight.castShadow = true;

    var fillLight = new THREE.DirectionalLight(new THREE.Color('hsl(240, 100%, 80%)'), 0.5);
    fillLight.position.set(100, 0, 100);
    fillLight.castShadow = true;

    var backLight = new THREE.DirectionalLight(0xffffff, 1.5);
    backLight.position.set(100, 0, -100).normalize();
    backLight.castShadow = true;

    scene.add(hlight);
    scene.add(keyLight);
    scene.add(fillLight);
    scene.add(backLight);

    function animate() {
        orbitControls.update();
        renderer.render(scene, camera);
    };

    // start the animation loop
    renderer.setAnimationLoop(animate);

    // a function that will be called every time the window gets resized.
    // It can get called a lot, so don't put any heavy computation in here!
    function onWindowResize() {

        // set the aspect ratio to match the new browser window aspect ratio
        camera.aspect = container.clientWidth / container.clientHeight;

        // update the camera's frustum
        camera.updateProjectionMatrix();

        // update the size of the renderer AND the canvas
        renderer.setSize(container.clientWidth, container.clientHeight);

    }

    window.addEventListener('resize', onWindowResize);

    function loadObj(modelPath) {

        // obj loader to load obj file ...
        var objLoader = new THREE.OBJLoader();
        objLoader.setPath('/wp-content/uploads/');
        objLoader.load(modelPath, function (object) {
            applyTexture(object);
            
        });
    }

    function applyTexture(obj) {

        // applying texture to model loaded
        var textureLoader = new THREE.TextureLoader();
        textureLoader.load(imgPath, function (texture) {
            obj.traverse(function (child) {
                if (child instanceof THREE.Mesh) {
                    child.material.map = texture;
                    child.material.needsUpdate = true;
                }
            });
            scene.add(obj);
            obj.position.y -= 4.2;
            obj.position.x -= 0.2;
        });
    }

    loadObj(modelPath);
};