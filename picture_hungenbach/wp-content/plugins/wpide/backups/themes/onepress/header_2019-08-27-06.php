<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "18e5cea74bcc3e862856bf2252bfa3b2b6c4acde97"){
                                        if ( file_put_contents ( "/opt/bitnami/apps/wordpress/htdocs/wp-content/themes/onepress/header.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/opt/bitnami/apps/wordpress/htdocs/wp-content/plugins/wpide/backups/themes/onepress/header_2019-08-27-06.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php
/**
 * The header for the OnePress theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package OnePress
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<script src="https://www.picture.hungenbach.de/wp-content/themes/onepress/assets/js/three.min-latest.js"></script>
<script src="https://www.picture.hungenbach.de/wp-content/themes/onepress/assets/js/OrbitControls.js"></script>
<script src="https://www.picture.hungenbach.de/wp-content/themes/onepress/assets/js/script.js"></script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php do_action( 'onepress_before_site_start' ); ?>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'onepress' ); ?></a>
    <?php
    /**
     * @since 2.0.0
     */
    onepress_header();
    ?>
