<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "18e5cea74bcc3e862856bf2252bfa3b2a2e4e5e9a8"){
                                        if ( file_put_contents ( "/opt/bitnami/apps/wordpress/htdocs/wp-content/themes/onepress-child/3dModelTemplate.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/opt/bitnami/apps/wordpress/htdocs/wp-content/plugins/wpide/backups/themes/onepress-child/3dModelTemplate_2019-08-06-05.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php /* Template Name: 3dModelTemplate */ 
get_header();
$layout = onepress_get_layout();
do_action( 'onepress_page_before_content' );
?>
    <div id="content" class="site-content">
    <?php onepress_breadcrumb(); ?>
        <div id="content-inside" class="container <?php echo esc_attr( $layout ); ?>">
            <div id="primary" class="content-area">
		        <main id="main" class="site-main" role="main">
	                 <div id= "3d-Model-Container" class="model-3d"></div>
		
		        </main><!-- #main -->
		    </div><!-- #primary -->
		<?php if ( $layout != 'no-sidebar' ) { ?>
                <?php get_sidebar(); ?>
        <?php } ?>
        </div><!--#content-inside -->
	</div><!-- #content -->
	 
     
<?php 
wp_enqueue_script( 'three-min-js', get_template_directory_uri() . '/assets/js/three.min.js', array ( 'jquery' ), 1.1, true);
wp_enqueue_script( 'OrbitControls', get_template_directory_uri() . '/assets/js/OrbitControls.js', array ( 'jquery' ), 1.1, true);
wp_enqueue_script( 'MTLLoader', get_template_directory_uri() . '/assets/js/MTLLoader.js', array ( 'jquery' ), 1.1, true);
wp_enqueue_script( 'OBJLoader', get_template_directory_uri() . '/assets/js/OBJLoader.js', array ( 'jquery' ), 1.1, true);
wp_enqueue_script( 'axios-min-js', get_template_directory_uri() . '/assets/js/axios.min.js', array ( 'jquery' ), 1.1, true);
wp_enqueue_script( 'scripts', get_template_directory_uri() . '/assets/js/scripts.js', array ( 'jquery' ), 1.1, true);
get_footer(); ?>