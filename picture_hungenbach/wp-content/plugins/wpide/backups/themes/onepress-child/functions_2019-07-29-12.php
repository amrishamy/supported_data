<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "18e5cea74bcc3e862856bf2252bfa3b2e6b2e6177d"){
                                        if ( file_put_contents ( "/opt/bitnami/apps/wordpress/htdocs/wp-content/themes/onepress-child/functions.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/opt/bitnami/apps/wordpress/htdocs/wp-content/plugins/wpide/backups/themes/onepress-child/functions_2019-07-29-12.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php
if ( ! function_exists( 'onepress_footer_site_info' ) ) {
    /**
     * Add custom copyright to footer
     * @since 1.0
	 *
	 * <span class="sep"> &ndash; </span>
     * <?php printf(esc_html__('%1$s', 'onepress'), '<a href="' . esc_url('https://creative.hungenbach.de/privacy-statement/', 'onepress') . '">Privacy Statement</a>'); ?>
	 *	
     */
    function onepress_footer_site_info()
    {
        ?>
        <?php printf(esc_html__('Copyright %1$s %2$s %3$s', 'Markus Hungenbach'), '&copy;', esc_attr(date('Y')), 'Markus Hungenbach'); ?>
        <?php
    }
	
	
	/**
	* Add custom shortcode [user_id] to show user ID in post or page
	*/
	

	function customized_map_3D_func( $atts ){
		global $current_user;
		get_currentuserinfo();
		$return_string = array(); 
		
		//echo 'User ID: ' . $current_user->ID . "\n";
		//return $current_user->user_login;
		
		//$dir = fn_get_upload_dir_var( 'baseurl', '' ).'/user_uploads/'.$current_user->user_login.'/';
		//$files1 = scandir($dir);
		//return getcwd();
		
		foreach (glob('wp-content/uploads/user_uploads/'.$current_user->user_login.'/*.png') as $file) 
		{ 
			$return_string[$file] = date ("F d Y H:i:s.", filemtime($file)); 
		}

		
		$fileList = glob('wp-content/uploads/user_uploads/'.$current_user->user_login.'/*.png');
		
		//return fn_get_upload_dir_var( 'baseurl', '' ).'/user_uploads/'.$current_user->user_login.'/';
		return print_r($return_string);
		//return count($fileList);
		
		
		
		//echo 'Username: ' . $current_user->user_login . "\n";
		//echo 'User email: ' . $current_user->user_email . "\n";
		//echo 'User display name: ' . $current_user->display_name . "\n";
		//echo 'User ID: ' . $current_user->ID . "\n";
	}
	add_shortcode( 'customized_map_3D', 'customized_map_3D_func' );

	
	/**
	 * Get the upload URL/path in right way (works with SSL).
	 *
	 * @param $param string "basedir" or "baseurl"
	 * @return string
	 */
	function fn_get_upload_dir_var( $param, $subfolder = '' ) {
		$upload_dir = wp_upload_dir();
		$url = $upload_dir[ $param ];
	 
		if ( $param === 'baseurl' && is_ssl() ) {
			$url = str_replace( 'http://', 'https://', $url );
		}
	 
		return $url . $subfolder;
	}

	
	/**
	* Child theme stylesheet einbinden in Abhängigkeit vom Original-Stylesheet
	*/

	function child_theme_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'child-theme-css', get_stylesheet_directory_uri() .'/style.css' , array('parent-style'));

	}
	add_action( 'wp_enqueue_scripts', 'child_theme_styles' );
	}
	 
	/**
	* Exclude everything except pages from search results - WordPress
	*/

	function remove_posts_from_wp_search($query) {
	if ($query->is_search) {
	$query->set('post_type', 'page');
	}
	return $query;
	}
	add_filter('pre_get_posts','remove_posts_from_wp_search');

	/**
	* Show Different Menus to Logged in Users in WordPress
	*/
	
	function my_wp_nav_menu_args( $args = '' ) {
	if( is_user_logged_in() ) { 
		$args['menu'] = 'header-logged-in';
	} else { 
		$args['menu'] = 'header-logged-out';
	} 
		return $args;
	}
	add_filter( 'wp_nav_menu_args', 'my_wp_nav_menu_args' );

	// custom user style sheet (e.g. hide login button-div)
	function add_theme_scripts() {
		if( is_user_logged_in() ) { 
			wp_enqueue_style( 'style', get_stylesheet_uri() );
			wp_enqueue_style( 'user_style', get_stylesheet_directory_uri() . '/user_style.css', array(), '1.1', 'all');
		}
	}
	add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

	
	/**
	 * Block wp-admin (dashboard) access for non-admins
	 */
	function ace_block_wp_admin() {
		if ( is_admin() && ! current_user_can( 'administrator' ) && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
			wp_safe_redirect( home_url() );
			exit;
		}
	}
	add_action( 'admin_init', 'ace_block_wp_admin' );

	
/**
        <?php printf(esc_html__('Copyright %1$s %2$s %3$s', 'onepress'), '&copy;', esc_attr(date('Y')), esc_attr(get_bloginfo())); ?>
        <span class="sep"> &ndash; </span>
        <?php printf(esc_html__('%1$s theme by %2$s', 'onepress'), '<a href="' . esc_url('https://www.famethemes.com/themes/onepress', 'onepress') . '">OnePress</a>', 'FameThemes'); ?>
*/
?>