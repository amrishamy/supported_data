jQuery(document).ready(function($) {
    window.setTimeout(function(){ jQuery('.alert').hide();},3000)
});

function deleteFile(postId) {
    //alert(postId);
    var res = confirm('Are you sure you want to delete this file.');
    if (res == true) {
        var data = {
            action: 'test_response',
            post_var: postId
        };
        // the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
        jQuery.post(the_ajax_script.ajaxurl, data, function(response) {
            //alert(response);
            //location.reload()
            if(response=='success'){
                jQuery('.entry-content').prepend('<div class="alert alert-success alert-dismissible fade show" role="alert" style="padding-left:15px; padding-right:15px;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success !</strong> File deleted Successfully.</div>');
            }
            else{
                jQuery('.entry-content').prepend('<div class="alert alert-danger alert-dismissible fade show" role="alert" style="padding-left:15px; padding-right:15px;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error !</strong> Error in deleting this file.</div>');
            }
            // This will reload the page after a delay of 3 seconds
            // window.setTimeout(function(){ window.location.href='custom-file-upload/';},3000)
            jQuery('.filedel_'+postId).remove();
        });
        return false;
    }

};

function deleteAllFile(userId){
         //alert(userId);
        var res = confirm('Are you sure you want to delete all files.');
        if (res == true) {
        var data = {
            action: 'delete_all_attachments',
            post_var: userId
        };

        // the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
        jQuery.post(the_ajax_script.ajaxurl, data, function(response) {
            //alert(response);
            //location.reload()
            if(response=='success'){
                jQuery('.entry-content').prepend('<div class="alert alert-success alert-dismissible fade show" role="alert" style="padding-left:15px; padding-right:15px;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success !</strong> Files deleted Successfully.</div>');
            }
            else{
                jQuery('.entry-content').prepend('<div class="alert alert-danger alert-dismissible fade show" role="alert" style="padding-left:15px; padding-right:15px;"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error !</strong> Error in deleting files.</div>');
            }
            jQuery('.uploaded_pics').hide();

        });
        return false;
    }
}