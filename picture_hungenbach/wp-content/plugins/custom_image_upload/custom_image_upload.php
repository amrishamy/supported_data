<?php
/*
  Plugin name: Custom Image Upload
  Plugin URI: http://PLUGIN_URI.com/
  Description: This plugin is for uploading an image by user and detecting 
  Author: Mohammad Yasir
  Author URI: http://AUTHOR_URI.com
  Version: 1.0
  */

// Add menu

    require __DIR__ . "/php-zxing/src/PHPZxing/PHPZxingBase.php";
    require __DIR__ . "/php-zxing/src/PHPZxing/PHPZxingInterface.php";
    require __DIR__ . "/php-zxing/src/PHPZxing/PHPZxingDecoder.php";
    require __DIR__ . "/php-zxing/src/PHPZxing/ZxingImage.php";
    require __DIR__ . "/php-zxing/src/PHPZxing/ZxingBarNotFound.php";    

    use PHPZxing\PHPZxingDecoder;


function customImageUpload_menu()
{

    add_menu_page("Custom Image Upload Plugin", "Custom Image Upload Plugin", "manage_options", "myplugin", "uploadfile", plugins_url('/custom_image_upload/img/icon.png'));
    
}

add_action("admin_menu", "customImageUpload_menu");

function norm($vec)
{
    $norm = 0;
    $components = count($vec);

    for ($i = 0; $i < $components; $i++)
        $norm += $vec[$i] * $vec[$i];

    return sqrt($norm);
}

function dot($vec1, $vec2)
{
    $prod = 0;
    $components = count($vec1);

    for ($i = 0; $i < $components; $i++)
        $prod += ($vec1[$i] * $vec2[$i]);

    return $prod;
}

function rotateImage($file, $degree){
    $sourceCopy = imagecreatefromjpeg($file);
    //Rotate this image by 90 degrees.
    $rotatedImg = imagerotate($sourceCopy, $degree, 0);
    //Replace original image with rotated / landscape version.
    return imagejpeg($rotatedImg, $file);
}

function check_rotation_angel($newFileUrl, $x, $y , $half_width, $half_height)
{
	if($x > $half_width && $y < $half_height )
    {
    rotateImage($newFileUrl, 270);
    }
    elseif($x > $half_width && $y > $half_height)
    {
    // no rotate
    }elseif($x < $half_width && $y > $half_height)
    {
    rotateImage($newFileUrl, 90);
    }elseif($x < $half_width && $y < $half_height)
    {
    rotateImage($newFileUrl, 180);
    }
}

function doSmallRotationIfRequired( $result_points = array(), $newFileUrl = '' ) { 

  if(!$result_points) return false;

  /*$result_points = $result_points->getResultPoints();
  // echo "<pre>";print_r( $result_points );
  foreach($result_points as $points){
      $points_array = (array) $points;
      $value_array[] = array_values($points_array);
  }
  // echo "<pre>";print_r( $value_array );

  $final_key = array('size', 'count', 'x', 'y');
  foreach($value_array as $Key => $values){
      $final_array[] = array_combine($final_key, $values );
  }*/

  $result_points = $result_points;
  $final_key = array('x', 'y');
  foreach($result_points as $k =>$val)
  {
      $exploded_value = explode(":",$val);
      $exploded_value = str_replace(array( '(', ')' ), '', $exploded_value);
      $exploded_points = explode(",",$exploded_value[1]);
      // print_r($exploded_points);
      $final_array[] = array_combine($final_key, $exploded_points );
      
  }

  $firstX = $final_array[0]['x'];
  $secondX = $final_array[1]['x'];
  $difference = $firstX - $secondX; // 1.5
  if( $difference != 0 ) { 
    $v1 = array($final_array[1]['x'] , $final_array[1]['y']);
    $v2 = array($final_array[2]['x'] , $final_array[2]['y']);

    $ang = acos(dot($v1, $v2) / (norm($v1) * norm($v2)));

    if( $ang != 0 ) rotateImage($newFileUrl, rad2deg($ang) );
  }
// die;

  /*if( $difference !== 0 ) { 
    var_dump($difference); //180 deg = 1753
    $oneDeg = $firstX/180; // 9.73px
    $onePxEqualToDeg = 1/( $firstX/180 ); // 0.10 deg
    $rotate = $difference * $onePxEqualToDeg; // 0.15 deg
    var_dump( $rotate );
    rotateImage($newFileUrl, $rotate < 0?360+$rotate:$rotate);
  }*/

}

function getCoOrdinates( $result_points = array() ) { 

  if(!$result_points) return [ 'x_points_diff' => '','y_points_diff' => '','x' => '','y' => '' ];

  /*$result_points = $result_points->getResultPoints();
  // echo "<pre>";print_r( $result_points );
  foreach($result_points as $points){
      $points_array = (array) $points;
      $value_array[] = array_values($points_array);
  }
  // echo "<pre>";print_r( $value_array );

  $final_key = array('size', 'count', 'x', 'y');
  foreach($value_array as $Key => $values){
      $final_array[] = array_combine($final_key, $values );
  }*/
  // echo "<pre>";print_r( $result_points );die;
  $result_points = $result_points;
  $final_key = array('x', 'y');
  foreach($result_points as $k =>$val)
  {
      $exploded_value = explode(":",$val);
      $exploded_value = str_replace(array( '(', ')' ), '', $exploded_value);
      $exploded_points = explode(",",$exploded_value[1]);
      // print_r($exploded_points);
      $final_array[] = array_combine($final_key, $exploded_points );
      
  }


  // echo "<pre>";print_r( $final_array );
  // die;
  if( ( abs($final_array[0]['x'] - $final_array[1]['x'] ) <=5 ) && ( abs($final_array[1]['y'] - $final_array[2]['y']) <=5 ) )
  {
      $y_points_diff = $final_array[0]['y'] - $final_array[1]['y'] ;
      $x_points_diff = $final_array[2]['x'] - $final_array[1]['x'] ;
  }
  elseif( ( abs($final_array[0]['y'] - $final_array[1]['y']) <=5 )  && ( abs($final_array[1]['x'] - $final_array[2]['x']) <=5 ) )
  {
      $x_points_diff = $final_array[1]['x'] - $final_array[0]['x'] ;
      $y_points_diff = $final_array[2]['y'] - $final_array[1]['y'] ;
  }
  
  $x = $final_array[2]['x'];
  $y = $final_array[2]['y'];

  return [ 'x_points_diff' => @$x_points_diff,'y_points_diff' => @$y_points_diff,'x' => $x,'y' => $y ];

}

function scale_large_width_images($newFileUrl)
{
  list($width, $height) = getimagesize($newFileUrl);
  if( $width > 1488 ) {
    $ratio = ( ( $width - 1488 ) * 100 ) / $width;
    $newWidth = 1488;
    $newHeight = $height - ( ( $height * $ratio ) / 100 );

    // Load
    $thumb = imagecreatetruecolor($newWidth, $newHeight);
    $source = imagecreatefromjpeg($newFileUrl);

    // Resize
    imagecopyresized($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

    // Output
    imagejpeg($thumb, $newFileUrl);
    return true;
  // die('after width scale');
  }
  return false;

}

function scale_large_height_images($newFileUrl)
{
  list($width, $height) = getimagesize($newFileUrl);
  if( $height > 1482 ) {
    $ratio = ( ( $height - 1482 ) * 100 ) / $height;
    $newHeight = 1482;
    $newWidth = $width - ( ( $width * $ratio ) / 100 );

    // Load
    $thumb = imagecreatetruecolor($newWidth, $newHeight);
    $source = imagecreatefromjpeg($newFileUrl);

    // Resize
    imagecopyresized($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

    // Output
    imagejpeg($thumb, $newFileUrl);
    // die('after height scale');
  }

}

function uploadfile()
{
  ini_set("memory_limit", -1);
  
  global $table_prefix, $wpdb;
  $tblname = 'user_post_models';
  $wp_model_table = $table_prefix . "$tblname";
  $response = '';
  $standard = [
    'bus'=>[
        'left'=>1457.5,
        'right'=> 30.5,
        'top'=> 776.5,
        'bottom'=>120.5,
        'width' => 1488,
        'height' => 897,
    ],
    'car'=>[
        'left'=>1401.5,
        'right'=> 30.5,
        'top'=> 728.5,
        'bottom'=>120.5,
        'width' => 1432,
        'height' => 849,
    ],
    'house_small'=>[
        'left'=>1374,
        'right'=> 34,
        'top'=> 877,
        'bottom'=>136,
        'width' => 1408,
        'height' => 1013,
    ],
    'house_big'=>[
        'left'=>811.5,
        'right'=> 33.5,
        'top'=> 1366.5,
        'bottom'=>115.5,
        'width' => 845,
        'height' => 1482,
    ],

];
$standard_json = json_encode($standard);
$standard_json = json_decode($standard_json);


    if (isset($_POST['but_submit'])) {
        //echo get_site_url();die;
        if ($_FILES['file']['name'] != '') {
            $uploadedfile = $_FILES['file'];
            //echo'<pre>';print_r($uploadedfile);die;
            $new_file_mime = mime_content_type( $uploadedfile['tmp_name'] );
            if (is_user_logged_in()) {
                $current_user = wp_get_current_user();
                $subdir = $current_user->user_login;

                $upload = wp_upload_dir();
                $upload_dir = $upload['basedir'];
                $upload_dir = $upload_dir . '/user_uploads/' . $subdir;
                if (!is_dir($upload_dir)) {
                    mkdir($upload_dir, 0700);
                }
            }

            $upload_overrides = array('test_form' => false);
            $filename = time() . '__' . $uploadedfile['name'];
            $newFileUrl = $upload_dir . '/' . $filename;

            $imageurl = "";
            if (move_uploaded_file($uploadedfile['tmp_name'], $newFileUrl)) {

                $exif = exif_read_data( $newFileUrl );
                if(!empty($exif['Orientation'])) {
                    switch($exif['Orientation']) {
                        case 8:
                        rotateImage($newFileUrl, 90); 
                            break;
                        case 3:
                        rotateImage($newFileUrl, 180 ); 
                            break;
                        case 6:
                        rotateImage($newFileUrl, 270); 
                            break;
                    }
                }
                // sleep(10);
              // die('orientation done if required');
              /*******Start Hnadle the large size images to scale them******** [ */
              $isScaled = scale_large_width_images($newFileUrl);
              // sleep(10);
              // die('large image width scale up done if required');
              if( !$isScaled ) 
              scale_large_height_images($newFileUrl);
              // die('large image scale up done if required');
              /*******End Hnadle the large size images to scale them******** ] */
              // sleep(10);
              /*require __DIR__ . "/zing-qr-code-reader/vendor/autoload.php";
              
              $qrcode = new Zxing\QrReader($newFileUrl);
			        $qrcode_text = $qrcode->text(); //return decoded text from QR Code
              $result_points = $qrcode->getResult();
              echo "<pre>";print_r($result_points);die;*/
              // die('Initial QR code reading and text is=>'.$qrcode_text);
              
              $config = array(
                'try_harder'            => true,
              );
              $decoder        = new PHPZxingDecoder($config);
              $data           = $decoder->decode($newFileUrl);
             
              if($data->isFound()) {
                $qrcode_text =  $data->getImageValue(); 
                $points  = $data->getImagePoints();
                // print_r($points);
                // die('hereee'.$points);
                $result_points = $points;
        
        
              }
              // print_r($points);
              // die('hereee'.$points);

              extract( getCoOrdinates($result_points) );
                
                /*echo "<pre>";print_r($result_points);
                echo "<pre>";print_r( getCoOrdinates($result_points) );
               die('come here before main rotation');
               */
           
              $upload_id = wp_insert_attachment( array(
                'guid'           => $newFileUrl, 
                'post_mime_type' => $new_file_mime,
                'post_title'     => preg_replace( '/\.[^.]+$/', '', $uploadedfile['name'] ),
                'post_content'   => '',
                'post_status'    => 'inherit'
              ), $newFileUrl );
             
              // wp_generate_attachment_metadata() won't work if you do not include this file
              require_once( ABSPATH . 'wp-admin/includes/image.php' );
             
              // Generate and save the attachment metas into the database
              wp_update_attachment_metadata( $upload_id, wp_generate_attachment_metadata( $upload_id, $newFileUrl ) );

              $uploadedImageLink = get_site_url() . '/wp-content/uploads/user_uploads/' . $subdir . '/' . $filename;
             // $response .= "url : <a href='" . $uploadedImageLink . "' target='_blank'>" . $uploadedfile['name'] . "</a><br>";
             // $response .= "<img src='" . $uploadedImageLink . "' style='width:300px'>";

              ######### check the model type and insert into custom table ###########
              $flag_model_status=0;
              list($width, $height) = getimagesize($newFileUrl);
              $half_width = $width / 2 ;
              $half_height = $height / 2 ;

              if( is_numeric($x) && is_numeric($y) )
                {
                  check_rotation_angel($newFileUrl, $x, $y, $half_width, $half_height);
                }
                
              //  die('here main rotation is done if required');
              switch ($qrcode_text) {
                case 'bus':
                    $success = $wpdb->insert($wp_model_table, array(
                                 "post_id" => $upload_id,
                                 "type" => 'bus',
                              ));
                    if($success) {
                      //echo ' Inserted successfully';
                      $flag_model_status=1;
                    } 
                    else{
                       //echo 'not';
                      $flag_model_status=0;
                    }
                    
                    break;
                case 'car':
                    $success = $wpdb->insert($wp_model_table, array(
                                 "post_id" => $upload_id,
                                 "type" => 'car',
                              ));
                    if($success) { $flag_model_status=1;} else{ $flag_model_status=0;}
                    
                    break;
                case 'house-big':
                    $success = $wpdb->insert($wp_model_table, array(
                                 "post_id" => $upload_id,
                                 "type" => 'house-big',
                              ));

                    if($success) { $flag_model_status=1;} else{ $flag_model_status=0;}
                    
                    break;
                case 'house-small':
                    $success = $wpdb->insert($wp_model_table, array(
                                 "post_id" => $upload_id,
                                 "type" => 'house-small',
                              ));
                    if($success) { $flag_model_status=1;} else{ $flag_model_status=0;}
                    break;
                        
                default:
                    $flag_model_status=0;
            }
            

            /***START scale Incorrect images to correct images**/
            // echo "status=>".$flag_model_status."==";
            if($flag_model_status == 1)
            {
                /*$qrcode = new Zxing\QrReader($newFileUrl);
                $qrcode_text = $qrcode->text(); //return decoded text from QR Code
                $result_points = $qrcode->getResult();*/

                $config = array(
                  'try_harder'            => true,
                );
                $decoder        = new PHPZxingDecoder($config);
                $data           = $decoder->decode($newFileUrl);
                if($data->isFound()) {
                  $qrcode_text =  $data->getImageValue(); 
                  $points  = $data->getImagePoints();
                  // print_r($points);
                  // die('hereee'.$points);
                  $result_points = $points;
          
          
                }
                
               /* echo "<pre>";print_r( $result_points );
                echo "<pre>";print_r( getCoOrdinates($result_points) );
                 die('before small rotation');*/
                 
                doSmallRotationIfRequired( $result_points, $newFileUrl );

                /*$qrcode = new Zxing\QrReader($newFileUrl);
                $qrcode_text = $qrcode->text(); //return decoded text from QR Code
                // echo $qrcode_text ;die('now here');
                $result_points = $qrcode->getResult();*/
                $config = array(
                  'try_harder'            => true,
                );
                $decoder        = new PHPZxingDecoder($config);
                $data           = $decoder->decode($newFileUrl);
                if($data->isFound()) {
                  $qrcode_text =  $data->getImageValue(); 
                  $points  = $data->getImagePoints();
                  // print_r($points);
                  // die('hereee'.$points);
                  $result_points = $points;
          
          
                }
                extract( getCoOrdinates( $result_points ) );

                 /*echo "<pre>";print_r( $result_points );
                echo "<pre>";print_r( getCoOrdinates($result_points) );
                 die('after small rotation');*/
                
                switch($qrcode_text)
                {
                  case 'bus':
                    $baseValues = $standard_json->bus;
                    $scaleX = ($x_points_diff/ 88 );
                    $scaleY = ($y_points_diff/ 88 );
                    
                  break;
                  case 'car':
                    $baseValues = $standard_json->car;
                    $scaleX = ($x_points_diff/ 88 );
                    $scaleY = ($y_points_diff/ 88 );
                    
                  break;
                  case 'house-small':
                    $baseValues = $standard_json->house_small;
                    $scaleX = ($x_points_diff/ 103 );
                    $scaleY = ($y_points_diff/ 103 );
                    
                  break;
                  case 'house-big':
                    $baseValues = $standard_json->house_big;
                    $scaleX = ($x_points_diff/ 81 );
                    $scaleY = ($y_points_diff/ 81 );
                    
                  break;

                }

                $standard_left = $baseValues->left * $scaleX ;
                  $standard_right = $baseValues->right * $scaleX;
                  $standard_top = $baseValues->top * $scaleY ;
                  $standard_bottom = $baseValues->bottom * $scaleY ;

                /* start check if image is small than standard image size */
                if( ($x - $standard_left ) < -5 || ($y - $standard_top ) < -5  )
                {
                  $flag_model_status = 0 ;
                }

                /* end check if image is small than standard image size */
                
                
                if($flag_model_status == 1)
                {
                  $changeRateX = 1;
                  $changeRateY = 1;
                  if($x_points_diff < $y_points_diff){
                      $changeRateY = $x_points_diff / $y_points_diff;
                  }else{
                      $changeRateX = $y_points_diff / $x_points_diff;
                  }
  
                  
                  
  
                  $start_x = ($x-$standard_left);
                  $start_y = ($y-$standard_top) ;
                  $end_x = ($x+$standard_right);
                  $end_y = ($y+$standard_bottom) ;

                  // Content type
                  // header('Content-Type: image');
  
                  // Load
                  $source = imagecreatefromjpeg($newFileUrl);
                  $dstWidth =$end_x - $start_x;
                  $dstHeight = $end_y - $start_y;
  
                  $desCanvasWidth = $dstWidth * $changeRateX;
                  $desCanvasHeight = $dstHeight * $changeRateY;
  
                  $croppedImage = imagecreatetruecolor( $end_x - $start_x, $end_y - $start_y);
  
                  // Resize and crop
                  imagecopyresampled($croppedImage,
                  $source,
                  0, // Center the image horizontally
                  0, // Center the image vertically
                  $start_x, $start_y,
                  $dstWidth, $dstHeight,
                  $dstWidth, $dstHeight);
                  $scaledImage = imagescale ($croppedImage,  $desCanvasWidth, $desCanvasHeight);
                  imagejpeg($scaledImage, $newFileUrl , 80);
  
                  

                }
            }


            /***END scale Incorrect images to correct images**/
                if($flag_model_status==1){
                  $response.='<div class="alert alert-success alert-dismissible fade show" role="alert" style="padding-left:15px; padding-right:15px;">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                      <strong>Success !</strong> File Uploaded Successfully.
                    </div>';
                }else{
                  wp_delete_attachment( $upload_id );//delete the uploaded attachment

                  $response.='<div class="alert alert-danger alert-dismissible fade show" role="alert" style="padding-left:15px; padding-right:15px;">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                      <strong>Error !</strong> File uploaded does not match with any of the templates or photo cannot be recognized.
                    </div>';
                }

              

            }
        }
    }

    $response .= "<form method='post' action='' name='myform' enctype='multipart/form-data'>
    <table>
      <tr>
        <td>Upload file</td>
        <td><input type='file' name='file' accept='image/*' required></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input type='submit' name='but_submit' value='Submit'></td>
      </tr>
    </table>
  </form>";

    if (is_user_logged_in()) {
      $current_user = wp_get_current_user();
     //echo '<pre>';print_r($current_user);die;
    $user_id = $current_user->ID;
    $role= $current_user->roles[0];
    
      if($role!='administrator'){
          $subdir = $current_user->user_login;

          $the_query = new WP_Query( array( 'post_type' => 'attachment', 'post_status' => 'inherit', 'author' => $user_id));

          if ( $the_query->have_posts() ){
              $deleteAllFile='onclick="deleteAllFile(\''.$user_id.'\')"';
              $response .="<p style='text-align: right;'><input type='button' value='Delete All' class='btn btn-danger' ".$deleteAllFile."></p>";
          }

          $response .="<ul class='uploaded_pics'>";
          if ( $the_query->have_posts() ) $count=0; while ( $the_query->have_posts() ) : $the_query->the_post();
            //echo the_title()."<br />";
            $postId=get_the_ID();

            $result = $wpdb->get_results( "SELECT * FROM $wp_model_table WHERE post_id='$postId'", ARRAY_A );
            if($result){
                //echo'<pre>';print_r($result);
                $type=$result['0']['type']; 
                $fetchedPostId=$result['0']['post_id'];
            }
            
            $filename = basename(get_the_guid($postId));
            $imageUrl=get_site_url() . '/wp-content/uploads/user_uploads/' . $subdir . '/' . $filename;
            $response .="<li class='filedel_".$postId."'>";
           
            $deleteFile='onclick="deleteFile(\''.$postId.'\')"';
            $lightbox=do_shortcode( '[wp_colorbox_media url="'.$imageUrl.'" type="image" hyperlink="'.$imageUrl.'"]' );
            
            // $lightbox='<div id= "3d-Model-Container" class="model-3d"></div>';
            $containerId="3d-Model-Container_".$postId;

            if($type=='bus'){
              $objModalPath='Bus.obj';
            }elseif ($type=='car') {
              $objModalPath='Car.obj';
            }
            elseif ($type=='house-big') {
              $objModalPath='house-big.obj';
            }
            elseif ($type=='house-small') {
              $objModalPath='house-small.obj';
            }

            //$objModalPath='Bus.obj';
            $imageUrlRelative=get_site_url() . '/wp-content/uploads/user_uploads/' . $subdir . '/' . $filename;
            $size='50';
            $loadModal='onload="renderScene(\''.$containerId.'\',\''.$imageUrlRelative.'\',\''.$objModalPath.'\',\''.$size.'\')"';
            $response .= "<img src='" . $imageUrl . "' ".$loadModal." style='width:300px;display:none'>";

            $lightbox='<canvas id= "3d-Model-Container_'.$postId.'" class="model-3d"></canvas>';
            $response .= $lightbox;    
               
            //$response .='<figure>'.$lightbox.'</figure>';
            $response .='<div class="row modal_div"><a href="/picture_hungenbach/3d-model?model='.$postId.'" class="col-6 m_title">View</a>
            <div class="col-6" style="background: red;"><a href="javascript:void(0)" '.$deleteFile.'><i class="fa fa-trash" style="color: #fff;font-size:16px" aria-hidden="true"></i></a></div></div>';
            $response .= "</li>";
            $count++;
          endwhile;
          $response .= "</ul>";
      }else{
        $response .= "<div class='alert alert-info'>Nothing to display here.</div>";
      }

    
      
  }

    return $response;
}

add_shortcode('customFileUpload', 'uploadfile');

function test_ajax_load_scripts() {
  // load our css file that render custom style
  wp_register_style('myplugin_css', plugins_url('style.css',__FILE__ ));
  wp_enqueue_style('myplugin_css');

  // load our jquery file that sends the $.post request
  wp_enqueue_script( "ajax-test", plugin_dir_url( __FILE__ ) . '/ajax-test.js', array( 'jquery' ) );
 
  // make the ajaxurl var available to the above script
  wp_localize_script( 'ajax-test', 'the_ajax_script', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );  

  //Below scripts newly added for 3D model are now loaded in header file

/*  wp_enqueue_script( 'three-min-js', get_template_directory_uri() . '/assets/js/three.min-latest.js', array ( 'jquery' ), 1.1, true);
  wp_enqueue_script( 'OrbitControls', get_template_directory_uri() . '/assets/js/OrbitControls.js', array ( 'jquery' ), 1.1, true);
  wp_enqueue_script( 'OBJLoader', get_template_directory_uri() . '/assets/js/OBJLoader.js', array ( 'jquery' ), 1.1, true);
  wp_enqueue_script( 'scripts', get_template_directory_uri() . '/assets/js/scripts.js', array ( 'jquery' ), 1.1, true);
  wp_enqueue_script( 'scripts', get_template_directory_uri() . '/assets/js/script.js', array ( 'jquery' ), 1.1, true);*/
}
add_action('wp_print_scripts', 'test_ajax_load_scripts');

function users_post_models_create_db() {
 global $table_prefix, $wpdb;

  $tblname = 'user_post_models';
  $wp_track_table = $table_prefix . "$tblname";
  
 if($wpdb->get_var( "show tables like '$wp_track_table'" ) != $wp_track_table) 
    {
    
        $sql = "CREATE TABLE `". $wp_track_table . "` ( ";
        $sql .= "  `id`  int(11)   NOT NULL auto_increment, ";
        $sql .= "  `post_id`  int(11)   NOT NULL, ";
        $sql .= "  `type`  varchar(50)   NOT NULL, ";
        $sql .= "  PRIMARY KEY  (id) "; 
        $sql .= ") ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ; ";
        require_once( ABSPATH . '/wp-admin/includes/upgrade.php' );
        dbDelta($sql);
    
    }

}
register_activation_hook( __FILE__, 'users_post_models_create_db' );