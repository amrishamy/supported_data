-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 08, 2019 at 01:43 PM
-- Server version: 5.7.27-0ubuntu0.16.04.1
-- PHP Version: 7.3.11-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hungenbach`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/picture_hungenbach/', 'yes'),
(2, 'home', 'http://localhost/picture_hungenbach/', 'yes'),
(3, 'blogname', 'Hungenbach', 'yes'),
(4, 'blogdescription', 'picture', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'markus@hungenbach.de', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'closed', 'yes'),
(20, 'default_ping_status', 'closed', 'yes'),
(21, 'default_pingback_flag', '', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:87:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=40&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:3:{i:0;s:81:\"/opt/bitnami/apps/wordpress/htdocs/wp-content/themes/onepress-child/functions.php\";i:1;s:77:\"/opt/bitnami/apps/wordpress/htdocs/wp-content/themes/onepress-child/style.css\";i:3;s:0:\"\";}', 'no'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:25:\"adminimize/adminimize.php\";s:24:\"_mw_adminimize_uninstall\";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '40', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '5', 'yes'),
(92, 'show_comments_cookies_opt_in', '', 'yes'),
(93, 'initial_db_version', '43764', 'yes'),
(94, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:7:{s:19:\"wp_inactive_widgets\";a:4:{i:0;s:10:\"archives-2\";i:1;s:6:\"meta-2\";i:2;s:12:\"categories-2\";i:3;s:17:\"recent-comments-2\";}s:9:\"sidebar-1\";a:2:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";}s:8:\"footer-1\";a:1:{i:0;s:7:\"pages-2\";}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}s:8:\"footer-4\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'widget_pages', 'a:2:{i:2;a:3:{s:5:\"title\";s:5:\"Legal\";s:6:\"sortby\";s:10:\"post_title\";s:7:\"exclude\";s:39:\"57,40,23,22,26,2,3,44,130,142, 197, 209\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'cron', 'a:8:{i:1573196258;a:1:{s:24:\"rm_chronos_task_exe_hook\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:22:\"rm_automation_interval\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1573197282;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1573229342;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1573272537;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1573272559;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1573273066;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1573277601;a:1:{s:21:\"ai1wm_storage_cleanup\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(112, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1546921713;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:2:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";}}}}', 'yes'),
(138, 'recently_activated', 'a:0:{}', 'yes'),
(143, 'cookie_notice_options', 'a:24:{s:12:\"message_text\";s:419:\"<b>Usage of Cookies:</b><br />\r\nWe would like to use cookies to better understand your use of this website. This enables us to improve your future experience on our website. Detailed information about the use of cookies on this website and how you can manage or withdraw your consent at any time can be found in our <a href=\"http://localhost/picture_hungenbach/privacy-statement/\" target=\"_blank\">Privacy Statement</a>.\";s:11:\"accept_text\";s:2:\"Ok\";s:12:\"see_more_opt\";a:5:{s:4:\"text\";s:17:\"Privacy Statement\";s:9:\"link_type\";s:4:\"page\";s:2:\"id\";s:5:\"empty\";s:4:\"link\";s:0:\"\";s:4:\"sync\";b:0;}s:11:\"link_target\";s:6:\"_blank\";s:10:\"refuse_opt\";s:3:\"yes\";s:11:\"refuse_text\";s:2:\"No\";s:14:\"revoke_cookies\";b:1;s:11:\"revoke_text\";s:14:\"Revoke cookies\";s:18:\"revoke_cookies_opt\";s:6:\"manual\";s:16:\"refuse_code_head\";s:790:\"<!-- Global site tag (gtag.js) - Google Analytics -->\r\n<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-131761028-1\"></script>\r\n<script>\r\n  window.dataLayer = window.dataLayer || [];\r\n  function gtag(){dataLayer.push(arguments);}\r\n  gtag(\'js\', new Date());\r\n\r\n gtag(\'config\', \'UA-131761028-1\');\r\n gtag(\'set\', \'anonymizeIp\', true);\r\n</script>\r\n\r\n<!-- Google Tag Manager -->\r\n<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':\r\nnew Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],\r\nj=d.createElement(s),dl=l!=\'dataLayer\'?\'&amp;l=\'+l:\'\';j.async=true;j.src=\r\n\'https://www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);\r\n})(window,document,\'script\',\'dataLayer\',\'GTM-52PCN3D\');</script>\r\n<!-- End Google Tag Manager -->\";s:11:\"refuse_code\";s:205:\"<!-- Google Tag Manager (noscript) -->\r\n<noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-52PCN3D\" height=\"0\" width=\"0\"></iframe></noscript>\r\n<!-- End Google Tag Manager (noscript) -->\";s:16:\"on_scroll_offset\";i:100;s:4:\"time\";s:5:\"month\";s:16:\"script_placement\";s:6:\"header\";s:8:\"position\";s:6:\"bottom\";s:11:\"hide_effect\";s:4:\"fade\";s:9:\"css_style\";s:9:\"bootstrap\";s:9:\"css_class\";s:6:\"button\";s:6:\"colors\";a:2:{s:4:\"text\";s:4:\"#fff\";s:3:\"bar\";s:4:\"#000\";}s:9:\"on_scroll\";s:2:\"no\";s:11:\"redirection\";b:0;s:19:\"deactivation_delete\";s:2:\"no\";s:8:\"see_more\";s:2:\"no\";s:9:\"translate\";b:0;}', 'no'),
(144, 'cookie_notice_version', '1.2.46', 'no'),
(150, '_wp_suggested_policy_text_has_changed', 'not-changed', 'yes'),
(239, 'current_theme', 'onepress Child', 'yes'),
(240, 'theme_mods_onepress', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1546922073;s:4:\"data\";a:6:{s:19:\"wp_inactive_widgets\";a:4:{i:0;s:10:\"archives-2\";i:1;s:6:\"meta-2\";i:2;s:12:\"categories-2\";i:3;s:17:\"recent-comments-2\";}s:9:\"sidebar-1\";a:2:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";}s:8:\"footer-1\";a:0:{}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}s:8:\"footer-4\";a:0:{}}}}', 'yes'),
(241, 'theme_switched', '', 'yes'),
(244, 'theme_mods_onepress-child', 'a:26:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:23:\"onepress_social_disable\";i:0;s:24:\"onepress_social_profiles\";a:1:{i:0;a:3:{s:7:\"network\";s:7:\"Twitter\";s:4:\"icon\";s:13:\"fa fa-twitter\";s:4:\"link\";s:30:\"https://twitter.com/hungenbach\";}}s:13:\"footer_layout\";s:1:\"1\";s:27:\"onepress_newsletter_disable\";i:1;s:23:\"onepress_hcl1_btn1_text\";s:8:\"Register\";s:23:\"onepress_hcl1_btn2_text\";s:5:\"Login\";s:23:\"onepress_hcl1_btn2_link\";s:39:\"https://picture.hungenbach.de/rm_login/\";s:23:\"onepress_hcl1_smalltext\";s:48:\"Right here take the <strong>first</strong> step.\";s:23:\"onepress_hcl1_btn1_link\";s:43:\"https://picture.hungenbach.de/registration/\";s:23:\"onepress_hcl1_largetext\";s:82:\"Don\'t give up on your <span class=\"js-rotating\">creativity | ideas | dreams</span>\";s:20:\"onepress_hero_images\";a:1:{i:0;a:1:{s:5:\"image\";a:2:{s:3:\"url\";s:76:\"https://picture.hungenbach.de/wp-content/uploads/2019/01/background_dino.jpg\";s:2:\"id\";i:105;}}}s:23:\"onepress_news_hide_meta\";s:1:\"1\";s:17:\"onepress_news_cat\";s:1:\"4\";s:22:\"onepress_news_subtitle\";s:8:\"whats up\";s:23:\"onepress_about_subtitle\";s:16:\"nice to meet you\";s:26:\"onepress_services_subtitle\";s:36:\"Want to leave your mark on the world\";s:17:\"onepress_services\";a:1:{i:0;a:5:{s:9:\"icon_type\";s:4:\"icon\";s:4:\"icon\";s:18:\"fa fa-fort-awesome\";s:5:\"image\";a:2:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:12:\"content_page\";s:3:\"130\";s:11:\"enable_link\";i:1;}}s:19:\"onepress_about_desc\";s:70:\"We provide creative solutions and create products that make an impact.\";s:23:\"onepress_services_title\";s:12:\"Our Products\";s:26:\"onepress_service_icon_size\";s:2:\"5x\";s:11:\"custom_logo\";s:0:\"\";s:23:\"onepress_hide_sitetitle\";i:0;s:21:\"onepress_hide_tagline\";i:0;}', 'yes'),
(247, 'WPLANG', '', 'yes'),
(248, 'new_admin_email', 'markus@hungenbach.de', 'yes'),
(256, 'rm_option_front_sub_page_id', '22', 'yes'),
(257, 'rm_option_front_login_page_id', '23', 'yes'),
(258, 'rm_option_inserted_sample_data', 'O:8:\"stdClass\":1:{s:5:\"forms\";a:2:{i:0;O:8:\"stdClass\":2:{s:7:\"form_id\";s:1:\"2\";s:9:\"form_type\";s:1:\"1\";}i:1;O:8:\"stdClass\":2:{s:7:\"form_id\";s:1:\"1\";s:9:\"form_type\";s:1:\"0\";}}}', 'no'),
(259, 'rm_option_install_date', '1546923458', 'no'),
(260, 'rm_option_install_type', 'basic', 'no'),
(261, 'rm_option_last_update_time', '1546923458', 'no'),
(262, 'rm_option_ex_chronos_db_version', '1', 'no'),
(263, 'rm_option_last_activation_time', '1546923458', 'no'),
(264, 'rm_option_db_version', '5.2', 'no'),
(265, 'widget_rm_otp_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(266, 'widget_rm_form_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(267, 'widget_rm_login_btn_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(271, 'rm_option_automation_intro_time', '1546923459', 'no'),
(281, 'rm_option_rm_version', '4.5.7.2', 'yes'),
(308, 'rm_option_tour_state', 'a:4:{s:17:\"form_manager_tour\";b:0;s:17:\"form_gensett_tour\";s:5:\"taken\";s:27:\"form_setting_dashboard_tour\";b:0;s:16:\"submissions_tour\";b:0;}', 'no'),
(312, 'nav_menu_options', 'a:1:{s:8:\"auto_add\";a:0:{}}', 'yes'),
(322, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:20:\"markus@hungenbach.de\";s:7:\"version\";s:5:\"5.2.4\";s:9:\"timestamp\";i:1571113081;}', 'no'),
(394, 'frontend_uploader_settings', 'a:14:{s:25:\"enable_akismet_protection\";s:3:\"off\";s:27:\"enable_recaptcha_protection\";s:3:\"off\";s:18:\"recaptcha_site_key\";s:0:\"\";s:20:\"recaptcha_secret_key\";s:0:\"\";s:12:\"notify_admin\";s:3:\"off\";s:23:\"admin_notification_text\";s:132:\"Someone uploaded a new UGC file, please moderate at: https://picture.hungenbach.de/wp-admin/upload.php?page=manage_frontend_uploader\";s:18:\"notification_email\";s:0:\"\";s:11:\"show_author\";s:3:\"off\";s:18:\"enabled_post_types\";a:1:{s:4:\"page\";s:4:\"page\";}s:15:\"wysiwyg_enabled\";s:3:\"off\";s:23:\"auto_approve_user_files\";s:2:\"on\";s:22:\"auto_approve_any_files\";s:3:\"off\";s:19:\"obfuscate_file_name\";s:3:\"off\";s:23:\"suppress_default_fields\";s:3:\"off\";}', 'yes'),
(434, 'widget_wordpress_file_upload_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(440, 'wfu_params_2C37BchERK3xeZZQ', '7b227769646765746964223a22222c2275706c6f61646964223a2231222c2273696e676c65627574746f6e223a2266616c7365222c2275706c6f616470617468223a2275706c6f616473222c226669746d6f6465223a226669786564222c22616c6c6f776e6f66696c65223a2266616c7365222c2272657365746d6f6465223a22616c77617973222c2275706c6f6164726f6c65223a22616c6c2c677565737473222c2275706c6f61647061747465726e73223a222a2e2a222c226d617873697a65223a223530222c2263726561746570617468223a22222c22666f72636566696c656e616d65223a2266616c7365222c226163636573736d6574686f64223a226e6f726d616c222c22667470696e666f223a22222c22757365667470646f6d61696e223a2266616c7365222c22667470706173736976656d6f6465223a2266616c7365222c2266747066696c657065726d697373696f6e73223a22222c2273686f77746172676574666f6c646572223a2266616c7365222c2261736b666f72737562666f6c64657273223a2266616c7365222c22737562666f6c64657274726565223a22222c226475706c696361746573706f6c696379223a226f7665727772697465222c22756e697175657061747465726e223a22696e646578222c227265646972656374223a2266616c7365222c2272656469726563746c696e6b223a22222c2261646d696e6d65737361676573223a66616c73652c22666f726365636c6173736963223a2266616c7365222c22746573746d6f6465223a2266616c7365222c2264656275676d6f6465223a2266616c7365222c22706c6163656d656e7473223a227469746c655c2f66696c656e616d652b73656c656374627574746f6e2b75706c6f6164627574746f6e5c2f737562666f6c646572735c2f75736572646174615c2f6d657373616765222c2275706c6f61647469746c65223a2255706c6f61642066696c6573222c2273656c656374627574746f6e223a2253656c6563742046696c65222c2275706c6f6164627574746f6e223a2255706c6f61642046696c65222c22746172676574666f6c6465726c6162656c223a2255706c6f6164204469726563746f7279222c22737562666f6c6465726c6162656c223a2253656c65637420537562666f6c646572222c22737563636573736d657373616765223a2246696c65202566696c656e616d65252075706c6f61646564207375636365737366756c6c79222c227761726e696e676d657373616765223a2246696c65202566696c656e616d65252075706c6f61646564207375636365737366756c6c79206275742077697468207761726e696e6773222c226572726f726d657373616765223a2246696c65202566696c656e616d6525206e6f742075706c6f61646564222c22776169746d657373616765223a2246696c65202566696c656e616d6525206973206265696e672075706c6f61646564222c2275706c6f61646d65646961627574746f6e223a2255706c6f6164204d65646961222c22766964656f6e616d65223a22766964656f73747265616d222c22696d6167656e616d65223a2273637265656e73686f74222c2272657175697265646c6162656c223a2228726571756972656429222c226e6f74696679223a2266616c7365222c226e6f74696679726563697069656e7473223a22222c226e6f7469667968656164657273223a22222c226e6f746966797375626a656374223a2246696c652055706c6f6164204e6f74696669636174696f6e222c226e6f746966796d657373616765223a224465617220526563697069656e742c256e25256e252020205468697320697320616e206175746f6d617469632064656c6976657279206d65737361676520746f206e6f7469667920796f7520746861742061206e65772066696c6520686173206265656e2075706c6f616465642e256e25256e25426573742052656761726473222c2261747461636866696c65223a2266616c7365222c2261736b636f6e73656e74223a2266616c7365222c22706572736f6e616c646174617479706573223a227573657264617461222c226e6f7472656d656d626572636f6e73656e74223a2266616c7365222c22636f6e73656e7472656a65637475706c6f6164223a2266616c7365222c22636f6e73656e7472656a6563746d657373616765223a22596f7520686176652064656e69656420746f206c6574207468652077656273697465206b65657020796f757220706572736f6e616c20646174612e2055706c6f61642063616e6e6f7420636f6e74696e756521222c22636f6e73656e74666f726d6174223a22726164696f222c22636f6e73656e7470726573656c656374223a226e6f6e65222c22636f6e73656e747175657374696f6e223a2242792061637469766174696e672074686973206f7074696f6e204920616772656520746f206c6574207468652077656273697465206b656570206d7920706572736f6e616c2064617461222c22636f6e73656e74646973636c61696d6572223a22222c22737563636573736d657373616765636f6c6f72223a226565222c22737563636573736d657373616765636f6c6f7273223a22233030363630302c234545464645452c23303036363636222c227761726e696e676d657373616765636f6c6f7273223a22234638383031372c234645463245372c23363333333039222c226661696c6d657373616765636f6c6f7273223a22233636303030302c234646454545452c23363636363030222c22776169746d657373616765636f6c6f7273223a22233636363636362c234545454545452c23333333333333222c22776964746873223a22222c2268656967687473223a22222c227573657264617461223a2266616c7365222c2275736572646174616c6162656c223a22596f7572206d6573736167657c743a746578747c733a6c6566747c723a307c613a307c703a696e6c696e657c643a222c2266696c65626173656c696e6b223a2266616c7365222c226d656469616c696e6b223a2266616c7365222c22706f73746c696e6b223a2266616c7365222c2277656263616d223a2266616c7365222c2277656263616d6d6f6465223a226361707475726520766964656f222c22617564696f63617074757265223a2266616c7365222c22766964656f7769647468223a22222c22766964656f686569676874223a22222c22766964656f617370656374726174696f223a22222c22766964656f6672616d6572617465223a22222c2263616d657261666163696e67223a22616e79222c226d61787265636f726474696d65223a223130222c22706167656964223a34342c22626c6f676964223a312c227068705f656e76223a223634626974222c2261646d696e6572726f7273223a22222c2275736572646174615f6669656c6473223a5b5d2c22737562666f6c646572736172726179223a5b5d2c227375626469725f73656c656374696f6e5f696e646578223a222d31227d', 'yes'),
(446, 'wpfm_settings', 'a:53:{s:15:\"wpfm_files_view\";s:4:\"grid\";s:15:\"wpfm_thumb_size\";s:0:\"\";s:17:\"wpfm_button_title\";s:11:\"Select File\";s:17:\"wpfm_upload_title\";s:9:\"Save File\";s:18:\"wpfm_max_file_size\";s:4:\"10mb\";s:14:\"wpfm_max_files\";s:0:\"\";s:19:\"wpfm_max_files_user\";s:0:\"\";s:16:\"wpfm_file_format\";s:0:\"\";s:15:\"wpfm_file_types\";s:11:\"jpg,png,gif\";s:17:\"wpfm_file_sharing\";s:0:\"\";s:27:\"wpfm_file_allow_drag_n_drop\";s:0:\"\";s:14:\"wpfm_min_files\";s:0:\"\";s:18:\"wpfm_default_quota\";s:0:\"\";s:22:\"wpfm_file_notification\";s:0:\"\";s:15:\"wpfm_from_email\";s:0:\"\";s:16:\"wpfm_public_user\";s:0:\"\";s:21:\"wpfm_email_recipients\";s:0:\"\";s:16:\"wpfm_file_rename\";s:0:\"\";s:22:\"wpfm_disable_bootstarp\";s:3:\"yes\";s:24:\"wpfm_disable_breadcrumbs\";s:3:\"yes\";s:18:\"wpfm_files_per_row\";s:0:\"\";s:23:\"wpfm_allow_guest_upload\";s:0:\"\";s:15:\"wpfm_create_dir\";s:0:\"\";s:14:\"wpfm_send_file\";s:0:\"\";s:16:\"wpfm_file_groups\";s:0:\"\";s:20:\"wpfm_file_groups_add\";s:0:\"\";s:23:\"wpfm_show_logout_button\";s:0:\"\";s:18:\"wpfm_hide_uploader\";s:0:\"\";s:15:\"wpfm_hide_files\";s:0:\"\";s:15:\"wpfm_file_saved\";s:17:\"File(s) Uploaded!\";s:19:\"wpfm_public_message\";s:0:\"\";s:17:\"wpfm_role_message\";s:0:\"\";s:27:\"wpfm_email_message_sendfile\";s:0:\"\";s:31:\"wpfm_email_message_notification\";s:0:\"\";s:24:\"wpfm_enable_image_sizing\";s:0:\"\";s:20:\"wpfm_image_min_width\";s:0:\"\";s:21:\"wpfm_image_min_height\";s:0:\"\";s:20:\"wpfm_image_max_width\";s:0:\"\";s:21:\"wpfm_image_max_height\";s:0:\"\";s:21:\"wpfm_resize_transform\";s:0:\"\";s:21:\"wpfm_ftp_notification\";s:0:\"\";s:22:\"wpfm_email_message_ftp\";s:0:\"\";s:18:\"wpfm_enable_amazon\";s:0:\"\";s:18:\"wpfm_amazon_apikey\";s:0:\"\";s:21:\"wpfm_amazon_apisecret\";s:0:\"\";s:18:\"wpfm_amazon_bucket\";s:0:\"\";s:19:\"wpfm_amazon_expires\";s:0:\"\";s:18:\"wpfm_amazon_region\";s:0:\"\";s:15:\"wpfm_acl_public\";s:0:\"\";s:24:\"wpfm_enable_google_drive\";s:0:\"\";s:18:\"wpfm_google_apikey\";s:0:\"\";s:20:\"wpfm_google_clientid\";s:0:\"\";s:23:\"wpfm_allow_file_sharing\";s:0:\"\";}', 'yes'),
(460, 'wpcf7', 'a:3:{s:7:\"version\";s:5:\"5.1.3\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1547269225;s:7:\"version\";s:5:\"5.1.1\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}s:9:\"recaptcha\";a:1:{s:40:\"6LcbG4kUAAAAADuLZizJUdomK5c2v9bMynpfd3vQ\";s:40:\"6LcbG4kUAAAAAOofi7n56CTmtrauP_pDu7MXH5tZ\";}}', 'yes'),
(462, 'widget_akismet_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(512, 'mw_adminimize', 'a:99:{s:29:\"mw_adminimize_admin_bar_nodes\";a:24:{s:12:\"user-actions\";O:8:\"stdClass\":6:{s:2:\"id\";s:12:\"user-actions\";s:5:\"title\";b:0;s:6:\"parent\";s:10:\"my-account\";s:4:\"href\";b:0;s:5:\"group\";b:1;s:4:\"meta\";a:0:{}}s:9:\"user-info\";O:8:\"stdClass\":6:{s:2:\"id\";s:9:\"user-info\";s:5:\"title\";s:325:\"<img alt=\'\' src=\'https://secure.gravatar.com/avatar/bc0aba7e498d6a0e32d9193d791e75ac?s=64&#038;d=mm&#038;r=g\' srcset=\'https://secure.gravatar.com/avatar/bc0aba7e498d6a0e32d9193d791e75ac?s=128&#038;d=mm&#038;r=g 2x\' class=\'avatar avatar-64 photo\' height=\'64\' width=\'64\' /><span class=\'display-name\'>markus@hungenbach.de</span>\";s:6:\"parent\";s:12:\"user-actions\";s:4:\"href\";s:50:\"https://picture.hungenbach.de/wp-admin/profile.php\";s:5:\"group\";b:0;s:4:\"meta\";a:1:{s:8:\"tabindex\";i:-1;}}s:12:\"edit-profile\";O:8:\"stdClass\":6:{s:2:\"id\";s:12:\"edit-profile\";s:5:\"title\";s:15:\"Edit My Profile\";s:6:\"parent\";s:12:\"user-actions\";s:4:\"href\";s:50:\"https://picture.hungenbach.de/wp-admin/profile.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:6:\"logout\";O:8:\"stdClass\":6:{s:2:\"id\";s:6:\"logout\";s:5:\"title\";s:7:\"Log Out\";s:6:\"parent\";s:12:\"user-actions\";s:4:\"href\";s:80:\"https://picture.hungenbach.de/wp-login.php?action=logout&amp;_wpnonce=c5d4d362f9\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:11:\"menu-toggle\";O:8:\"stdClass\":6:{s:2:\"id\";s:11:\"menu-toggle\";s:5:\"title\";s:73:\"<span class=\"ab-icon\"></span><span class=\"screen-reader-text\">Menu</span>\";s:6:\"parent\";b:0;s:4:\"href\";s:1:\"#\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:10:\"my-account\";O:8:\"stdClass\":6:{s:2:\"id\";s:10:\"my-account\";s:5:\"title\";s:331:\"Howdy, <span class=\"display-name\">markus@hungenbach.de</span><img alt=\'\' src=\'https://secure.gravatar.com/avatar/bc0aba7e498d6a0e32d9193d791e75ac?s=26&#038;d=mm&#038;r=g\' srcset=\'https://secure.gravatar.com/avatar/bc0aba7e498d6a0e32d9193d791e75ac?s=52&#038;d=mm&#038;r=g 2x\' class=\'avatar avatar-26 photo\' height=\'26\' width=\'26\' />\";s:6:\"parent\";s:13:\"top-secondary\";s:4:\"href\";s:50:\"https://picture.hungenbach.de/wp-admin/profile.php\";s:5:\"group\";b:0;s:4:\"meta\";a:1:{s:5:\"class\";s:11:\"with-avatar\";}}s:7:\"wp-logo\";O:8:\"stdClass\":6:{s:2:\"id\";s:7:\"wp-logo\";s:5:\"title\";s:84:\"<span class=\"ab-icon\"></span><span class=\"screen-reader-text\">About WordPress</span>\";s:6:\"parent\";b:0;s:4:\"href\";s:48:\"https://picture.hungenbach.de/wp-admin/about.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:5:\"about\";O:8:\"stdClass\":6:{s:2:\"id\";s:5:\"about\";s:5:\"title\";s:15:\"About WordPress\";s:6:\"parent\";s:7:\"wp-logo\";s:4:\"href\";s:48:\"https://picture.hungenbach.de/wp-admin/about.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:5:\"wporg\";O:8:\"stdClass\":6:{s:2:\"id\";s:5:\"wporg\";s:5:\"title\";s:13:\"WordPress.org\";s:6:\"parent\";s:16:\"wp-logo-external\";s:4:\"href\";s:22:\"https://wordpress.org/\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:13:\"documentation\";O:8:\"stdClass\":6:{s:2:\"id\";s:13:\"documentation\";s:5:\"title\";s:13:\"Documentation\";s:6:\"parent\";s:16:\"wp-logo-external\";s:4:\"href\";s:28:\"https://codex.wordpress.org/\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:14:\"support-forums\";O:8:\"stdClass\":6:{s:2:\"id\";s:14:\"support-forums\";s:5:\"title\";s:14:\"Support Forums\";s:6:\"parent\";s:16:\"wp-logo-external\";s:4:\"href\";s:30:\"https://wordpress.org/support/\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:8:\"feedback\";O:8:\"stdClass\":6:{s:2:\"id\";s:8:\"feedback\";s:5:\"title\";s:8:\"Feedback\";s:6:\"parent\";s:16:\"wp-logo-external\";s:4:\"href\";s:57:\"https://wordpress.org/support/forum/requests-and-feedback\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:9:\"site-name\";O:8:\"stdClass\":6:{s:2:\"id\";s:9:\"site-name\";s:5:\"title\";s:8:\"creative\";s:6:\"parent\";b:0;s:4:\"href\";s:30:\"https://picture.hungenbach.de/\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:9:\"view-site\";O:8:\"stdClass\":6:{s:2:\"id\";s:9:\"view-site\";s:5:\"title\";s:10:\"Visit Site\";s:6:\"parent\";s:9:\"site-name\";s:4:\"href\";s:30:\"https://picture.hungenbach.de/\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:7:\"updates\";O:8:\"stdClass\":6:{s:2:\"id\";s:7:\"updates\";s:5:\"title\";s:115:\"<span class=\"ab-icon\"></span><span class=\"ab-label\">4</span><span class=\"screen-reader-text\">4 Theme Updates</span>\";s:6:\"parent\";b:0;s:4:\"href\";s:54:\"https://picture.hungenbach.de/wp-admin/update-core.php\";s:5:\"group\";b:0;s:4:\"meta\";a:1:{s:5:\"title\";s:15:\"4 Theme Updates\";}}s:8:\"comments\";O:8:\"stdClass\":6:{s:2:\"id\";s:8:\"comments\";s:5:\"title\";s:184:\"<span class=\"ab-icon\"></span><span class=\"ab-label awaiting-mod pending-count count-0\" aria-hidden=\"true\">0</span><span class=\"screen-reader-text\">0 comments awaiting moderation</span>\";s:6:\"parent\";b:0;s:4:\"href\";s:56:\"https://picture.hungenbach.de/wp-admin/edit-comments.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:11:\"new-content\";O:8:\"stdClass\":6:{s:2:\"id\";s:11:\"new-content\";s:5:\"title\";s:62:\"<span class=\"ab-icon\"></span><span class=\"ab-label\">New</span>\";s:6:\"parent\";b:0;s:4:\"href\";s:51:\"https://picture.hungenbach.de/wp-admin/post-new.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:8:\"new-post\";O:8:\"stdClass\":6:{s:2:\"id\";s:8:\"new-post\";s:5:\"title\";s:4:\"Post\";s:6:\"parent\";s:11:\"new-content\";s:4:\"href\";s:51:\"https://picture.hungenbach.de/wp-admin/post-new.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:9:\"new-media\";O:8:\"stdClass\":6:{s:2:\"id\";s:9:\"new-media\";s:5:\"title\";s:5:\"Media\";s:6:\"parent\";s:11:\"new-content\";s:4:\"href\";s:52:\"https://picture.hungenbach.de/wp-admin/media-new.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:8:\"new-page\";O:8:\"stdClass\":6:{s:2:\"id\";s:8:\"new-page\";s:5:\"title\";s:4:\"Page\";s:6:\"parent\";s:11:\"new-content\";s:4:\"href\";s:66:\"https://picture.hungenbach.de/wp-admin/post-new.php?post_type=page\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:14:\"new-wpfm-files\";O:8:\"stdClass\":6:{s:2:\"id\";s:14:\"new-wpfm-files\";s:5:\"title\";s:10:\"User Files\";s:6:\"parent\";s:11:\"new-content\";s:4:\"href\";s:72:\"https://picture.hungenbach.de/wp-admin/post-new.php?post_type=wpfm-files\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:8:\"new-user\";O:8:\"stdClass\":6:{s:2:\"id\";s:8:\"new-user\";s:5:\"title\";s:4:\"User\";s:6:\"parent\";s:11:\"new-content\";s:4:\"href\";s:51:\"https://picture.hungenbach.de/wp-admin/user-new.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:13:\"top-secondary\";O:8:\"stdClass\":6:{s:2:\"id\";s:13:\"top-secondary\";s:5:\"title\";b:0;s:6:\"parent\";b:0;s:4:\"href\";b:0;s:5:\"group\";b:1;s:4:\"meta\";a:1:{s:5:\"class\";s:16:\"ab-top-secondary\";}}s:16:\"wp-logo-external\";O:8:\"stdClass\":6:{s:2:\"id\";s:16:\"wp-logo-external\";s:5:\"title\";b:0;s:6:\"parent\";s:7:\"wp-logo\";s:4:\"href\";b:0;s:5:\"group\";b:1;s:4:\"meta\";a:1:{s:5:\"class\";s:16:\"ab-sub-secondary\";}}}s:52:\"mw_adminimize_disabled_admin_bar_administrator_items\";a:0:{}s:45:\"mw_adminimize_disabled_admin_bar_editor_items\";a:0:{}s:45:\"mw_adminimize_disabled_admin_bar_author_items\";a:0:{}s:50:\"mw_adminimize_disabled_admin_bar_contributor_items\";a:0:{}s:49:\"mw_adminimize_disabled_admin_bar_subscriber_items\";a:24:{i:0;s:12:\"user-actions\";i:1;s:9:\"user-info\";i:2;s:12:\"edit-profile\";i:3;s:6:\"logout\";i:4;s:11:\"menu-toggle\";i:5;s:10:\"my-account\";i:6;s:7:\"wp-logo\";i:7;s:5:\"about\";i:8;s:5:\"wporg\";i:9;s:13:\"documentation\";i:10;s:14:\"support-forums\";i:11;s:8:\"feedback\";i:12;s:9:\"site-name\";i:13;s:9:\"view-site\";i:14;s:7:\"updates\";i:15;s:8:\"comments\";i:16;s:11:\"new-content\";i:17;s:8:\"new-post\";i:18;s:9:\"new-media\";i:19;s:8:\"new-page\";i:20;s:14:\"new-wpfm-files\";i:21;s:8:\"new-user\";i:22;s:13:\"top-secondary\";i:23;s:16:\"wp-logo-external\";}s:19:\"mw_adminimize_debug\";i:0;s:28:\"mw_adminimize_multiple_roles\";i:0;s:29:\"mw_adminimize_support_bbpress\";i:0;s:33:\"mw_adminimize_prevent_page_access\";i:0;s:38:\"mw_adminimize_admin_bar_frontend_nodes\";a:30:{s:12:\"user-actions\";O:8:\"stdClass\":6:{s:2:\"id\";s:12:\"user-actions\";s:5:\"title\";b:0;s:6:\"parent\";s:10:\"my-account\";s:4:\"href\";b:0;s:5:\"group\";b:1;s:4:\"meta\";a:0:{}}s:9:\"user-info\";O:8:\"stdClass\":6:{s:2:\"id\";s:9:\"user-info\";s:5:\"title\";s:325:\"<img alt=\'\' src=\'https://secure.gravatar.com/avatar/bc0aba7e498d6a0e32d9193d791e75ac?s=64&#038;d=mm&#038;r=g\' srcset=\'https://secure.gravatar.com/avatar/bc0aba7e498d6a0e32d9193d791e75ac?s=128&#038;d=mm&#038;r=g 2x\' class=\'avatar avatar-64 photo\' height=\'64\' width=\'64\' /><span class=\'display-name\'>markus@hungenbach.de</span>\";s:6:\"parent\";s:12:\"user-actions\";s:4:\"href\";s:50:\"https://picture.hungenbach.de/wp-admin/profile.php\";s:5:\"group\";b:0;s:4:\"meta\";a:1:{s:8:\"tabindex\";i:-1;}}s:12:\"edit-profile\";O:8:\"stdClass\":6:{s:2:\"id\";s:12:\"edit-profile\";s:5:\"title\";s:15:\"Edit My Profile\";s:6:\"parent\";s:12:\"user-actions\";s:4:\"href\";s:50:\"https://picture.hungenbach.de/wp-admin/profile.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:6:\"logout\";O:8:\"stdClass\":6:{s:2:\"id\";s:6:\"logout\";s:5:\"title\";s:7:\"Log Out\";s:6:\"parent\";s:12:\"user-actions\";s:4:\"href\";s:80:\"https://picture.hungenbach.de/wp-login.php?action=logout&amp;_wpnonce=c5d4d362f9\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:6:\"search\";O:8:\"stdClass\":6:{s:2:\"id\";s:6:\"search\";s:5:\"title\";s:316:\"<form action=\"https://picture.hungenbach.de/\" method=\"get\" id=\"adminbarsearch\"><input class=\"adminbar-input\" name=\"s\" id=\"adminbar-search\" type=\"text\" value=\"\" maxlength=\"150\" /><label for=\"adminbar-search\" class=\"screen-reader-text\">Search</label><input type=\"submit\" class=\"adminbar-button\" value=\"Search\"/></form>\";s:6:\"parent\";s:13:\"top-secondary\";s:4:\"href\";b:0;s:5:\"group\";b:0;s:4:\"meta\";a:2:{s:5:\"class\";s:16:\"admin-bar-search\";s:8:\"tabindex\";i:-1;}}s:10:\"my-account\";O:8:\"stdClass\":6:{s:2:\"id\";s:10:\"my-account\";s:5:\"title\";s:331:\"Howdy, <span class=\"display-name\">markus@hungenbach.de</span><img alt=\'\' src=\'https://secure.gravatar.com/avatar/bc0aba7e498d6a0e32d9193d791e75ac?s=26&#038;d=mm&#038;r=g\' srcset=\'https://secure.gravatar.com/avatar/bc0aba7e498d6a0e32d9193d791e75ac?s=52&#038;d=mm&#038;r=g 2x\' class=\'avatar avatar-26 photo\' height=\'26\' width=\'26\' />\";s:6:\"parent\";s:13:\"top-secondary\";s:4:\"href\";s:50:\"https://picture.hungenbach.de/wp-admin/profile.php\";s:5:\"group\";b:0;s:4:\"meta\";a:1:{s:5:\"class\";s:11:\"with-avatar\";}}s:7:\"wp-logo\";O:8:\"stdClass\":6:{s:2:\"id\";s:7:\"wp-logo\";s:5:\"title\";s:84:\"<span class=\"ab-icon\"></span><span class=\"screen-reader-text\">About WordPress</span>\";s:6:\"parent\";b:0;s:4:\"href\";s:48:\"https://picture.hungenbach.de/wp-admin/about.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:5:\"about\";O:8:\"stdClass\":6:{s:2:\"id\";s:5:\"about\";s:5:\"title\";s:15:\"About WordPress\";s:6:\"parent\";s:7:\"wp-logo\";s:4:\"href\";s:48:\"https://picture.hungenbach.de/wp-admin/about.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:5:\"wporg\";O:8:\"stdClass\":6:{s:2:\"id\";s:5:\"wporg\";s:5:\"title\";s:13:\"WordPress.org\";s:6:\"parent\";s:16:\"wp-logo-external\";s:4:\"href\";s:22:\"https://wordpress.org/\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:13:\"documentation\";O:8:\"stdClass\":6:{s:2:\"id\";s:13:\"documentation\";s:5:\"title\";s:13:\"Documentation\";s:6:\"parent\";s:16:\"wp-logo-external\";s:4:\"href\";s:28:\"https://codex.wordpress.org/\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:14:\"support-forums\";O:8:\"stdClass\":6:{s:2:\"id\";s:14:\"support-forums\";s:5:\"title\";s:14:\"Support Forums\";s:6:\"parent\";s:16:\"wp-logo-external\";s:4:\"href\";s:30:\"https://wordpress.org/support/\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:8:\"feedback\";O:8:\"stdClass\":6:{s:2:\"id\";s:8:\"feedback\";s:5:\"title\";s:8:\"Feedback\";s:6:\"parent\";s:16:\"wp-logo-external\";s:4:\"href\";s:57:\"https://wordpress.org/support/forum/requests-and-feedback\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:9:\"site-name\";O:8:\"stdClass\":6:{s:2:\"id\";s:9:\"site-name\";s:5:\"title\";s:8:\"creative\";s:6:\"parent\";b:0;s:4:\"href\";s:39:\"https://picture.hungenbach.de/wp-admin/\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:9:\"dashboard\";O:8:\"stdClass\":6:{s:2:\"id\";s:9:\"dashboard\";s:5:\"title\";s:9:\"Dashboard\";s:6:\"parent\";s:9:\"site-name\";s:4:\"href\";s:39:\"https://picture.hungenbach.de/wp-admin/\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:10:\"appearance\";O:8:\"stdClass\":6:{s:2:\"id\";s:10:\"appearance\";s:5:\"title\";b:0;s:6:\"parent\";s:9:\"site-name\";s:4:\"href\";b:0;s:5:\"group\";b:1;s:4:\"meta\";a:0:{}}s:6:\"themes\";O:8:\"stdClass\":6:{s:2:\"id\";s:6:\"themes\";s:5:\"title\";s:6:\"Themes\";s:6:\"parent\";s:10:\"appearance\";s:4:\"href\";s:49:\"https://picture.hungenbach.de/wp-admin/themes.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:7:\"widgets\";O:8:\"stdClass\":6:{s:2:\"id\";s:7:\"widgets\";s:5:\"title\";s:7:\"Widgets\";s:6:\"parent\";s:10:\"appearance\";s:4:\"href\";s:50:\"https://picture.hungenbach.de/wp-admin/widgets.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:5:\"menus\";O:8:\"stdClass\":6:{s:2:\"id\";s:5:\"menus\";s:5:\"title\";s:5:\"Menus\";s:6:\"parent\";s:10:\"appearance\";s:4:\"href\";s:52:\"https://picture.hungenbach.de/wp-admin/nav-menus.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:9:\"customize\";O:8:\"stdClass\":6:{s:2:\"id\";s:9:\"customize\";s:5:\"title\";s:9:\"Customize\";s:6:\"parent\";b:0;s:4:\"href\";s:106:\"https://picture.hungenbach.de/wp-admin/customize.php?url=https%3A%2F%2Fpicture.hungenbach.de%2Frm_login%2F\";s:5:\"group\";b:0;s:4:\"meta\";a:1:{s:5:\"class\";s:20:\"hide-if-no-customize\";}}s:7:\"updates\";O:8:\"stdClass\":6:{s:2:\"id\";s:7:\"updates\";s:5:\"title\";s:115:\"<span class=\"ab-icon\"></span><span class=\"ab-label\">4</span><span class=\"screen-reader-text\">4 Theme Updates</span>\";s:6:\"parent\";b:0;s:4:\"href\";s:54:\"https://picture.hungenbach.de/wp-admin/update-core.php\";s:5:\"group\";b:0;s:4:\"meta\";a:1:{s:5:\"title\";s:15:\"4 Theme Updates\";}}s:8:\"comments\";O:8:\"stdClass\":6:{s:2:\"id\";s:8:\"comments\";s:5:\"title\";s:184:\"<span class=\"ab-icon\"></span><span class=\"ab-label awaiting-mod pending-count count-0\" aria-hidden=\"true\">0</span><span class=\"screen-reader-text\">0 comments awaiting moderation</span>\";s:6:\"parent\";b:0;s:4:\"href\";s:56:\"https://picture.hungenbach.de/wp-admin/edit-comments.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:11:\"new-content\";O:8:\"stdClass\":6:{s:2:\"id\";s:11:\"new-content\";s:5:\"title\";s:62:\"<span class=\"ab-icon\"></span><span class=\"ab-label\">New</span>\";s:6:\"parent\";b:0;s:4:\"href\";s:51:\"https://picture.hungenbach.de/wp-admin/post-new.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:8:\"new-post\";O:8:\"stdClass\":6:{s:2:\"id\";s:8:\"new-post\";s:5:\"title\";s:4:\"Post\";s:6:\"parent\";s:11:\"new-content\";s:4:\"href\";s:51:\"https://picture.hungenbach.de/wp-admin/post-new.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:9:\"new-media\";O:8:\"stdClass\":6:{s:2:\"id\";s:9:\"new-media\";s:5:\"title\";s:5:\"Media\";s:6:\"parent\";s:11:\"new-content\";s:4:\"href\";s:52:\"https://picture.hungenbach.de/wp-admin/media-new.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:8:\"new-page\";O:8:\"stdClass\":6:{s:2:\"id\";s:8:\"new-page\";s:5:\"title\";s:4:\"Page\";s:6:\"parent\";s:11:\"new-content\";s:4:\"href\";s:66:\"https://picture.hungenbach.de/wp-admin/post-new.php?post_type=page\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:14:\"new-wpfm-files\";O:8:\"stdClass\":6:{s:2:\"id\";s:14:\"new-wpfm-files\";s:5:\"title\";s:10:\"User Files\";s:6:\"parent\";s:11:\"new-content\";s:4:\"href\";s:72:\"https://picture.hungenbach.de/wp-admin/post-new.php?post_type=wpfm-files\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:8:\"new-user\";O:8:\"stdClass\":6:{s:2:\"id\";s:8:\"new-user\";s:5:\"title\";s:4:\"User\";s:6:\"parent\";s:11:\"new-content\";s:4:\"href\";s:51:\"https://picture.hungenbach.de/wp-admin/user-new.php\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:4:\"edit\";O:8:\"stdClass\":6:{s:2:\"id\";s:4:\"edit\";s:5:\"title\";s:9:\"Edit Page\";s:6:\"parent\";b:0;s:4:\"href\";s:71:\"https://picture.hungenbach.de/wp-admin/post.php?post=23&amp;action=edit\";s:5:\"group\";b:0;s:4:\"meta\";a:0:{}}s:13:\"top-secondary\";O:8:\"stdClass\":6:{s:2:\"id\";s:13:\"top-secondary\";s:5:\"title\";b:0;s:6:\"parent\";b:0;s:4:\"href\";b:0;s:5:\"group\";b:1;s:4:\"meta\";a:1:{s:5:\"class\";s:16:\"ab-top-secondary\";}}s:16:\"wp-logo-external\";O:8:\"stdClass\":6:{s:2:\"id\";s:16:\"wp-logo-external\";s:5:\"title\";b:0;s:6:\"parent\";s:7:\"wp-logo\";s:4:\"href\";b:0;s:5:\"group\";b:1;s:4:\"meta\";a:1:{s:5:\"class\";s:16:\"ab-sub-secondary\";}}}s:61:\"mw_adminimize_disabled_admin_bar_frontend_administrator_items\";a:0:{}s:54:\"mw_adminimize_disabled_admin_bar_frontend_editor_items\";a:0:{}s:54:\"mw_adminimize_disabled_admin_bar_frontend_author_items\";a:0:{}s:59:\"mw_adminimize_disabled_admin_bar_frontend_contributor_items\";a:0:{}s:58:\"mw_adminimize_disabled_admin_bar_frontend_subscriber_items\";a:0:{}s:24:\"_mw_adminimize_user_info\";i:0;s:21:\"_mw_adminimize_footer\";i:0;s:21:\"_mw_adminimize_header\";i:0;s:34:\"_mw_adminimize_exclude_super_admin\";i:0;s:24:\"_mw_adminimize_tb_window\";i:0;s:23:\"_mw_adminimize_cat_full\";i:0;s:26:\"_mw_adminimize_db_redirect\";i:0;s:26:\"_mw_adminimize_ui_redirect\";i:0;s:21:\"_mw_adminimize_advice\";i:0;s:25:\"_mw_adminimize_advice_txt\";s:0:\"\";s:24:\"_mw_adminimize_timestamp\";i:0;s:30:\"_mw_adminimize_db_redirect_txt\";s:0:\"\";s:47:\"mw_adminimize_disabled_menu_administrator_items\";a:0:{}s:50:\"mw_adminimize_disabled_submenu_administrator_items\";a:0:{}s:40:\"mw_adminimize_disabled_menu_editor_items\";a:0:{}s:43:\"mw_adminimize_disabled_submenu_editor_items\";a:0:{}s:40:\"mw_adminimize_disabled_menu_author_items\";a:0:{}s:43:\"mw_adminimize_disabled_submenu_author_items\";a:0:{}s:45:\"mw_adminimize_disabled_menu_contributor_items\";a:0:{}s:48:\"mw_adminimize_disabled_submenu_contributor_items\";a:0:{}s:44:\"mw_adminimize_disabled_menu_subscriber_items\";a:0:{}s:47:\"mw_adminimize_disabled_submenu_subscriber_items\";a:0:{}s:28:\"_mw_adminimize_own_menu_slug\";s:0:\"\";s:35:\"_mw_adminimize_own_menu_custom_slug\";s:0:\"\";s:56:\"mw_adminimize_disabled_global_option_administrator_items\";a:0:{}s:57:\"mw_adminimize_disabled_metaboxes_post_administrator_items\";a:0:{}s:57:\"mw_adminimize_disabled_metaboxes_page_administrator_items\";a:0:{}s:63:\"mw_adminimize_disabled_metaboxes_wpfm-files_administrator_items\";a:0:{}s:54:\"mw_adminimize_disabled_link_option_administrator_items\";a:0:{}s:58:\"mw_adminimize_disabled_nav_menu_option_administrator_items\";a:0:{}s:56:\"mw_adminimize_disabled_widget_option_administrator_items\";a:0:{}s:59:\"mw_adminimize_disabled_dashboard_option_administrator_items\";a:0:{}s:49:\"mw_adminimize_disabled_global_option_editor_items\";a:0:{}s:50:\"mw_adminimize_disabled_metaboxes_post_editor_items\";a:0:{}s:50:\"mw_adminimize_disabled_metaboxes_page_editor_items\";a:0:{}s:56:\"mw_adminimize_disabled_metaboxes_wpfm-files_editor_items\";a:0:{}s:47:\"mw_adminimize_disabled_link_option_editor_items\";a:0:{}s:51:\"mw_adminimize_disabled_nav_menu_option_editor_items\";a:0:{}s:49:\"mw_adminimize_disabled_widget_option_editor_items\";a:0:{}s:52:\"mw_adminimize_disabled_dashboard_option_editor_items\";a:0:{}s:49:\"mw_adminimize_disabled_global_option_author_items\";a:0:{}s:50:\"mw_adminimize_disabled_metaboxes_post_author_items\";a:0:{}s:50:\"mw_adminimize_disabled_metaboxes_page_author_items\";a:0:{}s:56:\"mw_adminimize_disabled_metaboxes_wpfm-files_author_items\";a:0:{}s:47:\"mw_adminimize_disabled_link_option_author_items\";a:0:{}s:51:\"mw_adminimize_disabled_nav_menu_option_author_items\";a:0:{}s:49:\"mw_adminimize_disabled_widget_option_author_items\";a:0:{}s:52:\"mw_adminimize_disabled_dashboard_option_author_items\";a:0:{}s:54:\"mw_adminimize_disabled_global_option_contributor_items\";a:0:{}s:55:\"mw_adminimize_disabled_metaboxes_post_contributor_items\";a:0:{}s:55:\"mw_adminimize_disabled_metaboxes_page_contributor_items\";a:0:{}s:61:\"mw_adminimize_disabled_metaboxes_wpfm-files_contributor_items\";a:0:{}s:52:\"mw_adminimize_disabled_link_option_contributor_items\";a:0:{}s:56:\"mw_adminimize_disabled_nav_menu_option_contributor_items\";a:0:{}s:54:\"mw_adminimize_disabled_widget_option_contributor_items\";a:0:{}s:57:\"mw_adminimize_disabled_dashboard_option_contributor_items\";a:0:{}s:53:\"mw_adminimize_disabled_global_option_subscriber_items\";a:7:{i:0;s:15:\".show-admin-bar\";i:1;s:17:\"#favorite-actions\";i:2;s:12:\"#screen-meta\";i:3;s:42:\"#screen-options, #screen-options-link-wrap\";i:4;s:26:\"#contextual-help-link-wrap\";i:5;s:34:\"#your-profile .form-table fieldset\";i:6;s:14:\".admin-notices\";}s:54:\"mw_adminimize_disabled_metaboxes_post_subscriber_items\";a:0:{}s:54:\"mw_adminimize_disabled_metaboxes_page_subscriber_items\";a:0:{}s:60:\"mw_adminimize_disabled_metaboxes_wpfm-files_subscriber_items\";a:0:{}s:51:\"mw_adminimize_disabled_link_option_subscriber_items\";a:0:{}s:55:\"mw_adminimize_disabled_nav_menu_option_subscriber_items\";a:0:{}s:53:\"mw_adminimize_disabled_widget_option_subscriber_items\";a:0:{}s:56:\"mw_adminimize_disabled_dashboard_option_subscriber_items\";a:0:{}s:25:\"_mw_adminimize_own_values\";s:0:\"\";s:26:\"_mw_adminimize_own_options\";s:0:\"\";s:30:\"_mw_adminimize_own_post_values\";s:0:\"\";s:31:\"_mw_adminimize_own_post_options\";s:0:\"\";s:30:\"_mw_adminimize_own_page_values\";s:0:\"\";s:31:\"_mw_adminimize_own_page_options\";s:0:\"\";s:36:\"_mw_adminimize_own_values_wpfm-files\";s:0:\"\";s:37:\"_mw_adminimize_own_options_wpfm-files\";s:0:\"\";s:30:\"_mw_adminimize_own_link_values\";s:0:\"\";s:31:\"_mw_adminimize_own_link_options\";s:0:\"\";s:34:\"_mw_adminimize_own_nav_menu_values\";s:0:\"\";s:35:\"_mw_adminimize_own_nav_menu_options\";s:0:\"\";s:32:\"_mw_adminimize_own_widget_values\";s:0:\"\";s:33:\"_mw_adminimize_own_widget_options\";s:0:\"\";s:35:\"_mw_adminimize_own_dashboard_values\";s:0:\"\";s:36:\"_mw_adminimize_own_dashboard_options\";s:0:\"\";s:31:\"mw_adminimize_dashboard_widgets\";a:5:{s:19:\"dashboard_right_now\";a:4:{s:2:\"id\";s:19:\"dashboard_right_now\";s:5:\"title\";s:11:\"At a Glance\";s:7:\"context\";s:6:\"normal\";s:8:\"priority\";s:4:\"core\";}s:18:\"dashboard_activity\";a:4:{s:2:\"id\";s:18:\"dashboard_activity\";s:5:\"title\";s:8:\"Activity\";s:7:\"context\";s:6:\"normal\";s:8:\"priority\";s:4:\"core\";}s:27:\"rm_dashboard_widget_display\";a:4:{s:2:\"id\";s:27:\"rm_dashboard_widget_display\";s:5:\"title\";s:21:\"Registration Activity\";s:7:\"context\";s:6:\"normal\";s:8:\"priority\";s:4:\"core\";}s:21:\"dashboard_quick_press\";a:4:{s:2:\"id\";s:21:\"dashboard_quick_press\";s:5:\"title\";s:0:\"\";s:7:\"context\";s:4:\"side\";s:8:\"priority\";s:4:\"core\";}s:17:\"dashboard_primary\";a:4:{s:2:\"id\";s:17:\"dashboard_primary\";s:5:\"title\";s:25:\"WordPress Events and News\";s:7:\"context\";s:4:\"side\";s:8:\"priority\";s:4:\"core\";}}s:26:\"mw_adminimize_default_menu\";a:15:{i:2;a:7:{i:0;s:9:\"Dashboard\";i:1;s:4:\"read\";i:2;s:9:\"index.php\";i:3;s:0:\"\";i:4;s:57:\"menu-top menu-top-first menu-icon-dashboard menu-top-last\";i:5;s:14:\"menu-dashboard\";i:6;s:19:\"dashicons-dashboard\";}i:4;a:5:{i:0;s:0:\"\";i:1;s:4:\"read\";i:2;s:10:\"separator1\";i:3;s:0:\"\";i:4;s:17:\"wp-menu-separator\";}i:5;a:7:{i:0;s:5:\"Posts\";i:1;s:10:\"edit_posts\";i:2;s:8:\"edit.php\";i:3;s:0:\"\";i:4;s:52:\"menu-top menu-icon-post open-if-no-js menu-top-first\";i:5;s:10:\"menu-posts\";i:6;s:20:\"dashicons-admin-post\";}i:10;a:7:{i:0;s:5:\"Media\";i:1;s:12:\"upload_files\";i:2;s:10:\"upload.php\";i:3;s:0:\"\";i:4;s:24:\"menu-top menu-icon-media\";i:5;s:10:\"menu-media\";i:6;s:21:\"dashicons-admin-media\";}i:20;a:7:{i:0;s:5:\"Pages\";i:1;s:10:\"edit_pages\";i:2;s:23:\"edit.php?post_type=page\";i:3;s:0:\"\";i:4;s:23:\"menu-top menu-icon-page\";i:5;s:10:\"menu-pages\";i:6;s:20:\"dashicons-admin-page\";}i:25;a:7:{i:0;s:87:\"Comments <span class=\"awaiting-mod count-0\"><span class=\"pending-count\">0</span></span>\";i:1;s:10:\"edit_posts\";i:2;s:17:\"edit-comments.php\";i:3;s:0:\"\";i:4;s:27:\"menu-top menu-icon-comments\";i:5;s:13:\"menu-comments\";i:6;s:24:\"dashicons-admin-comments\";}i:26;a:7:{i:0;s:12:\"File Manager\";i:1;s:10:\"edit_posts\";i:2;s:29:\"edit.php?post_type=wpfm-files\";i:3;s:0:\"\";i:4;s:29:\"menu-top menu-icon-wpfm-files\";i:5;s:21:\"menu-posts-wpfm-files\";i:6;s:90:\"https://picture.hungenbach.de/wp-content/plugins/nmedia-user-file-uploader/images/logo.png\";}s:8:\"26.06006\";a:7:{i:0;s:17:\"RegistrationMagic\";i:1;s:14:\"manage_options\";i:2;s:14:\"rm_form_manage\";i:3;s:17:\"RegistrationMagic\";i:4;s:37:\"menu-top toplevel_page_rm_form_manage\";i:5;s:28:\"toplevel_page_rm_form_manage\";i:6;s:139:\"https://picture.hungenbach.de/wp-content/plugins/custom-registration-form-builder-with-submission-manager/admin/../images/profile-icon2.png\";}i:27;a:7:{i:0;s:7:\"Contact\";i:1;s:24:\"wpcf7_read_contact_forms\";i:2;s:5:\"wpcf7\";i:3;s:14:\"Contact Form 7\";i:4;s:42:\"menu-top toplevel_page_wpcf7 menu-top-last\";i:5;s:19:\"toplevel_page_wpcf7\";i:6;s:15:\"dashicons-email\";}i:59;a:5:{i:0;s:0:\"\";i:1;s:4:\"read\";i:2;s:10:\"separator2\";i:3;s:0:\"\";i:4;s:17:\"wp-menu-separator\";}i:60;a:7:{i:0;s:10:\"Appearance\";i:1;s:13:\"switch_themes\";i:2;s:10:\"themes.php\";i:3;s:0:\"\";i:4;s:44:\"menu-top menu-icon-appearance menu-top-first\";i:5;s:15:\"menu-appearance\";i:6;s:26:\"dashicons-admin-appearance\";}i:65;a:7:{i:0;s:87:\"Plugins <span class=\'update-plugins count-0\'><span class=\'plugin-count\'>0</span></span>\";i:1;s:16:\"activate_plugins\";i:2;s:11:\"plugins.php\";i:3;s:0:\"\";i:4;s:26:\"menu-top menu-icon-plugins\";i:5;s:12:\"menu-plugins\";i:6;s:23:\"dashicons-admin-plugins\";}i:70;a:7:{i:0;s:5:\"Users\";i:1;s:10:\"list_users\";i:2;s:9:\"users.php\";i:3;s:0:\"\";i:4;s:24:\"menu-top menu-icon-users\";i:5;s:10:\"menu-users\";i:6;s:21:\"dashicons-admin-users\";}i:75;a:7:{i:0;s:5:\"Tools\";i:1;s:10:\"edit_posts\";i:2;s:9:\"tools.php\";i:3;s:0:\"\";i:4;s:24:\"menu-top menu-icon-tools\";i:5;s:10:\"menu-tools\";i:6;s:21:\"dashicons-admin-tools\";}i:80;a:7:{i:0;s:9:\"Settings \";i:1;s:14:\"manage_options\";i:2;s:19:\"options-general.php\";i:3;s:0:\"\";i:4;s:41:\"menu-top menu-icon-settings menu-top-last\";i:5;s:13:\"menu-settings\";i:6;s:24:\"dashicons-admin-settings\";}}s:29:\"mw_adminimize_default_submenu\";a:13:{s:9:\"index.php\";a:2:{i:0;a:3:{i:0;s:4:\"Home\";i:1;s:4:\"read\";i:2;s:9:\"index.php\";}i:10;a:3:{i:0;s:87:\"Updates <span class=\'update-plugins count-4\'><span class=\'update-count\'>4</span></span>\";i:1;s:11:\"update_core\";i:2;s:15:\"update-core.php\";}}s:10:\"upload.php\";a:2:{i:5;a:3:{i:0;s:7:\"Library\";i:1;s:12:\"upload_files\";i:2;s:10:\"upload.php\";}i:10;a:3:{i:0;s:7:\"Add New\";i:1;s:12:\"upload_files\";i:2;s:13:\"media-new.php\";}}s:8:\"edit.php\";a:4:{i:5;a:3:{i:0;s:9:\"All Posts\";i:1;s:10:\"edit_posts\";i:2;s:8:\"edit.php\";}i:10;a:3:{i:0;s:7:\"Add New\";i:1;s:10:\"edit_posts\";i:2;s:12:\"post-new.php\";}i:15;a:3:{i:0;s:10:\"Categories\";i:1;s:17:\"manage_categories\";i:2;s:31:\"edit-tags.php?taxonomy=category\";}i:16;a:3:{i:0;s:4:\"Tags\";i:1;s:16:\"manage_post_tags\";i:2;s:31:\"edit-tags.php?taxonomy=post_tag\";}}s:23:\"edit.php?post_type=page\";a:2:{i:5;a:3:{i:0;s:9:\"All Pages\";i:1;s:10:\"edit_pages\";i:2;s:23:\"edit.php?post_type=page\";}i:10;a:3:{i:0;s:7:\"Add New\";i:1;s:10:\"edit_pages\";i:2;s:27:\"post-new.php?post_type=page\";}}s:29:\"edit.php?post_type=wpfm-files\";a:3:{i:5;a:3:{i:0;s:9:\"All Files\";i:1;s:10:\"edit_posts\";i:2;s:29:\"edit.php?post_type=wpfm-files\";}i:10;a:3:{i:0;s:7:\"Add New\";i:1;s:10:\"edit_posts\";i:2;s:33:\"post-new.php?post_type=wpfm-files\";}i:11;a:4:{i:0;s:8:\"Settings\";i:1;s:14:\"manage_options\";i:2;s:13:\"wpfm-settings\";i:3;s:8:\"Settings\";}}s:10:\"themes.php\";a:6:{i:5;a:3:{i:0;s:6:\"Themes\";i:1;s:13:\"switch_themes\";i:2;s:10:\"themes.php\";}i:6;a:5:{i:0;s:9:\"Customize\";i:1;s:9:\"customize\";i:2;s:82:\"customize.php?return=%2Fwp-admin%2Foptions-general.php%3Fpage%3Dadminimize-options\";i:3;s:0:\"\";i:4;s:20:\"hide-if-no-customize\";}i:7;a:3:{i:0;s:7:\"Widgets\";i:1;s:18:\"edit_theme_options\";i:2;s:11:\"widgets.php\";}i:10;a:3:{i:0;s:5:\"Menus\";i:1;s:18:\"edit_theme_options\";i:2;s:13:\"nav-menus.php\";}i:11;a:4:{i:0;s:120:\"OnePress Theme <span class=\'update-plugins count-1\' title=\'1 action required\'><span class=\'update-count\'>1</span></span>\";i:1;s:18:\"edit_theme_options\";i:2;s:11:\"ft_onepress\";i:3;s:18:\"OnePress Dashboard\";}i:12;a:4:{i:0;s:6:\"Editor\";i:1;s:11:\"edit_themes\";i:2;s:16:\"theme-editor.php\";i:3;s:6:\"Editor\";}}s:11:\"plugins.php\";a:3:{i:5;a:3:{i:0;s:17:\"Installed Plugins\";i:1;s:16:\"activate_plugins\";i:2;s:11:\"plugins.php\";}i:10;a:3:{i:0;s:7:\"Add New\";i:1;s:15:\"install_plugins\";i:2;s:18:\"plugin-install.php\";}i:15;a:3:{i:0;s:6:\"Editor\";i:1;s:12:\"edit_plugins\";i:2;s:17:\"plugin-editor.php\";}}s:9:\"users.php\";a:3:{i:5;a:3:{i:0;s:9:\"All Users\";i:1;s:10:\"list_users\";i:2;s:9:\"users.php\";}i:10;a:3:{i:0;s:7:\"Add New\";i:1;s:12:\"create_users\";i:2;s:12:\"user-new.php\";}i:15;a:3:{i:0;s:12:\"Your Profile\";i:1;s:4:\"read\";i:2;s:11:\"profile.php\";}}s:9:\"tools.php\";a:5:{i:5;a:3:{i:0;s:15:\"Available Tools\";i:1;s:10:\"edit_posts\";i:2;s:9:\"tools.php\";}i:10;a:3:{i:0;s:6:\"Import\";i:1;s:6:\"import\";i:2;s:10:\"import.php\";}i:15;a:3:{i:0;s:6:\"Export\";i:1;s:6:\"export\";i:2;s:10:\"export.php\";}i:16;a:4:{i:0;s:20:\"Export Personal Data\";i:1;s:27:\"export_others_personal_data\";i:2;s:20:\"export_personal_data\";i:3;s:20:\"Export Personal Data\";}i:17;a:4:{i:0;s:19:\"Erase Personal Data\";i:1;s:26:\"erase_others_personal_data\";i:2;s:20:\"remove_personal_data\";i:3;s:19:\"Erase Personal Data\";}}s:19:\"options-general.php\";a:9:{i:10;a:3:{i:0;s:7:\"General\";i:1;s:14:\"manage_options\";i:2;s:19:\"options-general.php\";}i:15;a:3:{i:0;s:7:\"Writing\";i:1;s:14:\"manage_options\";i:2;s:19:\"options-writing.php\";}i:20;a:3:{i:0;s:7:\"Reading\";i:1;s:14:\"manage_options\";i:2;s:19:\"options-reading.php\";}i:25;a:3:{i:0;s:10:\"Discussion\";i:1;s:14:\"manage_options\";i:2;s:22:\"options-discussion.php\";}i:30;a:3:{i:0;s:5:\"Media\";i:1;s:14:\"manage_options\";i:2;s:17:\"options-media.php\";}i:40;a:3:{i:0;s:10:\"Permalinks\";i:1;s:14:\"manage_options\";i:2;s:21:\"options-permalink.php\";}i:45;a:3:{i:0;s:8:\"Privacy \";i:1;s:22:\"manage_privacy_options\";i:2;s:11:\"privacy.php\";}i:46;a:4:{i:0;s:10:\"Adminimize\";i:1;s:14:\"manage_options\";i:2;s:18:\"adminimize-options\";i:3;s:18:\"Adminimize Options\";}i:47;a:4:{i:0;s:13:\"Cookie Notice\";i:1;s:14:\"manage_options\";i:2;s:13:\"cookie-notice\";i:3;s:13:\"Cookie Notice\";}}s:5:\"wpcf7\";a:3:{i:0;a:4:{i:0;s:13:\"Contact Forms\";i:1;s:24:\"wpcf7_read_contact_forms\";i:2;s:5:\"wpcf7\";i:3;s:17:\"Edit Contact Form\";}i:1;a:4:{i:0;s:7:\"Add New\";i:1;s:24:\"wpcf7_edit_contact_forms\";i:2;s:9:\"wpcf7-new\";i:3;s:20:\"Add New Contact Form\";}i:2;a:4:{i:0;s:11:\"Integration\";i:1;s:24:\"wpcf7_manage_integration\";i:2;s:17:\"wpcf7-integration\";i:3;s:31:\"Integration with Other Services\";}}s:0:\"\";a:54:{i:0;a:4:{i:0;s:8:\"New Form\";i:1;s:14:\"manage_options\";i:2;s:20:\"rm_form_sett_general\";i:3;s:8:\"New Form\";}i:1;a:4:{i:0;s:18:\"Manage Form Fields\";i:1;s:14:\"manage_options\";i:2;s:15:\"rm_field_manage\";i:3;s:18:\"Manage Form Fields\";}i:2;a:4:{i:0;s:9:\"Add Field\";i:1;s:14:\"manage_options\";i:2;s:12:\"rm_field_add\";i:3;s:9:\"Add Field\";}i:3;a:4:{i:0;s:12:\"RM Chronos 3\";i:1;s:14:\"manage_options\";i:2;s:23:\"rm_ex_chronos_edit_task\";i:3;s:12:\"RM Chronos 3\";}i:4;a:4:{i:0;s:16:\"Add PayPal Field\";i:1;s:14:\"manage_options\";i:2;s:19:\"rm_paypal_field_add\";i:3;s:16:\"Add PayPal Field\";}i:5;a:4:{i:0;s:0:\"\";i:1;s:14:\"manage_options\";i:2;s:14:\"rm_paypal_proc\";i:3;s:17:\"PayPal processing\";}i:6;a:4:{i:0;s:19:\"Attachment Download\";i:1;s:14:\"manage_options\";i:2;s:22:\"rm_attachment_download\";i:3;s:19:\"Attachment Download\";}i:7;a:4:{i:0;s:15:\"View Submission\";i:1;s:14:\"manage_options\";i:2;s:18:\"rm_submission_view\";i:3;s:15:\"View Submission\";}i:8;a:4:{i:0;s:15:\"NO STRING FOUND\";i:1;s:14:\"manage_options\";i:2;s:21:\"rm_submission_related\";i:3;s:15:\"NO STRING FOUND\";}i:9;a:4:{i:0;s:11:\"Sent Emails\";i:1;s:14:\"manage_options\";i:2;s:21:\"rm_sent_emails_manage\";i:3;s:11:\"Sent Emails\";}i:10;a:4:{i:0;s:11:\"Sent Emails\";i:1;s:14:\"manage_options\";i:2;s:19:\"rm_sent_emails_view\";i:3;s:11:\"Sent Emails\";}i:11;a:4:{i:0;s:16:\"General Settings\";i:1;s:14:\"manage_options\";i:2;s:18:\"rm_options_general\";i:3;s:16:\"General Settings\";}i:12;a:4:{i:0;s:26:\"Magic Popup Button Setting\";i:1;s:14:\"manage_options\";i:2;s:14:\"rm_options_fab\";i:3;s:26:\"Magic Popup Button Setting\";}i:13;a:4:{i:0;s:16:\"General Settings\";i:1;s:14:\"manage_options\";i:2;s:19:\"rm_options_security\";i:3;s:18:\"Anti Spam Settings\";}i:14;a:4:{i:0;s:21:\"User Account Settings\";i:1;s:14:\"manage_options\";i:2;s:15:\"rm_options_user\";i:3;s:21:\"User Account Settings\";}i:15;a:4:{i:0;s:32:\"Third Party Integration Settings\";i:1;s:14:\"manage_options\";i:2;s:24:\"rm_options_autoresponder\";i:3;s:23:\"Auto Responder Settings\";}i:16;a:4:{i:0;s:32:\"Third Party Integration Settings\";i:1;s:14:\"manage_options\";i:2;s:21:\"rm_options_thirdparty\";i:3;s:32:\"Third Party Integration Settings\";}i:17;a:4:{i:0;s:16:\"Payment Settings\";i:1;s:14:\"manage_options\";i:2;s:18:\"rm_options_payment\";i:3;s:16:\"Payment Settings\";}i:18;a:4:{i:0;s:16:\"Payment Settings\";i:1;s:14:\"manage_options\";i:2;s:24:\"rm_options_default_pages\";i:3;s:16:\"Payment Settings\";}i:19;a:4:{i:0;s:12:\"User Privacy\";i:1;s:14:\"manage_options\";i:2;s:23:\"rm_options_user_privacy\";i:3;s:12:\"User Privacy\";}i:20;a:4:{i:0;s:13:\"Save Settings\";i:1;s:14:\"manage_options\";i:2;s:15:\"rm_options_save\";i:3;s:13:\"Save Settings\";}i:21;a:4:{i:0;s:8:\"Add Note\";i:1;s:14:\"manage_options\";i:2;s:11:\"rm_note_add\";i:3;s:8:\"Add Note\";}i:22;a:4:{i:0;s:15:\"NO STRING FOUND\";i:1;s:14:\"manage_options\";i:2;s:14:\"rm_form_import\";i:3;s:15:\"NO STRING FOUND\";}i:23;a:4:{i:0;s:16:\"User Role Delete\";i:1;s:14:\"manage_options\";i:2;s:19:\"rm_user_role_delete\";i:3;s:16:\"User Role Delete\";}i:24;a:4:{i:0;s:10:\"Registrant\";i:1;s:14:\"manage_options\";i:2;s:12:\"rm_user_view\";i:3;s:10:\"Registrant\";}i:25;a:4:{i:0;s:15:\"NO STRING FOUND\";i:1;s:14:\"manage_options\";i:2;s:21:\"rm_form_sett_ccontact\";i:3;s:15:\"NO STRING FOUND\";}i:26;a:4:{i:0;s:15:\"NO STRING FOUND\";i:1;s:14:\"manage_options\";i:2;s:19:\"rm_form_sett_aweber\";i:3;s:15:\"NO STRING FOUND\";}i:27;a:4:{i:0;s:15:\"NO STRING FOUND\";i:1;s:14:\"manage_options\";i:2;s:21:\"rm_form_sett_override\";i:3;s:15:\"NO STRING FOUND\";}i:28;a:4:{i:0;s:15:\"NO STRING FOUND\";i:1;s:14:\"manage_options\";i:2;s:26:\"rm_form_sett_autoresponder\";i:3;s:15:\"NO STRING FOUND\";}i:29;a:4:{i:0;s:15:\"Email Templates\";i:1;s:14:\"manage_options\";i:2;s:28:\"rm_form_sett_email_templates\";i:3;s:15:\"Email Templates\";}i:30;a:4:{i:0;s:15:\"NO STRING FOUND\";i:1;s:14:\"manage_options\";i:2;s:19:\"rm_form_sett_limits\";i:3;s:15:\"NO STRING FOUND\";}i:31;a:4:{i:0;s:15:\"NO STRING FOUND\";i:1;s:14:\"manage_options\";i:2;s:21:\"rm_form_sett_post_sub\";i:3;s:15:\"NO STRING FOUND\";}i:32;a:4:{i:0;s:15:\"NO STRING FOUND\";i:1;s:14:\"manage_options\";i:2;s:21:\"rm_form_sett_accounts\";i:3;s:15:\"NO STRING FOUND\";}i:33;a:4:{i:0;s:15:\"NO STRING FOUND\";i:1;s:14:\"manage_options\";i:2;s:17:\"rm_form_sett_view\";i:3;s:15:\"NO STRING FOUND\";}i:34;a:4:{i:0;s:15:\"NO STRING FOUND\";i:1;s:14:\"manage_options\";i:2;s:22:\"rm_form_sett_mailchimp\";i:3;s:15:\"NO STRING FOUND\";}i:35;a:4:{i:0;s:15:\"NO STRING FOUND\";i:1;s:14:\"manage_options\";i:2;s:19:\"rm_form_sett_manage\";i:3;s:15:\"NO STRING FOUND\";}i:36;a:4:{i:0;s:15:\"NO STRING FOUND\";i:1;s:14:\"manage_options\";i:2;s:27:\"rm_form_sett_access_control\";i:3;s:15:\"NO STRING FOUND\";}i:37;a:4:{i:0;s:9:\"Add Field\";i:1;s:14:\"manage_options\";i:2;s:19:\"rm_field_add_widget\";i:3;s:9:\"Add Field\";}i:38;a:4:{i:0;s:15:\"Advance Options\";i:1;s:14:\"manage_options\";i:2;s:18:\"rm_options_advance\";i:3;s:15:\"Advance Options\";}i:39;a:4:{i:0;s:12:\"Login Fields\";i:1;s:14:\"manage_options\";i:2;s:21:\"rm_login_field_manage\";i:3;s:12:\"Login Fields\";}i:40;a:4:{i:0;s:12:\"Login Fields\";i:1;s:14:\"manage_options\";i:2;s:18:\"rm_login_field_add\";i:3;s:12:\"Login Fields\";}i:41;a:4:{i:0;s:12:\"Login Fields\";i:1;s:14:\"manage_options\";i:2;s:24:\"rm_login_field_view_sett\";i:3;s:12:\"Login Fields\";}i:42;a:4:{i:0;s:14:\"Logged in View\";i:1;s:14:\"manage_options\";i:2;s:13:\"rm_login_view\";i:3;s:14:\"Logged in View\";}i:43;a:4:{i:0;s:15:\"Login Dashboard\";i:1;s:14:\"manage_options\";i:2;s:20:\"rm_login_sett_manage\";i:3;s:15:\"Login Dashboard\";}i:44;a:4:{i:0;s:18:\"Login Redirections\";i:1;s:14:\"manage_options\";i:2;s:26:\"rm_login_sett_redirections\";i:3;s:18:\"Login Redirections\";}i:45;a:4:{i:0;s:27:\"Login Validation & Security\";i:1;s:14:\"manage_options\";i:2;s:16:\"rm_login_val_sec\";i:3;s:27:\"Login Validation & Security\";}i:46;a:4:{i:0;s:17:\"Password Recovery\";i:1;s:14:\"manage_options\";i:2;s:17:\"rm_login_recovery\";i:3;s:17:\"Password Recovery\";}i:47;a:4:{i:0;s:15:\"Email Templates\";i:1;s:14:\"manage_options\";i:2;s:19:\"rm_login_email_temp\";i:3;s:15:\"Email Templates\";}i:48;a:4:{i:0;s:25:\"Two Factor Authentication\";i:1;s:14:\"manage_options\";i:2;s:24:\"rm_login_two_factor_auth\";i:3;s:25:\"Two Factor Authentication\";}i:49;a:4:{i:0;s:23:\"Third Part Integrations\";i:1;s:14:\"manage_options\";i:2;s:21:\"rm_login_integrations\";i:3;s:23:\"Third Part Integrations\";}i:50;a:4:{i:0;s:15:\"Login Analytics\";i:1;s:14:\"manage_options\";i:2;s:18:\"rm_login_analytics\";i:3;s:15:\"Login Analytics\";}i:51;a:4:{i:0;s:13:\"Log Retention\";i:1;s:14:\"manage_options\";i:2;s:18:\"rm_login_retention\";i:3;s:13:\"Log Retention\";}i:52;a:4:{i:0;s:12:\"Advanced Log\";i:1;s:14:\"manage_options\";i:2;s:17:\"rm_login_advanced\";i:3;s:12:\"Advanced Log\";}i:53;a:4:{i:0;s:12:\"Landing Page\";i:1;s:14:\"manage_options\";i:2;s:15:\"rm_ex_lmsupport\";i:3;s:12:\"Landing Page\";}}s:14:\"rm_form_manage\";a:13:{i:0;a:4:{i:0;s:9:\"All Forms\";i:1;s:14:\"manage_options\";i:2;s:14:\"rm_form_manage\";i:3;s:17:\"RegistrationMagic\";}i:1;a:4:{i:0;s:5:\"Inbox\";i:1;s:14:\"manage_options\";i:2;s:20:\"rm_submission_manage\";i:3;s:5:\"Inbox\";}i:2;a:4:{i:0;s:11:\"Attachments\";i:1;s:14:\"manage_options\";i:2;s:20:\"rm_attachment_manage\";i:3;s:11:\"Attachments\";}i:3;a:4:{i:0;s:14:\"Form Analytics\";i:1;s:14:\"manage_options\";i:2;s:22:\"rm_analytics_show_form\";i:3;s:14:\"Form Analytics\";}i:4;a:4:{i:0;s:15:\"Field Analytics\";i:1;s:14:\"manage_options\";i:2;s:23:\"rm_analytics_show_field\";i:3;s:15:\"Field Analytics\";}i:5;a:4:{i:0;s:10:\"Automation\";i:1;s:14:\"manage_options\";i:2;s:26:\"rm_ex_chronos_manage_tasks\";i:3;s:10:\"Automation\";}i:6;a:4:{i:0;s:10:\"Bulk Email\";i:1;s:14:\"manage_options\";i:2;s:21:\"rm_invitations_manage\";i:3;s:10:\"Bulk Email\";}i:7;a:4:{i:0;s:12:\"User Manager\";i:1;s:14:\"manage_options\";i:2;s:14:\"rm_user_manage\";i:3;s:12:\"User Manager\";}i:8;a:4:{i:0;s:10:\"User Roles\";i:1;s:14:\"manage_options\";i:2;s:19:\"rm_user_role_manage\";i:3;s:10:\"User Roles\";}i:9;a:4:{i:0;s:8:\"Products\";i:1;s:14:\"manage_options\";i:2;s:22:\"rm_paypal_field_manage\";i:3;s:8:\"Products\";}i:10;a:4:{i:0;s:15:\"Global Settings\";i:1;s:14:\"manage_options\";i:2;s:17:\"rm_options_manage\";i:3;s:15:\"Global Settings\";}i:11;a:4:{i:0;s:7:\"Support\";i:1;s:14:\"manage_options\";i:2;s:16:\"rm_support_forum\";i:3;s:7:\"Support\";}i:12;a:4:{i:0;s:41:\"<div style=\'color:#ff6c6c;\'>Premium</div>\";i:1;s:14:\"manage_options\";i:2;s:23:\"rm_support_premium_page\";i:3;s:7:\"Premium\";}}}}', 'yes'),
(519, 'rm_option_theme', 'matchmytheme', 'no'),
(520, 'rm_option_allowed_file_types', '', 'no'),
(521, 'rm_option_default_registration_url', '', 'no'),
(522, 'rm_option_post_submission_redirection_url', '', 'no'),
(523, 'rm_option_post_logout_redirection_page_id', '', 'no'),
(524, 'rm_option_hide_toolbar', 'yes', 'no'),
(525, 'rm_option_user_ip', '', 'no'),
(526, 'rm_option_allow_multiple_file_uploads', '', 'no'),
(527, 'rm_option_form_layout', 'label_left', 'no'),
(528, 'rm_option_display_progress_bar', '', 'no'),
(529, 'rm_option_submission_on_card', 'all', 'no'),
(530, 'rm_option_show_asterix', 'yes', 'no'),
(531, 'rm_option_redirect_admin_to_dashboard_post_login', '', 'no'),
(532, 'rm_option_currency', 'EUR', 'no'),
(533, 'rm_option_currency_symbol', '$', 'no'),
(534, 'rm_option_session_policy', 'db', 'no'),
(535, 'rm_option_payment_gateway', 'a:1:{i:0;s:6:\"paypal\";}', 'no'),
(536, 'rm_option_stripe_api_key', '', 'no'),
(537, 'rm_option_an_senders_email', '{{useremail}}', 'no'),
(538, 'rm_option_stripe_publish_key', '', 'no'),
(539, 'rm_option_paypal_email', '', 'no'),
(540, 'rm_option_paypal_test_mode', '', 'no'),
(541, 'rm_option_an_senders_display_name', '{{user}}', 'no'),
(542, 'rm_option_paypal_page_style', '', 'no'),
(543, 'rm_option_currency_symbol_position', 'before', 'no'),
(544, 'rm_option_enable_captcha', '', 'no'),
(545, 'rm_option_sub_limit_antispam', '20', 'no'),
(546, 'rm_option_edd_notice', '1', 'no'),
(547, 'rm_option_wc_notice', '1', 'no'),
(548, 'rm_option_php_notice', '1', 'no'),
(549, 'rm_option_public_key', '', 'no'),
(550, 'rm_option_private_key', '', 'no'),
(551, 'rm_option_auto_generated_password', '', 'no'),
(552, 'rm_option_user_auto_approval', 'yes', 'no'),
(553, 'rm_option_admin_email', 'markus@hungenbach.de', 'no'),
(554, 'rm_option_admin_notification', '', 'no'),
(555, 'rm_option_senders_display_name', 'creative', 'no'),
(556, 'rm_option_senders_email', 'markus@hungenbach.de', 'no'),
(557, 'rm_option_enable_wordpress_default', '', 'no'),
(558, 'rm_option_wordpress_default_email_to', '', 'no'),
(559, 'rm_option_wordpress_default_email_message', '', 'no'),
(560, 'rm_option_user_notification_for_notes', 'yes', 'no'),
(561, 'rm_option_enable_smtp', '', 'no'),
(562, 'rm_option_smtp_host', '', 'no'),
(563, 'rm_option_smtp_encryption_type', 'enc_none', 'no'),
(564, 'rm_option_smtp_port', '', 'no'),
(565, 'rm_option_smtp_auth', '', 'no'),
(566, 'rm_option_smtp_user_name', '', 'no'),
(567, 'rm_option_smtp_password', 'UrujsvVfZCj0WAsr720BEotGPbJBvSJQ7NqTKd/SvM1RFgO8xN+tWIKsQWAEIrQaiaK3KRyZ6GeN7pmffsP06Q==', 'no'),
(568, 'rm_option_enable_social', '', 'no'),
(569, 'rm_option_facebook_app_id', '', 'no'),
(570, 'rm_option_facebook_app_secret', '', 'no'),
(571, 'rm_option_enable_facebook', '', 'no'),
(572, 'rm_option_enable_twitter', '', 'no'),
(573, 'rm_option_consumer_key', '', 'no'),
(574, 'rm_option_consumer_secret', '', 'no'),
(575, 'rm_option_enable_mailchimp', '', 'no'),
(576, 'rm_option_mailchimp_key', '', 'no'),
(577, 'rm_option_google_map_key', '', 'no'),
(578, 'rm_option_send_password', 'yes', 'no'),
(579, 'rm_option_done_with_review_banner', 'no', 'no'),
(580, 'rm_option_banned_ip', 'a:0:{}', 'no'),
(581, 'rm_option_banned_email', 'a:0:{}', 'no'),
(582, 'rm_option_enable_custom_pw_rests', '', 'no'),
(583, 'rm_option_custom_pw_rests', '', 'no'),
(584, 'rm_option_blacklisted_usernames', 'a:0:{}', 'no'),
(585, 'rm_option_default_form_id', '', 'no'),
(586, 'rm_option_display_floating_action_btn', '', 'no'),
(587, 'rm_option_floating_icon_bck_color', '008d7d', 'no'),
(588, 'rm_option_fab_color', '00aeff', 'no'),
(589, 'rm_option_fab_theme', 'Light', 'no'),
(590, 'rm_option_fab_icon', '', 'no'),
(591, 'rm_option_review_events', 'a:3:{s:5:\"event\";i:0;s:6:\"status\";a:2:{s:4:\"flag\";i:0;s:4:\"time\";s:0:\"\";}s:6:\"rating\";i:0;}', 'no'),
(592, 'rm_option_rm_option_default_forms', '', 'no'),
(593, 'rm_option_has_subbed_fb_page', 'no', 'no'),
(594, 'rm_option_one_time_actions', 'a:0:{}', 'no'),
(595, 'rm_option_is_visit_welcome_page', 'no', 'no'),
(769, 'category_children', 'a:0:{}', 'yes'),
(788, 'widget_better-image-credits-widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(791, 'better-image-credits-options', 'a:7:{s:7:\"display\";a:1:{i:0;s:5:\"after\";}s:8:\"template\";s:45:\"<a href=\"[link]\" target=\"_blank\">[source]</a>\";s:3:\"sep\";s:2:\", \";s:6:\"before\";s:40:\"<p class=\"image-credits\">Image Credits: \";s:5:\"after\";s:5:\".</p>\";s:13:\"overlay_color\";s:0:\"\";s:43:\"better-image-credits-options[overlay_color]\";s:0:\"\";}', 'yes'),
(884, 'WPGMZA_OTHER_SETTINGS', 'a:23:{s:27:\"wpgmza_settings_marker_pull\";s:1:\"0\";s:24:\"wpgmza_gdpr_company_name\";s:8:\"creative\";s:29:\"wpgmza_gdpr_retention_purpose\";s:81:\"displaying map tiles, geocoding addresses and calculating and display directions.\";s:32:\"wpgmza_gdpr_notice_override_text\";s:0:\"\";s:32:\"wpgmza_load_engine_api_condition\";s:14:\"where-required\";s:15:\"use_fontawesome\";s:3:\"4.*\";s:18:\"wpgmza_maps_engine\";s:11:\"google-maps\";s:34:\"wpgmza_settings_map_open_marker_by\";s:1:\"1\";s:17:\"wpgmza_custom_css\";s:0:\"\";s:16:\"wpgmza_custom_js\";s:128:\"jQuery(document.body).on(\\\"init.wpgmza\\\", function(event) {\r\nevent.target.googleMap.setTilt(70);\r\nconsole.log(\\\'trigger\\\');\r\n});\";s:28:\"wpgmza_settings_access_level\";s:14:\"manage_options\";s:26:\"wpgmza_store_locator_radii\";s:31:\"1,5,10,25,50,75,100,150,200,300\";s:26:\"wpgmza_google_maps_api_key\";s:39:\"AIzaSyAT_GS4qcRbkYp3kzlVFbYJ4e7axzDX-3s\";s:6:\"engine\";s:11:\"google-maps\";s:14:\"developer_mode\";s:2:\"on\";s:39:\"wpgmza_settings_map_full_screen_control\";s:3:\"yes\";s:30:\"wpgmza_settings_map_streetview\";s:3:\"yes\";s:24:\"wpgmza_settings_map_zoom\";s:3:\"yes\";s:23:\"wpgmza_settings_map_pan\";s:3:\"yes\";s:24:\"wpgmza_settings_map_type\";s:3:\"yes\";s:26:\"wpgmza_settings_map_scroll\";s:3:\"yes\";s:29:\"wpgmza_settings_map_draggable\";s:3:\"yes\";s:29:\"wpgmza_settings_map_clickzoom\";s:3:\"yes\";}', 'yes'),
(885, 'wpgmza_temp_api', 'AIzaSyChPphumyabdfggISDNBuGOlGVBgEvZnGE', 'yes'),
(886, 'wpgmza_xml_location', '{uploads_dir}/wp-google-maps/', 'yes'),
(887, 'wpgmza_xml_url', '{uploads_url}/wp-google-maps/', 'yes'),
(888, 'wpgmza_db_version', '7.11.32', 'yes'),
(889, 'wpgmaps_current_version', '7.11.32', 'yes'),
(890, 'WPGM_V6_FIRST_TIME', '1', 'yes'),
(891, 'widget_wpgmza_map_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(892, 'WPGMZA_FIRST_TIME', '7.11.14', 'yes'),
(893, 'wpgmza_stats', 'a:4:{s:14:\"settings_basic\";a:3:{s:5:\"views\";i:76;s:13:\"last_accessed\";s:19:\"2019-03-16 03:46:38\";s:14:\"first_accessed\";s:19:\"2019-01-27 11:09:43\";}s:15:\"list_maps_basic\";a:3:{s:5:\"views\";i:20;s:13:\"last_accessed\";s:19:\"2019-03-16 03:44:02\";s:14:\"first_accessed\";s:19:\"2019-01-27 11:14:02\";}s:9:\"dashboard\";a:3:{s:5:\"views\";i:37;s:13:\"last_accessed\";s:19:\"2019-03-16 03:46:13\";s:14:\"first_accessed\";s:19:\"2019-01-27 11:14:09\";}s:13:\"support_basic\";a:3:{s:5:\"views\";i:1;s:13:\"last_accessed\";s:19:\"2019-03-12 04:50:46\";s:14:\"first_accessed\";s:19:\"2019-03-12 04:50:46\";}}', 'yes'),
(894, 'wpgmza_google_maps_api_key', 'AIzaSyAT_GS4qcRbkYp3kzlVFbYJ4e7axzDX-3s', 'yes'),
(895, 'WPGMZA_SETTINGS', 'a:10:{s:24:\"map_default_starting_lat\";s:8:\"52.51437\";s:24:\"map_default_starting_lng\";s:18:\"13.350329999999985\";s:18:\"map_default_height\";s:3:\"800\";s:17:\"map_default_width\";s:3:\"100\";s:16:\"map_default_zoom\";i:18;s:20:\"map_default_max_zoom\";i:1;s:16:\"map_default_type\";i:2;s:21:\"map_default_alignment\";i:1;s:22:\"map_default_width_type\";s:2:\"\\%\";s:23:\"map_default_height_type\";s:2:\"px\";}', 'yes'),
(993, 'wpgmza_global_settings', '{\"wpgmza_settings_marker_pull\":\"0\",\"wpgmza_gdpr_company_name\":\"creative\",\"wpgmza_gdpr_retention_purpose\":\"displaying map tiles, geocoding addresses and calculating and display directions.\",\"wpgmza_gdpr_notice_override_text\":\"\",\"wpgmza_load_engine_api_condition\":\"where-required\",\"use_fontawesome\":\"4.*\",\"wpgmza_maps_engine\":\"google-maps\",\"wpgmza_settings_map_open_marker_by\":\"1\",\"wpgmza_custom_css\":\"\",\"wpgmza_custom_js\":\"jQuery(document.body).on(\\\\\\\"init.wpgmza\\\\\\\", function(event) {\\r\\nevent.target.googleMap.setTilt(70);\\r\\nconsole.log(\\\\\'trigger\\\\\');\\r\\n});\",\"wpgmza_settings_access_level\":\"manage_options\",\"wpgmza_store_locator_radii\":\"1,5,10,25,50,75,100,150,200,300\",\"wpgmza_google_maps_api_key\":\"AIzaSyAT_GS4qcRbkYp3kzlVFbYJ4e7axzDX-3s\",\"engine\":\"google-maps\",\"developer_mode\":\"on\",\"wpgmza_settings_map_full_screen_control\":\"yes\",\"wpgmza_settings_map_streetview\":\"yes\",\"wpgmza_settings_map_zoom\":\"yes\",\"wpgmza_settings_map_pan\":\"yes\",\"wpgmza_settings_map_type\":\"yes\",\"wpgmza_settings_map_scroll\":\"yes\",\"wpgmza_settings_map_draggable\":\"yes\",\"wpgmza_settings_map_clickzoom\":\"yes\"}', 'yes'),
(1176, 'db_upgraded', '', 'yes'),
(1181, 'can_compress_scripts', '1', 'no'),
(3726, 'recovery_keys', 'a:0:{}', 'yes'),
(4964, 'active_plugins', 'a:9:{i:0;s:45:\"better-image-credits/better-image-credits.php\";i:1;s:36:\"contact-form-7/wp-contact-form-7.php\";i:2;s:31:\"cookie-notice/cookie-notice.php\";i:3;s:79:\"custom-registration-form-builder-with-submission-manager/registration_magic.php\";i:4;s:43:\"custom_image_upload/custom_image_upload.php\";i:5;s:20:\"wp-colorbox/main.php\";i:6;s:39:\"wp-file-manager/file_folder_manager.php\";i:7;s:29:\"wp-mail-smtp/wp_mail_smtp.php\";i:8;s:15:\"wpide/WPide.php\";}', 'yes'),
(4965, 'ai1wm_secret_key', 'GgM0KLNHYU5W', 'yes'),
(4966, 'ai1wm_backups_labels', 'a:0:{}', 'yes'),
(4967, 'ai1wmte_plugin_key', '1', 'yes'),
(4968, 'template', 'onepress', 'yes'),
(4969, 'stylesheet', 'onepress-child', 'yes'),
(4970, 'jetpack_active_modules', 'a:0:{}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(4971, 'ai1wm_status', 'a:3:{s:4:\"type\";s:4:\"done\";s:5:\"title\";s:41:\"Your site has been imported successfully!\";s:7:\"message\";s:406:\"» <a class=\"ai1wm-no-underline\" href=\"http://localhost/picture_hungenbach/wp-admin/options-permalink.php#submit\" target=\"_blank\">Save permalinks structure</a>.</strong> (opens a new window)<br />» <a class=\"ai1wm-no-underline\" href=\"https://wordpress.org/support/view/plugin-reviews/all-in-one-wp-migration?rate=5#postform\" target=\"_blank\">Optionally, review the plugin</a>.</strong> (opens a new window)\";}', 'yes'),
(4994, 'ai1wm_updater', 'a:1:{s:38:\"all-in-one-wp-migration-file-extension\";a:13:{s:4:\"name\";s:14:\"File Extension\";s:4:\"slug\";s:14:\"file-extension\";s:8:\"homepage\";s:31:\"https://import.wp-migration.com\";s:13:\"download_link\";s:74:\"https://import.wp-migration.com/all-in-one-wp-migration-file-extension.zip\";s:7:\"version\";s:3:\"1.4\";s:6:\"author\";s:8:\"ServMask\";s:15:\"author_homepage\";s:20:\"https://servmask.com\";s:8:\"sections\";a:1:{s:11:\"description\";s:60:\"<ul class=\"description\"><li>Import from file</li></ul><br />\";}s:7:\"banners\";a:2:{s:3:\"low\";s:71:\"https://import.wp-migration.com/img/products/file-extension-772x250.png\";s:4:\"high\";s:72:\"https://import.wp-migration.com/img/products/file-extension-1544x500.png\";}s:5:\"icons\";a:3:{s:2:\"1x\";s:71:\"https://import.wp-migration.com/img/products/file-extension-128x128.png\";s:2:\"2x\";s:71:\"https://import.wp-migration.com/img/products/file-extension-256x256.png\";s:7:\"default\";s:71:\"https://import.wp-migration.com/img/products/file-extension-256x256.png\";}s:6:\"rating\";i:99;s:11:\"num_ratings\";i:309;s:10:\"downloaded\";i:40188;}}', 'yes'),
(9689, '_site_transient_timeout_theme_roots', '1573190760', 'no'),
(9690, '_site_transient_theme_roots', 'a:3:{s:14:\"onepress-child\";s:7:\"/themes\";s:8:\"onepress\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";}', 'no'),
(9691, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1573188963;s:7:\"checked\";a:3:{s:14:\"onepress-child\";s:5:\"1.0.0\";s:8:\"onepress\";s:5:\"2.2.4\";s:15:\"twentyseventeen\";s:3:\"1.9\";}s:8:\"response\";a:1:{s:15:\"twentyseventeen\";a:6:{s:5:\"theme\";s:15:\"twentyseventeen\";s:11:\"new_version\";s:3:\"2.2\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentyseventeen/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentyseventeen.2.2.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:0:{}}', 'no'),
(9692, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1573188964;s:7:\"checked\";a:21:{s:25:\"adminimize/adminimize.php\";s:6:\"1.11.4\";s:19:\"akismet/akismet.php\";s:5:\"4.1.2\";s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";s:4:\"6.97\";s:81:\"all-in-one-wp-migration-file-extension/all-in-one-wp-migration-file-extension.php\";s:3:\"1.4\";s:43:\"all-in-one-seo-pack/all_in_one_seo_pack.php\";s:5:\"3.1.1\";s:28:\"amazon-polly/amazonpolly.php\";s:5:\"3.0.6\";s:45:\"better-image-credits/better-image-credits.php\";s:5:\"2.0.3\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.1.3\";s:31:\"cookie-notice/cookie-notice.php\";s:6:\"1.2.46\";s:43:\"custom_image_upload/custom_image_upload.php\";s:3:\"1.0\";s:50:\"google-analytics-for-wordpress/googleanalytics.php\";s:5:\"7.6.0\";s:21:\"hello-dolly/hello.php\";s:5:\"1.7.2\";s:19:\"jetpack/jetpack.php\";s:5:\"7.4.1\";s:79:\"custom-registration-form-builder-with-submission-manager/registration_magic.php\";s:7:\"4.5.7.2\";s:27:\"simple-tags/simple-tags.php\";s:5:\"2.5.4\";s:20:\"wp-colorbox/main.php\";s:5:\"1.1.2\";s:39:\"wp-file-manager/file_folder_manager.php\";s:3:\"5.3\";s:45:\"nmedia-user-file-uploader/wp-file-manager.php\";s:4:\"14.6\";s:31:\"wp-google-maps/wpGoogleMaps.php\";s:7:\"7.11.32\";s:15:\"wpide/WPide.php\";s:5:\"2.4.0\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:5:\"1.5.2\";}s:8:\"response\";a:14:{s:25:\"adminimize/adminimize.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:24:\"w.org/plugins/adminimize\";s:4:\"slug\";s:10:\"adminimize\";s:6:\"plugin\";s:25:\"adminimize/adminimize.php\";s:11:\"new_version\";s:6:\"1.11.5\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/adminimize/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/adminimize.1.11.5.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:61:\"https://s.w.org/plugins/geopattern-icon/adminimize_000000.svg\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/adminimize/assets/banner-772x250.png?rev=1118207\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.4\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.3\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.4\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:37:\"w.org/plugins/all-in-one-wp-migration\";s:4:\"slug\";s:23:\"all-in-one-wp-migration\";s:6:\"plugin\";s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";s:11:\"new_version\";s:3:\"7.9\";s:3:\"url\";s:54:\"https://wordpress.org/plugins/all-in-one-wp-migration/\";s:7:\"package\";s:70:\"https://downloads.wordpress.org/plugin/all-in-one-wp-migration.7.9.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/all-in-one-wp-migration/assets/icon-256x256.png?rev=2187156\";s:2:\"1x\";s:76:\"https://ps.w.org/all-in-one-wp-migration/assets/icon-128x128.png?rev=2187156\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:79:\"https://ps.w.org/all-in-one-wp-migration/assets/banner-1544x500.png?rev=2187156\";s:2:\"1x\";s:78:\"https://ps.w.org/all-in-one-wp-migration/assets/banner-772x250.png?rev=2187156\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.3\";s:12:\"requires_php\";s:6:\"5.2.17\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:43:\"all-in-one-seo-pack/all_in_one_seo_pack.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:33:\"w.org/plugins/all-in-one-seo-pack\";s:4:\"slug\";s:19:\"all-in-one-seo-pack\";s:6:\"plugin\";s:43:\"all-in-one-seo-pack/all_in_one_seo_pack.php\";s:11:\"new_version\";s:6:\"3.2.10\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/all-in-one-seo-pack/\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/plugin/all-in-one-seo-pack.3.2.10.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/all-in-one-seo-pack/assets/icon-256x256.png?rev=2075006\";s:2:\"1x\";s:72:\"https://ps.w.org/all-in-one-seo-pack/assets/icon-128x128.png?rev=2075006\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/all-in-one-seo-pack/assets/banner-1544x500.png?rev=1354894\";s:2:\"1x\";s:74:\"https://ps.w.org/all-in-one-seo-pack/assets/banner-772x250.png?rev=1354894\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.4\";s:12:\"requires_php\";s:5:\"5.2.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:28:\"amazon-polly/amazonpolly.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:26:\"w.org/plugins/amazon-polly\";s:4:\"slug\";s:12:\"amazon-polly\";s:6:\"plugin\";s:28:\"amazon-polly/amazonpolly.php\";s:11:\"new_version\";s:5:\"4.0.0\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/amazon-polly/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/amazon-polly.4.0.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/amazon-polly/assets/icon-256x256.png?rev=2183954\";s:2:\"1x\";s:65:\"https://ps.w.org/amazon-polly/assets/icon-128x128.png?rev=2183954\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:67:\"https://ps.w.org/amazon-polly/assets/banner-772x250.png?rev=2183954\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.4\";s:12:\"requires_php\";s:3:\"5.6\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.4\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.4\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:50:\"google-analytics-for-wordpress/googleanalytics.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:44:\"w.org/plugins/google-analytics-for-wordpress\";s:4:\"slug\";s:30:\"google-analytics-for-wordpress\";s:6:\"plugin\";s:50:\"google-analytics-for-wordpress/googleanalytics.php\";s:11:\"new_version\";s:6:\"7.10.0\";s:3:\"url\";s:61:\"https://wordpress.org/plugins/google-analytics-for-wordpress/\";s:7:\"package\";s:80:\"https://downloads.wordpress.org/plugin/google-analytics-for-wordpress.7.10.0.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:83:\"https://ps.w.org/google-analytics-for-wordpress/assets/icon-256x256.png?rev=1598927\";s:2:\"1x\";s:75:\"https://ps.w.org/google-analytics-for-wordpress/assets/icon.svg?rev=1598927\";s:3:\"svg\";s:75:\"https://ps.w.org/google-analytics-for-wordpress/assets/icon.svg?rev=1598927\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:86:\"https://ps.w.org/google-analytics-for-wordpress/assets/banner-1544x500.png?rev=2159532\";s:2:\"1x\";s:85:\"https://ps.w.org/google-analytics-for-wordpress/assets/banner-772x250.png?rev=2159532\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.3\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:19:\"jetpack/jetpack.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/jetpack\";s:4:\"slug\";s:7:\"jetpack\";s:6:\"plugin\";s:19:\"jetpack/jetpack.php\";s:11:\"new_version\";s:3:\"7.9\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/jetpack/\";s:7:\"package\";s:54:\"https://downloads.wordpress.org/plugin/jetpack.7.9.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:60:\"https://ps.w.org/jetpack/assets/icon-256x256.png?rev=1791404\";s:2:\"1x\";s:52:\"https://ps.w.org/jetpack/assets/icon.svg?rev=1791404\";s:3:\"svg\";s:52:\"https://ps.w.org/jetpack/assets/icon.svg?rev=1791404\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/jetpack/assets/banner-1544x500.png?rev=1791404\";s:2:\"1x\";s:62:\"https://ps.w.org/jetpack/assets/banner-772x250.png?rev=1791404\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.3\";s:12:\"requires_php\";s:3:\"5.6\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:79:\"custom-registration-form-builder-with-submission-manager/registration_magic.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:70:\"w.org/plugins/custom-registration-form-builder-with-submission-manager\";s:4:\"slug\";s:56:\"custom-registration-form-builder-with-submission-manager\";s:6:\"plugin\";s:79:\"custom-registration-form-builder-with-submission-manager/registration_magic.php\";s:11:\"new_version\";s:7:\"4.5.9.0\";s:3:\"url\";s:87:\"https://wordpress.org/plugins/custom-registration-form-builder-with-submission-manager/\";s:7:\"package\";s:107:\"https://downloads.wordpress.org/plugin/custom-registration-form-builder-with-submission-manager.4.5.9.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:109:\"https://ps.w.org/custom-registration-form-builder-with-submission-manager/assets/icon-256x256.png?rev=2049638\";s:2:\"1x\";s:109:\"https://ps.w.org/custom-registration-form-builder-with-submission-manager/assets/icon-128x128.png?rev=2049638\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:111:\"https://ps.w.org/custom-registration-form-builder-with-submission-manager/assets/banner-772x250.jpg?rev=2081490\";}s:11:\"banners_rtl\";a:0:{}s:14:\"upgrade_notice\";s:324:\"<ul>\n<li>Admin toolbar visibility fixes.</li>\n<li>Kosovo Added in country dropdown Menu.</li>\n<li>Translation related fixes.</li>\n<li>Edit Submission button label fixes.</li>\n<li>Custom Hook added to modify file name  and path uploaded in Dropbox. (Premium Only)</li>\n<li>PDF fixes in User Manager (Premium only).</li>\n</ul>\";s:6:\"tested\";s:5:\"5.2.4\";s:12:\"requires_php\";s:3:\"5.6\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:27:\"simple-tags/simple-tags.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/simple-tags\";s:4:\"slug\";s:11:\"simple-tags\";s:6:\"plugin\";s:27:\"simple-tags/simple-tags.php\";s:11:\"new_version\";s:5:\"2.5.5\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/simple-tags/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/simple-tags.2.5.5.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:64:\"https://ps.w.org/simple-tags/assets/icon-256x256.png?rev=2114309\";s:2:\"1x\";s:56:\"https://ps.w.org/simple-tags/assets/icon.svg?rev=2114309\";s:3:\"svg\";s:56:\"https://ps.w.org/simple-tags/assets/icon.svg?rev=2114309\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/simple-tags/assets/banner-1544x500.png?rev=2114309\";s:2:\"1x\";s:66:\"https://ps.w.org/simple-tags/assets/banner-772x250.png?rev=2114309\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/simple-tags/assets/banner-1544x500-rtl.png?rev=2114309\";s:2:\"1x\";s:70:\"https://ps.w.org/simple-tags/assets/banner-772x250-rtl.png?rev=2114309\";}s:6:\"tested\";s:5:\"5.2.4\";s:12:\"requires_php\";s:3:\"5.6\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:39:\"wp-file-manager/file_folder_manager.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:29:\"w.org/plugins/wp-file-manager\";s:4:\"slug\";s:15:\"wp-file-manager\";s:6:\"plugin\";s:39:\"wp-file-manager/file_folder_manager.php\";s:11:\"new_version\";s:3:\"5.4\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/wp-file-manager/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/wp-file-manager.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/wp-file-manager/assets/icon-128x128.png?rev=1846029\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:70:\"https://ps.w.org/wp-file-manager/assets/banner-772x250.jpg?rev=1846030\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.3\";s:12:\"requires_php\";s:5:\"5.2.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:45:\"nmedia-user-file-uploader/wp-file-manager.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:39:\"w.org/plugins/nmedia-user-file-uploader\";s:4:\"slug\";s:25:\"nmedia-user-file-uploader\";s:6:\"plugin\";s:45:\"nmedia-user-file-uploader/wp-file-manager.php\";s:11:\"new_version\";s:4:\"15.2\";s:3:\"url\";s:56:\"https://wordpress.org/plugins/nmedia-user-file-uploader/\";s:7:\"package\";s:73:\"https://downloads.wordpress.org/plugin/nmedia-user-file-uploader.15.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/nmedia-user-file-uploader/assets/icon-256x256.jpg?rev=2095835\";s:2:\"1x\";s:78:\"https://ps.w.org/nmedia-user-file-uploader/assets/icon-128x128.jpg?rev=2095835\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:80:\"https://ps.w.org/nmedia-user-file-uploader/assets/banner-772x250.jpg?rev=2095835\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.4\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:31:\"wp-google-maps/wpGoogleMaps.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/wp-google-maps\";s:4:\"slug\";s:14:\"wp-google-maps\";s:6:\"plugin\";s:31:\"wp-google-maps/wpGoogleMaps.php\";s:11:\"new_version\";s:5:\"8.0.8\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/wp-google-maps/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/wp-google-maps.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/wp-google-maps/assets/icon-256x256.png?rev=970398\";s:2:\"1x\";s:66:\"https://ps.w.org/wp-google-maps/assets/icon-128x128.png?rev=970398\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/wp-google-maps/assets/banner-772x250.jpg?rev=792293\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.3\";s:12:\"requires_php\";s:3:\"5.3\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:29:\"wp-mail-smtp/wp_mail_smtp.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:26:\"w.org/plugins/wp-mail-smtp\";s:4:\"slug\";s:12:\"wp-mail-smtp\";s:6:\"plugin\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:11:\"new_version\";s:5:\"1.7.0\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/wp-mail-smtp/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wp-mail-smtp.1.7.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-256x256.png?rev=1755440\";s:2:\"1x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-128x128.png?rev=1755440\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/wp-mail-smtp/assets/banner-1544x500.png?rev=2120094\";s:2:\"1x\";s:67:\"https://ps.w.org/wp-mail-smtp/assets/banner-772x250.png?rev=2120094\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.4\";s:12:\"requires_php\";s:3:\"5.3\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:5:{s:45:\"better-image-credits/better-image-credits.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:34:\"w.org/plugins/better-image-credits\";s:4:\"slug\";s:20:\"better-image-credits\";s:6:\"plugin\";s:45:\"better-image-credits/better-image-credits.php\";s:11:\"new_version\";s:5:\"2.0.3\";s:3:\"url\";s:51:\"https://wordpress.org/plugins/better-image-credits/\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/plugin/better-image-credits.2.0.3.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:64:\"https://s.w.org/plugins/geopattern-icon/better-image-credits.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:31:\"cookie-notice/cookie-notice.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/cookie-notice\";s:4:\"slug\";s:13:\"cookie-notice\";s:6:\"plugin\";s:31:\"cookie-notice/cookie-notice.php\";s:11:\"new_version\";s:6:\"1.2.46\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/cookie-notice/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/cookie-notice.1.2.46.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/cookie-notice/assets/icon-256x256.png?rev=1805756\";s:2:\"1x\";s:66:\"https://ps.w.org/cookie-notice/assets/icon-128x128.png?rev=1805756\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/cookie-notice/assets/banner-772x250.png?rev=1805749\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"hello-dolly/hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:21:\"hello-dolly/hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}s:20:\"wp-colorbox/main.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/wp-colorbox\";s:4:\"slug\";s:11:\"wp-colorbox\";s:6:\"plugin\";s:20:\"wp-colorbox/main.php\";s:11:\"new_version\";s:5:\"1.1.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/wp-colorbox/\";s:7:\"package\";s:54:\"https://downloads.wordpress.org/plugin/wp-colorbox.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:64:\"https://ps.w.org/wp-colorbox/assets/icon-128x128.png?rev=1137713\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/wp-colorbox/assets/banner-772x250.jpg?rev=1666937\";}s:11:\"banners_rtl\";a:0:{}}s:15:\"wpide/WPide.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:19:\"w.org/plugins/wpide\";s:4:\"slug\";s:5:\"wpide\";s:6:\"plugin\";s:15:\"wpide/WPide.php\";s:11:\"new_version\";s:5:\"2.4.0\";s:3:\"url\";s:36:\"https://wordpress.org/plugins/wpide/\";s:7:\"package\";s:54:\"https://downloads.wordpress.org/plugin/wpide.2.4.0.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:56:\"https://s.w.org/plugins/geopattern-icon/wpide_f0f0f0.svg\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:59:\"https://ps.w.org/wpide/assets/banner-772x250.jpg?rev=537755\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(5508, 'recovery_mode_email_last_sent', '1570624587', 'yes'),
(9097, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.4.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.4.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.4-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.4-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.4\";s:7:\"version\";s:5:\"5.2.4\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1573188958;s:15:\"version_checked\";s:5:\"5.2.4\";s:12:\"translations\";a:0:{}}', 'no'),
(6664, 'filemanager_email_verified_15', 'yes', 'yes'),
(6784, 'mk_fm_close_fm_help_c_fm', 'done', 'yes'),
(5274, 'wp_mail_smtp_initial_version', '1.5.2', 'no'),
(5275, 'wp_mail_smtp_version', '1.5.2', 'no'),
(5276, 'wp_mail_smtp', 'a:5:{s:4:\"mail\";a:6:{s:10:\"from_email\";s:20:\"markus@hungenbach.de\";s:9:\"from_name\";s:8:\"creative\";s:6:\"mailer\";s:4:\"smtp\";s:11:\"return_path\";b:0;s:16:\"from_email_force\";b:0;s:15:\"from_name_force\";b:0;}s:4:\"smtp\";a:7:{s:7:\"autotls\";b:1;s:4:\"auth\";b:1;s:4:\"host\";s:13:\"smtp.ionos.de\";s:10:\"encryption\";s:3:\"tls\";s:4:\"port\";i:587;s:4:\"user\";s:21:\"picture@hungenbach.de\";s:4:\"pass\";s:16:\"J9.R3=XF9F8PT?m@\";}s:5:\"gmail\";a:2:{s:9:\"client_id\";s:0:\"\";s:13:\"client_secret\";s:0:\"\";}s:7:\"mailgun\";a:3:{s:7:\"api_key\";s:0:\"\";s:6:\"domain\";s:0:\"\";s:6:\"region\";s:2:\"US\";}s:8:\"sendgrid\";a:1:{s:7:\"api_key\";s:0:\"\";}}', 'no'),
(5277, '_amn_smtp_last_checked', '1573084800', 'yes'),
(5279, 'wp_mail_smtp_debug', 'a:0:{}', 'no'),
(9433, '_transient_health-check-site-status-result', '{\"good\":\"9\",\"recommended\":\"4\",\"critical\":\"3\"}', 'yes'),
(9695, '_transient_timeout_rm_user_online_status', '1573197037', 'no'),
(9696, '_transient_rm_user_online_status', 'a:1:{i:21;i:1573194981;}', 'no'),
(9665, '_site_transient_timeout_browser_5df79409a8a4b8f9cb4ab5ef474e3719', '1573737809', 'no'),
(9666, '_site_transient_browser_5df79409a8a4b8f9cb4ab5ef474e3719', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"77.0.3865.90\";s:8:\"platform\";s:5:\"Linux\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(9667, '_site_transient_timeout_php_check_a15c1e0c19aaa2847fb3da5439dce4ec', '1573737810', 'no'),
(9668, '_site_transient_php_check_a15c1e0c19aaa2847fb3da5439dce4ec', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(9683, '_transient_is_multi_author', '0', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(3, 5, '_edit_lock', '1564292076:1'),
(8, 1, '_edit_lock', '1548569469:1'),
(10, 14, '_edit_lock', '1547875628:1'),
(11, 17, '_edit_lock', '1547875644:1'),
(18, 26, '_edit_lock', '1547010173:1'),
(19, 26, '_edit_last', '1'),
(20, 26, '_hide_page_title', ''),
(21, 26, '_hide_header', ''),
(22, 26, '_hide_footer', ''),
(23, 26, '_hide_breadcrumb', ''),
(24, 26, '_cover', ''),
(25, 26, '_show_excerpt', ''),
(26, 26, '_wc_apply_product', ''),
(27, 33, '_menu_item_type', 'custom'),
(28, 33, '_menu_item_menu_item_parent', '0'),
(29, 33, '_menu_item_object_id', '33'),
(30, 33, '_menu_item_object', 'custom'),
(31, 33, '_menu_item_target', ''),
(32, 33, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(33, 33, '_menu_item_xfn', ''),
(34, 33, '_menu_item_url', 'https://picture.hungenbach.de'),
(35, 34, '_menu_item_type', 'post_type'),
(36, 34, '_menu_item_menu_item_parent', '0'),
(37, 34, '_menu_item_object_id', '26'),
(38, 34, '_menu_item_object', 'page'),
(39, 34, '_menu_item_target', ''),
(40, 34, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(41, 34, '_menu_item_xfn', ''),
(42, 34, '_menu_item_url', ''),
(43, 35, '_menu_item_type', 'post_type'),
(44, 35, '_menu_item_menu_item_parent', '0'),
(45, 35, '_menu_item_object_id', '23'),
(46, 35, '_menu_item_object', 'page'),
(47, 35, '_menu_item_target', ''),
(48, 35, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(49, 35, '_menu_item_xfn', ''),
(50, 35, '_menu_item_url', ''),
(77, 40, '_edit_lock', '1547096075:1'),
(78, 40, '_wp_page_template', 'template-frontpage.php'),
(79, 40, '_edit_last', '1'),
(80, 40, '_hide_page_title', ''),
(81, 40, '_hide_header', ''),
(82, 40, '_hide_footer', ''),
(83, 40, '_hide_breadcrumb', ''),
(84, 40, '_cover', ''),
(85, 40, '_show_excerpt', ''),
(86, 40, '_wc_apply_product', ''),
(92, 44, '_edit_lock', '1564131539:15'),
(93, 44, 'fu_form:92b6cbfa6120e13ff1654e28cef2a271', 'a:4:{s:8:\"internal\";a:4:{i:0;s:7:\"post_ID\";i:1;s:0:\"\";i:2;s:6:\"action\";i:3;s:11:\"form_layout\";}s:5:\"title\";a:1:{i:0;s:10:\"post_title\";}s:7:\"content\";a:1:{i:0;s:12:\"post_content\";}s:4:\"meta\";a:1:{i:0;s:5:\"files\";}}'),
(94, 44, '_edit_last', '1'),
(95, 44, '_hide_page_title', ''),
(96, 44, '_hide_header', ''),
(97, 44, '_hide_footer', ''),
(98, 44, '_hide_breadcrumb', ''),
(99, 44, '_cover', ''),
(100, 44, '_show_excerpt', ''),
(101, 44, '_wc_apply_product', ''),
(102, 46, '_wp_attached_file', '2019/01/image_20190110.png'),
(103, 46, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1152;s:6:\"height\";i:648;s:4:\"file\";s:26:\"2019/01/image_20190110.png\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"image_20190110-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"image_20190110-300x169.png\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"image_20190110-768x432.png\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"image_20190110-1024x576.png\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:26:\"image_20190110-300x150.png\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:26:\"image_20190110-480x300.png\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:26:\"image_20190110-640x400.png\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(104, 23, '_edit_lock', '1549082242:1'),
(105, 22, '_edit_lock', '1563943539:1'),
(106, 44, 'wpfm_found', 'upload'),
(122, 53, 'wpfm_file_name', '0f8c85d7_28e7_456f_afaa_6da0ad00c925.png'),
(123, 53, 'wpfm_node_type', 'file'),
(124, 53, 'wpfm_title', '0f8c85d7_28e7_456f_afaa_6da0ad00c925.png'),
(125, 53, 'wpfm_discription', ''),
(126, 53, 'wpfm_file_parent', '0'),
(127, 53, 'wpfm_dir_path', '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/test/0f8c85d7_28e7_456f_afaa_6da0ad00c925.png'),
(128, 53, 'wpfm_file_url', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/test/0f8c85d7_28e7_456f_afaa_6da0ad00c925.png'),
(129, 53, 'wpfm_date_created', 'January 12, 2019'),
(130, 53, 'wpfm_is_image', 'yes'),
(131, 53, 'wpfm_file_thumb_url', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/test/thumbs/0f8c85d7_28e7_456f_afaa_6da0ad00c925.png'),
(132, 53, 'wpfm_file_size', '4 MB'),
(133, 53, 'wpfm_total_downloads', '1'),
(136, 53, 'wpfm_file_location', 'local'),
(137, 56, '_form', '<label> Your Name (required)\n    [text* your-name] </label>\n\n<label> Your Email (required)\n    [email* your-email] </label>\n\n<label> Subject\n    [text your-subject] </label>\n\n<label> Your Message\n    [textarea your-message] </label>\n\n[submit \"Send\"]'),
(138, 56, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:26:\"[picture] \"[your-subject]\"\";s:6:\"sender\";s:43:\"[picture] <wordpress@picture.hungenbach.de>\";s:9:\"recipient\";s:20:\"markus@hungenbach.de\";s:4:\"body\";s:176:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on creative (https://picture.hungenbach.de)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(139, 56, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:25:\"creative \"[your-subject]\"\";s:6:\"sender\";s:42:\"creative <wordpress@picture.hungenbach.de>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:118:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on creative (https://picture.hungenbach.de)\";s:18:\"additional_headers\";s:30:\"Reply-To: markus@hungenbach.de\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(140, 56, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(141, 56, '_additional_settings', ''),
(142, 56, '_locale', 'en_US'),
(143, 57, '_edit_lock', '1564297580:1'),
(144, 57, '_edit_last', '1'),
(145, 57, '_hide_page_title', ''),
(146, 57, '_hide_header', ''),
(147, 57, '_hide_footer', ''),
(148, 57, '_hide_breadcrumb', ''),
(149, 57, '_cover', ''),
(150, 57, '_show_excerpt', ''),
(151, 57, '_wc_apply_product', ''),
(152, 59, '_menu_item_type', 'post_type'),
(153, 59, '_menu_item_menu_item_parent', '0'),
(154, 59, '_menu_item_object_id', '57'),
(155, 59, '_menu_item_object', 'page'),
(156, 59, '_menu_item_target', ''),
(157, 59, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(158, 59, '_menu_item_xfn', ''),
(159, 59, '_menu_item_url', ''),
(161, 60, 'wpfm_file_name', '1c065535_b9d5_4dfa_a078_891c916cef88.png'),
(162, 60, 'wpfm_node_type', 'file'),
(163, 60, 'wpfm_title', '1c065535_b9d5_4dfa_a078_891c916cef88.png'),
(164, 60, 'wpfm_discription', ''),
(165, 60, 'wpfm_file_parent', '0'),
(166, 60, 'wpfm_dir_path', '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/test/1c065535_b9d5_4dfa_a078_891c916cef88.png'),
(167, 60, 'wpfm_file_url', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/test/1c065535_b9d5_4dfa_a078_891c916cef88.png'),
(168, 60, 'wpfm_date_created', 'January 12, 2019'),
(169, 60, 'wpfm_is_image', 'yes'),
(170, 60, 'wpfm_file_thumb_url', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/test/thumbs/1c065535_b9d5_4dfa_a078_891c916cef88.png'),
(171, 60, 'wpfm_file_size', '6 MB'),
(172, 60, 'wpfm_total_downloads', '0'),
(175, 60, 'wpfm_file_location', 'local'),
(176, 62, 'wpfm_file_name', 'ac46ba24_e440_47fa_a1c4_e88bac69130b.png'),
(177, 62, 'wpfm_node_type', 'file'),
(178, 62, 'wpfm_title', 'ac46ba24_e440_47fa_a1c4_e88bac69130b.png'),
(179, 62, 'wpfm_discription', ''),
(180, 62, 'wpfm_file_parent', '0'),
(181, 62, 'wpfm_dir_path', '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/Test2/ac46ba24_e440_47fa_a1c4_e88bac69130b.png'),
(182, 62, 'wpfm_file_url', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/Test2/ac46ba24_e440_47fa_a1c4_e88bac69130b.png'),
(183, 62, 'wpfm_date_created', 'January 12, 2019'),
(184, 62, 'wpfm_is_image', 'yes'),
(185, 62, 'wpfm_file_thumb_url', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/Test2/thumbs/ac46ba24_e440_47fa_a1c4_e88bac69130b.png'),
(186, 62, 'wpfm_file_size', '165 KB'),
(187, 62, 'wpfm_total_downloads', '1'),
(188, 63, '_wp_attached_file', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/Test2/ac46ba24_e440_47fa_a1c4_e88bac69130b.png'),
(189, 63, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:750;s:6:\"height\";i:1334;s:4:\"file\";s:59:\"user_uploads/Test2/ac46ba24_e440_47fa_a1c4_e88bac69130b.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(190, 62, 'wpfm_file_location', 'local'),
(191, 23, 'wpfm_found', 'upload'),
(192, 23, '_edit_last', '1'),
(193, 23, '_hide_page_title', ''),
(194, 23, '_hide_header', ''),
(195, 23, '_hide_footer', ''),
(196, 23, '_hide_breadcrumb', ''),
(197, 23, '_cover', ''),
(198, 23, '_show_excerpt', ''),
(199, 23, '_wc_apply_product', ''),
(200, 67, '_menu_item_type', 'post_type'),
(201, 67, '_menu_item_menu_item_parent', '0'),
(202, 67, '_menu_item_object_id', '40'),
(203, 67, '_menu_item_object', 'page'),
(204, 67, '_menu_item_target', ''),
(205, 67, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(206, 67, '_menu_item_xfn', ''),
(207, 67, '_menu_item_url', ''),
(637, 241, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(636, 241, '_menu_item_target', ''),
(635, 241, '_menu_item_object', 'page'),
(634, 241, '_menu_item_object_id', '209'),
(633, 241, '_menu_item_menu_item_parent', '0'),
(632, 241, '_menu_item_type', 'post_type'),
(227, 70, '_menu_item_type', 'post_type'),
(228, 70, '_menu_item_menu_item_parent', '0'),
(229, 70, '_menu_item_object_id', '57'),
(230, 70, '_menu_item_object', 'page'),
(231, 70, '_menu_item_target', ''),
(232, 70, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(233, 70, '_menu_item_xfn', ''),
(234, 70, '_menu_item_url', ''),
(236, 22, '_edit_last', '1'),
(237, 22, '_hide_page_title', ''),
(238, 22, '_hide_header', ''),
(239, 22, '_hide_footer', ''),
(240, 22, '_hide_breadcrumb', ''),
(241, 22, '_cover', ''),
(242, 22, '_show_excerpt', ''),
(243, 22, '_wc_apply_product', ''),
(244, 72, '_menu_item_type', 'post_type'),
(245, 72, '_menu_item_menu_item_parent', '0'),
(246, 72, '_menu_item_object_id', '22'),
(247, 72, '_menu_item_object', 'page'),
(248, 72, '_menu_item_target', ''),
(249, 72, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(250, 72, '_menu_item_xfn', ''),
(251, 72, '_menu_item_url', ''),
(261, 14, '_edit_last', '1'),
(262, 14, '_hide_page_title', ''),
(263, 14, '_hide_header', ''),
(264, 14, '_hide_footer', ''),
(265, 14, '_hide_breadcrumb', ''),
(266, 14, '_cover', ''),
(267, 14, '_show_excerpt', ''),
(268, 14, '_wc_apply_product', ''),
(269, 5, '_edit_last', '1'),
(270, 5, '_hide_page_title', ''),
(271, 5, '_hide_header', ''),
(272, 5, '_hide_footer', ''),
(273, 5, '_hide_breadcrumb', ''),
(274, 5, '_cover', ''),
(275, 5, '_show_excerpt', ''),
(276, 5, '_wc_apply_product', ''),
(277, 17, '_edit_last', '1'),
(278, 17, '_hide_page_title', ''),
(279, 17, '_hide_header', ''),
(280, 17, '_hide_footer', ''),
(281, 17, '_hide_breadcrumb', ''),
(282, 17, '_cover', ''),
(283, 17, '_show_excerpt', ''),
(284, 17, '_wc_apply_product', ''),
(300, 105, '_wp_attached_file', '2019/01/background_dino.jpg'),
(301, 105, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:4032;s:6:\"height\";i:3024;s:4:\"file\";s:27:\"2019/01/background_dino.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"background_dino-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"background_dino-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"background_dino-768x576.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:28:\"background_dino-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:27:\"background_dino-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:27:\"background_dino-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:27:\"background_dino-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"1.8\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:8:\"iPhone 8\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1548002042\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"3.99\";s:3:\"iso\";s:3:\"100\";s:13:\"shutter_speed\";s:17:\"0.090909090909091\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(302, 105, '_wp_attachment_image_alt', 'Creative Hungenbach Dinosaur'),
(317, 1, '_thumbnail_id', '115'),
(318, 115, '_wp_attached_file', '2019/01/christopher-martyn-715646-unsplash.jpg'),
(319, 115, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:4177;s:6:\"height\";i:2785;s:4:\"file\";s:46:\"2019/01/christopher-martyn-715646-unsplash.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:46:\"christopher-martyn-715646-unsplash-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:46:\"christopher-martyn-715646-unsplash-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:46:\"christopher-martyn-715646-unsplash-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:47:\"christopher-martyn-715646-unsplash-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:46:\"christopher-martyn-715646-unsplash-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:46:\"christopher-martyn-715646-unsplash-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:46:\"christopher-martyn-715646-unsplash-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(321, 115, '_wp_attachment_source_name', 'Christopher Martyn'),
(322, 115, '_wp_attachment_source_url', 'https://unsplash.com/photos/_-ZzWO4jlRE?utm_source=unsplash&#038;utm_medium=referral&#038;utm_content=creditCopyText'),
(323, 115, '_wp_attachment_license', ''),
(324, 115, '_wp_attachment_license_url', ''),
(356, 130, '_edit_lock', '1564052787:1'),
(357, 130, '_edit_last', '1'),
(358, 130, '_hide_page_title', ''),
(359, 130, '_hide_header', ''),
(360, 130, '_hide_footer', ''),
(361, 130, '_hide_breadcrumb', ''),
(362, 130, '_cover', ''),
(363, 130, '_show_excerpt', ''),
(364, 130, '_wc_apply_product', ''),
(379, 142, '_edit_lock', '1564299456:1'),
(380, 142, '_edit_last', '1'),
(381, 142, '_hide_page_title', '1'),
(382, 142, '_hide_header', '1'),
(383, 142, '_hide_footer', '1'),
(384, 142, '_hide_breadcrumb', '1'),
(385, 142, '_cover', ''),
(386, 142, '_show_excerpt', ''),
(387, 142, '_wc_apply_product', ''),
(388, 142, '_wp_page_template', 'template-fullwidth-stretched.php'),
(554, 204, '_wp_attached_file', '2019/07/template_car_small.pdf'),
(555, 205, '_wp_attached_file', '2019/07/template_house_big.pdf'),
(556, 206, '_wp_attached_file', '2019/07/template_house_small.pdf'),
(553, 203, '_wp_attached_file', '2019/07/template_bus.pdf'),
(394, 150, '_wp_attached_file', '2019/02/thumbnail_bus.png'),
(395, 150, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1145;s:6:\"height\";i:816;s:4:\"file\";s:25:\"2019/02/thumbnail_bus.png\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"thumbnail_bus-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"thumbnail_bus-300x214.png\";s:5:\"width\";i:300;s:6:\"height\";i:214;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"thumbnail_bus-768x547.png\";s:5:\"width\";i:768;s:6:\"height\";i:547;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"thumbnail_bus-1024x730.png\";s:5:\"width\";i:1024;s:6:\"height\";i:730;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:25:\"thumbnail_bus-300x150.png\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:25:\"thumbnail_bus-480x300.png\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:25:\"thumbnail_bus-640x400.png\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(396, 151, '_wp_attached_file', '2019/02/thumbnail_car_small.png'),
(397, 151, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1145;s:6:\"height\";i:812;s:4:\"file\";s:31:\"2019/02/thumbnail_car_small.png\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"thumbnail_car_small-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"thumbnail_car_small-300x213.png\";s:5:\"width\";i:300;s:6:\"height\";i:213;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"thumbnail_car_small-768x545.png\";s:5:\"width\";i:768;s:6:\"height\";i:545;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:32:\"thumbnail_car_small-1024x726.png\";s:5:\"width\";i:1024;s:6:\"height\";i:726;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:31:\"thumbnail_car_small-300x150.png\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:31:\"thumbnail_car_small-480x300.png\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:31:\"thumbnail_car_small-640x400.png\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(398, 152, '_wp_attached_file', '2019/02/thumbnail_house_big.png'),
(399, 152, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:604;s:6:\"height\";i:833;s:4:\"file\";s:31:\"2019/02/thumbnail_house_big.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"thumbnail_house_big-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"thumbnail_house_big-218x300.png\";s:5:\"width\";i:218;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:31:\"thumbnail_house_big-300x150.png\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:31:\"thumbnail_house_big-480x300.png\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:31:\"thumbnail_house_big-604x400.png\";s:5:\"width\";i:604;s:6:\"height\";i:400;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(400, 153, '_wp_attached_file', '2019/02/thumbnail_house_small.png'),
(401, 153, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1254;s:6:\"height\";i:886;s:4:\"file\";s:33:\"2019/02/thumbnail_house_small.png\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"thumbnail_house_small-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"thumbnail_house_small-300x212.png\";s:5:\"width\";i:300;s:6:\"height\";i:212;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"thumbnail_house_small-768x543.png\";s:5:\"width\";i:768;s:6:\"height\";i:543;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:34:\"thumbnail_house_small-1024x723.png\";s:5:\"width\";i:1024;s:6:\"height\";i:723;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:33:\"thumbnail_house_small-300x150.png\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:33:\"thumbnail_house_small-480x300.png\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:33:\"thumbnail_house_small-640x400.png\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(402, 157, '_wp_attached_file', '2019/02/thumbnail_car_small-1.png'),
(403, 157, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1145;s:6:\"height\";i:812;s:4:\"file\";s:33:\"2019/02/thumbnail_car_small-1.png\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"thumbnail_car_small-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"thumbnail_car_small-1-300x213.png\";s:5:\"width\";i:300;s:6:\"height\";i:213;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"thumbnail_car_small-1-768x545.png\";s:5:\"width\";i:768;s:6:\"height\";i:545;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:34:\"thumbnail_car_small-1-1024x726.png\";s:5:\"width\";i:1024;s:6:\"height\";i:726;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:33:\"thumbnail_car_small-1-300x150.png\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:33:\"thumbnail_car_small-1-480x300.png\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:33:\"thumbnail_car_small-1-640x400.png\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(407, 179, 'wpfm_file_name', 'untitled.png'),
(408, 179, 'wpfm_node_type', 'file'),
(409, 179, 'wpfm_title', 'untitled.png'),
(410, 179, 'wpfm_discription', ''),
(411, 179, 'wpfm_file_parent', '0'),
(412, 179, 'wpfm_dir_path', '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus@hungenbach.de/untitled.png'),
(413, 179, 'wpfm_file_url', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/markus@hungenbach.de/untitled.png'),
(414, 179, 'wpfm_date_created', 'June 22, 2019'),
(415, 179, 'wpfm_is_image', 'yes'),
(416, 179, 'wpfm_file_thumb_url', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/markus@hungenbach.de/thumbs/untitled.png'),
(417, 179, 'wpfm_file_size', '12 KB'),
(418, 179, 'wpfm_total_downloads', '0'),
(419, 180, '_wp_attached_file', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/markus@hungenbach.de/untitled.png'),
(420, 180, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1157;s:6:\"height\";i:806;s:4:\"file\";s:46:\"user_uploads/markus@hungenbach.de/untitled.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(421, 179, 'wpfm_file_location', 'local'),
(422, 181, 'wpfm_file_name', 'my_image.jpg'),
(423, 181, 'wpfm_node_type', 'file'),
(424, 181, 'wpfm_title', 'my_image.jpg'),
(425, 181, 'wpfm_discription', ''),
(426, 181, 'wpfm_file_parent', '0'),
(427, 181, 'wpfm_dir_path', '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/test111/my_image.jpg'),
(428, 181, 'wpfm_file_url', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/test111/my_image.jpg'),
(429, 181, 'wpfm_date_created', 'June 24, 2019'),
(430, 181, 'wpfm_is_image', 'yes'),
(431, 181, 'wpfm_file_thumb_url', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/test111/thumbs/my_image.jpg'),
(432, 181, 'wpfm_file_size', '96 KB'),
(433, 181, 'wpfm_total_downloads', '1'),
(434, 182, '_wp_attached_file', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/test111/my_image.jpg'),
(435, 182, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:639;s:4:\"file\";s:33:\"user_uploads/test111/my_image.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(436, 181, 'wpfm_file_location', 'local'),
(437, 183, 'wpfm_file_name', 'images.jpg'),
(438, 183, 'wpfm_node_type', 'file'),
(439, 183, 'wpfm_title', 'images.jpg'),
(440, 183, 'wpfm_discription', ''),
(441, 183, 'wpfm_file_parent', '0'),
(442, 183, 'wpfm_dir_path', '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/AlexBregman/images.jpg'),
(443, 183, 'wpfm_file_url', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/AlexBregman/images.jpg'),
(444, 183, 'wpfm_date_created', 'June 29, 2019'),
(445, 183, 'wpfm_is_image', 'yes'),
(446, 183, 'wpfm_file_thumb_url', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/AlexBregman/thumbs/images.jpg'),
(447, 183, 'wpfm_file_size', '7 KB'),
(448, 183, 'wpfm_total_downloads', '0'),
(449, 184, '_wp_attached_file', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/AlexBregman/images.jpg'),
(450, 184, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:318;s:6:\"height\";i:159;s:4:\"file\";s:35:\"user_uploads/AlexBregman/images.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(451, 183, 'wpfm_file_location', 'local'),
(452, 185, 'wpfm_file_name', 'img_20180607_192631_237.jpg'),
(453, 185, 'wpfm_node_type', 'file'),
(454, 185, 'wpfm_title', 'img_20180607_192631_237.jpg'),
(455, 185, 'wpfm_discription', ''),
(456, 185, 'wpfm_file_parent', '0'),
(457, 185, 'wpfm_dir_path', '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/testing/img_20180607_192631_237.jpg'),
(458, 185, 'wpfm_file_url', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/testing/img_20180607_192631_237.jpg'),
(459, 185, 'wpfm_date_created', 'July 2, 2019'),
(460, 185, 'wpfm_is_image', 'yes'),
(461, 185, 'wpfm_file_thumb_url', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/testing/thumbs/img_20180607_192631_237.jpg'),
(462, 185, 'wpfm_file_size', '166 KB'),
(463, 185, 'wpfm_total_downloads', '2'),
(464, 186, '_wp_attached_file', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/testing/img_20180607_192631_237.jpg'),
(465, 186, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:818;s:6:\"height\";i:818;s:4:\"file\";s:48:\"user_uploads/testing/img_20180607_192631_237.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(466, 185, 'wpfm_file_location', 'local'),
(467, 187, 'wpfm_file_name', 'simple_house_3d_model_c4d_max_obj_fbx_ma_lwo_3ds_3dm_stl_1520429_o.jpg'),
(468, 187, 'wpfm_node_type', 'file'),
(469, 187, 'wpfm_title', 'simple_house_3d_model_c4d_max_obj_fbx_ma_lwo_3ds_3dm_stl_1520429_o.jpg'),
(470, 187, 'wpfm_discription', ''),
(471, 187, 'wpfm_file_parent', '0'),
(472, 187, 'wpfm_dir_path', '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/tt/simple_house_3d_model_c4d_max_obj_fbx_ma_lwo_3ds_3dm_stl_1520429_o.jpg'),
(473, 187, 'wpfm_file_url', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/tt/simple_house_3d_model_c4d_max_obj_fbx_ma_lwo_3ds_3dm_stl_1520429_o.jpg'),
(474, 187, 'wpfm_date_created', 'July 15, 2019'),
(475, 187, 'wpfm_is_image', 'yes'),
(476, 187, 'wpfm_file_thumb_url', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/tt/thumbs/simple_house_3d_model_c4d_max_obj_fbx_ma_lwo_3ds_3dm_stl_1520429_o.jpg'),
(477, 187, 'wpfm_file_size', '138 KB'),
(478, 187, 'wpfm_total_downloads', '0'),
(479, 188, '_wp_attached_file', 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/tt/simple_house_3d_model_c4d_max_obj_fbx_ma_lwo_3ds_3dm_stl_1520429_o.jpg'),
(480, 188, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1500;s:6:\"height\";i:844;s:4:\"file\";s:86:\"user_uploads/tt/simple_house_3d_model_c4d_max_obj_fbx_ma_lwo_3ds_3dm_stl_1520429_o.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(481, 187, 'wpfm_file_location', 'local'),
(536, 199, 'wpfm_file_name', 'template_bus.jpg'),
(535, 197, '_wc_apply_product', ''),
(533, 197, '_cover', ''),
(534, 197, '_show_excerpt', ''),
(530, 197, '_hide_header', ''),
(531, 197, '_hide_footer', ''),
(532, 197, '_hide_breadcrumb', ''),
(529, 197, '_hide_page_title', ''),
(528, 197, '_edit_last', '15'),
(591, 230, '_wp_attached_file', 'user_uploads/ChicMic/1564374016__template_bus-color.jpg'),
(527, 197, '_edit_lock', '1567142990:15'),
(592, 230, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2048;s:6:\"height\";i:1425;s:4:\"file\";s:55:\"user_uploads/ChicMic/1564374016__template_bus-color.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:42:\"1564374016__template_bus-color-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:42:\"1564374016__template_bus-color-300x209.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:209;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:42:\"1564374016__template_bus-color-768x534.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:534;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:43:\"1564374016__template_bus-color-1024x713.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:713;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:42:\"1564374016__template_bus-color-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:42:\"1564374016__template_bus-color-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:42:\"1564374016__template_bus-color-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(537, 199, 'wpfm_node_type', 'file'),
(538, 199, 'wpfm_title', 'template_bus.jpg'),
(539, 199, 'wpfm_discription', ''),
(540, 199, 'wpfm_file_parent', '0'),
(541, 199, 'wpfm_dir_path', '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/julian/template_bus.jpg'),
(542, 199, 'wpfm_file_url', 'http://localhost/picture_hungenbach/wp-content/uploads/user_uploads/julian/template_bus.jpg'),
(543, 199, 'wpfm_date_created', 'July 22, 2019'),
(544, 199, 'wpfm_is_image', 'yes'),
(545, 199, 'wpfm_file_thumb_url', 'http://localhost/picture_hungenbach/wp-content/uploads/user_uploads/julian/thumbs/template_bus.jpg'),
(546, 199, 'wpfm_file_size', '264 KB'),
(547, 199, 'wpfm_total_downloads', '0'),
(548, 200, '_wp_attached_file', 'http://localhost/picture_hungenbach/wp-content/uploads/user_uploads/julian/template_bus.jpg'),
(549, 200, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2798;s:6:\"height\";i:1781;s:4:\"file\";s:36:\"user_uploads/julian/template_bus.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:5:\"CHMSB\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:52:\"Microsoft Word - 20190122 Creative Template Bus.docx\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(550, 199, 'wpfm_file_location', 'local'),
(729, 286, '_wp_attached_file', 'user_uploads/SS/1566792793__sshot14.png.pagespeed.ce.bP3qghWPq2.png'),
(560, 209, '_edit_last', '15'),
(559, 209, '_edit_lock', '1567144182:15'),
(561, 209, '_hide_page_title', ''),
(562, 209, '_hide_header', ''),
(563, 209, '_hide_footer', ''),
(564, 209, '_hide_breadcrumb', ''),
(565, 209, '_cover', ''),
(566, 209, '_show_excerpt', ''),
(567, 209, '_wc_apply_product', ''),
(568, 211, '_wp_attached_file', 'user_uploads/ChicMic/1564131310__emily.png'),
(569, 211, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1201;s:6:\"height\";i:1202;s:4:\"file\";s:42:\"user_uploads/ChicMic/1564131310__emily.png\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"1564131310__emily-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"1564131310__emily-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"1564131310__emily-768x769.png\";s:5:\"width\";i:768;s:6:\"height\";i:769;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:31:\"1564131310__emily-1024x1024.png\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:29:\"1564131310__emily-300x150.png\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:29:\"1564131310__emily-480x300.png\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:29:\"1564131310__emily-640x400.png\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(629, 209, '_wp_page_template', ''),
(638, 241, '_menu_item_xfn', ''),
(639, 241, '_menu_item_url', ''),
(975, 413, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1570777472__54F0C925-9BE3-4589-AE65-AFFDA6B9F7B7.jpeg'),
(911, 380, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1570180924__FA4CF689-F85B-434A-B04E-8E8545129AFF.jpeg'),
(912, 380, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1536;s:6:\"height\";i:2158;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1570180924__FA4CF689-F85B-434A-B04E-8E8545129AFF.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1570180924__FA4CF689-F85B-434A-B04E-8E8545129AFF-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1570180924__FA4CF689-F85B-434A-B04E-8E8545129AFF-214x300.jpeg\";s:5:\"width\";i:214;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:62:\"1570180924__FA4CF689-F85B-434A-B04E-8E8545129AFF-768x1079.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:1079;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1570180924__FA4CF689-F85B-434A-B04E-8E8545129AFF-729x1024.jpeg\";s:5:\"width\";i:729;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1570180924__FA4CF689-F85B-434A-B04E-8E8545129AFF-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1570180924__FA4CF689-F85B-434A-B04E-8E8545129AFF-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1570180924__FA4CF689-F85B-434A-B04E-8E8545129AFF-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"1.8\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:8:\"iPhone 8\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1570187958\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"3.99\";s:3:\"iso\";s:2:\"32\";s:13:\"shutter_speed\";s:16:\"0.03030303030303\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(572, 215, '_wp_attached_file', 'user_uploads/ChicMic/1564131615__emily.png'),
(573, 215, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1201;s:6:\"height\";i:1202;s:4:\"file\";s:42:\"user_uploads/ChicMic/1564131615__emily.png\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:29:\"1564131615__emily-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:29:\"1564131615__emily-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:29:\"1564131615__emily-768x769.png\";s:5:\"width\";i:768;s:6:\"height\";i:769;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:31:\"1564131615__emily-1024x1024.png\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:29:\"1564131615__emily-300x150.png\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:29:\"1564131615__emily-480x300.png\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:29:\"1564131615__emily-640x400.png\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(574, 216, '_wp_attached_file', 'user_uploads/ChicMic/1564132880__testnude.png'),
(575, 216, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:300;s:6:\"height\";i:200;s:4:\"file\";s:45:\"user_uploads/ChicMic/1564132880__testnude.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"1564132880__testnude-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"1564132880__testnude-300x200.png\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:32:\"1564132880__testnude-300x150.png\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(576, 227, 'wpfm_file_name', 'untitled_2.png'),
(577, 227, 'wpfm_node_type', 'file'),
(578, 227, 'wpfm_title', 'untitled_2.png'),
(579, 227, 'wpfm_discription', ''),
(580, 227, 'wpfm_file_parent', '0'),
(581, 227, 'wpfm_dir_path', '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/untitled_2.png'),
(582, 227, 'wpfm_file_url', 'http://www.picture.hungenbach.de/wp-content/uploads/user_uploads/markus.hu@gmx.de/untitled_2.png'),
(583, 227, 'wpfm_date_created', 'July 28, 2019'),
(584, 227, 'wpfm_is_image', 'yes'),
(585, 227, 'wpfm_file_thumb_url', 'http://www.picture.hungenbach.de/wp-content/uploads/user_uploads/markus.hu@gmx.de/thumbs/untitled_2.png'),
(586, 227, 'wpfm_file_size', '844 B'),
(587, 227, 'wpfm_total_downloads', '0'),
(590, 227, 'wpfm_file_location', 'local'),
(593, 231, 'wpfm_file_name', 'img_20180607_192631_237.jpg'),
(594, 231, 'wpfm_node_type', 'file'),
(595, 231, 'wpfm_title', 'img_20180607_192631_237.jpg'),
(596, 231, 'wpfm_discription', ''),
(597, 231, 'wpfm_file_parent', '0'),
(598, 231, 'wpfm_dir_path', '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/ChicMic/img_20180607_192631_237.jpg'),
(599, 231, 'wpfm_file_url', 'http://www.picture.hungenbach.de/wp-content/uploads/user_uploads/ChicMic/img_20180607_192631_237.jpg'),
(600, 231, 'wpfm_date_created', 'July 29, 2019'),
(601, 231, 'wpfm_is_image', 'yes'),
(602, 231, 'wpfm_file_thumb_url', 'http://www.picture.hungenbach.de/wp-content/uploads/user_uploads/ChicMic/thumbs/img_20180607_192631_237.jpg'),
(603, 231, 'wpfm_file_size', '166 KB'),
(604, 231, 'wpfm_total_downloads', '0'),
(605, 232, '_wp_attached_file', 'http://www.picture.hungenbach.de/wp-content/uploads/user_uploads/ChicMic/img_20180607_192631_237.jpg'),
(606, 232, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:818;s:6:\"height\";i:818;s:4:\"file\";s:48:\"user_uploads/ChicMic/img_20180607_192631_237.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(607, 231, 'wpfm_file_location', 'local'),
(608, 233, 'wpfm_file_name', 'template_house_small.jpg'),
(609, 233, 'wpfm_node_type', 'file'),
(610, 233, 'wpfm_title', 'template_house_small.jpg'),
(611, 233, 'wpfm_discription', ''),
(612, 233, 'wpfm_file_parent', '0'),
(613, 233, 'wpfm_dir_path', '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/ChicMic/template_house_small.jpg'),
(614, 233, 'wpfm_file_url', 'http://www.picture.hungenbach.de/wp-content/uploads/user_uploads/ChicMic/template_house_small.jpg'),
(615, 233, 'wpfm_date_created', 'July 29, 2019'),
(616, 233, 'wpfm_is_image', 'yes'),
(617, 233, 'wpfm_file_thumb_url', 'http://www.picture.hungenbach.de/wp-content/uploads/user_uploads/ChicMic/thumbs/template_house_small.jpg'),
(618, 233, 'wpfm_file_size', '132 KB'),
(619, 233, 'wpfm_total_downloads', '0'),
(620, 234, '_wp_attached_file', 'http://www.picture.hungenbach.de/wp-content/uploads/user_uploads/ChicMic/template_house_small.jpg'),
(621, 234, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2048;s:6:\"height\";i:1425;s:4:\"file\";s:45:\"user_uploads/ChicMic/template_house_small.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(622, 233, 'wpfm_file_location', 'local'),
(909, 379, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1570180900__56CA10F0-107B-4762-B6EF-63E9163EC4A8.jpeg');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(910, 379, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2188;s:6:\"height\";i:1536;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1570180900__56CA10F0-107B-4762-B6EF-63E9163EC4A8.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1570180900__56CA10F0-107B-4762-B6EF-63E9163EC4A8-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1570180900__56CA10F0-107B-4762-B6EF-63E9163EC4A8-300x211.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:211;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:61:\"1570180900__56CA10F0-107B-4762-B6EF-63E9163EC4A8-768x539.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:539;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1570180900__56CA10F0-107B-4762-B6EF-63E9163EC4A8-1024x719.jpeg\";s:5:\"width\";i:1024;s:6:\"height\";i:719;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1570180900__56CA10F0-107B-4762-B6EF-63E9163EC4A8-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1570180900__56CA10F0-107B-4762-B6EF-63E9163EC4A8-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1570180900__56CA10F0-107B-4762-B6EF-63E9163EC4A8-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"1.8\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:8:\"iPhone 8\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1570187975\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"3.99\";s:3:\"iso\";s:2:\"32\";s:13:\"shutter_speed\";s:16:\"0.03030303030303\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(660, 197, '_wp_page_template', '3dModelTemplate.php'),
(757, 301, '_wp_attached_file', 'user_uploads/mohammad/1567157812__house_small (1).jpg'),
(758, 301, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2048;s:6:\"height\";i:1425;s:4:\"file\";s:53:\"user_uploads/mohammad/1567157812__house_small (1).jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:39:\"1567157812__house_small (1)-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:39:\"1567157812__house_small (1)-300x209.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:209;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:39:\"1567157812__house_small (1)-768x534.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:534;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:40:\"1567157812__house_small (1)-1024x713.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:713;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:39:\"1567157812__house_small (1)-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:39:\"1567157812__house_small (1)-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:39:\"1567157812__house_small (1)-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(677, 259, '_wp_attachment_image_alt', 'picture hungenbach'),
(678, 260, '_wp_attached_file', '2019/08/cropped-logo.png'),
(679, 260, '_wp_attachment_context', 'custom-logo'),
(680, 260, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:872;s:6:\"height\";i:196;s:4:\"file\";s:24:\"2019/08/cropped-logo.png\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"cropped-logo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"cropped-logo-300x67.png\";s:5:\"width\";i:300;s:6:\"height\";i:67;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"cropped-logo-768x173.png\";s:5:\"width\";i:768;s:6:\"height\";i:173;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:24:\"cropped-logo-300x150.png\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:24:\"cropped-logo-480x196.png\";s:5:\"width\";i:480;s:6:\"height\";i:196;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:24:\"cropped-logo-640x196.png\";s:5:\"width\";i:640;s:6:\"height\";i:196;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(730, 286, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:640;s:6:\"height\";i:446;s:4:\"file\";s:67:\"user_uploads/SS/1566792793__sshot14.png.pagespeed.ce.bP3qghWPq2.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:59:\"1566792793__sshot14.png.pagespeed.ce.bP3qghWPq2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:59:\"1566792793__sshot14.png.pagespeed.ce.bP3qghWPq2-300x209.png\";s:5:\"width\";i:300;s:6:\"height\";i:209;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:59:\"1566792793__sshot14.png.pagespeed.ce.bP3qghWPq2-300x150.png\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:59:\"1566792793__sshot14.png.pagespeed.ce.bP3qghWPq2-480x300.png\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:59:\"1566792793__sshot14.png.pagespeed.ce.bP3qghWPq2-640x400.png\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(675, 259, '_wp_attached_file', '2019/08/logo.png'),
(676, 259, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:196;s:4:\"file\";s:16:\"2019/08/logo.png\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"logo-300x61.png\";s:5:\"width\";i:300;s:6:\"height\";i:61;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"logo-768x157.png\";s:5:\"width\";i:768;s:6:\"height\";i:157;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:16:\"logo-300x150.png\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:16:\"logo-480x196.png\";s:5:\"width\";i:480;s:6:\"height\";i:196;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:16:\"logo-640x196.png\";s:5:\"width\";i:640;s:6:\"height\";i:196;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(747, 295, '_wp_attached_file', 'user_uploads/mohammad/1566891808__Car_Qr.jpg'),
(753, 298, '_wp_attached_file', 'user_uploads/mohammad/1567137805__bus_QR_n.jpg'),
(728, 285, '_wp_attached_file', '2019/08/qr-code-reader-master.zip'),
(748, 295, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2048;s:6:\"height\";i:1425;s:4:\"file\";s:44:\"user_uploads/mohammad/1566891808__Car_Qr.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"1566891808__Car_Qr-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"1566891808__Car_Qr-300x209.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:209;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"1566891808__Car_Qr-768x534.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:534;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:31:\"1566891808__Car_Qr-1024x713.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:713;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:30:\"1566891808__Car_Qr-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:30:\"1566891808__Car_Qr-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:30:\"1566891808__Car_Qr-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(749, 296, '_wp_attached_file', 'user_uploads/mohammad/1566900312__house_small.jpg'),
(750, 296, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2048;s:6:\"height\";i:1425;s:4:\"file\";s:49:\"user_uploads/mohammad/1566900312__house_small.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"1566900312__house_small-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"1566900312__house_small-300x209.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:209;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"1566900312__house_small-768x534.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:534;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:36:\"1566900312__house_small-1024x713.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:713;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:35:\"1566900312__house_small-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:35:\"1566900312__house_small-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:35:\"1566900312__house_small-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(754, 298, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2048;s:6:\"height\";i:1425;s:4:\"file\";s:46:\"user_uploads/mohammad/1567137805__bus_QR_n.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"1567137805__bus_QR_n-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"1567137805__bus_QR_n-300x209.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:209;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"1567137805__bus_QR_n-768x534.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:534;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"1567137805__bus_QR_n-1024x713.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:713;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:32:\"1567137805__bus_QR_n-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:32:\"1567137805__bus_QR_n-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:32:\"1567137805__bus_QR_n-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(755, 299, '_wp_attached_file', 'user_uploads/mohammad/1567139275__bus_QR_n (1).jpg'),
(756, 299, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2048;s:6:\"height\";i:1425;s:4:\"file\";s:50:\"user_uploads/mohammad/1567139275__bus_QR_n (1).jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:36:\"1567139275__bus_QR_n (1)-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:36:\"1567139275__bus_QR_n (1)-300x209.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:209;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:36:\"1567139275__bus_QR_n (1)-768x534.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:534;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"1567139275__bus_QR_n (1)-1024x713.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:713;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:36:\"1567139275__bus_QR_n (1)-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:36:\"1567139275__bus_QR_n (1)-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:36:\"1567139275__bus_QR_n (1)-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(751, 297, '_wp_attached_file', 'user_uploads/mohammad/1566900359__house_big.jpg'),
(752, 297, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1475;s:6:\"height\";i:2048;s:4:\"file\";s:47:\"user_uploads/mohammad/1566900359__house_big.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"1566900359__house_big-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"1566900359__house_big-216x300.jpg\";s:5:\"width\";i:216;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:34:\"1566900359__house_big-768x1066.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1066;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:34:\"1566900359__house_big-738x1024.jpg\";s:5:\"width\";i:738;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:33:\"1566900359__house_big-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:33:\"1566900359__house_big-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:33:\"1566900359__house_big-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(976, 413, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1191;s:6:\"height\";i:1976;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1570777472__54F0C925-9BE3-4589-AE65-AFFDA6B9F7B7.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1570777472__54F0C925-9BE3-4589-AE65-AFFDA6B9F7B7-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1570777472__54F0C925-9BE3-4589-AE65-AFFDA6B9F7B7-181x300.jpeg\";s:5:\"width\";i:181;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:62:\"1570777472__54F0C925-9BE3-4589-AE65-AFFDA6B9F7B7-768x1274.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:1274;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1570777472__54F0C925-9BE3-4589-AE65-AFFDA6B9F7B7-617x1024.jpeg\";s:5:\"width\";i:617;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1570777472__54F0C925-9BE3-4589-AE65-AFFDA6B9F7B7-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1570777472__54F0C925-9BE3-4589-AE65-AFFDA6B9F7B7-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1570777472__54F0C925-9BE3-4589-AE65-AFFDA6B9F7B7-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"1.8\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:8:\"iPhone 8\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1570784544\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"3.99\";s:3:\"iso\";s:2:\"40\";s:13:\"shutter_speed\";s:17:\"0.058823529411765\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(913, 381, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1570181059__21B912B1-6ECE-4545-A71B-9B3B33B9C582.jpeg'),
(914, 381, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1536;s:6:\"height\";i:2204;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1570181059__21B912B1-6ECE-4545-A71B-9B3B33B9C582.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1570181059__21B912B1-6ECE-4545-A71B-9B3B33B9C582-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1570181059__21B912B1-6ECE-4545-A71B-9B3B33B9C582-209x300.jpeg\";s:5:\"width\";i:209;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:62:\"1570181059__21B912B1-6ECE-4545-A71B-9B3B33B9C582-768x1102.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:1102;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1570181059__21B912B1-6ECE-4545-A71B-9B3B33B9C582-714x1024.jpeg\";s:5:\"width\";i:714;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1570181059__21B912B1-6ECE-4545-A71B-9B3B33B9C582-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1570181059__21B912B1-6ECE-4545-A71B-9B3B33B9C582-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1570181059__21B912B1-6ECE-4545-A71B-9B3B33B9C582-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"1.8\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:8:\"iPhone 8\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1570188189\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"3.99\";s:3:\"iso\";s:2:\"32\";s:13:\"shutter_speed\";s:16:\"0.03030303030303\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(915, 382, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1570181077__B53EEC76-92A7-4C1A-B2B3-787E77C60474.jpeg'),
(916, 382, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1536;s:6:\"height\";i:2178;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1570181077__B53EEC76-92A7-4C1A-B2B3-787E77C60474.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1570181077__B53EEC76-92A7-4C1A-B2B3-787E77C60474-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1570181077__B53EEC76-92A7-4C1A-B2B3-787E77C60474-212x300.jpeg\";s:5:\"width\";i:212;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:62:\"1570181077__B53EEC76-92A7-4C1A-B2B3-787E77C60474-768x1089.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:1089;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1570181077__B53EEC76-92A7-4C1A-B2B3-787E77C60474-722x1024.jpeg\";s:5:\"width\";i:722;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1570181077__B53EEC76-92A7-4C1A-B2B3-787E77C60474-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1570181077__B53EEC76-92A7-4C1A-B2B3-787E77C60474-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1570181077__B53EEC76-92A7-4C1A-B2B3-787E77C60474-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"1.8\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:8:\"iPhone 8\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1570188179\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"3.99\";s:3:\"iso\";s:2:\"32\";s:13:\"shutter_speed\";s:16:\"0.03030303030303\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(917, 383, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1570181099__8DB9482B-412A-4705-AA5C-F687C75652EC.jpeg'),
(918, 383, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1537;s:6:\"height\";i:2162;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1570181099__8DB9482B-412A-4705-AA5C-F687C75652EC.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1570181099__8DB9482B-412A-4705-AA5C-F687C75652EC-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1570181099__8DB9482B-412A-4705-AA5C-F687C75652EC-213x300.jpeg\";s:5:\"width\";i:213;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:62:\"1570181099__8DB9482B-412A-4705-AA5C-F687C75652EC-768x1080.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:1080;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1570181099__8DB9482B-412A-4705-AA5C-F687C75652EC-728x1024.jpeg\";s:5:\"width\";i:728;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1570181099__8DB9482B-412A-4705-AA5C-F687C75652EC-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1570181099__8DB9482B-412A-4705-AA5C-F687C75652EC-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1570181099__8DB9482B-412A-4705-AA5C-F687C75652EC-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"1.8\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:8:\"iPhone 8\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1570188168\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"3.99\";s:3:\"iso\";s:2:\"32\";s:13:\"shutter_speed\";s:16:\"0.03030303030303\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(921, 385, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1570181167__128B30EE-CC95-488A-B1BE-0484DCF9BB44.jpeg'),
(922, 385, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1475;s:6:\"height\";i:2048;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1570181167__128B30EE-CC95-488A-B1BE-0484DCF9BB44.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1570181167__128B30EE-CC95-488A-B1BE-0484DCF9BB44-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1570181167__128B30EE-CC95-488A-B1BE-0484DCF9BB44-216x300.jpeg\";s:5:\"width\";i:216;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:62:\"1570181167__128B30EE-CC95-488A-B1BE-0484DCF9BB44-768x1066.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:1066;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1570181167__128B30EE-CC95-488A-B1BE-0484DCF9BB44-738x1024.jpeg\";s:5:\"width\";i:738;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1570181167__128B30EE-CC95-488A-B1BE-0484DCF9BB44-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1570181167__128B30EE-CC95-488A-B1BE-0484DCF9BB44-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1570181167__128B30EE-CC95-488A-B1BE-0484DCF9BB44-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(933, 391, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1570454323__01E7BC4C-EAF9-4F20-84AB-6F42AAF50455.jpeg'),
(934, 391, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1537;s:6:\"height\";i:2162;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1570454323__01E7BC4C-EAF9-4F20-84AB-6F42AAF50455.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1570454323__01E7BC4C-EAF9-4F20-84AB-6F42AAF50455-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1570454323__01E7BC4C-EAF9-4F20-84AB-6F42AAF50455-213x300.jpeg\";s:5:\"width\";i:213;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:62:\"1570454323__01E7BC4C-EAF9-4F20-84AB-6F42AAF50455-768x1080.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:1080;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1570454323__01E7BC4C-EAF9-4F20-84AB-6F42AAF50455-728x1024.jpeg\";s:5:\"width\";i:728;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1570454323__01E7BC4C-EAF9-4F20-84AB-6F42AAF50455-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1570454323__01E7BC4C-EAF9-4F20-84AB-6F42AAF50455-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1570454323__01E7BC4C-EAF9-4F20-84AB-6F42AAF50455-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"1.8\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:8:\"iPhone 8\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1570188168\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"3.99\";s:3:\"iso\";s:2:\"32\";s:13:\"shutter_speed\";s:16:\"0.03030303030303\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(974, 412, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1280;s:6:\"height\";i:960;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1570777393__0514A769-3859-44C2-AD4D-984066A1BCC7.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1570777393__0514A769-3859-44C2-AD4D-984066A1BCC7-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1570777393__0514A769-3859-44C2-AD4D-984066A1BCC7-300x225.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:61:\"1570777393__0514A769-3859-44C2-AD4D-984066A1BCC7-768x576.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1570777393__0514A769-3859-44C2-AD4D-984066A1BCC7-1024x768.jpeg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1570777393__0514A769-3859-44C2-AD4D-984066A1BCC7-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1570777393__0514A769-3859-44C2-AD4D-984066A1BCC7-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1570777393__0514A769-3859-44C2-AD4D-984066A1BCC7-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(973, 412, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1570777393__0514A769-3859-44C2-AD4D-984066A1BCC7.jpeg'),
(1247, 550, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1571674478__1625BF89-19F0-4797-A828-8C7A087BF11F.jpeg'),
(1248, 550, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2048;s:6:\"height\";i:1536;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1571674478__1625BF89-19F0-4797-A828-8C7A087BF11F.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1571674478__1625BF89-19F0-4797-A828-8C7A087BF11F-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1571674478__1625BF89-19F0-4797-A828-8C7A087BF11F-300x225.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:61:\"1571674478__1625BF89-19F0-4797-A828-8C7A087BF11F-768x576.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1571674478__1625BF89-19F0-4797-A828-8C7A087BF11F-1024x768.jpeg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1571674478__1625BF89-19F0-4797-A828-8C7A087BF11F-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1571674478__1625BF89-19F0-4797-A828-8C7A087BF11F-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1571674478__1625BF89-19F0-4797-A828-8C7A087BF11F-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(971, 411, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1570771427__8A13EEE2-6676-4889-8B34-C2E957498709.jpeg'),
(972, 411, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2048;s:6:\"height\";i:1425;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1570771427__8A13EEE2-6676-4889-8B34-C2E957498709.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1570771427__8A13EEE2-6676-4889-8B34-C2E957498709-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1570771427__8A13EEE2-6676-4889-8B34-C2E957498709-300x209.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:209;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:61:\"1570771427__8A13EEE2-6676-4889-8B34-C2E957498709-768x534.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:534;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1570771427__8A13EEE2-6676-4889-8B34-C2E957498709-1024x713.jpeg\";s:5:\"width\";i:1024;s:6:\"height\";i:713;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1570771427__8A13EEE2-6676-4889-8B34-C2E957498709-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1570771427__8A13EEE2-6676-4889-8B34-C2E957498709-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1570771427__8A13EEE2-6676-4889-8B34-C2E957498709-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(821, 333, '_wp_attached_file', 'user_uploads/hungenbach/1567394634__1E8D6051-FB83-46B0-AC43-521175836A39.jpeg'),
(822, 333, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2592;s:6:\"height\";i:1936;s:4:\"file\";s:77:\"user_uploads/hungenbach/1567394634__1E8D6051-FB83-46B0-AC43-521175836A39.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1567394634__1E8D6051-FB83-46B0-AC43-521175836A39-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1567394634__1E8D6051-FB83-46B0-AC43-521175836A39-300x224.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:61:\"1567394634__1E8D6051-FB83-46B0-AC43-521175836A39-768x574.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:574;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1567394634__1E8D6051-FB83-46B0-AC43-521175836A39-1024x765.jpeg\";s:5:\"width\";i:1024;s:6:\"height\";i:765;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1567394634__1E8D6051-FB83-46B0-AC43-521175836A39-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1567394634__1E8D6051-FB83-46B0-AC43-521175836A39-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1567394634__1E8D6051-FB83-46B0-AC43-521175836A39-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"2.4\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:8:\"iPad Air\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1567271984\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:3:\"3.3\";s:3:\"iso\";s:3:\"320\";s:13:\"shutter_speed\";s:4:\"0.04\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1217, 534, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1571542868__D9B922A4-AF02-4AC7-AB85-253F27A1BC21.jpeg'),
(1218, 534, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2048;s:6:\"height\";i:1536;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1571542868__D9B922A4-AF02-4AC7-AB85-253F27A1BC21.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1571542868__D9B922A4-AF02-4AC7-AB85-253F27A1BC21-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1571542868__D9B922A4-AF02-4AC7-AB85-253F27A1BC21-300x225.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:61:\"1571542868__D9B922A4-AF02-4AC7-AB85-253F27A1BC21-768x576.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1571542868__D9B922A4-AF02-4AC7-AB85-253F27A1BC21-1024x768.jpeg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1571542868__D9B922A4-AF02-4AC7-AB85-253F27A1BC21-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1571542868__D9B922A4-AF02-4AC7-AB85-253F27A1BC21-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1571542868__D9B922A4-AF02-4AC7-AB85-253F27A1BC21-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(825, 335, '_wp_attached_file', 'user_uploads/hungenbach/1567396069__0B20D265-B151-4DDE-8382-6988A9B65D6D.jpeg'),
(826, 335, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2592;s:6:\"height\";i:1936;s:4:\"file\";s:77:\"user_uploads/hungenbach/1567396069__0B20D265-B151-4DDE-8382-6988A9B65D6D.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1567396069__0B20D265-B151-4DDE-8382-6988A9B65D6D-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1567396069__0B20D265-B151-4DDE-8382-6988A9B65D6D-300x224.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:61:\"1567396069__0B20D265-B151-4DDE-8382-6988A9B65D6D-768x574.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:574;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1567396069__0B20D265-B151-4DDE-8382-6988A9B65D6D-1024x765.jpeg\";s:5:\"width\";i:1024;s:6:\"height\";i:765;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1567396069__0B20D265-B151-4DDE-8382-6988A9B65D6D-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1567396069__0B20D265-B151-4DDE-8382-6988A9B65D6D-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1567396069__0B20D265-B151-4DDE-8382-6988A9B65D6D-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"2.4\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:8:\"iPad Air\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1567271955\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:3:\"3.3\";s:3:\"iso\";s:3:\"250\";s:13:\"shutter_speed\";s:4:\"0.04\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(827, 336, '_wp_attached_file', 'user_uploads/mohammad/1567406244__iphone_IMG_5477.jpg'),
(828, 336, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:4032;s:6:\"height\";i:3024;s:4:\"file\";s:53:\"user_uploads/mohammad/1567406244__iphone_IMG_5477.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:39:\"1567406244__iphone_IMG_5477-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:39:\"1567406244__iphone_IMG_5477-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:39:\"1567406244__iphone_IMG_5477-768x576.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:40:\"1567406244__iphone_IMG_5477-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:39:\"1567406244__iphone_IMG_5477-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:39:\"1567406244__iphone_IMG_5477-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:39:\"1567406244__iphone_IMG_5477-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"1.8\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:8:\"iPhone 8\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1567271144\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"3.99\";s:3:\"iso\";s:3:\"100\";s:13:\"shutter_speed\";s:17:\"0.071428571428571\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"6\";s:8:\"keywords\";a:0:{}}}'),
(829, 337, '_wp_attached_file', 'user_uploads/mohammad/1568007588__ipad_IMG_0511.JPG'),
(830, 337, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2592;s:6:\"height\";i:1936;s:4:\"file\";s:51:\"user_uploads/mohammad/1568007588__ipad_IMG_0511.JPG\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"1568007588__ipad_IMG_0511-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"1568007588__ipad_IMG_0511-300x224.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:224;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:37:\"1568007588__ipad_IMG_0511-768x574.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:574;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:38:\"1568007588__ipad_IMG_0511-1024x765.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:765;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:37:\"1568007588__ipad_IMG_0511-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:37:\"1568007588__ipad_IMG_0511-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:37:\"1568007588__ipad_IMG_0511-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"2.4\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:8:\"iPad Air\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1567271984\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:3:\"3.3\";s:3:\"iso\";s:3:\"320\";s:13:\"shutter_speed\";s:4:\"0.04\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1245, 549, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1571674413__4FAF2CC5-BBB4-4609-B722-FBC0A1EC87C3.jpeg'),
(1246, 549, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1444;s:6:\"height\";i:2048;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1571674413__4FAF2CC5-BBB4-4609-B722-FBC0A1EC87C3.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1571674413__4FAF2CC5-BBB4-4609-B722-FBC0A1EC87C3-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1571674413__4FAF2CC5-BBB4-4609-B722-FBC0A1EC87C3-212x300.jpeg\";s:5:\"width\";i:212;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:62:\"1571674413__4FAF2CC5-BBB4-4609-B722-FBC0A1EC87C3-768x1089.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:1089;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1571674413__4FAF2CC5-BBB4-4609-B722-FBC0A1EC87C3-722x1024.jpeg\";s:5:\"width\";i:722;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1571674413__4FAF2CC5-BBB4-4609-B722-FBC0A1EC87C3-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1571674413__4FAF2CC5-BBB4-4609-B722-FBC0A1EC87C3-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1571674413__4FAF2CC5-BBB4-4609-B722-FBC0A1EC87C3-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(847, 347, '_wp_attached_file', 'user_uploads/shiv/1568962507__156896081118536539.jpeg'),
(848, 347, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:696;s:6:\"height\";i:1000;s:4:\"file\";s:53:\"user_uploads/shiv/1568962507__156896081118536539.jpeg\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:43:\"1568962507__156896081118536539-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:43:\"1568962507__156896081118536539-209x300.jpeg\";s:5:\"width\";i:209;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:43:\"1568962507__156896081118536539-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:43:\"1568962507__156896081118536539-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:43:\"1568962507__156896081118536539-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(849, 348, '_wp_attached_file', 'user_uploads/mohammad/1569386926__bus_QR_nColor.png');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(850, 348, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2048;s:6:\"height\";i:1425;s:4:\"file\";s:51:\"user_uploads/mohammad/1569386926__bus_QR_nColor.png\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"1569386926__bus_QR_nColor-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:37:\"1569386926__bus_QR_nColor-300x209.png\";s:5:\"width\";i:300;s:6:\"height\";i:209;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:37:\"1569386926__bus_QR_nColor-768x534.png\";s:5:\"width\";i:768;s:6:\"height\";i:534;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:38:\"1569386926__bus_QR_nColor-1024x713.png\";s:5:\"width\";i:1024;s:6:\"height\";i:713;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:37:\"1569386926__bus_QR_nColor-300x150.png\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:37:\"1569386926__bus_QR_nColor-480x300.png\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:37:\"1569386926__bus_QR_nColor-640x400.png\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(855, 352, '_wp_attached_file', 'user_uploads/ChicMic/1570094592__bus_QR_n.jpg'),
(856, 352, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2048;s:6:\"height\";i:1425;s:4:\"file\";s:45:\"user_uploads/ChicMic/1570094592__bus_QR_n.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"1570094592__bus_QR_n-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"1570094592__bus_QR_n-300x209.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:209;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"1570094592__bus_QR_n-768x534.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:534;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"1570094592__bus_QR_n-1024x713.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:713;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:32:\"1570094592__bus_QR_n-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:32:\"1570094592__bus_QR_n-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:32:\"1570094592__bus_QR_n-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(907, 378, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1570180878__39B13CB8-661B-4886-8A96-4539372F8EA5.jpeg'),
(908, 378, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2150;s:6:\"height\";i:1537;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1570180878__39B13CB8-661B-4886-8A96-4539372F8EA5.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1570180878__39B13CB8-661B-4886-8A96-4539372F8EA5-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1570180878__39B13CB8-661B-4886-8A96-4539372F8EA5-300x214.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:214;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:61:\"1570180878__39B13CB8-661B-4886-8A96-4539372F8EA5-768x549.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:549;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1570180878__39B13CB8-661B-4886-8A96-4539372F8EA5-1024x732.jpeg\";s:5:\"width\";i:1024;s:6:\"height\";i:732;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1570180878__39B13CB8-661B-4886-8A96-4539372F8EA5-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1570180878__39B13CB8-661B-4886-8A96-4539372F8EA5-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1570180878__39B13CB8-661B-4886-8A96-4539372F8EA5-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"1.8\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:8:\"iPhone 8\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1570187985\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"3.99\";s:3:\"iso\";s:2:\"32\";s:13:\"shutter_speed\";s:16:\"0.03030303030303\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(865, 357, '_wp_attached_file', '2019/10/tc.jpg'),
(866, 357, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:400;s:6:\"height\";i:303;s:4:\"file\";s:14:\"2019/10/tc.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"tc-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"tc-300x227.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:227;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:14:\"tc-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:14:\"tc-400x300.jpg\";s:5:\"width\";i:400;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(867, 358, '_wp_attached_file', '2019/10/hello_world.png'),
(868, 358, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:200;s:6:\"height\";i:200;s:4:\"file\";s:23:\"2019/10/hello_world.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"hello_world-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:23:\"hello_world-200x150.png\";s:5:\"width\";i:200;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(903, 376, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1570180576__C73CB81E-C396-4162-BAA3-DE348AD04CE8.jpeg'),
(904, 376, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2086;s:6:\"height\";i:1536;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1570180576__C73CB81E-C396-4162-BAA3-DE348AD04CE8.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1570180576__C73CB81E-C396-4162-BAA3-DE348AD04CE8-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1570180576__C73CB81E-C396-4162-BAA3-DE348AD04CE8-300x221.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:221;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:61:\"1570180576__C73CB81E-C396-4162-BAA3-DE348AD04CE8-768x566.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:566;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1570180576__C73CB81E-C396-4162-BAA3-DE348AD04CE8-1024x754.jpeg\";s:5:\"width\";i:1024;s:6:\"height\";i:754;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1570180576__C73CB81E-C396-4162-BAA3-DE348AD04CE8-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1570180576__C73CB81E-C396-4162-BAA3-DE348AD04CE8-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1570180576__C73CB81E-C396-4162-BAA3-DE348AD04CE8-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"1.8\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:8:\"iPhone 8\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1570187735\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"3.99\";s:3:\"iso\";s:2:\"25\";s:13:\"shutter_speed\";s:16:\"0.03030303030303\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(905, 377, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1570180692__3AD1C724-E63D-458C-9835-FF36B748DDEE.jpeg'),
(906, 377, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1536;s:6:\"height\";i:2168;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1570180692__3AD1C724-E63D-458C-9835-FF36B748DDEE.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1570180692__3AD1C724-E63D-458C-9835-FF36B748DDEE-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1570180692__3AD1C724-E63D-458C-9835-FF36B748DDEE-213x300.jpeg\";s:5:\"width\";i:213;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:62:\"1570180692__3AD1C724-E63D-458C-9835-FF36B748DDEE-768x1084.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:1084;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1570180692__3AD1C724-E63D-458C-9835-FF36B748DDEE-725x1024.jpeg\";s:5:\"width\";i:725;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1570180692__3AD1C724-E63D-458C-9835-FF36B748DDEE-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1570180692__3AD1C724-E63D-458C-9835-FF36B748DDEE-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1570180692__3AD1C724-E63D-458C-9835-FF36B748DDEE-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"1.8\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:8:\"iPhone 8\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1570187861\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"3.99\";s:3:\"iso\";s:2:\"25\";s:13:\"shutter_speed\";s:16:\"0.03030303030303\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1249, 551, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1571674786__158B048D-24F1-438D-8455-E4EC5FA82862.jpeg'),
(1250, 551, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2048;s:6:\"height\";i:1326;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1571674786__158B048D-24F1-438D-8455-E4EC5FA82862.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1571674786__158B048D-24F1-438D-8455-E4EC5FA82862-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1571674786__158B048D-24F1-438D-8455-E4EC5FA82862-300x194.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:194;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:61:\"1571674786__158B048D-24F1-438D-8455-E4EC5FA82862-768x497.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:497;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1571674786__158B048D-24F1-438D-8455-E4EC5FA82862-1024x663.jpeg\";s:5:\"width\";i:1024;s:6:\"height\";i:663;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1571674786__158B048D-24F1-438D-8455-E4EC5FA82862-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1571674786__158B048D-24F1-438D-8455-E4EC5FA82862-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1571674786__158B048D-24F1-438D-8455-E4EC5FA82862-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1127, 489, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1571328509__0B70EFC7-C967-4C96-B6DD-63FA4CC4D0A7.jpeg'),
(1128, 489, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:4032;s:6:\"height\";i:3024;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1571328509__0B70EFC7-C967-4C96-B6DD-63FA4CC4D0A7.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1571328509__0B70EFC7-C967-4C96-B6DD-63FA4CC4D0A7-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1571328509__0B70EFC7-C967-4C96-B6DD-63FA4CC4D0A7-300x225.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:61:\"1571328509__0B70EFC7-C967-4C96-B6DD-63FA4CC4D0A7-768x576.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1571328509__0B70EFC7-C967-4C96-B6DD-63FA4CC4D0A7-1024x768.jpeg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1571328509__0B70EFC7-C967-4C96-B6DD-63FA4CC4D0A7-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1571328509__0B70EFC7-C967-4C96-B6DD-63FA4CC4D0A7-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1571328509__0B70EFC7-C967-4C96-B6DD-63FA4CC4D0A7-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:3:\"1.8\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:8:\"iPhone 8\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1570784544\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:4:\"3.99\";s:3:\"iso\";s:2:\"40\";s:13:\"shutter_speed\";s:17:\"0.058823529411765\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"6\";s:8:\"keywords\";a:0:{}}}'),
(1453, 653, '_wp_attached_file', 'user_uploads/markus.hu@gmx.de/1571906461__2163363B-90E8-4A13-8AB0-D67BAF27401E.jpeg'),
(1454, 653, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1488;s:6:\"height\";i:1015;s:4:\"file\";s:83:\"user_uploads/markus.hu@gmx.de/1571906461__2163363B-90E8-4A13-8AB0-D67BAF27401E.jpeg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:61:\"1571906461__2163363B-90E8-4A13-8AB0-D67BAF27401E-150x150.jpeg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:61:\"1571906461__2163363B-90E8-4A13-8AB0-D67BAF27401E-300x205.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:205;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:61:\"1571906461__2163363B-90E8-4A13-8AB0-D67BAF27401E-768x524.jpeg\";s:5:\"width\";i:768;s:6:\"height\";i:524;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:62:\"1571906461__2163363B-90E8-4A13-8AB0-D67BAF27401E-1024x698.jpeg\";s:5:\"width\";i:1024;s:6:\"height\";i:698;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:61:\"1571906461__2163363B-90E8-4A13-8AB0-D67BAF27401E-300x150.jpeg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:61:\"1571906461__2163363B-90E8-4A13-8AB0-D67BAF27401E-480x300.jpeg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:61:\"1571906461__2163363B-90E8-4A13-8AB0-D67BAF27401E-640x400.jpeg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1463, 658, '_wp_attached_file', 'user_uploads/amy/1571920400__bus_QR__B.jpg'),
(1464, 658, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1488;s:6:\"height\";i:1162;s:4:\"file\";s:42:\"user_uploads/amy/1571920400__bus_QR__B.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"1571920400__bus_QR__B-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"1571920400__bus_QR__B-300x234.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:234;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"1571920400__bus_QR__B-768x600.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:34:\"1571920400__bus_QR__B-1024x800.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:800;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:33:\"1571920400__bus_QR__B-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:33:\"1571920400__bus_QR__B-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:33:\"1571920400__bus_QR__B-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1507, 681, '_wp_attached_file', 'user_uploads/amrish/1573189316__IMG_6204.JPG'),
(1508, 681, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1488;s:6:\"height\";i:1984;s:4:\"file\";s:44:\"user_uploads/amrish/1573189316__IMG_6204.JPG\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"1573189316__IMG_6204-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"1573189316__IMG_6204-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"1573189316__IMG_6204-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"1573189316__IMG_6204-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:32:\"1573189316__IMG_6204-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:32:\"1573189316__IMG_6204-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:32:\"1573189316__IMG_6204-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1509, 682, '_wp_attached_file', 'user_uploads/amrish/1573189367__IMG_6203.JPG'),
(1510, 682, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1488;s:6:\"height\";i:1984;s:4:\"file\";s:44:\"user_uploads/amrish/1573189367__IMG_6203.JPG\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"1573189367__IMG_6203-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"1573189367__IMG_6203-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"1573189367__IMG_6203-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"1573189367__IMG_6203-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:32:\"1573189367__IMG_6203-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:32:\"1573189367__IMG_6203-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:32:\"1573189367__IMG_6203-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1511, 683, '_wp_attached_file', 'user_uploads/amrish/1573189389__IMG_6196.jpg'),
(1512, 683, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1488;s:6:\"height\";i:1984;s:4:\"file\";s:44:\"user_uploads/amrish/1573189389__IMG_6196.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"1573189389__IMG_6196-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"1573189389__IMG_6196-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"1573189389__IMG_6196-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"1573189389__IMG_6196-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:32:\"1573189389__IMG_6196-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:32:\"1573189389__IMG_6196-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:32:\"1573189389__IMG_6196-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1513, 684, '_wp_attached_file', 'user_uploads/amrish/1573189415__IMG_6203_1.jpg'),
(1514, 684, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1488;s:6:\"height\";i:1984;s:4:\"file\";s:46:\"user_uploads/amrish/1573189415__IMG_6203_1.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"1573189415__IMG_6203_1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"1573189415__IMG_6203_1-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"1573189415__IMG_6203_1-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:35:\"1573189415__IMG_6203_1-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:34:\"1573189415__IMG_6203_1-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:34:\"1573189415__IMG_6203_1-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:34:\"1573189415__IMG_6203_1-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1515, 685, '_wp_attached_file', 'user_uploads/amrish/1573189433__IMG_6204_1.jpg'),
(1516, 685, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1488;s:6:\"height\";i:1984;s:4:\"file\";s:46:\"user_uploads/amrish/1573189433__IMG_6204_1.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"1573189433__IMG_6204_1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"1573189433__IMG_6204_1-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"1573189433__IMG_6204_1-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:35:\"1573189433__IMG_6204_1-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:34:\"1573189433__IMG_6204_1-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:34:\"1573189433__IMG_6204_1-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:34:\"1573189433__IMG_6204_1-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1525, 690, '_wp_attached_file', 'user_uploads/amrish/1573194000__IMG_6204.JPG'),
(1526, 690, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1488;s:6:\"height\";i:1984;s:4:\"file\";s:44:\"user_uploads/amrish/1573194000__IMG_6204.JPG\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"1573194000__IMG_6204-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"1573194000__IMG_6204-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"1573194000__IMG_6204-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"1573194000__IMG_6204-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:32:\"1573194000__IMG_6204-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:32:\"1573194000__IMG_6204-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:32:\"1573194000__IMG_6204-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1527, 691, '_wp_attached_file', 'user_uploads/amrish/1573194282__IMG_6204_1.jpg'),
(1528, 691, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1488;s:6:\"height\";i:1984;s:4:\"file\";s:46:\"user_uploads/amrish/1573194282__IMG_6204_1.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"1573194282__IMG_6204_1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"1573194282__IMG_6204_1-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"1573194282__IMG_6204_1-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:35:\"1573194282__IMG_6204_1-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:34:\"1573194282__IMG_6204_1-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:34:\"1573194282__IMG_6204_1-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:34:\"1573194282__IMG_6204_1-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1523, 689, '_wp_attached_file', 'user_uploads/amrish/1573193272__IMG_6204.JPG'),
(1524, 689, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1488;s:6:\"height\";i:1984;s:4:\"file\";s:44:\"user_uploads/amrish/1573193272__IMG_6204.JPG\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"1573193272__IMG_6204-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"1573193272__IMG_6204-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:33:\"1573193272__IMG_6204-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"1573193272__IMG_6204-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:19:\"onepress-blog-small\";a:4:{s:4:\"file\";s:32:\"1573193272__IMG_6204-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"onepress-small\";a:4:{s:4:\"file\";s:32:\"1573193272__IMG_6204-480x300.jpg\";s:5:\"width\";i:480;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:15:\"onepress-medium\";a:4:{s:4:\"file\";s:32:\"1573193272__IMG_6204-640x400.jpg\";s:5:\"width\";i:640;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-01-06 04:09:02', '2019-01-06 04:09:02', '<!-- wp:paragraph -->\n<p>Welcome to creative. This is to be yourself. Register and then start!</p>\n<!-- /wp:paragraph -->', 'We are live!', '', 'publish', 'closed', 'closed', '', 'hello-world', '', '', '2019-01-27 06:13:02', '2019-01-27 06:13:02', '', 0, 'http://picture.hungenbach.de/?p=1', 0, 'post', '', 0),
(5, 1, '2019-01-06 04:25:29', '2019-01-06 04:25:29', '<!-- wp:paragraph -->\n<p><strong>Effective Jan 19, 2019 </strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Our website address is: https://picture.hungenbach.de.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>In order to improve our Website, we collect some statistical  information, for example, the number of visitors to individual website pages.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Setting of cookies</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>a) what are cookies</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>This website uses so-called “cookies”. Cookies are small text files that are stored on your terminal device. They store certain information (e.g. site settings) which your browser may (depending on the lifespan of the cookie) retransmit to us upon your next visit to our Website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>b) what cookies do we use?</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>The following table contains a description of the cookies we use:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:table {\"className\":\"is-style-stripes\"} -->\n<table class=\"wp-block-table is-style-stripes\"><tbody><tr><td><strong>Purpose and content </strong></td><td><strong>Expiration Time</strong></td></tr><tr><td>Website analysis with Google Analytics. <br>Cookies used to distinguish users. <br></td><td>2 years</td></tr></tbody></table>\n<!-- /wp:table -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Website Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>On our website we use Google Analytics, a web analysis service of Google  Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States (“Google”). </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Google Analytics is a web  analytics service offered by Google that tracks and reports website  traffic. Google uses the data collected to track and monitor the use of  our Service. This data is shared with other Google services. Google may  use the collected data to contextualize and personalize the ads of its  own advertising network.                         </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Our website is using the IP anonymizing function offered by  Google, which will delete the last 8 digits (type IPv4) or the last 80  bits (type IPv6) of your IP address. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You may revoke your consent to the use of web analysis at any time, either by downloading and installing the <a rel=\"noreferrer noopener\" href=\"https://tools.google.com/dlpage/gaoptout?hl=en-GB\" target=\"_blank\">provided Google Browser Plugin</a> or by administrating your consents via the following button \"revoke cookies\" (press button \"No\" on the cookie banner), in which case the tracking is deactivated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> <code>[cookies_revoke]</code>  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Both options will prevent the application of web analysis only as long as you use the browser on which you installed the plugin and do not consent (press button \"Ok\" on the cookie banner) to the tracking described on the cookie banner.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph {\"align\":\"left\"} -->\n<p style=\"text-align:left\">Further information on Google Analytics is available in the <a rel=\"noreferrer noopener\" href=\"https://www.google.com/analytics/terms/us.html\" target=\"_blank\">Google Analytics Terms of Use</a>, the <a rel=\"noreferrer noopener\" href=\"https://support.google.com/analytics/answer/6004245?hl=en-GB\" target=\"_blank\">Privacy and Data Protection Guidelines of Google Analytics</a> and in the <a rel=\"noreferrer noopener\" href=\"http://www.google.com/intl/en/policies/privacy\" target=\"_blank\">Google Privacy Policy</a>.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Updates to our privacy statement</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>This privacy statement is updated from time to time. You will find the date of the latest update at the top of this page.</p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'publish', 'closed', 'closed', '', 'privacy-statement', '', '', '2019-01-19 05:24:11', '2019-01-19 05:24:11', '', 0, 'http://picture.hungenbach.de/?page_id=5', 0, 'page', '', 0),
(6, 1, '2019-01-06 04:25:29', '2019-01-06 04:25:29', '<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br>In order to imprrove our Website, we collect some statistical information, for example, the number of visitors to individual website pages. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>What we save:<br>- Internet Provider / IP address<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><br> Without exception any information we collect will be used solely for internal purposes. Personal data will only be collected by us from this Website if you voluntarily provide it or expressly give us your consent.</p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-06 04:25:29', '2019-01-06 04:25:29', '', 5, 'https://picture.hungenbach.de/2019/01/06/5-revision-v1/', 0, 'revision', '', 0),
(8, 1, '2019-01-06 04:32:42', '2019-01-06 04:32:42', '<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Our website address is: http://localhost/picture_hungenbach/. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br>In order to imprrove our Website, we collect some statistical information, for example, the number of visitors to individual website pages. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>What we save:<br>- Internet Provider / IP address<br>Without exception any information we collect will be used solely for internal purposes. Personal data will only be collected by us from this Website if you voluntarily provide it or expressly give us your consent.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>We are not yet using analytics software on this website. </p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-06 04:32:42', '2019-01-06 04:32:42', '', 5, 'https://picture.hungenbach.de/2019/01/06/5-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2019-01-06 04:33:15', '2019-01-06 04:33:15', '<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Our website address is: http://localhost/picture_hungenbach/. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>In order to imprrove our Website, we collect some statistical  information, for example, the number of visitors to individual website  pages.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>What we save:<br>- Internet Provider / IP address<br>Without exception any information we collect will be used solely for internal purposes. Personal data will only be collected by us from this Website if you voluntarily provide it or expressly give us your consent.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>We are not yet using analytics software on this website. </p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-06 04:33:15', '2019-01-06 04:33:15', '', 5, 'https://picture.hungenbach.de/2019/01/06/5-revision-v1/', 0, 'revision', '', 0),
(12, 1, '2019-01-07 03:45:41', '2019-01-07 03:45:41', '<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Our website address is: http://localhost/picture_hungenbach/. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>In order to imprrove our Website, we collect some statistical  information, for example, the number of visitors to individual website  pages.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>What we save:<br>- Internet Provider / IP address<br>Without exception any information we collect will be used solely for internal purposes. Personal data will only be collected by us from this Website if you voluntarily provide it or expressly give us your consent.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><code>[cookies_revoke]</code></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>We are not yet using analytics software on this website. </p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-07 03:45:41', '2019-01-07 03:45:41', '', 5, 'https://picture.hungenbach.de/2019/01/07/5-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2019-01-07 04:14:31', '2019-01-07 04:14:31', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2019-01-07 04:14:31', '2019-01-07 04:14:31', '', 1, 'http://picture.hungenbach.de/2019/01/07/1-revision-v1/', 0, 'revision', '', 0),
(14, 1, '2019-01-07 04:20:33', '2019-01-07 04:20:33', '<!-- wp:heading {\"level\":3} -->\n<h3>Published by:</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Markus Hungenbach<br>Hardt 36<br>D-51429 Bergisch Gladbach<br>Germany<br> <br>E-mail: Inquiries can be sent to webmaster (at) hungenbach.de or using our <a href=\"https://picture.hungenbach.de/contact/\">contact form</a>.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Last updated: Jan 19, 2019 </p>\n<!-- /wp:paragraph -->', 'Imprint', '', 'publish', 'closed', 'closed', '', 'imprint', '', '', '2019-01-19 05:27:07', '2019-01-19 05:27:07', '', 0, 'http://picture.hungenbach.de/?page_id=14', 0, 'page', '', 0),
(15, 1, '2019-01-07 04:20:33', '2019-01-07 04:20:33', '<!-- wp:heading {\"level\":3} -->\n<h3>Published by:</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Markus Hungenbach<br> Hardt 36<br> D-61429 Bergisch Gladbach<br>Germany<br> <br>E-mail: Inquiries can be sent to  webmaster (at) hungenbach.de or using our contact form.</p>\n<!-- /wp:paragraph -->', 'Imprint', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2019-01-07 04:20:33', '2019-01-07 04:20:33', '', 14, 'http://picture.hungenbach.de/2019/01/07/14-revision-v1/', 0, 'revision', '', 0),
(16, 1, '2019-01-07 04:21:42', '2019-01-07 04:21:42', '<!-- wp:heading {\"level\":3} -->\n<h3>Published by:</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Markus Hungenbach<br>Hardt 36<br>D-61429 Bergisch Gladbach<br>Germany<br> <br>E-mail: Inquiries can be sent to webmaster (at) hungenbach.de or using our contact form.</p>\n<!-- /wp:paragraph -->', 'Imprint', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2019-01-07 04:21:42', '2019-01-07 04:21:42', '', 14, 'http://picture.hungenbach.de/2019/01/07/14-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2019-01-07 04:24:04', '2019-01-07 04:24:04', '<!-- wp:heading {\"level\":3} -->\n<h3>1. Terms</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>By accessing this website, you are agreeing to be bound by these website Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this website. The materials contained in this website are protected by applicable copyright and trade mark law. This website has been developed by Markus  Hungenbach and is administered  by the same. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>2. Use License</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Permission is granted to temporarily download one copy of the materials (information or software) on Markus Hungenbach’s website for personal, non-commercial transitory viewing only. This is the grant of a license,  not a transfer of title, and under this license you may not:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>modify or copy the materials;</li><li>use the materials for any commercial purpose, or for any public display (commercial or non-commercial);</li><li>attempt to decompile or reverse engineer any software contained on Markus Hungenbach’s website;</li><li>remove any copyright or other proprietary notations from the materials; or</li><li>transfer the materials to another person or “mirror” the materials on any other server.</li></ul>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<p>This license shall automatically terminate if you violate any of  these restrictions and may be terminated by Markus Hungenbach at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>3. Disclaimer</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>The materials on Markus Hungenbach’s website are provided “as is”. Markus Hungenbach makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, Markus Hungenbach does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its website or otherwise relating to such materials or on any websites linked to this website. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>4. Links</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Markus Hungenbach has not reviewed all of the websites linked to its website and is not responsible for the contents of any such linked websitesite. The inclusion of any link does not imply endorsement by Markus Hungenbach of the website. Use of any such linked web site is at the user’s own risk. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>5. Site Terms of Use Modifications</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Markus Hungenbach may revise these terms of use for its website at any time without notice. By using this website you are agreeing to be bound by the then current version of these Terms and Conditions of Use. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Last updated: Jan 19, 2019 </p>\n<!-- /wp:paragraph -->', 'Terms and Conditions of Use', '', 'publish', 'closed', 'closed', '', 'terms-and-conditions-of-use', '', '', '2019-01-19 05:26:03', '2019-01-19 05:26:03', '', 0, 'http://picture.hungenbach.de/?page_id=17', 0, 'page', '', 0),
(18, 1, '2019-01-07 04:24:04', '2019-01-07 04:24:04', '<!-- wp:paragraph -->\n<p><strong>1. Terms</strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><br>By accessing this website, you are agreeing to be bound by these website Terms and Conditions of Use, all applicable laws and regulations,  and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are  prohibited from using or accessing this site. The materials contained in  this website are protected by applicable copyright and trade mark law.  This website has been developed by Markus  Hungenbach and is administered  by the same. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'Terms and Conditions of Use', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2019-01-07 04:24:04', '2019-01-07 04:24:04', '', 17, 'http://picture.hungenbach.de/2019/01/07/17-revision-v1/', 0, 'revision', '', 0),
(20, 1, '2019-01-07 04:30:47', '2019-01-07 04:30:47', '<!-- wp:heading {\"level\":3} -->\n<h3>1. Terms</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>By accessing this website, you are agreeing to be bound by these website Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this website. The materials contained in this website are protected by applicable copyright and trade mark law. This website has been developed by Markus  Hungenbach and is administered  by the same. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>2. Use License</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Permission is granted to temporarily download one copy of the materials (information or software) on Markus Hungenbach’s website for personal, non-commercial transitory viewing only. This is the grant of a license,  not a transfer of title, and under this license you may not:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>modify or copy the materials;</li><li>use the materials for any commercial purpose, or for any public display (commercial or non-commercial);</li><li>attempt to decompile or reverse engineer any software contained on Markus Hungenbach’s website;</li><li>remove any copyright or other proprietary notations from the materials; or</li><li>transfer the materials to another person or “mirror” the materials on any other server.</li></ul>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<p>This license shall automatically terminate if you violate any of  these restrictions and may be terminated by Markus Hungenbach at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>3. Disclaimer</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>The materials on Markus Hungenbach’s website are provided “as is”. Markus Hungenbach makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, Markus Hungenbach does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its website or otherwise relating to such materials or on any websites linked to this website. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>4. Links</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Markus Hungenbach has not reviewed all of the websites linked to its website and is not responsible for the contents of any such linked websitesite. The inclusion of any link does not imply endorsement by Markus Hungenbach of the website. Use of any such linked web site is at the user’s own risk. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>5. Site Terms of Use Modifications</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Markus Hungenbach may revise these terms of use for its website at any time without notice. By using this website you are agreeing to be bound by the then current version of these Terms and Conditions of Use. </p>\n<!-- /wp:paragraph -->', 'Terms and Conditions of Use', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2019-01-07 04:30:47', '2019-01-07 04:30:47', '', 17, 'http://picture.hungenbach.de/2019/01/07/17-revision-v1/', 0, 'revision', '', 0),
(21, 1, '2019-01-07 04:46:46', '2019-01-07 04:46:46', '<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Our website address is: http://localhost/picture_hungenbach/. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>In order to imprrove our Website, we collect some statistical  information, for example, the number of visitors to individual website  pages.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>What we save:<br>- Internet Provider / IP address<br>Without exception any information we collect will be used solely for internal purposes. Personal data will only be collected by us from this Website if you voluntarily provide it or expressly give us your consent.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><code>[cookies_revoke]</code></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> On our website we use Google Analytics, a web analysis service of Google  Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States (“Google”). </p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-07 04:46:46', '2019-01-07 04:46:46', '', 5, 'http://picture.hungenbach.de/2019/01/07/5-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2019-01-08 04:57:38', '2019-01-08 04:57:38', '[RM_Front_Submissions]', 'My Account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2019-01-14 03:35:03', '2019-01-14 03:35:03', '', 0, 'https://picture.hungenbach.de/rm_submissions/', 0, 'page', '', 0),
(23, 1, '2019-01-08 04:57:38', '2019-01-08 04:57:38', '<p>[RM_Login]</p>\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'Login', '', 'publish', 'closed', 'closed', '', 'rm_login', '', '', '2019-01-13 05:09:43', '2019-01-13 05:09:43', '', 0, 'https://picture.hungenbach.de/rm_login/', 0, 'page', '', 0),
(26, 1, '2019-01-09 04:39:51', '2019-01-09 04:39:51', '<!-- wp:paragraph -->\n<p><strong>[RM_Form id=\'4\']</strong></p>\n<!-- /wp:paragraph -->', 'Register', '', 'publish', 'closed', 'closed', '', 'registration', '', '', '2019-01-09 05:02:52', '2019-01-09 05:02:52', '', 0, 'https://picture.hungenbach.de/?page_id=26', 0, 'page', '', 0),
(27, 1, '2019-01-09 04:39:51', '2019-01-09 04:39:51', '<!-- wp:paragraph -->\n<p><strong>[RM_Form id=\'3\']</strong></p>\n<!-- /wp:paragraph -->', 'Registration', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-01-09 04:39:51', '2019-01-09 04:39:51', '', 26, 'https://picture.hungenbach.de/2019/01/09/26-revision-v1/', 0, 'revision', '', 0),
(29, 1, '2019-01-09 04:45:09', '2019-01-09 04:45:09', '<!-- wp:paragraph -->\n<p><strong>[RM_Form id=\'1\']</strong></p>\n<!-- /wp:paragraph -->', 'Registration', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-01-09 04:45:09', '2019-01-09 04:45:09', '', 26, 'https://picture.hungenbach.de/2019/01/09/26-revision-v1/', 0, 'revision', '', 0),
(30, 1, '2019-01-09 04:45:26', '2019-01-09 04:45:26', '<!-- wp:paragraph -->\n<p><strong>[RM_Form id=\'2\']</strong></p>\n<!-- /wp:paragraph -->', 'Registration', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-01-09 04:45:26', '2019-01-09 04:45:26', '', 26, 'https://picture.hungenbach.de/2019/01/09/26-revision-v1/', 0, 'revision', '', 0),
(31, 1, '2019-01-09 04:48:22', '2019-01-09 04:48:22', '<!-- wp:paragraph -->\n<p><strong>[RM_Form id=\'4\']</strong></p>\n<!-- /wp:paragraph -->', 'Registration', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-01-09 04:48:22', '2019-01-09 04:48:22', '', 26, 'https://picture.hungenbach.de/2019/01/09/26-revision-v1/', 0, 'revision', '', 0),
(33, 1, '2019-01-09 05:02:25', '2019-01-09 05:02:25', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2019-01-14 03:27:23', '2019-01-14 03:27:23', '', 0, 'https://picture.hungenbach.de/2019/01/09/home/', 1, 'nav_menu_item', '', 0),
(34, 1, '2019-01-09 05:02:25', '2019-01-09 05:02:25', ' ', '', '', 'publish', 'closed', 'closed', '', '34', '', '', '2019-01-14 03:27:23', '2019-01-14 03:27:23', '', 0, 'https://picture.hungenbach.de/2019/01/09/34/', 2, 'nav_menu_item', '', 0),
(35, 1, '2019-01-09 05:02:25', '2019-01-09 05:02:25', ' ', '', '', 'publish', 'closed', 'closed', '', '35', '', '', '2019-01-14 03:27:23', '2019-01-14 03:27:23', '', 0, 'https://picture.hungenbach.de/2019/01/09/35/', 3, 'nav_menu_item', '', 0),
(39, 1, '2019-01-09 05:02:51', '2019-01-09 05:02:51', '<!-- wp:paragraph -->\n<p><strong>[RM_Form id=\'4\']</strong></p>\n<!-- /wp:paragraph -->', 'Register', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-01-09 05:02:51', '2019-01-09 05:02:51', '', 26, 'https://picture.hungenbach.de/2019/01/09/26-revision-v1/', 0, 'revision', '', 0),
(40, 1, '2019-01-10 04:54:34', '2019-01-10 04:54:34', '<!-- wp:paragraph -->\n<p>my home page</p>\n<!-- /wp:paragraph -->', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2019-01-10 04:54:34', '2019-01-10 04:54:34', '', 0, 'https://picture.hungenbach.de/?page_id=40', 0, 'page', '', 0),
(41, 1, '2019-01-10 04:54:34', '2019-01-10 04:54:34', '<!-- wp:paragraph -->\n<p>my home page</p>\n<!-- /wp:paragraph -->', 'Home', '', 'inherit', 'closed', 'closed', '', '40-revision-v1', '', '', '2019-01-10 04:54:34', '2019-01-10 04:54:34', '', 40, 'https://picture.hungenbach.de/2019/01/10/40-revision-v1/', 0, 'revision', '', 0),
(44, 1, '2019-01-11 04:51:03', '2019-01-11 04:51:03', '<!-- wp:paragraph -->\n<p>[nm-wp-file-uploader]</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'Upload Image', '', 'publish', 'closed', 'closed', '', 'upload-image', '', '', '2019-01-12 04:32:03', '2019-01-12 04:32:03', '', 0, 'https://picture.hungenbach.de/?page_id=44', 0, 'page', '', 0),
(45, 1, '2019-01-11 04:51:03', '2019-01-11 04:51:03', '<!-- wp:paragraph -->\n<p>\n[fu-upload-form]\n\n</p>\n<!-- /wp:paragraph -->', 'Upload Image', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2019-01-11 04:51:03', '2019-01-11 04:51:03', '', 44, 'https://picture.hungenbach.de/2019/01/11/44-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2019-01-11 04:57:02', '2019-01-11 04:57:02', 'test', 'test', 'test', 'inherit', 'closed', 'closed', '', 'test', '', '', '2019-01-11 04:57:02', '2019-01-11 04:57:02', '', 44, 'https://picture.hungenbach.de/wp-content/uploads/2019/01/image_20190110.png', 0, 'attachment', 'image/png', 0),
(47, 1, '2019-01-12 03:55:53', '2019-01-12 03:55:53', '<!-- wp:paragraph -->\n<p><strong>[RM_Form id=\'5\']</strong></p>\n<!-- /wp:paragraph -->', 'Upload Image', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2019-01-12 03:55:53', '2019-01-12 03:55:53', '', 44, 'https://picture.hungenbach.de/2019/01/12/44-revision-v1/', 0, 'revision', '', 0),
(48, 1, '2019-01-12 04:11:58', '2019-01-12 04:11:58', '[wordpress_file_upload]<!-- wp:paragraph -->\n<p><strong>[RM_Form id=\'5\']</strong></p>\n<!-- /wp:paragraph -->', 'Upload Image', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2019-01-12 04:11:58', '2019-01-12 04:11:58', '', 44, 'https://picture.hungenbach.de/2019/01/12/44-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2019-01-12 04:16:28', '2019-01-12 04:16:28', '<!-- wp:paragraph -->\n<p><strong>[</strong></p>\n<!-- /wp:paragraph -->', 'Upload Image', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2019-01-12 04:16:28', '2019-01-12 04:16:28', '', 44, 'https://picture.hungenbach.de/2019/01/12/44-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2019-01-12 04:24:03', '2019-01-12 04:24:03', '<!-- wp:paragraph -->\n<p>[nm-wp-file-uploader]<strong>[</strong></p>\n<!-- /wp:paragraph -->', 'Upload Image', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2019-01-12 04:24:03', '2019-01-12 04:24:03', '', 44, 'https://picture.hungenbach.de/2019/01/12/44-revision-v1/', 0, 'revision', '', 0),
(53, 2, '2019-01-12 04:29:02', '2019-01-12 04:29:02', '', '0f8c85d7_28e7_456f_afaa_6da0ad00c925.png', '', 'publish', 'closed', 'closed', '', '0f8c85d7_28e7_456f_afaa_6da0ad00c925-png', '', '', '2019-01-12 04:29:02', '2019-01-12 04:29:02', '', 0, 'https://picture.hungenbach.de/wpfm-files/0f8c85d7_28e7_456f_afaa_6da0ad00c925-png/', 0, 'wpfm-files', '', 0),
(55, 1, '2019-01-12 04:32:02', '2019-01-12 04:32:02', '<!-- wp:paragraph -->\n<p>[nm-wp-file-uploader]</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'Upload Image', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2019-01-12 04:32:02', '2019-01-12 04:32:02', '', 44, 'https://picture.hungenbach.de/2019/01/12/44-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2019-01-12 05:00:25', '2019-01-12 05:00:25', '<label> Your Name (required)\r\n    [text* your-name] </label>\r\n\r\n<label> Your Email (required)\r\n    [email* your-email] </label>\r\n\r\n<label> Subject\r\n    [text your-subject] </label>\r\n\r\n<label> Your Message\r\n    [textarea your-message] </label>\r\n\r\n[submit \"Send\"]\n1\n[picture] \"[your-subject]\"\n[picture] <wordpress@picture.hungenbach.de>\nmarkus@hungenbach.de\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on creative (https://picture.hungenbach.de)\nReply-To: [your-email]\n\n\n\n\ncreative \"[your-subject]\"\ncreative <wordpress@picture.hungenbach.de>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on creative (https://picture.hungenbach.de)\nReply-To: markus@hungenbach.de\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Contact form 1', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2019-07-24 04:05:29', '2019-07-24 04:05:29', '', 0, 'https://picture.hungenbach.de/?post_type=wpcf7_contact_form&#038;p=56', 0, 'wpcf7_contact_form', '', 0),
(57, 1, '2019-01-12 05:03:58', '2019-01-12 05:03:58', '<!-- wp:shortcode -->\n[contact-form-7 id=\"56\" title=\"Contact form 1\"]\n<!-- /wp:shortcode -->', 'Contact', '', 'publish', 'closed', 'closed', '', 'contact', '', '', '2019-01-12 05:03:58', '2019-01-12 05:03:58', '', 0, 'https://picture.hungenbach.de/?page_id=57', 0, 'page', '', 0),
(58, 1, '2019-01-12 05:03:58', '2019-01-12 05:03:58', '<!-- wp:shortcode -->\n[contact-form-7 id=\"56\" title=\"Contact form 1\"]\n<!-- /wp:shortcode -->', 'Contact', '', 'inherit', 'closed', 'closed', '', '57-revision-v1', '', '', '2019-01-12 05:03:58', '2019-01-12 05:03:58', '', 57, 'https://picture.hungenbach.de/2019/01/12/57-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2019-01-12 05:04:45', '2019-01-12 05:04:45', ' ', '', '', 'publish', 'closed', 'closed', '', '59', '', '', '2019-01-14 03:27:23', '2019-01-14 03:27:23', '', 0, 'https://picture.hungenbach.de/?p=59', 4, 'nav_menu_item', '', 0),
(60, 2, '2019-01-12 06:15:14', '2019-01-12 06:15:14', '', '1c065535_b9d5_4dfa_a078_891c916cef88.png', '', 'publish', 'closed', 'closed', '', '1c065535_b9d5_4dfa_a078_891c916cef88-png', '', '', '2019-01-12 06:15:14', '2019-01-12 06:15:14', '', 0, 'https://picture.hungenbach.de/wpfm-files/1c065535_b9d5_4dfa_a078_891c916cef88-png/', 0, 'wpfm-files', '', 0),
(62, 3, '2019-01-12 11:15:13', '2019-01-12 11:15:13', '', 'ac46ba24_e440_47fa_a1c4_e88bac69130b.png', '', 'publish', 'closed', 'closed', '', 'ac46ba24_e440_47fa_a1c4_e88bac69130b-png', '', '', '2019-01-12 11:15:13', '2019-01-12 11:15:13', '', 0, 'https://picture.hungenbach.de/wpfm-files/ac46ba24_e440_47fa_a1c4_e88bac69130b-png/', 0, 'wpfm-files', '', 0),
(63, 3, '2019-01-12 11:15:13', '2019-01-12 11:15:13', '', 'ac46ba24_e440_47fa_a1c4_e88bac69130b.png', '', 'inherit', 'closed', 'closed', '', 'ac46ba24_e440_47fa_a1c4_e88bac69130b-png-2', '', '', '2019-01-12 11:15:13', '2019-01-12 11:15:13', '', 62, 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/Test2/ac46ba24_e440_47fa_a1c4_e88bac69130b.png', 0, 'attachment', 'image/png', 0),
(65, 1, '2019-01-13 04:31:36', '2019-01-13 04:31:36', '<p>[RM_Login]</p>\n\n<!-- wp:paragraph -->\n<p>\n[nm-wp-file-uploader]\n\n</p>\n<!-- /wp:paragraph -->', 'Login', '', 'inherit', 'closed', 'closed', '', '23-revision-v1', '', '', '2019-01-13 04:31:36', '2019-01-13 04:31:36', '', 23, 'https://picture.hungenbach.de/2019/01/13/23-revision-v1/', 0, 'revision', '', 0),
(66, 1, '2019-01-13 05:09:42', '2019-01-13 05:09:42', '<p>[RM_Login]</p>\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'Login', '', 'inherit', 'closed', 'closed', '', '23-revision-v1', '', '', '2019-01-13 05:09:42', '2019-01-13 05:09:42', '', 23, 'https://picture.hungenbach.de/2019/01/13/23-revision-v1/', 0, 'revision', '', 0),
(67, 1, '2019-01-14 03:28:48', '2019-01-14 03:28:48', ' ', '', '', 'publish', 'closed', 'closed', '', '67', '', '', '2019-01-19 05:42:27', '2019-01-19 05:42:27', '', 0, 'https://picture.hungenbach.de/?p=67', 1, 'nav_menu_item', '', 0),
(241, 1, '2019-07-31 06:44:53', '2019-07-31 06:44:53', '', 'Upload Image', '', 'publish', 'closed', 'closed', '', 'upload-image', '', '', '2019-07-31 06:44:53', '2019-07-31 06:44:53', '', 0, 'http://www.picture.hungenbach.de/upload-image/', 5, 'nav_menu_item', '', 0),
(70, 1, '2019-01-14 03:28:48', '2019-01-14 03:28:48', ' ', '', '', 'publish', 'closed', 'closed', '', '70', '', '', '2019-01-19 05:42:27', '2019-01-19 05:42:27', '', 0, 'https://picture.hungenbach.de/?p=70', 3, 'nav_menu_item', '', 0),
(71, 1, '2019-01-14 03:34:40', '2019-01-14 03:34:40', '[RM_Front_Submissions]', 'My Account', '', 'inherit', 'closed', 'closed', '', '22-revision-v1', '', '', '2019-01-14 03:34:40', '2019-01-14 03:34:40', '', 22, 'https://picture.hungenbach.de/2019/01/14/22-revision-v1/', 0, 'revision', '', 0),
(72, 1, '2019-01-14 03:36:17', '2019-01-14 03:36:17', ' ', '', '', 'publish', 'closed', 'closed', '', '72', '', '', '2019-01-19 05:42:27', '2019-01-19 05:42:27', '', 0, 'https://picture.hungenbach.de/?p=72', 2, 'nav_menu_item', '', 0),
(76, 1, '2019-01-14 11:01:12', '2019-01-14 11:01:12', '<!-- wp:heading {\"level\":3} -->\n<h3>Published by:</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Markus Hungenbach<br>Hardt 36<br>D-51429 Bergisch Gladbach<br>Germany<br> <br>E-mail: Inquiries can be sent to webmaster (at) hungenbach.de or using our contact form.</p>\n<!-- /wp:paragraph -->', 'Imprint', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2019-01-14 11:01:12', '2019-01-14 11:01:12', '', 14, 'https://picture.hungenbach.de/2019/01/14/14-revision-v1/', 0, 'revision', '', 0),
(78, 1, '2019-01-19 04:39:25', '2019-01-19 04:39:25', '<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Our website address is: http://localhost/picture_hungenbach/. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>In order to imprrove our Website, we collect some statistical  information, for example, the number of visitors to individual website  pages.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>What we save:<br>- Internet Provider / IP address<br>Without exception any information we collect will be used solely for internal purposes. Personal data will only be collected by us from this Website if you voluntarily provide it or expressly give us your consent.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><code>[cookies_revoke]</code></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Website Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>On our website we use Google Analytics, a web analysis service of Google  Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States (“Google”). </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Our website is using the IP anonymizing function offered by  Google, which will delete the last 8 digits (type IPv4) or the last 80  bits (type IPv6) of your IP address. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You may revoke your consent to the use of web analysis at any time, either by downloading and installing the <a rel=\"noreferrer noopener\" href=\"https://tools.google.com/dlpage/gaoptout?hl=en-GB\" target=\"_blank\">provided Google Browser Plugin</a> or by administrating your consents via the following button \"revoke cookies\",  in which case the tracking is deactivated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> <code>[cookies_revoke]</code>  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Both options will  prevent the application of web analysis only as long as you use the  browser on which you installed the plugin and do not opt-in to the cookie banner. </p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-19 04:39:25', '2019-01-19 04:39:25', '', 5, 'https://picture.hungenbach.de/2019/01/19/5-revision-v1/', 0, 'revision', '', 0),
(80, 1, '2019-01-19 04:42:38', '2019-01-19 04:42:38', '<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Our website address is: http://localhost/picture_hungenbach/. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>In order to imprrove our Website, we collect some statistical  information, for example, the number of visitors to individual website  pages.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>What we save:<br>- Internet Provider / IP address<br>Without exception any information we collect will be used solely for internal purposes. Personal data will only be collected by us from this Website if you voluntarily provide it or expressly give us your consent.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><code>[cookies_revoke]</code></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Website Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>On our website we use Google Analytics, a web analysis service of Google  Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States (“Google”). </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Our website is using the IP anonymizing function offered by  Google, which will delete the last 8 digits (type IPv4) or the last 80  bits (type IPv6) of your IP address. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You may revoke your consent to the use of web analysis at any time, either by downloading and installing the <a rel=\"noreferrer noopener\" href=\"https://tools.google.com/dlpage/gaoptout?hl=en-GB\" target=\"_blank\">provided Google Browser Plugin</a> or by administrating your consents via the following button \"revoke cookies\",  in which case the tracking is deactivated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> <code>[cookies_revoke]</code>  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Both options will prevent the application of web analysis only as long as you use the browser on which you installed the plugin and do not consent to the tracking described on the cookie banner.</p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-19 04:42:38', '2019-01-19 04:42:38', '', 5, 'https://picture.hungenbach.de/2019/01/19/5-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(82, 1, '2019-01-19 04:54:18', '2019-01-19 04:54:18', '<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Our website address is: http://localhost/picture_hungenbach/. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>In order to imprrove our Website, we collect some statistical  information, for example, the number of visitors to individual website pages.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Setting of cookies</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>a) what are cookies</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>This website uses so-called “cookies”. Cookies are small text files that  are stored in the memory of your terminal via your browser. They store  certain information (e.g. your preferred language or site settings)  which your browser may (depending on the lifespan of the cookie)  retransmit to us upon your next visit to our Website. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>b) what cookies do we use?</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>The following tables contain a detailed description of the cookies we use: </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:table -->\n<table class=\"wp-block-table\"><tbody><tr><td><strong>Purpose and content </strong></td><td><strong>Expiration Time</strong></td></tr><tr><td>Website analysis with Google Analytics. <br>Cookies used to distinguish users. <br></td><td>2 years</td></tr><tr><td></td><td></td></tr><tr><td></td><td></td></tr><tr><td></td><td></td></tr></tbody></table>\n<!-- /wp:table -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Website Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>On our website we use Google Analytics, a web analysis service of Google  Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States (“Google”). </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Our website is using the IP anonymizing function offered by  Google, which will delete the last 8 digits (type IPv4) or the last 80  bits (type IPv6) of your IP address. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You may revoke your consent to the use of web analysis at any time, either by downloading and installing the <a rel=\"noreferrer noopener\" href=\"https://tools.google.com/dlpage/gaoptout?hl=en-GB\" target=\"_blank\">provided Google Browser Plugin</a> or by administrating your consents via the following button \"revoke cookies\",  in which case the tracking is deactivated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> <code>[cookies_revoke]</code>  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Both options will prevent the application of web analysis only as long as you use the browser on which you installed the plugin and do not consent to the tracking described on the cookie banner.</p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-19 04:54:18', '2019-01-19 04:54:18', '', 5, 'https://picture.hungenbach.de/2019/01/19/5-revision-v1/', 0, 'revision', '', 0),
(83, 1, '2019-01-19 04:54:39', '2019-01-19 04:54:39', '<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Our website address is: http://localhost/picture_hungenbach/. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>In order to imprrove our Website, we collect some statistical  information, for example, the number of visitors to individual website pages.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Setting of cookies</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>a) what are cookies</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>This website uses so-called “cookies”. Cookies are small text files that  are stored in the memory of your terminal via your browser. They store  certain information (e.g. your preferred language or site settings)  which your browser may (depending on the lifespan of the cookie)  retransmit to us upon your next visit to our Website. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>b) what cookies do we use?</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>The following tables contain a detailed description of the cookies we use: </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:table -->\n<table class=\"wp-block-table\"><tbody><tr><td><strong>Purpose and content </strong></td><td><strong>Expiration Time</strong></td></tr><tr><td>Website analysis with Google Analytics. <br>Cookies used to distinguish users. <br></td><td>2 years</td></tr></tbody></table>\n<!-- /wp:table -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Website Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>On our website we use Google Analytics, a web analysis service of Google  Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States (“Google”). </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Our website is using the IP anonymizing function offered by  Google, which will delete the last 8 digits (type IPv4) or the last 80  bits (type IPv6) of your IP address. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You may revoke your consent to the use of web analysis at any time, either by downloading and installing the <a rel=\"noreferrer noopener\" href=\"https://tools.google.com/dlpage/gaoptout?hl=en-GB\" target=\"_blank\">provided Google Browser Plugin</a> or by administrating your consents via the following button \"revoke cookies\",  in which case the tracking is deactivated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> <code>[cookies_revoke]</code>  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Both options will prevent the application of web analysis only as long as you use the browser on which you installed the plugin and do not consent to the tracking described on the cookie banner.</p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-19 04:54:39', '2019-01-19 04:54:39', '', 5, 'https://picture.hungenbach.de/2019/01/19/5-revision-v1/', 0, 'revision', '', 0),
(84, 1, '2019-01-19 04:54:54', '2019-01-19 04:54:54', '<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Our website address is: http://localhost/picture_hungenbach/. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>In order to imprrove our Website, we collect some statistical  information, for example, the number of visitors to individual website pages.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Setting of cookies</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>a) what are cookies</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>This website uses so-called “cookies”. Cookies are small text files that  are stored in the memory of your terminal via your browser. They store  certain information (e.g. your preferred language or site settings)  which your browser may (depending on the lifespan of the cookie)  retransmit to us upon your next visit to our Website. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>b) what cookies do we use?</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>The following tables contain a detailed description of the cookies we use: </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:table {\"hasFixedLayout\":true} -->\n<table class=\"wp-block-table has-fixed-layout\"><tbody><tr><td><strong>Purpose and content </strong></td><td><strong>Expiration Time</strong></td></tr><tr><td>Website analysis with Google Analytics. <br>Cookies used to distinguish users. <br></td><td>2 years</td></tr></tbody></table>\n<!-- /wp:table -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Website Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>On our website we use Google Analytics, a web analysis service of Google  Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States (“Google”). </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Our website is using the IP anonymizing function offered by  Google, which will delete the last 8 digits (type IPv4) or the last 80  bits (type IPv6) of your IP address. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You may revoke your consent to the use of web analysis at any time, either by downloading and installing the <a rel=\"noreferrer noopener\" href=\"https://tools.google.com/dlpage/gaoptout?hl=en-GB\" target=\"_blank\">provided Google Browser Plugin</a> or by administrating your consents via the following button \"revoke cookies\",  in which case the tracking is deactivated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> <code>[cookies_revoke]</code>  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Both options will prevent the application of web analysis only as long as you use the browser on which you installed the plugin and do not consent to the tracking described on the cookie banner.</p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-19 04:54:54', '2019-01-19 04:54:54', '', 5, 'https://picture.hungenbach.de/2019/01/19/5-revision-v1/', 0, 'revision', '', 0),
(85, 1, '2019-01-19 04:55:25', '2019-01-19 04:55:25', '<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Our website address is: http://localhost/picture_hungenbach/. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>In order to imprrove our Website, we collect some statistical  information, for example, the number of visitors to individual website pages.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Setting of cookies</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>a) what are cookies</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>This website uses so-called “cookies”. Cookies are small text files that  are stored in the memory of your terminal via your browser. They store  certain information (e.g. your preferred language or site settings)  which your browser may (depending on the lifespan of the cookie)  retransmit to us upon your next visit to our Website. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>b) what cookies do we use?</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>The following tables contain a detailed description of the cookies we use: </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:table {\"className\":\"is-style-stripes\"} -->\n<table class=\"wp-block-table is-style-stripes\"><tbody><tr><td><strong>Purpose and content </strong></td><td><strong>Expiration Time</strong></td></tr><tr><td>Website analysis with Google Analytics. <br>Cookies used to distinguish users. <br></td><td>2 years</td></tr></tbody></table>\n<!-- /wp:table -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Website Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>On our website we use Google Analytics, a web analysis service of Google  Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States (“Google”). </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Our website is using the IP anonymizing function offered by  Google, which will delete the last 8 digits (type IPv4) or the last 80  bits (type IPv6) of your IP address. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You may revoke your consent to the use of web analysis at any time, either by downloading and installing the <a rel=\"noreferrer noopener\" href=\"https://tools.google.com/dlpage/gaoptout?hl=en-GB\" target=\"_blank\">provided Google Browser Plugin</a> or by administrating your consents via the following button \"revoke cookies\",  in which case the tracking is deactivated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> <code>[cookies_revoke]</code>  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Both options will prevent the application of web analysis only as long as you use the browser on which you installed the plugin and do not consent to the tracking described on the cookie banner.</p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-19 04:55:25', '2019-01-19 04:55:25', '', 5, 'https://picture.hungenbach.de/2019/01/19/5-revision-v1/', 0, 'revision', '', 0),
(86, 1, '2019-01-19 04:55:50', '2019-01-19 04:55:50', '<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Our website address is: http://localhost/picture_hungenbach/. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>In order to imprrove our Website, we collect some statistical  information, for example, the number of visitors to individual website pages.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Setting of cookies</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>a) what are cookies</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>This website uses so-called “cookies”. Cookies are small text files that  are stored in the memory of your terminal via your browser. They store  certain information (e.g. your preferred language or site settings)  which your browser may (depending on the lifespan of the cookie)  retransmit to us upon your next visit to our Website. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>b) what cookies do we use?</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>The following table contains a description of the cookies we use:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:table {\"className\":\"is-style-stripes\"} -->\n<table class=\"wp-block-table is-style-stripes\"><tbody><tr><td><strong>Purpose and content </strong></td><td><strong>Expiration Time</strong></td></tr><tr><td>Website analysis with Google Analytics. <br>Cookies used to distinguish users. <br></td><td>2 years</td></tr></tbody></table>\n<!-- /wp:table -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Website Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>On our website we use Google Analytics, a web analysis service of Google  Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States (“Google”). </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Our website is using the IP anonymizing function offered by  Google, which will delete the last 8 digits (type IPv4) or the last 80  bits (type IPv6) of your IP address. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You may revoke your consent to the use of web analysis at any time, either by downloading and installing the <a rel=\"noreferrer noopener\" href=\"https://tools.google.com/dlpage/gaoptout?hl=en-GB\" target=\"_blank\">provided Google Browser Plugin</a> or by administrating your consents via the following button \"revoke cookies\",  in which case the tracking is deactivated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> <code>[cookies_revoke]</code>  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Both options will prevent the application of web analysis only as long as you use the browser on which you installed the plugin and do not consent to the tracking described on the cookie banner.</p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-19 04:55:50', '2019-01-19 04:55:50', '', 5, 'https://picture.hungenbach.de/2019/01/19/5-revision-v1/', 0, 'revision', '', 0),
(88, 1, '2019-01-19 04:57:11', '2019-01-19 04:57:11', '<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Our website address is: http://localhost/picture_hungenbach/. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>In order to imprrove our Website, we collect some statistical  information, for example, the number of visitors to individual website pages.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Setting of cookies</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>a) what are cookies</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>This website uses so-called “cookies”. Cookies are small text files that are stored via your browser. They store certain information (e.g. site settings) which your browser may (depending on the lifespan of the cookie) retransmit to us upon your next visit to our Website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>b) what cookies do we use?</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>The following table contains a description of the cookies we use:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:table {\"className\":\"is-style-stripes\"} -->\n<table class=\"wp-block-table is-style-stripes\"><tbody><tr><td><strong>Purpose and content </strong></td><td><strong>Expiration Time</strong></td></tr><tr><td>Website analysis with Google Analytics. <br>Cookies used to distinguish users. <br></td><td>2 years</td></tr></tbody></table>\n<!-- /wp:table -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Website Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>On our website we use Google Analytics, a web analysis service of Google  Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States (“Google”). </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Our website is using the IP anonymizing function offered by  Google, which will delete the last 8 digits (type IPv4) or the last 80  bits (type IPv6) of your IP address. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You may revoke your consent to the use of web analysis at any time, either by downloading and installing the <a rel=\"noreferrer noopener\" href=\"https://tools.google.com/dlpage/gaoptout?hl=en-GB\" target=\"_blank\">provided Google Browser Plugin</a> or by administrating your consents via the following button \"revoke cookies\",  in which case the tracking is deactivated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> <code>[cookies_revoke]</code>  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Both options will prevent the application of web analysis only as long as you use the browser on which you installed the plugin and do not consent to the tracking described on the cookie banner.</p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-19 04:57:11', '2019-01-19 04:57:11', '', 5, 'https://picture.hungenbach.de/2019/01/19/5-revision-v1/', 0, 'revision', '', 0),
(89, 1, '2019-01-19 04:58:11', '2019-01-19 04:58:11', '<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Our website address is: http://localhost/picture_hungenbach/. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>In order to imprrove our Website, we collect some statistical  information, for example, the number of visitors to individual website pages.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Setting of cookies</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>a) what are cookies</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>This website uses so-called “cookies”. Cookies are small text files that are stored via your browser. They store certain information (e.g. site settings) which your browser may (depending on the lifespan of the cookie) retransmit to us upon your next visit to our Website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>b) what cookies do we use?</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>The following table contains a description of the cookies we use:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:table {\"className\":\"is-style-stripes\"} -->\n<table class=\"wp-block-table is-style-stripes\"><tbody><tr><td><strong>Purpose and content </strong></td><td><strong>Expiration Time</strong></td></tr><tr><td>Website analysis with Google Analytics. <br>Cookies used to distinguish users. <br></td><td>2 years</td></tr></tbody></table>\n<!-- /wp:table -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Website Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>On our website we use Google Analytics, a web analysis service of Google  Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States (“Google”). </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Our website is using the IP anonymizing function offered by  Google, which will delete the last 8 digits (type IPv4) or the last 80  bits (type IPv6) of your IP address. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You may revoke your consent to the use of web analysis at any time, either by downloading and installing the <a rel=\"noreferrer noopener\" href=\"https://tools.google.com/dlpage/gaoptout?hl=en-GB\" target=\"_blank\">provided Google Browser Plugin</a> or by administrating your consents via the following button \"revoke cookies\" (press button \"No\" on the cookie banner), in which case the tracking is deactivated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> <code>[cookies_revoke]</code>  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Both options will prevent the application of web analysis only as long as you use the browser on which you installed the plugin and do not consent to the tracking described on the cookie banner.</p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-19 04:58:11', '2019-01-19 04:58:11', '', 5, 'https://picture.hungenbach.de/2019/01/19/5-revision-v1/', 0, 'revision', '', 0),
(90, 1, '2019-01-19 04:58:29', '2019-01-19 04:58:29', '<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Our website address is: http://localhost/picture_hungenbach/. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>In order to imprrove our Website, we collect some statistical  information, for example, the number of visitors to individual website pages.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Setting of cookies</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>a) what are cookies</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>This website uses so-called “cookies”. Cookies are small text files that are stored via your browser. They store certain information (e.g. site settings) which your browser may (depending on the lifespan of the cookie) retransmit to us upon your next visit to our Website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>b) what cookies do we use?</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>The following table contains a description of the cookies we use:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:table {\"className\":\"is-style-stripes\"} -->\n<table class=\"wp-block-table is-style-stripes\"><tbody><tr><td><strong>Purpose and content </strong></td><td><strong>Expiration Time</strong></td></tr><tr><td>Website analysis with Google Analytics. <br>Cookies used to distinguish users. <br></td><td>2 years</td></tr></tbody></table>\n<!-- /wp:table -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Website Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>On our website we use Google Analytics, a web analysis service of Google  Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States (“Google”). </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Our website is using the IP anonymizing function offered by  Google, which will delete the last 8 digits (type IPv4) or the last 80  bits (type IPv6) of your IP address. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You may revoke your consent to the use of web analysis at any time, either by downloading and installing the <a rel=\"noreferrer noopener\" href=\"https://tools.google.com/dlpage/gaoptout?hl=en-GB\" target=\"_blank\">provided Google Browser Plugin</a> or by administrating your consents via the following button \"revoke cookies\" (press button \"No\" on the cookie banner), in which case the tracking is deactivated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> <code>[cookies_revoke]</code>  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Both options will prevent the application of web analysis only as long as you use the browser on which you installed the plugin and do not consent  (press button \"Yes\" on the cookie banner) to the tracking described on the cookie banner.</p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-19 04:58:29', '2019-01-19 04:58:29', '', 5, 'https://picture.hungenbach.de/2019/01/19/5-revision-v1/', 0, 'revision', '', 0),
(91, 1, '2019-01-19 04:58:50', '2019-01-19 04:58:50', '<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Our website address is: http://localhost/picture_hungenbach/. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>In order to imprrove our Website, we collect some statistical  information, for example, the number of visitors to individual website pages.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Setting of cookies</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>a) what are cookies</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>This website uses so-called “cookies”. Cookies are small text files that are stored via your browser. They store certain information (e.g. site settings) which your browser may (depending on the lifespan of the cookie) retransmit to us upon your next visit to our Website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>b) what cookies do we use?</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>The following table contains a description of the cookies we use:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:table {\"className\":\"is-style-stripes\"} -->\n<table class=\"wp-block-table is-style-stripes\"><tbody><tr><td><strong>Purpose and content </strong></td><td><strong>Expiration Time</strong></td></tr><tr><td>Website analysis with Google Analytics. <br>Cookies used to distinguish users. <br></td><td>2 years</td></tr></tbody></table>\n<!-- /wp:table -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Website Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>On our website we use Google Analytics, a web analysis service of Google  Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States (“Google”). </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Our website is using the IP anonymizing function offered by  Google, which will delete the last 8 digits (type IPv4) or the last 80  bits (type IPv6) of your IP address. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You may revoke your consent to the use of web analysis at any time, either by downloading and installing the <a rel=\"noreferrer noopener\" href=\"https://tools.google.com/dlpage/gaoptout?hl=en-GB\" target=\"_blank\">provided Google Browser Plugin</a> or by administrating your consents via the following button \"revoke cookies\" (press button \"No\" on the cookie banner), in which case the tracking is deactivated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> <code>[cookies_revoke]</code>  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Both options will prevent the application of web analysis only as long as you use the browser on which you installed the plugin and do not consent (press button \"Ok\" on the cookie banner) to the tracking described on the cookie banner.</p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-19 04:58:50', '2019-01-19 04:58:50', '', 5, 'https://picture.hungenbach.de/2019/01/19/5-revision-v1/', 0, 'revision', '', 0),
(93, 1, '2019-01-19 05:08:24', '2019-01-19 05:08:24', '<!-- wp:paragraph -->\n<p><strong>Effective Jan 19, 2019 </strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Our website address is: http://localhost/picture_hungenbach/. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>In order to imprrove our Website, we collect some statistical  information, for example, the number of visitors to individual website pages.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Setting of cookies</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>a) what are cookies</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>This website uses so-called “cookies”. Cookies are small text files that are stored via your browser. They store certain information (e.g. site settings) which your browser may (depending on the lifespan of the cookie) retransmit to us upon your next visit to our Website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>b) what cookies do we use?</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>The following table contains a description of the cookies we use:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:table {\"className\":\"is-style-stripes\"} -->\n<table class=\"wp-block-table is-style-stripes\"><tbody><tr><td><strong>Purpose and content </strong></td><td><strong>Expiration Time</strong></td></tr><tr><td>Website analysis with Google Analytics. <br>Cookies used to distinguish users. <br></td><td>2 years</td></tr></tbody></table>\n<!-- /wp:table -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Website Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>On our website we use Google Analytics, a web analysis service of Google  Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States (“Google”). </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Our website is using the IP anonymizing function offered by  Google, which will delete the last 8 digits (type IPv4) or the last 80  bits (type IPv6) of your IP address. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You may revoke your consent to the use of web analysis at any time, either by downloading and installing the <a rel=\"noreferrer noopener\" href=\"https://tools.google.com/dlpage/gaoptout?hl=en-GB\" target=\"_blank\">provided Google Browser Plugin</a> or by administrating your consents via the following button \"revoke cookies\" (press button \"No\" on the cookie banner), in which case the tracking is deactivated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> <code>[cookies_revoke]</code>  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Both options will prevent the application of web analysis only as long as you use the browser on which you installed the plugin and do not consent (press button \"Ok\" on the cookie banner) to the tracking described on the cookie banner.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>This privacy statement is updated from time to time. You will find the date of the latest update at the top of this page. </p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-19 05:08:24', '2019-01-19 05:08:24', '', 5, 'https://picture.hungenbach.de/2019/01/19/5-revision-v1/', 0, 'revision', '', 0),
(94, 1, '2019-01-19 05:09:24', '2019-01-19 05:09:24', '<!-- wp:paragraph -->\n<p><strong>Effective Jan 19, 2019 </strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Our website address is: http://localhost/picture_hungenbach/. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>In order to imprrove our Website, we collect some statistical  information, for example, the number of visitors to individual website pages.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Setting of cookies</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>a) what are cookies</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>This website uses so-called “cookies”. Cookies are small text files that are stored via your browser. They store certain information (e.g. site settings) which your browser may (depending on the lifespan of the cookie) retransmit to us upon your next visit to our Website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>b) what cookies do we use?</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>The following table contains a description of the cookies we use:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:table {\"className\":\"is-style-stripes\"} -->\n<table class=\"wp-block-table is-style-stripes\"><tbody><tr><td><strong>Purpose and content </strong></td><td><strong>Expiration Time</strong></td></tr><tr><td>Website analysis with Google Analytics. <br>Cookies used to distinguish users. <br></td><td>2 years</td></tr></tbody></table>\n<!-- /wp:table -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Website Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>On our website we use Google Analytics, a web analysis service of Google  Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States (“Google”). </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Our website is using the IP anonymizing function offered by  Google, which will delete the last 8 digits (type IPv4) or the last 80  bits (type IPv6) of your IP address. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You may revoke your consent to the use of web analysis at any time, either by downloading and installing the <a rel=\"noreferrer noopener\" href=\"https://tools.google.com/dlpage/gaoptout?hl=en-GB\" target=\"_blank\">provided Google Browser Plugin</a> or by administrating your consents via the following button \"revoke cookies\" (press button \"No\" on the cookie banner), in which case the tracking is deactivated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> <code>[cookies_revoke]</code>  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Both options will prevent the application of web analysis only as long as you use the browser on which you installed the plugin and do not consent (press button \"Ok\" on the cookie banner) to the tracking described on the cookie banner.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Updates to our privacy statement</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>This privacy statement is updated from time to time. You will find the date of the latest update at the top of this page.</p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-19 05:09:24', '2019-01-19 05:09:24', '', 5, 'https://picture.hungenbach.de/2019/01/19/5-revision-v1/', 0, 'revision', '', 0),
(96, 1, '2019-01-19 05:23:43', '2019-01-19 05:23:43', '<!-- wp:paragraph -->\n<p><strong>Effective Jan 19, 2019 </strong></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Who we are</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Our website address is: https://picture.hungenbach.de.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Your privacy is very important to us. Accordingly, we have developed this Policy in order for you to understand how we collect, use, communicate and disclose and make use of personal information. The following outlines our privacy policy.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3 id=\"mce_14\">What personal data we collect and why we collect it</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>In order to improve our Website, we collect some statistical  information, for example, the number of visitors to individual website pages.  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Setting of cookies</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>a) what are cookies</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>This website uses so-called “cookies”. Cookies are small text files that are stored on your terminal device. They store certain information (e.g. site settings) which your browser may (depending on the lifespan of the cookie) retransmit to us upon your next visit to our Website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":4} -->\n<h4>b) what cookies do we use?</h4>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>The following table contains a description of the cookies we use:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:table {\"className\":\"is-style-stripes\"} -->\n<table class=\"wp-block-table is-style-stripes\"><tbody><tr><td><strong>Purpose and content </strong></td><td><strong>Expiration Time</strong></td></tr><tr><td>Website analysis with Google Analytics. <br>Cookies used to distinguish users. <br></td><td>2 years</td></tr></tbody></table>\n<!-- /wp:table -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Media</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>If you upload images to the website, you should avoid  uploading images with embedded location data (EXIF GPS) included.  Visitors to the website can download and extract any location data from  images on the website.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Website Analytics</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>On our website we use Google Analytics, a web analysis service of Google  Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, United States (“Google”). </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Google Analytics is a web  analytics service offered by Google that tracks and reports website  traffic. Google uses the data collected to track and monitor the use of  our Service. This data is shared with other Google services. Google may  use the collected data to contextualize and personalize the ads of its  own advertising network.                         </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Our website is using the IP anonymizing function offered by  Google, which will delete the last 8 digits (type IPv4) or the last 80  bits (type IPv6) of your IP address. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>You may revoke your consent to the use of web analysis at any time, either by downloading and installing the <a rel=\"noreferrer noopener\" href=\"https://tools.google.com/dlpage/gaoptout?hl=en-GB\" target=\"_blank\">provided Google Browser Plugin</a> or by administrating your consents via the following button \"revoke cookies\" (press button \"No\" on the cookie banner), in which case the tracking is deactivated.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> <code>[cookies_revoke]</code>  </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Both options will prevent the application of web analysis only as long as you use the browser on which you installed the plugin and do not consent (press button \"Ok\" on the cookie banner) to the tracking described on the cookie banner.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph {\"align\":\"left\"} -->\n<p style=\"text-align:left\">Further information on Google Analytics is available in the <a rel=\"noreferrer noopener\" href=\"https://www.google.com/analytics/terms/us.html\" target=\"_blank\">Google Analytics Terms of Use</a>, the <a rel=\"noreferrer noopener\" href=\"https://support.google.com/analytics/answer/6004245?hl=en-GB\" target=\"_blank\">Privacy and Data Protection Guidelines of Google Analytics</a> and in the <a rel=\"noreferrer noopener\" href=\"http://www.google.com/intl/en/policies/privacy\" target=\"_blank\">Google Privacy Policy</a>.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Updates to our privacy statement</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>This privacy statement is updated from time to time. You will find the date of the latest update at the top of this page.</p>\n<!-- /wp:paragraph -->', 'Privacy Statement', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-01-19 05:23:43', '2019-01-19 05:23:43', '', 5, 'https://picture.hungenbach.de/2019/01/19/5-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(97, 1, '2019-01-19 05:25:03', '2019-01-19 05:25:03', '<!-- wp:heading {\"level\":3} -->\n<h3>Published by:</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Markus Hungenbach<br>Hardt 36<br>D-51429 Bergisch Gladbach<br>Germany<br> <br>E-mail: Inquiries can be sent to webmaster (at) hungenbach.de or using our contact form.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Last updated: Jan 19, 2019 </p>\n<!-- /wp:paragraph -->', 'Imprint', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2019-01-19 05:25:03', '2019-01-19 05:25:03', '', 14, 'https://picture.hungenbach.de/2019/01/19/14-revision-v1/', 0, 'revision', '', 0),
(98, 1, '2019-01-19 05:25:50', '2019-01-19 05:25:50', '<!-- wp:heading {\"level\":3} -->\n<h3>1. Terms</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>By accessing this website, you are agreeing to be bound by these website Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this website. The materials contained in this website are protected by applicable copyright and trade mark law. This website has been developed by Markus  Hungenbach and is administered  by the same. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>2. Use License</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Permission is granted to temporarily download one copy of the materials (information or software) on Markus Hungenbach’s website for personal, non-commercial transitory viewing only. This is the grant of a license,  not a transfer of title, and under this license you may not:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>modify or copy the materials;</li><li>use the materials for any commercial purpose, or for any public display (commercial or non-commercial);</li><li>attempt to decompile or reverse engineer any software contained on Markus Hungenbach’s website;</li><li>remove any copyright or other proprietary notations from the materials; or</li><li>transfer the materials to another person or “mirror” the materials on any other server.</li></ul>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<p>This license shall automatically terminate if you violate any of  these restrictions and may be terminated by Markus Hungenbach at any time. Upon terminating your viewing of these materials or upon the termination of this license, you must destroy any downloaded materials in your possession whether in electronic or printed format.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>3. Disclaimer</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>The materials on Markus Hungenbach’s website are provided “as is”. Markus Hungenbach makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, Markus Hungenbach does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its website or otherwise relating to such materials or on any websites linked to this website. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>4. Links</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Markus Hungenbach has not reviewed all of the websites linked to its website and is not responsible for the contents of any such linked websitesite. The inclusion of any link does not imply endorsement by Markus Hungenbach of the website. Use of any such linked web site is at the user’s own risk. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>5. Site Terms of Use Modifications</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Markus Hungenbach may revise these terms of use for its website at any time without notice. By using this website you are agreeing to be bound by the then current version of these Terms and Conditions of Use. </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Last updated: Jan 19, 2019 </p>\n<!-- /wp:paragraph -->', 'Terms and Conditions of Use', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2019-01-19 05:25:50', '2019-01-19 05:25:50', '', 17, 'https://picture.hungenbach.de/2019/01/19/17-revision-v1/', 0, 'revision', '', 0),
(99, 1, '2019-01-19 05:27:06', '2019-01-19 05:27:06', '<!-- wp:heading {\"level\":3} -->\n<h3>Published by:</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Markus Hungenbach<br>Hardt 36<br>D-51429 Bergisch Gladbach<br>Germany<br> <br>E-mail: Inquiries can be sent to webmaster (at) hungenbach.de or using our <a href=\"https://picture.hungenbach.de/contact/\">contact form</a>.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Last updated: Jan 19, 2019 </p>\n<!-- /wp:paragraph -->', 'Imprint', '', 'inherit', 'closed', 'closed', '', '14-revision-v1', '', '', '2019-01-19 05:27:06', '2019-01-19 05:27:06', '', 14, 'https://picture.hungenbach.de/2019/01/19/14-revision-v1/', 0, 'revision', '', 0),
(105, 1, '2019-01-26 04:31:01', '2019-01-26 04:31:01', '', 'Creative Hungenbach Dinosaur', 'Creative Hungenbach Dinosaur', 'inherit', 'closed', 'closed', '', 'background_dino', '', '', '2019-01-26 04:31:58', '2019-01-26 04:31:58', '', 0, 'https://picture.hungenbach.de/wp-content/uploads/2019/01/background_dino.jpg', 0, 'attachment', 'image/jpeg', 0),
(110, 1, '2019-01-26 04:40:58', '2019-01-26 04:40:58', '<!-- wp:paragraph -->\n<p>Welcome to creative. This is to be yourself. Register and then start!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2019-01-26 04:40:58', '2019-01-26 04:40:58', '', 1, 'https://picture.hungenbach.de/2019/01/26/1-revision-v1/', 0, 'revision', '', 0),
(114, 1, '2019-01-26 04:48:38', '2019-01-26 04:48:38', '<!-- wp:paragraph -->\n<p>Welcome to creative. This is to be yourself. Register and then start!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Photo by <a href=\"https://unsplash.com/photos/u9skEqysM1g?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText\">Annie Spratt</a> on <a href=\"https://unsplash.com/search/photos/birthday-cake?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText\">Unsplash</a></p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2019-01-26 04:48:38', '2019-01-26 04:48:38', '', 1, 'https://picture.hungenbach.de/2019/01/26/1-revision-v1/', 0, 'revision', '', 0),
(115, 1, '2019-01-26 04:50:30', '2019-01-26 04:50:30', '', 'birthday cake', '', 'inherit', 'closed', 'closed', '', 'christopher-martyn-715646-unsplash', '', '', '2019-01-26 05:21:37', '2019-01-26 05:21:37', '', 1, 'https://picture.hungenbach.de/wp-content/uploads/2019/01/christopher-martyn-715646-unsplash.jpg', 0, 'attachment', 'image/jpeg', 0),
(116, 1, '2019-01-26 04:50:51', '2019-01-26 04:50:51', '<!-- wp:paragraph -->\n<p>Welcome to creative. This is to be yourself. Register and then start!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Photo by <a href=\"https://unsplash.com/photos/_-ZzWO4jlRE?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText\">Christopher Martyn</a> on <a href=\"https://unsplash.com/search/photos/birthday-cake?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText\">Unsplash</a></p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2019-01-26 04:50:51', '2019-01-26 04:50:51', '', 1, 'https://picture.hungenbach.de/2019/01/26/1-revision-v1/', 0, 'revision', '', 0),
(117, 1, '2019-01-26 05:22:42', '2019-01-26 05:22:42', '<!-- wp:paragraph -->\n<p>Welcome to creative. This is to be yourself. Register and then start!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2019-01-26 05:22:42', '2019-01-26 05:22:42', '', 1, 'https://picture.hungenbach.de/2019/01/26/1-revision-v1/', 0, 'revision', '', 0),
(130, 1, '2019-01-27 06:10:31', '2019-01-27 06:10:31', '<!-- wp:paragraph -->\n<p>Download drawing templates of cars and buildings. Print out the template and draw from your imagination. Let your artwork come alive.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 1: Print your favorite template</h3>\n<!-- /wp:heading -->\n\n<!-- wp:file {\"id\":204,\"href\":\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_car_small.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_car_small.pdf\">① Car Small</a><a href=\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_car_small.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":203,\"href\":\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_bus.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_bus.pdf\">② Bus</a><a href=\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_bus.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":205,\"href\":\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_house_big.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_house_big.pdf\">③ House Big</a><a href=\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_house_big.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":206,\"href\":\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_house_small.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_house_small.pdf\">④ House Small</a><a href=\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_house_small.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 2: Upload</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Register or login to upload and manage your drawings <a href=\"https://picture.hungenbach.de/rm_login/\">here</a>.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 3: Bring it alive</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Watch your drawings move on a canvas <a href=\"https://picture.hungenbach.de/picture-town-map/\">here</a>.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><br></p>\n<!-- /wp:paragraph -->', 'Picture Town', '', 'publish', 'closed', 'closed', '', 'picture-town', '', '', '2019-07-25 11:07:56', '2019-07-25 11:07:56', '', 0, 'https://picture.hungenbach.de/?page_id=130', 0, 'page', '', 0),
(131, 1, '2019-01-27 06:10:31', '2019-01-27 06:10:31', '<!-- wp:paragraph -->\n<p>Download drawing templates of cars and buildings. Print the template and draw from your imagination. After the drawing is photographed it becomes alive and enters a virtual townmap.</p>\n<!-- /wp:paragraph -->', 'Picture Town', '', 'inherit', 'closed', 'closed', '', '130-revision-v1', '', '', '2019-01-27 06:10:31', '2019-01-27 06:10:31', '', 130, 'https://picture.hungenbach.de/2019/01/27/130-revision-v1/', 0, 'revision', '', 0),
(134, 1, '2019-01-27 06:13:02', '2019-01-27 06:13:02', '<!-- wp:paragraph -->\n<p>Welcome to creative. This is to be yourself. Register and then start!</p>\n<!-- /wp:paragraph -->', 'We are live!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2019-01-27 06:13:02', '2019-01-27 06:13:02', '', 1, 'https://picture.hungenbach.de/2019/01/27/1-revision-v1/', 0, 'revision', '', 0),
(136, 1, '2019-01-27 06:14:26', '2019-01-27 06:14:26', '<!-- wp:paragraph -->\n<p>Download drawing templates of cars and buildings. Print out the template and draw from your imagination. Take a photo and it will comes alive and enter a virtual townmap.</p>\n<!-- /wp:paragraph -->', 'Picture Town', '', 'inherit', 'closed', 'closed', '', '130-revision-v1', '', '', '2019-01-27 06:14:26', '2019-01-27 06:14:26', '', 130, 'https://picture.hungenbach.de/2019/01/27/130-revision-v1/', 0, 'revision', '', 0),
(137, 1, '2019-01-27 06:14:51', '2019-01-27 06:14:51', '<!-- wp:paragraph -->\n<p>Download drawing templates of cars and buildings. Print out the template and draw from your imagination. Take a photo and it will come alive and enter a virtual townmap.</p>\n<!-- /wp:paragraph -->', 'Picture Town', '', 'inherit', 'closed', 'closed', '', '130-revision-v1', '', '', '2019-01-27 06:14:51', '2019-01-27 06:14:51', '', 130, 'https://picture.hungenbach.de/2019/01/27/130-revision-v1/', 0, 'revision', '', 0),
(142, 1, '2019-01-27 11:00:40', '2019-01-27 11:00:40', '<!-- wp:paragraph -->\n<p>[customized_map_3D] </p>\n<!-- /wp:paragraph -->', 'Picture Town Map', '', 'private', 'closed', 'closed', '', 'picture-town-map', '', '', '2019-07-28 07:07:07', '2019-07-28 07:07:07', '', 0, 'https://picture.hungenbach.de/?page_id=142', 0, 'page', '', 0),
(143, 1, '2019-01-27 11:00:40', '2019-01-27 11:00:40', '<!-- wp:paragraph -->\n<p>Here Google Maps will be displayed</p>\n<!-- /wp:paragraph -->', 'Picture Town Map', '', 'inherit', 'closed', 'closed', '', '142-revision-v1', '', '', '2019-01-27 11:00:40', '2019-01-27 11:00:40', '', 142, 'https://picture.hungenbach.de/2019/01/27/142-revision-v1/', 0, 'revision', '', 0),
(144, 1, '2019-01-27 11:29:34', '2019-01-27 11:29:34', '<!-- wp:paragraph -->\n<p>[wpgmza id=\"1\"]</p>\n<!-- /wp:paragraph -->', 'Picture Town Map', '', 'inherit', 'closed', 'closed', '', '142-revision-v1', '', '', '2019-01-27 11:29:34', '2019-01-27 11:29:34', '', 142, 'https://picture.hungenbach.de/2019/01/27/142-revision-v1/', 0, 'revision', '', 0),
(203, 1, '2019-07-25 11:06:32', '2019-07-25 11:06:32', '', '② Bus', '', 'inherit', 'closed', 'closed', '', 'template_bus', '', '', '2019-07-25 11:07:23', '2019-07-25 11:07:23', '', 0, 'http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_bus.pdf', 0, 'attachment', 'application/pdf', 0),
(204, 1, '2019-07-25 11:06:33', '2019-07-25 11:06:33', '', '① Car Small', '', 'inherit', 'closed', 'closed', '', 'template_car_small', '', '', '2019-07-25 11:07:12', '2019-07-25 11:07:12', '', 0, 'http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_car_small.pdf', 0, 'attachment', 'application/pdf', 0),
(205, 1, '2019-07-25 11:06:34', '2019-07-25 11:06:34', '', '③ House Big', '', 'inherit', 'closed', 'closed', '', 'template_house_big', '', '', '2019-07-25 11:07:36', '2019-07-25 11:07:36', '', 0, 'http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_house_big.pdf', 0, 'attachment', 'application/pdf', 0),
(206, 1, '2019-07-25 11:06:35', '2019-07-25 11:06:35', '', '④ House Small', '', 'inherit', 'closed', 'closed', '', 'template_house_small', '', '', '2019-07-25 11:07:52', '2019-07-25 11:07:52', '', 0, 'http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_house_small.pdf', 0, 'attachment', 'application/pdf', 0),
(150, 1, '2019-02-02 04:16:54', '2019-02-02 04:16:54', '', 'thumbnail_bus', 'Bus', 'inherit', 'closed', 'closed', '', 'thumbnail_bus', '', '', '2019-02-02 04:21:22', '2019-02-02 04:21:22', '', 0, 'https://picture.hungenbach.de/wp-content/uploads/2019/02/thumbnail_bus.png', 0, 'attachment', 'image/png', 0),
(151, 1, '2019-02-02 04:16:55', '2019-02-02 04:16:55', '', 'thumbnail_car_small', 'Car Small', 'inherit', 'closed', 'closed', '', 'thumbnail_car_small', '', '', '2019-02-02 04:21:19', '2019-02-02 04:21:19', '', 0, 'https://picture.hungenbach.de/wp-content/uploads/2019/02/thumbnail_car_small.png', 0, 'attachment', 'image/png', 0),
(152, 1, '2019-02-02 04:16:56', '2019-02-02 04:16:56', '', 'thumbnail_house_big', 'House Big', 'inherit', 'closed', 'closed', '', 'thumbnail_house_big', '', '', '2019-02-02 04:21:13', '2019-02-02 04:21:13', '', 0, 'https://picture.hungenbach.de/wp-content/uploads/2019/02/thumbnail_house_big.png', 0, 'attachment', 'image/png', 0),
(153, 1, '2019-02-02 04:16:57', '2019-02-02 04:16:57', '', 'thumbnail_house_small', 'House Small', 'inherit', 'closed', 'closed', '', 'thumbnail_house_small', '', '', '2019-02-02 04:21:16', '2019-02-02 04:21:16', '', 0, 'https://picture.hungenbach.de/wp-content/uploads/2019/02/thumbnail_house_small.png', 0, 'attachment', 'image/png', 0),
(155, 1, '2019-02-02 04:21:33', '2019-02-02 04:21:33', '<!-- wp:paragraph -->\n<p>Download drawing templates of cars and buildings. Print out the template and draw from your imagination. Let your artwork come alive.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Step 1: Download</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:gallery {\"ids\":[153,152,151,150]} -->\n<ul class=\"wp-block-gallery columns-3 is-cropped\"><li class=\"blocks-gallery-item\"><figure><img src=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/thumbnail_house_small-1024x723.png\" alt=\"\" data-id=\"153\" data-link=\"https://picture.hungenbach.de/thumbnail_house_small/\" class=\"wp-image-153\"/><figcaption>House Small</figcaption></figure></li><li class=\"blocks-gallery-item\"><figure><img src=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/thumbnail_house_big.png\" alt=\"\" data-id=\"152\" data-link=\"https://picture.hungenbach.de/thumbnail_house_big/\" class=\"wp-image-152\"/><figcaption>House Big</figcaption></figure></li><li class=\"blocks-gallery-item\"><figure><img src=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/thumbnail_car_small-1024x726.png\" alt=\"\" data-id=\"151\" data-link=\"https://picture.hungenbach.de/thumbnail_car_small/\" class=\"wp-image-151\"/><figcaption>Car Small</figcaption></figure></li><li class=\"blocks-gallery-item\"><figure><img src=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/thumbnail_bus-1024x730.png\" alt=\"\" data-id=\"150\" data-link=\"https://picture.hungenbach.de/thumbnail_bus/\" class=\"wp-image-150\"/><figcaption>Bus</figcaption></figure></li></ul>\n<!-- /wp:gallery -->\n\n<!-- wp:paragraph -->\n<p>Step 2: Upload</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Step 3: Watch your artwork<br></p>\n<!-- /wp:paragraph -->', 'Picture Town', '', 'inherit', 'closed', 'closed', '', '130-revision-v1', '', '', '2019-02-02 04:21:33', '2019-02-02 04:21:33', '', 130, 'https://picture.hungenbach.de/2019/02/02/130-revision-v1/', 0, 'revision', '', 0),
(157, 1, '2019-02-02 04:22:31', '2019-02-02 04:22:31', '', 'thumbnail_car_small', '', 'inherit', 'closed', 'closed', '', 'thumbnail_car_small-2', '', '', '2019-02-02 04:22:31', '2019-02-02 04:22:31', '', 130, 'https://picture.hungenbach.de/wp-content/uploads/2019/02/thumbnail_car_small-1.png', 0, 'attachment', 'image/png', 0),
(158, 1, '2019-02-02 04:22:44', '2019-02-02 04:22:44', '<!-- wp:paragraph -->\n<p>Download drawing templates of cars and buildings. Print out the template and draw from your imagination. Let your artwork come alive.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Step 1: Download</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:media-text {\"mediaId\":157,\"mediaType\":\"image\"} -->\n<div class=\"wp-block-media-text alignwide\"><figure class=\"wp-block-media-text__media\"><img src=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/thumbnail_car_small-1-1024x726.png\" alt=\"\" class=\"wp-image-157\"/></figure><div class=\"wp-block-media-text__content\"><!-- wp:paragraph {\"placeholder\":\"Content…\",\"fontSize\":\"large\"} -->\n<p class=\"has-large-font-size\">Small Car</p>\n<!-- /wp:paragraph --></div></div>\n<!-- /wp:media-text -->\n\n<!-- wp:paragraph -->\n<p>Step 2: Upload</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Step 3: Watch your artwork<br></p>\n<!-- /wp:paragraph -->', 'Picture Town', '', 'inherit', 'closed', 'closed', '', '130-revision-v1', '', '', '2019-02-02 04:22:44', '2019-02-02 04:22:44', '', 130, 'https://picture.hungenbach.de/2019/02/02/130-revision-v1/', 0, 'revision', '', 0),
(160, 1, '2019-02-02 04:27:39', '2019-02-02 04:27:39', '<!-- wp:paragraph -->\n<p>Download drawing templates of cars and buildings. Print out the template and draw from your imagination. Let your artwork come alive.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Step 1: DownloadHouse Small</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:columns {\"columns\":3} -->\n<div class=\"wp-block-columns has-3-columns\"><!-- wp:column -->\n<div class=\"wp-block-column\"></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"></div>\n<!-- /wp:column -->\n\n<!-- wp:column -->\n<div class=\"wp-block-column\"></div>\n<!-- /wp:column --></div>\n<!-- /wp:columns -->\n\n<!-- wp:image {\"id\":157} -->\n<figure class=\"wp-block-image\"><img src=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/thumbnail_car_small-1-150x150.png\" alt=\"\" class=\"wp-image-157\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img alt=\"\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Step 2: Upload</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Step 3: Watch your artwork<br></p>\n<!-- /wp:paragraph -->', 'Picture Town', '', 'inherit', 'closed', 'closed', '', '130-revision-v1', '', '', '2019-02-02 04:27:39', '2019-02-02 04:27:39', '', 130, 'https://picture.hungenbach.de/2019/02/02/130-revision-v1/', 0, 'revision', '', 0),
(162, 1, '2019-02-02 04:31:15', '2019-02-02 04:31:15', '<!-- wp:paragraph -->\n<p>Download drawing templates of cars and buildings. Print out the template and draw from your imagination. Let your artwork come alive.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Step 1: Download Template</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:file {\"id\":146,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\"} -->\n<div class=\"wp-block-file\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\">template_bus</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:paragraph -->\n<p>Step 2: Upload</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Step 3: Watch your artwork<br></p>\n<!-- /wp:paragraph -->', 'Picture Town', '', 'inherit', 'closed', 'closed', '', '130-revision-v1', '', '', '2019-02-02 04:31:15', '2019-02-02 04:31:15', '', 130, 'https://picture.hungenbach.de/2019/02/02/130-revision-v1/', 0, 'revision', '', 0),
(164, 1, '2019-02-02 04:34:07', '2019-02-02 04:34:07', '<!-- wp:paragraph -->\n<p>Download drawing templates of cars and buildings. Print out the template and draw from your imagination. Let your artwork come alive.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Step 1: Print your favorite template</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:file {\"id\":147,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_car_small.pdf\"} -->\n<div class=\"wp-block-file\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_car_small.pdf\">Car Small</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_car_small.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":146,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\"} -->\n<div class=\"wp-block-file\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\">Bus</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":148,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_big.pdf\"} -->\n<div class=\"wp-block-file\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_big.pdf\">House Big</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_big.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":149,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_small.pdf\"} -->\n<div class=\"wp-block-file\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_small.pdf\">House Small</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_small.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:paragraph -->\n<p>Step 2: Upload</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Step 3: Bring it alive<br></p>\n<!-- /wp:paragraph -->', 'Picture Town', '', 'inherit', 'closed', 'closed', '', '130-revision-v1', '', '', '2019-02-02 04:34:07', '2019-02-02 04:34:07', '', 130, 'https://picture.hungenbach.de/2019/02/02/130-revision-v1/', 0, 'revision', '', 0),
(166, 1, '2019-02-02 04:35:32', '2019-02-02 04:35:32', '<!-- wp:paragraph -->\n<p>Download drawing templates of cars and buildings. Print out the template and draw from your imagination. Let your artwork come alive.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 1: Print your favorite template</h3>\n<!-- /wp:heading -->\n\n<!-- wp:file {\"id\":147,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_car_small.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_car_small.pdf\">Car Small</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_car_small.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":146,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\">Bus</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":148,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_big.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_big.pdf\">House Big</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_big.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":149,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_small.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_small.pdf\">House Small</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_small.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 2: Upload</h3>\n<!-- /wp:heading -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 3: Bring it alive</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p><br></p>\n<!-- /wp:paragraph -->', 'Picture Town', '', 'inherit', 'closed', 'closed', '', '130-revision-v1', '', '', '2019-02-02 04:35:32', '2019-02-02 04:35:32', '', 130, 'https://picture.hungenbach.de/2019/02/02/130-revision-v1/', 0, 'revision', '', 0),
(168, 1, '2019-02-02 04:37:26', '2019-02-02 04:37:26', '<!-- wp:paragraph -->\n<p>Download drawing templates of cars and buildings. Print out the template and draw from your imagination. Let your artwork come alive.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 1: Print your favorite template</h3>\n<!-- /wp:heading -->\n\n<!-- wp:file {\"id\":147,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_car_small.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_car_small.pdf\">Car Small</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_car_small.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":146,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\">Bus</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":148,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_big.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_big.pdf\">House Big</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_big.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":149,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_small.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_small.pdf\">House Small</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_small.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 2: Upload</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Register or login to upload and manage your drawings <a href=\"https://picture.hungenbach.de/upload-image/\">here</a>.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 3: Bring it alive</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Watch your drawings move on a canvas here.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><br></p>\n<!-- /wp:paragraph -->', 'Picture Town', '', 'inherit', 'closed', 'closed', '', '130-revision-v1', '', '', '2019-02-02 04:37:26', '2019-02-02 04:37:26', '', 130, 'https://picture.hungenbach.de/2019/02/02/130-revision-v1/', 0, 'revision', '', 0),
(169, 1, '2019-02-02 04:37:36', '2019-02-02 04:37:36', '<!-- wp:paragraph -->\n<p>Download drawing templates of cars and buildings. Print out the template and draw from your imagination. Let your artwork come alive.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 1: Print your favorite template</h3>\n<!-- /wp:heading -->\n\n<!-- wp:file {\"id\":147,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_car_small.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_car_small.pdf\">Car Small</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_car_small.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":146,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\">Bus</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":148,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_big.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_big.pdf\">House Big</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_big.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":149,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_small.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_small.pdf\">House Small</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_small.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 2: Upload</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Register or login to upload and manage your drawings <a href=\"https://picture.hungenbach.de/upload-image/\">here</a>.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 3: Bring it alive</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Watch your drawings move on a canvas <a href=\"https://picture.hungenbach.de/picture-town-map/\">here</a>.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><br></p>\n<!-- /wp:paragraph -->', 'Picture Town', '', 'inherit', 'closed', 'closed', '', '130-revision-v1', '', '', '2019-02-02 04:37:36', '2019-02-02 04:37:36', '', 130, 'https://picture.hungenbach.de/2019/02/02/130-revision-v1/', 0, 'revision', '', 0),
(170, 1, '2019-02-02 04:38:28', '2019-02-02 04:38:28', '<!-- wp:paragraph -->\n<p>Download drawing templates of cars and buildings. Print out the template and draw from your imagination. Let your artwork come alive.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 1: Print your favorite template</h3>\n<!-- /wp:heading -->\n\n<!-- wp:file {\"id\":147,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_car_small.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_car_small.pdf\">Car Small</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_car_small.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":146,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\">Bus</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":148,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_big.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_big.pdf\">House Big</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_big.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":149,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_small.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_small.pdf\">House Small</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_small.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 2: Upload</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Register or login to upload and manage your drawings <a href=\"https://picture.hungenbach.de/rm_login/\">here</a>.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 3: Bring it alive</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Watch your drawings move on a canvas <a href=\"https://picture.hungenbach.de/picture-town-map/\">here</a>.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><br></p>\n<!-- /wp:paragraph -->', 'Picture Town', '', 'inherit', 'closed', 'closed', '', '130-revision-v1', '', '', '2019-02-02 04:38:28', '2019-02-02 04:38:28', '', 130, 'https://picture.hungenbach.de/2019/02/02/130-revision-v1/', 0, 'revision', '', 0),
(173, 1, '2019-02-02 04:48:39', '2019-02-02 04:48:39', '<!-- wp:paragraph -->\n<p>Download drawing templates of cars and buildings. Print out the template and draw from your imagination. Let your artwork come alive.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 1: Print your favorite template</h3>\n<!-- /wp:heading -->\n\n<!-- wp:file {\"id\":147,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_car_small.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_car_small.pdf\">① Car Small</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_car_small.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":146,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\">② Bus</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_bus.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":148,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_big.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_big.pdf\">③ House Big</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_big.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":149,\"href\":\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_small.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_small.pdf\">④ House Small</a><a href=\"https://picture.hungenbach.de/wp-content/uploads/2019/02/template_house_small.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 2: Upload</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Register or login to upload and manage your drawings <a href=\"https://picture.hungenbach.de/rm_login/\">here</a>.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 3: Bring it alive</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Watch your drawings move on a canvas <a href=\"https://picture.hungenbach.de/picture-town-map/\">here</a>.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><br></p>\n<!-- /wp:paragraph -->', 'Picture Town', '', 'inherit', 'closed', 'closed', '', '130-revision-v1', '', '', '2019-02-02 04:48:39', '2019-02-02 04:48:39', '', 130, 'https://picture.hungenbach.de/2019/02/02/130-revision-v1/', 0, 'revision', '', 0),
(179, 1, '2019-06-22 04:17:54', '2019-06-22 04:17:54', 'test', 'untitled.png', '', 'publish', 'closed', 'closed', '', 'untitled-png', '', '', '2019-06-22 04:17:54', '2019-06-22 04:17:54', '', 0, 'https://picture.hungenbach.de/wpfm-files/untitled-png/', 0, 'wpfm-files', '', 0),
(180, 1, '2019-06-22 04:17:54', '2019-06-22 04:17:54', '', 'untitled.png', '', 'inherit', 'closed', 'closed', '', 'untitled-png-2', '', '', '2019-06-22 04:17:54', '2019-06-22 04:17:54', '', 179, 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/markus@hungenbach.de/untitled.png', 0, 'attachment', 'image/png', 0),
(181, 10, '2019-06-24 16:24:37', '2019-06-24 16:24:37', '', 'my_image.jpg', '', 'publish', 'closed', 'closed', '', 'my_image-jpg', '', '', '2019-06-24 16:24:37', '2019-06-24 16:24:37', '', 0, 'https://picture.hungenbach.de/wpfm-files/my_image-jpg/', 0, 'wpfm-files', '', 0),
(182, 10, '2019-06-24 16:24:37', '2019-06-24 16:24:37', '', 'my_image.jpg', '', 'inherit', 'closed', 'closed', '', 'my_image-jpg-2', '', '', '2019-06-24 16:24:37', '2019-06-24 16:24:37', '', 181, 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/test111/my_image.jpg', 0, 'attachment', 'image/jpeg', 0),
(183, 11, '2019-06-29 04:59:42', '2019-06-29 04:59:42', 'MY CAR', 'images.jpg', '', 'publish', 'closed', 'closed', '', 'images-jpg', '', '', '2019-06-29 04:59:42', '2019-06-29 04:59:42', '', 0, 'https://picture.hungenbach.de/wpfm-files/images-jpg/', 0, 'wpfm-files', '', 0),
(184, 11, '2019-06-29 04:59:42', '2019-06-29 04:59:42', '', 'images.jpg', '', 'inherit', 'closed', 'closed', '', 'images-jpg-2', '', '', '2019-06-29 04:59:42', '2019-06-29 04:59:42', '', 183, 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/AlexBregman/images.jpg', 0, 'attachment', 'image/jpeg', 0),
(185, 12, '2019-07-02 07:46:06', '2019-07-02 07:46:06', 'test image', 'img_20180607_192631_237.jpg', '', 'publish', 'closed', 'closed', '', 'img_20180607_192631_237-jpg', '', '', '2019-07-02 07:46:06', '2019-07-02 07:46:06', '', 0, 'https://picture.hungenbach.de/wpfm-files/img_20180607_192631_237-jpg/', 0, 'wpfm-files', '', 0),
(186, 12, '2019-07-02 07:46:06', '2019-07-02 07:46:06', '', 'img_20180607_192631_237.jpg', '', 'inherit', 'closed', 'closed', '', 'img_20180607_192631_237-jpg-2', '', '', '2019-07-02 07:46:06', '2019-07-02 07:46:06', '', 185, 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/testing/img_20180607_192631_237.jpg', 0, 'attachment', 'image/jpeg', 0),
(187, 13, '2019-07-15 09:17:51', '2019-07-15 09:17:51', '', 'simple_house_3d_model_c4d_max_obj_fbx_ma_lwo_3ds_3dm_stl_1520429_o.jpg', '', 'publish', 'closed', 'closed', '', 'simple_house_3d_model_c4d_max_obj_fbx_ma_lwo_3ds_3dm_stl_1520429_o-jpg', '', '', '2019-07-15 09:17:51', '2019-07-15 09:17:51', '', 0, 'https://picture.hungenbach.de/wpfm-files/simple_house_3d_model_c4d_max_obj_fbx_ma_lwo_3ds_3dm_stl_1520429_o-jpg/', 0, 'wpfm-files', '', 0),
(188, 13, '2019-07-15 09:17:51', '2019-07-15 09:17:51', '', 'simple_house_3d_model_c4d_max_obj_fbx_ma_lwo_3ds_3dm_stl_1520429_o.jpg', '', 'inherit', 'closed', 'closed', '', 'simple_house_3d_model_c4d_max_obj_fbx_ma_lwo_3ds_3dm_stl_1520429_o-jpg-2', '', '', '2019-07-15 09:17:51', '2019-07-15 09:17:51', '', 187, 'https://picture.hungenbach.de/wp-content/uploads/user_uploads/tt/simple_house_3d_model_c4d_max_obj_fbx_ma_lwo_3ds_3dm_stl_1520429_o.jpg', 0, 'attachment', 'image/jpeg', 0),
(231, 15, '2019-07-29 04:20:59', '2019-07-29 04:20:59', '', 'img_20180607_192631_237.jpg', '', 'publish', 'closed', 'closed', '', 'img_20180607_192631_237-jpg-2', '', '', '2019-07-29 04:20:59', '2019-07-29 04:20:59', '', 0, 'http://www.picture.hungenbach.de/wpfm-files/img_20180607_192631_237-jpg-2/', 0, 'wpfm-files', '', 0),
(232, 15, '2019-07-29 04:20:59', '2019-07-29 04:20:59', '', 'img_20180607_192631_237.jpg', '', 'inherit', 'closed', 'closed', '', 'img_20180607_192631_237-jpg-3', '', '', '2019-07-29 04:20:59', '2019-07-29 04:20:59', '', 231, 'http://www.picture.hungenbach.de/wp-content/uploads/user_uploads/ChicMic/img_20180607_192631_237.jpg', 0, 'attachment', 'image/jpeg', 0),
(197, 15, '2019-07-19 12:08:01', '2019-07-19 12:08:01', '', '3D Model', '', 'publish', 'closed', 'closed', '', '3d-model', '', '', '2019-08-05 06:44:49', '2019-08-05 06:44:49', '', 0, 'http://picture.hungenbach.de/?page_id=197', 0, 'page', '', 0),
(198, 15, '2019-07-19 12:08:01', '2019-07-19 12:08:01', '', '3D Model', '', 'inherit', 'closed', 'closed', '', '197-revision-v1', '', '', '2019-07-19 12:08:01', '2019-07-19 12:08:01', '', 197, 'http://picture.hungenbach.de/2019/07/19/197-revision-v1/', 0, 'revision', '', 0),
(260, 1, '2019-08-16 03:37:41', '2019-08-16 03:37:41', 'http://www.picture.hungenbach.de/wp-content/uploads/2019/08/cropped-logo.png', 'cropped-logo.png', '', 'inherit', 'closed', 'closed', '', 'cropped-logo-png', '', '', '2019-08-16 03:37:41', '2019-08-16 03:37:41', '', 0, 'http://www.picture.hungenbach.de/wp-content/uploads/2019/08/cropped-logo.png', 0, 'attachment', 'image/png', 0),
(199, 16, '2019-07-22 05:13:49', '2019-07-22 05:13:49', '', 'template_bus.jpg', '', 'publish', 'closed', 'closed', '', 'template_bus-jpg', '', '', '2019-07-22 05:13:49', '2019-07-22 05:13:49', '', 0, 'http://picture.hungenbach.de/wpfm-files/template_bus-jpg/', 0, 'wpfm-files', '', 0),
(200, 16, '2019-07-22 05:13:49', '2019-07-22 05:13:49', '', 'template_bus.jpg', '', 'inherit', 'closed', 'closed', '', 'template_bus-jpg-2', '', '', '2019-07-22 05:13:49', '2019-07-22 05:13:49', '', 199, 'http://picture.hungenbach.de/wp-content/uploads/user_uploads/julian/template_bus.jpg', 0, 'attachment', 'image/jpeg', 0),
(286, 19, '2019-08-26 04:13:13', '2019-08-26 04:13:13', '', 'sshot14.png.pagespeed.ce.bP3qghWPq2', '', 'inherit', 'closed', 'closed', '', 'sshot14-png-pagespeed-ce-bp3qghwpq2', '', '', '2019-08-26 04:13:13', '2019-08-26 04:13:13', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/SS/1566792793__sshot14.png.pagespeed.ce.bP3qghWPq2.png', 0, 'attachment', 'image/jpeg', 0),
(299, 17, '2019-08-30 04:27:58', '2019-08-30 04:27:58', '', 'bus_QR_n (1)', '', 'inherit', 'closed', 'closed', '', 'bus_qr_n-1', '', '', '2019-08-30 04:27:58', '2019-08-30 04:27:58', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/mohammad/1567139275__bus_QR_n%20(1).jpg', 0, 'attachment', 'image/jpeg', 0),
(298, 17, '2019-08-30 04:03:28', '2019-08-30 04:03:28', '', 'bus_QR_n', '', 'inherit', 'closed', 'closed', '', 'bus_qr_n', '', '', '2019-08-30 04:03:28', '2019-08-30 04:03:28', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/mohammad/1567137805__bus_QR_n.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(207, 1, '2019-07-25 11:07:55', '2019-07-25 11:07:55', '<!-- wp:paragraph -->\n<p>Download drawing templates of cars and buildings. Print out the template and draw from your imagination. Let your artwork come alive.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 1: Print your favorite template</h3>\n<!-- /wp:heading -->\n\n<!-- wp:file {\"id\":204,\"href\":\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_car_small.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_car_small.pdf\">① Car Small</a><a href=\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_car_small.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":203,\"href\":\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_bus.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_bus.pdf\">② Bus</a><a href=\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_bus.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":205,\"href\":\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_house_big.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_house_big.pdf\">③ House Big</a><a href=\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_house_big.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:file {\"id\":206,\"href\":\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_house_small.pdf\",\"align\":\"center\"} -->\n<div class=\"wp-block-file aligncenter\"><a href=\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_house_small.pdf\">④ House Small</a><a href=\"http://www.picture.hungenbach.de/wp-content/uploads/2019/07/template_house_small.pdf\" class=\"wp-block-file__button\" download>Download</a></div>\n<!-- /wp:file -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 2: Upload</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Register or login to upload and manage your drawings <a href=\"https://picture.hungenbach.de/rm_login/\">here</a>.<br></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3>Step 3: Bring it alive</h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p>Watch your drawings move on a canvas <a href=\"https://picture.hungenbach.de/picture-town-map/\">here</a>.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><br></p>\n<!-- /wp:paragraph -->', 'Picture Town', '', 'inherit', 'closed', 'closed', '', '130-revision-v1', '', '', '2019-07-25 11:07:55', '2019-07-25 11:07:55', '', 130, 'https://www.picture.hungenbach.de/2019/07/25/130-revision-v1/', 0, 'revision', '', 0),
(209, 15, '2019-07-26 08:53:57', '2019-07-26 08:53:57', '<!-- wp:paragraph -->\n<p>[customFileUpload]</p>\n<!-- /wp:paragraph -->', 'Upload Image For 3D', '', 'publish', 'closed', 'closed', '', 'custom-file-upload', '', '', '2019-07-30 05:27:16', '2019-07-30 05:27:16', '', 0, 'http://www.picture.hungenbach.de/?page_id=209', 0, 'page', '', 0),
(210, 15, '2019-07-26 08:53:57', '2019-07-26 08:53:57', '<!-- wp:paragraph -->\n<p>[customFileUpload]</p>\n<!-- /wp:paragraph -->', 'Custom File Upload', '', 'inherit', 'closed', 'closed', '', '209-revision-v1', '', '', '2019-07-26 08:53:57', '2019-07-26 08:53:57', '', 209, 'https://www.picture.hungenbach.de/2019/07/26/209-revision-v1/', 0, 'revision', '', 0),
(211, 15, '2019-07-26 08:55:10', '2019-07-26 08:55:10', '', 'emily', '', 'inherit', 'closed', 'closed', '', 'emily', '', '', '2019-07-26 08:55:10', '2019-07-26 08:55:10', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/ChicMic/1564131310__emily.png', 0, 'attachment', 'image/png', 0),
(212, 15, '2019-07-26 08:57:55', '2019-07-26 08:57:55', '<!-- wp:paragraph -->\n<p>[customFileUpload]</p>\n<!-- /wp:paragraph -->', 'Upload Image', '', 'inherit', 'closed', 'closed', '', '209-revision-v1', '', '', '2019-07-26 08:57:55', '2019-07-26 08:57:55', '', 209, 'https://www.picture.hungenbach.de/2019/07/26/209-revision-v1/', 0, 'revision', '', 0),
(336, 17, '2019-09-02 06:37:39', '2019-09-02 06:37:39', '', 'iphone_IMG_5477', '', 'inherit', 'closed', 'closed', '', 'iphone_img_5477', '', '', '2019-09-02 06:37:39', '2019-09-02 06:37:39', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/mohammad/1567406244__iphone_IMG_5477.jpg', 0, 'attachment', 'image/jpeg', 0),
(214, 15, '2019-07-26 08:59:37', '2019-07-26 08:59:37', '<!-- wp:paragraph -->\n<p>[customFileUpload]</p>\n<!-- /wp:paragraph -->', 'Upload Image For 3D', '', 'inherit', 'closed', 'closed', '', '209-revision-v1', '', '', '2019-07-26 08:59:37', '2019-07-26 08:59:37', '', 209, 'https://www.picture.hungenbach.de/2019/07/26/209-revision-v1/', 0, 'revision', '', 0),
(215, 15, '2019-07-26 09:00:15', '2019-07-26 09:00:15', '', 'emily', '', 'inherit', 'closed', 'closed', '', 'emily-3', '', '', '2019-07-26 09:00:15', '2019-07-26 09:00:15', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/ChicMic/1564131615__emily.png', 0, 'attachment', 'image/png', 0),
(216, 15, '2019-07-26 09:21:20', '2019-07-26 09:21:20', '', 'testnude', '', 'inherit', 'closed', 'closed', '', 'testnude', '', '', '2019-07-26 09:21:20', '2019-07-26 09:21:20', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/ChicMic/1564132880__testnude.png', 0, 'attachment', 'image/png', 0),
(230, 15, '2019-07-29 04:20:16', '2019-07-29 04:20:16', '', 'template_bus-color', '', 'inherit', 'closed', 'closed', '', 'template_bus-color', '', '', '2019-07-29 04:20:16', '2019-07-29 04:20:16', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/ChicMic/1564374016__template_bus-color.jpg', 0, 'attachment', 'image/jpeg', 0),
(217, 1, '2019-07-28 05:11:03', '2019-07-28 05:11:03', '<!-- wp:paragraph -->\n<p>This page:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>is restricted to paid members only</li><li>is user specific</li><li>shows a map (e.g. background image) </li><li>shows all 3D objects of the respective user (appearing in the sequence they have been uploaded - file timestamp)</li><li>is opening in a new tab</li><li>is full screen (does not contain menu / footer / etc)</li></ul>\n<!-- /wp:list -->', 'Picture Town Map', '', 'inherit', 'closed', 'closed', '', '142-revision-v1', '', '', '2019-07-28 05:11:03', '2019-07-28 05:11:03', '', 142, 'https://www.picture.hungenbach.de/2019/07/28/142-revision-v1/', 0, 'revision', '', 0),
(218, 1, '2019-07-28 05:13:07', '2019-07-28 05:13:07', '<!-- wp:paragraph -->\n<p>This page:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>is restricted to paid members only (defaults to an animation in case use is \"guest\" - not logged in)</li><li>is user specific</li><li>shows a map (e.g. background image) </li><li>shows all 3D objects of the respective user (appearing in the sequence they have been uploaded - file timestamp)</li><li>is opening in a new tab</li><li>is full screen (does not contain menu / footer / etc)</li></ul>\n<!-- /wp:list -->', 'Picture Town Map', '', 'inherit', 'closed', 'closed', '', '142-revision-v1', '', '', '2019-07-28 05:13:07', '2019-07-28 05:13:07', '', 142, 'https://www.picture.hungenbach.de/2019/07/28/142-revision-v1/', 0, 'revision', '', 0),
(219, 1, '2019-07-28 05:13:24', '2019-07-28 05:13:24', '<!-- wp:paragraph -->\n<p>This page:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>is restricted to paid members only (defaults to an animation in case use is \"guest\" - not logged in)</li><li>is user specific (knows the user id)</li><li>shows a map (e.g. background image) </li><li>shows all 3D objects of the respective user (appearing in the sequence they have been uploaded - file timestamp)</li><li>is opening in a new tab</li><li>is full screen (does not contain menu / footer / etc)</li></ul>\n<!-- /wp:list -->', 'Picture Town Map', '', 'inherit', 'closed', 'closed', '', '142-revision-v1', '', '', '2019-07-28 05:13:24', '2019-07-28 05:13:24', '', 142, 'https://www.picture.hungenbach.de/2019/07/28/142-revision-v1/', 0, 'revision', '', 0),
(221, 1, '2019-07-28 05:38:23', '2019-07-28 05:38:23', '<!-- wp:paragraph -->\n<p>This page:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>is restricted to paid members only (defaults to an animation in case use is \"guest\" - not logged in)</li><li>is user specific (knows the user id)</li><li>shows a map (e.g. background image) </li><li>shows all 3D objects of the respective user (appearing in the sequence they have been uploaded - file timestamp)</li><li>is opening in a new tab</li><li>is full screen (does not contain menu / footer / etc)</li></ul>\n<!-- /wp:list -->', 'Picture Town Map', '', 'inherit', 'closed', 'closed', '', '142-revision-v1', '', '', '2019-07-28 05:38:23', '2019-07-28 05:38:23', '', 142, 'https://www.picture.hungenbach.de/2019/07/28/142-revision-v1/', 0, 'revision', '', 0),
(220, 1, '2019-07-28 05:37:28', '2019-07-28 05:37:28', '<!-- wp:paragraph -->\n<p>This page:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>is restricted to paid members only (defaults to an animation in case use is \"guest\" - not logged in)</li><li>is user specific (knows the user id)</li><li>shows a map (e.g. background image) </li><li>shows all 3D objects of the respective user (appearing in the sequence they have been uploaded - file timestamp)</li><li>is opening in a new tab</li><li>is full screen (does not contain menu / footer / etc)</li></ul>\n<!-- /wp:list -->\n\n<!-- wp:preformatted -->\n<pre class=\"wp-block-preformatted\">[customized_map_3D foo=\"bar\" bar=\"bing\"]</pre>\n<!-- /wp:preformatted -->', 'Picture Town Map', '', 'inherit', 'closed', 'closed', '', '142-revision-v1', '', '', '2019-07-28 05:37:28', '2019-07-28 05:37:28', '', 142, 'https://www.picture.hungenbach.de/2019/07/28/142-revision-v1/', 0, 'revision', '', 0),
(224, 1, '2019-07-28 05:40:23', '2019-07-28 05:40:23', '<!-- wp:paragraph -->\n<p>This page:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>is restricted to paid members only (defaults to an animation in case use is \"guest\" - not logged in)</li><li>is user specific (knows the user id)</li><li>shows a map (e.g. background image) </li><li>shows all 3D objects of the respective user (appearing in the sequence they have been uploaded - file timestamp)</li><li>is opening in a new tab</li><li>is full screen (does not contain menu / footer / etc)</li></ul>\n<!-- /wp:list -->', 'Picture Town Map', '', 'inherit', 'closed', 'closed', '', '142-revision-v1', '', '', '2019-07-28 05:40:23', '2019-07-28 05:40:23', '', 142, 'https://www.picture.hungenbach.de/2019/07/28/142-revision-v1/', 0, 'revision', '', 0),
(222, 1, '2019-07-28 05:39:16', '2019-07-28 05:39:16', '<!-- wp:paragraph -->\n<p>This page:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>is restricted to paid members only (defaults to an animation in case use is \"guest\" - not logged in)</li><li>is user specific (knows the user id)</li><li>shows a map (e.g. background image) </li><li>shows all 3D objects of the respective user (appearing in the sequence they have been uploaded - file timestamp)</li><li>is opening in a new tab</li><li>is full screen (does not contain menu / footer / etc)</li><li>[customized_map_3D foo=\"bar\" bar=\"bing\"]</li></ul>\n<!-- /wp:list -->', 'Picture Town Map', '', 'inherit', 'closed', 'closed', '', '142-revision-v1', '', '', '2019-07-28 05:39:16', '2019-07-28 05:39:16', '', 142, 'https://www.picture.hungenbach.de/2019/07/28/142-revision-v1/', 0, 'revision', '', 0),
(225, 1, '2019-07-28 05:42:57', '2019-07-28 05:42:57', '<!-- wp:paragraph -->\n<p>This page:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>is restricted to paid members only (defaults to an animation in case use is \"guest\" - not logged in)</li><li>is user specific (knows the user id)</li><li>shows a map (e.g. background image) </li><li>shows all 3D objects of the respective user (appearing in the sequence they have been uploaded - file timestamp)</li><li>is opening in a new tab</li><li>is full screen (does not contain menu / footer / etc)</li><li>[customized_map_3D]</li></ul>\n<!-- /wp:list -->', 'Picture Town Map', '', 'inherit', 'closed', 'closed', '', '142-revision-v1', '', '', '2019-07-28 05:42:57', '2019-07-28 05:42:57', '', 142, 'https://www.picture.hungenbach.de/2019/07/28/142-revision-v1/', 0, 'revision', '', 0),
(229, 1, '2019-07-28 07:07:02', '2019-07-28 07:07:02', '<!-- wp:paragraph -->\n<p>[customized_map_3D]  </p>\n<!-- /wp:paragraph -->', 'Picture Town Map', '', 'inherit', 'closed', 'closed', '', '142-revision-v1', '', '', '2019-07-28 07:07:02', '2019-07-28 07:07:02', '', 142, 'https://www.picture.hungenbach.de/2019/07/28/142-revision-v1/', 0, 'revision', '', 0),
(226, 1, '2019-07-28 05:43:32', '2019-07-28 05:43:32', '<!-- wp:paragraph -->\n<p>This page:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li>is restricted to paid members only (defaults to an animation in case use is \"guest\" - not logged in)</li><li>is user specific (knows the user id - in this case:  [customized_map_3D] )</li><li>shows a map (e.g. background image) </li><li>shows all 3D objects of the respective user (appearing in the sequence they have been uploaded - file timestamp)</li><li>is opening in a new tab</li><li>is full screen (does not contain menu / footer / etc)</li><li></li></ul>\n<!-- /wp:list -->', 'Picture Town Map', '', 'inherit', 'closed', 'closed', '', '142-revision-v1', '', '', '2019-07-28 05:43:32', '2019-07-28 05:43:32', '', 142, 'https://www.picture.hungenbach.de/2019/07/28/142-revision-v1/', 0, 'revision', '', 0),
(227, 4, '2019-07-28 05:57:51', '2019-07-28 05:57:51', 'test description', 'untitled_2.png', '', 'publish', 'closed', 'closed', '', 'untitled_2-png', '', '', '2019-07-28 05:57:51', '2019-07-28 05:57:51', '', 0, 'http://www.picture.hungenbach.de/wpfm-files/untitled_2-png/', 0, 'wpfm-files', '', 0),
(233, 15, '2019-07-29 04:21:26', '2019-07-29 04:21:26', '', 'template_house_small.jpg', '', 'publish', 'closed', 'closed', '', 'template_house_small-jpg', '', '', '2019-07-29 04:21:26', '2019-07-29 04:21:26', '', 0, 'http://www.picture.hungenbach.de/wpfm-files/template_house_small-jpg/', 0, 'wpfm-files', '', 0),
(234, 15, '2019-07-29 04:21:26', '2019-07-29 04:21:26', '', 'template_house_small.jpg', '', 'inherit', 'closed', 'closed', '', 'template_house_small-jpg-2', '', '', '2019-07-29 04:21:26', '2019-07-29 04:21:26', '', 233, 'http://www.picture.hungenbach.de/wp-content/uploads/user_uploads/ChicMic/template_house_small.jpg', 0, 'attachment', 'image/jpeg', 0),
(412, 4, '2019-10-11 07:03:14', '2019-10-11 07:03:14', '', '0514A769-3859-44C2-AD4D-984066A1BCC7', '', 'inherit', 'closed', 'closed', '', '0514a769-3859-44c2-ad4d-984066a1bcc7', '', '', '2019-10-11 07:03:14', '2019-10-11 07:03:14', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1570777393__0514A769-3859-44C2-AD4D-984066A1BCC7.jpeg', 0, 'attachment', 'image/jpeg', 0),
(333, 20, '2019-09-02 03:23:58', '2019-09-02 03:23:58', '', '1E8D6051-FB83-46B0-AC43-521175836A39', '', 'inherit', 'closed', 'closed', '', '1e8d6051-fb83-46b0-ac43-521175836a39', '', '', '2019-09-02 03:23:58', '2019-09-02 03:23:58', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/hungenbach/1567394634__1E8D6051-FB83-46B0-AC43-521175836A39.jpeg', 0, 'attachment', 'image/jpeg', 0),
(411, 4, '2019-10-11 05:23:49', '2019-10-11 05:23:49', '', '8A13EEE2-6676-4889-8B34-C2E957498709', '', 'inherit', 'closed', 'closed', '', '8a13eee2-6676-4889-8b34-c2e957498709', '', '', '2019-10-11 05:23:49', '2019-10-11 05:23:49', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1570771427__8A13EEE2-6676-4889-8B34-C2E957498709.jpeg', 0, 'attachment', 'image/jpeg', 0),
(335, 20, '2019-09-02 03:47:54', '2019-09-02 03:47:54', '', '0B20D265-B151-4DDE-8382-6988A9B65D6D', '', 'inherit', 'closed', 'closed', '', '0b20d265-b151-4dde-8382-6988a9b65d6d', '', '', '2019-09-02 03:47:54', '2019-09-02 03:47:54', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/hungenbach/1567396069__0B20D265-B151-4DDE-8382-6988A9B65D6D.jpeg', 0, 'attachment', 'image/jpeg', 0),
(285, 15, '2019-08-23 12:30:32', '2019-08-23 12:30:32', '', '', '', 'inherit', 'closed', 'closed', '', '285', '', '', '2019-08-23 12:30:32', '2019-08-23 12:30:32', '', 0, 'http://www.picture.hungenbach.de/wp-content/uploads/2019/08/qr-code-reader-master.zip', 0, 'attachment', 'application/zip', 0),
(534, 4, '2019-10-20 03:41:12', '2019-10-20 03:41:12', '', 'D9B922A4-AF02-4AC7-AB85-253F27A1BC21', '', 'inherit', 'closed', 'closed', '', 'd9b922a4-af02-4ac7-ab85-253f27a1bc21', '', '', '2019-10-20 03:41:12', '2019-10-20 03:41:12', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1571542868__D9B922A4-AF02-4AC7-AB85-253F27A1BC21.jpeg', 0, 'attachment', 'image/jpeg', 0),
(551, 4, '2019-10-21 16:19:48', '2019-10-21 16:19:48', '', '158B048D-24F1-438D-8455-E4EC5FA82862', '', 'inherit', 'closed', 'closed', '', '158b048d-24f1-438d-8455-e4ec5fa82862', '', '', '2019-10-21 16:19:48', '2019-10-21 16:19:48', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1571674786__158B048D-24F1-438D-8455-E4EC5FA82862.jpeg', 0, 'attachment', 'image/jpeg', 0),
(413, 4, '2019-10-11 07:04:35', '2019-10-11 07:04:35', '', '54F0C925-9BE3-4589-AE65-AFFDA6B9F7B7', '', 'inherit', 'closed', 'closed', '', '54f0c925-9be3-4589-ae65-affda6b9f7b7', '', '', '2019-10-11 07:04:35', '2019-10-11 07:04:35', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1570777472__54F0C925-9BE3-4589-AE65-AFFDA6B9F7B7.jpeg', 0, 'attachment', 'image/jpeg', 0),
(379, 4, '2019-10-04 09:21:43', '2019-10-04 09:21:43', '', '56CA10F0-107B-4762-B6EF-63E9163EC4A8', '', 'inherit', 'closed', 'closed', '', '56ca10f0-107b-4762-b6ef-63e9163ec4a8', '', '', '2019-10-04 09:21:43', '2019-10-04 09:21:43', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1570180900__56CA10F0-107B-4762-B6EF-63E9163EC4A8.jpeg', 0, 'attachment', 'image/jpeg', 0),
(380, 4, '2019-10-04 09:22:07', '2019-10-04 09:22:07', '', 'FA4CF689-F85B-434A-B04E-8E8545129AFF', '', 'inherit', 'closed', 'closed', '', 'fa4cf689-f85b-434a-b04e-8e8545129aff', '', '', '2019-10-04 09:22:07', '2019-10-04 09:22:07', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1570180924__FA4CF689-F85B-434A-B04E-8E8545129AFF.jpeg', 0, 'attachment', 'image/jpeg', 0),
(249, 15, '2019-08-05 06:44:49', '2019-08-05 06:44:49', '', '3D Model', '', 'inherit', 'closed', 'closed', '', '197-revision-v1', '', '', '2019-08-05 06:44:49', '2019-08-05 06:44:49', '', 197, 'https://www.picture.hungenbach.de/197-revision-v1/', 0, 'revision', '', 0),
(248, 15, '2019-08-05 06:44:18', '2019-08-05 06:44:18', '<!-- wp:paragraph -->\n<p>&lt;div id= \"3d-Model-Container\" style=\"background-color: #000; width: 800px; height: 800px;\">This is just for testing.&lt;/div></p>\n<!-- /wp:paragraph -->', '3D Model', '', 'inherit', 'closed', 'closed', '', '197-revision-v1', '', '', '2019-08-05 06:44:18', '2019-08-05 06:44:18', '', 197, 'https://www.picture.hungenbach.de/197-revision-v1/', 0, 'revision', '', 0),
(301, 17, '2019-08-30 09:36:55', '2019-08-30 09:36:55', '', 'house_small (1)', '', 'inherit', 'closed', 'closed', '', 'house_small-1', '', '', '2019-08-30 09:36:55', '2019-08-30 09:36:55', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/mohammad/1567157812__house_small%20(1).jpg', 0, 'attachment', 'image/jpeg', 0),
(337, 17, '2019-09-09 05:39:55', '2019-09-09 05:39:55', '', 'ipad_IMG_0511', '', 'inherit', 'closed', 'closed', '', 'ipad_img_0511', '', '', '2019-09-09 05:39:55', '2019-09-09 05:39:55', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/mohammad/1568007588__ipad_IMG_0511.JPG', 0, 'attachment', 'image/jpeg', 0),
(297, 17, '2019-08-27 10:06:01', '2019-08-27 10:06:01', '', 'house_big', '', 'inherit', 'closed', 'closed', '', 'house_big', '', '', '2019-08-27 10:06:01', '2019-08-27 10:06:01', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/mohammad/1566900359__house_big.jpg', 0, 'attachment', 'image/jpeg', 0),
(259, 1, '2019-08-16 03:37:22', '2019-08-16 03:37:22', '', 'logo', '', 'inherit', 'closed', 'closed', '', 'logo', '', '', '2019-08-16 03:37:34', '2019-08-16 03:37:34', '', 0, 'http://www.picture.hungenbach.de/wp-content/uploads/2019/08/logo.png', 0, 'attachment', 'image/png', 0),
(391, 4, '2019-10-07 13:18:47', '2019-10-07 13:18:47', '', '01E7BC4C-EAF9-4F20-84AB-6F42AAF50455', '', 'inherit', 'closed', 'closed', '', '01e7bc4c-eaf9-4f20-84ab-6f42aaf50455', '', '', '2019-10-07 13:18:47', '2019-10-07 13:18:47', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1570454323__01E7BC4C-EAF9-4F20-84AB-6F42AAF50455.jpeg', 0, 'attachment', 'image/jpeg', 0),
(385, 4, '2019-10-04 09:26:09', '2019-10-04 09:26:09', '', '128B30EE-CC95-488A-B1BE-0484DCF9BB44', '', 'inherit', 'closed', 'closed', '', '128b30ee-cc95-488a-b1be-0484dcf9bb44', '', '', '2019-10-04 09:26:09', '2019-10-04 09:26:09', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1570181167__128B30EE-CC95-488A-B1BE-0484DCF9BB44.jpeg', 0, 'attachment', 'image/jpeg', 0),
(348, 17, '2019-09-25 04:48:49', '2019-09-25 04:48:49', '', 'bus_QR_nColor', '', 'inherit', 'closed', 'closed', '', 'bus_qr_ncolor', '', '', '2019-09-25 04:48:49', '2019-09-25 04:48:49', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/mohammad/1569386926__bus_QR_nColor.png', 0, 'attachment', 'image/png', 0),
(347, 18, '2019-09-20 06:55:07', '2019-09-20 06:55:07', '', '156896081118536539', '', 'inherit', 'closed', 'closed', '', '156896081118536539', '', '', '2019-09-20 06:55:07', '2019-09-20 06:55:07', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/shiv/1568962507__156896081118536539.jpeg', 0, 'attachment', 'image/jpeg', 0),
(295, 17, '2019-08-27 07:43:31', '2019-08-27 07:43:31', '', 'Car_Qr', '', 'inherit', 'closed', 'closed', '', 'car_qr', '', '', '2019-08-27 07:43:31', '2019-08-27 07:43:31', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/mohammad/1566891808__Car_Qr.jpg', 0, 'attachment', 'image/jpeg', 0),
(296, 17, '2019-08-27 10:05:16', '2019-08-27 10:05:16', '', 'house_small', '', 'inherit', 'closed', 'closed', '', 'house_small', '', '', '2019-08-27 10:05:16', '2019-08-27 10:05:16', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/mohammad/1566900312__house_small.jpg', 0, 'attachment', 'image/jpeg', 0),
(381, 4, '2019-10-04 09:24:22', '2019-10-04 09:24:22', '', '21B912B1-6ECE-4545-A71B-9B3B33B9C582', '', 'inherit', 'closed', 'closed', '', '21b912b1-6ece-4545-a71b-9b3b33b9c582', '', '', '2019-10-04 09:24:22', '2019-10-04 09:24:22', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1570181059__21B912B1-6ECE-4545-A71B-9B3B33B9C582.jpeg', 0, 'attachment', 'image/jpeg', 0),
(382, 4, '2019-10-04 09:24:40', '2019-10-04 09:24:40', '', 'B53EEC76-92A7-4C1A-B2B3-787E77C60474', '', 'inherit', 'closed', 'closed', '', 'b53eec76-92a7-4c1a-b2b3-787e77c60474', '', '', '2019-10-04 09:24:40', '2019-10-04 09:24:40', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1570181077__B53EEC76-92A7-4C1A-B2B3-787E77C60474.jpeg', 0, 'attachment', 'image/jpeg', 0),
(383, 4, '2019-10-04 09:25:02', '2019-10-04 09:25:02', '', '8DB9482B-412A-4705-AA5C-F687C75652EC', '', 'inherit', 'closed', 'closed', '', '8db9482b-412a-4705-aa5c-f687c75652ec', '', '', '2019-10-04 09:25:02', '2019-10-04 09:25:02', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1570181099__8DB9482B-412A-4705-AA5C-F687C75652EC.jpeg', 0, 'attachment', 'image/jpeg', 0),
(352, 15, '2019-10-03 09:23:15', '2019-10-03 09:23:15', '', 'bus_QR_n', '', 'inherit', 'closed', 'closed', '', 'bus_qr_n-2', '', '', '2019-10-03 09:23:15', '2019-10-03 09:23:15', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/ChicMic/1570094592__bus_QR_n.jpg', 0, 'attachment', 'image/jpeg', 0),
(357, 15, '2019-10-03 09:52:00', '2019-10-03 09:52:00', '', '', '', 'inherit', 'closed', 'closed', '', '357', '', '', '2019-10-03 09:52:00', '2019-10-03 09:52:00', '', 0, 'http://www.picture.hungenbach.de/wp-content/uploads/2019/10/tc.jpg', 0, 'attachment', 'image/jpeg', 0),
(358, 15, '2019-10-03 09:52:00', '2019-10-03 09:52:00', '', '', '', 'inherit', 'closed', 'closed', '', '358', '', '', '2019-10-03 09:52:00', '2019-10-03 09:52:00', '', 0, 'http://www.picture.hungenbach.de/wp-content/uploads/2019/10/hello_world.png', 0, 'attachment', 'image/png', 0),
(376, 4, '2019-10-04 09:16:19', '2019-10-04 09:16:19', '', 'C73CB81E-C396-4162-BAA3-DE348AD04CE8', '', 'inherit', 'closed', 'closed', '', 'c73cb81e-c396-4162-baa3-de348ad04ce8', '', '', '2019-10-04 09:16:19', '2019-10-04 09:16:19', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1570180576__C73CB81E-C396-4162-BAA3-DE348AD04CE8.jpeg', 0, 'attachment', 'image/jpeg', 0),
(377, 4, '2019-10-04 09:18:15', '2019-10-04 09:18:15', '', '3AD1C724-E63D-458C-9835-FF36B748DDEE', '', 'inherit', 'closed', 'closed', '', '3ad1c724-e63d-458c-9835-ff36b748ddee', '', '', '2019-10-04 09:18:15', '2019-10-04 09:18:15', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1570180692__3AD1C724-E63D-458C-9835-FF36B748DDEE.jpeg', 0, 'attachment', 'image/jpeg', 0),
(378, 4, '2019-10-04 09:21:21', '2019-10-04 09:21:21', '', '39B13CB8-661B-4886-8A96-4539372F8EA5', '', 'inherit', 'closed', 'closed', '', '39b13cb8-661b-4886-8a96-4539372f8ea5', '', '', '2019-10-04 09:21:21', '2019-10-04 09:21:21', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1570180878__39B13CB8-661B-4886-8A96-4539372F8EA5.jpeg', 0, 'attachment', 'image/jpeg', 0),
(549, 4, '2019-10-21 16:13:35', '2019-10-21 16:13:35', '', '4FAF2CC5-BBB4-4609-B722-FBC0A1EC87C3', '', 'inherit', 'closed', 'closed', '', '4faf2cc5-bbb4-4609-b722-fbc0a1ec87c3', '', '', '2019-10-21 16:13:35', '2019-10-21 16:13:35', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1571674413__4FAF2CC5-BBB4-4609-B722-FBC0A1EC87C3.jpeg', 0, 'attachment', 'image/jpeg', 0),
(550, 4, '2019-10-21 16:14:41', '2019-10-21 16:14:41', '', '1625BF89-19F0-4797-A828-8C7A087BF11F', '', 'inherit', 'closed', 'closed', '', '1625bf89-19f0-4797-a828-8c7a087bf11f', '', '', '2019-10-21 16:14:41', '2019-10-21 16:14:41', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1571674478__1625BF89-19F0-4797-A828-8C7A087BF11F.jpeg', 0, 'attachment', 'image/jpeg', 0),
(489, 4, '2019-10-17 16:08:42', '2019-10-17 16:08:42', '', '0B70EFC7-C967-4C96-B6DD-63FA4CC4D0A7', '', 'inherit', 'closed', 'closed', '', '0b70efc7-c967-4c96-b6dd-63fa4cc4d0a7', '', '', '2019-10-17 16:08:42', '2019-10-17 16:08:42', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1571328509__0B70EFC7-C967-4C96-B6DD-63FA4CC4D0A7.jpeg', 0, 'attachment', 'image/jpeg', 0),
(653, 4, '2019-10-24 08:41:03', '2019-10-24 08:41:03', '', '2163363B-90E8-4A13-8AB0-D67BAF27401E', '', 'inherit', 'closed', 'closed', '', '2163363b-90e8-4a13-8ab0-d67baf27401e', '', '', '2019-10-24 08:41:03', '2019-10-24 08:41:03', '', 0, '/opt/bitnami/apps/wordpress/htdocs/wp-content/uploads/user_uploads/markus.hu@gmx.de/1571906461__2163363B-90E8-4A13-8AB0-D67BAF27401E.jpeg', 0, 'attachment', 'image/jpeg', 0),
(658, 22, '2019-10-24 12:33:22', '2019-10-24 12:33:22', '', 'bus_QR__B', '', 'inherit', 'closed', 'closed', '', 'bus_qr__b-4', '', '', '2019-10-24 12:33:22', '2019-10-24 12:33:22', '', 0, '/var/www/html/hungenbach/wp-content/uploads/user_uploads/amy/1571920400__bus_QR__B.jpg', 0, 'attachment', 'image/jpeg', 0),
(665, 15, '2019-11-07 13:23:30', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-11-07 13:23:30', '0000-00-00 00:00:00', '', 0, 'http://localhost/picture_hungenbach/?p=665', 0, 'post', '', 0),
(681, 21, '2019-11-08 05:01:58', '2019-11-08 05:01:58', '', 'IMG_6204', '', 'inherit', 'closed', 'closed', '', 'img_6204', '', '', '2019-11-08 05:01:58', '2019-11-08 05:01:58', '', 0, '/var/www/html/picture_hungenbach/wp-content/uploads/user_uploads/amrish/1573189316__IMG_6204.JPG', 0, 'attachment', 'image/jpeg', 0),
(682, 21, '2019-11-08 05:02:50', '2019-11-08 05:02:50', '', 'IMG_6203', '', 'inherit', 'closed', 'closed', '', 'img_6203', '', '', '2019-11-08 05:02:50', '2019-11-08 05:02:50', '', 0, '/var/www/html/picture_hungenbach/wp-content/uploads/user_uploads/amrish/1573189367__IMG_6203.JPG', 0, 'attachment', 'image/jpeg', 0),
(683, 21, '2019-11-08 05:03:11', '2019-11-08 05:03:11', '', 'IMG_6196', '', 'inherit', 'closed', 'closed', '', 'img_6196', '', '', '2019-11-08 05:03:11', '2019-11-08 05:03:11', '', 0, '/var/www/html/picture_hungenbach/wp-content/uploads/user_uploads/amrish/1573189389__IMG_6196.jpg', 0, 'attachment', 'image/jpeg', 0),
(684, 21, '2019-11-08 05:03:38', '2019-11-08 05:03:38', '', 'IMG_6203_1', '', 'inherit', 'closed', 'closed', '', 'img_6203_1', '', '', '2019-11-08 05:03:38', '2019-11-08 05:03:38', '', 0, '/var/www/html/picture_hungenbach/wp-content/uploads/user_uploads/amrish/1573189415__IMG_6203_1.jpg', 0, 'attachment', 'image/jpeg', 0),
(685, 21, '2019-11-08 05:03:56', '2019-11-08 05:03:56', '', 'IMG_6204_1', '', 'inherit', 'closed', 'closed', '', 'img_6204_1', '', '', '2019-11-08 05:03:56', '2019-11-08 05:03:56', '', 0, '/var/www/html/picture_hungenbach/wp-content/uploads/user_uploads/amrish/1573189433__IMG_6204_1.jpg', 0, 'attachment', 'image/jpeg', 0),
(690, 21, '2019-11-08 06:20:03', '2019-11-08 06:20:03', '', 'IMG_6204', '', 'inherit', 'closed', 'closed', '', 'img_6204-3', '', '', '2019-11-08 06:20:03', '2019-11-08 06:20:03', '', 0, '/var/www/html/picture_hungenbach/wp-content/uploads/user_uploads/amrish/1573194000__IMG_6204.JPG', 0, 'attachment', 'image/jpeg', 0),
(691, 21, '2019-11-08 06:24:45', '2019-11-08 06:24:45', '', 'IMG_6204_1', '', 'inherit', 'closed', 'closed', '', 'img_6204_1-2', '', '', '2019-11-08 06:24:45', '2019-11-08 06:24:45', '', 0, '/var/www/html/picture_hungenbach/wp-content/uploads/user_uploads/amrish/1573194282__IMG_6204_1.jpg', 0, 'attachment', 'image/jpeg', 0),
(689, 21, '2019-11-08 06:07:55', '2019-11-08 06:07:55', '', 'IMG_6204', '', 'inherit', 'closed', 'closed', '', 'img_6204-2', '', '', '2019-11-08 06:07:55', '2019-11-08 06:07:55', '', 0, '/var/www/html/picture_hungenbach/wp-content/uploads/user_uploads/amrish/1573193272__IMG_6204.JPG', 0, 'attachment', 'image/jpeg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_rm_fields`
--

CREATE TABLE `wp_rm_fields` (
  `field_id` int(6) UNSIGNED NOT NULL,
  `form_id` int(6) DEFAULT NULL,
  `page_no` int(6) UNSIGNED NOT NULL DEFAULT '1',
  `field_label` text COLLATE utf8mb4_unicode_ci,
  `field_type` text COLLATE utf8mb4_unicode_ci,
  `field_value` mediumtext COLLATE utf8mb4_unicode_ci,
  `field_order` int(6) DEFAULT NULL,
  `field_show_on_user_page` tinyint(1) DEFAULT NULL,
  `is_field_primary` tinyint(1) NOT NULL DEFAULT '0',
  `field_is_editable` tinyint(1) NOT NULL DEFAULT '0',
  `is_deletion_allowed` tinyint(1) NOT NULL DEFAULT '1',
  `field_options` mediumtext COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_rm_fields`
--

INSERT INTO `wp_rm_fields` (`field_id`, `form_id`, `page_no`, `field_label`, `field_type`, `field_value`, `field_order`, `field_show_on_user_page`, `is_field_primary`, `field_is_editable`, `is_deletion_allowed`, `field_options`) VALUES
(1, 1, 1, 'Your Email', 'Email', NULL, 2, NULL, 1, 0, 1, '0'),
(2, 1, 1, 'Your Name', 'Textbox', NULL, 0, 1, 0, 0, 1, '0'),
(3, 1, 1, 'Your Phone Number', 'Number', NULL, 1, 1, 0, 1, 1, '0'),
(4, 1, 1, 'Message', 'Textarea', NULL, 3, NULL, 0, 0, 1, '0'),
(5, 2, 1, 'Email', 'Email', NULL, 3, NULL, 1, 0, 1, '0'),
(6, 2, 1, 'Username', 'Username', NULL, -2, 1, 1, 0, 1, 'O:8:\"stdClass\":125:{s:18:\"field_is_multiline\";N;s:17:\"field_placeholder\";s:8:\"Username\";s:14:\"field_timezone\";N;s:16:\"field_max_length\";i:70;s:23:\"field_is_required_range\";N;s:27:\"field_is_required_max_range\";N;s:27:\"field_is_required_min_range\";N;s:24:\"field_is_required_scroll\";N;s:19:\"field_default_value\";N;s:15:\"field_css_class\";s:22:\"rm_form_default_fields\";s:22:\"field_textarea_columns\";N;s:19:\"field_textarea_rows\";N;s:17:\"field_is_required\";i:1;s:21:\"field_is_show_asterix\";N;s:18:\"field_is_read_only\";i:0;s:21:\"field_is_other_option\";N;s:9:\"help_text\";N;s:4:\"icon\";N;s:16:\"field_validation\";N;s:17:\"custom_validation\";N;s:12:\"tnc_cb_label\";N;s:11:\"date_format\";N;s:15:\"field_is_unique\";N;s:10:\"un_err_msg\";N;s:11:\"rating_conf\";N;s:10:\"conditions\";N;s:14:\"field_meta_add\";N;s:16:\"link_same_window\";N;s:9:\"link_page\";N;s:9:\"link_href\";N;s:9:\"link_type\";N;s:12:\"yt_auto_play\";N;s:9:\"yt_repeat\";N;s:17:\"yt_related_videos\";N;s:15:\"yt_player_width\";N;s:9:\"rm_widget\";N;s:16:\"yt_player_height\";N;s:8:\"if_width\";N;s:9:\"if_height\";N;s:18:\"field_address_type\";N;s:23:\"field_ca_address1_label\";N;s:23:\"field_ca_address2_label\";N;s:19:\"field_ca_city_label\";N;s:20:\"field_ca_state_label\";N;s:22:\"field_ca_country_label\";N;s:18:\"field_ca_zip_label\";N;s:16:\"field_ca_city_en\";N;s:17:\"field_ca_state_en\";N;s:15:\"field_ca_zip_en\";N;s:19:\"field_ca_country_en\";N;s:20:\"field_ca_address1_en\";N;s:20:\"field_ca_address2_en\";N;s:13:\"ca_state_type\";N;s:21:\"field_ca_address1_req\";N;s:17:\"field_ca_city_req\";N;s:16:\"field_ca_zip_req\";N;s:20:\"field_ca_country_req\";N;s:18:\"field_ca_state_req\";N;s:21:\"field_ca_address2_req\";N;s:29:\"field_ca_label_as_placeholder\";N;s:20:\"field_ca_lmark_label\";N;s:17:\"field_ca_lmark_en\";N;s:18:\"field_ca_lmark_req\";N;s:20:\"field_ca_state_codes\";N;s:28:\"field_ca_country_america_can\";N;s:24:\"field_ca_country_limited\";N;s:26:\"field_ca_en_country_search\";N;s:19:\"img_caption_enabled\";N;s:17:\"img_title_enabled\";N;s:16:\"img_link_enabled\";N;s:19:\"img_effects_enabled\";N;s:12:\"border_color\";N;s:12:\"border_width\";N;s:12:\"border_shape\";N;s:15:\"img_pop_enabled\";N;s:8:\"img_size\";N;s:3:\"lat\";N;s:4:\"long\";N;s:4:\"zoom\";N;s:5:\"width\";N;s:13:\"nu_form_views\";N;s:20:\"nu_views_text_before\";N;s:19:\"nu_views_text_after\";N;s:17:\"nu_sub_text_after\";N;s:18:\"nu_sub_text_before\";N;s:14:\"nu_submissions\";N;s:10:\"sub_limits\";N;s:21:\"sub_limit_text_before\";N;s:20:\"sub_limit_text_after\";N;s:15:\"sub_date_limits\";N;s:26:\"sub_date_limit_text_before\";N;s:25:\"sub_date_limit_text_after\";N;s:12:\"last_sub_rec\";N;s:14:\"ls_text_before\";N;s:13:\"ls_text_after\";N;s:14:\"show_form_name\";N;s:9:\"form_desc\";N;s:13:\"sub_limit_ind\";N;s:12:\"custom_value\";N;s:12:\"hide_country\";N;s:9:\"hide_date\";N;s:13:\"show_gravatar\";N;s:9:\"max_items\";N;s:10:\"time_range\";N;s:17:\"user_exists_error\";s:69:\"This username has already been taken. Please try something different.\";s:19:\"username_characters\";a:4:{i:0;s:9:\"alphabets\";i:1;s:7:\"numbers\";i:2;s:11:\"underscores\";i:3;s:7:\"periods\";}s:23:\"invalid_username_format\";s:60:\"Invalid username format. Only {{allowed_characters}} allowed\";s:14:\"en_confirm_pwd\";N;s:17:\"pass_mismatch_err\";N;s:16:\"en_pass_strength\";N;s:17:\"pwd_strength_type\";N;s:13:\"pwd_short_msg\";N;s:12:\"pwd_weak_msg\";N;s:14:\"pwd_medium_msg\";N;s:14:\"pwd_strong_msg\";N;s:11:\"format_type\";N;s:19:\"preferred_countries\";N;s:8:\"en_geoip\";N;s:20:\"custom_mobile_format\";N;s:13:\"lim_countries\";N;s:18:\"lim_pref_countries\";N;s:14:\"mobile_err_msg\";N;s:13:\"country_field\";N;s:12:\"sync_country\";N;s:13:\"country_match\";N;}'),
(7, 2, 1, 'Password', 'UserPassword', NULL, -1, 0, 1, 0, 1, 'O:8:\"stdClass\":125:{s:18:\"field_is_multiline\";N;s:17:\"field_placeholder\";s:8:\"Password\";s:14:\"field_timezone\";N;s:16:\"field_max_length\";N;s:23:\"field_is_required_range\";N;s:27:\"field_is_required_max_range\";N;s:27:\"field_is_required_min_range\";N;s:24:\"field_is_required_scroll\";N;s:19:\"field_default_value\";N;s:15:\"field_css_class\";s:22:\"rm_form_default_fields\";s:22:\"field_textarea_columns\";N;s:19:\"field_textarea_rows\";N;s:17:\"field_is_required\";i:1;s:21:\"field_is_show_asterix\";N;s:18:\"field_is_read_only\";i:0;s:21:\"field_is_other_option\";N;s:9:\"help_text\";s:44:\"Password must be at least 7 characters long.\";s:4:\"icon\";N;s:16:\"field_validation\";N;s:17:\"custom_validation\";N;s:12:\"tnc_cb_label\";N;s:11:\"date_format\";N;s:15:\"field_is_unique\";N;s:10:\"un_err_msg\";N;s:11:\"rating_conf\";N;s:10:\"conditions\";N;s:14:\"field_meta_add\";N;s:16:\"link_same_window\";N;s:9:\"link_page\";N;s:9:\"link_href\";N;s:9:\"link_type\";N;s:12:\"yt_auto_play\";N;s:9:\"yt_repeat\";N;s:17:\"yt_related_videos\";N;s:15:\"yt_player_width\";N;s:9:\"rm_widget\";N;s:16:\"yt_player_height\";N;s:8:\"if_width\";N;s:9:\"if_height\";N;s:18:\"field_address_type\";N;s:23:\"field_ca_address1_label\";N;s:23:\"field_ca_address2_label\";N;s:19:\"field_ca_city_label\";N;s:20:\"field_ca_state_label\";N;s:22:\"field_ca_country_label\";N;s:18:\"field_ca_zip_label\";N;s:16:\"field_ca_city_en\";N;s:17:\"field_ca_state_en\";N;s:15:\"field_ca_zip_en\";N;s:19:\"field_ca_country_en\";N;s:20:\"field_ca_address1_en\";N;s:20:\"field_ca_address2_en\";N;s:13:\"ca_state_type\";N;s:21:\"field_ca_address1_req\";N;s:17:\"field_ca_city_req\";N;s:16:\"field_ca_zip_req\";N;s:20:\"field_ca_country_req\";N;s:18:\"field_ca_state_req\";N;s:21:\"field_ca_address2_req\";N;s:29:\"field_ca_label_as_placeholder\";N;s:20:\"field_ca_lmark_label\";N;s:17:\"field_ca_lmark_en\";N;s:18:\"field_ca_lmark_req\";N;s:20:\"field_ca_state_codes\";N;s:28:\"field_ca_country_america_can\";N;s:24:\"field_ca_country_limited\";N;s:26:\"field_ca_en_country_search\";N;s:19:\"img_caption_enabled\";N;s:17:\"img_title_enabled\";N;s:16:\"img_link_enabled\";N;s:19:\"img_effects_enabled\";N;s:12:\"border_color\";N;s:12:\"border_width\";N;s:12:\"border_shape\";N;s:15:\"img_pop_enabled\";N;s:8:\"img_size\";N;s:3:\"lat\";N;s:4:\"long\";N;s:4:\"zoom\";N;s:5:\"width\";N;s:13:\"nu_form_views\";N;s:20:\"nu_views_text_before\";N;s:19:\"nu_views_text_after\";N;s:17:\"nu_sub_text_after\";N;s:18:\"nu_sub_text_before\";N;s:14:\"nu_submissions\";N;s:10:\"sub_limits\";N;s:21:\"sub_limit_text_before\";N;s:20:\"sub_limit_text_after\";N;s:15:\"sub_date_limits\";N;s:26:\"sub_date_limit_text_before\";N;s:25:\"sub_date_limit_text_after\";N;s:12:\"last_sub_rec\";N;s:14:\"ls_text_before\";N;s:13:\"ls_text_after\";N;s:14:\"show_form_name\";N;s:9:\"form_desc\";N;s:13:\"sub_limit_ind\";N;s:12:\"custom_value\";N;s:12:\"hide_country\";N;s:9:\"hide_date\";N;s:13:\"show_gravatar\";N;s:9:\"max_items\";N;s:10:\"time_range\";N;s:17:\"user_exists_error\";N;s:19:\"username_characters\";N;s:23:\"invalid_username_format\";N;s:14:\"en_confirm_pwd\";i:1;s:17:\"pass_mismatch_err\";s:48:\"Your passwords do not match. Please check again.\";s:16:\"en_pass_strength\";i:1;s:17:\"pwd_strength_type\";i:1;s:13:\"pwd_short_msg\";s:9:\"Too Short\";s:12:\"pwd_weak_msg\";s:4:\"Weak\";s:14:\"pwd_medium_msg\";s:6:\"Medium\";s:14:\"pwd_strong_msg\";s:6:\"Strong\";s:11:\"format_type\";N;s:19:\"preferred_countries\";N;s:8:\"en_geoip\";N;s:20:\"custom_mobile_format\";N;s:13:\"lim_countries\";N;s:18:\"lim_pref_countries\";N;s:14:\"mobile_err_msg\";N;s:13:\"country_field\";N;s:12:\"sync_country\";N;s:13:\"country_match\";N;}'),
(8, 2, 1, 'First Name', 'Fname', NULL, 1, NULL, 0, 0, 1, '0'),
(9, 2, 1, 'Last Name', 'Lname', NULL, 2, NULL, 0, 0, 1, '0'),
(10, 2, 1, 'Website', 'Website', NULL, 4, NULL, 0, 1, 1, '0'),
(11, 2, 1, 'Do you agree with our terms and conditions?', 'Terms', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tempor metus nec elit auctor venenatis non facilisis nibh. In lorem neque, vulputate id metus id, rhoncus convallis eros. In urna erat, accumsan quis pretium nec, posuere in arcu. Nulla eleifend aliquet accumsan. Pellentesque consectetur sollicitudin orci nec suscipit. Donec sit amet risus suscipit, gravida nulla semper, interdum tellus. In cursus ultricies turpis, quis suscipit massa faucibus et. Vestibulum euismod est ac vehicula viverra. Aliquam quis est quis eros feugiat varius a non ligula.', 6, NULL, 0, 0, 1, '0'),
(12, 2, 1, 'Divider', 'Divider', NULL, 0, NULL, 0, 0, 1, '0'),
(13, 2, 1, 'Divider', 'Divider', NULL, 5, NULL, 0, 0, 1, '0'),
(14, 2, 1, 'Divider', 'Divider', NULL, 7, NULL, 0, 0, 1, '0'),
(18, 4, 1, 'Username', 'Username', NULL, 0, 1, 1, 0, 1, 'O:8:\"stdClass\":207:{s:18:\"field_is_multiline\";N;s:17:\"field_placeholder\";s:8:\"Username\";s:14:\"field_timezone\";N;s:16:\"field_max_length\";i:70;s:23:\"field_is_required_range\";N;s:27:\"field_is_required_max_range\";N;s:27:\"field_is_required_min_range\";N;s:24:\"field_is_required_scroll\";N;s:19:\"field_default_value\";N;s:15:\"field_css_class\";s:22:\"rm_form_default_fields\";s:22:\"field_textarea_columns\";N;s:19:\"field_textarea_rows\";N;s:17:\"field_is_required\";i:1;s:21:\"field_is_show_asterix\";N;s:18:\"field_is_read_only\";i:0;s:21:\"field_is_other_option\";N;s:9:\"help_text\";N;s:4:\"icon\";N;s:16:\"field_validation\";N;s:17:\"custom_validation\";N;s:12:\"tnc_cb_label\";N;s:11:\"date_format\";N;s:15:\"field_is_unique\";N;s:10:\"un_err_msg\";N;s:11:\"rating_conf\";N;s:10:\"conditions\";N;s:14:\"field_meta_add\";N;s:16:\"link_same_window\";N;s:9:\"link_page\";N;s:9:\"link_href\";N;s:9:\"link_type\";N;s:12:\"yt_auto_play\";N;s:9:\"yt_repeat\";N;s:17:\"yt_related_videos\";N;s:15:\"yt_player_width\";N;s:9:\"rm_widget\";N;s:16:\"yt_player_height\";N;s:8:\"if_width\";N;s:9:\"if_height\";N;s:18:\"field_address_type\";N;s:23:\"field_ca_address1_label\";N;s:23:\"field_ca_address2_label\";N;s:19:\"field_ca_city_label\";N;s:20:\"field_ca_state_label\";N;s:22:\"field_ca_country_label\";N;s:18:\"field_ca_zip_label\";N;s:16:\"field_ca_city_en\";N;s:17:\"field_ca_state_en\";N;s:15:\"field_ca_zip_en\";N;s:19:\"field_ca_country_en\";N;s:20:\"field_ca_address1_en\";N;s:20:\"field_ca_address2_en\";N;s:13:\"ca_state_type\";N;s:21:\"field_ca_address1_req\";N;s:17:\"field_ca_city_req\";N;s:16:\"field_ca_zip_req\";N;s:20:\"field_ca_country_req\";N;s:18:\"field_ca_state_req\";N;s:21:\"field_ca_address2_req\";N;s:29:\"field_ca_label_as_placeholder\";N;s:20:\"field_ca_lmark_label\";N;s:17:\"field_ca_lmark_en\";N;s:18:\"field_ca_lmark_req\";N;s:20:\"field_ca_state_codes\";N;s:28:\"field_ca_country_america_can\";N;s:24:\"field_ca_country_limited\";N;s:26:\"field_ca_en_country_search\";N;s:19:\"img_caption_enabled\";N;s:17:\"img_title_enabled\";N;s:16:\"img_link_enabled\";N;s:19:\"img_effects_enabled\";N;s:12:\"border_color\";N;s:12:\"border_width\";N;s:12:\"border_shape\";N;s:15:\"img_pop_enabled\";N;s:8:\"img_size\";N;s:3:\"lat\";N;s:4:\"long\";N;s:4:\"zoom\";N;s:5:\"width\";N;s:13:\"nu_form_views\";N;s:20:\"nu_views_text_before\";N;s:19:\"nu_views_text_after\";N;s:17:\"nu_sub_text_after\";N;s:18:\"nu_sub_text_before\";N;s:14:\"nu_submissions\";N;s:10:\"sub_limits\";N;s:21:\"sub_limit_text_before\";N;s:20:\"sub_limit_text_after\";N;s:15:\"sub_date_limits\";N;s:26:\"sub_date_limit_text_before\";N;s:25:\"sub_date_limit_text_after\";N;s:12:\"last_sub_rec\";N;s:14:\"ls_text_before\";N;s:13:\"ls_text_after\";N;s:14:\"show_form_name\";N;s:9:\"form_desc\";N;s:13:\"sub_limit_ind\";N;s:12:\"custom_value\";N;s:12:\"hide_country\";N;s:9:\"hide_date\";N;s:13:\"show_gravatar\";N;s:9:\"max_items\";N;s:10:\"time_range\";N;s:17:\"user_exists_error\";s:69:\"This username has already been taken. Please try something different.\";s:19:\"username_characters\";a:4:{i:0;s:9:\"alphabets\";i:1;s:7:\"numbers\";i:2;s:11:\"underscores\";i:3;s:7:\"periods\";}s:23:\"invalid_username_format\";s:60:\"Invalid username format. Only {{allowed_characters}} allowed\";s:14:\"en_confirm_pwd\";N;s:17:\"pass_mismatch_err\";N;s:16:\"en_pass_strength\";N;s:17:\"pwd_strength_type\";N;s:13:\"pwd_short_msg\";N;s:12:\"pwd_weak_msg\";N;s:14:\"pwd_medium_msg\";N;s:14:\"pwd_strong_msg\";N;s:11:\"format_type\";N;s:19:\"preferred_countries\";N;s:8:\"en_geoip\";N;s:20:\"custom_mobile_format\";N;s:13:\"lim_countries\";N;s:18:\"lim_pref_countries\";N;s:14:\"mobile_err_msg\";N;s:13:\"country_field\";N;s:12:\"sync_country\";N;s:13:\"country_match\";N;s:19:\"privacy_policy_page\";N;s:22:\"privacy_policy_content\";N;s:24:\"privacy_display_checkbox\";N;s:26:\"field_wcb_email_as_primary\";N;s:25:\"field_wcb_firstname_label\";N;s:24:\"field_wcb_lastname_label\";N;s:23:\"field_wcb_company_label\";N;s:24:\"field_wcb_address1_label\";N;s:24:\"field_wcb_address2_label\";N;s:21:\"field_wcb_phone_label\";N;s:21:\"field_wcb_email_label\";N;s:20:\"field_wcb_city_label\";N;s:21:\"field_wcb_state_label\";N;s:23:\"field_wcb_country_label\";N;s:19:\"field_wcb_zip_label\";N;s:17:\"field_wcb_city_en\";N;s:18:\"field_wcb_state_en\";N;s:16:\"field_wcb_zip_en\";N;s:20:\"field_wcb_country_en\";N;s:22:\"field_wcb_firstname_en\";N;s:21:\"field_wcb_lastname_en\";N;s:20:\"field_wcb_company_en\";N;s:21:\"field_wcb_address1_en\";N;s:21:\"field_wcb_address2_en\";N;s:18:\"field_wcb_phone_en\";N;s:18:\"field_wcb_email_en\";N;s:14:\"wcb_state_type\";N;s:23:\"field_wcb_firstname_req\";N;s:22:\"field_wcb_lastname_req\";N;s:21:\"field_wcb_company_req\";N;s:22:\"field_wcb_address1_req\";N;s:19:\"field_wcb_phone_req\";N;s:19:\"field_wcb_email_req\";N;s:18:\"field_wcb_city_req\";N;s:17:\"field_wcb_zip_req\";N;s:21:\"field_wcb_country_req\";N;s:19:\"field_wcb_state_req\";N;s:22:\"field_wcb_address2_req\";N;s:30:\"field_wcb_label_as_placeholder\";N;s:21:\"field_wcb_lmark_label\";N;s:18:\"field_wcb_lmark_en\";N;s:19:\"field_wcb_lmark_req\";N;s:21:\"field_wcb_state_codes\";N;s:30:\"field_wcb_country_ameriwcb_can\";N;s:25:\"field_wcb_country_limited\";N;s:27:\"field_wcb_en_country_search\";N;s:25:\"field_wcs_firstname_label\";N;s:24:\"field_wcs_lastname_label\";N;s:23:\"field_wcs_company_label\";N;s:24:\"field_wcs_address1_label\";N;s:24:\"field_wcs_address2_label\";N;s:20:\"field_wcs_city_label\";N;s:21:\"field_wcs_state_label\";N;s:23:\"field_wcs_country_label\";N;s:19:\"field_wcs_zip_label\";N;s:17:\"field_wcs_city_en\";N;s:18:\"field_wcs_state_en\";N;s:16:\"field_wcs_zip_en\";N;s:20:\"field_wcs_country_en\";N;s:21:\"field_wcs_address1_en\";N;s:21:\"field_wcs_address2_en\";N;s:14:\"wcs_state_type\";N;s:22:\"field_wcs_firstname_en\";N;s:21:\"field_wcs_lastname_en\";N;s:20:\"field_wcs_company_en\";N;s:22:\"field_wcs_address1_req\";N;s:18:\"field_wcs_city_req\";N;s:17:\"field_wcs_zip_req\";N;s:21:\"field_wcs_country_req\";N;s:19:\"field_wcs_state_req\";N;s:23:\"field_wcs_firstname_req\";N;s:22:\"field_wcs_lastname_req\";N;s:21:\"field_wcs_company_req\";N;s:22:\"field_wcs_address2_req\";N;s:30:\"field_wcs_label_as_placeholder\";N;s:21:\"field_wcs_lmark_label\";N;s:18:\"field_wcs_lmark_en\";N;s:19:\"field_wcs_lmark_req\";N;s:21:\"field_wcs_state_codes\";N;s:30:\"field_wcs_country_ameriwcs_can\";N;s:25:\"field_wcs_country_limited\";N;s:27:\"field_wcs_en_country_search\";N;}'),
(19, 4, 1, 'Password', 'UserPassword', NULL, 2, 0, 1, 0, 1, 'O:8:\"stdClass\":207:{s:18:\"field_is_multiline\";N;s:17:\"field_placeholder\";s:8:\"Password\";s:14:\"field_timezone\";N;s:16:\"field_max_length\";N;s:23:\"field_is_required_range\";N;s:27:\"field_is_required_max_range\";N;s:27:\"field_is_required_min_range\";N;s:24:\"field_is_required_scroll\";N;s:19:\"field_default_value\";N;s:15:\"field_css_class\";s:22:\"rm_form_default_fields\";s:22:\"field_textarea_columns\";N;s:19:\"field_textarea_rows\";N;s:17:\"field_is_required\";i:1;s:21:\"field_is_show_asterix\";N;s:18:\"field_is_read_only\";i:0;s:21:\"field_is_other_option\";N;s:9:\"help_text\";s:44:\"Password must be at least 7 characters long.\";s:4:\"icon\";N;s:16:\"field_validation\";N;s:17:\"custom_validation\";N;s:12:\"tnc_cb_label\";N;s:11:\"date_format\";N;s:15:\"field_is_unique\";N;s:10:\"un_err_msg\";N;s:11:\"rating_conf\";N;s:10:\"conditions\";N;s:14:\"field_meta_add\";N;s:16:\"link_same_window\";N;s:9:\"link_page\";N;s:9:\"link_href\";N;s:9:\"link_type\";N;s:12:\"yt_auto_play\";N;s:9:\"yt_repeat\";N;s:17:\"yt_related_videos\";N;s:15:\"yt_player_width\";N;s:9:\"rm_widget\";N;s:16:\"yt_player_height\";N;s:8:\"if_width\";N;s:9:\"if_height\";N;s:18:\"field_address_type\";N;s:23:\"field_ca_address1_label\";N;s:23:\"field_ca_address2_label\";N;s:19:\"field_ca_city_label\";N;s:20:\"field_ca_state_label\";N;s:22:\"field_ca_country_label\";N;s:18:\"field_ca_zip_label\";N;s:16:\"field_ca_city_en\";N;s:17:\"field_ca_state_en\";N;s:15:\"field_ca_zip_en\";N;s:19:\"field_ca_country_en\";N;s:20:\"field_ca_address1_en\";N;s:20:\"field_ca_address2_en\";N;s:13:\"ca_state_type\";N;s:21:\"field_ca_address1_req\";N;s:17:\"field_ca_city_req\";N;s:16:\"field_ca_zip_req\";N;s:20:\"field_ca_country_req\";N;s:18:\"field_ca_state_req\";N;s:21:\"field_ca_address2_req\";N;s:29:\"field_ca_label_as_placeholder\";N;s:20:\"field_ca_lmark_label\";N;s:17:\"field_ca_lmark_en\";N;s:18:\"field_ca_lmark_req\";N;s:20:\"field_ca_state_codes\";N;s:28:\"field_ca_country_america_can\";N;s:24:\"field_ca_country_limited\";N;s:26:\"field_ca_en_country_search\";N;s:19:\"img_caption_enabled\";N;s:17:\"img_title_enabled\";N;s:16:\"img_link_enabled\";N;s:19:\"img_effects_enabled\";N;s:12:\"border_color\";N;s:12:\"border_width\";N;s:12:\"border_shape\";N;s:15:\"img_pop_enabled\";N;s:8:\"img_size\";N;s:3:\"lat\";N;s:4:\"long\";N;s:4:\"zoom\";N;s:5:\"width\";N;s:13:\"nu_form_views\";N;s:20:\"nu_views_text_before\";N;s:19:\"nu_views_text_after\";N;s:17:\"nu_sub_text_after\";N;s:18:\"nu_sub_text_before\";N;s:14:\"nu_submissions\";N;s:10:\"sub_limits\";N;s:21:\"sub_limit_text_before\";N;s:20:\"sub_limit_text_after\";N;s:15:\"sub_date_limits\";N;s:26:\"sub_date_limit_text_before\";N;s:25:\"sub_date_limit_text_after\";N;s:12:\"last_sub_rec\";N;s:14:\"ls_text_before\";N;s:13:\"ls_text_after\";N;s:14:\"show_form_name\";N;s:9:\"form_desc\";N;s:13:\"sub_limit_ind\";N;s:12:\"custom_value\";N;s:12:\"hide_country\";N;s:9:\"hide_date\";N;s:13:\"show_gravatar\";N;s:9:\"max_items\";N;s:10:\"time_range\";N;s:17:\"user_exists_error\";N;s:19:\"username_characters\";N;s:23:\"invalid_username_format\";N;s:14:\"en_confirm_pwd\";i:1;s:17:\"pass_mismatch_err\";s:48:\"Your passwords do not match. Please check again.\";s:16:\"en_pass_strength\";i:1;s:17:\"pwd_strength_type\";i:1;s:13:\"pwd_short_msg\";s:9:\"Too Short\";s:12:\"pwd_weak_msg\";s:4:\"Weak\";s:14:\"pwd_medium_msg\";s:6:\"Medium\";s:14:\"pwd_strong_msg\";s:6:\"Strong\";s:11:\"format_type\";N;s:19:\"preferred_countries\";N;s:8:\"en_geoip\";N;s:20:\"custom_mobile_format\";N;s:13:\"lim_countries\";N;s:18:\"lim_pref_countries\";N;s:14:\"mobile_err_msg\";N;s:13:\"country_field\";N;s:12:\"sync_country\";N;s:13:\"country_match\";N;s:19:\"privacy_policy_page\";N;s:22:\"privacy_policy_content\";N;s:24:\"privacy_display_checkbox\";N;s:26:\"field_wcb_email_as_primary\";N;s:25:\"field_wcb_firstname_label\";N;s:24:\"field_wcb_lastname_label\";N;s:23:\"field_wcb_company_label\";N;s:24:\"field_wcb_address1_label\";N;s:24:\"field_wcb_address2_label\";N;s:21:\"field_wcb_phone_label\";N;s:21:\"field_wcb_email_label\";N;s:20:\"field_wcb_city_label\";N;s:21:\"field_wcb_state_label\";N;s:23:\"field_wcb_country_label\";N;s:19:\"field_wcb_zip_label\";N;s:17:\"field_wcb_city_en\";N;s:18:\"field_wcb_state_en\";N;s:16:\"field_wcb_zip_en\";N;s:20:\"field_wcb_country_en\";N;s:22:\"field_wcb_firstname_en\";N;s:21:\"field_wcb_lastname_en\";N;s:20:\"field_wcb_company_en\";N;s:21:\"field_wcb_address1_en\";N;s:21:\"field_wcb_address2_en\";N;s:18:\"field_wcb_phone_en\";N;s:18:\"field_wcb_email_en\";N;s:14:\"wcb_state_type\";N;s:23:\"field_wcb_firstname_req\";N;s:22:\"field_wcb_lastname_req\";N;s:21:\"field_wcb_company_req\";N;s:22:\"field_wcb_address1_req\";N;s:19:\"field_wcb_phone_req\";N;s:19:\"field_wcb_email_req\";N;s:18:\"field_wcb_city_req\";N;s:17:\"field_wcb_zip_req\";N;s:21:\"field_wcb_country_req\";N;s:19:\"field_wcb_state_req\";N;s:22:\"field_wcb_address2_req\";N;s:30:\"field_wcb_label_as_placeholder\";N;s:21:\"field_wcb_lmark_label\";N;s:18:\"field_wcb_lmark_en\";N;s:19:\"field_wcb_lmark_req\";N;s:21:\"field_wcb_state_codes\";N;s:30:\"field_wcb_country_ameriwcb_can\";N;s:25:\"field_wcb_country_limited\";N;s:27:\"field_wcb_en_country_search\";N;s:25:\"field_wcs_firstname_label\";N;s:24:\"field_wcs_lastname_label\";N;s:23:\"field_wcs_company_label\";N;s:24:\"field_wcs_address1_label\";N;s:24:\"field_wcs_address2_label\";N;s:20:\"field_wcs_city_label\";N;s:21:\"field_wcs_state_label\";N;s:23:\"field_wcs_country_label\";N;s:19:\"field_wcs_zip_label\";N;s:17:\"field_wcs_city_en\";N;s:18:\"field_wcs_state_en\";N;s:16:\"field_wcs_zip_en\";N;s:20:\"field_wcs_country_en\";N;s:21:\"field_wcs_address1_en\";N;s:21:\"field_wcs_address2_en\";N;s:14:\"wcs_state_type\";N;s:22:\"field_wcs_firstname_en\";N;s:21:\"field_wcs_lastname_en\";N;s:20:\"field_wcs_company_en\";N;s:22:\"field_wcs_address1_req\";N;s:18:\"field_wcs_city_req\";N;s:17:\"field_wcs_zip_req\";N;s:21:\"field_wcs_country_req\";N;s:19:\"field_wcs_state_req\";N;s:23:\"field_wcs_firstname_req\";N;s:22:\"field_wcs_lastname_req\";N;s:21:\"field_wcs_company_req\";N;s:22:\"field_wcs_address2_req\";N;s:30:\"field_wcs_label_as_placeholder\";N;s:21:\"field_wcs_lmark_label\";N;s:18:\"field_wcs_lmark_en\";N;s:19:\"field_wcs_lmark_req\";N;s:21:\"field_wcs_state_codes\";N;s:30:\"field_wcs_country_ameriwcs_can\";N;s:25:\"field_wcs_country_limited\";N;s:27:\"field_wcs_en_country_search\";N;}'),
(23, 4, 1, 'Email', 'Email', NULL, 1, NULL, 1, 0, 0, 'O:8:\"stdClass\":204:{s:18:\"field_is_multiline\";N;s:17:\"field_placeholder\";s:0:\"\";s:14:\"field_timezone\";N;s:16:\"field_max_length\";N;s:23:\"field_is_required_range\";N;s:27:\"field_is_required_max_range\";N;s:27:\"field_is_required_min_range\";N;s:24:\"field_is_required_scroll\";N;s:19:\"field_default_value\";N;s:15:\"field_css_class\";s:0:\"\";s:22:\"field_textarea_columns\";N;s:19:\"field_textarea_rows\";N;s:17:\"field_is_required\";s:1:\"1\";s:21:\"field_is_show_asterix\";N;s:18:\"field_is_read_only\";N;s:21:\"field_is_other_option\";N;s:9:\"help_text\";s:0:\"\";s:4:\"icon\";O:8:\"stdClass\":4:{s:9:\"codepoint\";s:0:\"\";s:8:\"fg_color\";s:6:\"000000\";s:8:\"bg_color\";s:6:\"FFFFFF\";s:5:\"shape\";s:6:\"square\";}s:16:\"field_validation\";N;s:17:\"custom_validation\";N;s:12:\"tnc_cb_label\";N;s:11:\"date_format\";N;s:14:\"field_meta_add\";N;s:10:\"conditions\";a:2:{s:5:\"rules\";a:0:{}s:8:\"settings\";a:0:{}}s:16:\"link_same_window\";N;s:9:\"link_page\";N;s:9:\"link_href\";N;s:9:\"link_type\";N;s:12:\"yt_auto_play\";N;s:9:\"yt_repeat\";N;s:17:\"yt_related_videos\";N;s:15:\"yt_player_width\";N;s:9:\"rm_widget\";N;s:16:\"yt_player_height\";N;s:8:\"if_width\";N;s:9:\"if_height\";N;s:18:\"field_address_type\";N;s:23:\"field_ca_address1_label\";N;s:23:\"field_ca_address2_label\";N;s:19:\"field_ca_city_label\";N;s:20:\"field_ca_state_label\";N;s:22:\"field_ca_country_label\";N;s:18:\"field_ca_zip_label\";N;s:16:\"field_ca_city_en\";N;s:17:\"field_ca_state_en\";N;s:15:\"field_ca_zip_en\";N;s:19:\"field_ca_country_en\";N;s:20:\"field_ca_address1_en\";N;s:20:\"field_ca_address2_en\";N;s:13:\"ca_state_type\";N;s:21:\"field_ca_address1_req\";N;s:17:\"field_ca_city_req\";N;s:16:\"field_ca_zip_req\";N;s:20:\"field_ca_country_req\";N;s:18:\"field_ca_state_req\";N;s:21:\"field_ca_address2_req\";N;s:29:\"field_ca_label_as_placeholder\";N;s:20:\"field_ca_lmark_label\";N;s:17:\"field_ca_lmark_en\";N;s:18:\"field_ca_lmark_req\";N;s:20:\"field_ca_state_codes\";N;s:28:\"field_ca_country_america_can\";N;s:24:\"field_ca_country_limited\";N;s:26:\"field_ca_en_country_search\";N;s:19:\"img_caption_enabled\";N;s:17:\"img_title_enabled\";N;s:16:\"img_link_enabled\";N;s:19:\"img_effects_enabled\";N;s:12:\"border_color\";N;s:12:\"border_width\";N;s:12:\"border_shape\";N;s:15:\"img_pop_enabled\";N;s:8:\"img_size\";N;s:3:\"lat\";N;s:4:\"long\";N;s:4:\"zoom\";N;s:5:\"width\";N;s:13:\"nu_form_views\";N;s:20:\"nu_views_text_before\";N;s:19:\"nu_views_text_after\";N;s:17:\"nu_sub_text_after\";N;s:18:\"nu_sub_text_before\";N;s:14:\"nu_submissions\";N;s:10:\"sub_limits\";N;s:21:\"sub_limit_text_before\";N;s:20:\"sub_limit_text_after\";N;s:15:\"sub_date_limits\";N;s:26:\"sub_date_limit_text_before\";N;s:25:\"sub_date_limit_text_after\";N;s:12:\"last_sub_rec\";N;s:14:\"ls_text_before\";N;s:13:\"ls_text_after\";N;s:14:\"show_form_name\";N;s:9:\"form_desc\";N;s:13:\"sub_limit_ind\";N;s:12:\"custom_value\";N;s:12:\"hide_country\";N;s:9:\"hide_date\";N;s:13:\"show_gravatar\";N;s:9:\"max_items\";N;s:10:\"time_range\";N;s:17:\"user_exists_error\";N;s:19:\"username_characters\";N;s:23:\"invalid_username_format\";N;s:14:\"en_confirm_pwd\";N;s:17:\"pass_mismatch_err\";N;s:16:\"en_pass_strength\";N;s:17:\"pwd_strength_type\";N;s:13:\"pwd_short_msg\";N;s:12:\"pwd_weak_msg\";N;s:14:\"pwd_medium_msg\";N;s:14:\"pwd_strong_msg\";N;s:11:\"format_type\";N;s:19:\"preferred_countries\";N;s:8:\"en_geoip\";N;s:20:\"custom_mobile_format\";N;s:13:\"lim_countries\";N;s:18:\"lim_pref_countries\";N;s:14:\"mobile_err_msg\";N;s:13:\"country_field\";N;s:12:\"sync_country\";N;s:13:\"country_match\";N;s:19:\"privacy_policy_page\";N;s:22:\"privacy_policy_content\";N;s:24:\"privacy_display_checkbox\";N;s:26:\"field_wcb_email_as_primary\";N;s:25:\"field_wcb_firstname_label\";N;s:24:\"field_wcb_lastname_label\";N;s:23:\"field_wcb_company_label\";N;s:24:\"field_wcb_address1_label\";N;s:24:\"field_wcb_address2_label\";N;s:21:\"field_wcb_phone_label\";N;s:21:\"field_wcb_email_label\";N;s:20:\"field_wcb_city_label\";N;s:21:\"field_wcb_state_label\";N;s:23:\"field_wcb_country_label\";N;s:19:\"field_wcb_zip_label\";N;s:17:\"field_wcb_city_en\";N;s:18:\"field_wcb_state_en\";N;s:16:\"field_wcb_zip_en\";N;s:20:\"field_wcb_country_en\";N;s:22:\"field_wcb_firstname_en\";N;s:21:\"field_wcb_lastname_en\";N;s:20:\"field_wcb_company_en\";N;s:21:\"field_wcb_address1_en\";N;s:21:\"field_wcb_address2_en\";N;s:18:\"field_wcb_phone_en\";N;s:18:\"field_wcb_email_en\";N;s:14:\"wcb_state_type\";N;s:23:\"field_wcb_firstname_req\";N;s:22:\"field_wcb_lastname_req\";N;s:21:\"field_wcb_company_req\";N;s:22:\"field_wcb_address1_req\";N;s:19:\"field_wcb_phone_req\";N;s:19:\"field_wcb_email_req\";N;s:18:\"field_wcb_city_req\";N;s:17:\"field_wcb_zip_req\";N;s:21:\"field_wcb_country_req\";N;s:19:\"field_wcb_state_req\";N;s:22:\"field_wcb_address2_req\";N;s:30:\"field_wcb_label_as_placeholder\";N;s:21:\"field_wcb_lmark_label\";N;s:18:\"field_wcb_lmark_en\";N;s:19:\"field_wcb_lmark_req\";N;s:21:\"field_wcb_state_codes\";N;s:30:\"field_wcb_country_ameriwcb_can\";N;s:25:\"field_wcb_country_limited\";N;s:27:\"field_wcb_en_country_search\";N;s:25:\"field_wcs_firstname_label\";N;s:24:\"field_wcs_lastname_label\";N;s:23:\"field_wcs_company_label\";N;s:24:\"field_wcs_address1_label\";N;s:24:\"field_wcs_address2_label\";N;s:20:\"field_wcs_city_label\";N;s:21:\"field_wcs_state_label\";N;s:23:\"field_wcs_country_label\";N;s:19:\"field_wcs_zip_label\";N;s:17:\"field_wcs_city_en\";N;s:18:\"field_wcs_state_en\";N;s:16:\"field_wcs_zip_en\";N;s:20:\"field_wcs_country_en\";N;s:21:\"field_wcs_address1_en\";N;s:21:\"field_wcs_address2_en\";N;s:14:\"wcs_state_type\";N;s:22:\"field_wcs_firstname_en\";N;s:21:\"field_wcs_lastname_en\";N;s:20:\"field_wcs_company_en\";N;s:22:\"field_wcs_address1_req\";N;s:18:\"field_wcs_city_req\";N;s:17:\"field_wcs_zip_req\";N;s:21:\"field_wcs_country_req\";N;s:19:\"field_wcs_state_req\";N;s:23:\"field_wcs_firstname_req\";N;s:22:\"field_wcs_lastname_req\";N;s:21:\"field_wcs_company_req\";N;s:22:\"field_wcs_address2_req\";N;s:30:\"field_wcs_label_as_placeholder\";N;s:21:\"field_wcs_lmark_label\";N;s:18:\"field_wcs_lmark_en\";N;s:19:\"field_wcs_lmark_req\";N;s:21:\"field_wcs_state_codes\";N;s:30:\"field_wcs_country_ameriwcs_can\";N;s:25:\"field_wcs_country_limited\";N;s:27:\"field_wcs_en_country_search\";N;}'),
(28, 5, 1, 'Email', 'Email', NULL, 99999999, 0, 1, 0, 0, 'O:8:\"stdClass\":204:{s:18:\"field_is_multiline\";N;s:17:\"field_placeholder\";s:5:\"Email\";s:14:\"field_timezone\";N;s:16:\"field_max_length\";N;s:23:\"field_is_required_range\";N;s:27:\"field_is_required_max_range\";N;s:27:\"field_is_required_min_range\";N;s:24:\"field_is_required_scroll\";N;s:19:\"field_default_value\";N;s:15:\"field_css_class\";s:22:\"rm_form_default_fields\";s:22:\"field_textarea_columns\";N;s:19:\"field_textarea_rows\";N;s:17:\"field_is_required\";i:1;s:21:\"field_is_show_asterix\";N;s:18:\"field_is_read_only\";i:0;s:21:\"field_is_other_option\";N;s:9:\"help_text\";N;s:4:\"icon\";N;s:16:\"field_validation\";N;s:17:\"custom_validation\";N;s:12:\"tnc_cb_label\";N;s:11:\"date_format\";N;s:14:\"field_meta_add\";N;s:10:\"conditions\";N;s:16:\"link_same_window\";N;s:9:\"link_page\";N;s:9:\"link_href\";N;s:9:\"link_type\";N;s:12:\"yt_auto_play\";N;s:9:\"yt_repeat\";N;s:17:\"yt_related_videos\";N;s:15:\"yt_player_width\";N;s:9:\"rm_widget\";N;s:16:\"yt_player_height\";N;s:8:\"if_width\";N;s:9:\"if_height\";N;s:18:\"field_address_type\";N;s:23:\"field_ca_address1_label\";N;s:23:\"field_ca_address2_label\";N;s:19:\"field_ca_city_label\";N;s:20:\"field_ca_state_label\";N;s:22:\"field_ca_country_label\";N;s:18:\"field_ca_zip_label\";N;s:16:\"field_ca_city_en\";N;s:17:\"field_ca_state_en\";N;s:15:\"field_ca_zip_en\";N;s:19:\"field_ca_country_en\";N;s:20:\"field_ca_address1_en\";N;s:20:\"field_ca_address2_en\";N;s:13:\"ca_state_type\";N;s:21:\"field_ca_address1_req\";N;s:17:\"field_ca_city_req\";N;s:16:\"field_ca_zip_req\";N;s:20:\"field_ca_country_req\";N;s:18:\"field_ca_state_req\";N;s:21:\"field_ca_address2_req\";N;s:29:\"field_ca_label_as_placeholder\";N;s:20:\"field_ca_lmark_label\";N;s:17:\"field_ca_lmark_en\";N;s:18:\"field_ca_lmark_req\";N;s:20:\"field_ca_state_codes\";N;s:28:\"field_ca_country_america_can\";N;s:24:\"field_ca_country_limited\";N;s:26:\"field_ca_en_country_search\";N;s:19:\"img_caption_enabled\";N;s:17:\"img_title_enabled\";N;s:16:\"img_link_enabled\";N;s:19:\"img_effects_enabled\";N;s:12:\"border_color\";N;s:12:\"border_width\";N;s:12:\"border_shape\";N;s:15:\"img_pop_enabled\";N;s:8:\"img_size\";N;s:3:\"lat\";N;s:4:\"long\";N;s:4:\"zoom\";N;s:5:\"width\";N;s:13:\"nu_form_views\";N;s:20:\"nu_views_text_before\";N;s:19:\"nu_views_text_after\";N;s:17:\"nu_sub_text_after\";N;s:18:\"nu_sub_text_before\";N;s:14:\"nu_submissions\";N;s:10:\"sub_limits\";N;s:21:\"sub_limit_text_before\";N;s:20:\"sub_limit_text_after\";N;s:15:\"sub_date_limits\";N;s:26:\"sub_date_limit_text_before\";N;s:25:\"sub_date_limit_text_after\";N;s:12:\"last_sub_rec\";N;s:14:\"ls_text_before\";N;s:13:\"ls_text_after\";N;s:14:\"show_form_name\";N;s:9:\"form_desc\";N;s:13:\"sub_limit_ind\";N;s:12:\"custom_value\";N;s:12:\"hide_country\";N;s:9:\"hide_date\";N;s:13:\"show_gravatar\";N;s:9:\"max_items\";N;s:10:\"time_range\";N;s:17:\"user_exists_error\";N;s:19:\"username_characters\";N;s:23:\"invalid_username_format\";N;s:14:\"en_confirm_pwd\";N;s:17:\"pass_mismatch_err\";N;s:16:\"en_pass_strength\";N;s:17:\"pwd_strength_type\";N;s:13:\"pwd_short_msg\";N;s:12:\"pwd_weak_msg\";N;s:14:\"pwd_medium_msg\";N;s:14:\"pwd_strong_msg\";N;s:11:\"format_type\";N;s:19:\"preferred_countries\";N;s:8:\"en_geoip\";N;s:20:\"custom_mobile_format\";N;s:13:\"lim_countries\";N;s:18:\"lim_pref_countries\";N;s:14:\"mobile_err_msg\";N;s:13:\"country_field\";N;s:12:\"sync_country\";N;s:13:\"country_match\";N;s:19:\"privacy_policy_page\";N;s:22:\"privacy_policy_content\";N;s:24:\"privacy_display_checkbox\";N;s:26:\"field_wcb_email_as_primary\";N;s:25:\"field_wcb_firstname_label\";N;s:24:\"field_wcb_lastname_label\";N;s:23:\"field_wcb_company_label\";N;s:24:\"field_wcb_address1_label\";N;s:24:\"field_wcb_address2_label\";N;s:21:\"field_wcb_phone_label\";N;s:21:\"field_wcb_email_label\";N;s:20:\"field_wcb_city_label\";N;s:21:\"field_wcb_state_label\";N;s:23:\"field_wcb_country_label\";N;s:19:\"field_wcb_zip_label\";N;s:17:\"field_wcb_city_en\";N;s:18:\"field_wcb_state_en\";N;s:16:\"field_wcb_zip_en\";N;s:20:\"field_wcb_country_en\";N;s:22:\"field_wcb_firstname_en\";N;s:21:\"field_wcb_lastname_en\";N;s:20:\"field_wcb_company_en\";N;s:21:\"field_wcb_address1_en\";N;s:21:\"field_wcb_address2_en\";N;s:18:\"field_wcb_phone_en\";N;s:18:\"field_wcb_email_en\";N;s:14:\"wcb_state_type\";N;s:23:\"field_wcb_firstname_req\";N;s:22:\"field_wcb_lastname_req\";N;s:21:\"field_wcb_company_req\";N;s:22:\"field_wcb_address1_req\";N;s:19:\"field_wcb_phone_req\";N;s:19:\"field_wcb_email_req\";N;s:18:\"field_wcb_city_req\";N;s:17:\"field_wcb_zip_req\";N;s:21:\"field_wcb_country_req\";N;s:19:\"field_wcb_state_req\";N;s:22:\"field_wcb_address2_req\";N;s:30:\"field_wcb_label_as_placeholder\";N;s:21:\"field_wcb_lmark_label\";N;s:18:\"field_wcb_lmark_en\";N;s:19:\"field_wcb_lmark_req\";N;s:21:\"field_wcb_state_codes\";N;s:30:\"field_wcb_country_ameriwcb_can\";N;s:25:\"field_wcb_country_limited\";N;s:27:\"field_wcb_en_country_search\";N;s:25:\"field_wcs_firstname_label\";N;s:24:\"field_wcs_lastname_label\";N;s:23:\"field_wcs_company_label\";N;s:24:\"field_wcs_address1_label\";N;s:24:\"field_wcs_address2_label\";N;s:20:\"field_wcs_city_label\";N;s:21:\"field_wcs_state_label\";N;s:23:\"field_wcs_country_label\";N;s:19:\"field_wcs_zip_label\";N;s:17:\"field_wcs_city_en\";N;s:18:\"field_wcs_state_en\";N;s:16:\"field_wcs_zip_en\";N;s:20:\"field_wcs_country_en\";N;s:21:\"field_wcs_address1_en\";N;s:21:\"field_wcs_address2_en\";N;s:14:\"wcs_state_type\";N;s:22:\"field_wcs_firstname_en\";N;s:21:\"field_wcs_lastname_en\";N;s:20:\"field_wcs_company_en\";N;s:22:\"field_wcs_address1_req\";N;s:18:\"field_wcs_city_req\";N;s:17:\"field_wcs_zip_req\";N;s:21:\"field_wcs_country_req\";N;s:19:\"field_wcs_state_req\";N;s:23:\"field_wcs_firstname_req\";N;s:22:\"field_wcs_lastname_req\";N;s:21:\"field_wcs_company_req\";N;s:22:\"field_wcs_address2_req\";N;s:30:\"field_wcs_label_as_placeholder\";N;s:21:\"field_wcs_lmark_label\";N;s:18:\"field_wcs_lmark_en\";N;s:19:\"field_wcs_lmark_req\";N;s:21:\"field_wcs_state_codes\";N;s:30:\"field_wcs_country_ameriwcs_can\";N;s:25:\"field_wcs_country_limited\";N;s:27:\"field_wcs_en_country_search\";N;}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_rm_forms`
--

CREATE TABLE `wp_rm_forms` (
  `form_id` int(6) UNSIGNED NOT NULL,
  `form_name` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_type` int(6) DEFAULT NULL,
  `form_user_role` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_user_role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_should_send_email` tinyint(1) DEFAULT NULL,
  `form_redirect` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_redirect_to_page` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_redirect_to_url` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_should_auto_expire` tinyint(1) DEFAULT NULL,
  `form_options` text COLLATE utf8mb4_unicode_ci,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(6) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_rm_forms`
--

INSERT INTO `wp_rm_forms` (`form_id`, `form_name`, `form_type`, `form_user_role`, `default_user_role`, `form_should_send_email`, `form_redirect`, `form_redirect_to_page`, `form_redirect_to_url`, `form_should_auto_expire`, `form_options`, `created_on`, `created_by`, `modified_on`, `modified_by`) VALUES
(1, 'Sample Contact Form', 0, NULL, NULL, 1, 'none', '0', NULL, NULL, 'O:8:\"stdClass\":59:{s:13:\"hide_username\";N;s:23:\"form_is_opt_in_checkbox\";N;s:19:\"mailchimp_relations\";N;s:16:\"form_opt_in_text\";N;s:21:\"form_should_user_pick\";N;s:20:\"form_is_unique_token\";N;s:16:\"form_description\";s:202:\"A standard contact form to get your started right away with RegistrationMagic. This form has Name, Phone No., Email and Message fields. To add this form to a page or post, use shortcode [rm_form ID=\"1\"]\";s:21:\"form_user_field_label\";N;s:16:\"form_custom_text\";s:45:\"Please fill out the form below to contact us.\";s:20:\"form_success_message\";s:69:\"Thank you! We have received your message and will reply back shortly.\";s:18:\"form_email_subject\";s:29:\"We have received your message\";s:18:\"form_email_content\";s:411:\"Dear {{Textbox_1234}},\r\n\r\nThis is a confirmation of the message you submitted through our site. We shall get back to you soon.\r\n\r\nFor your reference, below is a copy of your message. If any information is incorrect, please submit the form again with correct information.\r\n\r\nThank you!\r\n\r\nYour Name: {{Textbox_1234}}\r\n\r\nYour Phone: {{Number_1235}}\r\n\r\nYour Email: {{Email_1233}}\r\n\r\nMessage: {{Textarea_1236}}\";s:21:\"form_submit_btn_label\";s:4:\"Send\";s:21:\"form_submit_btn_color\";N;s:25:\"form_submit_btn_bck_color\";N;s:15:\"form_expired_by\";N;s:22:\"form_submissions_limit\";N;s:16:\"form_expiry_date\";N;s:25:\"form_message_after_expiry\";N;s:14:\"mailchimp_list\";N;s:22:\"mailchimp_mapped_email\";N;s:27:\"mailchimp_mapped_first_name\";N;s:26:\"mailchimp_mapped_last_name\";N;s:25:\"should_export_submissions\";i:0;s:25:\"export_submissions_to_url\";N;s:10:\"form_pages\";N;s:14:\"access_control\";N;s:14:\"style_btnfield\";N;s:10:\"style_form\";N;s:15:\"style_textfield\";N;s:10:\"auto_login\";N;s:12:\"cc_relations\";N;s:7:\"cc_list\";N;s:19:\"form_opt_in_text_cc\";N;s:26:\"form_is_opt_in_checkbox_cc\";N;s:12:\"aw_relations\";N;s:7:\"aw_list\";N;s:19:\"form_opt_in_text_aw\";N;s:26:\"form_is_opt_in_checkbox_aw\";N;s:14:\"enable_captcha\";s:7:\"default\";s:16:\"enable_mailchimp\";N;s:15:\"enable_ccontact\";N;s:13:\"enable_aweber\";N;s:20:\"display_progress_bar\";s:7:\"default\";s:18:\"sub_limit_antispam\";N;s:15:\"placeholder_css\";N;s:15:\"btn_hover_color\";N;s:20:\"field_bg_focus_color\";N;s:16:\"text_focus_color\";N;s:13:\"style_section\";N;s:11:\"style_label\";N;s:18:\"post_expiry_action\";N;s:19:\"post_expiry_form_id\";N;s:14:\"no_prev_button\";i:1;s:18:\"user_auto_approval\";s:7:\"default\";s:25:\"form_opt_in_default_state\";N;s:28:\"form_opt_in_default_state_cc\";N;s:28:\"form_opt_in_default_state_aw\";N;s:18:\"ordered_form_pages\";N;}', '2016-12-15 06:31:04', 1, '2016-12-15 06:51:04', 1),
(2, 'Sample Registration Form', 1, 'a:0:{}', 'subscriber', 1, 'none', '0', NULL, NULL, 'O:8:\"stdClass\":59:{s:13:\"hide_username\";i:0;s:23:\"form_is_opt_in_checkbox\";N;s:19:\"mailchimp_relations\";N;s:16:\"form_opt_in_text\";N;s:21:\"form_should_user_pick\";N;s:20:\"form_is_unique_token\";N;s:16:\"form_description\";s:415:\"This is a sample registration form that can be used to register users on your WordPress site. The form includes Username, Password, First Name, Last Name, Email, Website and Terms and Conditions fields. Feel free to edit them, remove them or add new ones as it suits your needs.\r\n\r\nPlease note, T&C field currently has dummy text. You will need to paste actual text of your terms and condition by editing the field.\";s:21:\"form_user_field_label\";s:0:\"\";s:16:\"form_custom_text\";s:48:\"Register with us by filling out the form below.\";s:20:\"form_success_message\";s:105:\"Thank you for registering with us! Once your account is active, we\'ll send you an email with the details.\";s:18:\"form_email_subject\";s:10:\"Thank you!\";s:18:\"form_email_content\";s:183:\"Hello {{Fname_1238}},\r\n\r\nThank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.\r\n\r\nRegards.\";s:21:\"form_submit_btn_label\";s:0:\"\";s:21:\"form_submit_btn_color\";N;s:25:\"form_submit_btn_bck_color\";N;s:15:\"form_expired_by\";N;s:22:\"form_submissions_limit\";N;s:16:\"form_expiry_date\";N;s:25:\"form_message_after_expiry\";N;s:14:\"mailchimp_list\";N;s:22:\"mailchimp_mapped_email\";N;s:27:\"mailchimp_mapped_first_name\";N;s:26:\"mailchimp_mapped_last_name\";N;s:25:\"should_export_submissions\";i:0;s:25:\"export_submissions_to_url\";N;s:10:\"form_pages\";N;s:14:\"access_control\";N;s:14:\"style_btnfield\";s:0:\"\";s:10:\"style_form\";s:0:\"\";s:15:\"style_textfield\";s:0:\"\";s:10:\"auto_login\";N;s:12:\"cc_relations\";N;s:7:\"cc_list\";N;s:19:\"form_opt_in_text_cc\";N;s:26:\"form_is_opt_in_checkbox_cc\";N;s:12:\"aw_relations\";N;s:7:\"aw_list\";N;s:19:\"form_opt_in_text_aw\";N;s:26:\"form_is_opt_in_checkbox_aw\";N;s:14:\"enable_captcha\";s:7:\"default\";s:16:\"enable_mailchimp\";N;s:15:\"enable_ccontact\";N;s:13:\"enable_aweber\";N;s:20:\"display_progress_bar\";s:7:\"default\";s:18:\"sub_limit_antispam\";N;s:15:\"placeholder_css\";s:0:\"\";s:15:\"btn_hover_color\";s:0:\"\";s:20:\"field_bg_focus_color\";s:0:\"\";s:16:\"text_focus_color\";s:0:\"\";s:13:\"style_section\";s:0:\"\";s:11:\"style_label\";s:0:\"\";s:18:\"post_expiry_action\";N;s:19:\"post_expiry_form_id\";N;s:14:\"no_prev_button\";i:1;s:18:\"user_auto_approval\";s:7:\"default\";s:25:\"form_opt_in_default_state\";N;s:28:\"form_opt_in_default_state_cc\";N;s:28:\"form_opt_in_default_state_aw\";N;s:18:\"ordered_form_pages\";N;}', '2016-12-15 07:19:35', 1, '2016-12-15 09:16:52', 1),
(4, 'Sample Registration Form(copy)', 1, 'a:0:{}', 'subscriber', 1, 'none', '0', NULL, NULL, 'O:8:\"stdClass\":72:{s:13:\"hide_username\";i:0;s:23:\"form_is_opt_in_checkbox\";N;s:19:\"mailchimp_relations\";N;s:16:\"form_opt_in_text\";N;s:21:\"form_should_user_pick\";N;s:20:\"form_is_unique_token\";N;s:16:\"form_description\";s:415:\"This is a sample registration form that can be used to register users on your WordPress site. The form includes Username, Password, First Name, Last Name, Email, Website and Terms and Conditions fields. Feel free to edit them, remove them or add new ones as it suits your needs.\r\n\r\nPlease note, T&C field currently has dummy text. You will need to paste actual text of your terms and condition by editing the field.\";s:21:\"form_user_field_label\";s:0:\"\";s:16:\"form_custom_text\";s:48:\"Register with us by filling out the form below.\";s:20:\"form_success_message\";s:105:\"Thank you for registering with us! Once your account is active, we\'ll send you an email with the details.\";s:18:\"form_email_subject\";s:10:\"Thank you!\";s:18:\"form_email_content\";s:183:\"Hello {{Fname_1238}},\r\n\r\nThank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.\r\n\r\nRegards.\";s:21:\"form_submit_btn_label\";s:6:\"Submit\";s:21:\"form_submit_btn_color\";N;s:25:\"form_submit_btn_bck_color\";N;s:15:\"form_expired_by\";N;s:22:\"form_submissions_limit\";N;s:16:\"form_expiry_date\";N;s:25:\"form_message_after_expiry\";N;s:14:\"mailchimp_list\";N;s:22:\"mailchimp_mapped_email\";N;s:27:\"mailchimp_mapped_first_name\";N;s:26:\"mailchimp_mapped_last_name\";N;s:25:\"should_export_submissions\";i:0;s:25:\"export_submissions_to_url\";N;s:10:\"form_pages\";N;s:14:\"access_control\";N;s:14:\"style_btnfield\";s:0:\"\";s:10:\"style_form\";s:0:\"\";s:15:\"style_textfield\";s:0:\"\";s:10:\"auto_login\";N;s:12:\"cc_relations\";N;s:7:\"cc_list\";N;s:19:\"form_opt_in_text_cc\";N;s:26:\"form_is_opt_in_checkbox_cc\";N;s:12:\"aw_relations\";N;s:7:\"aw_list\";N;s:19:\"form_opt_in_text_aw\";N;s:26:\"form_is_opt_in_checkbox_aw\";N;s:14:\"enable_captcha\";s:7:\"default\";s:16:\"enable_mailchimp\";N;s:15:\"enable_ccontact\";N;s:13:\"enable_aweber\";N;s:20:\"display_progress_bar\";s:7:\"default\";s:18:\"sub_limit_antispam\";N;s:15:\"placeholder_css\";s:0:\"\";s:15:\"btn_hover_color\";s:0:\"\";s:20:\"field_bg_focus_color\";s:0:\"\";s:16:\"text_focus_color\";s:0:\"\";s:13:\"style_section\";s:0:\"\";s:11:\"style_label\";s:0:\"\";s:18:\"post_expiry_action\";N;s:19:\"post_expiry_form_id\";N;s:14:\"no_prev_button\";i:1;s:18:\"user_auto_approval\";s:7:\"default\";s:25:\"form_opt_in_default_state\";N;s:28:\"form_opt_in_default_state_cc\";N;s:28:\"form_opt_in_default_state_aw\";N;s:18:\"ordered_form_pages\";N;s:16:\"show_total_price\";N;s:20:\"form_nu_notification\";N;s:32:\"form_user_activated_notification\";N;s:31:\"form_activate_user_notification\";N;s:26:\"form_admin_ns_notification\";N;s:18:\"admin_notification\";N;s:11:\"admin_email\";N;s:19:\"form_next_btn_label\";N;s:19:\"form_prev_btn_label\";N;s:14:\"form_btn_align\";s:6:\"center\";s:30:\"form_admin_ns_notification_sub\";s:23:\" New Form Notification \";s:24:\"form_nu_notification_sub\";s:21:\"New User Registration\";s:36:\"form_user_activated_notification_sub\";s:17:\"Account Activated\";}', '2019-01-09 04:47:37', 1, '2019-01-09 04:52:47', 1),
(5, 'Upload File', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'O:8:\"stdClass\":66:{s:23:\"form_is_opt_in_checkbox\";N;s:19:\"mailchimp_relations\";N;s:16:\"form_opt_in_text\";N;s:21:\"form_should_user_pick\";N;s:20:\"form_is_unique_token\";N;s:16:\"form_description\";N;s:21:\"form_user_field_label\";N;s:16:\"form_custom_text\";N;s:20:\"form_success_message\";N;s:18:\"form_email_subject\";N;s:18:\"form_email_content\";N;s:21:\"form_submit_btn_label\";N;s:21:\"form_submit_btn_color\";N;s:25:\"form_submit_btn_bck_color\";N;s:15:\"form_expired_by\";N;s:22:\"form_submissions_limit\";N;s:16:\"form_expiry_date\";N;s:25:\"form_message_after_expiry\";N;s:14:\"mailchimp_list\";N;s:22:\"mailchimp_mapped_email\";N;s:27:\"mailchimp_mapped_first_name\";N;s:26:\"mailchimp_mapped_last_name\";N;s:25:\"should_export_submissions\";N;s:25:\"export_submissions_to_url\";N;s:10:\"form_pages\";N;s:14:\"access_control\";N;s:14:\"style_btnfield\";N;s:10:\"style_form\";N;s:15:\"style_textfield\";N;s:10:\"auto_login\";N;s:12:\"cc_relations\";N;s:7:\"cc_list\";N;s:19:\"form_opt_in_text_cc\";N;s:26:\"form_is_opt_in_checkbox_cc\";N;s:12:\"aw_relations\";N;s:7:\"aw_list\";N;s:19:\"form_opt_in_text_aw\";N;s:26:\"form_is_opt_in_checkbox_aw\";N;s:14:\"enable_captcha\";N;s:16:\"enable_mailchimp\";N;s:15:\"enable_ccontact\";N;s:13:\"enable_aweber\";N;s:20:\"display_progress_bar\";N;s:18:\"sub_limit_antispam\";N;s:15:\"placeholder_css\";N;s:15:\"btn_hover_color\";N;s:20:\"field_bg_focus_color\";N;s:16:\"text_focus_color\";N;s:13:\"style_section\";N;s:11:\"style_label\";N;s:18:\"post_expiry_action\";N;s:19:\"post_expiry_form_id\";N;s:25:\"form_opt_in_default_state\";N;s:16:\"show_total_price\";N;s:20:\"form_nu_notification\";N;s:32:\"form_user_activated_notification\";N;s:31:\"form_activate_user_notification\";N;s:26:\"form_admin_ns_notification\";N;s:18:\"admin_notification\";N;s:11:\"admin_email\";N;s:19:\"form_next_btn_label\";N;s:19:\"form_prev_btn_label\";N;s:14:\"form_btn_align\";N;s:30:\"form_admin_ns_notification_sub\";s:23:\" New Form Notification \";s:24:\"form_nu_notification_sub\";s:21:\"New User Registration\";s:36:\"form_user_activated_notification_sub\";s:17:\"Account Activated\";}', '2019-01-10 04:40:43', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wp_rm_front_users`
--

CREATE TABLE `wp_rm_front_users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity_time` datetime DEFAULT NULL,
  `created_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_rm_login`
--

CREATE TABLE `wp_rm_login` (
  `id` int(6) UNSIGNED NOT NULL,
  `m_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_rm_login`
--

INSERT INTO `wp_rm_login` (`id`, `m_key`, `value`) VALUES
(1, 'fields', '{\"form_fields\":[{\"username_accepts\":\"username\",\"field_label\":\"Username\",\"placeholder\":\"Enter Username\",\"input_selected_icon_codepoint\":\"\",\"icon_fg_color\":\"CBFFC2\",\"icon_bg_color\":\"FFFFFF\",\"icon_bg_alpha\":\"0.5\",\"icon_shape\":\"square\",\"field_css_class\":\"\",\"field_type\":\"username\"},{\"field_label\":\"Password\",\"placeholder\":\"Enter Password\",\"input_selected_icon_codepoint\":\"\",\"icon_fg_color\":\"FFFFFF\",\"icon_bg_color\":\"FFFFFF\",\"icon_bg_alpha\":\"0.5\",\"icon_shape\":\"square\",\"field_css_class\":\"test\",\"field_type\":\"password\"}]}'),
(2, 'redirections', '{\"redirection_type\":\"common\",\"redirection_link\":\"\",\"admin_redirection_link\":0,\"logout_redirection\":\"\",\"role_based_login_redirection\":[],\"administrator_login_redirection\":\"\",\"administrator_logout_redirection\":\"\",\"editor_login_redirection\":\"\",\"editor_logout_redirection\":\"\",\"author_login_redirection\":\"\",\"author_logout_redirection\":\"\",\"contributor_login_redirection\":\"\",\"contributor_logout_redirection\":\"\",\"subscriber_login_redirection\":\"\",\"subscriber_logout_redirection\":\"\",\"translator_login_redirection\":\"\",\"translator_logout_redirection\":\"\",\"customer_login_redirection\":\"\",\"customer_logout_redirection\":\"\"}'),
(3, 'validations', '{\"un_error_msg\":\"The login credentials you entered are incorrect. Please try again.\",\"pass_error_msg\":\"The login credentials you entered are incorrect. Please try again.\",\"en_recovery_link\":1,\"en_failed_user_notification\":0,\"en_failed_admin_notification\":0,\"en_captcha\":0,\"allowed_failed_attempts\":3,\"allowed_failed_duration\":60,\"en_ban_ip\":0,\"allowed_attempts_before_ban\":6,\"allowed_duration_before_ban\":60,\"ban_type\":\"temp\",\"ban_duration\":1440,\"ban_error_msg\":\"<div style=\\\"font-weight: 400;\\\" class=\\\"rm-failed-ip-error\\\">Your IP has been banned by the Admin due to repeated failed login attempts.<\\/div>\",\"notify_admin_on_ban\":1}'),
(4, 'recovery', '{\"en_pwd_recovery\":1,\"recovery_link_text\":\"Lost your password?\"}'),
(5, 'auth', '{\"otp_type\":\"numeric\",\"en_two_fa\":0,\"otp_length\":6,\"otp_expiry_action\":\"regenerate\",\"otp_expiry\":10,\"otp_regen_success_msg\":\"A new OTP was successfully sent to your email address!\",\"otp_regen_text\":\"Re-generate OTP\",\"otp_exp_msg\":\"Sorry, your OTP has expired. You can re-generate OTP using link below.\",\"otp_exp_restart_msg\":\"Sorry, your OTP has expired. You need to login again to proceed.\",\"otp_field_label\":\"Enter Your OTP\",\"msg_above_otp\":\"We emailed you a one-time-password (OTP) to your registered email address. Please enter it below to complete the login process.\",\"en_resend_otp\":1,\"otp_resend_text\":\"Did not received OTP? Resend it\",\"otp_resent_msg\":\"OTP was resent successfully to your email address!\",\"otp_resend_limit\":\"3\",\"allowed_incorrect_attempts\":5,\"invalid_otp_error\":\"The OTP you entered is incorrect.\",\"apply_on\":\"all\",\"disable_two_fa_for_admin\":1,\"enable_two_fa_for_roles\":[]}'),
(6, 'email_templates', '{\"failed_login_err\":\"<span style=\\\"font-weight: 400;\\\">There was a failed login attempt using your account username\\/ password {{username}} on our site {{sitename}} from IP {{Login_IP}} on {{login_time}}. If you have forgotten your password, you can easily reset it by visiting login page on our site. <\\/span>\\r\\n\\r\\n&nbsp;\\r\\n\\r\\n<span style=\\\"font-weight: 400;\\\">If you think it was an unauthorized login attempt, please contact site admin immediately.<\\/span>\",\"otp_message\":\"<span style=\\\"font-weight: 400;\\\">Here is your one-time-password (OTP) for logging into {{site_name}}. The OTP will automatically expire after {{OTP_expiry}} minutes.<\\/span>\\r\\n\\r\\n&nbsp;\\r\\n\\r\\n<span style=\\\"font-weight: 400;\\\">{{OTP}}<\\/span>\\r\\n\\r\\n&nbsp;\\r\\n\\r\\n<span style=\\\"font-weight: 400;\\\">If you think it was an unauthorized login attempt, please contact site admin immediately. <\\/span>\\r\\n\\r\\n&nbsp;\\r\\n\\r\\n<span style=\\\"font-weight: 400;\\\"><\\/span>\",\"failed_login_err_admin\":\"<span style=\\\"font-weight: 400;\\\">There was a failed login attempt using username\\/ password {{username}} on your site {{sitename}} from IP {{Login_IP}} on {{login_time}}.<\\/span>\\r\\n\\r\\n&nbsp;\\r\\n\\r\\n<span style=\\\"font-weight: 400;\\\">If you think this is an unauthorized login attempt<\\/span><i><span style=\\\"font-weight: 400;\\\">, <\\/span><\\/i><span style=\\\"font-weight: 400;\\\">you can also immediately ban the IP by clicking <\\/span><span style=\\\"font-weight: 400;\\\"><a href=\\\"https://picture.hungenbach.de/wp-admin/admin.php?page=rm_options_security\\\">here</a><\\/span><span style=\\\"font-weight: 400;\\\">. <\\/span>\\r\\n\\r\\n<span style=\\\"font-weight: 400;\\\">You can managed the blocked IPs and\\/ or usernames by visiting <\\/span><span style=\\\"font-weight: 400;\\\"><a href=\\\"https://picture.hungenbach.de/wp-admin/admin.php?page=rm_options_security\\\">this link</a><\\/span> <i><span style=\\\"font-weight: 400;\\\">Global Settings \\u2192 Security page link<\\/span><\\/i>\",\"ban_message_admin\":\"<span style=\\\"font-weight: 400;\\\">There were multiple failed login attempts from IP {{login_IP}}. As a preset security measure, RegistrationMagic has blocked the IP. Here are the details of the ban:<\\/span>\\r\\n\\r\\n&nbsp;\\r\\n\\r\\n<span style=\\\"font-weight: 400;\\\">Ban Period: {{ban_period}}<\\/span>\\r\\n\\r\\n<span style=\\\"font-weight: 400;\\\">Failed Login Attempts: {{ban_trigger}}<\\/span>\\r\\n\\r\\n<span style=\\\"font-weight: 400;\\\">If you think this IP is secure, you can lift the ban by clicking <\\/span><span style=\\\"font-weight: 400;\\\"><a href=\\\"https://picture.hungenbach.de/wp-admin/admin.php?page=rm_options_security\\\">here</a><\\/span><span style=\\\"font-weight: 400;\\\">. <\\/span>\\r\\n\\r\\n<span style=\\\"font-weight: 400;\\\">You can managed the blocked IPs and\\/ or usernames by visiting <\\/span><span style=\\\"font-weight: 400;\\\"><a href=\\\"https://picture.hungenbach.de/wp-admin/admin.php?page=rm_options_security\\\">this link</a><\\/span> <i><span style=\\\"font-weight: 400;\\\">Global Settings \\u2192 Security page link<\\/span><\\/i>\"}'),
(7, 'btn_config', '{\"register_btn\":\"Register\",\"login_btn\":\"Login\",\"align\":\"center\",\"display_register\":1}'),
(8, 'design', '{\"style_form\":\"\",\"style_textfield\":\"\",\"style_btnfield\":\"\",\"form_submit_btn_label\":\"Submit\",\"style_section\":\"\",\"form_id\":\"login\",\"placeholder_css\":\"\"}'),
(9, 'login_view', '{\"display_user_avatar\":1,\"display_user_name\":1,\"display_greetings\":1,\"greetings_text\":\"Welcome\",\"display_custom_msg\":1,\"custom_msg\":\"You are already logged in.\",\"separator_bar_color\":\"DDDDDD\",\"display_account_link\":1,\"account_link_text\":\"My Account\",\"display_logout_link\":1,\"logout_text\":\"Logout\"}'),
(10, 'log_retention', '{\"logs_retention\":\"records\",\"no_of_records\":1000,\"no_of_days\":7}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_rm_login_log`
--

CREATE TABLE `wp_rm_login_log` (
  `id` int(6) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username_used` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `browser` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ban` int(1) DEFAULT NULL,
  `result` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `failure_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ban_til` datetime DEFAULT NULL,
  `login_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_rm_login_log`
--

INSERT INTO `wp_rm_login_log` (`id`, `email`, `username_used`, `time`, `status`, `ip`, `browser`, `type`, `ban`, `result`, `failure_reason`, `ban_til`, `login_url`, `social_type`) VALUES
(1, 'markus.hu@gmx.de', 'test', '2019-01-09 05:04:41', 1, '79.237.62.214', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(2, 'markus.hu@gmx.de', 'test', '2019-01-10 04:37:17', 1, '79.237.62.214', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(3, 'markus.hu@gmx.de', 'test', '2019-01-11 04:52:59', 1, '79.237.62.214', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(4, 'markus@hungenbach.de', 'markus@hungenbach.de', '2019-01-11 04:55:43', 1, '79.237.62.214', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(5, 'markus@hungenbach.de', 'markus@hungenbach.de', '2019-01-12 03:54:08', 1, '79.237.62.214', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(6, 'markus.hu@gmx.de', 'test', '2019-01-12 03:56:44', 1, '79.237.62.214', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(7, 'markus.hu@gmx.de', 'test', '2019-01-12 04:13:15', 1, '79.237.62.214', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(8, 'markus.hu@gmx.de', 'test', '2019-01-12 04:24:21', 1, '79.237.62.214', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(9, 'markus.hu@gmx.de', 'test', '2019-01-12 04:26:14', 1, '2003:ec:2bde:55e5:f847:a581:cf5:dff4', 'iPhone', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(10, 'markus.hu@gmx.de', 'test', '2019-01-12 04:34:36', 1, '79.237.62.214', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(11, 'markus.hu@gmx.de', 'test', '2019-01-12 06:02:17', 1, '79.237.62.214', 'iPad', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(12, 'markus.hu@gmx.de', 'Test', '2019-01-12 06:02:50', 1, '79.237.62.214', 'iPad', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_submissions', ''),
(13, 'hungenbach@web.de', 'test2', '2019-01-12 11:14:20', 1, '2003:ec:2bde:55e5:188c:4750:b612:d4e9', 'iPhone', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(14, 'hungenbach@web.de', 'test2', '2019-01-13 04:29:31', 1, '79.237.59.75', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(15, 'hungenbach@web.de', 'test2', '2019-01-13 05:05:50', 1, '79.237.59.75', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_submissions', ''),
(16, 'markus.hu@gmx.de', 'test', '2019-01-13 10:09:30', 1, '109.41.0.55', 'iPhone', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(17, 'markus.hu@gmx.de', 'test', '2019-01-13 11:16:46', 1, '109.41.0.55', 'iPhone', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(18, 'hungenbach@web.de', 'test2', '2019-01-14 03:30:04', 1, '79.237.59.75', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(19, 'markus.hu@gmx.de', 'test', '2019-01-14 03:31:55', 0, '79.237.59.75', 'Firefox', 'normal', 0, 'failure', 'incorrect_password', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(20, 'markus.hu@gmx.de', 'test', '2019-01-14 03:32:07', 0, '79.237.59.75', 'Firefox', 'normal', 0, 'failure', 'incorrect_password', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(21, 'markus.hu@gmx.de', 'test', '2019-01-14 03:32:16', 1, '79.237.59.75', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(22, 'markus.hu@gmx.de', 'test', '2019-01-14 03:36:46', 1, '79.237.59.75', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(23, 'markus.hu@gmx.de', 'test', '2019-01-14 03:37:23', 1, '79.237.59.75', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/my-account', ''),
(24, 'markus.hu@gmx.de', 'test', '2019-01-14 03:38:08', 0, '79.237.59.75', 'Firefox', 'normal', 0, 'failure', 'incorrect_password', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(25, 'markus.hu@gmx.de', 'test', '2019-01-14 03:38:17', 0, '79.237.59.75', 'Firefox', 'normal', 0, 'failure', 'incorrect_password', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(26, 'markus.hu@gmx.de', 'test', '2019-01-14 03:38:30', 1, '79.237.59.75', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(27, 'markus.hu@gmx.de', 'test', '2019-01-14 03:39:27', 1, '79.237.59.75', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(28, 'markus.hu@gmx.de', 'test', '2019-01-14 04:17:58', 1, '79.237.59.75', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(29, 'markus.hu@gmx.de', 'test', '2019-01-14 09:51:18', 1, '164.59.255.225', 'iPhone', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(30, 'hungenbach@web.de', 'test2', '2019-01-19 05:34:21', 1, '79.237.59.75', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(31, 'hungenbach@web.de', 'test2', '2019-01-19 05:37:49', 1, '79.237.59.75', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(32, 'hungenbach@web.de', 'test2', '2019-01-19 05:38:11', 1, '79.237.59.75', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(33, 'markus.hu@gmx.de', 'test', '2019-02-02 07:41:46', 1, '2003:ec:2bde:9c49:14c2:7c5d:b7c9:70c7', 'iPad', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(34, 'hungenbach@web.de', 'test2', '2019-02-02 17:35:27', 1, '109.41.2.0', 'iPhone', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(35, 'hungenbach@web.de', 'test2', '2019-02-11 04:20:04', 1, '79.237.57.67', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(36, 'markus.hu@gmx.de', 'test', '2019-02-24 04:10:03', 1, '79.237.57.67', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(37, 'umesh.wordpressexpert@gmail.com', 'umesh', '2019-03-22 10:54:08', 1, '2405:205:2082:d760:c41f:b18c:2334:c472', 'Chrome', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(38, 'bule_house@yahoo.com', 'bule_house@yahoo.com', '2019-03-31 07:12:12', 0, '85.203.47.80', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(39, 'bule_house@yahoo.com', 'bule_house@yahoo.com', '2019-03-31 07:13:04', 0, '85.203.47.80', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(40, 'bule_house@yahoo.com', 'bule_house@yahoo.com', '2019-03-31 07:13:35', 0, '85.203.47.80', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(41, 'shahid.ranjha@gmail.com', 'shahidabd', '2019-03-31 07:32:59', 1, '103.255.5.249', 'Chrome', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(42, 'christoph.selbach@gmx.de', 'cselbach', '2019-04-29 05:15:26', 1, '109.41.67.95', 'Chrome', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(43, 'test@gmail.com', 'test', '2019-06-21 04:03:07', 1, '50.97.232.186', 'Chrome', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(44, 'markus.hungenbach', 'markus.hungenbach', '2019-06-22 04:08:17', 0, '46.88.69.5', 'Firefox', 'normal', 0, 'failure', 'incorrect_username', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(45, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-06-22 04:10:46', 0, '46.88.69.5', 'Firefox', 'normal', 0, 'failure', 'incorrect_password', NULL, 'https://picture.hungenbach.de/my-account', ''),
(46, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-06-22 04:12:13', 1, '46.88.69.5', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(47, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-06-24 16:18:52', 1, '89.246.71.189', 'iPhone', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(48, 'giefsl@outlook.com', 'test111', '2019-06-24 16:24:20', 1, '3.82.189.140', 'Chrome', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(49, 'giefsl@outlook.com', 'test111', '2019-06-25 04:42:17', 1, '69.4.228.114', 'Chrome', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(50, 'webdev1888@outlook.com', 'AlexBregman', '2019-06-29 04:54:58', 1, '188.43.136.32', 'Chrome', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(51, 'ipanimationworld@gmail.com', 'testing', '2019-07-02 07:44:45', 1, '27.5.7.79', 'Chrome', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(52, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-07-08 12:51:13', 1, '89.246.71.189', 'iPhone', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(53, 'tt@mailinator.com', 'tt', '2019-07-10 07:10:11', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(54, 'pramodnp.pnp@gmail.com', 'pramod', '2019-07-12 09:43:43', 1, '150.129.88.74', 'Chrome', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(55, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-07-16 04:34:16', 1, '79.237.53.39', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(56, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-07-16 05:45:43', 1, '79.237.53.39', 'Firefox', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(57, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-07-18 05:19:09', 1, '79.237.53.39', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://18.194.238.195/rm_login', ''),
(58, 'julian@yopmail.com', 'julian', '2019-07-22 04:22:22', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(59, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-07-22 05:34:19', 1, '87.62.138.85', 'Firefox', 'normal', 0, 'success', '', NULL, 'https://picture.hungenbach.de/rm_login', ''),
(60, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-07-23 06:39:19', 1, '87.62.138.85', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(61, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-07-24 04:42:03', 1, '109.40.1.131', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(62, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-07-28 05:14:07', 1, '212.112.154.229', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(63, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-07-28 05:21:46', 1, '212.112.154.229', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(64, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-07-28 05:35:42', 1, '212.112.154.229', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(65, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-07-28 08:59:52', 1, '109.40.3.210', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(66, 'mgulati@chicmic.in', 'ChicMic', '2019-07-29 04:23:46', 1, '122.173.83.53', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(67, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-07-29 05:02:33', 1, '212.112.152.47', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(68, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-07-29 05:08:55', 1, '212.112.152.47', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(69, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-07-29 05:18:28', 1, '212.112.152.47', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(70, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-07-29 05:20:50', 1, '212.112.152.47', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(71, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-07-29 09:28:46', 1, '212.112.152.179', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(72, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-07-29 09:29:59', 1, '212.112.152.179', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(73, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-07-31 06:44:10', 1, '212.112.155.185', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(74, 'julian@yopmail.com', 'julian@yopmail.com', '2019-08-02 04:27:45', 0, '27.56.156.91', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(75, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-02 04:29:21', 1, '27.56.156.91', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(76, 'mgulati@chicmic.in', 'ChicMic', '2019-08-06 06:47:45', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(77, 'julian@yopmail.com', 'julian@yopmail.com', '2019-08-06 06:48:10', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(78, 'julian@yopmail.com', 'julian@yopmail.com', '2019-08-06 06:48:28', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(79, 'julian@yopmail.com', 'julian@yopmail.com', '2019-08-06 06:48:45', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(80, 'shiv@mailinator.com', 'shiv@mailinator.com', '2019-08-06 06:49:27', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(81, 'shiv@mailinator.com', 'shiv@mailinator.com', '2019-08-06 06:49:35', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(82, 'shiv@mailinator.com', 'shiv@mailinator.com', '2019-08-06 06:49:50', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(83, 'shiv@mailinator.com', 'shiv@mailinator.com', '2019-08-06 06:50:01', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(84, 'shiv@mailinator.com', 'shiv@mailinator.com', '2019-08-06 06:51:35', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(85, 'shiv@mailinator.com', 'shiv', '2019-08-06 06:51:48', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(86, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-08-06 22:41:28', 1, '79.237.50.97', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(87, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-08-09 12:04:19', 1, '89.246.71.189', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(88, 'd', 'd', '2019-08-12 12:00:29', 0, '112.196.9.242', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(89, 'shiv@mailinator.com', 'shiv@mailinator.com', '2019-08-13 06:04:33', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(90, 'ss@mailinator.com', 'ss@mailinator.com', '2019-08-13 06:04:41', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(91, 'sss@mailinator.com', 'sss@mailinator.com', '2019-08-13 06:04:48', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(92, 'julian@yopmail.com', 'julian@yopmail.com', '2019-08-13 06:05:08', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(93, 'sss@mailinator.com', 'sss@mailinator.com', '2019-08-13 06:05:41', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(94, 'sss@mailinator.com', 'sss@mailinator.com', '2019-08-13 06:05:50', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(95, 'sss@mailinator.com', 'sss@mailinator.com', '2019-08-13 06:06:34', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(96, 'sss@mailinator.com', 'ss', '2019-08-13 06:06:43', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(97, 'sss@mailinator.com', 'ss', '2019-08-13 07:52:32', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(98, 'sss@mailinator.com', 'ss', '2019-08-13 08:34:25', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(99, 'sss@mailinator.com', 'ss', '2019-08-13 11:11:15', 1, '106.78.57.165', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(100, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-08-13 11:20:43', 1, '89.246.71.189', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(101, 'mohammadyasirchicmic59', 'mohammadyasirchicmic59', '2019-08-19 04:40:27', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(102, 'mohammadyasirchicmic59@gmail.com', 'mohammadyasirchicmic59@gmail.com', '2019-08-19 04:46:32', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(103, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-19 05:11:22', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(104, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-20 04:33:08', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(105, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-08-20 05:28:37', 1, '109.40.67.214', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(106, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-20 09:43:34', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(107, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-21 07:29:47', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(108, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-23 09:46:55', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(109, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-23 10:31:53', 0, '112.196.9.242', 'Chrome', 'normal', 0, 'failure', 'incorrect_password', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(110, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-23 10:32:08', 0, '112.196.9.242', 'Chrome', 'normal', 0, 'failure', 'incorrect_password', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(111, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-23 10:32:15', 1, '112.196.9.242', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(112, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-26 04:16:57', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(113, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-26 12:21:08', 1, '112.196.9.242', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(114, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-27 04:15:24', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(115, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-27 08:15:52', 1, '106.205.154.86', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(116, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-28 08:48:01', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(117, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-08-28 08:49:26', 1, '89.246.71.189', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(118, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-08-29 04:12:40', 1, '46.88.76.102', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(119, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-30 04:01:56', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(120, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-30 06:07:43', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(121, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-30 06:53:59', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(122, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-30 11:08:33', 0, '109.41.1.182', 'iPhone', 'normal', 0, 'failure', 'incorrect_password', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(123, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-30 11:09:17', 1, '109.41.1.182', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(124, 'markus.hu@gmx.de', 'Markus.hu@gmx.de', '2019-08-30 11:20:39', 1, '89.246.71.189', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(125, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-08-30 11:21:39', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(126, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-30 11:27:43', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(127, 'mohammadyasirchicmic59@gmail.com', 'Mohammad', '2019-08-30 11:57:48', 1, '106.205.165.162', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(128, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-08-30 12:03:14', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(129, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-08-30 12:15:19', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(130, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-08-30 12:42:39', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(131, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-08-30 13:45:45', 1, '89.246.71.189', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(132, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-08-30 13:54:10', 1, '89.246.71.189', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(133, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-08-30 15:33:24', 1, '46.88.76.102', 'iPad', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(134, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-08-30 17:09:21', 1, '46.88.78.152', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(135, 'markis.hu@gmx.de', 'markis.hu@gmx.de', '2019-08-31 15:26:14', 0, '46.88.76.102', 'iPad', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(136, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-08-31 15:26:33', 1, '46.88.76.102', 'iPad', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(137, 'webmaster@hungenbach.de', 'hungenbach', '2019-09-02 03:23:38', 1, '46.88.76.102', 'iPad', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(138, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-09-02 05:21:02', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(139, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-09-02 06:35:49', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(140, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-09-02 06:41:41', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(141, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-09-02 07:06:53', 1, '112.196.9.242', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(142, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-09-02 07:15:26', 1, '112.196.9.242', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(143, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-09-04 03:18:59', 1, '46.88.76.102', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(144, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-09-07 09:33:51', 1, '46.88.76.102', 'iPad', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(145, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-09-08 05:16:27', 1, '46.88.76.102', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(146, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-09-09 05:38:23', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(147, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-09-10 17:32:52', 1, '46.88.76.102', 'iPad', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(148, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-09-16 04:10:13', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(149, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-09-16 04:17:49', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(150, 'yasir', 'yasir', '2019-09-20 04:42:12', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(151, 'mohammadyasirchicmic59@gmail.com', 'mohammadyasirchicmic59@gmail.com', '2019-09-20 04:42:48', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(152, 'mohammadyasirchicmic59@gmail.com', 'mohammadyasirchicmic59@gmail.com', '2019-09-20 04:42:56', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(153, 'julian@yopmail.com', 'julian@yopmail.com', '2019-09-20 04:43:46', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(154, 'julian@yopmail.com', 'julian@yopmail.com', '2019-09-20 04:44:11', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(155, 'shiv@mailinator.com', 'shiv@mailinator.com', '2019-09-20 04:45:52', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(156, 'shiv@mailinator.com', 'shiv@mailinator.com', '2019-09-20 04:46:03', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(157, 'shiv@mailinator.com', 'shiv@mailinator.com', '2019-09-20 04:46:18', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(158, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-09-25 04:24:44', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(159, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-09-28 17:35:36', 1, '46.88.74.12', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(160, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-09-30 11:45:51', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(161, 'amrish.kumar@chicmic.co.in', 'amrish.kumar@chicmic.co.in', '2019-10-03 09:25:32', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(162, 'amrish.kumar@chicmic.co.in', 'amrish.kumar@chicmic.co.in', '2019-10-03 09:26:01', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(163, 'amrish.kumar@chicmic.co.in', 'amrish', '2019-10-03 09:26:27', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(164, 'amrish.kumar@chicmic.co.in', 'amrish', '2019-10-04 04:56:22', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(165, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-10-04 09:09:03', 1, '89.246.71.189', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(166, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-10-07 04:49:00', 1, '46.88.75.144', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(167, 'amrish.kumar@chicmic.co.in', 'amrish', '2019-10-09 10:50:47', 1, '157.39.111.243', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(168, 'mohammadyasirchicmic59', 'mohammadyasirchicmic59', '2019-10-10 07:19:12', 0, '112.196.31.138', 'Chrome', 'normal', 0, 'failure', 'incorrect_username', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(169, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-10-10 07:19:35', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(170, 'mohammadyasirchicmic59@gmail.com', 'mohammad', '2019-10-10 07:30:19', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(171, 'amrish.kumar@chicmic.co.in', 'amrish', '2019-10-16 10:52:56', 1, '112.196.9.242', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(172, 'amrish.kumar@chicmic.co.in', 'amrish', '2019-10-18 08:49:54', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(173, 'amrish.kumar@chicmic.co.in', 'amrish', '2019-10-18 10:19:12', 1, '112.196.9.242', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(174, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-10-20 03:40:38', 1, '46.88.78.196', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(175, 'amrish.kumar@chicmic.co.in', 'amrish', '2019-10-21 11:00:58', 1, '112.196.31.138', 'Firefox', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(176, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-10-21 16:05:17', 1, '109.40.67.190', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(177, 'markus.hu@gmx.de', 'markus.hu@gmx.de', '2019-10-21 16:12:31', 1, '109.40.67.190', 'iPhone', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(178, 'amrish.kumar@chicmic.co.in', 'amrish', '2019-10-22 05:46:01', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(179, 'amrish.kumar@chicmic.co.in', 'amrish', '2019-10-23 10:29:45', 1, '112.196.9.242', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(180, 'dhiraj@chicmic.co.in', 'amy', '2019-10-23 10:43:37', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(181, 'amrish.kumar@chicmic.co.in', 'amrish', '2019-10-24 06:26:16', 1, '112.196.9.242', 'Safari', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(182, 'dhiraj@chicmic.co.in', 'amy', '2019-10-24 06:57:05', 1, '112.196.31.138', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(183, 'dhiraj@chicmic.co.in', 'amy', '2019-10-24 11:40:56', 1, '192.168.2.254', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/', ''),
(184, 'dhiraj@chicmic.co.in', 'amy', '2019-10-24 12:00:23', 1, '192.168.2.254', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(185, 'amrish.kumar@chicmic.co.in', 'amrish', '2019-10-24 12:11:27', 1, '192.168.2.254', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(186, 'dhiraj@chicmic.co.in', 'amy', '2019-10-24 12:14:02', 1, '192.168.2.254', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/my-account', ''),
(187, 'dhiraj@chicmic.co.in', 'amy', '2019-11-01 06:09:04', 0, '192.168.2.254', 'Chrome', 'normal', 0, 'failure', 'incorrect_password', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(188, 'amrish.kumar@chicmic.co.in', 'amrish', '2019-11-01 06:09:18', 1, '192.168.2.254', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', ''),
(189, 'amrish.kumar@chicmic.co.in', 'amrish', '2019-11-07 11:05:09', 1, '::1', 'Chrome', 'normal', 0, 'success', '', NULL, 'http://localhost/picture_hungenbach/rm_login', '');

-- --------------------------------------------------------

--
-- Table structure for table `wp_rm_notes`
--

CREATE TABLE `wp_rm_notes` (
  `note_id` int(11) NOT NULL,
  `submission_id` int(11) NOT NULL,
  `notes` longtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publication_date` datetime NOT NULL,
  `published_by` bigint(20) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  `last_edited_by` bigint(20) DEFAULT NULL,
  `note_options` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_rm_paypal_fields`
--

CREATE TABLE `wp_rm_paypal_fields` (
  `field_id` int(6) UNSIGNED NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  `class` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `option_label` longtext COLLATE utf8mb4_unicode_ci,
  `option_price` longtext COLLATE utf8mb4_unicode_ci,
  `option_value` longtext COLLATE utf8mb4_unicode_ci,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `require` longtext COLLATE utf8mb4_unicode_ci,
  `order` int(11) DEFAULT NULL,
  `extra_options` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_rm_paypal_fields`
--

INSERT INTO `wp_rm_paypal_fields` (`field_id`, `type`, `name`, `value`, `class`, `option_label`, `option_price`, `option_value`, `description`, `require`, `order`, `extra_options`) VALUES
(1, 'fixed', 'Picture Town', '4.99', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a:2:{s:12:\"show_on_form\";s:3:\"yes\";s:14:\"allow_quantity\";s:2:\"no\";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_rm_paypal_logs`
--

CREATE TABLE `wp_rm_paypal_logs` (
  `id` int(6) UNSIGNED NOT NULL,
  `submission_id` int(6) DEFAULT NULL,
  `form_id` int(6) DEFAULT NULL,
  `invoice` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `txn_id` varchar(600) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  `currency` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `log` longtext COLLATE utf8mb4_unicode_ci,
  `posted_date` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_proc` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill` longtext COLLATE utf8mb4_unicode_ci,
  `ex_data` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_rm_rules`
--

CREATE TABLE `wp_rm_rules` (
  `rule_id` int(6) UNSIGNED NOT NULL,
  `type` int(6) DEFAULT NULL,
  `attr_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attr_value` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `operator` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta` text COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_rm_sent_mails`
--

CREATE TABLE `wp_rm_sent_mails` (
  `mail_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub` longtext COLLATE utf8mb4_unicode_ci,
  `body` longtext COLLATE utf8mb4_unicode_ci,
  `sent_on` datetime DEFAULT NULL,
  `headers` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_id` int(11) DEFAULT NULL,
  `exdata` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read_by_user` tinyint(1) NOT NULL DEFAULT '0',
  `was_sent_success` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_rm_sent_mails`
--

INSERT INTO `wp_rm_sent_mails` (`mail_id`, `type`, `to`, `sub`, `body`, `sent_on`, `headers`, `form_id`, `exdata`, `is_read_by_user`, `was_sent_success`) VALUES
(1, 2, 'markus.hu@gmx.de', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-01-09 04:57:18', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '3', 0, 1),
(2, 2, 'hungenbach@web.de', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-01-12 11:05:14', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '4', 0, 1),
(3, 2, 'hungenbach@web.de', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-01-12 11:13:07', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '5', 0, 1),
(4, 2, 'umesh.wordpressexpert@gmail.com', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-03-22 10:50:24', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '6', 0, 1),
(5, 2, 'bule_house@yahoo.com', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-03-31 07:11:48', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '7', 0, 1),
(6, 2, 'shahid.ranjha@gmail.com', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-03-31 07:32:27', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '8', 0, 1),
(7, 2, 'christoph.selbach@gmx.de', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-04-29 05:13:21', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '9', 0, 1),
(8, 2, 'test@gmail.com', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-06-21 04:02:59', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '10', 0, 1),
(9, 2, 'giefsl@outlook.com', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-06-24 16:23:27', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '11', 0, 1),
(10, 2, 'webdev1888@outlook.com', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-06-29 04:40:43', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '12', 0, 1),
(11, 2, 'ipanimationworld@gmail.com', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-07-02 07:44:36', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '13', 0, 1),
(12, 2, 'tt@mailinator.com', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-07-10 07:09:49', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '14', 0, 1),
(13, 2, 'pramodnp.pnp@gmail.com', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-07-12 09:43:31', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '15', 0, 1),
(14, 2, 'julian@yopmail.com', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-07-22 04:21:53', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '16', 0, 0),
(15, 2, 'mohammadyasirchicmic59@gmail.com', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-08-02 04:28:37', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '17', 0, 1),
(16, 2, 'shiv@mailinator.com', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-08-06 06:49:17', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '18', 0, 1),
(17, 2, 'sss@mailinator.com', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-08-13 06:05:31', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '19', 0, 1),
(18, 2, 'webmaster@hungenbach.de', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-09-02 03:23:10', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '20', 0, 1),
(19, 2, 'amrish.kumar@chicmic.co.in', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-10-03 09:24:53', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '21', 0, 1),
(20, 2, 'dhiraj@chicmic.co.in', 'Thank you!', '&lt;div class=&quot;mail-wrapper&quot;&gt;&lt;p&gt;Hello ,&lt;/p&gt;\n&lt;p&gt;Thank you for registering with us. You will soon receive an account activation email. After that you can log into our website through login page.&lt;/p&gt;\n&lt;p&gt;Regards.&lt;/p&gt;\n&lt;br&gt;&lt;br&gt;&lt;/div&gt;', '2019-10-23 10:43:19', 'From: creative <markus@hungenbach.de>\r\nContent-Type: text/html; charset=UTF-8', 4, '22', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_rm_sessions`
--

CREATE TABLE `wp_rm_sessions` (
  `id` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `timestamp` int(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_rm_stats`
--

CREATE TABLE `wp_rm_stats` (
  `stat_id` int(11) NOT NULL,
  `form_id` int(6) DEFAULT NULL,
  `user_ip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ua_string` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `browser_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visited_on` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `submitted_on` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_taken` int(11) DEFAULT NULL,
  `submission_id` int(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_rm_stats`
--

INSERT INTO `wp_rm_stats` (`stat_id`, `form_id`, `user_ip`, `ua_string`, `browser_name`, `visited_on`, `submitted_on`, `time_taken`, `submission_id`) VALUES
(4, 1, '79.237.62.214', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1547009115', NULL, NULL, NULL),
(5, 2, '79.237.62.214', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1547009130', NULL, NULL, NULL),
(6, 4, '79.237.62.214', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1547009307', NULL, NULL, NULL),
(7, 4, '79.237.62.214', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1547009325', NULL, NULL, NULL),
(8, 4, '79.237.62.214', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1547009371', NULL, NULL, NULL),
(9, 4, '79.237.62.214', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1547009647', NULL, NULL, NULL),
(10, 4, '79.237.62.214', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1547009750', NULL, NULL, NULL),
(11, 4, '79.237.62.214', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1547009779', '1547009837', 58, 3),
(12, 4, '79.237.62.214', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1547096666', NULL, NULL, NULL),
(13, 4, '79.237.62.214', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1', 'iPhone', '1547098013', NULL, NULL, NULL),
(14, 4, '2003:ec:2bde:5530:5027:489e:c4dd:9145', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1', 'iPhone', '1547100151', NULL, NULL, NULL),
(15, 4, '109.41.3.178', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1', 'iPhone', '1547103712', NULL, NULL, NULL),
(16, 5, '79.237.62.214', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1547265412', NULL, NULL, NULL),
(17, 5, '79.237.62.214', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1547265423', NULL, NULL, NULL),
(18, 5, '79.237.62.214', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1547266403', NULL, NULL, NULL),
(19, 4, '2003:ec:2bde:55e5:188c:4750:b612:d4e9', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1', 'iPhone', '1547291102', '1547291114', 12, 4),
(20, 4, '2003:ec:2bde:55e5:188c:4750:b612:d4e9', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1', 'iPhone', '1547291545', '1547291587', 42, 5),
(21, 4, '79.237.59.75', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1547352104', NULL, NULL, NULL),
(22, 4, '79.237.59.75', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1547352108', NULL, NULL, NULL),
(23, 4, '79.237.59.75', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1547436586', NULL, NULL, NULL),
(24, 4, '79.237.59.75', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1547436592', NULL, NULL, NULL),
(25, 4, '79.237.59.75', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1547437056', NULL, NULL, NULL),
(26, 4, '2003:ec:2bd5:d037:4cb2:150a:a45:e9a9', 'Mozilla/5.0 (iPad; CPU OS 12_1_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1', 'iPad', '1547968771', NULL, NULL, NULL),
(27, 4, '2003:ec:2bde:9c49:2953:852e:222f:599c', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1', 'iPhone', '1549082417', NULL, NULL, NULL),
(28, 4, '2003:ec:2bde:9c49:2953:852e:222f:599c', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1', 'iPhone', '1549082956', NULL, NULL, NULL),
(29, 4, '109.41.2.0', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_1_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1', 'iPhone', '1549129224', NULL, NULL, NULL),
(30, 4, '46.88.70.54', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1552366170', NULL, NULL, NULL),
(31, 4, '2003:ec:2f17:d2a0:89ea:7db6:e830:bb28', 'Mozilla/5.0 (iPad; CPU OS 12_1_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1', 'iPad', '1552717843', NULL, NULL, NULL),
(32, 4, '2003:ec:2f0c:500:5dab:4d19:6f5f:4631', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 'Chrome', '1552747418', NULL, NULL, NULL),
(33, 4, '46.88.70.54', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1552797007', NULL, NULL, NULL),
(34, 4, '2405:205:2082:d760:c41f:b18c:2334:c472', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 'Chrome', '1553251455', '1553251824', 369, 6),
(35, 4, '85.203.47.80', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'Chrome', '1554016272', '1554016308', 36, 7),
(36, 4, '103.255.5.249', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'Chrome', '1554017518', '1554017547', 29, 8),
(37, 4, '79.237.54.26', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1556448870', NULL, NULL, NULL),
(38, 4, '109.41.67.95', 'Mozilla/5.0 (Linux; Android 9; SM-G960F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.112 Mobile Safari/537.36', 'Chrome', '1556514735', '1556514801', 66, 9),
(39, 4, '109.41.3.25', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1 Mobile/15E148 Safari/604.1', 'iPhone', '1559167225', NULL, NULL, NULL),
(40, 4, '163.172.255.4', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 'Chrome', '1559461697', NULL, NULL, NULL),
(41, 4, '17.142.153.24', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/600.2.5 (KHTML, like Gecko) Version/8.0.2 Safari/600.2.5 (Applebot/0.1; +http://www.apple.com/go/applebot)', 'Safari', '1559692640', NULL, NULL, NULL),
(42, 4, '18.224.38.7', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36', 'Chrome', '1560574264', NULL, NULL, NULL),
(43, 4, '17.142.157.79', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/600.2.5 (KHTML, like Gecko) Version/8.0.2 Safari/600.2.5 (Applebot/0.1; +http://www.apple.com/go/applebot)', 'Safari', '1560904406', NULL, NULL, NULL),
(44, 4, '50.97.232.186', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'Chrome', '1561089767', '1561089779', 12, 10),
(45, 4, '185.38.241.4', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'Chrome', '1561089812', NULL, NULL, NULL),
(46, 4, '116.74.111.86', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'Chrome', '1561092737', NULL, NULL, NULL),
(47, 4, '46.88.69.5', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1561176619', NULL, NULL, NULL),
(48, 4, '46.88.69.5', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1561177214', NULL, NULL, NULL),
(49, 4, '45.41.181.56', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'Chrome', '1561237301', NULL, NULL, NULL),
(50, 4, '3.82.189.140', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'Chrome', '1561393359', '1561393407', 48, 11),
(51, 4, '69.4.228.114', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'Chrome', '1561437723', NULL, NULL, NULL),
(52, 4, '17.142.154.37', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/600.2.5 (KHTML, like Gecko) Version/8.0.2 Safari/600.2.5 (Applebot/0.1; +http://www.apple.com/go/applebot)', 'Safari', '1561625198', NULL, NULL, NULL),
(53, 4, '188.43.136.32', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.18362', 'Edge', '1561783198', '1561783243', 45, 12),
(54, 4, '27.5.7.79', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'Chrome', '1562053433', '1562053476', 43, 13),
(55, 4, '89.246.71.189', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Mobile/15E148 Safari/604.1', 'iPhone', '1562573154', NULL, NULL, NULL),
(56, 4, '36.72.254.174', 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'Chrome', '1562662902', NULL, NULL, NULL),
(57, 4, '210.56.127.245', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'Chrome', '1562665989', NULL, NULL, NULL),
(58, 4, '112.196.31.138', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'Chrome', '1562742570', '1562742589', 19, 14),
(59, 4, '13.59.180.12', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36', 'Chrome', '1562910967', NULL, NULL, NULL),
(60, 4, '150.129.88.74', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'Chrome', '1562924592', '1562924611', 19, 15),
(61, 4, '39.48.211.242', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'Chrome', '1562924856', NULL, NULL, NULL),
(62, 4, '94.185.62.205', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0', 'Firefox', '1562932475', NULL, NULL, NULL),
(63, 4, '42.112.231.29', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36', 'Chrome', '1562941726', NULL, NULL, NULL),
(64, 4, '192.162.234.179', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', 'Chrome', '1563175386', NULL, NULL, NULL),
(65, 4, '79.237.53.39', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1563251457', NULL, NULL, NULL),
(66, 4, '79.237.53.39', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1563255899', NULL, NULL, NULL),
(67, 4, '79.237.53.39', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1563255905', NULL, NULL, NULL),
(68, 4, '79.237.53.39', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1563255905', NULL, NULL, NULL),
(69, 4, '79.237.53.39', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1563333870', NULL, NULL, NULL),
(70, 4, '112.196.31.138', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'Chrome', '1563769257', '1563769313', 56, 16),
(71, 4, '109.40.1.131', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:60.0) Gecko/20100101 Firefox/60.0', 'Firefox', '1563943239', NULL, NULL, NULL),
(72, 4, '87.62.138.85', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Mobile/15E148 Safari/604.1', 'iPhone', '1563944327', NULL, NULL, NULL),
(73, 4, '109.40.131.224', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Mobile/15E148 Safari/604.1', 'iPhone', '1564053054', NULL, NULL, NULL),
(74, 4, '109.40.131.224', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36', 'Chrome', '1564053490', NULL, NULL, NULL),
(75, 4, '66.249.64.8', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)', 'Chrome', '1564355330', NULL, NULL, NULL),
(76, 4, '27.56.156.91', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'Chrome', '1564720085', '1564720117', 32, 17),
(77, 4, '104.192.74.237', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/70.0.3538.110 Safari/537.36', 'Chrome', '1564868325', NULL, NULL, NULL),
(78, 4, '104.192.74.237', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/70.0.3538.110 Safari/537.36', 'Chrome', '1564868372', NULL, NULL, NULL),
(79, 4, '112.196.31.138', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'Chrome', '1565074133', '1565074157', 24, 18),
(80, 4, '112.196.31.138', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'Chrome', '1565074208', NULL, NULL, NULL),
(81, 4, '112.196.31.138', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'Chrome', '1565676314', '1565676331', 17, 19),
(82, 4, '18.220.141.88', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'Chrome', '1565854890', NULL, NULL, NULL),
(83, 4, '18.220.141.88', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'Chrome', '1565854892', NULL, NULL, NULL),
(84, 4, '46.88.76.102', 'Mozilla/5.0 (iPad; CPU OS 12_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1', 'iPad', '1567394498', '1567394589', 91, 20),
(85, 4, '40.77.190.106', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534+ (KHTML, like Gecko) BingPreview/1.0b', 'Mozilla', '1567913594', NULL, NULL, NULL),
(86, 4, '3.15.22.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'Chrome', '1568540957', NULL, NULL, NULL),
(87, 4, '3.15.22.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'Chrome', '1568540962', NULL, NULL, NULL),
(88, 4, '112.196.31.138', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'Chrome', '1568954657', NULL, NULL, NULL),
(89, 4, '167.114.90.33', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36', 'Chrome', '1569510599', NULL, NULL, NULL),
(90, 4, '167.114.90.33', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0', 'Firefox', '1569510627', NULL, NULL, NULL),
(91, 4, '112.196.31.138', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'Chrome', '1570094648', '1570094692', 44, 21),
(92, 4, '18.188.177.47', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'Chrome', '1570884121', NULL, NULL, NULL),
(93, 4, '18.188.177.47', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'Chrome', '1570884127', NULL, NULL, NULL),
(94, 4, '112.196.31.138', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'Chrome', '1571827328', '1571827398', 70, 22);

-- --------------------------------------------------------

--
-- Table structure for table `wp_rm_submissions`
--

CREATE TABLE `wp_rm_submissions` (
  `submission_id` int(6) UNSIGNED NOT NULL,
  `form_id` int(6) DEFAULT NULL,
  `data` text COLLATE utf8mb4_unicode_ci,
  `user_email` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `child_id` int(6) NOT NULL DEFAULT '0',
  `last_child` int(6) NOT NULL DEFAULT '0',
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `submitted_on` datetime DEFAULT NULL,
  `unique_token` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_rm_submissions`
--

INSERT INTO `wp_rm_submissions` (`submission_id`, `form_id`, `data`, `user_email`, `child_id`, `last_child`, `is_read`, `submitted_on`, `unique_token`) VALUES
(3, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:16:\"markus.hu@gmx.de\";s:4:\"type\";s:5:\"Email\";}}', 'markus.hu@gmx.de', 0, 3, 0, '2019-01-09 04:57:17', '415470098374503'),
(4, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:17:\"hungenbach@web.de\";s:4:\"type\";s:5:\"Email\";}}', 'hungenbach@web.de', 0, 4, 0, '2019-01-12 11:05:14', '415472911141899'),
(5, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:17:\"hungenbach@web.de\";s:4:\"type\";s:5:\"Email\";}}', 'hungenbach@web.de', 0, 5, 0, '2019-01-12 11:13:07', '415472915877179'),
(6, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:31:\"umesh.wordpressexpert@gmail.com\";s:4:\"type\";s:5:\"Email\";}}', 'umesh.wordpressexpert@gmail.com', 0, 6, 0, '2019-03-22 10:50:24', '415532518248602'),
(7, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:20:\"bule_house@yahoo.com\";s:4:\"type\";s:5:\"Email\";}}', 'bule_house@yahoo.com', 0, 7, 0, '2019-03-31 07:11:48', '415540163085803'),
(8, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:23:\"shahid.ranjha@gmail.com\";s:4:\"type\";s:5:\"Email\";}}', 'shahid.ranjha@gmail.com', 0, 8, 0, '2019-03-31 07:32:27', '415540175476650'),
(9, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:24:\"christoph.selbach@gmx.de\";s:4:\"type\";s:5:\"Email\";}}', 'christoph.selbach@gmx.de', 0, 9, 0, '2019-04-29 05:13:21', '415565148012811'),
(10, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:14:\"test@gmail.com\";s:4:\"type\";s:5:\"Email\";}}', 'test@gmail.com', 0, 10, 0, '2019-06-21 04:02:59', '415610897796795'),
(11, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:18:\"giefsl@outlook.com\";s:4:\"type\";s:5:\"Email\";}}', 'giefsl@outlook.com', 0, 11, 0, '2019-06-24 16:23:27', '415613934079607'),
(12, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:22:\"webdev1888@outlook.com\";s:4:\"type\";s:5:\"Email\";}}', 'webdev1888@outlook.com', 0, 12, 0, '2019-06-29 04:40:43', '415617832431850'),
(13, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:26:\"ipanimationworld@gmail.com\";s:4:\"type\";s:5:\"Email\";}}', 'ipanimationworld@gmail.com', 0, 13, 0, '2019-07-02 07:44:36', '415620534766907'),
(14, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:17:\"tt@mailinator.com\";s:4:\"type\";s:5:\"Email\";}}', 'tt@mailinator.com', 0, 14, 0, '2019-07-10 07:09:49', '415627425894864'),
(15, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:22:\"pramodnp.pnp@gmail.com\";s:4:\"type\";s:5:\"Email\";}}', 'pramodnp.pnp@gmail.com', 0, 15, 0, '2019-07-12 09:43:31', '415629246115584'),
(16, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:18:\"julian@yopmail.com\";s:4:\"type\";s:5:\"Email\";}}', 'julian@yopmail.com', 0, 16, 0, '2019-07-22 04:21:53', '415637693133278'),
(17, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:32:\"mohammadyasirchicmic59@gmail.com\";s:4:\"type\";s:5:\"Email\";}}', 'mohammadyasirchicmic59@gmail.com', 0, 17, 0, '2019-08-02 04:28:37', '415647201179814'),
(18, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:19:\"shiv@mailinator.com\";s:4:\"type\";s:5:\"Email\";}}', 'shiv@mailinator.com', 0, 18, 0, '2019-08-06 06:49:17', '41565074157562'),
(19, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:18:\"sss@mailinator.com\";s:4:\"type\";s:5:\"Email\";}}', 'sss@mailinator.com', 0, 19, 0, '2019-08-13 06:05:31', '415656763312040'),
(20, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:23:\"webmaster@hungenbach.de\";s:4:\"type\";s:5:\"Email\";}}', 'webmaster@hungenbach.de', 0, 20, 0, '2019-09-02 03:23:09', '415673945894672'),
(21, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:26:\"amrish.kumar@chicmic.co.in\";s:4:\"type\";s:5:\"Email\";}}', 'amrish.kumar@chicmic.co.in', 0, 21, 0, '2019-10-03 09:24:52', '415700946926610'),
(22, 4, 'a:1:{i:23;O:8:\"stdClass\":3:{s:5:\"label\";s:5:\"Email\";s:5:\"value\";s:20:\"dhiraj@chicmic.co.in\";s:4:\"type\";s:5:\"Email\";}}', 'dhiraj@chicmic.co.in', 0, 22, 0, '2019-10-23 10:43:18', '415718273981827');

-- --------------------------------------------------------

--
-- Table structure for table `wp_rm_submission_fields`
--

CREATE TABLE `wp_rm_submission_fields` (
  `sub_field_id` int(6) UNSIGNED NOT NULL,
  `submission_id` int(6) DEFAULT NULL,
  `field_id` int(6) DEFAULT NULL,
  `form_id` int(6) DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_rm_submission_fields`
--

INSERT INTO `wp_rm_submission_fields` (`sub_field_id`, `submission_id`, `field_id`, `form_id`, `value`) VALUES
(3, 3, 23, 4, 'markus.hu@gmx.de'),
(4, 4, 23, 4, 'hungenbach@web.de'),
(5, 5, 23, 4, 'hungenbach@web.de'),
(6, 6, 23, 4, 'umesh.wordpressexpert@gmail.com'),
(7, 7, 23, 4, 'bule_house@yahoo.com'),
(8, 8, 23, 4, 'shahid.ranjha@gmail.com'),
(9, 9, 23, 4, 'christoph.selbach@gmx.de'),
(10, 10, 23, 4, 'test@gmail.com'),
(11, 11, 23, 4, 'giefsl@outlook.com'),
(12, 12, 23, 4, 'webdev1888@outlook.com'),
(13, 13, 23, 4, 'ipanimationworld@gmail.com'),
(14, 14, 23, 4, 'tt@mailinator.com'),
(15, 15, 23, 4, 'pramodnp.pnp@gmail.com'),
(16, 16, 23, 4, 'julian@yopmail.com'),
(17, 17, 23, 4, 'mohammadyasirchicmic59@gmail.com'),
(18, 18, 23, 4, 'shiv@mailinator.com'),
(19, 19, 23, 4, 'sss@mailinator.com'),
(20, 20, 23, 4, 'webmaster@hungenbach.de'),
(21, 21, 23, 4, 'amrish.kumar@chicmic.co.in'),
(22, 22, 23, 4, 'dhiraj@chicmic.co.in');

-- --------------------------------------------------------

--
-- Table structure for table `wp_rm_tasks`
--

CREATE TABLE `wp_rm_tasks` (
  `task_id` int(6) UNSIGNED NOT NULL,
  `form_id` int(6) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `desc` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `must_rules` text COLLATE utf8mb4_unicode_ci,
  `any_rules` text COLLATE utf8mb4_unicode_ci,
  `is_active` tinyint(1) DEFAULT '1',
  `actions` text COLLATE utf8mb4_unicode_ci,
  `task_order` int(6) DEFAULT NULL,
  `meta` text COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_rm_task_exe_log`
--

CREATE TABLE `wp_rm_task_exe_log` (
  `texe_log_id` int(6) UNSIGNED NOT NULL,
  `task_id` int(6) DEFAULT NULL,
  `action` int(6) DEFAULT NULL,
  `sub_ids` longtext COLLATE utf8mb4_unicode_ci,
  `user_ids` longtext COLLATE utf8mb4_unicode_ci,
  `meta` text COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'header-logged-out', 'header-logged-out', 0),
(3, 'header-logged-in', 'header-logged-in', 0),
(4, 'General', 'general', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 4, 0),
(33, 2, 0),
(34, 2, 0),
(35, 2, 0),
(59, 2, 0),
(67, 3, 0),
(241, 3, 0),
(70, 3, 0),
(72, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'nav_menu', '', 0, 4),
(3, 3, 'nav_menu', '', 0, 4),
(4, 4, 'category', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'markus@hungenbach.de'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '258'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:10:\"46.88.76.0\";}'),
(19, 1, 'wp_user-settings', 'libraryContent=browse&editor=html'),
(20, 1, 'wp_user-settings-time', '1547096633'),
(38, 1, 'nav_menu_recently_edited', '3'),
(39, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(40, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:\"add-post_tag\";}'),
(56, 3, 'nickname', 'Test2'),
(57, 3, 'first_name', ''),
(58, 3, 'last_name', ''),
(59, 3, 'description', ''),
(60, 3, 'rich_editing', 'true'),
(61, 3, 'syntax_highlighting', 'true'),
(62, 3, 'comment_shortcuts', 'false'),
(63, 3, 'admin_color', 'fresh'),
(64, 3, 'use_ssl', '0'),
(65, 3, 'show_admin_bar_front', 'true'),
(66, 3, 'locale', ''),
(67, 3, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(68, 3, 'wp_user_level', '0'),
(69, 3, 'dismissed_wp_pointers', 'wp496_privacy'),
(70, 3, 'rm_user_status', '0'),
(71, 3, 'RM_UMETA_FORM_ID', '4'),
(72, 3, 'RM_UMETA_SUB_ID', '5'),
(74, 3, 'wpfm_total_filesize_used', '169152'),
(75, 3, 'community-events-location', 'a:1:{s:2:\"ip\";s:11:\"79.237.59.0\";}'),
(84, 1, 'closedpostboxes_page', 'a:0:{}'),
(85, 1, 'metaboxhidden_page', 'a:0:{}'),
(86, 1, 'wp_media_library_mode', 'list'),
(91, 4, 'nickname', 'markus.hu@gmx.de'),
(92, 4, 'first_name', ''),
(93, 4, 'last_name', ''),
(94, 4, 'description', ''),
(95, 4, 'rich_editing', 'true'),
(96, 4, 'syntax_highlighting', 'true'),
(97, 4, 'comment_shortcuts', 'false'),
(98, 4, 'admin_color', 'fresh'),
(99, 4, 'use_ssl', '0'),
(100, 4, 'show_admin_bar_front', 'true'),
(101, 4, 'locale', ''),
(102, 4, 'wp_capabilities', 'a:1:{s:6:\"editor\";b:1;}'),
(103, 4, 'wp_user_level', '7'),
(104, 4, 'dismissed_wp_pointers', 'wp496_privacy'),
(106, 5, 'nickname', 'umesh'),
(107, 5, 'first_name', ''),
(108, 5, 'last_name', ''),
(109, 5, 'description', ''),
(110, 5, 'rich_editing', 'true'),
(111, 5, 'syntax_highlighting', 'true'),
(112, 5, 'comment_shortcuts', 'false'),
(113, 5, 'admin_color', 'fresh'),
(114, 5, 'use_ssl', '0'),
(115, 5, 'show_admin_bar_front', 'true'),
(116, 5, 'locale', ''),
(117, 5, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(118, 5, 'wp_user_level', '0'),
(119, 5, 'dismissed_wp_pointers', 'wp496_privacy'),
(120, 5, 'rm_user_status', '0'),
(121, 5, 'RM_UMETA_FORM_ID', '4'),
(122, 5, 'RM_UMETA_SUB_ID', '6'),
(123, 5, 'session_tokens', 'a:1:{s:64:\"de23d09c8e3f86756ffd8677fef50dffc634a3ac074a1d7343c4004001350543\";a:4:{s:10:\"expiration\";i:1554461648;s:2:\"ip\";s:38:\"2405:205:2082:d760:c41f:b18c:2334:c472\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36\";s:5:\"login\";i:1553252048;}}'),
(124, 6, 'nickname', 'KZZ'),
(125, 6, 'first_name', ''),
(126, 6, 'last_name', ''),
(127, 6, 'description', ''),
(128, 6, 'rich_editing', 'true'),
(129, 6, 'syntax_highlighting', 'true'),
(130, 6, 'comment_shortcuts', 'false'),
(131, 6, 'admin_color', 'fresh'),
(132, 6, 'use_ssl', '0'),
(133, 6, 'show_admin_bar_front', 'true'),
(134, 6, 'locale', ''),
(135, 6, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(136, 6, 'wp_user_level', '0'),
(137, 6, 'dismissed_wp_pointers', 'wp496_privacy'),
(138, 6, 'rm_user_status', '0'),
(139, 6, 'RM_UMETA_FORM_ID', '4'),
(140, 6, 'RM_UMETA_SUB_ID', '7'),
(141, 7, 'nickname', 'shahidabd'),
(142, 7, 'first_name', ''),
(143, 7, 'last_name', ''),
(144, 7, 'description', ''),
(145, 7, 'rich_editing', 'true'),
(146, 7, 'syntax_highlighting', 'true'),
(147, 7, 'comment_shortcuts', 'false'),
(148, 7, 'admin_color', 'fresh'),
(149, 7, 'use_ssl', '0'),
(150, 7, 'show_admin_bar_front', 'true'),
(151, 7, 'locale', ''),
(152, 7, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(153, 7, 'wp_user_level', '0'),
(154, 7, 'dismissed_wp_pointers', 'wp496_privacy'),
(155, 7, 'rm_user_status', '0'),
(156, 7, 'RM_UMETA_FORM_ID', '4'),
(157, 7, 'RM_UMETA_SUB_ID', '8'),
(158, 7, 'session_tokens', 'a:1:{s:64:\"4b2c39e568f36689f966cc6769f0a5da190566c6411950999206f2fdd26c73c2\";a:4:{s:10:\"expiration\";i:1555227179;s:2:\"ip\";s:13:\"103.255.5.249\";s:2:\"ua\";s:113:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36\";s:5:\"login\";i:1554017579;}}'),
(159, 8, 'nickname', 'cselbach'),
(160, 8, 'first_name', ''),
(161, 8, 'last_name', ''),
(162, 8, 'description', ''),
(163, 8, 'rich_editing', 'true'),
(164, 8, 'syntax_highlighting', 'true'),
(165, 8, 'comment_shortcuts', 'false'),
(166, 8, 'admin_color', 'fresh'),
(167, 8, 'use_ssl', '0'),
(168, 8, 'show_admin_bar_front', 'true'),
(169, 8, 'locale', ''),
(170, 8, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(171, 8, 'wp_user_level', '0'),
(172, 8, 'dismissed_wp_pointers', 'wp496_privacy'),
(173, 8, 'rm_user_status', '0'),
(174, 8, 'RM_UMETA_FORM_ID', '4'),
(175, 8, 'RM_UMETA_SUB_ID', '9'),
(176, 8, 'session_tokens', 'a:1:{s:64:\"27057de6303c22abfb07698b222644f537f8065eb71f276f74474e78e9a08958\";a:4:{s:10:\"expiration\";i:1557724526;s:2:\"ip\";s:12:\"109.41.67.95\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Linux; Android 9; SM-G960F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.112 Mobile Safari/537.36\";s:5:\"login\";i:1556514926;}}'),
(177, 9, 'nickname', 'test'),
(178, 9, 'first_name', ''),
(179, 9, 'last_name', ''),
(180, 9, 'description', ''),
(181, 9, 'rich_editing', 'true'),
(182, 9, 'syntax_highlighting', 'true'),
(183, 9, 'comment_shortcuts', 'false'),
(184, 9, 'admin_color', 'fresh'),
(185, 9, 'use_ssl', '0'),
(186, 9, 'show_admin_bar_front', 'true'),
(187, 9, 'locale', ''),
(188, 9, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(189, 9, 'wp_user_level', '0'),
(190, 9, 'dismissed_wp_pointers', 'wp496_privacy'),
(191, 9, 'rm_user_status', '0'),
(192, 9, 'RM_UMETA_FORM_ID', '4'),
(193, 9, 'RM_UMETA_SUB_ID', '10'),
(194, 9, 'session_tokens', 'a:4:{s:64:\"fcd1418a57167b72cf523ca5b62cb0d752f1269b8f6f6e22ba40ac338b16a8f2\";a:4:{s:10:\"expiration\";i:1571839969;s:2:\"ip\";s:12:\"5.188.62.147\";s:2:\"ua\";s:107:\"Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2226.0 Safari/537.36\";s:5:\"login\";i:1571667169;}s:64:\"731ce46c8b9ee247a6d8021686c55731388b040354f1d344f356dc4c6c71ffe7\";a:4:{s:10:\"expiration\";i:1571839970;s:2:\"ip\";s:12:\"5.188.62.147\";s:2:\"ua\";s:107:\"Mozilla/5.0 (Windows NT 6.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2226.0 Safari/537.36\";s:5:\"login\";i:1571667170;}s:64:\"47fb20c47e802b0cdcb7afda24d9a968f0e2037191835e188a417a41d38df1f6\";a:4:{s:10:\"expiration\";i:1571840599;s:2:\"ip\";s:10:\"5.188.62.5\";s:2:\"ua\";s:100:\"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36\";s:5:\"login\";i:1571667799;}s:64:\"4d2f6da73f71fa42cf7a1a1e19babe1810c7609a9eb7556ca5b39a13c7246112\";a:4:{s:10:\"expiration\";i:1571840600;s:2:\"ip\";s:10:\"5.188.62.5\";s:2:\"ua\";s:100:\"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2225.0 Safari/537.36\";s:5:\"login\";i:1571667800;}}'),
(195, 4, 'default_password_nag', ''),
(196, 4, 'session_tokens', 'a:5:{s:64:\"11e4fc22325ff39299ace700ee8d0cb3ae5bbdd2d5d5c9cd9b368ea692b57d06\";a:4:{s:10:\"expiration\";i:1571715638;s:2:\"ip\";s:12:\"46.88.78.196\";s:2:\"ua\";s:139:\"Mozilla/5.0 (iPhone; CPU iPhone OS 13_1_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.1 Mobile/15E148 Safari/604.1\";s:5:\"login\";i:1571542838;}s:64:\"68d29d1d22f8b18c4c2e3b138c65d618f05f0b4b911d4b94154e63491249d78e\";a:4:{s:10:\"expiration\";i:1572752438;s:2:\"ip\";s:12:\"46.88.78.196\";s:2:\"ua\";s:139:\"Mozilla/5.0 (iPhone; CPU iPhone OS 13_1_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.1 Mobile/15E148 Safari/604.1\";s:5:\"login\";i:1571542838;}s:64:\"bda2d1af0ecfaed77348a1b156c159db9584c8e7a2a803ad7433ad20c4589d4b\";a:4:{s:10:\"expiration\";i:1571846717;s:2:\"ip\";s:13:\"109.40.67.190\";s:2:\"ua\";s:139:\"Mozilla/5.0 (iPhone; CPU iPhone OS 13_1_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.1 Mobile/15E148 Safari/604.1\";s:5:\"login\";i:1571673917;}s:64:\"c3f41c1534a5827962007f1c3bee9ad05e694c2cacf7936e30bad14b94e3f0fd\";a:4:{s:10:\"expiration\";i:1571847151;s:2:\"ip\";s:13:\"109.40.67.190\";s:2:\"ua\";s:139:\"Mozilla/5.0 (iPhone; CPU iPhone OS 13_1_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.1 Mobile/15E148 Safari/604.1\";s:5:\"login\";i:1571674351;}s:64:\"05a24cbe0f44d485ea1fbd15a14560c92ab00b4ce05fdfb5a139ed4b041cfdf3\";a:4:{s:10:\"expiration\";i:1572883951;s:2:\"ip\";s:13:\"109.40.67.190\";s:2:\"ua\";s:139:\"Mozilla/5.0 (iPhone; CPU iPhone OS 13_1_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.1 Mobile/15E148 Safari/604.1\";s:5:\"login\";i:1571674351;}}'),
(198, 1, 'wpfm_total_filesize_used', '11811'),
(199, 10, 'nickname', 'test111'),
(200, 10, 'first_name', ''),
(201, 10, 'last_name', ''),
(202, 10, 'description', ''),
(203, 10, 'rich_editing', 'true'),
(204, 10, 'syntax_highlighting', 'true'),
(205, 10, 'comment_shortcuts', 'false'),
(206, 10, 'admin_color', 'fresh'),
(207, 10, 'use_ssl', '0'),
(208, 10, 'show_admin_bar_front', 'true'),
(209, 10, 'locale', ''),
(210, 10, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(211, 10, 'wp_user_level', '0'),
(212, 10, 'dismissed_wp_pointers', ''),
(213, 10, 'rm_user_status', '0'),
(214, 10, 'RM_UMETA_FORM_ID', '4'),
(215, 10, 'RM_UMETA_SUB_ID', '11'),
(216, 10, 'session_tokens', 'a:4:{s:64:\"0b4c7636d146dd795e4cd97587b9d9728dfa63d82f73b4e08b80292042607722\";a:4:{s:10:\"expiration\";i:1561566260;s:2:\"ip\";s:12:\"3.82.189.140\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36\";s:5:\"login\";i:1561393460;}s:64:\"057023c529ab0fc417443687dc236a342096c74a15efaa2efd313fdadec48a99\";a:4:{s:10:\"expiration\";i:1562603060;s:2:\"ip\";s:12:\"3.82.189.140\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36\";s:5:\"login\";i:1561393460;}s:64:\"15f3c51b431f22a6ca7656ca40d655d15648c6b431c685fffa45387763386e91\";a:4:{s:10:\"expiration\";i:1561610537;s:2:\"ip\";s:12:\"69.4.228.114\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36\";s:5:\"login\";i:1561437737;}s:64:\"e51d55a23a45308025962bca2ed59ebf2e383c82fcd07f1e692774f7e0042b87\";a:4:{s:10:\"expiration\";i:1562647337;s:2:\"ip\";s:12:\"69.4.228.114\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36\";s:5:\"login\";i:1561437737;}}'),
(217, 10, 'wpfm_total_filesize_used', '97989'),
(356, 17, 'RM_UMETA_SUB_ID', '17'),
(357, 17, 'session_tokens', 'a:4:{s:64:\"fe04a018ee0793ce1c13bf11ee2deca7801ceb8ef9aedcf69a9364a67d60baae\";a:4:{s:10:\"expiration\";i:1570864775;s:2:\"ip\";s:14:\"112.196.31.138\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1570691975;}s:64:\"21d05738fa624401841a11a661372c499978b632f092bb0cd2b3687108b85396\";a:4:{s:10:\"expiration\";i:1571901575;s:2:\"ip\";s:14:\"112.196.31.138\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1570691975;}s:64:\"77782cf5d77f3734c6b64add45bf0761445b9bdaad510a6cfb9d6782b4b29fda\";a:4:{s:10:\"expiration\";i:1570865419;s:2:\"ip\";s:14:\"112.196.31.138\";s:2:\"ua\";s:104:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1570692619;}s:64:\"95a79e12468736f8b270f14a64fad48bd532562e9f36c265b2eb28bb677ce378\";a:4:{s:10:\"expiration\";i:1571902219;s:2:\"ip\";s:14:\"112.196.31.138\";s:2:\"ua\";s:104:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1570692619;}}'),
(219, 11, 'nickname', 'AlexBregman'),
(220, 11, 'first_name', ''),
(221, 11, 'last_name', ''),
(222, 11, 'description', ''),
(223, 11, 'rich_editing', 'true'),
(224, 11, 'syntax_highlighting', 'true'),
(225, 11, 'comment_shortcuts', 'false'),
(226, 11, 'admin_color', 'fresh'),
(227, 11, 'use_ssl', '0'),
(228, 11, 'show_admin_bar_front', 'true'),
(229, 11, 'locale', ''),
(230, 11, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(231, 11, 'wp_user_level', '0'),
(232, 11, 'dismissed_wp_pointers', ''),
(233, 11, 'rm_user_status', '0'),
(234, 11, 'RM_UMETA_FORM_ID', '4'),
(235, 11, 'RM_UMETA_SUB_ID', '12'),
(236, 11, 'session_tokens', 'a:2:{s:64:\"eebd41fa87fba04da7fa44c0f9aba31d79c4c95c778f486357e64e5cfdfd1da2\";a:4:{s:10:\"expiration\";i:1561956898;s:2:\"ip\";s:13:\"188.43.136.32\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36\";s:5:\"login\";i:1561784098;}s:64:\"592479ab47d0f7070e94c9e7ff121b3753859513279944a6d99674411ef629f2\";a:4:{s:10:\"expiration\";i:1562993698;s:2:\"ip\";s:13:\"188.43.136.32\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36\";s:5:\"login\";i:1561784098;}}'),
(237, 11, 'wpfm_total_filesize_used', '7553'),
(238, 12, 'nickname', 'testing'),
(239, 12, 'first_name', ''),
(240, 12, 'last_name', ''),
(241, 12, 'description', ''),
(242, 12, 'rich_editing', 'true'),
(243, 12, 'syntax_highlighting', 'true'),
(244, 12, 'comment_shortcuts', 'false'),
(245, 12, 'admin_color', 'fresh'),
(246, 12, 'use_ssl', '0'),
(247, 12, 'show_admin_bar_front', 'true'),
(248, 12, 'locale', ''),
(249, 12, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(250, 12, 'wp_user_level', '0'),
(251, 12, 'dismissed_wp_pointers', ''),
(252, 12, 'rm_user_status', '0'),
(253, 12, 'RM_UMETA_FORM_ID', '4'),
(254, 12, 'RM_UMETA_SUB_ID', '13'),
(255, 12, 'session_tokens', 'a:2:{s:64:\"5736d8c16e5181fa48f85aba3ce64b47582f7a51ad0fdb1f4086a3148bed1aae\";a:4:{s:10:\"expiration\";i:1562226285;s:2:\"ip\";s:9:\"27.5.7.79\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36\";s:5:\"login\";i:1562053485;}s:64:\"7fb5072eb9feb8c149109dad92e96f033575ebc6430187bb076f330fd51dd421\";a:4:{s:10:\"expiration\";i:1563263085;s:2:\"ip\";s:9:\"27.5.7.79\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36\";s:5:\"login\";i:1562053485;}}'),
(256, 12, 'wpfm_total_filesize_used', '170088'),
(257, 13, 'nickname', 'tt'),
(258, 13, 'first_name', ''),
(259, 13, 'last_name', ''),
(260, 13, 'description', ''),
(261, 13, 'rich_editing', 'true'),
(262, 13, 'syntax_highlighting', 'true'),
(263, 13, 'comment_shortcuts', 'false'),
(264, 13, 'admin_color', 'fresh'),
(265, 13, 'use_ssl', '0'),
(266, 13, 'show_admin_bar_front', 'true'),
(267, 13, 'locale', ''),
(268, 13, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(269, 13, 'wp_user_level', '0'),
(270, 13, 'dismissed_wp_pointers', ''),
(271, 13, 'rm_user_status', '0'),
(272, 13, 'RM_UMETA_FORM_ID', '4'),
(273, 13, 'RM_UMETA_SUB_ID', '14'),
(275, 14, 'nickname', 'pramod'),
(276, 14, 'first_name', ''),
(277, 14, 'last_name', ''),
(278, 14, 'description', ''),
(279, 14, 'rich_editing', 'true'),
(280, 14, 'syntax_highlighting', 'true'),
(281, 14, 'comment_shortcuts', 'false'),
(282, 14, 'admin_color', 'fresh'),
(283, 14, 'use_ssl', '0'),
(284, 14, 'show_admin_bar_front', 'true'),
(285, 14, 'locale', ''),
(286, 14, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(287, 14, 'wp_user_level', '0'),
(288, 14, 'dismissed_wp_pointers', ''),
(289, 14, 'rm_user_status', '0'),
(290, 14, 'RM_UMETA_FORM_ID', '4'),
(291, 14, 'RM_UMETA_SUB_ID', '15'),
(292, 14, 'session_tokens', 'a:2:{s:64:\"0eb901782cb61fb7907729fdbeaa45cc9248356d1d67766313388734860a1ddc\";a:4:{s:10:\"expiration\";i:1563097423;s:2:\"ip\";s:13:\"150.129.88.74\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36\";s:5:\"login\";i:1562924623;}s:64:\"2c6cda3e341a12a20e8d130e380909680903a996c2bde7e0343b82b7292062f5\";a:4:{s:10:\"expiration\";i:1564134223;s:2:\"ip\";s:13:\"150.129.88.74\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36\";s:5:\"login\";i:1562924623;}}'),
(293, 13, 'wpfm_total_filesize_used', '141138'),
(294, 4, 'wpfm_total_filesize_used', '844'),
(414, 15, 'session_tokens', 'a:1:{s:64:\"059714a241ad482c14d18de29e9e5a6cb863503ca893b78daae149f7a222b200\";a:4:{s:10:\"expiration\";i:1573305807;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:104:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1573133007;}}'),
(295, 15, 'nickname', 'ChicMic'),
(296, 15, 'first_name', ''),
(297, 15, 'last_name', ''),
(298, 15, 'description', ''),
(299, 15, 'rich_editing', 'true'),
(300, 15, 'syntax_highlighting', 'true'),
(301, 15, 'comment_shortcuts', 'false'),
(302, 15, 'admin_color', 'fresh'),
(303, 15, 'use_ssl', '0'),
(304, 15, 'show_admin_bar_front', 'true'),
(305, 15, 'locale', ''),
(306, 15, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(307, 15, 'wp_user_level', '10'),
(308, 15, 'dismissed_wp_pointers', 'theme_editor_notice'),
(313, 16, 'first_name', ''),
(314, 16, 'last_name', ''),
(315, 16, 'description', ''),
(316, 16, 'rich_editing', 'true'),
(317, 16, 'syntax_highlighting', 'true'),
(318, 16, 'comment_shortcuts', 'false'),
(312, 16, 'nickname', 'julian'),
(310, 15, 'wp_dashboard_quick_press_last_post_id', '665'),
(311, 15, 'community-events-location', 'a:1:{s:2:\"ip\";s:11:\"192.168.2.0\";}'),
(319, 16, 'admin_color', 'fresh'),
(320, 16, 'use_ssl', '0'),
(321, 16, 'show_admin_bar_front', 'true'),
(322, 16, 'locale', ''),
(323, 16, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(324, 16, 'wp_user_level', '0'),
(325, 16, 'dismissed_wp_pointers', ''),
(326, 16, 'rm_user_status', '0'),
(327, 16, 'RM_UMETA_FORM_ID', '4'),
(328, 16, 'RM_UMETA_SUB_ID', '16'),
(329, 16, 'session_tokens', 'a:2:{s:64:\"e0d07aca7fd80db49cab347452164cfaa8282d08eca24df6d769f7d8e4ec3d63\";a:4:{s:10:\"expiration\";i:1563942142;s:2:\"ip\";s:14:\"112.196.31.138\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36\";s:5:\"login\";i:1563769342;}s:64:\"1a27f28a969f5a0972b3e93bf17091bd33188dfa8371cdaed4c34450360ec3e0\";a:4:{s:10:\"expiration\";i:1564978942;s:2:\"ip\";s:14:\"112.196.31.138\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36\";s:5:\"login\";i:1563769342;}}'),
(330, 16, 'wpfm_total_filesize_used', '270598'),
(332, 15, 'wp_media_library_mode', 'grid'),
(361, 18, 'description', ''),
(384, 19, 'admin_color', 'fresh'),
(385, 19, 'use_ssl', '0'),
(386, 19, 'show_admin_bar_front', 'true'),
(387, 19, 'locale', ''),
(388, 19, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(389, 19, 'wp_user_level', '0'),
(390, 19, 'dismissed_wp_pointers', ''),
(391, 19, 'rm_user_status', '0'),
(392, 19, 'RM_UMETA_FORM_ID', '4'),
(393, 19, 'RM_UMETA_SUB_ID', '19'),
(334, 15, 'wpfm_total_filesize_used', '305193'),
(377, 19, 'nickname', 'SS'),
(378, 19, 'first_name', ''),
(379, 19, 'last_name', ''),
(380, 19, 'description', ''),
(381, 19, 'rich_editing', 'true'),
(382, 19, 'syntax_highlighting', 'true'),
(383, 19, 'comment_shortcuts', 'false'),
(362, 18, 'rich_editing', 'true'),
(363, 18, 'syntax_highlighting', 'true'),
(352, 17, 'wp_user_level', '0'),
(353, 17, 'dismissed_wp_pointers', ''),
(354, 17, 'rm_user_status', '0'),
(355, 17, 'RM_UMETA_FORM_ID', '4'),
(337, 15, 'closedpostboxes_page', 'a:1:{i:0;s:22:\"onepress_page_settings\";}'),
(338, 15, 'metaboxhidden_page', 'a:0:{}'),
(340, 17, 'nickname', 'mohammad'),
(341, 17, 'first_name', ''),
(342, 17, 'last_name', ''),
(343, 17, 'description', ''),
(344, 17, 'rich_editing', 'true'),
(345, 17, 'syntax_highlighting', 'true'),
(346, 17, 'comment_shortcuts', 'false'),
(347, 17, 'admin_color', 'fresh'),
(348, 17, 'use_ssl', '0'),
(349, 17, 'show_admin_bar_front', 'true'),
(350, 17, 'locale', ''),
(351, 17, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(358, 18, 'nickname', 'shiv'),
(359, 18, 'first_name', ''),
(360, 18, 'last_name', ''),
(364, 18, 'comment_shortcuts', 'false'),
(365, 18, 'admin_color', 'fresh'),
(366, 18, 'use_ssl', '0'),
(367, 18, 'show_admin_bar_front', 'true'),
(368, 18, 'locale', ''),
(369, 18, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(370, 18, 'wp_user_level', '0'),
(371, 18, 'dismissed_wp_pointers', ''),
(372, 18, 'rm_user_status', '0'),
(373, 18, 'RM_UMETA_FORM_ID', '4'),
(374, 18, 'RM_UMETA_SUB_ID', '18'),
(375, 18, 'default_password_nag', ''),
(376, 18, 'session_tokens', 'a:1:{s:64:\"a0e10c9bdbfcacb27b97e3eca24e0ea3a7cf7c877a4458447a2a4044896879e9\";a:4:{s:10:\"expiration\";i:1569127606;s:2:\"ip\";s:14:\"112.196.31.138\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36\";s:5:\"login\";i:1568954806;}}'),
(394, 19, 'session_tokens', 'a:5:{s:64:\"768f65ed04e4441b53fbc8d77bf86635056c37ffb841f1f8e9341880a75911d4\";a:4:{s:10:\"expiration\";i:1565849203;s:2:\"ip\";s:14:\"112.196.31.138\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36\";s:5:\"login\";i:1565676403;}s:64:\"6a0c1c4b6fcc1c0e8e413d6907e4f1245d40e29daa0c4fcd1a71d1ee1f566f65\";a:4:{s:10:\"expiration\";i:1565855551;s:2:\"ip\";s:14:\"112.196.31.138\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36\";s:5:\"login\";i:1565682751;}s:64:\"14ec2aba07345641e1466599a769d5b113051573d3a7d4b68f8381f3e05f0d5b\";a:4:{s:10:\"expiration\";i:1565858065;s:2:\"ip\";s:14:\"112.196.31.138\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36\";s:5:\"login\";i:1565685265;}s:64:\"dcde5693de46ca5594747de81df1981caa0bed82dcee4c8d2941b7612e97e268\";a:4:{s:10:\"expiration\";i:1565867475;s:2:\"ip\";s:13:\"106.78.57.165\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36\";s:5:\"login\";i:1565694675;}s:64:\"7df8bb9078f8d00a962695ff1b73d551afac99bad9ea65906aee843f495faa6b\";a:4:{s:10:\"expiration\";i:1566904275;s:2:\"ip\";s:13:\"106.78.57.165\";s:2:\"ua\";s:121:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36\";s:5:\"login\";i:1565694675;}}'),
(415, 21, 'nickname', 'amrish'),
(416, 21, 'first_name', ''),
(417, 21, 'last_name', ''),
(418, 21, 'description', ''),
(419, 21, 'rich_editing', 'true'),
(420, 21, 'syntax_highlighting', 'true'),
(421, 21, 'comment_shortcuts', 'false'),
(422, 21, 'admin_color', 'fresh'),
(423, 21, 'use_ssl', '0'),
(424, 21, 'show_admin_bar_front', 'true'),
(425, 21, 'locale', ''),
(426, 21, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(427, 21, 'wp_user_level', '0'),
(428, 21, 'dismissed_wp_pointers', ''),
(429, 21, 'rm_user_status', '0'),
(430, 21, 'RM_UMETA_FORM_ID', '4'),
(431, 21, 'RM_UMETA_SUB_ID', '21'),
(432, 21, 'session_tokens', 'a:3:{s:64:\"5f675a2f6c735ac07b659cd78d952ef1dccbc85ee7580425483cd47cc0778410\";a:4:{s:10:\"expiration\";i:1573798158;s:2:\"ip\";s:13:\"192.168.2.254\";s:2:\"ua\";s:104:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1572588558;}s:64:\"e1485b5a1b1e8d42951c910b6598f72bcc769c79a05f8802af7886e1cbbd9dc6\";a:4:{s:10:\"expiration\";i:1573297509;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:104:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1573124709;}s:64:\"46bef9a46dd08dea6522c3ab988dea151acfb9e952c4bf91c1b2cd80e5ad816e\";a:4:{s:10:\"expiration\";i:1574334309;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:104:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1573124709;}}'),
(433, 15, 'wp_user-settings', 'unfold=1&mfold=o'),
(434, 15, 'wp_user-settings-time', '1570620708'),
(435, 22, 'nickname', 'amy'),
(436, 22, 'first_name', ''),
(437, 22, 'last_name', ''),
(438, 22, 'description', ''),
(439, 22, 'rich_editing', 'true'),
(440, 22, 'syntax_highlighting', 'true'),
(441, 22, 'comment_shortcuts', 'false'),
(442, 22, 'admin_color', 'fresh'),
(443, 22, 'use_ssl', '0'),
(444, 22, 'show_admin_bar_front', 'true'),
(445, 22, 'locale', ''),
(446, 22, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(447, 22, 'wp_user_level', '0'),
(448, 22, 'dismissed_wp_pointers', ''),
(449, 22, 'rm_user_status', '0'),
(450, 22, 'RM_UMETA_FORM_ID', '4'),
(451, 22, 'RM_UMETA_SUB_ID', '22'),
(452, 22, 'session_tokens', 'a:9:{s:64:\"10ff1c3cabfc239410777635b526a5063871b5f2913a532525c35fe6cad95edb\";a:4:{s:10:\"expiration\";i:1572000217;s:2:\"ip\";s:14:\"112.196.31.138\";s:2:\"ua\";s:104:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1571827417;}s:64:\"c9abe0a173fbac9bca60450fb4b8c40e80483951b0372b031a18245748a49587\";a:4:{s:10:\"expiration\";i:1573037017;s:2:\"ip\";s:14:\"112.196.31.138\";s:2:\"ua\";s:104:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1571827417;}s:64:\"e0da6b92503b0f2d9308dbc3ee7b97f383043af73414b624d0167b556bcade67\";a:4:{s:10:\"expiration\";i:1572073025;s:2:\"ip\";s:14:\"112.196.31.138\";s:2:\"ua\";s:104:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1571900225;}s:64:\"1c920bfad03da4e8c7cf33357312692caf9868e5e7d1fd3c8c85b42309082a3c\";a:4:{s:10:\"expiration\";i:1573109825;s:2:\"ip\";s:14:\"112.196.31.138\";s:2:\"ua\";s:104:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1571900225;}s:64:\"5ac7248720d0e2fc0534cb28d828a1a05a1c891dda86483cf7a135b43556f740\";a:4:{s:10:\"expiration\";i:1572090056;s:2:\"ip\";s:13:\"192.168.2.254\";s:2:\"ua\";s:104:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1571917256;}s:64:\"42a2a1eec384fa19ab9ce6fe32a6a75af1f82d0b88b4a2187b0c208e9a50e25f\";a:4:{s:10:\"expiration\";i:1573126856;s:2:\"ip\";s:13:\"192.168.2.254\";s:2:\"ua\";s:104:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1571917256;}s:64:\"de7caf4ab80b15a6e80e725e9453aa2efb961b638f2e49dcf12700979df3dc9c\";a:4:{s:10:\"expiration\";i:1572091223;s:2:\"ip\";s:13:\"192.168.2.254\";s:2:\"ua\";s:104:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1571918423;}s:64:\"cf73ed2ac2fb6ab8cfad20f54409edf1b68344ccc1ecc11211c001ff5a019fa7\";a:4:{s:10:\"expiration\";i:1572092042;s:2:\"ip\";s:13:\"192.168.2.254\";s:2:\"ua\";s:104:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1571919242;}s:64:\"7cca527b219d1b7a4e0f10d1c2a466d20e775e1b72ffdfd8b6847b36e7cd38d5\";a:4:{s:10:\"expiration\";i:1573128842;s:2:\"ip\";s:13:\"192.168.2.254\";s:2:\"ua\";s:104:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1571919242;}}'),
(396, 20, 'nickname', 'hungenbach'),
(397, 20, 'first_name', ''),
(398, 20, 'last_name', ''),
(399, 20, 'description', ''),
(400, 20, 'rich_editing', 'true'),
(401, 20, 'syntax_highlighting', 'true'),
(402, 20, 'comment_shortcuts', 'false'),
(403, 20, 'admin_color', 'fresh'),
(404, 20, 'use_ssl', '0'),
(405, 20, 'show_admin_bar_front', 'true'),
(406, 20, 'locale', ''),
(407, 20, 'wp_capabilities', 'a:1:{s:10:\"subscriber\";b:1;}'),
(408, 20, 'wp_user_level', '0'),
(409, 20, 'dismissed_wp_pointers', ''),
(410, 20, 'rm_user_status', '0'),
(411, 20, 'RM_UMETA_FORM_ID', '4'),
(412, 20, 'RM_UMETA_SUB_ID', '20'),
(413, 20, 'session_tokens', 'a:2:{s:64:\"54b33cfbfff397965bef9bd6d64e3eef38fa31897a19efa1c97fdf3c20de0b7e\";a:4:{s:10:\"expiration\";i:1567567417;s:2:\"ip\";s:12:\"46.88.76.102\";s:2:\"ua\";s:128:\"Mozilla/5.0 (iPad; CPU OS 12_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1\";s:5:\"login\";i:1567394617;}s:64:\"390b4090f565e245c6b9914edde3d2f74a31bd016f0342715d40335d0c5d2eb2\";a:4:{s:10:\"expiration\";i:1568604218;s:2:\"ip\";s:12:\"46.88.76.102\";s:2:\"ua\";s:128:\"Mozilla/5.0 (iPad; CPU OS 12_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1\";s:5:\"login\";i:1567394618;}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'markus@hungenbach.de', '$P$BOX3St.38hx91Q7HZVZ0Gzkk1iUosQ/', 'markushungenbach-de', 'markus@hungenbach.de', '', '2019-01-06 04:09:02', '', 0, 'markus@hungenbach.de'),
(3, 'Test2', '$P$BYdgEtbz.8tSvNtBvRLUq4T7zZ9YjQ1', 'test2', 'hungenbach@web.de', '', '2019-01-12 11:13:07', '', 0, 'Test2'),
(4, 'markus.hu@gmx.de', '$P$BuPc6FtKAAcs/uLhaNfhjfrsfm1HVE0', 'markus-hugmx-de', 'markus.hu@gmx.de', '', '2019-03-16 03:36:15', '', 0, 'markus.hu@gmx.de'),
(5, 'umesh', '$P$BWFJqIxfsd8EINWTSiTSsmz8y5uWBD0', 'umesh', 'umesh.wordpressexpert@gmail.com', '', '2019-03-22 10:50:24', '', 0, 'umesh'),
(6, 'KZZ', '$P$BjcdLVXl1r2jCv974Xz5YBYfsnqSkl1', 'kzz', 'bule_house@yahoo.com', '', '2019-03-31 07:11:48', '', 0, 'KZZ'),
(7, 'shahidabd', '$P$BAc/WJvaIwrMab3zd2zZSb5VN9ryyX/', 'shahidabd', 'shahid.ranjha@gmail.com', '', '2019-03-31 07:32:27', '', 0, 'shahidabd'),
(8, 'cselbach', '$P$BCZHB/LhfYLwFIeQTqODFZCNJfGvf1.', 'cselbach', 'christoph.selbach@gmx.de', '', '2019-04-29 05:13:21', '', 0, 'cselbach'),
(9, 'test', '$P$ByiNk9W3y3NwN1AhzHIWUOTWQORJfD1', 'test', 'test@gmail.com', '', '2019-06-21 04:02:59', '', 0, 'test'),
(10, 'test111', '$P$BnDLd5csj0zoQ2SNJeqFPitqcGHxOT1', 'test111', 'giefsl@outlook.com', '', '2019-06-24 16:23:27', '', 0, 'test111'),
(11, 'AlexBregman', '$P$Btghs87kFqSL2EURiVSJZ65YC/yS0p1', 'alexbregman', 'webdev1888@outlook.com', '', '2019-06-29 04:40:43', '', 0, 'AlexBregman'),
(12, 'testing', '$P$BJ/bzm37lkPqCtfcBcv.SrASnUVTcJ.', 'testing', 'ipanimationworld@gmail.com', '', '2019-07-02 07:44:36', '', 0, 'testing'),
(13, 'tt', '$P$BOC0buPqerTiA55Gxgb82QIUMzX26t0', 'tt', 'tt@mailinator.com', '', '2019-07-10 07:09:49', '', 0, 'tt'),
(14, 'pramod', '$P$BYAvYUTTFTBoaEat8/qkvPHvaq0PNY1', 'pramod', 'pramodnp.pnp@gmail.com', '', '2019-07-12 09:43:31', '', 0, 'pramod'),
(15, 'ChicMic', '$P$BZBl29.BOxuCq06bvM/rmXOvvWykAb/', 'chicmic', 'mgulati@chicmic.in', '', '2019-07-17 05:41:51', '', 0, 'ChicMic'),
(16, 'julian', '$P$BaoNTHv3hE9iIGX8efHQhUxtXUvzxE/', 'julian', 'julian@yopmail.com', '', '2019-07-22 04:21:53', '', 0, 'julian'),
(17, 'mohammad', '$P$B1BoRrRmzUjOG8S4hj70aNqs3nYR9q0', 'mohammad', 'mohammadyasirchicmic59@gmail.com', '', '2019-08-02 04:28:37', '1566191432:$P$BJDpRNfvmlSv0Btjc5kge0OnOvai8c.', 0, 'mohammad'),
(18, 'shiv', '$P$BRsCHHuPAWg3tsfOM0kot08dmtKosc.', 'shiv', 'shiv@mailinator.com', '', '2019-08-06 06:49:17', '1568954788:$P$B4j3hC.gANrB0Q8hiYHMDex5aytYg5.', 0, 'shiv'),
(19, 'SS', '$P$Bti/7AQ0p/DYTkDBxQQq.kxIwprSFa.', 'ss', 'sss@mailinator.com', '', '2019-08-13 06:05:31', '', 0, 'SS'),
(20, 'hungenbach', '$P$BLPEvaK2rDePUw5Aw.iGMGHUJFAneG.', 'hungenbach', 'webmaster@hungenbach.de', '', '2019-09-02 03:23:10', '', 0, 'hungenbach'),
(21, 'amrish', '$P$BuEA34UskXg50rqSvV1oEZgmzS7A34/', 'amrish', 'amrish.kumar@chicmic.co.in', '', '2019-10-03 09:24:53', '', 0, 'amrish'),
(22, 'amy', '$P$BMW3F1aD7DGbE453psMClYTm5CBlji.', 'amy', 'dhiraj@chicmic.co.in', '', '2019-10-23 10:43:19', '', 0, 'amy');

-- --------------------------------------------------------

--
-- Table structure for table `wp_user_post_models`
--

CREATE TABLE `wp_user_post_models` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_user_post_models`
--

INSERT INTO `wp_user_post_models` (`id`, `post_id`, `type`) VALUES
(11, 301, 'house-small'),
(10, 299, 'bus'),
(9, 298, 'bus'),
(6, 295, 'car'),
(7, 296, 'house-small'),
(8, 297, 'house-big'),
(76, 380, 'house-small'),
(77, 381, 'house-big'),
(78, 382, 'bus'),
(79, 383, 'car'),
(81, 385, 'house-big'),
(87, 391, 'car'),
(177, 492, 'bus'),
(100, 411, 'car'),
(33, 333, 'house-big'),
(297, 619, 'bus'),
(35, 335, 'car'),
(36, 336, 'bus'),
(37, 337, 'house-big'),
(178, 493, 'bus'),
(102, 413, 'car'),
(46, 347, 'bus'),
(47, 348, 'bus'),
(228, 549, 'bus'),
(101, 412, 'car'),
(50, 352, 'bus'),
(75, 379, 'car'),
(72, 376, 'bus'),
(73, 377, 'house-big'),
(74, 378, 'bus'),
(229, 550, 'car'),
(188, 503, 'car'),
(215, 534, 'car'),
(176, 489, 'car'),
(190, 505, 'car'),
(191, 506, 'car'),
(192, 507, 'car'),
(193, 508, 'car'),
(230, 551, 'bus'),
(257, 578, 'bus'),
(258, 580, 'bus'),
(259, 581, 'bus'),
(260, 582, 'bus'),
(261, 583, 'bus'),
(262, 584, 'bus'),
(263, 585, 'bus'),
(246, 567, 'car'),
(247, 568, 'car'),
(265, 587, 'bus'),
(264, 586, 'bus'),
(252, 573, 'car'),
(253, 574, 'car'),
(266, 588, 'bus'),
(268, 590, 'bus'),
(269, 591, 'bus'),
(298, 620, 'bus'),
(322, 653, 'bus'),
(294, 616, 'bus'),
(327, 658, 'bus'),
(330, 666, 'car'),
(343, 681, 'house-small'),
(344, 682, 'house-big'),
(345, 683, 'car'),
(346, 684, 'house-big'),
(347, 685, 'house-small'),
(353, 691, 'house-small'),
(352, 690, 'house-small'),
(351, 689, 'house-small');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wpfm_backup`
--

CREATE TABLE `wp_wpfm_backup` (
  `id` int(11) NOT NULL,
  `backup_name` text COLLATE utf8mb4_unicode_520_ci,
  `backup_date` text COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wpgmza`
--

CREATE TABLE `wp_wpgmza` (
  `id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL,
  `address` varchar(700) NOT NULL,
  `description` mediumtext NOT NULL,
  `pic` varchar(700) NOT NULL,
  `link` varchar(700) NOT NULL,
  `icon` varchar(700) NOT NULL,
  `lat` varchar(100) NOT NULL,
  `lng` varchar(100) NOT NULL,
  `anim` varchar(3) NOT NULL,
  `title` varchar(700) NOT NULL,
  `infoopen` varchar(3) NOT NULL,
  `category` varchar(500) NOT NULL,
  `approved` tinyint(1) DEFAULT '1',
  `retina` tinyint(1) DEFAULT '0',
  `type` tinyint(1) DEFAULT '0',
  `did` varchar(500) NOT NULL,
  `other_data` longtext NOT NULL,
  `latlng` point DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wpgmza_categories`
--

CREATE TABLE `wp_wpgmza_categories` (
  `id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `category_icon` varchar(700) NOT NULL,
  `retina` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wpgmza_category_maps`
--

CREATE TABLE `wp_wpgmza_category_maps` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wpgmza_circles`
--

CREATE TABLE `wp_wpgmza_circles` (
  `id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL,
  `name` text,
  `center` point DEFAULT NULL,
  `radius` float DEFAULT NULL,
  `color` varchar(16) DEFAULT NULL,
  `opacity` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wpgmza_maps`
--

CREATE TABLE `wp_wpgmza_maps` (
  `id` int(11) NOT NULL,
  `map_title` varchar(55) NOT NULL,
  `map_width` varchar(6) NOT NULL,
  `map_height` varchar(6) NOT NULL,
  `map_start_lat` varchar(700) NOT NULL,
  `map_start_lng` varchar(700) NOT NULL,
  `map_start_location` varchar(700) NOT NULL,
  `map_start_zoom` int(10) NOT NULL,
  `default_marker` varchar(700) NOT NULL,
  `type` int(10) NOT NULL,
  `alignment` int(10) NOT NULL,
  `directions_enabled` int(10) NOT NULL,
  `styling_enabled` int(10) NOT NULL,
  `styling_json` mediumtext NOT NULL,
  `active` int(1) NOT NULL,
  `kml` varchar(700) NOT NULL,
  `bicycle` int(10) NOT NULL,
  `traffic` int(10) NOT NULL,
  `dbox` int(10) NOT NULL,
  `dbox_width` varchar(10) NOT NULL,
  `listmarkers` int(10) NOT NULL,
  `listmarkers_advanced` int(10) NOT NULL,
  `filterbycat` tinyint(1) NOT NULL,
  `ugm_enabled` int(10) NOT NULL,
  `ugm_category_enabled` tinyint(1) NOT NULL,
  `fusion` varchar(100) NOT NULL,
  `map_width_type` varchar(3) NOT NULL,
  `map_height_type` varchar(3) NOT NULL,
  `mass_marker_support` int(10) NOT NULL,
  `ugm_access` int(10) NOT NULL,
  `order_markers_by` int(10) NOT NULL,
  `order_markers_choice` int(10) NOT NULL,
  `show_user_location` int(3) NOT NULL,
  `default_to` varchar(700) NOT NULL,
  `other_settings` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_wpgmza_maps`
--

INSERT INTO `wp_wpgmza_maps` (`id`, `map_title`, `map_width`, `map_height`, `map_start_lat`, `map_start_lng`, `map_start_location`, `map_start_zoom`, `default_marker`, `type`, `alignment`, `directions_enabled`, `styling_enabled`, `styling_json`, `active`, `kml`, `bicycle`, `traffic`, `dbox`, `dbox_width`, `listmarkers`, `listmarkers_advanced`, `filterbycat`, `ugm_enabled`, `ugm_category_enabled`, `fusion`, `map_width_type`, `map_height_type`, `mass_marker_support`, `ugm_access`, `order_markers_by`, `order_markers_choice`, `show_user_location`, `default_to`, `other_settings`) VALUES
(1, 'Picture Town Map', '100', '800', '52.514370', '13.350330', '52.51437,13.350329999999985', 18, '0', 2, 1, 1, 0, '', 0, '', 2, 2, 1, '100', 0, 0, 0, 0, 0, '', '\\%', 'px', 1, 0, 1, 2, 0, '', 'a:14:{s:21:\"store_locator_enabled\";i:2;s:22:\"store_locator_distance\";i:2;s:28:\"store_locator_default_radius\";s:2:\"10\";s:31:\"store_locator_not_found_message\";s:52:\"No results found in this location. Please try again.\";s:20:\"store_locator_bounce\";i:1;s:26:\"store_locator_query_string\";s:14:\"ZIP / Address:\";s:29:\"store_locator_default_address\";s:0:\"\";s:29:\"wpgmza_store_locator_restrict\";s:0:\"\";s:19:\"store_locator_style\";s:6:\"modern\";s:33:\"wpgmza_store_locator_radius_style\";s:6:\"modern\";s:12:\"map_max_zoom\";s:1:\"1\";s:15:\"transport_layer\";i:2;s:17:\"wpgmza_theme_data\";s:0:\"\";s:30:\"wpgmza_show_points_of_interest\";i:1;}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wpgmza_polygon`
--

CREATE TABLE `wp_wpgmza_polygon` (
  `id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL,
  `polydata` longtext NOT NULL,
  `innerpolydata` longtext NOT NULL,
  `linecolor` varchar(7) NOT NULL,
  `lineopacity` varchar(7) NOT NULL,
  `fillcolor` varchar(7) NOT NULL,
  `opacity` varchar(3) NOT NULL,
  `title` varchar(250) NOT NULL,
  `link` varchar(700) NOT NULL,
  `ohfillcolor` varchar(7) NOT NULL,
  `ohlinecolor` varchar(7) NOT NULL,
  `ohopacity` varchar(3) NOT NULL,
  `polyname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wpgmza_polylines`
--

CREATE TABLE `wp_wpgmza_polylines` (
  `id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL,
  `polydata` longtext NOT NULL,
  `linecolor` varchar(7) NOT NULL,
  `linethickness` varchar(3) NOT NULL,
  `opacity` varchar(3) NOT NULL,
  `polyname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wpgmza_rectangles`
--

CREATE TABLE `wp_wpgmza_rectangles` (
  `id` int(11) NOT NULL,
  `map_id` int(11) NOT NULL,
  `name` text,
  `cornerA` point DEFAULT NULL,
  `cornerB` point DEFAULT NULL,
  `color` varchar(16) DEFAULT NULL,
  `opacity` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_rm_fields`
--
ALTER TABLE `wp_rm_fields`
  ADD PRIMARY KEY (`field_id`);

--
-- Indexes for table `wp_rm_forms`
--
ALTER TABLE `wp_rm_forms`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `wp_rm_front_users`
--
ALTER TABLE `wp_rm_front_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_rm_login`
--
ALTER TABLE `wp_rm_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_rm_login_log`
--
ALTER TABLE `wp_rm_login_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_rm_notes`
--
ALTER TABLE `wp_rm_notes`
  ADD PRIMARY KEY (`note_id`);

--
-- Indexes for table `wp_rm_paypal_fields`
--
ALTER TABLE `wp_rm_paypal_fields`
  ADD PRIMARY KEY (`field_id`);

--
-- Indexes for table `wp_rm_paypal_logs`
--
ALTER TABLE `wp_rm_paypal_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_rm_rules`
--
ALTER TABLE `wp_rm_rules`
  ADD PRIMARY KEY (`rule_id`);

--
-- Indexes for table `wp_rm_sent_mails`
--
ALTER TABLE `wp_rm_sent_mails`
  ADD PRIMARY KEY (`mail_id`);

--
-- Indexes for table `wp_rm_sessions`
--
ALTER TABLE `wp_rm_sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_rm_stats`
--
ALTER TABLE `wp_rm_stats`
  ADD PRIMARY KEY (`stat_id`);

--
-- Indexes for table `wp_rm_submissions`
--
ALTER TABLE `wp_rm_submissions`
  ADD PRIMARY KEY (`submission_id`);

--
-- Indexes for table `wp_rm_submission_fields`
--
ALTER TABLE `wp_rm_submission_fields`
  ADD PRIMARY KEY (`sub_field_id`);

--
-- Indexes for table `wp_rm_tasks`
--
ALTER TABLE `wp_rm_tasks`
  ADD PRIMARY KEY (`task_id`);

--
-- Indexes for table `wp_rm_task_exe_log`
--
ALTER TABLE `wp_rm_task_exe_log`
  ADD PRIMARY KEY (`texe_log_id`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_user_post_models`
--
ALTER TABLE `wp_user_post_models`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_wpfm_backup`
--
ALTER TABLE `wp_wpfm_backup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_wpgmza`
--
ALTER TABLE `wp_wpgmza`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_wpgmza_categories`
--
ALTER TABLE `wp_wpgmza_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_wpgmza_category_maps`
--
ALTER TABLE `wp_wpgmza_category_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_wpgmza_circles`
--
ALTER TABLE `wp_wpgmza_circles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_wpgmza_maps`
--
ALTER TABLE `wp_wpgmza_maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_wpgmza_polygon`
--
ALTER TABLE `wp_wpgmza_polygon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_wpgmza_polylines`
--
ALTER TABLE `wp_wpgmza_polylines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_wpgmza_rectangles`
--
ALTER TABLE `wp_wpgmza_rectangles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9698;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1529;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=692;

--
-- AUTO_INCREMENT for table `wp_rm_fields`
--
ALTER TABLE `wp_rm_fields`
  MODIFY `field_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `wp_rm_forms`
--
ALTER TABLE `wp_rm_forms`
  MODIFY `form_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `wp_rm_front_users`
--
ALTER TABLE `wp_rm_front_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_rm_login`
--
ALTER TABLE `wp_rm_login`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `wp_rm_login_log`
--
ALTER TABLE `wp_rm_login_log`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;

--
-- AUTO_INCREMENT for table `wp_rm_notes`
--
ALTER TABLE `wp_rm_notes`
  MODIFY `note_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_rm_paypal_fields`
--
ALTER TABLE `wp_rm_paypal_fields`
  MODIFY `field_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_rm_paypal_logs`
--
ALTER TABLE `wp_rm_paypal_logs`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_rm_rules`
--
ALTER TABLE `wp_rm_rules`
  MODIFY `rule_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_rm_sent_mails`
--
ALTER TABLE `wp_rm_sent_mails`
  MODIFY `mail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `wp_rm_stats`
--
ALTER TABLE `wp_rm_stats`
  MODIFY `stat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `wp_rm_submissions`
--
ALTER TABLE `wp_rm_submissions`
  MODIFY `submission_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `wp_rm_submission_fields`
--
ALTER TABLE `wp_rm_submission_fields`
  MODIFY `sub_field_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `wp_rm_tasks`
--
ALTER TABLE `wp_rm_tasks`
  MODIFY `task_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_rm_task_exe_log`
--
ALTER TABLE `wp_rm_task_exe_log`
  MODIFY `texe_log_id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=453;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `wp_user_post_models`
--
ALTER TABLE `wp_user_post_models`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=354;

--
-- AUTO_INCREMENT for table `wp_wpfm_backup`
--
ALTER TABLE `wp_wpfm_backup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wpgmza`
--
ALTER TABLE `wp_wpgmza`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wpgmza_categories`
--
ALTER TABLE `wp_wpgmza_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wpgmza_category_maps`
--
ALTER TABLE `wp_wpgmza_category_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wpgmza_circles`
--
ALTER TABLE `wp_wpgmza_circles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wpgmza_maps`
--
ALTER TABLE `wp_wpgmza_maps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_wpgmza_polygon`
--
ALTER TABLE `wp_wpgmza_polygon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wpgmza_polylines`
--
ALTER TABLE `wp_wpgmza_polylines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wpgmza_rectangles`
--
ALTER TABLE `wp_wpgmza_rectangles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
