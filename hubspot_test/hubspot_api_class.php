<?php
class Hubspot_api
{
	    
    private $hapikey;

	function __construct($hapikey)
	{
        $this->hapikey = $hapikey;

    }




	function create_contact($arr)
	{

		$hapikey = $this->hapikey;		
				
		$post_json = json_encode($arr);

	    $endpoint = 'https://api.hubapi.com/contacts/v1/contact?hapikey=' . $hapikey;  // for create a contact

	    $ch = @curl_init();
	    @curl_setopt($ch, CURLOPT_POST, true);
	    @curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
	    @curl_setopt($ch, CURLOPT_URL, $endpoint);
	    @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	    @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    $response = @curl_exec($ch);
	    $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    $curl_errors = curl_error($ch);
	    @curl_close($ch);
	    //echo "curl Errors: " . $curl_errors;
	   // echo "\nStatus code: " . $status_code;
	   // echo "\nResponse: " . $response;

	    if ($curl_errors) 
	    {
		  return "curl Errors: " . $curl_errors;exit;
		} else 
		{
		  return "\nStatus code: " . $status_code."\nResponse: " . $response;
		}


	}






	function update_contact($arr,$vid)
	{

		$hapikey = $this->hapikey;		
				
		$post_json = json_encode($arr);

	    $endpoint = 'https://api.hubapi.com/contacts/v1/contact/vid/'.$vid.'/profile?hapikey=' . $hapikey;  // for update exist contact
	    //echo $endpoint;die;
	    $ch = @curl_init();
	    @curl_setopt($ch, CURLOPT_POST, true);
	    @curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
	    @curl_setopt($ch, CURLOPT_URL, $endpoint);
	    @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	    @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    $response = @curl_exec($ch);
	    $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    $curl_errors = curl_error($ch);
	    @curl_close($ch);
	    //echo "curl Errors: " . $curl_errors;
	    //echo "\nStatus code: " . $status_code;
	    //echo "\nResponse: " . $response;

	    if ($curl_errors) 
	    {
		  return "curl Errors: " . $curl_errors;exit;
		} else 
		{
		  return "\nStatus code: " . $status_code."\nResponse: " . $response;
		}

	}



	function check_exist_contact_by_email($email)
	{

		$hapikey = $this->hapikey;		
				
		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://api.hubapi.com/contacts/v1/contact/email/'.$email.'/profile?hapikey=' . $hapikey,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		
		));

        $response = curl_exec($curl);
        $response = json_decode($response);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		echo "cURL Error #:" . $err;die('==error==');
		} else {
        //echo "<pre>";print_r($response);
       // echo $response->vid;
        if( isset($response->vid) )
        {
			//echo "yes  exist";
			return true;
        }
        else
        {
			//echo "Not  exist";
			return false;
        }
		//echo $response->is-contact;die('==Response==');
		}


	}


}

?>