<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.hubapi.com/properties/v1/contacts/properties?hapikey=bd6b842c-fea7-4a3f-a98d-542462332778",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\n  \"name\": \"software_update_emails\",\n  \"label\": \"Software Update Emails\",\n  \"description\": \"\",\n  \"groupName\": \"contactinformation\",\n  \"type\": \"string\",\n  \"fieldType\": \"text\",\n  \"formField\": true,\n  \"displayOrder\": 6,\n  \"options\": [\n    \n  ]\n}",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/json",
    "postman-token: 1e89a329-ba48-b6d7-df00-a3073cd11eb9"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}

?>