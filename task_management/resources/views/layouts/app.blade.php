<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Task Management') }}</title>

    <script type="text/javascript">
        var baseURL = {!! json_encode(url('/')) !!}
    </script>

    <script type="text/javascript" src="{{ url('public/js/jquery-3.3.1.js') }}"></script>
    <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <!-- Touch devices -->
    <script type="text/javascript" src="{{ url('public/js/jquery.ui.touch-punch.js') }}"></script>

    <script type="text/javascript" src="{{ url('public/js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('public/js/bootstrap.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('js/jquery-3.3.1.slim.min.js') }}" ></script> -->    
    <script type="text/javascript" src="{{ url('public/js/jquery.validate.js') }}"></script>
    
    <script type="text/javascript" src="{{ url('public/js/custom.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- Gantt chart -->
    <script src="{{ url('public/js/im-gantt.js') }}"></script>

    <!-- include summernote -->
    <link rel="stylesheet" href="{{ url('public/css/summernote-bs4.css') }}">
    <script type="text/javascript" src="{{ url('public/js/summernote-bs4.js') }}"></script>

    <!-- Drag drop folder -->
    <script type="text/javascript" src="{{ url('public/js/responder.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ url('public/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Gantt chart -->
    <link href="{{ url('public/css/im-gantt.css') }}" rel="stylesheet">

    <link href="{{ url('public/css/style.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app"> <span class="loader" style="display:block;"></span>
        <!-- <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#"></a>
            </nav> -->
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel navbar-taskmanagement">
            <div class="container">
                @guest
                    <a class="navbar-brand" href="{{ url('/login') }}">
                        {{ config('app.name', 'Task Management') }}
                    </a>
                @else
                    <a class="navbar-brand" href="{{ url('/tasks') }}">
                        {{ config('app.name', 'Task Management') }}
                    </a>
                @endguest

                @guest

                @else
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
                @endguest
                

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <!-- <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li> -->
                        @auth
                        @if(Auth::check() && Auth::user()->role == "admin")
                        <li class="nav-item">
                        <a class="nav-link" href="{{route('users')}}">Users</a>
                        </li>
                        <!-- <li class="nav-item">
                        <a class="nav-link" href="{{route('folders')}}">Manage folders</a>
                        </li> -->
                        @endif
                        <li class="nav-item">
                        <a class="nav-link" href="{{route('tasks')}}">Tasks</a>
                        </li>

                        <li class="nav-item">
                        <a class="nav-link" href="<?php echo url('/summary/excel'); ?>">Export</a>
                        
                        </li>


                        @endauth
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <!-- <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li> -->
                            <!-- @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif -->
                        @else

                            

                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>

                            
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>



        <main class="py-4 app-main-content">
            @yield('content')
        </main>
    </div>
</body>
</html>
