@extends('layouts.app')
@section('content')
<div class="container">
    <div class="middle-arg">
        @include('flash-message')
        @yield('content')
        <div class="">
            <div class="heading-sec">
                <div class="col-sm-12">
                    <h1>Testing file transfer</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="">
				<input type="file" name="file_upload" onchange="file_upload(this);" />
			</div>
		</div>
       
</div>
</div>

<script>
	$(document).ready(function(){		
	});
	
	function file_upload(el) {
		if(el.files.length > 0) {
			formdata = new FormData();
            formdata.append("file", el.files[0]); 
			
			$.ajax({
                type: 'POST',
                url: 'http://tasks.silentbeacon.com/public/test_file_receive',
                data: formdata,
                processData: false,
                contentType: false,
                beforeSend: function() {
					console.log('Sending..');
                },
                success: function(data) {
                    console.log(data);
                },
                error: function(jqXHR, exception) {
                    alert('<b>ERROR:</b> '+jqXHR.status+': '+jqXHR.responseText+': '+exception);
                }
            });
		}
	}
</script>
@endsection


