<!DOCTYPE html>
<html>
<head>
    <title>Welcome on Task Management</title>
</head>

<body>
<p>Dear <b> {{$name}} </b>,<br/></p>

<p>{{$assignee}} wants you to work on task <b><u> <a href="{{route('task_detail', ['id' => $task_id ])}}"> {{$task_name}} </a><u></b> {{ ( strtotime($due_date) > 0 ) ? 'to be completed by '. date('m/d/Y', strtotime($due_date)):'' }}.</p>
<br/>
Thanks

</body>

</html>