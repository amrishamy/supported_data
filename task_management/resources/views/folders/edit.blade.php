@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Folder</div>                
                <div class="card-body">
                   <form method="POST" action="<?php echo url('/folderupdate'); ?>" >
                   {{ csrf_field() }}
                   <input name="id" type="hidden" class="form-control" id="inputName" placeholder="Name" value="{{@$folder->id}}">
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Name</label>
                      <div class="col-sm-10">
                        <input name="name" type="text" class="form-control" id="inputName" placeholder="Folder Name" value="{{@$folder->name}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <a class="btn btn-primary" href="{{URL::route('folders')}}" role="button">Cancel</a>
                      </div>
                    </div>
                  </form>

                    
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
