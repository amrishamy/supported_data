@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Folder{{$folder->name}}</div>                
                <div class="card-body">
                   
                <a class="btn btn-success" href="" role="button">Add a task</a>

                <form method="POST" class="add_task_form" action="<?php echo url('/folderupdate'); ?>" >
                   {{ csrf_field() }}
                   <input name="id" type="hidden" class="form-control" id="inputName" value="{{@$folder->id}}">
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Assigned to</label>
                      <div class="col-sm-10">
                        <input name="assigned_to" type="text" class="form-control" id="inputName" placeholder="Assigned to" value="" >
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Due date</label>
                      <div class="col-sm-10">
                        <input name="due_date" type="text" class="form-control" id="inputName" placeholder="Due Date" value="">
                      </div>
                    </div>

                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Notes</label>
                      <div class="col-sm-10">
                        <input name="notes" type="text" class="form-control" id="inputName" placeholder="Notes" value="">
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Add this task</button>
                        <a class="btn btn-primary" href="{{URL::route('folders')}}" role="button">Cancel</a>
                      </div>
                    </div>
                  </form>

                    
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
