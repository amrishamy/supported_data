@extends('layouts.app')

@section('content')


<div class="container">
            <div class="middle-arg">
                <div class="options-details">
                    <div class="col-sm-12">
                        <div class="options-secD">
                            <div class="optionsSec-header">
                                <h3>
                                    <a class="taskHeading" href="#">{{$folder->name}}</a>
                                    <!-- <div class="todo-progress todo-progress--todolist" data-behavior="progress-chart" data-progress-chart-id="1706115594">
                                        <span class="todo-progressDot"></span>
                                        <span class="todo-progress__ratio">
                                            <a title="View all completed to-dos…" href="/3142486/buckets/11615450/todolists/1706115594#completed_todos" draggable="false">
                                            0/2 completed</a>    
                                        </span>
                                    </div> -->
                                </h3>
                            </div>
                            <ul class="todos remaining pending_tasks">
                            @isset($tasks)
                            @foreach ($tasks as $task) 
                            @if($task->status== 0 )
                                <li class="todo">
                                    <div class="indent">
                                        <div class="checkbox">
                                            <label  style="font-size: 1em" class="checkbox__label">
                                                <input type="checkbox" data-taskid="{{$task->id}}" folder-id="{{$task->folder}}" name="todo_complete" value="1" class="checkbox__input" data-behavior="todo_completion" data-move-completed="true" data-url="/3142486/buckets/11615450/todos/1706117094/completion?replace=false">
                                                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                            </label>
                                            <span class="checkbox__content unassigned">
                                                <a href="" draggable="false">{{$task->name}}</a>
                                            </span>
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @endforeach
                            @endisset

                                
                            </ul>
                            <div class="todolist-actionsBtn">
                                <button name="button" type="submit" class="btn btn--small btn--primary-on-android">Add a task</button>
                                <div class="todo-addDescribe">
                                    <form method="POST" class="add_task_frm">
                                    {{ csrf_field() }}
                                    <input name="logged_user_id" class="logged_user_id" type="hidden" class="form-control" value="{{ Auth::user()->id }}">
                                    <input name="folder_id" class="folder_id" type="hidden" class="form-control" value="{{$folder->id}}">
                                        <header class="todos-form__header">
                                            <div class="checkbox todos-form__checkbox">
                                                <!-- <label class="checkbox__label">
                                                  <span class="checkbox__button"></span>
                                                </label> -->
                                                <span class="checkbox__content">
                                                <input type="text" name="name" class="todos-form__title input input--full-width input--borderless input--unpadded" placeholder="Describe this to-do…" autofocus="autofocus">
                                                </span>
                                            </div>
                                        </header>
                                        <section class="todos-form__details">
                                            <div class="todos-form__field">
                                                <div class="form-group row">
                                                    <label class="col-sm-2">Assigned to</label>
                                                    <div class="col-sm-10">
                                                    <select name="assigned_to" class="form-control form-control-sm assigned_to">
                                                    <option>Select user</option>
                                                    @isset($user_list)
                                                    @foreach($user_list as $row)
                                                    @if($row->role != 'admin')
                                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                                    @endif
                                                    @endforeach
                                                    @endisset
                                                    </select>
                                                      <!-- <input name="assigned_to" type="text" class="form-control-plaintext assigned_to" id="staticassign" value="" placeholder="Type names to assign…"> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="todos-form__field">
                                                <div class="form-group row">
                                                    <label class="col-sm-2">Due on</label>
                                                    <div class="col-sm-10">
                                                      <input name="due_date" type="text" class="datepicker form-control-plaintext due_date " id="" value="" placeholder="Select a date…">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="todos-form__field">
                                                <div class="form-group row">
                                                    <label class="col-sm-2">Notes</label>
                                                    <div class="col-sm-10">
                                                    <textarea name="notes" class="form-control-plaintext" id="staticassign" value="" placeholder="Add extra details or attach a file…"></textarea>
                                                      <!-- <input name="notes" type="text" class="form-control-plaintext notes" id="staticassign" value="" placeholder="Add extra details or attach a file…"> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="submit push_half--ends">  
                                                <input type="submit" value="Add this task" class="btn btn--small btn--primary add_task_btn">
                                                <button type="reset" class="btn btn--small btn--secondary addDescribe-cancel reset_btn">Cancel</button>
                                            </div>
                                        </section>
                                    </form>
                                </div>   
                            </div>
                            <ul>
                            @isset($tasks)
                            @foreach ($tasks as $task) 
                            @if($task->status== 1 )
                                <li class="todo">
                                    <div class="indent">
                                        <div class="checkbox">
                                            <label  style="font-size: 1em" class="checkbox__label">
                                                <input type="checkbox" data-taskid="{{$task->id}}" folder-id="{{$task->folder}}" name="todo_complete" value="1" class="checkbox__input" data-behavior="todo_completion" data-move-completed="true" data-url="/3142486/buckets/11615450/todos/1706117094/completion?replace=false">
                                                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                            </label>
                                            <span class="checkbox__content unassigned">
                                                <a href="" draggable="false">{{$task->name}}</a>
                                            </span>
                                        </div>
                                    </div>
                                </li>
                            @endif
                            @endforeach
                            @endisset
                            </ul>                         
                            <div class="indent indent-checked">
                                <div class="checkbox">
                                    <label class="checkbox__label">
                                        <input type="checkbox" name="todo_complete" value="1" class="checkbox__input checked" checked="checked">
                                        <span class="checkbox__button checked"></span>
                                    </label>
                                    <span class="checkbox__content">
                                        <a href="#">task1</a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
 

@endsection
