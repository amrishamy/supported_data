@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><span class="card-user">Folders</span>
                 <div class="create-task">
                        <a class="btn btn-primary" href="foldercreate" role="button">Add Folder</a>
                    </div>
                </div>

                
                <div class="card-body">
                    <table class="table">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">Folder ID</th>
                          <th scope="col">Name</th>
                          <th scope="col">created by</th>
                          <th scope="col">Created Date</th>
                          
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                       
                        @isset($folder_list)
                        @foreach($folder_list as $row)
                <tr>
                    <td>{{$row->id}}</td>
                    <td>{{$row->name}}</td>
                    <td>{{$row->created_b}}</td>
                    <td>{{$row->created_at}}</td>
                    
                    <td>
                            <a href="folderedit/{{$row->id}}" class="edit" ><i class="fas fa-edit"></i></a>
                            <a onclick="return confirm('Are you sure?')" href="folderdelete/{{$row->id}}" class="delete" ><i class="far fa-trash-alt"></i></a>
                    </td>
                </tr>
                @endforeach
                @endisset
                      </tbody>
                    </table>

                    
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
