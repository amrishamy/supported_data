@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Folder Creation</div>

                

                
                <div class="card-body">
                   <form method="POST" action="<?php echo url('/foldercreate'); ?>" >
                   {{ csrf_field() }}
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Folder Name</label>
                      <div class="col-sm-10">
                        <input name="name" type="text" class="form-control" id="inputName" placeholder="Folder Name">
                      </div>
                    </div>
                    <input name="logged_user_id" type="hidden" class="form-control" value="{{ Auth::user()->id }}">
                    
                    
                    <div class="form-group row">
                      <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Create</button>

                        <a class="btn btn-primary" href="folders" role="button">Cancel</a>
                      </div>
                    </div>
                  </form>

                    
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
