@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit User</div>                
                <div class="card-body">
                   <form method="POST" action="<?php echo url('/update'); ?>" >
                   {{ csrf_field() }}
                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Name</label>
                      <div class="col-sm-10">
                        <input name="name" type="text" class="form-control" id="inputName" placeholder="Name" value="{{@$user->name}}">
                      </div>
                    </div>

                    


                    <div class="form-group row">
                      <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                      <div class="col-sm-10">
                        <input name="email" type="email" class="form-control" id="inputEmail3" placeholder="Email" value="{{@$user->email}}">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
                      <div class="col-sm-10">
                        <input name="password" type="password" class="form-control" id="inputPassword3" placeholder="Password">
                      </div>
                    </div>
                    
                    
                    <div class="form-group row">
                      <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <a class="btn btn-primary" href="{{URL::route('users')}}" role="button">Cancel</a>
                      </div>
                    </div>
                  </form>

                    
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
