@extends('layouts.app')

@section('content')

<div class="container">
            <div class="middle-arg">
                <div class="heading-sec">
                    <h1>Users on the task</h1>
                    <label class="web-header-new">
                        <!-- <span class="btn btn-small btn-primary btn--add-icon-reversed todo-addPeople">Add User...</span> -->
                        <a class="btn btn-small btn-primary btn--add-icon-reversed todo-addPeople" href="create" role="button">Add User...</a>
                    </label>
                </div>

                <div class="col-sm-12">
                    <div class="todo-newPeople">
                    <ul class="nav nav-tabs">
                            <li class="nav-item float-left">
                              <a class="nav-link active" data-toggle="tab" href="#home">Active User</a>
                            </li>
                            <li class="nav-item float-right">
                              <a class="nav-link" data-toggle="tab" href="#menu1">Padding User</a>
                            </li>
                            
                          </ul>
                          <div class="tab-content">
                            <div id="home" class="container tab-pane active"><br>
                              <h3>Active User</h3>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                            <div id="menu1" class="container tab-pane fade"><br>
                              <h3>Padding User</h3>
                              <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>

                            </div>
                        <form method="POST" action="<?php echo url('/userInvitation'); ?>" class="add_user_frm">
                            {{ csrf_field() }}
                            
                            <div class="newPeople-form">
                            
                                <div class="form-group">
                                    <input name="name" type="text" class="form-control-plaintext user_name" id="exampleFormControl1" placeholder="Name">
                                    <input name="logged_user_id" type="hidden" class="form-control" value="{{ Auth::user()->id }}">
                                </div>
                                 <div class="form-group">
                                    <input name="email" type="email" class="form-control-plaintext" id="exampleFormControl2" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input name="password" type="password" class="form-control-plaintext" id="exampleFormControl2" placeholder="Password">
                                </div>
                                <div class="submit push_half--ends">  
                                    <input type="submit" value="Add and Invite user" class="btn btn--small btn--primary add_this_user">
                                    <button type="reset" class="btn btn--small btn--secondary people-cancel">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                
                <div class="user-editSec">
                    <div class="row">
                @isset($user_list)
                    @foreach($user_list as $row)
                    <div class="col-sm-6 col-md-6">
                        <div class="thumbnail" id="client-people">
                            <div class="people-header">
                                <!-- <h3 class="flush--ends">
                                  testClient
                                  <span class="client-visibility-flag">The Client</span>
                                </h3> -->
                                <div class="row">
                                    <div class="col-md-12 col-lg-2">
                                        <div class="comment-nameHead">
                                            
                                           {{ ucfirst( Str::limit($row->name, 1,'') ) }}
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-10">
                                        <div class="">
                                            <span class="thread-comment-author">
                                                <strong>{{$row->name}}</strong>
                                            </span>
                                            <span class="formatted_content">
                                               title optional at testClient 
                                            </span>
                                            <span class="formatted_content">
                                                {{$row->email}} 
                                            </span>
                                            <ul class="formatted_content_edit">
                                                <li>
                                                    <a href="">Edit</a>
                                                </li>
                                                <li>
                                                <a onclick="return confirm('Are you sure?')" href="delete/{{$row->id}}" class="delete" ><i class="far fa-trash-alt"></i></a>
                                                </li>
                                                <li>
                                                    <a href="">Send a link to log in</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endisset
                    </div>
                </div>
            </div>
        </div>


@endsection
