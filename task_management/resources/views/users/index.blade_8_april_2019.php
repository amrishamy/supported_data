@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><span class="card-user">Users</span>
                 <div class="create-task">
                        <a class="btn btn-primary" href="create" role="button">Invite User</a>
                    </div>
                </div>

                
                <div class="card-body">
                    <table class="table">
                      <thead class="thead-dark">
                        <tr>
                          <th scope="col">User ID</th>
                          <th scope="col">Name</th>
                          <th scope="col">Email</th>
                          <th scope="col">Created Date</th>
                          <th scope="col">Status</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        
                        <!-- <tr>
                          <th scope="row">3</th>
                          <td>Larry</td>
                          <td>the Bird</td>
                          <td>@twitter</td>
                           <td>
                            <a href="edit" class="edit" ><i class="fas fa-edit"></i></a>
                            <a href="delete" class="delete" ><i class="far fa-trash-alt"></i></a>
                        </td>
                        </tr> -->
                        @isset($user_list)
                        @foreach($user_list as $row)
                <tr>
                    <td>{{$row->id}}</td>
                    <td>{{$row->name}}</td>
                    <td>{{$row->email}}</td>
                    <td>{{$row->created_at}}</td>
                    <td>Pending</td>
                    <td>
                            <!-- <a href="edit/{{$row->id}}" class="edit" ><i class="fas fa-edit"></i></a> -->
                            <a onclick="return confirm('Are you sure?')" href="delete/{{$row->id}}" class="delete" ><i class="far fa-trash-alt"></i></a>
                    </td>
                </tr>
                @endforeach
                @endisset
                      </tbody>
                    </table>

                    
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
