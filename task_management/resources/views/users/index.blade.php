@extends('layouts.app')
@section('content')
<div class="container">
    <div class="middle-arg">
        @include('flash-message')
        @yield('content')
        <div class="">
            <div class="heading-sec">
                <div class="col-sm-12">
                    <h1>Users</h1>

                    <label class="web-header-new">
                        <!-- <span class="btn btn-small btn-primary btn--add-icon-reversed todo-addPeople">Add User...</span> -->
                        <!-- <a class="btn btn-small btn-primary btn--add-icon-reversed todo-addPeople" href="create" >Add User</a> -->
                        <span class="btn btn-small btn-primary btn--add-icon-reversed todo-addPeople">Invite User</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="todo-newPeople">
                <form method="POST" action="<?php echo url('/userInvitation'); ?>" class="add_user_frm" id="add_user_frm">
                    {{ csrf_field() }}
                    <div class="newPeople-form">
                        <div class="form-group">
                            <input name="name" required type="text" class="form-control-plaintext user_name" id="exampleFormControl1" value="{{ old('name') }}" placeholder="Name">
                            <input name="logged_user_id" type="hidden" class="form-control"  value="{{ Auth::user()->id }}">
                        </div>
                        <div class="form-group">
                            <input name="email" required type="email" class="form-control-plaintext" id="exampleFormControl2" placeholder="Email" value="{{ old('email') }}">
                        </div>
                        <div class="form-group">
                            <input name="password" minlength="6" maxlength="20" required type="password" class="form-control-plaintext" id="exampleFormControl3" placeholder="Password" value="{{ old('password') }}">
                        </div>
                        <div class="submit push_half--ends">  
                            <input type="submit" value="Add and Invite user" class="btn btn--small btn--primary add_this_user">
                            <button type="reset" class="btn btn--small btn--secondary people-cancel">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="">
        <div class="user-tab">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#active_users">Active Users</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#pending_users">Pending Users</a>
                </li>
            </ul>
        </div>
            <div class="clearfix"></div>
        <div class="tab-content">
            
            <div id="active_users" class="container tab-pane active">
                <br>
                <div class="row">
                <div class="user-editSec">
                    <div class="">
                        <div class="row">
                        @isset($user_list)
                        @foreach($user_list as $row)
                        @if($row->status=="Active")
                        <div class="col-sm-6 col-md-6 col-lg-3">
                            <div class="thumbnail" id="client-people">
                                <div class="people-header">
                                    <!-- <h3 class="flush--ends">
                                        testClient
                                        <span class="client-visibility-flag">The Client</span>
                                        </h3> -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="comment-nameHead text-center">
                                                {{ ucfirst( Str::limit($row->name, 1,'') ) }}
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="text-center">
                                                <span class="thread-comment-author">
                                                <strong>{{$row->name}}</strong>
                                                </span>
                                                <!-- <span class="formatted_content">
                                                    title optional at testClient 
                                                    </span> -->
                                                <span class="formatted_content">
                                                {{$row->email}} 
                                                </span>
                                                <ul class="formatted_content_edit">
                                                    <!-- <li>
                                                        <a href="">Edit</a>
                                                        </li> -->
                                                    <!-- <li>
                                                        <a onclick="return confirm('Are you sure?')" href="delete/{{$row->id}}" class="delete" >
                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                    </li> -->
                                                    <li>
                                                    <a href="reset_password/{{$row->id}}">Reset Password</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                        @endisset
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div id="pending_users" class="container tab-pane fade">
                <br>
                <div class="row">
                <div class="user-editSec">
                    <div class="row">
                        @isset($user_list)
                        @foreach($user_list as $row)
                        @if($row->status=="Pending")
                        <div class="col-sm-6 col-md-6 col-lg-3">
                            <div class="thumbnail" id="client-people">
                                <div class="people-header">
                                    <!-- <h3 class="flush--ends">
                                        testClient
                                        <span class="client-visibility-flag">The Client</span>
                                        </h3> -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="comment-nameHead">
                                                {{ ucfirst( Str::limit($row->name, 1,'') ) }}
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="text-center">
                                                <span class="thread-comment-author">
                                                <strong>{{$row->name}}</strong>
                                                </span>
                                                <!-- <span class="formatted_content">
                                                    title optional at testClient 
                                                    </span> -->
                                                <span class="formatted_content">
                                                {{$row->email}} 
                                                </span>
                                                <ul class="formatted_content_edit">
                                                    <!-- <li>
                                                        <a href="">Edit</a>
                                                        </li> -->
                                                    <!-- <li>
                                                        <a onclick="return confirm('Are you sure?')" href="delete/{{$row->id}}" class="delete" >
                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                        
                                                        </li> -->
                                                    <li>
                                                        <a href="send_link_to_pending_user/{{$row->id}}">Send login link  </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                        @endisset
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


@endsection


