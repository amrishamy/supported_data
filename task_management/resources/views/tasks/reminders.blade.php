<div class="todos-form__field">
    <div class="form-group row">
        <label class="col-3">Reminders At</label>
        <div class="col-9 reminders_hours">
            @php

            $reminders_arr = array() ;   
            if ($tsk->reminders != "")
            $reminders_arr = explode(",", $tsk->reminders );
            
            @endphp

            <div class="checkbox">
                <label><input type="checkbox" name="reminders[]" {{ ( in_array( '48', $reminders_arr ) ) ? 'checked' :'' }} value="48" class="mr-2">48 Hours</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox" name="reminders[]" {{ ( in_array( '24', $reminders_arr ) ) ? 'checked' :'' }} value="24" class="mr-2">24 Hours</label>
            </div>
            <div class="checkbox disabled">
                <label><input type="checkbox" name="reminders[]" {{ ( in_array( '12', $reminders_arr ) ) ? 'checked' :'' }} value="12" class="mr-2">12 Hours</label>
            </div>
            <div class="checkbox disabled">
                <label><input type="checkbox" name="reminders[]" {{ ( in_array( '1', $reminders_arr ) ) ? 'checked' :'' }} value="1" class="mr-2">1 Hour</label>
            </div>


        </div>
    </div>
</div>