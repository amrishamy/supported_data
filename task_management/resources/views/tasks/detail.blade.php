@extends('layouts.app')
@section('content')
<style>
.alerts-danger{
    color: red;
}
</style>


<div class="container">
    <div class="middle-arg">
        <div class="options-details">
            <div class="col-sm-12 single_task_section">
                <div class="options-secD">
                    <div class="task_info">                        
                        <div class="edit_task_section edit_task_section_details" >
                            <form class="task_disply_form_only">
                                <fieldset  disabled="disabled" >
                                <div class="row">
                                    <div class="col-2">
                                        <a class="icon-style back_button" href="{{url('tasks')}}"> <i class="fa fa-angle-left" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="col-8">
                                        <h1 class="display-4 text-center">{{$task->name}}</h1>
                                    </div> 
                                    @if( Auth::user()->id == $task->created_by  || Auth::user()->role == 'admin' )                               
                                    <div class="col-2">
                                        <a href="#" class="icon-style edit_task_icon pull-right"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    @endif
                                </div>

                                <div class="todos-form__field">
                                    <div class="form-group row">
                                        <label class="col-12 col-form-label text-center ">Made by <span class="font-italic font-weight-bold ">{{$task->createdBy->name }}</span> for <span class="font-italic font-weight-bold ">{{ ( @$task->taskAssignedToFirst->usersa ) ? @$task->taskAssignedToFirst->usersa->name: '____' }} </span> </label>
                                        
                                    </div>
                                </div>

                                @if(Auth::user()->id == $task->createdBy->id )
                                <div class="todos-form__field">
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right font-weight-bold">Currently assigned to</label>
                                        <div class="col-9">
                                            <label class="currently_assign_label"> {{  @$task->taskAssignedToLast->usersa->name  }} </label>
                                        </div>
                                    </div>
                                </div>
                                @endif

                                <!-- <div class="todos-form__field">
                                    <div class="form-group row">
                                        <label class="col-3 col-form-label text-right font-weight-bold">Created By</label>
                                        <div class=" col-9">
                                        <input name="created_by" readonly="readonly" type="text" class="form-control-plaintext" value="{{$task->createdBy->name }}" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="assigned_to" class="col-3 col-form-label text-right font-weight-bold">Assigned to</label>
                                    <div class="col-9">
                                    <input type="text" readonly class="form-control-plaintext" id="assigned_to" value=" {{ ($task->users) ? $task->users->name: '' }}">
                                    </div>
                                </div> -->

                                <div class="form-group row">
                                    <label for="due_date" class="col-3 col-form-label text-right font-weight-bold">Due on</label>
                                    <div class="col-9">
                                    <input type="text" readonly class="form-control-plaintext" id="due_date" value="{{ (strtotime($task->due_date) > 0 ) ? date('M d Y', strtotime($task->due_date)): 'No Due Date' }}">
                                    </div>
                                </div>

                                
                                <div class="form-group row">
                                    <label class="col-3 col-form-label text-right font-weight-bold">Requires Approval for completion </label>
                                    <div class="col-9 reminders_hours">
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="admin_approval" value="1" class="mr-2" @if(Auth::user()->role != "admin") disabled @endif {{ $task->admin_approval ? 'checked':'' }} >  </label>
                                        </div>
                                        
                                    </div>
                                </div>
                                

                                <!-- @include('tasks.reminders', ['tsk' => $task ] ) -->

                                <div class="form-group row">
                                    <label for="notes" class="col-3 col-form-label text-right font-weight-bold">Notes</label>
                                    <div class="col-9">
                                    {!!html_entity_decode($task->notes)!!}
                                    
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="notes" class="col-3 col-form-label text-right font-weight-bold">Uploaded file</label>
                                    <div class="col-9">
                                    
                                    @if($task->uploads)                                    
                                    <a class="attachment_task btn btn--primary" href="{{ url('public/uploads').'/'. $task->uploads }}"  download="{{$task->uploads}}" > <i class="fa fa-paperclip" aria-hidden="true"></i> {{ $task->uploads }} </a>
                                    @else
                                    {{ 'No file uploaded' }}
                                    @endif
                                    
                                    </div>
                                </div>
                                </fieldset>
                                <div class="form-group row">
                                    <div class="col-12 text-center">
                                        <!-- <button class="btn btn--small btn--secondary mark-folder">Mark Complete</button> -->
                                        @if( (Auth::user()->role == 'admin') || ($task->created_by == Auth::user()->id) || Auth::user()->id == @$task->assigned_to_who->assigned_by )
                                            @if( $task->admin_approval && Auth::user()->role != 'admin' )

                                            @else
                                                <a role="button" data-taskid="{{$task->id}}" folder-id="{{$task->folder}}" check-type="{{ (@$task->assigned_to_who->status ? "checked":"un-checked") }}" href="#" class="btn btn--small btn--secondary mark-folder update_status_single_task"> {{ (@$task->assigned_to_who->status ? "Reopen":"Mark Done") }} </a>
                                            @endif
                                        @endif
                                        
                                    </div>
                                </div>

                            </form>
                            <div class="edit_task_frm_single_div" style="display:none;">
                                <form method="POST" class="edit_task_frm_single" enctype="multipart/form-data" >
                                    <fieldset @if( !(Auth::user()->id == $task->created_by || Auth::user()->role == 'admin') ) disabled="disabled" @endif >
                                    <div class="alerts alerts-danger" style="display:none"></div>
                                        {{ csrf_field() }}
                                        <input type="hidden" name="form_type" value="edit_task_single">
                                        <input name="logged_user_id" class="logged_user_id" type="hidden" class="form-control" value="{{ Auth::user()->id }}">
                                        <input name="folder_id" class="folder_id" type="hidden" class="form-control" value="{{$task->folder}}">
                                        <input name="task_id" class="task_id" type="hidden" class="form-control" value="{{$task->id}}">
                                        <header class="todos-form__header">
                                            <div class="checkbox todos-form__checkbox">                                            
                                                <span class="checkbox__content">
                                                <input type="text" name="task_name" class="todos-form__title input input--full-width input--borderless input--unpadded" placeholder="Describe this to-do…" autofocus="autofocus" value="{{$task->name}}" required>
                                                </span>
                                            </div>
                                        </header>
                                        <section class="todos-form__details">

                                            <div class="todos-form__field">
                                                <div class="form-group row">
                                                    <label class="col-12 col-form-label text-center ">Made by <span class="font-italic font-weight-bold ">{{$task->createdBy->name }}</span> for <span class="font-italic font-weight-bold select-assign">

                                                    @if(Auth::user()->id == @$task->taskAssignedToFirst->assigned_to  )

                                                        {{  @$task->taskAssignedToFirst->usersa->name  }}

                                                    @else
                                                        <select name="assigned_to" class="form-control form-control-sm assigned_to" >
                                                        <option value="">Select user</option>
                                                        @isset($user_list)
                                                        @foreach($user_list as $row)
                                                        @if($row->role != 'admin')
                                                            <option value="{{$row->id}}" {{ (@$task->taskAssignedToFirst->assigned_to == $row->id ? "selected":"") }} > {{$row->name}} {{ ($row->status == "Pending" ? "( Pending)" : "") }}</option>
                                                        @endif
                                                        @endforeach
                                                        @endisset
                                                        </select>
                                                    @endif 

                                                    </span> </label>
                                                    
                                                </div>
                                            </div>

                                            @if(Auth::user()->id == $task->createdBy->id )
                                            <div class="todos-form__field">
                                                <div class="form-group row">
                                                    <label class="col-3 col-form-label text-right font-weight-bold">Currently assigned to</label>
                                                    <div class="col-9">
                                                        <label class="currently_assign_label"> {{  @$task->taskAssignedToLast->usersa->name  }} </label>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif

                                            
                                            <div class="todos-form__field">
                                                <div class="form-group row">
                                                    <label class="col-3 col-form-label text-right font-weight-bold">Due on</label>
                                                    <div class="col-9">
                                                    <input name="due_date"  type="text" class="datepicker form-control-plaintext due_date" value="{{ ( strtotime($task->due_date) > 0 ) ? date('M d Y', strtotime($task->due_date)):'' }}" placeholder="" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-3 col-form-label text-right font-weight-bold">Requires Approval for completion </label>
                                                <div class="col-9 reminders_hours">
                                                    <div class="checkbox">
                                                        <label><input type="checkbox" name="admin_approval" value="1" class="mr-2" @if(Auth::user()->role != "admin") disabled @endif {{ $task->admin_approval ? 'checked':'' }} >  </label>
                                                    </div>
                                                    
                                                </div>
                                            </div>

                                            <!-- @include('tasks.reminders', ['tsk' => $task ] ) -->
                                            
                                            <div class="todos-form__field">
                                                <div class="form-group row">
                                                    <label class="col-3 col-form-label text-right font-weight-bold">Notes</label>
                                                    <div class="col-9">
                                                        <!-- <textarea name="notes" class="form-control-plaintext" id="staticassign"  placeholder="Add extra details or attach a file…">{{$task->notes}}</textarea> -->
                                                        <textarea name="notes" class="form-control-plaintext summernote"  placeholder="blah blah blah">{{$task->notes}}</textarea>                                        
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="todos-form__field">
                                                <div class="form-group row">
                                                    <label class="col-3 col-form-label text-right font-weight-bold">Upload File</label>
                                                    <div class="col-9">
                                                    <input name="file"  type="file" class=" form-control-plaintext file" placeholder="" />

                                                    <input type="hidden" class="up_file" name="up_file" value="" />

                                                    <span> {{$task->uploads}} </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- <div class="todos-form__field">
                                                <div class="form-group">
                                                    <div class="checkbox">
                                                        <label class="checkbox-details"><input type="checkbox" name="notify_user" class="form-control-plaintext notify_user" value="1" {{ $task->notify ? 'checked':'' }} >Notify user by Email</label>
                                                    </div>
                                                </div>
                                            </div> -->

                                    </fieldset>        

                                            <div class="submit push_half--ends">  
                                                <?php if( Auth::user()->id == $task->created_by  || Auth::user()->role == 'admin' )
                                                { 
                                                ?>
                                                    <input type="submit" value="Save" class="btn btn--small btn--primary edit_task_btn_single">
                                                    

                                                    
                                                    <a class="delete_task_single btn btn--secondary" role="button" folder-id="{{$task->folder}}" data-taskid="{{$task->id}}" href="{{url('delete_task_single')}}"> Delete</a>
                                                <?php
                                                } ?>

                                                <button type="reset" class="btn btn--small btn--secondary edit-task-single-cancel reset_btn">Discard</button>

                                                    

                                            </div>
                                        </section>
                                </form>
                            </div>    
                        </div>

                    </div>                       
                </div>
            </div>

            <div class="col-12 comment_section">
                <div class="todo-comment">
                    <h2 class="comment-headline">
                        <span>
                            <span class="comments-balloon">
                            
                            {{ $task_comments->count() }}
                            
                            </span>
                            <span class="comment-title">
                                Comments
                            </span>
                        </span>
                    </h2>
                    <div class="thread-comment">
                    @foreach ($task_comments as $comment )
                        <div class="row comment_display_section">
                            <!-- <div class="col-sm-2 col-md-2 col-lg-1">
                                <div class="comment-nameHead comment-nameHeadcomment">
                                    {{ ucfirst( Str::limit($comment->users->name, 1,'') ) }}
                                </div>
                            </div> -->
                            <div class="col-12">
                                <div class="comment-sec">
                                    <span class="thread-comment-author">
                                        <strong>{{ $comment->users->name }}</strong>
                                    </span>
                                    <span class="thread-comment-options">
                                        <span class="thread-comment-time">
                                            @if(date('Y') == date('Y',strtotime($comment->created_at) ) )
                                            {{ date('M d H:i',strtotime($comment->created_at->timezone( session('user_time_zone') )->toDateTimeString() ) ) }}
                                            @else
                                            {{date('M d Y H:i',strtotime($comment->created_at->timezone( session('user_time_zone') )->toDateTimeString()  ) ) }}
                                            @endif
                                        </span>
                                        <div class="dropdown">
                                            <!-- <a class="btn btn-secondary comment-btnsmall dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">…</a> -->
                                            <!-- <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> -->
                                                <?php  if($comment->users->id == Auth::user()->id  )  {  ?>
                                                <a class="dropdown-item edit_comment" href="#"><i class="fa fa-edit"></i></a>
                                                <a class="dropdown-item delete_comment" comment_id="{{ $comment->id }}" task_id="{{ $task->id }}" href="{{url('delete_comment')}}"><i class="fa fa-remove"></i></a>
                                                <?php } ?>
                                            <!-- </div> -->
                                        </div>
                                    </span>
                                    <span class="formatted_content">
                                       {{ $comment->comment }}
                                    </span>
                                </div>
                            </div>
                        </div>

                        <!-- edit form of comment will show when click on edit button  -->

                        <div class="row edit_comment_form_section" style="display:none;">
                            <!-- <div class="col-sm-2 col-md-2 col-lg-1">
                                <div class="comment-nameHead comment-nameHeadcomment">
                                        {{ ucfirst( Str::limit(Auth::user()->name, 1,'') ) }}
                                </div>
                            </div> -->
                            <div class="col-12">
                                <div class="comment-sec-last">
                                    <div class="comment-sec">
                                        
                                        <div class="edit-form-commetBox">
                                            <form class="edit_comment_form" method="post" action="{{route ('update_comment')}}">
                                            <div class="alerts alerts-danger" style="display:none"></div>
                                            {{ csrf_field() }}
                                            <input type="hidden" name="form_type" value="task_comment_edit">
                                            <input name="comment_id" class="comment_id" type="hidden" class="form-control" value="{{ $comment->id }}">
                                            <input name="logged_user_id" class="logged_user_id" type="hidden" class="form-control" value="{{ Auth::user()->id }}">
                                            <input name="folder_id" class="folder_id" type="hidden" class="form-control" value="{{$task->folder}}">
                                            <input name="task_id" class="task_id" type="hidden" class="form-control" value="{{$task->id}}">
                                                <div class="form-group">
                                                    <textarea class="form-control" name="comment" id="comment" rows="3"> {{ $comment->comment }} </textarea>
                                                </div>
                                                <!-- <div class="form-group">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" class="comment_notify" name="comment_notify" value="1" {{ $comment->notify ? 'checked':'' }}/> Notify user by Email
                                                        </label>
                                                    </div>                                                    
                                                </div> -->
                                                <div class="submit push_half--ends">
                                                    <input type="submit" value="Save" class="btn btn--small btn--primary edit_comment_btn">
                                                    <button type="reset" class="btn btn--small btn--secondary edit-comment-cancel reset_btn">Discard</button>

                                                    
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  end of comment edit form -->

                    @endforeach
                        <div class="row">
                            <!-- <div class="col-sm-2 col-md-2 col-lg-1">
                                <div class="comment-nameHead comment-nameHeadcomment">
                                        {{ ucfirst( Str::limit(Auth::user()->name, 1,'') ) }}
                                </div>
                            </div> -->
                            <div class="col-12">
                                <div class="comment-sec-last">
                                    <div class="comment-sec">
                                        <button type="button" class="prompt-commentbtn">Add a comment…</button>
                                        <div class="form-commetBox">
                                            <form class="add_comment_form" method="post" action="{{route ('save_comment')}}" id="add_comment_form">
                                            <div class="alerts alerts-danger" style="display:none"></div>
                                            {{ csrf_field() }}
                                            <input type="hidden" name="form_type" value="task_comment">
                                            <input name="logged_user_id" class="logged_user_id" type="hidden" class="form-control" value="{{ Auth::user()->id }}">
                                            <input name="folder_id" class="folder_id" type="hidden" class="form-control" value="{{$task->folder}}">
                                            <input name="task_id" class="task_id" type="hidden" class="form-control" value="{{$task->id}}">
                                                <div class="form-group">
                                                    <textarea class="form-control" name="comment" class="comment" rows="3" required id="comment"></textarea>
                                                </div>
                                                <!-- <div class="form-group">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" class="comment_notify" name="comment_notify" value="1" /> Notify user by Email
                                                        </label>
                                                    </div>                                                    
                                                </div> -->
                                                <div class="submit push_half--ends">
                                                    <a href="{{route ('save_comment')}}" class="btn btn--small btn--primary add_comment_btn">Add this comment</a>
                                                    <button type="reset" class="btn btn--small btn--secondary comment-cancel">Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div>
</div>
 
<script>
 $(document).ready(function(){
    $('.datepicker').datepicker({
    minDate:new Date()
    });

   /*  $(document).ready(function(){
        setInterval(function(){
            if( !($('textarea[name=comment]').is(":focus")) )
            {
                window.location.reload();
            }
           
        },10000);
    }) */
    
    /*$('.summernote').summernote({
        toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'underline', 'clear']],
        ['font', [] ],
        ['fontsize', [] ],
        ['color', [] ],
        ['para', [] ],
        ['insert', ['link','picture'] ],
        ['height', [] ]
        ],
        height: 100,
        });*/

        $('.summernote').summernote({
                placeholder: true,
                placeholder: "blah blah blah",
                height: 100,
                airMode: true,
                popover: {
                air: [
                // [groupName, [list of button]]
                ['style', ['bold', 'underline', 'clear']],
                ['font', [] ],
                ['fontsize', [] ],
                ['color', [] ],
                ['para', [] ],
                ['insert', ['link','picture'] ],
                ['height', [] ]
                ]
                }

                });

    });
        </script>
@endsection
