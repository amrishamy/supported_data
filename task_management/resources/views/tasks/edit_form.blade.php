<div class="edit_task_section" style="display:none;">
    <form method="POST" class="edit_task_frm_list" enctype="multipart/form-data">
    <fieldset @if( !(Auth::user()->id == $tsk->created_by || Auth::user()->role == 'admin') ) disabled="disabled" @endif >
        <div class="alerts alerts-danger" style="display:none"></div>
        {{ csrf_field() }}
        <input type="hidden" name="form_type" value="edit_task">
        <input name="logged_user_id" class="logged_user_id" type="hidden" class="form-control" value="{{ Auth::user()->id }}">
        <input name="folder_id" class="folder_id" type="hidden" class="form-control" value="{{$f_w_t->id}}">
        <input name="task_id" class="task_id" type="hidden" class="form-control" value="{{$tsk->id}}">
        <header class="todos-form__header">
            <div class="checkbox todos-form__checkbox">

                <span class="checkbox__content">
                <input type="text" name="task_name" class="todos-form__title input input--full-width input--borderless input--unpadded" placeholder="Describe this to-do…" autofocus="autofocus" value="{{$tsk->name}}" required>
                </span>
            </div>
        </header>
        <section class="todos-form__details">

            <div class="todos-form__field">
                <div class="form-group row">
                    <label class="col-12 col-form-label text-center ">Made by <span class="font-italic font-weight-bold ">{{$tsk->createdBy->name }}</span> for <span class="font-italic font-weight-bold select-assign">
                    <?php //echo "<pre>";@print_r( @$tsk->taskAssignedToFirst->assigned_to ); 
                       // echo "<pre>";@print_r( @$tsk->taskchain ); 
                    ?>
                    @if(Auth::user()->id == @$tsk->taskAssignedToFirst->assigned_to  )

                        {{  @$tsk->taskAssignedToFirst->usersa->name  }}

                    @else
                        <select name="assigned_to" class="form-control form-control-sm assigned_to" >
                        <option value="">Select user</option>
                        @isset($user_list)
                        @foreach($user_list as $row)
                        
                            <option value="{{$row->id}}" {{ ( @$tsk->taskAssignedToFirst->assigned_to == $row->id ? "selected":"") }} > {{$row->name}} {{ ($row->status == "Pending" ? "( Pending)" : "") }}</option>
                        
                        @endforeach
                        @endisset
                        </select>
                    @endif 

                    </span> </label>
                    
                </div>
            </div>

            @if(Auth::user()->id == $tsk->createdBy->id )
            <div class="todos-form__field">
                <div class="form-group row">
                    <label class="col-3">Currently assigned to</label>
                    <div class="col-9">
                        <label class="currently_assign_label"> {{  @$tsk->taskAssignedToLast->usersa->name  }} </label>
                    </div>
                </div>
            </div>
            @endif 

            <!-- <div class="todos-form__field">
                <div class="form-group row">
                    <label class="col-3">Created By</label>
                    <div class="col-9">
                    <input name="created_by" readonly="readonly" type="text" class="form-control-plaintext" value="{{$tsk->createdBy->name }}" />
                    </div>
                </div>
            </div>

            <div class="todos-form__field">
                <div class="form-group row">
                    <label class="col-3">Assigned to</label>
                    <div class="col-9">
                    @if(Auth::user()->id == $tsk->assigned_to  )

                        {{  $tsk->users->name  }}

                    @else
                        <select name="assigned_to" class="form-control form-control-sm assigned_to"   >
                        <option value="">Select user</option>
                        @isset($user_list)
                        @foreach($user_list as $row)
                        @if($row->role != 'admin')
                            <option value="{{$row->id}}" {{ ($tsk->assigned_to == $row->id ? "selected":"") }} > {{$row->name}} {{ ($row->status == "Pending" ? "( Pending)" : "") }}</option>
                        @endif
                        @endforeach
                        @endisset
                        </select>
                    @endif
                    
                    </div>
                </div>
            </div> -->
            <div class="todos-form__field">
                <div class="form-group row">
                    <label class="col-3">Due on</label>
                    <div class="col-9">
                    <input name="due_date"  type="text" class="datepicker form-control-plaintext due_date" value="{{ ( strtotime($tsk->due_date) > 0 ) ? date('M d Y', strtotime($tsk->due_date)): '' }}" placeholder="" />
                    </div>
                </div>
            </div>
            <div class="todos-form__field">
                <div class="form-group row">
                    <label class="col-3">Requires Approval for completion </label>
                    <div class="col-9 reminders_hours">
                        <div class="checkbox">
                            <label><input type="checkbox" name="admin_approval" value="1" class="mr-2" @if(Auth::user()->role != "admin") disabled @endif {{ $tsk->admin_approval ? 'checked':'' }} >  </label>
                        </div>
                        
                    </div>
                </div>
            </div>

            <!-- @include('tasks.reminders') -->

            <div class="todos-form__field">
                <div class="form-group row">
                    <label class="col-3">Notes</label>
                    <div class="col-9">
                    <textarea name="notes" class="form-control-plaintext summernote"  placeholder="blah blah blah">{{$tsk->notes}}</textarea>

                    </div>
                </div>
            </div>

            <div class="todos-form__field">
                <div class="form-group row">
                    <label class="col-3">Upload File</label>
                    <div class="col-9">
                    <input name="file"  type="file" class=" form-control-plaintext file" placeholder="" />

                    <input type="hidden" class="up_file" name="up_file" value="" />

                    <span> {{$tsk->uploads}} </span>
                    </div>
                </div>
            </div>

            <div class="todos-form__field">
                <div class="form-group row">
                    <label class="col-12">
                    <?php /*foreach($tsk->taskchain as $chain){

                        echo $chain->usersa->name."=>";
                    } */?>
                    
                    </label>
                    
                </div>
            </div>

            @if(Auth::user()->id == $tsk->created_by || Auth::user()->role == 'admin' )
            <!-- <div class="todos-form__field">
                <div class="form-group">  
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="notify_user" class="form-control-plaintext notify_user" value="1" {{ $tsk->notify ? 'checked':'' }} >Notify user by Email
                        </label>
                    </div>                                                                            
                </div>
            </div> -->
            @endif
    </fieldset>        
            <div class="submit push_half--ends"> 
                @if(Auth::user()->id == $tsk->created_by || Auth::user()->role == 'admin' || Auth::user()->id == $tsk->assigned_to_who->assigned_by )
                    @if(@$tsk->assigned_to_who->status == 0)
                        @if( Auth::user()->id == $tsk->created_by || Auth::user()->role == 'admin' )
                            <input type="submit" value="Save" class="btn btn-sm btn--small btn--primary edit_task_btn_list">
                        @endif
                        @if( $tsk->admin_approval && Auth::user()->role != 'admin' )

                        @else
                            <input type="submit" data-taskid="{{$tsk->id}}" folder-id="{{$f_w_t->id}}" value="Done" class="btn btn-sm btn--small btn--primary complete_task_btn" >
                        @endif
                    @else
                        <input type="submit" data-taskid="{{$tsk->id}}"  value="Reopen" class="btn btn-sm btn--small btn--primary reopen_task_btn_list" folder-id="{{$tsk->folder}}">
                    @endif
                @endif
                
                <!-- <button type="reset" class="btn btn-sm btn--small btn--secondary edit-task-cancel reset_btn">Discard</button> -->

                @if(Auth::user()->id == $tsk->created_by || Auth::user()->role == 'admin' )
                <a class="delete_task btn btn-sm btn--secondary" role="button" folder-id="{{$f_w_t->id}}" data-taskid="{{$tsk->id}}" href="{{url('delete_task')}}"> Delete</a>
                @endif



            </div>
        </section>
    </form>

    <!-- start form for further task assignment  -->
    <?php 
    DB::enableQueryLog(); 
    $further_assigned_results = (object)[];
    $further_assigned_results = $tsk->further_assigned() ;
    //echo "<pre>";print_r( @$further_assigned_results );
    //$all_assignments = $tsk->assignments->all() ;
    //echo "<pre>hello";print_r( $tsk->assignments );
    $assigned_count = $tsk->is_assigned_to_me();
    
    //echo "<pre>";print_r($tsk->assignments);
    //echo DB::getQueryLog() ;
    if( $assigned_count &&  Auth::user()->id != $tsk->created_by && @$tsk->assigned_to_who->status == 0 )
    {
    ?>
        <form method="POST" class="further_assignment_frm" enctype="multipart/form-data">
            <div class="alerts alerts-danger" style="display:none"></div>
            {{ csrf_field() }}
            
            <input name="logged_user_id" class="logged_user_id" type="hidden" class="form-control" value="{{ Auth::user()->id }}">
            <input name="folder_id" class="folder_id" type="hidden" class="form-control" value="{{$f_w_t->id}}">
            <input name="task_id" class="task_id" type="hidden" class="form-control" value="{{$tsk->id}}">

            <div class="todos-form__field">
                <div class="form-group row">
                    <label class="col-3">Assigned to </label>
                    <div class="col-9">
                    <select name="further_assigned_to" class="form-control form-control-sm further_assigned_to">
                        <option value="">Select user</option>
                        @isset($user_list)
                            @foreach($user_list as $row)
                                
                                    <option value="{{$row->id}}" {{ ( @$tsk->assigned_to_who->assigned_to == $row->id ? "selected":"") }} > {{$row->name}} {{ ($row->status == "Pending" ? "( Pending)" : "") }} </option>
                                
                            @endforeach
                        @endisset
                    </select>
                    </div>
                </div>
            </div>

            <div class="submit push_half--ends"> 
                <input type="submit" value="Assign" class="btn btn-sm btn--small btn--primary further_assign_btn">


            </div>

        </form>
    <!-- end form for further task assignment  -->
    <?php 
    } ?>
</div> 