<div class="optionsSec-header">                                
    <h3>
        <div style="color:{{ $f_w_t->color() }};" class="comment-nameHead text-center folder_color">  
            @if($f_w_t->color() == 'black')
                <i class="fa fa-folder-open-o" aria-hidden="true"></i>
            @else
                <i class="fa fa-folder-open" aria-hidden="true"></i>
            @endif
        </div>

        @if(Auth::user()->role == "admin")
            <a class="taskHeading" href="javascript:void(0);">{{$f_w_t->name}}</a>
            <span class="taskToggle"><i class="fa fa-angle-down"></i></span>
        @else
            <a class="taskHeading" href="javascript:void(0);">{{$f_w_t->name}}</a>
            <span class="taskToggle"><i class="fa fa-angle-down"></i></span>
        @endif  
    </h3>
                                    
    <!-- <div class="col-2 pull-right">
        <div class="iconsDeledit">
            <a class="edit_folder_btn" href="#" folder_id="{{$f_w_t->id}}" ><i class="fa fa-edit"></i></a>
            <a class="delete_folder_btn" href="{{url('delete_folder')}}" folder_id="{{$f_w_t->id}}"><i class="fa fa-remove"></i></a>
        </div>
    </div> -->
                                
    <form method="POST" class="edit_folder_frm" style="display:none;">
        {{ csrf_field() }}
        
        <div class="newList-form">                            
            <div class="form-group">
                <input name="name" required type="text" class="form-control-plaintext folder_name" value="{{$f_w_t->name}}" placeholder="Name this folder">
                <input name="folder_id" type="hidden" class="form-control folder_id" value="{{$f_w_t->id}}">
            </div>
            
            <div class="submit push_half--ends">  
                <input type="submit" value="Save" class="btn btn--small btn--primary edit_this_folder">
                <button type="reset" class="btn btn--small btn--secondary edit-folder-cancel">Cancel</button>
            </div>
        </div>
    </form>
</div>

<div class="folder_info" >
    <ul class="todos remaining ">
        @foreach($all_tasks as $tsk)
            @if( strtotime($tsk->due_date) > 0 )
                @if(@$tsk->assigned_to_who->status== 0 )
                    @include('tasks.list_item')
                @endif
            @endif
        @endforeach

        @foreach($all_tasks as $tsk)
            @if( strtotime($tsk->due_date) <= 0 )
                @if(@$tsk->assigned_to_who->status== 0 )
                    @include('tasks.list_item')
                @endif
            @endif
        @endforeach
    </ul>

    <div class="todolist-actionsBtn">
        <button name="button" type="submit" class="btn btn--small btn--primary-on-android">Add a task</button>
        <div class="todo-addDescribe">
            <form method="POST" class="add_task_frm_list" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="alerts alerts-danger" style="display:none"></div>
            <input type="hidden" name="form_type" value="task_list">
            <input name="logged_user_id" class="logged_user_id" type="hidden" class="form-control" value="{{ Auth::user()->id }}">
            <input name="folder_id" class="folder_id" type="hidden" class="form-control" value="{{$folder}}">
                <header class="todos-form__header">
                    <div class="checkbox todos-form__checkbox">
                        
                        <span class="checkbox__content">
                        <input type="text" name="task_name" class="todos-form__title input input--full-width input--borderless input--unpadded" placeholder="Describe this to-do…" autofocus="autofocus" required >
                        </span>
                    </div>
                </header>
                <section class="todos-form__details">
                    <div class="todos-form__field">
                        <div class="form-group row">
                            <label class="col-3">Assigned to</label>
                            <div class="col-9">
                            <select name="assigned_to" class="form-control form-control-sm assigned_to"  >
                            <option value="">Select user</option>
                            @isset($user_list)
                            @foreach($user_list as $row)
                            
                            <option value="{{$row->id}}">{{$row->name}} {{ ($row->status == "Pending" ? "( Pending)" : "") }} </option>
                            
                            @endforeach
                            @endisset
                            </select>
                            <!-- <input name="assigned_to" type="text" class="form-control-plaintext assigned_to" id="staticassign" value="" placeholder="Type names to assign…"> -->
                            </div>
                        </div>
                    </div>
                    <div class="todos-form__field">
                        <div class="form-group row">
                            <label class="col-3">Due on</label>
                            <div class="col-9">
                            <input name="due_date"  type="text" class="datepicker form-control-plaintext due_date " id="" value="" placeholder="Select a date…">
                            </div>
                        </div>
                    </div>

                    <div class="todos-form__field">
                        <div class="form-group row">
                            <label class="col-3">Requires Approval for completion </label>
                            <div class="col-9 reminders_hours">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="admin_approval" value="1" class="mr-2" @if(Auth::user()->role != "admin") disabled @endif >  </label>
                                </div>
                                
                            </div>
                        </div>
                    </div>

                    <div class="todos-form__field">
                        <div class="form-group row">
                            <label class="col-3">Notes</label>
                            <div class="col-9">
                            <textarea name="notes" class="form-control-plaintext summernote"  placeholder="blah blah blah"></textarea>
                            
                            </div>
                        </div>
                    </div>
                    <!-- <div class="todos-form__field">
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="notify_user" class="form-control-plaintext notify_user" value="1" > Notify user by Email
                                </label>
                            </div>                                                        
                        </div>
                    </div> -->
                    <div class="todos-form__field">
                        <div class="form-group row">
                            <label class="col-3">Upload File</label>
                            <div class="col-9">
                            <input name="file"  type="file" class=" form-control-plaintext file" placeholder="" />

                            <input type="hidden" class="up_file" name="up_file" value="" />
                                
                            </div>
                        </div>
                    </div>
                    <div class="submit push_half--ends">  
                        <input type="submit" value="Add this task" class="btn btn--small btn--primary add_task_btn_list">
                        <button type="reset" class="btn btn--small btn--secondary addDescribe-cancel reset_btn">Cancel</button>
                    </div>
                </section>
            </form>
        </div>   
    </div>
    <div class="task-subtasks">
        <div class="task-subtasks-inner">
            <div style="color:blue ;" class="completed_folder_color completed_task_folder ">  
                <i class="fa fa-folder-open" aria-hidden="true"></i>         
            </div>
            <a class="completed_folder_title" href="#">Completed Tasks</a>   
        </div>
                            
        <ul class="todos remaining completed_task " style="display:none;">
        @foreach($all_tasks as $tsk)
            @if( strtotime($tsk->due_date) > 0 )
                @if(@$tsk->assigned_to_who->status== 1 )
                        @include('tasks.list_item')
                @endif
            @endif
        @endforeach

        @foreach($all_tasks as $tsk)
            @if( strtotime($tsk->due_date) <= 0 )
                @if(@$tsk->assigned_to_who->status== 1 )
                    @include('tasks.list_item')
                @endif
            @endif
        @endforeach

        </ul>   
    </div>
    @if(Auth::user()->role == 'admin' )
        <div class="submit push_half--ends button-add">
            <a href="#" folder_id="{{$f_w_t->id}}" class="btn btn--small btn--primary edit_folder_btn">Edit</a>
            <a href="{{url('delete_folder')}}" folder_id="{{$f_w_t->id}}" class="btn btn--small btn--secondary delete_folder_btn">Delete</a>
        </div>
    @endif
</div>

<script>
    $('.datepicker').datepicker({
        minDate:new Date()
    });
    /*$('.summernote').summernote({
        toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'underline', 'clear']],
        ['font', [] ],
        ['fontsize', [] ],
        ['color', [] ],
        ['para', [] ],
        ['insert', ['link','picture'] ],
        ['height', [] ]
        ],
        height: 100,
    });*/

    $('.summernote').summernote({
                height: 100,
                airMode: true,
                popover: {
                air: [
                // [groupName, [list of button]]
                ['style', ['bold', 'underline', 'clear']],
                ['font', [] ],
                ['fontsize', [] ],
                ['color', [] ],
                ['para', [] ],
                ['insert', ['link','picture'] ],
                ['height', [] ]
                ]
                }

                });
        // Toggle on Folder name and icon
    
    // $(".optionsSec-header").click(function(){
    // $(this).next('.folder_info').toggle();
    // });
</script>