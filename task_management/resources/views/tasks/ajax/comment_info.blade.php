                <div class="todo-comment">
                    <h2 class="comment-headline">
                        <span>
                            <span class="comments-balloon">
                            
                            {{ $task_comments->count() }}
                            
                            </span>
                            <span class="comment-title">
                                Comments
                            </span>
                        </span>
                    </h2>
                    <div class="thread-comment">
                    @foreach ($task_comments as $comment )
                        <div class="row comment_display_section">
                            <!-- <div class="col-sm-2 col-md-2 col-lg-1">
                                <div class="comment-nameHead comment-nameHeadcomment">
                                {{ ucfirst( Str::limit($comment->users->name, 1,'') ) }}
                                </div>
                            </div> -->
                            <div class="col-12">
                                <div class="comment-sec">
                                    <span class="thread-comment-author">
                                        <strong>{{ $comment->users->name }}</strong>
                                    </span>
                                    <span class="thread-comment-options">
                                        <span class="thread-comment-time">
                                            @if(date('Y') == date('Y',strtotime($comment->created_at) ) )
                                            {{date('M d  H:i',strtotime($comment->created_at->timezone( session('user_time_zone') )->toDateTimeString() ) ) }}
                                            @else
                                            {{date('M d Y  H:i',strtotime($comment->created_at->timezone( session('user_time_zone') )->toDateTimeString() ) ) }}
                                            @endif
                                        </span>
                                        <div class="dropdown">
                                            <!-- <a class="btn btn-secondary comment-btnsmall dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">…</a> -->
                                            <!-- <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> -->
                                                <?php  if($comment->users->id == Auth::user()->id )  {  ?>
                                                <a class="dropdown-item edit_comment" href="#"><i class="fa fa-edit"></i></a>
                                                <a class="dropdown-item delete_comment" comment_id="{{ $comment->id }}" task_id="{{ $task->id }}" href="{{url('delete_comment')}}"><i class="fa fa-remove"></i></a>
                                                <?php } ?>
                                            <!-- </div> -->
                                        </div>
                                    </span>
                                    <span class="formatted_content">
                                       {{ $comment->comment }}
                                    </span>
                                </div>
                            </div>
                        </div>

                        <!-- edit form of comment will show when click on edit button  -->

                        <div class="row edit_comment_form_section" style="display:none;">
                            <!-- <div class="col-sm-2 col-md-2 col-lg-1">
                                <div class="comment-nameHead comment-nameHeadcomment">
                                        {{ ucfirst( Str::limit(Auth::user()->name, 1,'') ) }}
                                </div>
                            </div> -->
                            <div class="col-12">
                                <div class="comment-sec-last">
                                    <div class="comment-sec">
                                        
                                        <div class="edit-form-commetBox">
                                            <form class="edit_comment_form" method="post" action="{{route ('update_comment')}}">
                                            <div class="alerts alerts-danger" style="display:none"></div>
                                            {{ csrf_field() }}
                                            <input type="hidden" name="form_type" value="task_comment_edit">
                                            <input name="comment_id" class="comment_id" type="hidden" class="form-control" value="{{ $comment->id }}">
                                            <input name="logged_user_id" class="logged_user_id" type="hidden" class="form-control" value="{{ Auth::user()->id }}">
                                            <input name="folder_id" class="folder_id" type="hidden" class="form-control" value="{{$task->folder}}">
                                            <input name="task_id" class="task_id" type="hidden" class="form-control" value="{{$task->id}}">
                                                <div class="form-group">
                                                    <textarea class="form-control" name="comment" id="comment" rows="3"> {{ $comment->comment }} </textarea>
                                                </div>
                                                <!-- <div class="form-group">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" class="comment_notify" name="comment_notify" value="1" {{ $comment->notify ? 'checked':'' }}/> Notify user by Email
                                                        </label>
                                                    </div>                                                    
                                                </div> -->
                                                <div class="submit push_half--ends">
                                                    <input type="submit" value="Save" class="btn btn-sm btn--small btn--primary edit_comment_btn">
                                                    <button type="reset" class="btn btn-sm btn--small btn--secondary edit-comment-cancel reset_btn">Discard</button>

                                                    
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  end of comment edit form -->

                    @endforeach
                        <div class="row">
                            <!-- <div class="col-sm-2 col-md-2 col-lg-1">
                                <div class="comment-nameHead comment-nameHeadcomment">
                                        {{ ucfirst( Str::limit(Auth::user()->name, 1,'') ) }}
                                </div>
                            </div> -->
                            <div class="col-12">
                                <div class="comment-sec-last">
                                    <div class="comment-sec">
                                        <button type="button" class="prompt-commentbtn">Add a comment…</button>
                                        <div class="form-commetBox">
                                            <form class="add_comment_form" method="post" action="{{route ('save_comment')}}" id="add_comment_form">
                                            <div class="alerts alerts-danger" style="display:none"></div>
                                            {{ csrf_field() }}
                                            <input type="hidden" name="form_type" value="task_comment">
                                            <input name="logged_user_id" class="logged_user_id" type="hidden" class="form-control" value="{{ Auth::user()->id }}">
                                            <input name="folder_id" class="folder_id" type="hidden" class="form-control" value="{{$task->folder}}">
                                            <input name="task_id" class="task_id" type="hidden" class="form-control" value="{{$task->id}}">
                                                <div class="form-group">
                                                    <textarea class="form-control" name="comment" class="comment" rows="3"></textarea>
                                                </div>
                                                <!-- <div class="form-group">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" class="comment_notify" name="comment_notify" value="1" /> Notify user by Email
                                                        </label>
                                                    </div>                                                    
                                                </div> -->
                                                <div class="submit push_half--ends">
                                                    <a href="{{route ('save_comment')}}" class="btn btn--small btn--primary add_comment_btn">Add this comment</a>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>