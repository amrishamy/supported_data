<div class="options-secD">
    <div class="optionsSec-header">
        <h3>
            <!-- <a class="taskHeading" href="#">{{$task->name}}</a> -->
        </h3>
    </div>
    <div class="task_info">

        <div class="edit_task_section edit_task_section_details" >
            <form class="task_disply_form_only">
                <fieldset  disabled="disabled" >
                <div class="row">
                    <div class="col-2">
                        <a class="icon-style back_button" href="{{url('tasks')}}"> <i class="fa fa-angle-left" aria-hidden="true"></i></a>
                    </div>
                    <div class="col-8">
                        <h1 class="display-4 text-center">{{$task->name}}</h1>
                    </div> 
                    <div class="col-2">
                        <a href="" class="icon-style edit_task_icon pull-right" > <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                    </div>
                </div>

                <div class="todos-form__field">
                    <div class="form-group row">
                        <label class="col-12 col-form-label text-center ">Made by <span class="font-italic font-weight-bold ">{{$task->createdBy->name }}</span> for <span class="font-italic font-weight-bold ">{{ (@$task->taskAssignedToFirst->usersa) ? @$task->taskAssignedToFirst->usersa->name: '____' }} </span> </label>
                        
                    </div>
                </div>

                @if(Auth::user()->id == $task->createdBy->id )
                <div class="todos-form__field">
                    <div class="form-group row">
                        <label class="col-3 col-form-label text-right font-weight-bold">Currently assigned to</label>
                        <div class="col-9">
                            <label class="currently_assign_label"> {{  @$task->taskAssignedToLast->usersa->name  }} </label>
                        </div>
                    </div>
                </div>
                @endif
                                
                <!-- <div class="todos-form__field">
                    <div class="form-group row">
                        <label class="col-3 col-form-label text-right font-weight-bold">Created By</label>
                        <div class=" col-9">
                        <input name="created_by" readonly="readonly" type="text" class="form-control-plaintext" value="{{$task->createdBy->name }}" />
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="assigned_to" class="col-3 col-form-label text-right font-weight-bold">Assigned to</label>
                    <div class="col-9">
                    <input type="text" readonly class="form-control-plaintext" id="assigned_to" value=" {{ ($task->users) ? $task->users->name: '' }}">
                    </div>
                </div> -->

                <div class="form-group row">
                    <label for="due_date" class="col-3 col-form-label text-right font-weight-bold">Due on</label>
                    <div class="col-9">
                    <input type="text" readonly class="form-control-plaintext" id="due_date" value="{{ (strtotime($task->due_date) > 0 ) ? date('M d Y', strtotime($task->due_date)): 'No Due Date' }}">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-3 col-form-label text-right font-weight-bold">Requires Approval for completion </label>
                    <div class="col-9 reminders_hours">
                        <div class="checkbox">
                            <label><input type="checkbox" name="admin_approval" value="1" class="mr-2" @if(Auth::user()->role != "admin") disabled @endif {{ $task->admin_approval ? 'checked':'' }} >  </label>
                        </div>
                        
                    </div>
                </div>

                 <!-- @include('tasks.reminders', ['tsk' => $task ] ) -->

                <div class="form-group row">
                    <label for="notes" class="col-3 col-form-label text-right font-weight-bold">Notes</label>
                    <div class="col-9">
                        {!!html_entity_decode($task->notes)!!}
                        <!-- <textarea  style="resize: none;" readonly class="form-control-plaintext" id="notes"> {{ $task->notes }}
                        </textarea> -->
                    </div>
                </div>

                <div class="form-group row">
                    <label for="notes" class="col-3 col-form-label text-right font-weight-bold">Uploaded file</label>
                    <div class="col-9">
                    
                    @if($task->uploads)
                    <a class="attachment_task btn btn--primary" href="{{ url('public/uploads').'/'. $task->uploads }}"  download="{{$task->uploads}}" > {{ $task->uploads }} </a>
                    @else
                    {{ 'No file uploaded' }}
                    @endif
                    
                    </div>
                </div>
                </fieldset>
                <div class="form-group row">
                    <div class="col-12 text-center">
                        <!-- <button class="btn btn--small btn--secondary mark-folder">Mark Complete</button> -->

                        @if( (Auth::user()->role == 'admin') || ($task->created_by == Auth::user()->id) || Auth::user()->id == @$task->assigned_to_who->assigned_by )
                            @if( $task->admin_approval && Auth::user()->role != 'admin' )

                            @else
                                <a role="button" data-taskid="{{$task->id}}" folder-id="{{$task->folder}}" check-type="{{ (@$task->assigned_to_who->status ? "checked":"un-checked") }}" href="#" class="btn btn--small btn--secondary mark-folder update_status_single_task"> {{ (@$task->assigned_to_who->status ? "Reopen":"Mark Done") }} </a>
                            @endif
                        @endif
                        
                    </div>
                </div>
            </form>
            <div class="edit_task_frm_single_div" style="display:none;">
                <form method="POST" class="edit_task_frm_single">
                    <fieldset @if( !(Auth::user()->id == $task->created_by || Auth::user()->role == 'admin') ) disabled="disabled" @endif >
                    <div class="alerts alerts-danger" style="display:none"></div>
                        {{ csrf_field() }}
                        <input type="hidden" name="form_type" value="edit_task_single">
                        <input name="logged_user_id" class="logged_user_id" type="hidden" class="form-control" value="{{ Auth::user()->id }}">
                        <input name="folder_id" class="folder_id" type="hidden" class="form-control" value="{{$task->folder}}">
                        <input name="task_id" class="task_id" type="hidden" class="form-control" value="{{$task->id}}">
                        <header class="todos-form__header">
                            <div class="checkbox todos-form__checkbox">

                                <span class="checkbox__content">
                                <input type="text" name="task_name" class="todos-form__title input input--full-width input--borderless input--unpadded" placeholder="Describe this to-do…" autofocus="autofocus" value="{{$task->name}}" required>
                                </span>
                            </div>
                        </header>
                        <section class="todos-form__details">

                            <div class="todos-form__field">
                                <div class="form-group row">
                                    <label class="col-12 col-form-label text-center ">Made by <span class="font-italic font-weight-bold ">{{$task->createdBy->name }}</span> for <span class="font-italic font-weight-bold select-assign">

                                    @if(Auth::user()->id == @$task->taskAssignedToFirst->assigned_to  )

                                        {{  @$task->taskAssignedToFirst->usersa->name  }}

                                    @else
                                        <select name="assigned_to" class="form-control form-control-sm assigned_to" >
                                        <option value="">Select user</option>
                                        @isset($user_list)
                                        @foreach($user_list as $row)
                                        @if($row->role != 'admin')
                                            <option value="{{$row->id}}" {{ (@$task->taskAssignedToFirst->assigned_to == $row->id ? "selected":"") }} > {{$row->name}} {{ ($row->status == "Pending" ? "( Pending)" : "") }}</option>
                                        @endif
                                        @endforeach
                                        @endisset
                                        </select>
                                    @endif 

                                    </span> </label>
                                    
                                </div>
                            </div>

                            @if(Auth::user()->id == $task->createdBy->id )
                            <div class="todos-form__field">
                                <div class="form-group row">
                                    <label class="col-3 col-form-label text-right font-weight-bold">Currently assigned to</label>
                                    <div class="col-9">
                                        <label class="currently_assign_label"> {{  @$task->taskAssignedToLast->usersa->name  }} </label>
                                    </div>
                                </div>
                            </div>
                            @endif

                            <!-- <div class="todos-form__field">
                                <div class="form-group row">
                                    <label class="col-3">Assigned to</label>
                                    <div class="col-9">
                                        <select name="assigned_to" class="form-control form-control-sm assigned_to" >
                                            <option value="">Select user</option>
                                            @isset($user_list)
                                            @foreach($user_list as $row)
                                            @if($row->role != 'admin')
                                            <option value="{{$row->id}}" {{ ($task->assigned_to == $row->id ? "selected":"") }} > {{$row->name}} {{ ($row->status == "Pending" ? "( Pending)" : "") }} </option>
                                            @endif
                                            @endforeach
                                            @endisset
                                        </select>
                                    </div>
                                </div>
                            </div> -->
                            <div class="todos-form__field">
                                <div class="form-group row">
                                    <label class="col-3">Due on</label>
                                    <div class="col-9">
                                    <input name="due_date" readonly="readonly" type="text" class="datepicker form-control-plaintext due_date" value="{{ ( strtotime($task->due_date) > 0 ) ? date('M d Y', strtotime($task->due_date)):'' }}" placeholder="" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3 col-form-label text-right font-weight-bold">Requires Approval for completion </label>
                                <div class="col-9 reminders_hours">
                                    <div class="checkbox">
                                        <label><input type="checkbox" name="admin_approval" value="1" class="mr-2" @if(Auth::user()->role != "admin") disabled @endif {{ $task->admin_approval ? 'checked':'' }} >  </label>
                                    </div>
                                    
                                </div>
                            </div>

                            <!-- @include('tasks.reminders', ['tsk' => $task ] ) -->

                            <div class="todos-form__field">
                                <div class="form-group row">
                                    <label class="col-3">Notes</label>
                                    <div class="col-9">
                                    <!-- <textarea name="notes" class="form-control-plaintext" id="staticassign"  placeholder="Add extra details or attach a file…">{{$task->notes}}</textarea> -->

                                    <textarea name="notes" class="form-control-plaintext summernote"  placeholder="blah blah blah">{{$task->notes}}</textarea>

                                    </div>
                                </div>
                            </div>

                            <div class="todos-form__field">
                                <div class="form-group row">
                                    <label class="col-3">Upload File</label>
                                    <div class="col-9">
                                    <input name="file"  type="file" class=" form-control-plaintext file" placeholder="" />

                                    <input type="hidden" class="up_file" name="up_file" value="" />

                                    <span> {{$task->uploads}} </span>
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="todos-form__field">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label class="checkbox-details">
                                            <input type="checkbox" name="notify_user" class="form-control-plaintext notify_user" value="1" {{ $task->notify ? 'checked':'' }} > Notify user by Email
                                        </label>
                                    </div>
                                </div>
                            </div> -->
                    </fieldset>
                            <div class="submit push_half--ends">  
                                <?php if( Auth::user()->id == $task->created_by  || Auth::user()->role == 'admin' )
                                { 
                                ?>
                                    <input type="submit" value="Save changes" class="btn btn--small btn--primary edit_task_btn_single">
                                    <a class="delete_task_single btn btn--secondary" role="button" folder-id="{{$task->folder}}" data-taskid="{{$task->id}}" href="{{url('delete_task_single')}}"> Delete task</a>
                                <?php
                                } ?>
                                <button type="reset" class="btn btn--small btn--secondary edit-task-single-cancel reset_btn">Discard</button>
                            </div>
                        </section>
                </form>
            </div>    
        </div>
    </div>                       
</div>


<script>
 $(document).ready(function(){
    $('.datepicker').datepicker({
    minDate:new Date()
    });

    /*$('.summernote').summernote({
        toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'underline', 'clear']],
        ['font', [] ],
        ['fontsize', [] ],
        ['color', [] ],
        ['para', [] ],
        ['insert', ['link','picture'] ],
        ['height', [] ]
        ],
        height: 100,
        });*/

        $('.summernote').summernote({
                height: 100,
                airMode: true,
                popover: {
                air: [
                // [groupName, [list of button]]
                ['style', ['bold', 'underline', 'clear']],
                ['font', [] ],
                ['fontsize', [] ],
                ['color', [] ],
                ['para', [] ],
                ['insert', ['link','picture'] ],
                ['height', [] ]
                ]
                }

                });
});
</script>