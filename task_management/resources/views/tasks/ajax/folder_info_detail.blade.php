    <ul class="todos remaining pending_tasks">
    @isset($tasks)
    @foreach ($tasks as $task) 
    @if($task->status== 0 )
        <li class="todo">
            <div class="indent">
                <div class="checkbox">
                    <label  style="font-size: 1em; @if((Auth::user()->id != $task->created_by || Auth::user()->id != $task->assigned_to) && Auth::user()->role != 'admin' ) cursor:not-allowed  @endif " class="checkbox__label">
                        <input type="checkbox" data-taskid="{{$task->id}}" folder-id="{{$task->folder}}" name="todo_complete" value="1" class="checkbox__input" data-behavior="todo_completion" @if((Auth::user()->id != $task->created_by || Auth::user()->id != $task->assigned_to) && Auth::user()->role != 'admin' ) disabled @endif data-move-completed="true" data-url="/3142486/buckets/11615450/todos/1706117094/completion?replace=false">
                        <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                    </label>
                    <span class="checkbox__content unassigned">
                        <a href="" class="task_name" draggable="false">{{$task->name}}</a>
                        <a href="javascript:;" class="task_name taskUserdate"> - 
                            {{$task->users->name}}</a>
                        <a href="javascript:;" class="task_name taskUserdate"> - 
                        @if(date('Y') == date('Y',strtotime($task->due_date) ) )
                            {{date('d-M',strtotime($task->due_date) ) }}
                        @else
                            {{date('d-M-Y',strtotime($task->due_date) ) }}
                        @endif
                        </a>
                        
                        @if((Auth::user()->role == 'admin') || (Auth::user()->id != $task->assigned_to))
                        <a class="delete_task" data-taskid="{{$task->id}}" href="{{url('delete_task')}}"> <i class="fa fa-remove" style="font-size:16px;color:red ;float: right"></i></a>
                        @endif

                        <a class="comment_task btn btn--primary" role="button" folder-id="{{$f_w_t->id}}" data-taskid="{{$tsk->id}}" href="{{route('task_detail', ['id' => $tsk->id ])}}"> Comments</a>
                    </span>
                    
                    <div class="edit_task_section" style="display:none;">
                        <form method="POST" class="edit_task_frm_list">
                        <div class="alerts alerts-danger" style="display:none"></div>
                        {{ csrf_field() }}
                        <input type="hidden" name="form_type" value="edit_task">
                        <input name="logged_user_id" class="logged_user_id" type="hidden" class="form-control" value="{{ Auth::user()->id }}">
                        <input name="folder_id" class="folder_id" type="hidden" class="form-control" value="{{$folder}}">
                        <input name="task_id" class="task_id" type="hidden" class="form-control" value="{{$task->id}}">
                            <header class="todos-form__header">
                                <div class="checkbox todos-form__checkbox">                                    
                                    <span class="checkbox__content">
                                    <input type="text" name="task_name" class="todos-form__title input input--full-width input--borderless input--unpadded" placeholder="Describe this to-do…" autofocus="autofocus" value="{{$task->name}}" required>
                                    </span>
                                </div>
                            </header>
                            <section class="todos-form__details">
                                <div class="todos-form__field">
                                    <div class="form-group row">
                                        <label class="col-sm-12 col-lg-3">Assigned to</label>
                                        <div class="col-sm-12 col-lg-9">
                                        <select name="assigned_to" class="form-control form-control-sm assigned_to" required  @if(Auth::user()->id != $task->created_by && Auth::user()->role != 'Admin' ) disabled @endif >
                                        <option value="">Select user</option>
                                        @isset($user_list)
                                        @foreach($user_list as $row)
                                        @if($row->role != 'admin')
                                        <option value="{{$row->id}}" {{ ($task->assigned_to == $row->id ? "selected":"") }} > {{$row->name}}</option>
                                        @endif
                                        @endforeach
                                        @endisset
                                        </select>
                                        <!-- <input name="assigned_to" type="text" class="form-control-plaintext assigned_to" id="staticassign" value="" placeholder="Type names to assign…"> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="todos-form__field">
                                    <div class="form-group row">
                                        <label class="col-sm-12 col-lg-3">Due on</label>
                                        <div class="col-sm-12 col-lg-9">
                                        <input name="due_date" readonly="readonly" type="text" class="datepicker form-control-plaintext due_date" value="{{ date('m/d/Y', strtotime($task->due_date)) }}" placeholder="" required/>
                                        </div>
                                    </div>
                                </div>
                                <div class="todos-form__field">
                                    <div class="form-group row">
                                        <label class="col-sm-12 col-lg-3">Notes</label>
                                        <div class="col-sm-12 col-lg-9">
                                        <textarea name="notes" class="form-control-plaintext summernote" id="staticassign"  placeholder="blah blah blah">{{$task->notes}}</textarea>
                                        <!-- <input name="notes" type="text" class="form-control-plaintext notes" id="staticassign" value="" placeholder="Add extra details or attach a file…"> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="todos-form__field">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="notify_user" class="form-control-plaintext notify_user" value="1" {{ $task->notify ? 'checked':'' }} > Notify user by Email
                                            </label>
                                        </div>                                        
                                    </div>
                                </div>
                                <div class="submit push_half--ends">  
                                    <input type="submit" value="Save changes" class="btn btn-sm btn--small btn--primary edit_task_btn_list">
                                    <button type="reset" class="btn btn-sm btn--small btn--secondary edit-task-cancel reset_btn">Discard changes</button>
                                </div>
                            </section>
                        </form>
                    </div>
                </div>
            </div>
        </li>
    @endif
    @endforeach
    @endisset

        
    </ul>
    <div class="todolist-actionsBtn">
        <button name="button" type="submit" class="btn btn-sm btn--small btn--primary-on-android">Add a task</button>
        <div class="todo-addDescribe">
            <form method="POST" class="add_task_frm">
            {{ csrf_field() }}
            <input type="hidden" name="form_type" value="detail">
            <input name="logged_user_id" class="logged_user_id" type="hidden" class="form-control" value="{{ Auth::user()->id }}">
            <input name="folder_id" class="folder_id" type="hidden" class="form-control" value="{{$folder->id}}">
                <header class="todos-form__header">
                    <div class="checkbox todos-form__checkbox">
                        <!-- <label class="checkbox__label">
                        <span class="checkbox__button"></span>
                        </label> -->
                        <span class="checkbox__content">
                        <input type="text" name="task_name" class="todos-form__title input input--full-width input--borderless input--unpadded" placeholder="Describe this to-do…" autofocus="autofocus">
                        </span>
                    </div>
                </header>
                <section class="todos-form__details">
                    <div class="todos-form__field">
                        <div class="form-group row">
                            <label class="col-sm-2 col-lg-3">Assigned to</label>
                            <div class="col-sm-10 col-lg-9">
                            <select name="assigned_to" class="form-control form-control-sm assigned_to">
                            <option>Select user</option>
                            @isset($user_list)
                            @foreach($user_list as $row)
                            @if($row->role != 'admin')
                            <option value="{{$row->id}}">{{$row->name}}</option>
                            @endif
                            @endforeach
                            @endisset
                            </select>
                            <!-- <input name="assigned_to" type="text" class="form-control-plaintext assigned_to" id="staticassign" value="" placeholder="Type names to assign…"> -->
                            </div>
                        </div>
                    </div>
                    <div class="todos-form__field">
                        <div class="form-group row">
                            <label class="col-sm-2 col-lg-3">Due on</label>
                            <div class="col-sm-10 col-lg-9">
                            <input name="due_date" readonly="readonly" type="text" class="datepicker form-control-plaintext due_date " id="" value="" placeholder="Select a date…">
                            </div>
                        </div>
                    </div>
                    <div class="todos-form__field">
                        <div class="form-group row">
                            <label class="col-sm-2 col-lg-3">Notes</label>
                            <div class="col-sm-10 col-lg-9">
                            <textarea name="notes" class="form-control-plaintext summernote" id="staticassign" value="" placeholder="blah blah blah"></textarea>
                            <!-- <input name="notes" type="text" class="form-control-plaintext notes" id="staticassign" value="" placeholder="Add extra details or attach a file…"> -->
                            </div>
                        </div>
                    </div>
                    <div class="submit push_half--ends">  
                        <input type="submit" value="Add this task" class="btn btn-sm btn--small btn--primary add_task_btn">
                        <button type="reset" class="btn btn-sm btn--small btn--secondary addDescribe-cancel reset_btn">Cancel</button>
                    </div>
                </section>
            </form>
        </div>   
    </div>
    <ul class="todos remaining completed_task ">
    @isset($tasks)
    @foreach ($tasks as $task) 
    @if($task->status== 1 )
    <li class="todo">
        <div class="indent">
            <div class="checkbox">
                <label  style="font-size: 1em; @if((Auth::user()->id != $task->created_by || Auth::user()->id != $task->assigned_to) && Auth::user()->role != 'admin' ) cursor:not-allowed  @endif" class="checkbox__label">
                    <input checked type="checkbox" data-taskid="{{$task->id}}" folder-id="{{$task->folder}}" name="todo_complete" value="1" class="checkbox__input" data-behavior="todo_completion" @if((Auth::user()->id != $task->created_by || Auth::user()->id != $task->assigned_to) && Auth::user()->role != 'admin' ) disabled @endif data-move-completed="true" data-url="/3142486/buckets/11615450/todos/1706117094/completion?replace=false">
                    <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                </label>
                <span class="checkbox__content unassigned">
                    <a href="" class="task_name" draggable="false">{{$task->name}}</a>
                    <a href="javascript:;" class="task_name taskUserdate"> - 
                            {{$task->users->name}}</a>
                    <a href="javascript:;" class="task_name taskUserdate"> - 
                    @if(date('Y') == date('Y',strtotime($task->due_date) ) )
                        {{date('d-M',strtotime($task->due_date) ) }}
                    @else
                        {{date('d-M-Y',strtotime($task->due_date) ) }}
                    @endif
                    </a>
                    
                    @if((Auth::user()->role == 'admin') || (Auth::user()->id != $task->assigned_to))
                    <a class="delete_task" data-taskid="{{$task->id}}" href="{{url('delete_task')}}"> <i class="fa fa-remove" style="font-size:16px;color:red ;float: right"></i></a>
                    @endif
                    <!-- <a class="todo__unassigned-unscheduled" data-remote="true" href="/3142486/buckets/11615450/todos/1706117094/edit?assign=true" draggable="false">Assign/Schedule…</a> -->
                </span>
                <div class="edit_task_section" style="display:none;">
                    <form method="POST" class="edit_task_frm_list">
                    <div class="alerts alerts-danger" style="display:none"></div>
                    {{ csrf_field() }}
                    <input type="hidden" name="form_type" value="edit_task">
                    <input name="logged_user_id" class="logged_user_id" type="hidden" class="form-control" value="{{ Auth::user()->id }}">
                    <input name="folder_id" class="folder_id" type="hidden" class="form-control" value="{{$task->folder}}">
                    <input name="task_id" class="task_id" type="hidden" class="form-control" value="{{$task->id}}">
                        <header class="todos-form__header">
                            <div class="checkbox todos-form__checkbox">                                
                                <span class="checkbox__content">
                                <input type="text" name="task_name" class="todos-form__title input input--full-width input--borderless input--unpadded" placeholder="Describe this to-do…" autofocus="autofocus" value="{{$task->name}}" required>
                                </span>
                            </div>
                        </header>
                        <section class="todos-form__details">
                            <div class="todos-form__field">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-lg-3">Assigned to</label>
                                    <div class="col-sm-10 col-lg-9">
                                        <select name="assigned_to" class="form-control form-control-sm assigned_to" required>
                                        <option value="">Select user</option>
                                        @isset($user_list)
                                        @foreach($user_list as $row)
                                        @if($row->role != 'admin')
                                        <option value="{{$row->id}}" {{ ($task->assigned_to == $row->id ? "selected":"") }} > {{$row->name}}</option>
                                        @endif
                                        @endforeach
                                        @endisset
                                        </select>
                                        <!-- <input name="assigned_to" type="text" class="form-control-plaintext assigned_to" id="staticassign" value="" placeholder="Type names to assign…"> -->
                                    </div>
                                </div>
                            </div>
                            <div class="todos-form__field">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-lg-3">Due on</label>
                                    <div class="col-sm-10 col-lg-9">
                                        <input name="due_date" readonly="readonly" type="text" class="datepicker form-control-plaintext due_date" value="{{ date('m/d/Y', strtotime($task->due_date)) }}" placeholder="" required/>
                                    </div>
                                </div>
                            </div>
                            <div class="todos-form__field">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-lg-3">Notes</label>
                                    <div class="col-sm-10 col-lg-9">
                                        <textarea name="notes" class="form-control-plaintext summernote" id="staticassign"  placeholder="blah blah blah">{{$task->notes}}</textarea>
                                        <!-- <input name="notes" type="text" class="form-control-plaintext notes" id="staticassign" value="" placeholder="Add extra details or attach a file…"> -->
                                    </div>
                                </div>
                            </div>

                            <div class="todos-form__field">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="notify_user" class="form-control-plaintext notify_user" value="1" {{ $task->notify ? 'checked':'' }} > Notify user by Email
                                        </label>
                                    </div>                                    
                                </div>
                            </div>

                            <div class="submit push_half--ends">
                                @if((Auth::user()->id == $tsk->created_by || Auth::user()->id == $tsk->assigned_to) || Auth::user()->role == 'admin' )  
                                <input type="submit" data-taskid="{{$task->id}}"  value="Reopen Task" class="btn btn-sm btn--small btn--primary reopen_task_btn_list" folder-id="{{$task->folder}}">
                                @endif
                                <button type="reset" class="btn btn-sm btn--small btn--secondary edit-task-cancel reset_btn">Discard changes</button>
                            </div>
                        </section>
                    </form>

                </div>
            </div>
        </div>
    </li>
    @endif
    @endforeach
    @endisset
    </ul>


<script>
    $('.datepicker').datepicker({
        minDate:new Date()
    });
</script>