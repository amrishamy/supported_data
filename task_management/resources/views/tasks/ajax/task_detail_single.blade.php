        <div class="indent">
        
            <div class="checkbox">
                <label  style="font-size: 1em" class="checkbox__label">
                    <input type="checkbox" data-taskid="{{$tsk->id}}" folder-id="{{$f_w_t->id}}" name="todo_complete" value="1" class="checkbox__input" data-behavior="todo_completion" @if(Auth::user()->id == $tsk->assigned_to) disabled @endif data-move-completed="true" data-url="/3142486/buckets/11615450/todos/1706117094/completion?replace=false">
                    <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                </label>
                <span class="checkbox__content unassigned">
                    <a href="javascript:;" class="task_name">{{$tsk->name}}</a>
                </span>
                <div class="edit_task_section" style="display:none;">
                    
                    <form method="POST" class="edit_task_frm_list">
                    {{ csrf_field() }}
                    <div class="alerts alerts-danger" style="display:none"></div>
                    <input type="hidden" name="form_type" value="edit_task">
                    <input name="logged_user_id" class="logged_user_id" type="hidden" class="form-control" value="{{ Auth::user()->id }}">
                    <input name="folder_id" class="folder_id" type="hidden" class="form-control" value="{{$f_w_t->id}}">
                    <input name="task_id" class="task_id" type="hidden" class="form-control" value="{{$tsk->id}}">
                        <header class="todos-form__header">
                            <div class="checkbox todos-form__checkbox">
                                
                                <span class="checkbox__content">
                                <input type="text" name="task_name" class="todos-form__title input input--full-width input--borderless input--unpadded" placeholder="Describe this to-do…" autofocus="autofocus" value="{{$tsk->name}}">
                                </span>
                            </div>
                        </header>
                        <section class="todos-form__details">
                            <div class="todos-form__field">
                                <div class="form-group row">
                                    <label class="col-sm-2">Assigned to</label>
                                    <div class="col-sm-10">
                                    <select name="assigned_to" class="form-control form-control-sm assigned_to">
                                    <option value="">Select user</option>
                                    @isset($user_list)
                                    @foreach($user_list as $row)
                                    @if($row->role != 'admin')
                                    <option value="{{$row->id}}" {{ ($tsk->assigned_to == $row->id ? "selected":"") }} > {{$row->name}}</option>
                                    @endif
                                    @endforeach
                                    @endisset
                                    </select>
                                    </div>
                                </div>
                            </div>
                            <div class="todos-form__field">
                                <div class="form-group row">
                                    <label class="col-sm-2">Due on</label>
                                    <div class="col-sm-10">
                                    <input name="due_date" readonly="readonly" type="text" class="datepicker form-control-plaintext due_date" value="{{ date('m/d/Y', strtotime($tsk->due_date)) }}" placeholder="" />
                                    </div>
                                </div>
                            </div>
                            <div class="todos-form__field">
                                <div class="form-group row">
                                    <label class="col-sm-2">Notes</label>
                                    <div class="col-sm-10">
                                    <textarea name="notes" class="form-control-plaintext summernote" id="staticassign"  placeholder="blah blah blah">{{$tsk->notes}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="submit push_half--ends">  
                                <input type="submit" value="Save changes" class="btn btn-sm btn--small btn--primary edit_task_btn_list">
                                <button type="reset" class="btn btn-sm btn--small btn--secondary edit-task-cancel reset_btn">Discard changes</button>
                            </div>
                        </section>
                    </form>

                </div>
                
            </div>
        </div>
        <script>
           $('.datepicker').datepicker({
            minDate:new Date()
           });
        </script>