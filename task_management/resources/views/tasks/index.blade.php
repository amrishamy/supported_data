@extends('layouts.app')
@section('content')

<style>
	.alerts-danger { color: red; }
</style>

<div class="container">
    <div class="middle-arg task">
        @include('flash-message')
            <div class="heading-sec">
                <h1>Folders</h1>
                @if(Auth::user()->role == "admin")
                <label class="web-header-new">
                    <span class="btn btn-small btn-primary btn--add-icon-reversed todo-addNewList">New Folder</span>
                </label>
                @endif
            </div>
            <div class="col-sm-12 ">
                <div class="todo-newList">
                    <form method="POST" action="<?php echo url('/foldercreate'); ?>" class="add_folder_frm" id="add_folder_frm">
                        {{ csrf_field() }}                         
                        <div class="newList-form">                            
                            <div class="form-group">
                                <input name="name" required type="text" class="form-control-plaintext folder_name" id="exampleFormControl1" placeholder="Name this folder">
                                <input name="logged_user_id" type="hidden" class="form-control" value="{{ Auth::user()->id }}">
                            </div>
                            <!-- <div class="form-group">
                                <input type="text" class="form-control-plaintext" id="exampleFormControl2" placeholder="Add extra details or attach a file…">
                            </div> -->
                            <div class="submit push_half--ends">  
                                <input type="submit" value="Add this folder" class="btn btn--small btn--primary add_this_folder">
                                <button type="reset" class="btn btn--small btn--secondary todo-cancel">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


            <div class="options-secs">
            @if(isset($folders_with_tasks) && !empty($folders_with_tasks))
                <div class='dragbox-container'>
                    <div class="title dd-folder-title" id='title0'>  </div>
                    <ul class='space first-space'>
                        @foreach($folders_with_tasks as $f_w_t)
                            @if($f_w_t->order == 0)
                                @continue
                            @endif

                            @include('tasks.folder_info_recursive', $f_w_t)
                        @endforeach
                    </ul>
                </div>
            @endif
            </div>
                
			<div class="clearfix"><br/><br/></div>
            <div class="heading-sec">
                <h1>Gantt Chart</h1>
            </div>

            <div class="table-responsive" style="padding-bottom: 10px;"><div style="position:relative" class="gantt" id="GanttChartDIV"></div></div>

            </div>
        </div>
        
<script>
    $(document).ready(function(){
        $('.datepicker').datepicker({
            minDate:new Date()
        });

        $('.summernote').summernote({
            placeholder: true,
            placeholder: "blah blah blah",
            height: 100,
            airMode: true,
            popover: {
				air: [
					// [groupName, [list of button]]
					['style', ['bold', 'underline', 'clear']],
					['font', [] ],
					['fontsize', [] ],
					['color', [] ],
					['para', [] ],
					['insert', ['link','picture'] ],
					['height', [] ]
				]
            }
		});
    });
</script>
@endsection