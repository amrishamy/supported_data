@if( ($tsk->is_assigned_to_me() || $tsk->created_by == Auth::user()->id ) || ( Auth::user()->role == "admin") )
<li class="todo task-li-{{$tsk->id}}">
       <?php 
           // echo $tsk->id;
            //$all_assignments = $tsk->assignments->all() ;
//echo "<pre>hello";print_r( $tsk->assignments );
/*foreach($tsk->assignments as $TA)
{
    echo $TA->assigned_to ;
}*/
       ?>
    <div class="indent">
    
        <div class="checkbox">
            <!-- <label  style="font-size: 1em; @if( (Auth::user()->role != 'admin') && ($tsk->created_by != Auth::user()->id) ) cursor:not-allowed  @endif " class="checkbox__label">
                @if($tsk->status== 1 )
                    <input checked type="checkbox" data-taskid="{{$tsk->id}}" folder-id="{{$f_w_t->id}}" name="todo_complete" value="1" class="checkbox__input" data-behavior="todo_completion" data-move-completed="true" data-url="/3142486/buckets/11615450/todos/1706117094/completion?replace=false" @if( (Auth::user()->role != 'admin') && ($tsk->created_by != Auth::user()->id) ) disabled @endif>
                @else
                    <input type="checkbox" data-taskid="{{$tsk->id}}" folder-id="{{$f_w_t->id}}" name="todo_complete" value="1" class="checkbox__input" data-behavior="todo_completion" data-move-completed="true" data-url="/3142486/buckets/11615450/todos/1706117094/completion?replace=false" @if( (Auth::user()->role != 'admin') && ($tsk->created_by != Auth::user()->id) ) disabled @endif>

                @endif
                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
            </label> -->
            <span class="checkbox__content unassigned">
                <a href="javascript:;" class="task_name">{{$tsk->name}}</a>
                 <?php //print_r( @$tsk->assigned_to_who[0]->usersa->name); 
                        //echo "<pre>";print_r( @$tsk->assigned_to_who); 
                 ?>
                <a href="javascript:;" class="task_name taskUserdate">  {{ @$tsk->assigned_to_who->usersa->name }}</a>
                @if( strtotime($tsk->due_date) > 0 )
                <a href="javascript:;" class="task_name taskUserdate"> - 
                @if(date('Y') == date('Y',strtotime($tsk->due_date) ) )
                    {{date('M d', strtotime ( $tsk->due_date->timezone( session('user_time_zone') )->toDateTimeString() ) ) }}
                @else
                {{date('M d Y',strtotime($tsk->due_date->timezone( session('user_time_zone') )->toDateTimeString() ) ) }}
                @endif
                </a>
                @endif                                                        
                <a class="comment_task btn btn--primary" role="button" folder-id="{{$f_w_t->id}}" data-taskid="{{$tsk->id}}" href="{{route('task_detail', ['id' => $tsk->id ])}}"  > <!--Comments--> <i class="fa fa-comments" aria-hidden="true" @if( $tsk->unread_comments_count->count() ) style="color:#37A6F3" @endif > {{ ( count($tsk->comments) > 0 ) ? count($tsk->comments) : ''  }} </i> 
                </a>
                @if($tsk->uploads)
                    <a class="attachment_task btn btn--primary" href="{{ url('public/uploads').'/'. $tsk->uploads }}" download="{{$tsk->uploads}}" ><i class="fa fa-paperclip" aria-hidden="true"></i>
                    </a>
                @endif
            </span>                                                  
        </div>
        <!-- edit task form here -->
        @include('tasks.edit_form')
    </div>
</li>
@endif