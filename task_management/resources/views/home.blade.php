@extends('layouts.app')
@section('content')
<style>
.alerts-danger{
    color: red;
}
</style>

<div class="container">
    <div class="middle-arg task">
        @include('flash-message')
        <div class="heading-sec">
            <h1>Summary</h1> 
        </div>

        <div class="options-secs">
            <div class="row">
                @isset($folders_with_tasks)
                @foreach($folders_with_tasks as $f_w_t)
                    @if( count( $f_w_t->tasks->where("status", 0) ) > 0 )
                        <div class="col-sm-12 col-md-6 folder-{{$f_w_t->id}}">
                        
                            <div class="options-secD thumbnail">
                                <div class="optionsSec-header">
                                    
                                    <h3>
                                        <div style="color:{{ $f_w_t->color() }};" class="comment-nameHead text-center folder_color">  
                                        @if($f_w_t->color() == 'black')
                                            <i class="fa fa-folder-open-o" aria-hidden="true"></i>
                                        @else
                                        <i class="fa fa-folder-open" aria-hidden="true"></i>
                                        @endif
                                        </div>

                                        @if(Auth::user()->role == "admin")
                                        
                                        <a class="taskHeading" href="javascript:void(0);">{{$f_w_t->name}}</a>
                                        @else
                                        <a class="taskHeading" href="javascript:void(0);">{{$f_w_t->name}}</a>
                                        @endif

                                    </h3>
                                </div>
                                <div class="folder_info" style="display:none;">
                                    <ul class="todos remaining ">
                                    @foreach($f_w_t->tasks as $tsk)
                                    <!-- <pre> -->
                                    <!-- {{print_r($tsk->users)}} -->
                                        @if($tsk->status== 0 )
                                            <li class="todo task-li-{{$tsk->id}}">
                                            
                                                <div class="indent">
                                                
                                                    <div class="checkbox">
                                                        
                                                        <span class="checkbox__content unassigned">
                                                            <a href="{{route('task_detail', ['id' => $tsk->id ])}}" class="task_name_home">{{$tsk->name}}</a>
                                                            <!-- <a href="{{route('task_detail', ['id' => $tsk->id ])}}" class="task_name_home taskUserdate"> - {{ ($tsk->users) ? $tsk->users->name : '' }}</a> -->

                                                            <a href="{{route('task_detail', ['id' => $tsk->id ])}}" class="task_name_home taskUserdate"> - Made by <b><i>{{$tsk->createdBy->name }}</i></b> for  <b><i>{{ ($tsk->users) ? $tsk->users->name : '____' }}</i></b> </a>

                                                            @if( strtotime($tsk->due_date) > 0 )
                                                            <a href="{{route('task_detail', ['id' => $tsk->id ])}}" class="task_name_home taskUserdate"> - 
                                                            @if(date('Y') == date('Y',strtotime($tsk->due_date) ) )
                                                                {{date('m/d',strtotime($tsk->due_date) ) }}
                                                            @else
                                                            {{date('m/d/Y',strtotime($tsk->due_date) ) }}
                                                            @endif
                                                            </a>
                                                            @endif
                                                            <a class="comment_task btn btn--primary" role="button" folder-id="{{$f_w_t->id}}" data-taskid="{{$tsk->id}}" href="{{route('task_detail', ['id' => $tsk->id ])}}" >  <i class="fa fa-comments" aria-hidden="true"> {{ count ($tsk->comments) }} </i>
                                                            </a>

                                                            @if($tsk->uploads)
                                                            <a class="attachment_task btn btn--primary" href="{{ url('public/uploads').'/'. $tsk->uploads }}" download="{{$tsk->uploads}}" ><i class="fa fa-paperclip" aria-hidden="true"></i>
                                                            </a>
                                                            @endif
                                                            
                                                        </span>
                                                        
                                                        
                                                        
                                                    </div>
                                                </div>
                                            </li>
                                        @endif
                                    @endforeach                            
                                    </ul>
                                    
                                                            
                                    
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
                @endisset
            </div>
        </div>

    </div>
</div>
        


@endsection