<!DOCTYPE html>
<html>
<head>
    <title>Welcome on Task Management</title>
</head>

<body>
<p>Dear <b> {{$task['user_info']['name']}} </b>,</p>
<br/>
<p>
Your task  <b><u><a href="{{route('task_detail', ['id' => $task->id ])}}"> {{$task['name']}} </a></u></b>  assigned by {{ $assigned_by_name }} is due on {{date('Y-m-d', strtotime($task['due_date']))}} 
</p>
 

Thanks

</body>

</html>