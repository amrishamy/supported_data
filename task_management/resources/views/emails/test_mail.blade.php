@component('mail::message')
<p>You are receiving this email because we received a password reset request for your account.</p>

@component('mail::button', ['url' =>url(config('app.url').route('password.reset',  $data['token'] , false))])
Reset Password 
@endcomponent

<p>If you did not request a password reset, no further action is required.</p>
Thanks,<br>
{{ config('app.name') }}
@endcomponent