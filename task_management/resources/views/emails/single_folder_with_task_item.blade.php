@isset($folders_with_tasks)
                            @foreach($folders_with_tasks as $key => $f_w_t)

                            <tr>
                                
                                <td style="width: 50%;padding: 0 15px;border-radius: 3px;">
                                    <p> 
                                    <img src="{{ asset('images/'.$f_w_t['folder']->color().'-folder.png') }}" />
                                    &nbsp;&nbsp; {{$f_w_t['folder']->name}} </p>
                                    
                                       
                                        @foreach( $f_w_t['new_task'] as $task )   
                                           
                                            @include('emails.task_item')

                                        @endforeach  



                                        @foreach( $f_w_t['due_task'] as $task )   
                                           
                                            @include('emails.task_item')

                                        @endforeach 

                                        

                                        @foreach( $f_w_t['past_due_task'] as $task )   
                                           
                                            @include('emails.task_item')

                                        @endforeach  

                                </td>
                                
                            </tr>


                            
                            @endforeach
                            @endisset