<p style="margin-left: 42px;">
    <a style="font-size: 15px;color: #283c46;text-decoration: none;" href="{{ route('task_detail', ['id' => $task->task_id ]) }}">{{ $task->name }}  </a>
    <a style="font-size: 14px;color: #848484;text-decoration: none; pointer-events:none;cursor: default;" href="{{ route('task_detail', ['id' => $task->task_id ]) }}"> {{ ($task->users) ? ' - '.$task->users->name : '' }} 
    
        @if(date('Y') == date('Y',strtotime($task->due_date) ) )
        {{date('M d',strtotime($task->due_date ) ) }}
        @else
        {{date('M d Y',strtotime($task->due_date ) ) }}
        @endif
    
                                                    </a>
    <a style="font-size: 14px;color: #848484; pointer-events:none;cursor: default;" href="{{ route('task_detail', ['id' => $task->task_id ]) }}"><img src="{{ url('public/images/comment.png') }}" /> {{ ( count($task->comments) > 0 ) ? count($task->comments) : ''  }} </a>
</p>