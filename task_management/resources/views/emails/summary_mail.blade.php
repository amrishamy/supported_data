<!DOCTYPE html>
<html>
<head>
    <title>Task Management</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://fortawesome.github.io/Font-Awesome/assets/font-awesome/css/font-awesome.css">

</head>
<body style="margin:0; padding:0;font-family: 'Open Sans', sans-serif;" bgcolor="#fff">
    <table style="width:600px;margin: 0 auto;" cellspacing="0" cellpadding="0" bgcolor="#fff">
        <tbody>
            <tr>
                <td>
                    <table style="background: #fff;margin-top: 20px;" width="100%" cellspacing="10" cellpadding="0">
                        <tbody style="vertical-align: top;">
                            <tr >
                                <td colspan="2">
                                   
                                    <p>
                                    <span style="text-align: left;font-size: 14px;line-height: 1;font-weight: 800;color: #575757;">Hi {{$user->name}}, </span>
                                    </p>
                                    <p style="text-align: left;font-size: 14px;">
                                        Here is your summary !
                                    </p>
                                </td>
                            </tr>

                            <tr >
                                <td colspan="2">
                                    <p style="margin-bottom: 0px;padding-bottom: 0.625em;border-bottom: 3px solid #575757; position: relative;width: 100%;text-align: left;font-size: 15px;line-height: 1;font-weight: 400;color: #575757;">Due Tasks</p>
                                </td>
                            </tr>
                            <?php
                                $NewTask        = false;
                                $DueTask        = false;
                                $PastDueTask    = false;
                            ?>

                            @isset($folders_with_tasks_due)
                                @foreach($folders_with_tasks_due as $key => $f_w_t)
                                    @if( count($f_w_t['coming_due_task'] ) > 0  ) 

                                        <?php $DueTask = true ; ?> 
                                        <tr>
                                            
                                            <td style="width: 50%;padding: 0 15px;border-radius: 3px;">
                                                <p> 
                                                <img src="{{ url('public/images/'.$f_w_t['folder']->color().'-folder.png') }}" />
                                                &nbsp;&nbsp; {{$f_w_t['folder']->name}} </p>
                                                
                                                   
                                                    @foreach( $f_w_t['coming_due_task'] as $task )   
                                                       
                                                        @include('emails.task_item')

                                                    @endforeach 
                                            </td>
                                            
                                        </tr>
                                    @endif
                                @endforeach
                            @endisset

                            @if( !$DueTask )
                                <tr>
                                            
                                    <td style="width: 50%;padding: 0 15px;border-radius: 3px;">
                                        <p>
                                        There are no due tasks.
                                        </p>
                                        
                                           
                                    </td>
                                            
                                </tr>
                            @endif

                            <tr >
                                <td colspan="2"> <br> <br>
                                    <p style="margin-bottom: 0px;padding-bottom: 0.625em;border-bottom: 3px solid #575757; position: relative;width: 100%;text-align: left;font-size: 15px;line-height: 1;font-weight: 400;color: #575757;">New Tasks</p>
                                </td>
                            </tr>

                            @isset($folders_with_tasks_new)
                                @foreach($folders_with_tasks_new as $key => $f_w_t)
                                    @if( count($f_w_t['new_task']) > 0  )
                                        <?php $NewTask = true ; ?> 
                                        <tr>
                                            
                                            <td style="width: 50%;padding: 0 15px;border-radius: 3px;">
                                                <p> 
                                                <img src="{{ url('public/images/'.$f_w_t['folder']->color().'-folder.png') }}" />
                                                &nbsp;&nbsp; {{$f_w_t['folder']->name}} </p>
                                                
                                                   
                                                    @foreach( $f_w_t['new_task'] as $task )   
                                                       
                                                        @include('emails.task_item')

                                                    @endforeach 
                                            </td>
                                            
                                        </tr>
                                    @endif
                                @endforeach
                            @endisset

                            @if( !$NewTask )
                                <tr>
                                            
                                    <td style="width: 50%;padding: 0 15px;border-radius: 3px;">
                                        <p>
                                        There are no new tasks.
                                        </p>
                                        
                                           
                                    </td>
                                            
                                </tr>
                            @endif

                            <tr >
                                <td colspan="2"> <br> <br>
                                    <p style="margin-bottom: 0px;padding-bottom: 0.625em;border-bottom: 3px solid #575757; position: relative;width: 100%;text-align: left;font-size: 15px;line-height: 1;font-weight: 400;color: #575757;">Past Due Tasks</p>
                                </td>
                            </tr>

                            @isset($folders_with_tasks_past_due)
                                @foreach($folders_with_tasks_past_due as $key => $f_w_t)
                                    @if( count($f_w_t['past_due_task']) > 0 )
                                        <?php $PastDueTask = true ; ?> 
                                        <tr>
                                            
                                            <td style="width: 50%;padding: 0 15px;border-radius: 3px;">
                                                <p> 
                                                <img src="{{ url('public/images/'.$f_w_t['folder']->color().'-folder.png') }}" />
                                                &nbsp;&nbsp; {{$f_w_t['folder']->name}} </p>
                                                
                                                   
                                                    @foreach( $f_w_t['past_due_task'] as $task )   
                                                       
                                                        @include('emails.task_item')

                                                    @endforeach 
                                            </td>
                                            
                                        </tr>
                                    @endif
                                @endforeach
                            @endisset

                            @if( !$PastDueTask )
                                <tr>
                                            
                                    <td style="width: 50%;padding: 0 15px;border-radius: 3px;">
                                        <p>
                                        There are no past due tasks.
                                        </p>
                                        
                                           
                                    </td>
                                            
                                </tr>
                            @endif
                            
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>
