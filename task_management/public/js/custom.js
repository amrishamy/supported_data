$(document).ready(function() {
    var timezone_offset_minutes = new Date().getTimezoneOffset();
    timezone_offset_minutes = timezone_offset_minutes == 0 ? 0 : -timezone_offset_minutes;

    // Timezone difference in minutes such as 330 or -360 or 0
    console.log(timezone_offset_minutes);
    console.log(Intl.DateTimeFormat().resolvedOptions().timeZone);

    $(document).ajaxStart(function(){
        $('.loader').addClass('show');
      });

      $(document).ajaxStop(function(){
        $('.loader').removeClass('show');
      });


});

$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    // some default hidden elements
    $('.add_folder_section').hide();
    $('.add_task_form').hide();

    // Toggle on Folder name and icon    
    // $(".optionsSec-header").click(function(){
    // $(this).next('.folder_info').toggle();
    // });

    $(".options-secs").on('click','.taskToggle, .taskHeading',function(e) {
        e.preventDefault();
        $item = $(this).parents('.optionsSec-header').next('.folder_info');
        $(this).parents('.optionsSec-header').next('.folder_info').toggle();

        $(".options-secs").find(".folder_info").not($item).hide();

        if($(this).parents('.optionsSec-header').find('i.angle').hasClass('fa-angle-up')) {
            $(this).parents('.optionsSec-header').find('i.angle').removeClass('fa-angle-up').addClass('fa-angle-down');
        } else {
            $(this).parents('.optionsSec-header').find('i.angle').removeClass('fa-angle-down').addClass('fa-angle-up');
        }
        return false;
    });

    $(".options-secs").on('click','.completed_folder_title',function(e){
        e.preventDefault();
        
        
        $item = $(this).closest('.task-subtasks').find('.completed_task');

       $item.toggle();
       

        //$(this).parents('.options-secD.thumbnail').find('.completed_task').toggle();

        //$(".options-secs").find(".completed_task").not( $item ).hide();
        //return false;
    });
    

    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 5000);

    $("#add_user_frm").validate();
    $(".add_folder_frm").validate();
    //$(".add_task_frm_list").validate();
    $(".add_task_frm").validate();
    $("#add_comment_form").validate();
    
    

    $(document).on('click', '.reset_btn', function(){
        $(this).closest('form').find('.alerts-danger').html('');
    })

    $('.new_folder_btn').on("click",function(e){
        e.preventDefault();
        $('.add_folder_section').show();
       
    });

    $('.hide_add_folder_section').on("click",function(e){
        e.preventDefault();
        $('.add_folder_section').hide();
       
    });

    /* $('.add_this_folder').on("click",function(e){
        e.preventDefault();
        var folder_name = $('.folder_name').val();
        //var datastring = "name="+folder_name;
        var datastring = $('.add_folder_frm').serialize();
        console.log(datastring);
        // return false;
        $.ajax({
            type: "POST",
            url: "foldercreate",
            data: datastring,
            dataType: "json",
            success: function(data){
                if(data.status=="okay")
                window.location.href = "";

              return true;
            },
            error: function(xhr, error){
                console.debug(xhr); console.debug(error);
            },

          });
       
    }); */

    // ***************************************start for task module*******************

    // add task using ajax on details page

    $(document).on('click', '.add_task_btn', "click",function(e){
        var curr_elem = $(this);
        e.preventDefault();
        var datastring = $(this).closest('.add_task_frm').serialize();
        console.log(datastring);
        $.ajax({
            type: "POST",
            url: baseURL+"/tasksave",
            data: datastring,
            dataType: "html",
            success: function(data){  
                // var thisSession = JSON.parse(data);
                // if(thisSession.hasOwnProperty('errors')){
                //     $(curr_elem).closest('form').find('.alerts-danger').html('');
                //     jQuery.each(thisSession.errors, function(key, value){
                //         $(curr_elem).closest('form').find('.alerts-danger').show();
                //         $(curr_elem).closest('form').find('.alerts-danger').append('<p>'+value+'</p>');
                //     });
                // }else{
                //     $(curr_elem).closest('.folder_info').html(data.success);
                //     $('.reset_btn').trigger("click");    
                // }
                if(data.includes('is required')){
                    $(curr_elem).closest('form').find('.alerts-danger').html('');
                        $(curr_elem).closest('form').find('.alerts-danger').show();
                        $(curr_elem).closest('form').find('.alerts-danger').html(data);
                }else{
                    $(curr_elem).closest('.folder_info').html(data);
                    $('.reset_btn').trigger("click");    
                }

            },
            error: function(error){
              //  console.debug(xhr); console.debug(error);
            },
        });
    }); 

    $(document).on('click', '.add_task_btn_list', function(e){
        var curr_elem = $(this);
        //var style = curr_elem.parents('.options-secD').attr('style').replace(/ /g,'');
        var form = $(this).closest('.add_task_frm_list');

        //$(this).closest(".add_task_frm_list").validate();
        //e.preventDefault();
        $(this).closest(".add_task_frm_list").validate({
            rules: {
                task_name: {
                    required: true,
                }
            },

            submitHandler: function(form) {
                var folder_id = $(form).find('.folder_id').val();
                var datastring = $(form).serialize();
                //var datastring = new FormData(form);
                console.log(datastring);
                 //return false;
                $.ajax({
                    type: "POST",
                    url: baseURL+"/save_task",
                    //enctype: 'multipart/form-data',
                    //processData: false,  // Important!
                    //contentType: false,
                    //cache: false,
                    data: datastring,
                    dataType: "html",
                    success: function(data) {                        
                        // var thisSession = JSON.stringify(data);
                        if(data.includes('is required')) {
                            $(curr_elem).closest('form').find('.alerts-danger').html('');
                            $(curr_elem).closest('form').find('.alerts-danger').show();
                            $(curr_elem).closest('form').find('.alerts-danger').html(data);
                            // $(".todo-addDescribe").fadeOut();
                        } else {
                            //$(curr_elem).closest('.folder_info').html(data);
                            $(curr_elem).closest('.folder-'+folder_id).find('.options-secD:first').html(data);
                            $('.reset_btn').trigger("click");    
                        }
                    },
                    error: function(xhr, error){
                      console.debug(xhr); console.debug(error);
                    },

                });


            }
        });
        
       
    }); 

    // update the task changes - Edit task

    $(document).on('click', '.edit_task_btn_list', "click",function(e){
        var curr_elem = $(this);
        var form = $(this).closest('.edit_task_frm_list') ;
        //e.preventDefault();
        $(this).closest(".edit_task_frm_list").validate({
            rules: {
                task_name: {
                    required: true,
                }
            },
            submitHandler: function(form) {
                var task_id = $(form).find('.task_id').val();
                var folder_id = $(form).find('.folder_id').val();
                var datastring = $(form).serialize();
                //var datastring = new FormData(form);
                // return false;
                $.ajax({
                    type: "POST",
                    url: baseURL+"/update_task",
                    //enctype: 'multipart/form-data',
                    //processData: false,  // Important!
                    //contentType: false,
                    //cache: false,
                    data: datastring,
                    dataType: "html",
                    success: function(data){
                        // var thisSession = JSON.stringify(data);
                        if(data.includes('is required')){
                            $(curr_elem).closest('form').find('.alerts-danger').html('');
                                $(curr_elem).closest('form').find('.alerts-danger').show();
                                $(curr_elem).closest('form').find('.alerts-danger').html(data);
                        }else{
                            //$(curr_elem).closest('.folder_info').html(data);
                            $(curr_elem).closest('.folder-'+folder_id).find('.options-secD:first').html(data);
                            $('.reset_btn').trigger("click");    
                        }
                    },
                    // success: function(data){  
                    //     $(curr_elem).closest('.task-li-'+task_id).html(data);
                    //     $(curr_elem).closest('.edit-task-cancel').trigger("click");
                    // },
                    error: function(error){
                    //  console.debug(xhr); console.debug(error);
                    },

                });
            }

        });
        
       
    }); 

}) // on ready function close




$(document).ready(function(){
    $('.todo-addNewList').css('cursor','pointer');
    $('.middle-arg').on('click', ".todo-addNewList", function(e){
        e.preventDefault()
        $(".todo-newList").fadeIn();
    });
    $(document).on('click', ".todo-cancel", function(){
        $(".todo-newList").fadeOut();
    });
    $(document).on('click', ".todolist-actionsBtn button", function(){
        $(this).next(".todo-addDescribe").fadeIn();
    });
    $(document).on('click', ".addDescribe-cancel", function(){
        $(this).closest(".todo-addDescribe").fadeOut();
    });
    $(document).on('click', ".prompt-commentbtn", function(){
        $(this).hide();
        $(".form-commetBox").fadeIn();
    });

    $(document).on('click', ".todo-addPeople", function(e){
        e.preventDefault();
        $(".todo-newPeople").fadeIn();
    });
    $(document).on('click', ".people-cancel", function(){
        $(".todo-newPeople").fadeOut();
    });

    $(document).on('click', ".task_name", function(e){
        e.preventDefault();

        $item = $(this).parents().next(".edit_task_section") ;
   
        //$(this).parents().next(".edit_task_section").fadeIn();
        $(this).parents().next(".edit_task_section").toggle();

        $(".options-secs").find(".edit_task_section").not( $item ).hide();
        return false;

    });
    $(document).on('click', ".edit-task-cancel", function(){
        $(this).closest(".edit_task_section").fadeOut();
    });

    $(document).on('click', ".comment-cancel", function(){
        $('.prompt-commentbtn').show();
        $(".form-commetBox").hide();
    });

   

}) ;

$(document).on('click', '.checkbox__input', function(){
    var curr_elem = $(this);
    var task_id = $(this).attr('data-taskid');
    var folder_id = $(this).attr('folder-id');

    if($(this).is(":checked")) {
        var check_type = "checked";
    }else{
        var check_type = "un-checked";
    }

    $.ajax({
        type: "POST",
        url: baseURL+"/update_task_status",
        data: {task_id: task_id,folder_id: folder_id, check_type: check_type},
        dataType: "html",
        success: function(data){
            //$(curr_elem).closest('.folder_info').html(data);
            $(curr_elem).closest('.folder-'+folder_id).html(data);
        },
        error: function(error){            
        },

    });
});

// mark complete task on listing page by clicking the "Done" button
$(document).on('click', '.complete_task_btn', function(e){
    var curr_elem = $(this);
    var task_id = $(this).attr('data-taskid');
    var folder_id = $(this).attr('folder-id');
    
    var check_type = "checked";
    e.preventDefault();

    $.ajax({
        type: "POST",
        url: baseURL+"/update_task_status",
        data: {task_id: task_id,folder_id: folder_id, check_type: check_type},
        dataType: "html",
        success: function(data){
            //$(curr_elem).closest('.folder_info').html(data);
            $(curr_elem).closest('.folder-'+folder_id).find('.options-secD:first').html(data);
        },
        error: function(error){
            
        },

    });
});

$(document).on('click', '.delete_task', function(e){
    var task_url = $(this).attr('href');
    var task_id = $(this).attr('data-taskid');
    var folder_id = $(this).attr('folder-id');
    var curr_elem = $(this);

    e.preventDefault();
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Task!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if(willDelete) {
            $.ajax({
                url: task_url,
                type: "POST",
                data: {task_id: task_id},
                dataType: "html",
                success: function (data) {
                    //$(curr_elem).closest('.folder_info').html(data);
                    $(curr_elem).closest('.folder-'+folder_id).find('.options-secD:first').html(data);
                }
            });
            
        } else {
            // swal("Your Task is safe!");
        }
    });
})


$(document).on('click', '.reopen_task_btn_list', function(e) {
    var task_url = $(this).attr('href');
    var task_id = $(this).attr('data-taskid');
    var folder_id = $(this).attr('folder-id');
    var curr_elem = $(this);

    e.preventDefault();

    $.ajax({
        url: baseURL+"/update_task_status",
        type: "POST",
        data: {task_id: task_id, folder_id: folder_id, check_type: 'uncheked'},
        dataType: "html",
        success: function(data) {
            //$(curr_elem).closest('.folder_info').html(response);
            $(curr_elem).closest('.folder-'+folder_id).find('.options-secD:first').html(data);
        }
    });
    
})


// start comment section

// add comment

$(document).on('click', '.add_comment_btn', function(e){
    e.preventDefault();
    var curr_elem = $(this);

    var comment_url = $(this).attr('href');
    var task_id = $(this).closest('.add_comment_form').find('.task_id').val();
    var user_id = $(this).closest('.add_comment_form').find('.logged_user_id').val();
    var folder_id = $(this).closest('.add_comment_form').find('.folder_id').val();
    var datastring = $(this).closest('.add_comment_form').serialize();

    //$(curr_elem).css("pointer-events", "none");

    $.ajax({
        type: "POST",
        url: comment_url,
        data: datastring,
        dataType: "html",
        success: function(data){
            // var thisSession = JSON.stringify(data);
            if(data.includes('is required')){
                $(curr_elem).closest('form').find('.alerts-danger').html('');
                    $(curr_elem).closest('form').find('.alerts-danger').show();
                    $(curr_elem).closest('form').find('.alerts-danger').html(data);
            }else{
                
                $(curr_elem).closest('.comment_section').html(data);
                  
            }
        },
        
        error: function(error){
          //  console.debug(xhr); console.debug(error);
        },

      });

      
});

// on click edit comment show editable comment form and hide display comment form

$(document).on('click', '.edit_comment', function(e){
    e.preventDefault();
    var curr_elem = $(this);
    
    $(this).closest('.row').next('.edit_comment_form_section').show();
    $(this).closest('.comment_display_section').hide();
    

});

// on click edit comment show editable comment form and hide display comment form

$(document).on('click', '.edit-comment-cancel', function(e){
    e.preventDefault();
    var curr_elem = $(this);
    
    $(this).closest('.row').prev('.comment_display_section').show();
    $(this).closest('.edit_comment_form_section').hide();
    
    

});


// update comment 

$(document).on('click', '.edit_comment_btn', function(e){
    var curr_elem = $(this);
    e.preventDefault();
    var comment_id = $(this).closest('.edit_comment_form').find('.comment_id').val();
    var task_id = $(this).closest('.edit_comment_form').find('.task_id').val();
    var folder_id = $(this).closest('.edit_comment_form').find('.folder_id').val();
    var datastring = $(this).closest('.edit_comment_form').serialize();
    // return false;
    $.ajax({
        type: "POST",
        url: baseURL+"/update_comment",
        data: datastring,
        dataType: "html",
        success: function(data){
            // var thisSession = JSON.stringify(data);
            if(data.includes('is required')){
                $(curr_elem).closest('form').find('.alerts-danger').html('');
                    $(curr_elem).closest('form').find('.alerts-danger').show();
                    $(curr_elem).closest('form').find('.alerts-danger').html(data);
            }else{
                //$(curr_elem).closest('.folder_info').html(data);
                $('.comment_section').html(data);
                //$('.reset_btn').trigger("click");    
            }
        },
        error: function(error){
          //  console.debug(xhr); console.debug(error);
        },

      });
   
}); 


// delete comment

$(document).on('click', '.delete_comment', function(e){
    var curr_elem = $(this);
    e.preventDefault();
    var comment_id = $(this).attr('comment_id');
    var task_id = $(this).attr('task_id');
    
    var delete_url = $(this).attr('href');
    
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Comment!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url: delete_url,
                type: "POST",
                data: { comment_id: comment_id, task_id:task_id },
                dataType: "html",
                success: function (data) {
                    $('.comment_section').html(data);
                }
            });
            
        } else {
            // swal("Your Task is safe!");
        }
    });
   
}); 



// start edit the single task on task detail/comment page

$(document).ready(function(){
//$(document).on('click','.edit_task_icon', function(e){
    $('.edit_task_icon').click(function(e){
    e.preventDefault();
    $('.task_disply_form_only').hide();
    $('.edit_task_frm_single_div').show();
})
})

$(document).on('click','.edit-task-single-cancel', function(e){
    e.preventDefault();
    $('.task_disply_form_only').show();
    $('.edit_task_frm_single_div').hide();
})

$(document).on('click', '.edit_task_btn_single', function(e){
    var curr_elem = $(this);
    var form = $(this).closest('.edit_task_frm_single');

    $(this).closest(".edit_task_frm_single").validate({
        rules: {
            task_name: {
                required: true,
            }
        },

        submitHandler: function (form) 
        {
            var task_id = $(form).find('.task_id').val();
            var folder_id = $(form).find('.folder_id').val();
            var datastring = $(form).closest('.edit_task_frm_single').serialize();
            //var datastring = new FormData(form);

            $.ajax({
                type: "POST",
                url: baseURL+"/update_task_single",
                //enctype: 'multipart/form-data',
                //processData: false,  // Important!
                //contentType: false,
                //cache: false,
                data: datastring,
                dataType: "html",
                success: function(data){
                    // var thisSession = JSON.stringify(data);
                    if(data.includes('is required')){
                        $(curr_elem).closest('form').find('.alerts-danger').html('');
                            $(curr_elem).closest('form').find('.alerts-danger').show();
                            $(curr_elem).closest('form').find('.alerts-danger').html(data);
                    }else{
                        //$(curr_elem).closest('.folder_info').html(data);
                        $('.single_task_section').html(data);
                        //$('.reset_btn').trigger("click");    
                    }
                },
                error: function(error){
                  //  console.debug(xhr); console.debug(error);
                },
        
              });
        }

    })
    
   
}); 

$(document).on('click', '.delete_task_single', function(e){
    var task_url = $(this).attr('href');
    var task_id = $(this).attr('data-taskid');
    var folder_id = $(this).attr('folder-id');
    var curr_elem = $(this);
    e.preventDefault();

    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Task!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url: task_url,
                type: "POST",
                data: { task_id: task_id},
                dataType: "html",
                success: function (data) {
                    //$(curr_elem).closest('.folder_info').html(data);
                    //$(curr_elem).closest('.folder-'+folder_id).html(data);
                    window.location.href = baseURL+'/tasks';
                }
            });
            
        } else {
            // swal("Your Task is safe!");
        }
    });
})

// start function for update the status of single task on comment page

$(document).on('click', '.update_status_single_task', function(e){
    e.preventDefault();
    var curr_elem = $(this);
    var task_id = $(this).attr('data-taskid');
    var folder_id = $(this).attr('folder-id');
    var check_type = $(this).attr('check-type');
    

    $.ajax({
        type: "POST",
        url: baseURL+"/update_task_status_single",
        data: {task_id: task_id,folder_id: folder_id, check_type: check_type},
        dataType: "html",
        success: function(data){
            $('.single_task_section').html(data);
            //$(curr_elem).closest('.folder-'+folder_id).html(data);
        },
        error: function(error){
            
        },

    });
});


// start code for edit folder name

$(document).ready(function() {
    $(".options-secs").on('click','.edit_folder_btn',function(e) {
        e.preventDefault();
        //var form = $(this).parents('.optionsSec-header').find('.edit_folder_frm').show() ;
        var form = $(this).parents('.options-secD').find('.edit_folder_frm').show() ;
        $(this).parents('.options-secD').find('.edit_folder_frm').find('.folder_name').focus();
    });

    $(".options-secs").on('click','.edit-folder-cancel',function(e){
        e.preventDefault();
        var form = $(this).parents('.options-secD').find('.edit_folder_frm').hide() ;
        //form.show();
    });

    $(document).on('click', '.edit_this_folder',function(e) {
        var curr_elem = $(this);
        var form = $(this).closest('.edit_folder_frm');
        //e.preventDefault();
        $(this).closest(".edit_folder_frm").validate({
            rules: {
                name: {
                    required: true,
                }
            },

            submitHandler: function(form) {
                var folder_id = $(form).find('.folder_id').val();
                var datastring = $(form).serialize();
                console.log(datastring);
                //return false;
                $.ajax({
                    type: "POST",
                    url: baseURL+"/update_folder",
                    data: datastring,
                    dataType: "html",
                    success: function(data) {                        
                        if(data.includes('is required')) {                            
                        } else {                            
                            $(curr_elem).closest('.folder-'+folder_id).find('.options-secD:first').html(data);
                            //$('.reset_btn').trigger("click");    
                        }
                    },
                    
                    error: function(error){
                    console.debug(error);
                    },

                });
            }

        });
        
    
    }); 

    // delete folder and all its tasks, comments

    $(document).on('click', '.delete_folder_btn', function(e){
        var delete_folder_url = $(this).attr('href');
        var folder_id = $(this).attr('folder_id');
        var curr_elem = $(this);
        e.preventDefault();
    
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Folder!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
            })
            .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: delete_folder_url,
                    type: "POST",
                    data: { folder_id: folder_id},
                    dataType: "html",
                    success: function (data) {
                        //$(curr_elem).closest('.folder_info').html(data);
                        //$(curr_elem).closest('.folder-'+folder_id).html(data);
                        window.location.href = baseURL+'/tasks';
                    }
                });
                
            } else {
                // swal("Your Task is safe!");
            }
        });
    })

    // go to previous page when click on 'Back' button

    $(".back_button").on("click", function(e){
        e.preventDefault();
        window.history.back();
    })

    //$("input.file").on("change", function(e){
        $(document).on("change","input.file", function(e){
       var el = e.target;
       if(el.files.length > 0) {
            formdata = new FormData();
            formdata.append("file", el.files[0]); 

            $.ajax({
                type: 'POST',
                url: baseURL+"/upload_file",
                data: formdata,
                processData: false,
                contentType: false,
                beforeSend: function() { 
                //$(el).closest(".btn").prop('disabled', true);
                },
                success: function(data) {
                    let res = JSON.parse(data);
                    console.log(res);
                    //$(el).closest(".btn").prop('disabled', false);

                    if(res.status == 200 ) {
                        $(el).next(".up_file").val(res.filename);
                    } else {
                        console.log("error : ",res);
                    }
                },
                error: function(jqXHR, exception) {
                    console.log('error final .............');
                    alert('<b>ERROR:</b> '+jqXHR.status+': '+jqXHR.responseText+': '+exception);
                }
            });
        }


    })

    // start function for save further assignment of task
    $(document).on('click', '.further_assign_btn', "click",function(e){
        var curr_elem = $(this);
        var form = $(this).closest('.further_assignment_frm') ;
        //e.preventDefault();
        $(this).closest(".further_assignment_frm").validate({
            rules: {
                further_assigned_to: {
                    required: true,
                }
            },
            submitHandler: function(form) {
                var task_id = $(form).find('.task_id').val();
                var folder_id = $(form).find('.folder_id').val();
                var datastring = $(form).serialize();
                //var datastring = new FormData(form);
                // return false;
                $.ajax({
                    type: "POST",
                    url: baseURL+"/further_assign_task",
                    
                    data: datastring,
                    dataType: "html",
                    success: function(data){
                        // var thisSession = JSON.stringify(data);
                        if(data.includes('is required')){
                            $(curr_elem).closest('form').find('.alerts-danger').html('');
                                $(curr_elem).closest('form').find('.alerts-danger').show();
                                $(curr_elem).closest('form').find('.alerts-danger').html(data);
                        }else{
                            //$(curr_elem).closest('.folder_info').html(data);
                            $(curr_elem).closest('.folder-'+folder_id).find('.options-secD:first').html(data);
                            $('.reset_btn').trigger("click");    
                        }
                    },
                    // success: function(data){  
                    //     $(curr_elem).closest('.task-li-'+task_id).html(data);
                    //     $(curr_elem).closest('.edit-task-cancel').trigger("click");
                    // },
                    error: function(error){
                    //  console.debug(xhr); console.debug(error);
                    },

                });
            }

        });
        
       
    });
    // end function for save further assignment of task

}); 






