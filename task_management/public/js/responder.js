$(document).ready(function() {
	
	calcWidth($('#title0'));

	window.onresize = function(event) {
    	//console.log("window resized");
    	//method to execute one time after a timer
		calcWidth($('#title0'));
    };

	//recursively calculate the Width all titles
	function calcWidth(obj){
		//console.log('---- calcWidth -----');

		var titles = $(obj).siblings('.space').children('.drag-route').children('.title');
		$(titles).each(function(index, element){
			var pTitleWidth = parseInt($(obj).css('width'));
			var leftOffset = parseInt($(obj).siblings('.space').css('margin-left'));
			var newWidth = pTitleWidth - leftOffset;

			if ($(obj).attr('id') == 'title0'){
				//console.log("called");
				newWidth = newWidth - 0;
			}

			$(element).css({
				'width': newWidth,
			})

			calcWidth(element);
		});
	}


	$('.space').sortable({
		connectWith:'.space',
		handle:'.drag-handler',
		// placeholder: ....,
		tolerance:'intersect',
		change: function(event, ui) {
			ui.placeholder.css({visibility: 'visible', background: '#f3f283'});
		},
		over:function(event,ui) {			
			// //Recaculate width of all children
			// var pTitleWidth = parseInt($(this).siblings('.title').css('width').replace('px', ''));
			
			// if ($(this).siblings('.title').attr('id') == 'title0'){
			// 	var newWidth = (pTitleWidth-20).toString().concat('px');
			// }
			// else {
			// 	var newWidth = (pTitleWidth-70).toString().concat('px');
			// }
			
			// console.log(pTitleWidth + ', ' + newWidth);

			// $(ui.item).children('.title').css({
			// 	'width': newWidth,
			// });
		},
		receive:function(event, ui) {
			calcWidth($(this).siblings('.title'));

			//console.log($(this));
		},
		stop:function(event, ui) {
			// Save folder order
			var dropped_folder_id = parseInt(ui.item[0].getAttribute('data-fid'));
			var parent_folder_id = '';
			var order = [];
			if($(ui.item).closest('ul').closest('li').data('fid')) {
				parent_folder_id = $(ui.item).closest('ul').closest('li').data('fid')
			} else {
				var all_prev_li = Array.prototype.reverse.call($(ui.item).prevAll('li'));
				var all_next_li = $(ui.item).nextAll('li');
				if(all_prev_li.length > 0) {
					$.each(all_prev_li, function(k,v) {
						order.push($(v).data('fid'));
					});
				}
				order.push(dropped_folder_id);
				if(all_next_li.length > 0) {
					$.each(all_next_li, function(k,v) {
						order.push($(v).data('fid'));
					});
				}
			}
			var child_obj = $(ui.item).closest('ul').children('li');
			var child_folder_ids_after_dropped = [];
			$.each(child_obj, function(i,v){
				child_folder_ids_after_dropped.push($(v).data('fid'));
			});
			order = (order.length > 0) ? order : '';
			$.ajax({
				type: 'POST',
				url: baseURL+'/rearrange_folders',
				data: {parent: parent_folder_id, child: dropped_folder_id, children: child_folder_ids_after_dropped, order: order},
				success: function(data) {
					//console.log(data);
					if(data) {
						ganttChartData();
						console.log('placed');
					} else {
						console.log('Not Placed');
					}
				},
				error: function(xhr, error) {
					console.log(xhr, error);
				}
			});
		}
	});
	
	//$('.space').disableSelection();

/**
 * Gantt Chart display
 */
	function ganttChartData() {
		var gantt_chart_div = document.getElementById('GanttChartDIV');

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			url: baseURL+'/chart_data',
			method: 'get',
			beforeSend: function() {
				// gantt_chart_div.innerHTML = '<div class="cloader"></div>';
			},
			success: function(res) {                            
				if(res.success) {
					// here's all the html code neccessary to display the chart object
					// Future idea would be to allow XML file name to be passed in and chart tasks built from file.
					gChart = new IMGantt.GanttChart('gChart',gantt_chart_div, 'day'); // day, week, month

					gChart.setShowRes(1); // Show/Hide Responsible (0/1)
					gChart.setShowDur(1); // Show/Hide Duration (0/1)
					gChart.setShowComp(1); // Show/Hide % Complete(0/1)
					gChart.setCaptionType('Resource');  // Set to Show Caption (None,Caption,Resource,Duration,Complete)

					//var gr = new Graphics();
					if(gChart) {
						// Parameters (pID, pName, pStart, pEnd, pColor, pLink, pMile, pRes, pComp, pGroup, pParent, pOpen, pDepend, pCaption)
						// You can also use the XML file parser IMGantt.parseXML('project.xml',gChart)
						$.each(res.data, function(i,v){
							console.log(i,"==>",v);
							gChart.AddTaskItem(new IMGantt.TaskItem(v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7], v[8], v[9], v[10], v[11], v[12], v[13]));
						});

						/*gChart.AddTaskItem(new IMGantt.TaskItem(1,   'Define Chart API',     '',           '',                'ff0000', 'http://google.com',    0, 'Brian',     0, 1, 0, 1, ''));
						gChart.AddTaskItem(new IMGantt.TaskItem(100,   'Define Chart API 2',     '',           '',                'ff0000', 'http://google.com',    0, 'Brian',     0, 1, 1, 1, ''));
						gChart.AddTaskItem(new IMGantt.TaskItem(11,  'Chart Object',         '7/20/2008',  '7/20/2008',       'ff00ff', 'http://www.yahoo.com', 0, 'Shlomy',    100, 0, 1, 1));
						gChart.AddTaskItem(new IMGantt.TaskItem(12,  'Task Objects',         '',           '',                '00ff00', '',                     0, 'Shlomy',    40, 1, 100, 1));
						gChart.AddTaskItem(new IMGantt.TaskItem(121, 'Constructor Proc',     '7/21/2008',  '8/9/2008',        '00ffff', 'http://www.yahoo.com', 0, 'Brian T.',  60, 0, 12, 1));
						gChart.AddTaskItem(new IMGantt.TaskItem(122, 'Task Variables',       '8/6/2008',   '8/11/2008',       'ff0000', 'http://google.com',    0, 'Brian',     60, 0, 12, 1,121));
						gChart.AddTaskItem(new IMGantt.TaskItem(123, 'Task by Minute/Hour',  '8/6/2008',   '8/11/2008 12:00', 'ffff00', 'http://google.com',    0, 'Ilan',      60, 0, 12, 1,121));
						gChart.AddTaskItem(new IMGantt.TaskItem(124, 'Task Functions',       '8/9/2008',   '8/29/2008',       'ff0000', 'http://google.com',    0, 'Anyone',    60, 0, 12, 1, 0, 'This is another caption'));
						gChart.AddTaskItem(new IMGantt.TaskItem(2,   'Create HTML Shell',    '8/24/2008',  '8/25/2008',       'ffff00', 'http://google.com',    0, 'Brian',     20, 0, 0, 1,122));
						gChart.AddTaskItem(new IMGantt.TaskItem(3,   'Code Javascript',      '',           '',                'ff0000', 'http://google.com',    0, 'Brian',     0, 1, 0, 1));
						gChart.AddTaskItem(new IMGantt.TaskItem(31,  'Define Variables',     '7/25/2008',  '8/17/2008',       'ff00ff', 'http://google.com',    0, 'Brian',     30, 0, 3, 1, '','Caption 1'));
						gChart.AddTaskItem(new IMGantt.TaskItem(32,  'Calculate Chart Size', '8/15/2008',  '8/24/2008',       '00ff00', 'http://google.com',    0, 'Shlomy',    40, 0, 3, 1));
						gChart.AddTaskItem(new IMGantt.TaskItem(33,  'Draw Taks Items',      '',           '',                '00ff00', 'http://google.com',    0, 'Someone',   40, 1, 3, 1));
						gChart.AddTaskItem(new IMGantt.TaskItem(332, 'Task Label Table',     '8/6/2008',   '8/11/2008',       '0000ff', 'http://google.com',    0, 'Brian',     60, 0, 33, 1));
						gChart.AddTaskItem(new IMGantt.TaskItem(333, 'Task Scrolling Grid',  '8/9/2008',   '8/20/2008',       '0000ff', 'http://google.com',    0, 'Brian',     60, 0, 33, 1));
						gChart.AddTaskItem(new IMGantt.TaskItem(34,  'Draw Task Bars',       '',           '',                '990000', 'http://google.com',    0, 'Anybody',   60, 1, 3, 0));
						gChart.AddTaskItem(new IMGantt.TaskItem(341, 'Loop each Task',       '8/26/2008',  '9/11/2008',       'ff0000', 'http://google.com',    0, 'Brian',     60, 0, 34, 1));
						gChart.AddTaskItem(new IMGantt.TaskItem(342, 'Calculate Start/Stop', '9/12/2008',  '10/18/2008',      'ff6666', 'http://google.com',    0, 'Brian',     60, 0, 34, 1));
						gChart.AddTaskItem(new IMGantt.TaskItem(343, 'Draw Task Div',        '10/13/2008', '10/17/2008',      'ff0000', 'http://google.com',    0, 'Brian',     60, 0, 34, 1));
						gChart.AddTaskItem(new IMGantt.TaskItem(344, 'Draw Completion Div',  '10/17/2008', '11/04/2008',      'ff0000', 'http://google.com',    0, 'Brian',     60, 0, 34, 1,"342,343"));
						gChart.AddTaskItem(new IMGantt.TaskItem(35,  'Make Updates',         '12/17/2008', '2/04/2009',       'f600f6', 'http://google.com',    0, 'Brian',     30, 0, 3,  1));*/
						
						gChart.Draw();	
						gChart.DrawDependencies();
					} else {
						console.log("not defined");
					}
				} else {
					console.log('error!');
				}                            
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(textStatus, errorThrown);
			}
		});
	}

	ganttChartData();
});