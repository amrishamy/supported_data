var baseURL = "http://taskmanagement.urtestsite.com/public";

$(document).ready(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });



    
    // some default hidden elements
    $('.add_folder_section').hide();
    $('.add_task_form').hide();
    

    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
    }, 5000);

    $("#add_user_frm").validate();
    $("#add_folder_frm").validate();
    $(".add_task_frm_list").validate();
    $(".add_task_frm").validate();
    

    $(document).on('click', '.reset_btn', function(){
        $(this).closest('form').find('.alerts-danger').html('');
    })

    $('.new_folder_btn').on("click",function(e){
        e.preventDefault();
        $('.add_folder_section').show();
       
    });

    $('.hide_add_folder_section').on("click",function(e){
        e.preventDefault();
        $('.add_folder_section').hide();
       
    });

    /* $('.add_this_folder').on("click",function(e){
        e.preventDefault();
        var folder_name = $('.folder_name').val();
        //var datastring = "name="+folder_name;
        var datastring = $('.add_folder_frm').serialize();
        console.log(datastring);
        // return false;
        $.ajax({
            type: "POST",
            url: "foldercreate",
            data: datastring,
            dataType: "json",
            success: function(data){
                if(data.status=="okay")
                window.location.href = "";

              return true;
            },
            error: function(xhr, error){
                console.debug(xhr); console.debug(error);
            },

          });
       
    }); */

    // ***************************************start for task module*******************

    // add task using ajax

    $(document).on('click', '.add_task_btn', "click",function(e){
        var curr_elem = $(this);
        e.preventDefault();
        var datastring = $(this).closest('.add_task_frm').serialize();
        console.log(datastring);
        $.ajax({
            type: "POST",
            url: baseURL+"/tasksave",
            data: datastring,
            dataType: "html",
            success: function(data){  
                var thisSession = JSON.parse(data);
                if(thisSession.hasOwnProperty('errors')){
                    $(curr_elem).closest('form').find('.alerts-danger').html('');
                    jQuery.each(thisSession.errors, function(key, value){
                        $(curr_elem).closest('form').find('.alerts-danger').show();
                        $(curr_elem).closest('form').find('.alerts-danger').append('<p>'+value+'</p>');
                    });
                }else{
                    $(curr_elem).closest('.folder_info').html(data.success);
                    $('.reset_btn').trigger("click");    
                }
            },
            error: function(error){
              //  console.debug(xhr); console.debug(error);
            },

          });
       
    }); 

    $(document).on('click', '.add_task_btn_list', "click",function(e){
        var curr_elem = $(this);
        e.preventDefault();
        var datastring = $(this).closest('.add_task_frm_list').serialize();
        // return false;
        $.ajax({
            type: "POST",
            url: baseURL+"/save_task",
            data: datastring,
            dataType: "html",
            success: function(data){
                
                // var thisSession = JSON.stringify(data);
                if(data.includes('is required')){
                    $(curr_elem).closest('form').find('.alerts-danger').html('');
                        $(curr_elem).closest('form').find('.alerts-danger').show();
                        $(curr_elem).closest('form').find('.alerts-danger').html(data);
                }else{
                    $(curr_elem).closest('.folder_info').html(data);
                    $('.reset_btn').trigger("click");    
                }
            },
            error: function(error){
              //  console.debug(xhr); console.debug(error);
            },

          });
       
    }); 

    // update the task changes - Edit task

    $(document).on('click', '.edit_task_btn_list', "click",function(e){
        var curr_elem = $(this);
        e.preventDefault();
        var task_id = $(this).closest('.edit_task_frm_list').find('.task_id').val();
        var datastring = $(this).closest('.edit_task_frm_list').serialize();
        // return false;
        $.ajax({
            type: "POST",
            url: baseURL+"/update_task",
            data: datastring,
            dataType: "html",
            success: function(data){
                // var thisSession = JSON.stringify(data);
                if(data.includes('is required')){
                    $(curr_elem).closest('form').find('.alerts-danger').html('');
                        $(curr_elem).closest('form').find('.alerts-danger').show();
                        $(curr_elem).closest('form').find('.alerts-danger').html(data);
                }else{
                    $(curr_elem).closest('.folder_info').html(data);
                    $('.reset_btn').trigger("click");    
                }
            },
            // success: function(data){  
            //     $(curr_elem).closest('.task-li-'+task_id).html(data);
            //     $(curr_elem).closest('.edit-task-cancel').trigger("click");
            // },
            error: function(error){
              //  console.debug(xhr); console.debug(error);
            },

          });
       
    }); 

})




$(document).ready(function(){
    $(document).on('click', ".todo-addNewList", function(){
        $(".todo-newList").fadeIn();
    });
    $(document).on('click', ".todo-cancel", function(){
        $(".todo-newList").fadeOut();
    });
    $(document).on('click', ".todolist-actionsBtn button", function(){
        $(this).next(".todo-addDescribe").fadeIn();
    });
    $(document).on('click', ".addDescribe-cancel", function(){
        $(this).closest(".todo-addDescribe").fadeOut();
    });
    $(document).on('click', ".prompt-commentbtn", function(){
        $(this).hide();
        $(".form-commetBox").fadeIn();
    });

    $(document).on('click', ".todo-addPeople", function(e){
        e.preventDefault();
        $(".todo-newPeople").fadeIn();
    });
    $(document).on('click', ".people-cancel", function(){
        $(".todo-newPeople").fadeOut();
    });

    $(document).on('click', ".task_name", function(e){
        e.preventDefault();
       
        $(this).parents().next(".edit_task_section").fadeIn();
    });
    $(document).on('click', ".edit-task-cancel", function(){
        $(this).closest(".edit_task_section").fadeOut();
    });

}) ;

$(document).on('click', '.checkbox__input', function(){
    var curr_elem = $(this);
    var task_id = $(this).attr('data-taskid');
    var folder_id = $(this).attr('folder-id');
    if($(this).is(":checked")) {
        var check_type = "checked";
    }else{
        var check_type = "un-checked";
    }

    $.ajax({
        type: "POST",
        url: baseURL+"/update_task_status",
        data: {task_id: task_id,folder_id: folder_id, check_type: check_type},
        dataType: "html",
        success: function(data){
            $(curr_elem).closest('.folder_info').html(data);
        },
        error: function(error){
            
        },

    });
});

$(document).on('click', '.delete_task', function(e){
    var task_url = $(this).attr('href');
    var task_id = $(this).attr('data-taskid');
    var curr_elem = $(this);
    e.preventDefault();

    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Task!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
        })
        .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url: task_url,
                type: "POST",
                data: { task_id: task_id},
                dataType: "html",
                success: function (response) {
                    $(curr_elem).closest('.folder_info').html(response);
                    swal("Done! Your Task has been deleted!", {
                        icon: "success",
                    });
                }
            });
            
        } else {
            swal("Your Task is safe!");
        }
    });
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this Task!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     closeOnConfirm: false
    // }, function (isConfirm) {
    //     if (!isConfirm) return;
    //     $.ajax({
    //         url: task_url,
    //         type: "POST",
    //         data: { task_id: task_id},
    //         dataType: "html",
    //         success: function () {
    //             $(curr_elem).closest('.folder_info').html(data);
    //             swal("Done!", "It was succesfully deleted!", "success");
    //         },
    //         error: function (xhr, ajaxOptions, thrownError) {
    //             swal("Error deleting!", "Please try again", "error");
    //         }
    //     });
    // });
})

// $(document).on('click', '.task_name', function(e){
//     e.preventDefault();
//     var curr_elem = $(this);
//     alert("kjh");
// });



