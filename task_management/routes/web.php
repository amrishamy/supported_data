<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['login']], function() {
Route::get('/', function () {
    return view('auth/login');
    });
});
Route::group(['middleware' => ['login']], function() {
    Route::get('/login', function () {
        return view('auth/login');
    });
});
//Route::group(['middleware'=>'auth','prefix'=>'/'],function () {
    
    ///Route::get('/users', 'User\UserController@index')->name('users');
//});

Route::any('/retrieve_emails', 'TestController@retrieve_emails');

Auth::routes();
Route::post('password/sendemail','Auth\ForgotPasswordController@sendResetPassword')->name('password.sendemail');
Route::post('password/updatepassword','Auth\ForgotPasswordController@updateuserpassword')->name('password.updatepass');
//Routes for users
Route::get('/home', 'HomeController@index')->name('home');


/* Route::get('/users', 'User\UserController@index')->name('users');
Route::get('/create', 'User\UserController@create')->name('create');
Route::get('/edit/{id}', 'User\UserController@edit')->name('edit');
Route::post('/update', 'User\UserController@update')->name('update');

Route::get('/delete/{id}', 'User\UserController@delete')->name('delete');
Route::post('/userInvitation', 'User\UserController@userInvitation');
Route::any('/userVerify', 'User\UserController@userVerify')->name('userVerify'); */

Route::group(['middleware' => ['auth', 'admin']], function() {

    // users routes
    Route::get('/users', 'User\UserController@index')->name('users');
    Route::get('/create', 'User\UserController@create')->name('create');
    Route::get('/edit/{id}', 'User\UserController@edit')->name('edit');
    Route::post('/update', 'User\UserController@update')->name('update');

    Route::get('/delete/{id}', 'User\UserController@delete')->name('delete');
    Route::post('/userInvitation', 'User\UserController@userInvitation');
    Route::any('/userVerify', 'User\UserController@userVerify')->name('userVerify');

    Route::get('/reset_password/{id}', 'User\UserController@reset_password')->name('reset_password');

    Route::get('/send_link_to_pending_user/{id}', 'User\UserController@send_link_to_pending_user')->name('send_link_to_pending_user');

    // folders routes
    Route::get('/folder/{id}', 'Folder\FolderController@detail')->name('folderdetail');
    Route::get('/folders', 'Folder\FolderController@index')->name('folders');
    Route::get('/foldercreate', 'Folder\FolderController@create')->name('foldercreate');
    Route::POST('/foldercreate', 'Folder\FolderController@save')->name('foldersave');
    Route::get('/folderedit/{id}', 'Folder\FolderController@edit')->name('folderedit');
    Route::post('/update_folder', 'Folder\FolderController@update_folder')->name('update_folder');
    Route::post('/delete_folder', 'Folder\FolderController@delete_folder')->name('delete_folder');
    Route::post('/rearrange_folders', 'Folder\FolderController@rearrange_folders')->name('rearrange_folders');
});

Route::group(['middleware' => ['auth']], function() {
    // Routes for tasks
    Route::get('/tasks', 'Task\TaskController@index')->name('tasks');
    Route::get('/taskcreate', 'Task\TaskController@create')->name('taskcreate');
    Route::get('/taskedit', 'Task\TaskController@edit')->name('taskedit');

    Route::post('/tasksave', 'Task\TaskController@save')->name('tasksave');
    Route::post('/save_task', 'Task\TaskController@save_task')->name('save_task');
    Route::post('/reopen_task', 'Task\TaskController@reopen_task')->name('reopen_task');
    Route::post('/update_task', 'Task\TaskController@update_task')->name('update_task');
    Route::post('/update_task_status', 'Task\TaskController@update_task_status')->name('update_task_status');
    Route::post('/update_task_status_single', 'Task\TaskController@update_task_status_single')->name('update_task_status_single');
    Route::post('/delete_task', 'Task\TaskController@delete_task')->name('delete_task');

    Route::post('/update_task_single', 'Task\TaskController@update_task_single')->name('update_task_single');
    Route::post('/delete_task_single', 'Task\TaskController@delete_task_single')->name('delete_task_single');

    Route::get('/summary/excel', 'Task\TaskController@excel')->name('summary/excel');

    Route::post('/further_assign_task', 'Task\TaskController@further_assign_task')->name('further_assign_task');
    
    // task detail page
    Route::get('/task/{id}', 'Task\TaskController@task_detail')->name('task_detail');

    // comment Routes
    Route::post('/save_comment', 'Task\CommentsController@save_comment')->name('save_comment');
    Route::post('/update_comment', 'Task\CommentsController@update_comment')->name('update_comment');
    Route::post('/delete_comment', 'Task\CommentsController@delete_comment')->name('delete_comment');

    // route for file upload
    Route::post('/upload_file', 'Task\TaskController@upload_file')->name('upload_file');

    // Test
    Route::get('/test_file_transfer', 'TestController@file_transfer');
    Route::post('/test_file_receive', 'TestController@file_receive');
    Route::get('/new_task_list', 'Task\TaskController@new_tasks');

    // Ajax
    Route::get('/chart_data','Folder\FolderController@getGaintChartRecords');
});


