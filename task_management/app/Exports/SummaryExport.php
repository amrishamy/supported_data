<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

use \App\Http\Controllers\Task\TaskController;
use App\Task;
use App\Comment;
use App\Folder;
use App\User;
use DB;

class SummaryExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        
        $collection_arr = array();
        
        
        $task = new TaskController(new Folder,new Task,new Comment);
        $data = $task->exportable_summary();
        //echo "<pre>";print_r(  $data ) ;
        foreach($data as $key => $dta)
        {   
            if( count($dta['tasks']) > 0  )
            {
                //echo "<pre>";print_r(  $dta ) ;
                
                foreach( $dta['tasks'] as $task )
                {
                    //echo $task->name."<br>";
                  
                    $comment = $task->comments()->latest()->first();
                    //echo "<pre>";print_r($aa['comment']);
                    $latest_comment = $comment['comment'];
                    
                 
                   $single_arr = array(
                        "name"=> ($task->users) ? $task->users->name: 'Not Assigned', 
                        "folder"=>$dta['folder']->name,
                        "Task"=>$task->name, 
                        "comment"=> $latest_comment ,
                        "due_date"=> ( strtotime($task->due_date) > 0 ) ? date('Y-m-d',strtotime($task->due_date ) ) : '' , 
                        "creation_date"=>date('Y-m-d',strtotime($task->created_at ) ) 
                    );
                    $collection_arr[] = $single_arr; 
                }

                
            }
                                                       

                                                    

        }
        //die();

        return collect([
            $collection_arr
        ]);


    }


    public function headings(): array
    {
        return [
            'NAME',
            'FOLDER',
            'TASK',
            'LATEST COMMENT',
            'DUE DATE',
            'CREATION DATE',
        ];
    }

}
