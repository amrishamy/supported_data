<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Support\Carbon;

class Folder extends Model
{
    //
    public function tasks()
    {
        return $this->hasMany('App\Task', 'folder');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment','folder_id');
    }

    
    public function color(){
        //$count = $this->hasMany('App\Task', 'folder')->count();

        //$count = $this->hasMany('App\Task', 'folder')->where('tasks.assigned_to', '=', Auth::user()->id )->orWhere('tasks.created_by', '=', Auth::user()->id )->count();

        $count = $this->hasMany('App\Task', 'folder')->where(function ($query) {
            $query->where('tasks.assigned_to', '=', Auth::user()->id )
            ->orWhere('tasks.created_by', '=', Auth::user()->id );
        }) ->count();

        
        //dd($count);
        //echo Auth::user()->id."AAAB".$count; die;
        if($count > 0)
        {
            $assigned_to_count  =  $this->hasMany('App\Task', 'folder')->with(array('tasks' => function($query)
            {
                $query->where('tasks.assigned_to', '>', 0 );
            }))->count();
           
            
            $task = $this->hasMany('App\Task', 'folder')->where('status', 0 );

            if(Auth::user()->role != "Admin"){
              $task->where(function ($query) {
                  $query->where('tasks.assigned_to', '=', Auth::user()->id )
                  ->orWhere('tasks.created_by', '=', Auth::user()->id );
              });
            }

            $total_pending_tasks = $task->count();
            
            if($total_pending_tasks >= 1)
            {
               if ($assigned_to_count > 0 )
               {
                return 'red';
               }
               else
               {
                   return 'grey';
               }
            }
            else
            {
                if ($assigned_to_count > 0 )
               {
                return 'blue';
               }
               else
               {
                   return 'grey';
               }  
            }
                
            
        }
        else
        {
            //return 'white';
            return 'black';
        }
        
    }
}
