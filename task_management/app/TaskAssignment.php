<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskAssignment extends Model
{
    //
    public function usersa()
    {
        return $this->belongsTo('App\User', 'assigned_to');
    }
    
    public function usersb()
    {
        return $this->belongsTo('App\User', 'assigned_by');
    }
}
