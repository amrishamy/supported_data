<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Http\Controllers\Task\TaskController;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Auth;
use Mail;
use App\Task;
use App\Comment;
use App\Folder;
use App\User;

class HomeController extends Controller
{
    public $folderModel;
    public $taskModel;
    public $commentModel;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Folder $folderModel, Task $taskModel, Comment $commentModel)
    {
        $this->middleware('auth');

        $this->folderModel = $folderModel;
        $this->taskModel = $taskModel;
        $this->commentModel = $commentModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $task = new TaskController($this->folderModel,$this->taskModel,$this->commentModel);
        $data = $task->index();
        
        
        //return view('home');
        return view('home')->with(['folders_with_tasks'=> $data['folders_with_tasks'], 'user_list'=> $data['user_list'] ]);
    }
}
