<?php

namespace App\Http\Controllers\Task;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;

use Auth;
use Mail;
use App\Comment;
use App\Task;
use App\Folder;
use App\User;

class CommentsController extends Controller
{
    //

    private function comment_emails($comment, $task)
    {

        $mail_to = array();
        //echo "<pre>";print_r($task->assignments);die();
        foreach($task->assignments as $assigment){
            $mail_to[$assigment->assigned_by] = $assigment->assigned_by;
            $mail_to[$assigment->assigned_to] = $assigment->assigned_to;
        }
        //$mail_to[$task->assigned_to] = $task->assigned_to;
        //$mail_to[$task->created_by] = $task->created_by;
        
        
        if(isset($mail_to[ Auth::user()->id ]))
        unset($mail_to[  Auth::user()->id  ]);
        
        //echo "<pre>";print_r($mail_to);die();
        $login_url = URL::to('/');
        foreach($mail_to as $user_id){
            $user_info = User::find($user_id);
            if(isset($user_info))
            {
                $data = array(
                    'name'=>$user_info->name,
                    "email"=>$user_info->email,
                    "id"=>$user_info->id , 
                    "password"=>'123456',
                    "login_url"=>$login_url,
                    "task_id"=>$task->id,
                    "task_name"=>$task->name,
                    "comment_info"=>$comment 
                );
                
                //if(isset($comment['notify']) && $comment['notify'] )
                //{ 
                    Mail::send(['html'=>'add_comment_mail'], $data, 
                        function($mail) use ($user_info,$task) {
                            $mail->to( $user_info->email,  $user_info->name)
                                ->subject('Comment Posted On '.$task->name)
                                ->from('tasks@silentbeacon.com','Task Management Admin')
                                ->getHeaders()->addTextHeader('X-Amrish-Header', 'Just testing header');
                                //->from('admin@sandboxa6d179340e0c40acac738aa3ddc78a98.mailgun.org','Task Management Admin');
                        }
                    );
                //}
            }
        }

    }

    public function save_comment(Request $request)
    {   
        $validator = \Validator::make($request->all(), [
            'comment' => 'required',
        ]);
        
        if ($validator->fails()){
            $error = "";
            foreach($validator->errors()->all() as $key => $err){
                $error .= $err ."<br>";
            }   
            $error = str_replace('"',"",$error);
            return $error; 
        }else{
            $comment= new Comment();
            $comment->comment= $request['comment'];
            $comment->user_id= $request['logged_user_id'];
            $comment->folder_id= $request['folder_id'];
            $comment->task_id= $request['task_id'];
            //$comment->notify= $request['comment_notify'];
            
            // add other fields
            $comment->save();
            $comment_id = $comment->id;
            // send notify email to user for assigned task
            $task = Task::find($request['task_id']) ;
            
            $this->comment_emails($comment, $task );
                
                    

            // Generate view for replace current comment section
            $user_list = User::where('status', 'Active')->orderBy('name')->get();
            
            
            $folder = $request['folder_id'];
            $f_w_t =  Folder::find($folder);
            $user = User::find( Auth::user()->id );
            $task_comments =  Comment::where('task_id',$request['task_id'])->get() ;
            return view('tasks/ajax/comment_info', compact('task', 'user_list', 'task_comments','user'));
            //return view('tasks/detail');
        }
    }


    public function update_comment(Request $request)
    {   
        $validator = \Validator::make($request->all(), [
            'comment' => 'required',
        ]);
        
        if ($validator->fails()){
            $error = "";
            foreach($validator->errors()->all() as $key => $err){
                $error .= $err ."<br>";
            }   
            $error = str_replace('"',"",$error);
            return $error; 
        }else{
            $comment= Comment::find($request['comment_id']);
            $comment->comment= $request['comment'];
            //$comment->user_id= $request['logged_user_id'];
            //$comment->folder_id= $request['folder_id'];
            //$comment->task_id= $request['task_id'];
            //$comment->notify= $request['comment_notify'];
            
            // add other fields
            $comment->save();
            $comment_id = $comment->id;
            // send notify email to user for assigned task
            $task = Task::find($request['task_id']) ;

            $this->comment_emails($comment, $task );


            // Generate view for replace current comment section
            $user_list = User::where('status', 'Active')->orderBy('name')->get();
            
            
            $folder = $request['folder_id'];
            $f_w_t =  Folder::find($folder);
            $user = User::find( Auth::user()->id );
            $task_comments =  Comment::where('task_id',$request['task_id'])->get() ;
            return view('tasks/ajax/comment_info', compact('task', 'user_list', 'task_comments','user'));
            //return view('tasks/detail');
        }
    }


    // delete comment

    public function delete_comment(Request $request)
    {   
        $request_data = $request->all();
         $comment_id = $request['comment_id'];
         $task_id = $request['task_id'];
         $comment_del = Comment::where('id', $comment_id)->delete();
        
        
        // Task::where('id', $task_id)->delete();
        

        // Genetate view for replace current folder section
        $user_list = User::where('status', 'Active')->orderBy('name')->get();
        
        $task = Task::find($task_id);
        //$folder = $request['folder_id'];
        //$f_w_t =  Folder::find($folder);
        $user = User::find( Auth::user()->id );
        $task_comments =  Comment::where('task_id',$task_id)->get() ;
        return view('tasks/ajax/comment_info', compact('task', 'user_list', 'task_comments','user'));
            //return view('tasks/detail');
        
    }


    
}
