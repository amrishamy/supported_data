<?php

namespace App\Http\Controllers\Task;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

use Illuminate\Support\Carbon;

use Auth;
use Mail;
use App\Task;
use App\Comment;
use App\Folder;
use App\User;
use App\TaskAssignment;
use DB;

use App\Exports\UsersExport;
use App\Exports\SummaryExport;
use Maatwebsite\Excel\Facades\Excel;

class TaskController extends Controller
{
    public $folderModel;
    public $taskModel;
    public $commentModel;

    

    public function __construct(Folder $folderModel, Task $taskModel, Comment $commentModel)
    {
       
        $this->folderModel = $folderModel;
        $this->taskModel = $taskModel;
        $this->commentModel = $commentModel;

    }

    private function user_list()
    {
        return  User::where('id', '!=', Auth::user()->id )->orderBy('name')->get();

    }
    private function task_emails($task, $update=false, $complete=false)
    {

        $mail_to = array();
        //$mail_to[$task->assigned_to] = $task->assigned_to;
        $mail_to[@$task->assigned_to_who->assigned_to] = @$task->assigned_to_who->assigned_to;
        $mail_to[$task->created_by] = $task->created_by;
        

        if(isset($mail_to[ Auth::user()->id ]))
            unset($mail_to[  Auth::user()->id  ]);

        if($complete)
        {
            $mail_to = array();
            $admin_users = $this->get_admin_user();
            //echo "<pre>";print_r($admin_users);
            foreach($admin_users as $admin)
            {
                //echo "<pre>";print_r($admin);
                $mail_to[ $admin['id'] ] = $admin['id'] ;
            }
            //die;

        }
        
        $login_url = URL::to('/');
        //echo "<pre>";print_r($task);die;
        foreach($mail_to as $user_id){
            $user_info = User::find($user_id);
            if(isset($user_info))
            {   //var_dump($user_info);die;
                $data = array(
                    'name'=>$user_info->name,
                    "email"=>$user_info->email,
                    "password"=>'123456',
                    "login_url"=>$login_url,
                    "task_id"=>$task->id,
                    "task_name"=>$task->name,
                    "assignee"=>$task->createdBy->name,
                    "due_date"=>$task->due_date 
                );
                
                //var_dump($data);die;
                //if(isset($task['notify']) && $task['notify'] )
                //{ 
                    if($update)
                    {
                        $email_template = "update_task_mail";
                        $email_subject = 'Assigned task has been updated' ;
                        
                    }
                    elseif($complete)
                    {
                        $email_template = "complete_task_mail";
                        $email_subject = 'task has been completed' ;
                    }
                    else
                    {
                        $email_template = "add_task_mail";
                        $email_subject = 'New task Assigned' ;
                    }
                    Mail::send(['html'=>$email_template ], $data, 
                            function($mail) use ($user_info,$task,$email_template, $email_subject ) {
                                $mail->to( $user_info->email,  $user_info->name)
                                    ->subject($email_subject)
                                    ->from('tasks@silentbeacon.com','Task Management Admin');
                            }
                        );
                        
                //}
            }
        }

    }
    

    public function index() {
        $data =  $this->list();

        /*echo "<pre>";
        $desired_object = $data['folders_with_tasks']->first(function($item) {
            return $item->id == 4;
        });
        print_r($desired_object);
        echo "</pre>";
        die();*/
        return view('tasks/index')->with(['folders_with_tasks'=>$data['folders_with_tasks'], 'user_list'=>$data['user_list']]);
    }

/**
 * Get Tasks listing along with Folders
 * 
 * @return array
 */
    public function list() {
        $folders_with_tasks = (object)[];
       
        //print_r($this->folderModel->with('tasks')->get());
        // $user_list = User::where('status', 'Active')->where('id', '!=', Auth::user()->id )->orderBy('name')->get();
        $user_list = $this->user_list();
        $user_status = User::where('id', Auth::user()->id)->first();
        if($user_status['status'] == "Pending") {
            $user_status->status = "Active";
            $user_status->save();
        }

        //$folders_with_tasks = $this->folderModel->with('tasks')->get();
        if(Auth::user()->role == "admin") {   
            //$folders_with_tasks = $this->folderModel->with('tasks')->orderBy('created_at','desc')->get();
            $folders_with_tasks = $this->folderModel->with(array('tasks' => function($query) {
                $query->orderBy('due_date','asc');
            }))->orderBy('order','desc')->orderBy('id','desc')->get();
       } else {

       //echo "<pre>"; print_r($folders_with_tasks = $this->folderModel->with('tasks.assignments')->get());

            $folders_with_tasks = $this->folderModel
                                    /*->with(array('tasks' => function($query) {
                                            //$query->where('tasks.assigned_to', Auth::user()->id);
                                            $query->rightJoin('task_assignments', function($join)
                                            {
                                               $join->on('tasks.id', '=', 'task_assignments.task_id');
                                            })
                                            ->orwhere('tasks.created_by', Auth::user()->id)
                                            ->orWhere('task_assignments.assigned_to', Auth::user()->id)
                                            ->orderBy('tasks.due_date','asc');*/
                                    ->with(array('tasks' => function($query) {
                                        $query
                                            //->where('task_assignments.assigned_to', Auth::user()->id)
                                            //->orWhere('tasks.created_by', Auth::user()->id)
                                            ->orderBy('due_date','asc');
                                    }))
                                    
                                    /*->with(array('tasks.assignments' => function($query){
                                        $query->where("created_by",Auth::user()->id);
                                        $query->orWhere("assigned_to",Auth::user()->id);
                                    }))*/
                                    
                                    ->orderBy('order','desc')->orderBy('id','desc')    
                                    ->get();

          //  echo $folders_with_tasks ; die;
       }
        
        // echo '<pre>';
        // print_r($folders_with_tasks);
        return array('folders_with_tasks'=>$folders_with_tasks,'user_list'=>$user_list);
    }

    public function new_tasks() {
        DB::enableQueryLog();

        //$user_list = $this->user_list();
        
        $folder_task_new = array();
        $folder_task_due = array();
        $folder_task_past_due = array();

        $folders = $this->folderModel->orderBy('created_at','desc')->get();
        foreach($folders as $folder)
        {
            //echo $folder->name;
            $folder_task_new[$folder->id]['dt'] = date("Y-m-d H:00:00");
            $folder_task_new[$folder->id]['folder'] = $folder;

            $folder_task_due[$folder->id]['dt'] = date("Y-m-d H:00:00");
            $folder_task_due[$folder->id]['folder'] = $folder;

            $folder_task_past_due[$folder->id]['dt'] = date("Y-m-d H:00:00");
            $folder_task_past_due[$folder->id]['folder'] = $folder;

            // task having today created_at date
            

            if(Auth::user()->role != "admin")
            {
                $folder_task_new[$folder->id]['new_task'] = $tasks_new = Task::where('folder', $folder->id )
                ->leftJoin('task_assignments', function($join)
                {
                    $join->on('tasks.id', '=', 'task_assignments.task_id');
                })
                ->where( function ($query) {
                    $query->where('task_assignments.assigned_to', Auth::user()->id )
                            ->orWhere('tasks.created_by', Auth::user()->id);
                })
                
                ->where('task_assignments.status', 0 )
                ->whereDate('tasks.created_at', date('Y-m-d') )
                ->orderBy('tasks.due_date','asc')
                ->get();

                            
            }
            else
            {
                $folder_task_new[$folder->id]['new_task'] = $tasks_new = Task::where('folder', $folder->id )
                ->leftJoin('task_assignments', function($join)
                {
                    $join->on('tasks.id', '=', 'task_assignments.task_id');
                })

                ->where('task_assignments.status', 0 )
                ->whereDate('created_at', date('Y-m-d') )
                ->orderBy('due_date','asc')
                ->get();

                                            

            }
            //    echo "<pre>"; print_r($folder_task);continue;
            //dd( DB::getQueryLog() );
            
            //echo "<pre>"; print_r( $tasks_new ) ;

            // task having future due date


            if(Auth::user()->role != "admin")
            {
                $folder_task_due[$folder->id]['coming_due_task'] = $due_task = Task::where('folder', $folder->id )
                ->leftJoin('task_assignments', function($join)
                {
                    $join->on('tasks.id', '=', 'task_assignments.task_id');
                })
            
                ->where( function ($query) {
                        $query->where('task_assignments.assigned_to', Auth::user()->id )
                                ->orWhere('tasks.created_by', Auth::user()->id);
                    })
                ->where('task_assignments.status', 0 )
                ->whereDate('tasks.created_at', '!=', date('Y-m-d'))
                //->where('due_date', '>', 0 )
                ->where('tasks.due_date', '>', date("Y-m-d H") )
                ->orderBy('tasks.due_date','asc')
                ->get();
            

            }
            else
            {
                $folder_task_due[$folder->id]['coming_due_task'] = $due_task = Task::where('folder', $folder->id )
                ->leftJoin('task_assignments', function($join)
                {
                    $join->on('tasks.id', '=', 'task_assignments.task_id');
                })
                                        
                ->where('tasks.status', 0 )
                ->whereDate('tasks.created_at', '!=', date("Y-m-d"))
                //->where('due_date', '>', 0 )
                ->where('tasks.due_date', '>', date("Y-m-d H:00:00") )
                ->orderBy('tasks.due_date','asc')
                ->get();
            }
            
            //dd( DB::getQueryLog() );
            // task having past due date

            
            if(Auth::user()->role != "admin")
            {
                $folder_task_past_due[$folder->id]['past_due_task'] = $past_due_task = Task::where('folder', $folder->id )
                ->leftJoin('task_assignments', function($join)
                {
                    $join->on('tasks.id', '=', 'task_assignments.task_id');
                })

                ->where( function ($query) {
                    $query->where('task_assignments.assigned_to', Auth::user()->id )
                            ->orWhere('tasks.created_by', Auth::user()->id);
                })
                ->where('tasks.status', 0 )
                ->whereDate('tasks.created_at', '!=', date("Y-m-d"))
                ->where('tasks.due_date', '>', 0 )
                ->where('tasks.due_date', '<', date("Y-m-d H:00:00") )
                ->orderBy('tasks.due_date','asc')
                ->get();
            
            }
            else
            {
                $folder_task_past_due[$folder->id]['past_due_task'] = $past_due_task = Task::where('folder', $folder->id )
                ->leftJoin('task_assignments', function($join)
                {
                    $join->on('tasks.id', '=', 'task_assignments.task_id');
                })

                ->where('tasks.status', 0 )
                ->whereDate('tasks.created_at', '!=', date("Y-m-d"))
                ->where('tasks.due_date', '>', 0 )
                ->where('tasks.due_date', '<', date("Y-m-d H:00:00") )
                ->orderBy('tasks.due_date','asc')
                ->get();
            }

        }

        // foreach( $folder_task_due as $ft)
        // {
        //     echo $ft['folder']->name."==count is=>".count ( $ft['coming_due_task'] );
        // }
        //echo "<pre>"; print_r( $folder_task_due ) ;die();
        return array( "folder_task_new"=> $folder_task_new, "folder_task_due"=> $folder_task_due, "folder_task_past_due"=> $folder_task_past_due ) ;
        //return view('emails/email_html_view')->with([ 'folders_with_tasks'=> $folder_task ]); 

      

    }

    // function for excel export of summary
    public function exportable_summary()
    {
        DB::enableQueryLog();
        $summary = array();
        
        $folders = $this->folderModel->orderBy('created_at','desc')->get();
        foreach($folders as $folder)
        {
            //echo $folder->name;
            $summary[$folder->id]['user'] = Auth::user()->name ;
            $summary[$folder->id]['folder'] = $folder;

            if(Auth::user()->role != "admin")
            {
                $summary[$folder->id]['tasks'] = $tasks_new = Task::where('folder', $folder->id )
                    ->leftJoin('task_assignments', function($join)
                    {
                        $join->on('tasks.id', '=', 'task_assignments.task_id');
                    })
                    ->where( function ($query) {
                        $query->where('task_assignments.assigned_to', Auth::user()->id )
                                ->orWhere('tasks.created_by', Auth::user()->id);
                    })
                    ->where('task_assignments.status', 0 )
                    //->whereDate('created_at', date('Y-m-d') )
                    ->orderBy('tasks.due_date','asc')
                    ->select('tasks.id', 'tasks.name', 'tasks.due_date', 'tasks.created_at', 'task_assignments.task_id', 'task_assignments.assigned_by', 'task_assignments.assigned_to' )
                    ->get();
            }
            else
            {
                $summary[$folder->id]['tasks'] = $tasks_new = Task::where('folder', $folder->id )
                    ->leftJoin('task_assignments', function($join)
                    {
                        $join->on('tasks.id', '=', 'task_assignments.task_id');
                    })
                    ->where('task_assignments.status', 0 )
                    //->whereDate('created_at', date('Y-m-d') )
                    ->orderBy('tasks.due_date','asc')
                    ->select('tasks.id', 'tasks.name', 'tasks.due_date', 'tasks.created_at', 'task_assignments.task_id', 'task_assignments.assigned_by', 'task_assignments.assigned_to' )
                    ->get();
            }
           
        }

        return  $summary  ;
        
    }

    public function create()
    {
    	return view('tasks/create');
    }

    public function edit()
    {
    	return view('tasks/edit');
    }

    public function save(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'task_name' => 'required',
            'assigned_to' => 'required',
            'due_date' => 'required',
        ]);
        
        if ($validator->fails()){
            $error = "";
            foreach($validator->errors()->all() as $key => $err){
                $error .= $err ."<br>";
            }   
            $error = str_replace('"',"",$error);
            return $error; 
        }else{
            $task= new Task();
            $task->name= $request['task_name'];
            $task->assigned_to= $request['assigned_to'];
            
            $task->due_date= (strtotime($request['due_date']) > 0 ) ? date('Y-m-d 10::00', strtotime($request['due_date'])) : '' ;
            $task->notes= $request['notes'];
            $task->created_by= $request['logged_user_id'];
            $task->folder= $request['folder_id'];
            $task->notify= $request['notify_user'];
            // add other fields
            $task->save();
            $taskId = $task->id;

            // send notify email to user for assigned task
            $user = User::find($request['assigned_to']);
            $login_url = URL::to('/');
            
            if( isset($user) && isset($request['notify_user']) && $request['notify_user'] )
            { 
                $data = array('name'=>$user->name,"email"=>$user->email,"password"=>'123456',"login_url"=>$login_url,"task_name"=>$request['task_name'] );

                Mail::send(['html'=>'add_task_mail'], $data, function($message) use ($user) {
                $message->to( $user->email,  $user->name)->subject
                    ('New task Assigned');
                $message->from('tasks@silentbeacon.com','Task Management Admin');
                });
            }
            
            // html for refresh task list according to folder.
            $tasklist = Folder::find($request['folder_id'])->tasks; 
            // Check if function called from Detail page or task listing page
            $refresh_tasks = "";
            if($request['form_type'] == "detail"){
                // Genetate view for replace current folder section
                //$user_list = User::where('status', 'Active')->where('id', '!=', Auth::user()->id )->orderBy('name')->get();
                $user_list = $this->user_list();
                if(Auth::user()->role == "admin")
                {
                    $tasks = Task::where('folder', $request['folder_id'])->get();            
                }
                else
                {
                    $tasks = Task::where('folder', $request['folder_id'])
                    ->where(function($query)
                    {
                        $query->where('tasks.assigned_to', Auth::user()->id )
                        ->orWhere('tasks.created_by', Auth::user()->id);
                    })->get();
                    
                }
                //echo "<pre>";print_r($tasks);die;
                $folder = Folder::find($request['folder_id']); 
                return $refresh_tasks = view('tasks/ajax/folder_info_detail', compact('tasks', 'user_list', 'folder'));
                // return response()->json(['success'=>$refresh_tasks]);
            }
        }
        
    }
    public function save_task(Request $request)
    {   
        //echo "<pre>";print_r( $request->all() );
        if ($request->hasFile('file'))
        {
            $file =  $request->file('file')  ;
            
                    //Display File Name
            $File_Name = $file->getClientOriginalName();
            
        
            //Display File Extension
            $File_Extension  = $file->getClientOriginalExtension();
           
        
            //Display File Real Path
            $File_Real_Path = $file->getRealPath();
           
        
            //Display File Size
            $File_Size = $file->getSize();
           
        
            //Display File Mime Type
            $File_Mime_Type = $file->getMimeType();
        
            //Move Uploaded File
            $destinationPath = public_path('uploads');
            $file->move( $destinationPath , time().'_'.$File_Name );
        }
       
       // die;
        $validator = \Validator::make($request->all(), [
            'task_name' => 'required',
            //'assigned_to' => 'required',
            //'due_date' => 'required',
        ]);
        
        if ($validator->fails()){
            $error = "";
            foreach($validator->errors()->all() as $key => $err){
                $error .= $err ."<br>";
            }   
            $error = str_replace('"',"",$error);
            return $error; 
        }else{
            $task = new Task();
            $task->name = $request['task_name'];
            if(empty($request['assigned_to']) || !isset( $request['assigned_to'] ) )
            {
                //$request['assigned_to'] = $request['logged_user_id'];
            }
            //$task->assigned_to= $request['assigned_to'];
            if(isset($request['due_date']) && !empty($request['due_date'])){
                $task->due_date= (strtotime($request['due_date']) > 0 ) ? date('Y-m-d 10:00:00', strtotime($request['due_date'])) : '' ;
            }else{

            }
            
            if(isset( $request['reminders'] ) && !empty( $request['reminders'] ) )
            {
                $task->reminders= implode( ",", $request['reminders'] ) ;
 
            }
            
            $task->notes= $request['notes'];
            //if ($request->hasFile('file'))
            //$task->uploads= time().'_'.$File_Name ;
            if( isset($request['up_file']) && !empty($request['up_file']) )
            $task->uploads= $request['up_file'] ;
            $task->created_by= $request['logged_user_id'];
            $task->folder= $request['folder_id'];
            //$task->notify= $request['notify_user'];
            if(isset($request['admin_approval']))
            $task->admin_approval= $request['admin_approval'];
            
            // add other fields
            $task->save();
            $taskId = $task->id;

            // save in task_assignment
            if( isset($request['assigned_to']) && !empty($request['assigned_to'])  )
            {
                $task_assignment = new TaskAssignment();
                $task_assignment->task_id = $task->id;
                $task_assignment->created_by = $task->created_by;
                $task_assignment->assigned_by = $request['logged_user_id'];
                $task_assignment->assigned_to = $request['assigned_to'];
                $task_assignment->save();
            }

            // send notify email to user for assigned task
            $this->task_emails($task);

            // Genetate view for replace current folder section
            //$user_list = User::where('status', 'Active')->where('id', '!=', Auth::user()->id )->orderBy('name')->get();
            $user_list = $this->user_list();
            //$all_tasks = Task::where('folder', $request['folder_id'])->get();
            if(Auth::user()->role == "admin") {
                //$all_tasks = Task::where('folder', $request['folder_id'])->get();            
                $all_tasks = Task::where('folder', $request['folder_id'])->orderBy('due_date','asc')->get();            
            } else {
                $all_tasks = Task::where('folder', $request['folder_id'])
                ->where(function($query)
                {
                    $query//->where('tasks.assigned_to', Auth::user()->id )
                    //->orWhere('tasks.created_by', Auth::user()->id)
                    ->orderBy('due_date','asc');
                })->get();                
            }
            //dd($all_tasks);
            //echo "<pre>";print_r($all_tasks);die;
            $folder = $request['folder_id'];
            //return $refresh_tasks = view('tasks/ajax/folder_info', compact('all_tasks', 'user_list', 'folder'));
            $f_w_t =  Folder::find($folder);
            return view('tasks/ajax/folder_info', compact('all_tasks','user_list','folder','f_w_t'));
            // return response()->json(['success'=>$refresh_tasks]);
        }
    }

    public function update_task(Request $request) {
        if ($request->hasFile('file')) {
            $file =  $request->file('file')  ;
            
            // Display File Name
            $File_Name = $file->getClientOriginalName();            
        
            // Display File Extension
            $File_Extension  = $file->getClientOriginalExtension();           
        
            // Display File Real Path
            $File_Real_Path = $file->getRealPath();           
        
            // Display File Size
            $File_Size = $file->getSize();           
        
            // Display File Mime Type
            $File_Mime_Type = $file->getMimeType();
        
            // Move Uploaded File
            $destinationPath = public_path('uploads');
            $file->move( $destinationPath , time().'_'.$File_Name );
        }

        $validator = \Validator::make($request->all(), [
            'task_name' => 'required',
            //'assigned_to' => 'required',
            //'due_date' => 'required',
        ]);
        
        if ($validator->fails()){
            // echo json_encode(['errors'=>$validator->errors()->all()]);
            $error = "";
            foreach($validator->errors()->all() as $key => $err){
                $error .= $err ."<br>";
            }   
            $error = str_replace('"',"",$error);
            return $error; 
        } else {
            $task= Task::find($request['task_id']);
            $task->name= $request['task_name'];
            if(empty($request['assigned_to']) || !isset( $request['assigned_to'] ) ) {
                //$request['assigned_to'] = $request['logged_user_id'];
            }
            //$task->assigned_to= $request['assigned_to'];
            
            if(isset($request['due_date']) && !empty($request['due_date'])){
                $task->due_date= (strtotime($request['due_date']) > 0 ) ? date('Y-m-d 10:00:00', strtotime($request['due_date'])) : '' ;    
            }else{

            }
            $task->notes= $request['notes'];
            //if ($request->hasFile('file'))
            //$task->uploads= time().'_'.$File_Name ;
            if( isset($request['up_file']) && !empty($request['up_file']) )
            $task->uploads= $request['up_file'] ;
            //$task->created_by= $request['logged_user_id'];
            //$task->folder= $request['folder_id'];
            //$task->notify= $request['notify_user'];
            if(isset($request['admin_approval']))
            $task->admin_approval= $request['admin_approval'];
            // add other fields
            $task->save();

            $taskId = $task->id;

            // save in task_assignment
            if( isset($request['assigned_to'])   )
            {   
                $task_assignment = TaskAssignment::where('task_id', $task->id )->where('assigned_by', Auth::user()->id )->orWhere('assigned_by', '0' )->first() ;
                //echo "<pre>";print_r($exist);
                if($task_assignment)
                    TaskAssignment::where('task_id', $task->id )->where('id','>',$task_assignment->id)->delete();
                else
                    $task_assignment = new TaskAssignment();
                
                //$task_assignment = new TaskAssignment();
                $task_assignment->task_id = $task->id;
                $task_assignment->created_by = $task->created_by;
                $task_assignment->assigned_by = $request['logged_user_id'];
                $task_assignment->assigned_to = $request['assigned_to'];
                $task_assignment->save();

            }
            
            // notify user for assined task updation

            $this->task_emails($task, true);

            $tsk = Task::find($request['task_id']);
            $f_w_t = Folder::find($request['folder_id']);
            
            // Genetate view for replace current task section
            //$user_list = User::where('status', 'Active')->where('id', '!=', Auth::user()->id )->orderBy('name')->get();
            $user_list = $this->user_list();
            //$all_tasks = Task::where('folder', $request['folder_id'])->get();
            if(Auth::user()->role == "admin")
            {
                //$all_tasks = Task::where('folder', $request['folder_id'])->get(); 
                $all_tasks = Task::where('folder', $request['folder_id'])->orderBy('due_date','asc')->get();                                       
            }
            else
            {
                $all_tasks = Task::where('folder', $request['folder_id'])
                ->where(function($query)
                {
                    $query//->where('tasks.assigned_to', Auth::user()->id )
                    //->orWhere('tasks.created_by', Auth::user()->id)
                    ->orderBy('due_date','asc');
                })->get();
                
            }
            // dd($all_tasks);
            // echo "<pre>";print_r($all_tasks);die;
            $folder = $request['folder_id'];
            // $refresh_tasks = view('tasks/ajax/task_detail_single', compact('all_tasks', 'user_list', 'folder','tsk','f_w_t'));
            
            // return response()->json(['success'=>$refresh_tasks]);
            // return view('tasks/ajax/folder_info', compact('all_tasks', 'user_list', 'folder'));
            $f_w_t =  Folder::find($folder);

            return view('tasks/ajax/folder_info', compact('all_tasks','user_list','folder','f_w_t'));
        }
    }
    
    public function update_task_status(Request $request) {
        $request_data = $request->all();
        //print_r($request_data);
        $task_id =  $request_data['task_id'];
        $check_type =  $request_data['check_type'];

        $task = Task::find($task_id);
        $task_assignment = ( @$task->assigned_to_who) ? @$task->assigned_to_who : new TaskAssignment() ;
        if($check_type=="checked") {
            $task_assignment->status = 1 ;
            $task_assignment->task_id = $task->id;
            $task_assignment->created_by = $task->created_by;
            $task_assignment->save();

            $task = Task::find($task_id);

            $this->task_emails($task, $update=false, $complete=true );
        } else {
            $task_assignment->status = 0 ;
            $task_assignment->save();
            $task = Task::find($task_id);
        }
        
        //$user_list = User::where('status', 'Active')->where('id', '!=', Auth::user()->id )->orderBy('name')->get();
        $user_list = $this->user_list();
        if(Auth::user()->role == "admin")
        {
            $all_tasks = Task::where('folder', $request['folder_id'])->orderBy('due_date','asc')->get();            
        }
        else
        {
            $all_tasks = Task::where('folder', $request['folder_id'])
            ->where(function($query)
            {
                $query//->where('tasks.assigned_to', Auth::user()->id )
                //->orWhere('tasks.created_by', Auth::user()->id)
                ->orderBy('due_date','asc');
            })->get();
        }
        $folder = $task->folder;
        $f_w_t =  Folder::find($folder);
        return view('tasks/ajax/folder_info', compact('all_tasks','user_list','folder','f_w_t'));
    }

    public function delete_task(Request $request)
    {
        $request_data = $request->all();
        //print_r($request_data);
        $task_id =  $request_data['task_id'];
        $task_info = Task::where('id', $task_id)->first();
        $task_del = Task::where('id', $task_id)->delete();
        $comments_del = $task_info->comments()->delete();
        $assignments_del = $task_info->assignments()->delete();
        //$user_list = User::where('status', 'Active')->where('id', '!=', Auth::user()->id )->orderBy('name')->get();
        $user_list = $this->user_list();
        //$all_tasks = Task::where('folder', $task['folder'])->get();
        if(Auth::user()->role == "admin")
        {
            $all_tasks = Task::where('folder', $task_info->folder)->orderBy('due_date','asc')->get();            
        }
        else
        {
            $all_tasks = Task::where('folder', $task_info->folder)
            ->where(function($query)
            {
                $query//->where('tasks.assigned_to', Auth::user()->id )
                //->orWhere('tasks.created_by', Auth::user()->id)
                ->orderBy('due_date','asc');
            })->get();
            
        }
        $folder = $task_info->folder;
        $f_w_t =  Folder::find($folder);

        return view('tasks/ajax/folder_info', compact('all_tasks','user_list','folder','f_w_t'));
    }

    public function task_detail($task_id)
    {
        
        $task = Task::find($task_id); 
        
      
       $task_comments =  Comment::where('task_id',$task_id)->get() ;
       //echo $task_comments->count();die;
    //    /echo "<pre>";print_r( $task_comments  ) ; die;
        // $task = Folder::find($folderId)->tasks; 
        Comment::where('task_id', '=', $task_id)->where('user_id', '!=', Auth::user()->id )->update(['is_read'=>'1']);
        $user = User::find( Auth::user()->id );
        //$user_list = User::where('status', 'Active')->where('id', '!=', Auth::user()->id )->orderBy('name')->get();
        $user_list = $this->user_list();
        $comments = "";
    	return view('tasks/detail', compact('task', 'user_list', 'task_comments','user'));


    }
    
    // update the single task on task detail page

    public function update_task_single(Request $request)
    {
        if ($request->hasFile('file'))
        {
            $file =  $request->file('file')  ;
            
                    //Display File Name
            $File_Name = $file->getClientOriginalName();
            
        
            //Display File Extension
            $File_Extension  = $file->getClientOriginalExtension();
           
        
            //Display File Real Path
            $File_Real_Path = $file->getRealPath();
           
        
            //Display File Size
            $File_Size = $file->getSize();
           
        
            //Display File Mime Type
            $File_Mime_Type = $file->getMimeType();
        
            //Move Uploaded File
            $destinationPath = public_path('uploads');
            $file->move( $destinationPath , time().'_'.$File_Name );
        }

        $validator = \Validator::make($request->all(), [
            'task_name' => 'required',
            //'assigned_to' => 'required',
            //'due_date' => 'required',
        ]);
        
        if ($validator->fails()){
            // echo json_encode(['errors'=>$validator->errors()->all()]);
            $error = "";
            foreach($validator->errors()->all() as $key => $err){
                $error .= $err ."<br>";
            }   
            $error = str_replace('"',"",$error);
            return $error; 
        }else{
            $task= Task::find($request['task_id']);
            $task->name= $request['task_name'];
            if(empty($request['assigned_to']) || !isset( $request['assigned_to'] ) )
            {
                //$request['assigned_to'] = $request['logged_user_id'];
            }
            //$task->assigned_to= $request['assigned_to'];
            if(isset($request['due_date']) && !empty($request['due_date'])){
                $task->due_date= (strtotime($request['due_date']) > 0 ) ? date('Y-m-d 10:00:00', strtotime($request['due_date'])) : '' ;    
            }else{

            }
            $task->notes= $request['notes'];
            //if ($request->hasFile('file'))
            //$task->uploads= time().'_'.$File_Name ;
            if( isset($request['up_file']) && !empty($request['up_file']) )
            $task->uploads= $request['up_file'] ;
            //$task->created_by= $request['logged_user_id'];
            //$task->folder= $request['folder_id'];
            //$task->notify= $request['notify_user'];
            if(isset($request['admin_approval']))
            $task->admin_approval= $request['admin_approval'];
            // add other fields
            $task->save();

            // save in task_assignment
            if( isset($request['assigned_to'])   )
            {   
                $task_assignment = TaskAssignment::where('task_id', $task->id )->where('assigned_by', Auth::user()->id )->orWhere('assigned_by', '0' )->first() ;
                //echo "<pre>";print_r($exist);
                if($task_assignment)
                    TaskAssignment::where('task_id', $task->id )->where('id','>',$task_assignment->id)->delete();
                else
                    $task_assignment = new TaskAssignment();
                
                //$task_assignment = new TaskAssignment();
                $task_assignment->task_id = $task->id;
                $task_assignment->created_by = $task->created_by;
                $task_assignment->assigned_by = $request['logged_user_id'];
                $task_assignment->assigned_to = $request['assigned_to'];
                $task_assignment->save();

            }

            // notify user for assined task updation

            $this->task_emails($task, true);

            /* $user = User::find($request['assigned_to']);
            $login_url = URL::to('/');
            
            if( isset($user) && isset($request['notify_user']) && $request['notify_user'] )
            { 
                $data = array('name'=>$user->name,"email"=>$user->email,"password"=>'123456',"login_url"=>$login_url,"task_name"=>$request['task_name'] );

                Mail::send(['html'=>'update_task_mail'], $data, function($message) use ($user) {
                $message->to( $user->email,  $user->name)->subject
                    ('Assigned task has been updated');
                $message->from('admin@taskmanagement.com','Task Management Admin');
                });
            } */

            $task = Task::find($request['task_id']);
            $f_w_t = Folder::find($request['folder_id']);
            
            // Genetate view for replace current task section
            //$user_list = User::where('status', 'Active')->where('id', '!=', Auth::user()->id )->orderBy('name')->get();
            $user_list = $this->user_list();
            $folder = $request['folder_id'];           
            
            $f_w_t = Folder::find($folder);
            return view('tasks/ajax/single_task_info', compact('task','user_list','folder','f_w_t'));
            
        }
    }

    public function delete_task_single(Request $request)
    {
        $request_data = $request->all();
        //print_r($request_data);
        $task_id =  $request_data['task_id'];
        $task_info = Task::where('id', $task_id)->first();
        $task_del = Task::where('id', $task_id)->delete();

        $comments_del = $task_info->comments()->delete();
        

        //$user_list = User::where('status', 'Active')->where('id', '!=', Auth::user()->id )->orderBy('name')->get();
        $user_list = $this->user_list();
        //$all_tasks = Task::where('folder', $task['folder'])->get();
        if(Auth::user()->role == "admin")
        {
            $all_tasks = Task::where('folder', $task_info->folder)->get();            
        }
        else
        {
            $all_tasks = Task::where('folder', $task_info->folder)
            ->where(function($query)
            {
                $query->where('tasks.assigned_to', Auth::user()->id )
                ->orWhere('tasks.created_by', Auth::user()->id);
            })->get();
            
        }
        $folder = $task_info->folder;
        $f_w_t =  Folder::find($folder);
        //return view('tasks/ajax/folder_info', compact('all_tasks', 'user_list', 'folder','f_w_t'));
        return "okay";
    }


    // function for update Task status of single task/comment page

    public function update_task_status_single(Request $request)
    {
        $request_data = $request->all();
        //print_r($request_data);
        $task_id =  $request_data['task_id'];
        $folder_id =  $request_data['folder_id'];
        $check_type =  $request_data['check_type'];
        $task = Task::find($task_id);
        $task_assignment = ( @$task->assigned_to_who) ? @$task->assigned_to_who : new TaskAssignment() ;
        if($check_type=="un-checked")
        {
            $task_assignment->status = 1 ;
            $task_assignment->task_id = $task->id;
            $task_assignment->created_by = $task->created_by;
            $task_assignment->save();
            
            $task = Task::find($task_id);

            $this->task_emails($task, $update=false, $complete=true );
        }
        else
        {
            $task_assignment->status = 0 ;
            $task_assignment->save();
            $task = Task::find($task_id);
        }
        
        $task = Task::find($task_id);
        $f_w_t = Folder::find($folder_id);
        
        // Genetate view for replace current task section
        //$user_list = User::where('status', 'Active')->where('id', '!=', Auth::user()->id )->orderBy('name')->get();
        $user_list = $this->user_list();
        
        
        $folder = $request['folder_id'];
        return view('tasks/ajax/single_task_info', compact('task', 'user_list', 'folder','f_w_t'));
    }

    public function upload_file(Request $request)

    {
        //print_r($request->all() );
        //$file =  $request->file('file')  ;
        //print_r($file);
        if ($request->hasFile('file'))
        {
            $file =  $request->file('file')  ;
            
                    //Display File Name
            $File_Name = $file->getClientOriginalName();
            
        
            //Display File Extension
            $File_Extension  = $file->getClientOriginalExtension();
           
        
            //Display File Real Path
            $File_Real_Path = $file->getRealPath();
           
        
            //Display File Size
            $File_Size = $file->getSize();
           
        
            //Display File Mime Type
            $File_Mime_Type = $file->getMimeType();
        
            //Move Uploaded File
            $destinationPath = public_path('uploads');

            $file_name_new = time().'_'.$File_Name ;
            
            if ( $file->move( $destinationPath , $file_name_new ) )
            {
                echo json_encode(array("status"=>"200","filename"=>$file_name_new ) );
            }
            else
            {
                echo json_encode(array("status"=>"405","filename"=>$file_name_new ) );

            }
        }

        



    }

    public function excel() {
        return Excel::download(new SummaryExport, 'summary'.time().'.xlsx');
        
    }

    /* start function to get admin user*/
    private function get_admin_user()
    {
        return  User::where('role', '=', 'admin' )->orderBy('name')->get();
    }
    /* end function to get admin user*/

    /* Start function for task further assignment */
    public function further_assign_task(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'further_assigned_to' => 'required',
        ]);
        
        if ($validator->fails()){
            // echo json_encode(['errors'=>$validator->errors()->all()]);
            $error = "";
            foreach($validator->errors()->all() as $key => $err){
                $error .= $err ."<br>";
            }   
            $error = str_replace('"',"",$error);
            return $error; 
        }else{
            $task = Task::find($request['task_id']);
            //echo "<pre>";print_r($task); die;
            $exist = TaskAssignment::where('task_id', $task->id )->where('assigned_by', Auth::user()->id )->first() ;
            //echo "<pre>";print_r($exist);
            if($exist)
            $task_assignment = TaskAssignment::find($exist->id);
            else
            $task_assignment = new TaskAssignment();
            
            $task_assignment->task_id = $task->id ;
            $task_assignment->created_by = $task->created_by ;
            $task_assignment->assigned_by = $request['logged_user_id'];
            $task_assignment->assigned_to = $request['further_assigned_to'];
            
            $task_assignment->save();

            //echo $taskId = $task_assignment->id;
            
            // notify user for assined task updation

            $this->task_emails($task, true);
            
            
            $user_list = $this->user_list();
            if(Auth::user()->role == "admin")
            {
                $all_tasks = Task::where('folder', $request['folder_id'])->orderBy('due_date','asc')->get();                                       
            }
            else
            {
                $all_tasks = Task::where('folder', $request['folder_id'])
                ->where(function($query)
                {
                    $query//->where('tasks.assigned_to', Auth::user()->id )
                    //->orWhere('tasks.created_by', Auth::user()->id)
                    ->orderBy('due_date','asc');
                })->get();
                
            }
            //dd($all_tasks);
            $folder = $request['folder_id'];
            $f_w_t =  Folder::find($folder);
            return view('tasks/ajax/folder_info', compact('all_tasks', 'user_list', 'folder','f_w_t'));
            
        }
    }

    /* End function for task further assignment */

}
