<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\URL;

use Illuminate\Support\Str;

use Mail;
use App\User;


class UserController extends Controller
{
    public function index()
    {
        /* echo "<Pre>";
        print_r(auth()->user()->id);die; */
        //$user_list = User::all();
        $user_list = User:: orderBy('name', 'ASC')->where("role", "!=", "Admin")->get();
        //print_r($user_list); die;
        return view('users/index')->with('user_list', $user_list);
    	//return view('users/index');
    }
    public function create()
    {
    	return view('users/create');
    }
    public function userInvitation(Request $request)
    {   
       // print_r($request);die;

       $request->validate([
        'name' => 'required',
        'email' => 'required|email',
        'password' => 'required|max:20|min:6',
        ]);

        $user= new User();
        $user->name= $request['name'];
        $user->email= $request['email'];
        $user->role= "customer";
        $user->password= Hash::make($request['password']);
        if (User::where('email', '=', $request['email'])->exists()) {
            return redirect()->action('User\UserController@index')->with('error','User already Invited with this email !');
         }

        $user->save();
        $uid =  encrypt($user->id);
        $link = URL::route('userVerify');
        $login_url = URL::to('/');
        $data = array('name'=>$user->name,"email"=>$user->email,"password"=>$request['password'],"login_url"=>$login_url);
   
        Mail::send(['html'=>'mail'], $data, function($message) use ($user) {
         $message->to( $user->email,  $user->name)->subject
            ('Invitation Mail');
         $message->from('tasks@silentbeacon.com','Task Management Admin');
      });
     
           
      return redirect()->action('User\UserController@index')->with('success','Email Invitation Successfully Sent!');
        //return redirect('users');
    }
    public function edit($id)
    {
        $user = User::find($id);

    	return view('users/edit')->with('user',$user);
    }
    public function update(Request $request)
    {
        echo $request['name'];die;
        $user = User::find($id);

    	return view('users/edit')->with('user',$user);
    }
    public function userVerify()
    {
    	echo "test";
    }
    public function delete($id)
    {
        $user = User::find($id);
        $user->delete();
    	return redirect()->action('User\UserController@index');
    }
    public function reset_password($id)
    {
        $user = User::find($id);
        $password = Hash::make('123456');
        $user->password = $password;
        $user->save();
        $user = User::find($id);
        $login_url = URL::to('/');
        $data = array('name'=>$user->name,"email"=>$user->email,"password"=>"123456","login_url"=>$login_url);
   
        Mail::send(['html'=>'mail'], $data, function($message) use ($user) {
            $message->to( $user->email,  $user->name)->subject
                ('Reset Password Mail');
            $message->from('tasks@silentbeacon.com','Task Management Admin');
        });
        return redirect()->action('User\UserController@index')->with('success','Email Successfully Sent!');
    }

    public function send_link_to_pending_user($id)
    {
        $user = User::find($id);
        $password = Hash::make('123456');
        $user->password = $password;
        $user->save();
        $user = User::find($id);
        $login_url = URL::to('/');
        $data = array('name'=>$user->name,"email"=>$user->email,"password"=>"123456","login_url"=>$login_url);
   
        Mail::send(['html'=>'mail'], $data, function($message) use ($user) {
            $message->to( $user->email,  $user->name)->subject
                ('Invitation Mail');
            $message->from('tasks@silentbeacon.com','Task Management Admin');
        });
        return redirect()->action('User\UserController@index')->with('success','Invitation Email Successfully Sent!');
    }
    
}
