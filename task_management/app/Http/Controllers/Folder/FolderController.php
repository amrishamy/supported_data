<?php

namespace App\Http\Controllers\Folder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Mail;
use App\Task;
use App\Comment;
use App\Folder;
use App\User;

class FolderController extends Controller {
    public function index() {
        $folder_list = Folder::all();
        //print_r($folder_list);die;        
        return view('folders/index')->with('folder_list', $folder_list);	
    }

    public function create() {
    	return view('folders/create');
    }

    public function save(Request $request) {
        $request->validate([
            'name' => 'required',
        ]);
        //echo "<pre>";print_r( $request->all() );die;
        $folder = new Folder();
        $folder->name = $request['name'];
        $folder->order = empty(Folder::max('order')) ? 1 : Folder::max('order')+1;
        $folder->created_by = 1;//$request['logged_user_id'];
        // add other fields
        $folder->save();
        $folderId = $folder->id;
        //return view('folders/detail')->with('folder', $folder);       
        //return redirect()->route('folderdetail', ['folderId' => $folderId]);
        //return redirect()->action('FolderController@detail');
       return redirect()->route('tasks');
    }

    public function detail($folderId)
    {
        $folder = Folder::find($folderId); 
        // $task = Folder::find($folderId)->tasks; 
        $task = Task::where('folder', $folderId)->get();            
        //echo "<pre>";print_r($task);die;
        // $user_list = User::all();
        $user_list = User::where('status', 'Active')->get();
    	return view('folders/detail')->with(['folder'=> $folder,'user_list'=> $user_list,'tasks'=> $task]);;
    }

    public function edit($id)
    {
        $folder = Folder::find($id);
    	return view('folders/edit')->with('folder',$folder);
    }

    public function update_folder(Request $request) {
        //die('<p>hkjhkjhkjhjkhjk</p>');
        $request->validate([
            'name' => 'required',
        ]);
        $id = $request['folder_id'];
        
        $folder = Folder::find($id);
        $folder->name = $request['name'];
        $folder->save();
        
        if(Auth::user()->role == "admin") {
            //$all_tasks = Task::where('folder', $request['folder_id'])->get(); 
            $all_tasks = Task::where('folder', $request['folder_id'])->orderBy('due_date','desc')->get();                                       
        } else {
            $all_tasks = Task::where('folder', $request['folder_id'])
            ->where(function($query) {
                $query->where('tasks.assigned_to', Auth::user()->id )
                ->orWhere('tasks.created_by', Auth::user()->id)
                ->orderBy('due_date','desc');
            })->get();
            
        }

        $user_list = User::where('status', 'Active')->where('id', '!=', Auth::user()->id )->orderBy('name')->get();
        $folder = $request['folder_id'];
        $f_w_t =  Folder::find($folder);
        
        return view('tasks/ajax/folder_info', compact('all_tasks','user_list','folder','f_w_t'));    	
    }

    public function delete_folder(Request $request) {
        $request_data = $request->all();
        //print_r($request_data);
        $folderid =  $request_data['folder_id'];
        $folder_info = Folder::where('id', $folderid)->first();
        $folder_del = Folder::where('id', $folderid)->delete();
        $task_del = $folder_info->tasks()->delete();
        $comments_del = $folder_info->comments()->delete();

        return redirect()->route('tasks');
    }

    public function getTasksByFolder($folderId) {
        $folder = Folder::find($folderId); 
        $user_list = User::all();

    	return view('folders/detail')->with(['folder'=> $folder,'user_list'=> $user_list]);
    }


/**
 * Save ordering of Folders after Drag Drop
 */
    public function rearrange_folders(Request $request) {
        $parent = $request['parent']; // Immediate Parent of dropped element
        $child = $request['child']; // Dropped element
        $children = $request['children']; // Dropped element's siblings
        $order = $request['order'];

        $res = Folder::whereRaw('FIND_IN_SET(?,children_ids)',[$child])->first();
        if($res) {
            $children_ids = explode(',',$res->children_ids);
            $key = array_search($child,$children_ids);
            unset($children_ids[$key]);

            $folder = Folder::find($res['id']);
            $folder->children_ids = implode(',',$children_ids);
            $folder->save();
        }

        if($parent) {
            $folder = Folder::find($parent);
            $folder->children_ids = implode(',',$children);
            $folder->save();

            $folder = Folder::find($child);
            $folder->root = 1;
            $folder->order = 0;
            $folder->save();
        } else {
            $folder = Folder::find($child);
            $folder->root = 0;
            $folder->save();

            if(!empty($order)) {
                $order = array_reverse($order);
                foreach($order as $key => $val) {
                    $folder = Folder::find($val);
                    $folder->order = $key+1;
                    $folder->save();
                }
            }
        }

        return array($parent, $children);
    }

/**
 * Get Folder and Tasks details collectively
 */
    public function getGaintChartRecords() {
        if(Auth::user()->role == "admin") {
            $folders_with_tasks = Folder::with(array('tasks' => function($query) {
                $query->leftJoin('task_assignments', function($join)
                                            {
                                                $join->on('tasks.id', '=', 'task_assignments.task_id');
                                            })
                ->orderBy('tasks.due_date','asc')
                ->select('tasks.*','task_assignments.assigned_to','task_assignments.assigned_by','task_assignments.status','task_assignments.task_id', 'task_assignments.created_at as assign_created_at' );
            }))->orderBy('order','desc')->orderBy('id','desc')->get();
        } else {
            $folders_with_tasks = Folder::with(array('tasks' => function($query) {
                                            $query->leftJoin('task_assignments', function($join)
                                            {
                                                $join->on('tasks.id', '=', 'task_assignments.task_id');
                                            })->where('task_assignments.assigned_to', Auth::user()->id)
                                                ->orWhere('tasks.created_by', Auth::user()->id)
                                                ->orderBy('tasks.due_date','asc')
                                                ->select('tasks.*','task_assignments.assigned_to','task_assignments.assigned_by','task_assignments.status','task_assignments.task_id', 'task_assignments.created_at as assign_created_at' );
                                    }))->orderBy('order','desc')->orderBy('id','desc')->get();
        }
        $user_list = User::pluck('name','id');
        
        $ft_array = array();
        foreach($folders_with_tasks as $val) {
            if($val->root == 0) {
                $ft_array = $this->getFolderTaskListRecursive($folders_with_tasks, $val->id, $ft_array, 0, $user_list);
            }
        }

        return response()->json(['success'=>true,'data'=>$ft_array]);
    }

/**
 * Recursive function, create array of Folders and Tasks records
 * 
 * @param object $folders_with_tasks Folder and Tasks data
 * @param integer $f_id Folder id
 * @param array $ft_array Result array
 * @param integer $parent Parent ID
 * @param array $user_list User name,id array with id as key, name as value
 * 
 * @return array
 */
    private function getFolderTaskListRecursive($folders_with_tasks, $f_id, $ft_array, $parent, $user_list) {
        foreach($folders_with_tasks as $val) {
            if($val->id == $f_id) {
                $ft_array[] = array($val->id,$val->name, '', '', 'ff0000', url('/tasks'), 0, '', 0, 1, $parent, 1, '', '');
                if(!empty($val->tasks->toArray())) {
                    foreach($val->tasks as $k => $tsk) {
                        $assigned_to = !empty($tsk->assigned_to) ? $user_list[$tsk->assigned_to] : '';
                        $completion = ($tsk->status==1) ? 100 : 0;
                        $ft_array[] = array($tsk->task_id.'1234567890',$tsk->name, date('m/d/Y',strtotime($tsk->created_at)), (empty($tsk->due_date) || strtotime($tsk->due_date) == 0) ? date('m/d/Y') : date('m/d/Y',strtotime($tsk->due_date)), '00ffff', url("/task/{$tsk->task_id}"), 0, $assigned_to, $completion, 0, $val->id, 1, '', '');
                    }
                }
                $childs = !empty($val->children_ids) ? explode(',',$val->children_ids) : false;
                
                if($childs) {
                    foreach($childs as $c_id) {
                        $ft_array = $this->getFolderTaskListRecursive($folders_with_tasks, $c_id, $ft_array, $f_id, $user_list);
                    }
                }

                return $ft_array;
            }
        }
    }

}
