<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\User;
use Illuminate\Http\Request;
use DB;
use App\Mail\CustomResetPassword;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function updateuserpassword(Request $request){
        $request->validate([
            'email' => 'required',
            'password' => 'required',
            'password_confirmation'=> 'required',
            'token' => 'required',
        ]);

        $pwd_reset = DB::table('password_resets')->where('email', $request->email )->where('token', $request->token )->first(); 
        if($pwd_reset){
            $user = User::where('email', $request->email)->first();
            $user->password= Hash::make($request->password);
            $user->save();
            return redirect()->route('login')->with('success','Password reset successfully!');
        } else {
            return redirect()->back()->withErrors(['email'=>'Invalid match']);
        }

        

    }

    public function sendResetPassword(Request $request){
        //dd( $request->all() );
        $this->validateEmail($request);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        $pwd_reset = DB::table('password_resets')->where('email', $request->email )->first(); 
        //dd($pwd_reset);
        $data = array
                        (
                            'token' => $pwd_reset->token,
                        );
        $user = USER::where('email',$request->email)->first();

        // Mail::to($user->email)->send(new CustomResetPassword($data));

        // Mail::send(['html'=>'emails.test_mail'], $data , 
        //                 function($mail) use ($user) {
        //                     $mail->to( $user->email ,  $user->name )
        //                         ->subject('Reset password ')
        //                         ->from('tasks@silentbeacon.com','Task Management Admin');
        //                 }
        //             );

        if( $response == Password::RESET_LINK_SENT )
        {
            Mail::to($user->email)->send(new CustomResetPassword($data));
            return $response  = $this->sendResetLinkResponse($response) ;
            
        }
        else
        {
            return $response  = $this->sendResetLinkFailedResponse($request, $response);
            
        }
        // return $response == Password::RESET_LINK_SENT
        //             ? $this->sendResetLinkResponse($response)
        //             : $this->sendResetLinkFailedResponse($request, $response);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
}
