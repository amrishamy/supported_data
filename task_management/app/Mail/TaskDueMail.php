<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TaskDueMail extends Mailable
{
    use Queueable, SerializesModels;
    public $task;
    public $assigned_by_name ;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($task, $assigned_by_name)
    {
        $this->task = $task;
        $this->assigned_by_name = $assigned_by_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->view('emails.task_due')->subject('Your Task is Due')->from('tasks@silentbeacon.com','Task Management Admin')->with([ 'task'=> $this->task, 'assigned_by_name'=> $this->assigned_by_name ]);
    }
}
