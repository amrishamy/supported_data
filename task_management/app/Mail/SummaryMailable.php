<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use \App\Http\Controllers\Task\TaskController;
use App\Task;
use App\Comment;
use App\Folder;
use App\User;

class SummaryMailable extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $task = new TaskController(new Folder,new Task,new Comment);
        // $data = $task->index();
        $data = $task->new_tasks();
       

        return $this->view('emails.summary_mail')->subject('Your Task Summary')->from('tasks@silentbeacon.com','Task Management Admin')->with( [ 'folders_with_tasks_new'=> $data['folder_task_new'], 'folders_with_tasks_due'=> $data['folder_task_due'], 'folders_with_tasks_past_due'=> $data['folder_task_past_due'], 'user'=> $this->user ] );
    }
}
