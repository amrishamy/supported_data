<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Task extends Model
{
    //
    
        protected $dates = [
        'created_at',
        'updated_at',
        'due_date'
        ];

        public function assignments(){
            
            return $this->hasMany('App\TaskAssignment', 'task_id');
        }

        public function is_assigned_to_me(){
            
            $r = $this->hasMany('App\TaskAssignment', 'task_id')->where('assigned_to','=',Auth::user()->id);

            if($r->count())
                return true;
            return false;
        }

        public function assigned_to_who(){
            
            $r = $this->hasOne('App\TaskAssignment', 'task_id')->where('assigned_by','=',Auth::user()->id);
            
            if($r->count() == 0 )
                $r = $this->hasOne('App\TaskAssignment', 'task_id')->where('assigned_to','=',Auth::user()->id);

            if($r->count() == 0 )
                $r = $this->taskAssignedToLast();

            return $r;
            
        }

                
        public function taskAssignedToFirst(){
            
            //return $this->hasMany('App\TaskAssignment', 'task_id')->where('assigned_to','=',Auth::user()->id)->orderBy('id', 'asc');
            
            return $this->hasOne('App\TaskAssignment', 'task_id')->oldest();
    
        }

     
        public function taskAssignedToLast(){
            
            
            return $this->hasOne('App\TaskAssignment', 'task_id')->latest();
    
        }
        
        public function users()
        {
            return $this->belongsTo('App\User', 'assigned_to');
        }

        public function createdBy()
        {
            return $this->belongsTo('App\User', 'created_by');
        }

        public function comments()
        {
            return $this->hasMany('App\Comment','task_id');
        }

        public function unread_comments_count()
        {
            return $this->hasMany('App\Comment','task_id')->where('is_read', '=', 0 )
            ->where('user_id', '!=' , Auth::user()->id );
        }

        public static function task_count($uid)
        {
            $results = DB::table('tasks')
                     ->leftJoin('task_assignments', function($join)
                         {
                             $join->on('tasks.id', '=', 'task_assignments.task_id');
                         })
                     ->where('task_assignments.status', '=', 0 )
                     ->where('task_assignments.assigned_to', '=', $uid )
                     ->get();

            return $results->count() ;
        }

        public function further_assigned()
        {

            $results = DB::table('tasks')
                     ->leftJoin('task_assignments', function($join)
                         {
                             $join->on('tasks.id', '=', 'task_assignments.task_id');
                             $join->on('tasks.created_by','=', 'task_assignments.created_by' );
                         })
                     ->where('task_assignments.assigned_by', '=', Auth::user()->id )
                     ->select('tasks.id', 'tasks.name', 'task_assignments.task_id', 'task_assignments.assigned_by', 'task_assignments.assigned_to AS further_assigned_to' ) ->first();

            return $results ;
                     

        }
        
}
