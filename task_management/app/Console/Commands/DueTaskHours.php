<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

use App\Mail\DueTaskHoursMail;

use App\Task;
use App\Folder;
use App\User;
use DB;

class DueTaskHours extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'duetask:hours';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Due Task Hourely';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        DB::enableQueryLog();
        
        $total_due_task = Task::where('due_date','like', '%'. date('Y-m-d H', strtotime("+12 hours")).'%' )
        ->orWhere('due_date', 'like', '%'.date('Y-m-d H', strtotime("+1 hours")).'%' )->get();
        //dd(DB::getQueryLog());
        //die('comes here');
        if(count($total_due_task)>0){
            foreach($total_due_task as $key => $task){
                foreach( $task->assignments as $assignment)
                {   
                    if( !empty($assignment->assigned_to ) )
                    {
                        $assigned_by_name = $assignment->usersb->name ;
                        $task['user_info'] = User::where('id', $assignment->assigned_to )->first();
                        $task['login_url'] = URL::to('/');
                        Mail::to($task['user_info']['email'])->send(new DueTaskHoursMail($task, $assigned_by_name ));
                        $this->info('For 12 or 1 hours:-'.$assignment->usersb->name );
                    }
                }
            }
        }
        // amrishk@impingeonline.com

        $this->info('Task Due Hour basis email sent successfully!');

    }
}
