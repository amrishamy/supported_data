<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use App\Mail\TaskDueMail;


use App\Task;
use App\Folder;
use App\User;
use DB;

class DueTasks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'registered:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Task Due Day basis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::enableQueryLog();
        $total_due_task = Task::where('due_date', 'like', '%'. date('Y-m-d', strtotime("+2 days")).'%' )
        ->orWhere('due_date','like', '%'.date('Y-m-d',strtotime("+1 day")).'%' )->get();
        
        //dd(DB::getQueryLog());
        //die();
        if(count($total_due_task)>0){ 
            foreach($total_due_task as $key => $task){
                foreach( $task->assignments as $assignment)
                {   //echo "<pre>";print_r($assignment->assigned_to);
                    if( !empty($assignment->assigned_to ) )
                    {
                        $assigned_by_name = $assignment->usersb->name ;
                        $task['user_info'] = User::where('id', $assignment->assigned_to )->first();
                        $task['login_url'] = URL::to('/');
                        Mail::to($task['user_info']['email'])->send(new TaskDueMail($task, $assigned_by_name ));
                        //Mail::to("amrishk@impingeonline.com")->send(new TaskDueMail($task));
    
                        $this->info('For 48 or 24 hours:-'.$task['user_info']['email']);
                    }

                }
            }
        }
        
        // amrishk@impingeonline.com

        $this->info('Task Due messages sent successfully!');
    }
}
