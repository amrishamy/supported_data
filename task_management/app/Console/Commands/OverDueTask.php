<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

use App\Mail\OverDueTaskMail;

use App\Task;
use App\Folder;
use App\User;
use DB;

class OverDueTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'overdue:tasks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Over Due Tasks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        // send emails for Over Due Task
        $over_due_task = Task::where('due_date','like', '%'. date('Y-m-d H', strtotime("-1 hours")).'%' )->get();

        //dd(DB::getQueryLog());
        //die('comes here');
        if(count($over_due_task)>0){
            foreach($over_due_task as $key => $task){
                foreach( $task->assignments as $assignment)
                {
                    if( !empty($assignment->assigned_to ) )
                    {
                        $assigned_by_name = $assignment->usersb->name ;
                        $task['user_info'] = User::where('id', $assignment->assigned_to )->first();
                        $task['login_url'] = URL::to('/');
                        Mail::to($task['user_info']['email'])->send(new OverDueTaskMail($task, $assigned_by_name ));
                        Mail::to($task->createdBy->email)->send(new OverDueTaskMail($task, $assigned_by_name ));
                        //Mail::to('pawanvir.impinge@gmail.com')->send(new OverDueTaskMail($task));
                        $this->info('For over due:-'.$task->createdBy->name);
                    }

                }
            }
        }


        $this->info('Over Due Email sent successfully!');
    }
}
