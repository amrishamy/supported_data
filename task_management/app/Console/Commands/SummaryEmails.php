<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use App\Mail\SummaryMailable;

use Auth;

use App\Task;
use App\Folder;
use App\User;

use App\TaskAssignment;

class SummaryEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'summary:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a summary email to users on daily basis';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        $users = User::where('status','=','Active')->get();
        if(count($users) > 0 ){
            foreach($users as $user){
                //$tasks = Task::where('status','=', 0 )->where('assigned_to','=', $user->id );
                $tasks = Task::task_count( $user->id ) ;
                //echo $tasks ;
                //dd(DB::getQueryLog());
                //dd($tasks);
                Auth::login($user);
                if( $tasks  > 0){
                // echo "<pre>";print_r($tasks);die;
                    //if( $user->name == "Amrish K" )
                    //{
                        
                    Mail::to($user->email)->send(new SummaryMailable($user));
                    //Mail::to('pawanvir.impinge@gmail.com')->send(new SummaryMailable($user));
                    //}
                    $this->info('Summary Emails sent successfully to '.$user->name.'!');
                }
                Auth::logout();
            }
        }
        //Mail::to('yash@mailinator.com')->send(new SummaryMailable($user));
        
    }
}
