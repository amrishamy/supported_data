<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // 'App\Console\Commands\DueTasks',
        Commands\DueTasks::class,
        'App\Console\Commands\SummaryEmails',
        'App\Console\Commands\OverDuetask',
        'App\Console\Commands\DueTaskHours',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        // email for due tasks day basis
        $schedule->command('registered:users')->dailyAt('10:00');; // Run the task daily at 0:00 & 12:00 // 12 houirs
        //$schedule->command('registered:users')->twiceDaily(0, 12); // Run the task daily at 0:00 & 12:00 // 24 hours

        // emails for due task hourly
        $schedule->command('duetask:hours')->hourly(); // 

        // email for over due tasks
        $schedule->command('overdue:tasks')->hourly(); // 

        

        

        $schedule->command('summary:emails')->dailyAt('10:00');; // Run the task every day at midnight
        // $schedule->command('summary:emails')->everyMinute(); // Run the task every minute

        
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
