
<?php

class PushNotification {

    public function sendnotification($date, $catid, $newsname, $typeofaction, $deviceToken, $devicetype, $catname = null,$extraInfo = array(),$categoryItemImagesData = array()) {
        $filedir = dirname(__FILE__);

        if ($devicetype == 1) {
            $passphrase = '';
            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', $filedir . '/cert/apns-prod-cert.pem');
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
            stream_set_blocking($fp, 0);
            if (!$fp)
                return
                        $body = array();
            $body['params'] = array(
                'date' => $date,
                'app_id' => "com.application.kolMevaser",
                //'news_id' => $newsid
                'news_id' => $catid,
                //'news_title' => $newsname
                'news_title' => $catname
            );

            if(count($extraInfo) > 0){
                $body['params']['cat_sub_type'] = $extraInfo['cat_subtype'];
                $body['params']['subCategoryAdLink'] = $extraInfo['subCategoryAdLink'];
                $body['params']['subCategoryAdDuration'] = $extraInfo['subCategoryAdDuration'];
                $body['params']['subCategoryAdDurationMinute'] = $extraInfo['subCategoryAdDurationMinute'];
                $body['params']['cat_title'] = $extraInfo['cat_title'];
                $body['params']['cat_parent_id'] = $extraInfo['cat_parent_id'];
                $body['params']['item_id'] = $extraInfo['item_id'];
                $body['params']['otherCustomURL'] = $extraInfo['categoryItemImagesData'];

            }

            $body['aps'] = array(
                'alert' => $newsname,
                'sound' => 'default'
            );
            //echo "<pre>";print_r($body);die;
            $payload = json_encode($body);
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
            $result = fwrite($fp, $msg, strlen($msg));
            if ($result) {
                $response['flag'] = 's';
                $response['message'] = 'Notification Sent success';
                //return "notification sent success";
            } else {
                $response['flag'] = 'e';
                $response['message'] = 'Notification Not Sent';
                //return "failure";
            }
            usleep(500000);
            $this->checkAppleErrorResponse($fp, $deviceToken);
            fclose($fp);

        } elseif ($devicetype == 0) {
            define('API_ACCESS_KEY', 'AIzaSyA3fXArg5-sBIQ5HcdzN7ufsOBytdymio0');
            $registrationIds = array($deviceToken);
// prep the bundle
            $msg = array
                (
                //'news_title' => $newsname,
                'news_title' => $catname,
                'news_id' => $catid,
                'vibrate' => 1,
                'sound' => 1
            );

            if(count($extraInfo) > 0){
                $msg['cat_sub_type'] = $extraInfo['cat_subtype'];
                $msg['subCategoryAdLink'] = $extraInfo['subCategoryAdLink'];
                $msg['subCategoryAdDuration'] = $extraInfo['subCategoryAdDuration'];
                $msg['subCategoryAdDurationMinute'] = $extraInfo['subCategoryAdDurationMinute'];
                $msg['cat_title'] = $extraInfo['cat_title'];
                $msg['cat_parent_id'] = $extraInfo['cat_parent_id'];
                $msg['item_id'] = $extraInfo['item_id'];
                $msg['otherCustomURL'] = $categoryItemImagesData;
            }

            $fields = array
                (
                'registration_ids' => $registrationIds,
                'data' => $msg
            );

            $headers = array
                (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
        }
    }

        private function checkAppleErrorResponse($fp, $deviceToken) {

   //byte1=always 8, byte2=StatusCode, bytes3,4,5,6=identifier(rowID). Should return nothing if OK.
   $apple_error_response = fread($fp, 6);
   //NOTE: Make sure you set stream_set_blocking($fp, 0) or else fread will pause your script and wait forever when there is no response to be sent.

   if ($apple_error_response) {
        //unpack the error response (first byte 'command" should always be 8)
        $error_response = unpack('Ccommand/Cstatus_code/Nidentifier', $apple_error_response);
var_dump($error_response);
        if ($error_response['status_code'] == '0') {
            $error_response['status_code'] = '0-No errors encountered';
        } else if ($error_response['status_code'] == '1') {
            $error_response['status_code'] = '1-Processing error';
        } else if ($error_response['status_code'] == '2') {
            $error_response['status_code'] = '2-Missing device token';
        } else if ($error_response['status_code'] == '3') {
            $error_response['status_code'] = '3-Missing topic';
        } else if ($error_response['status_code'] == '4') {
            $error_response['status_code'] = '4-Missing payload';
        } else if ($error_response['status_code'] == '5') {
            $error_response['status_code'] = '5-Invalid token size';
        } else if ($error_response['status_code'] == '6') {
            $error_response['status_code'] = '6-Invalid topic size';
        } else if ($error_response['status_code'] == '7') {
            $error_response['status_code'] = '7-Invalid payload size';
        } else if ($error_response['status_code'] == '8') {
            $error_response['status_code'] = '8-Invalid token';
        } else if ($error_response['status_code'] == '255') {
            $error_response['status_code'] = '255-None (unknown)';
        } else {
            $error_response['status_code'] = $error_response['status_code'] . '-Not listed';
        }

        //echo '<br><b>+ + + + + + ERROR</b> Response Command:<b>' . $error_response['command'] . '</b>&nbsp;&nbsp;&nbsp;Identifier:<b>' . $error_response['identifier'] . '</b>&nbsp;&nbsp;&nbsp;Status:<b>' . $error_response['status_code'] . '</b><br>';
        //echo $deviceToken . 'Identifier is the rowID (index) in the database that caused the problem, and Apple will disconnect you from server. To continue sending Push Notifications, just start at the next rowID after this Identifier.<br>';

        return true;
   }
   return false;
}

}

?>


