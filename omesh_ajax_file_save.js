onChange: function(el) {
let id = el.getAttribute('id');
let type = el.getAttribute('data-type');
let userid = el.getAttribute('data-userid'); // Applicant/Member ID
let btn_id = el.getAttribute('data-uploadbtnid');
let blk = el.getAttribute('data-usernum'); // Applicant/Member number
let key = el.getAttribute('data-dockey'); // Document slug

let url, flex_grid_blk;
if(type=='applicant') {
url = 'organizations/upload_applicant_documents_through_fp_profile';
flex_grid_blk = 'applicant_'+userid+'.flex-grid';
} else if(type=='member') {
url = 'organizations/upload_household_member_documents_through_fp_profile';
flex_grid_blk = 'member_'+userid+'.flex-grid';
}

if(el.files.length > 0) {
formdata = new FormData();
formdata.append('blk', blk);
formdata.append('key', key);
formdata.append('userid', userid);
formdata.append("file", el.files[0]); 

$.ajax({
type: 'POST',
url: getBaseUrl()+url,
data: formdata,
processData: false,
contentType: false,
beforeSend: function() { 
$('#'+btn_id).html('<div class="f-grid rect-loader"><div class="rect1"></div><div class="rect2"></div><div class="rect3"></div></div>');
$('#'+btn_id).attr('disabled',true);
$('#'+btn_id).removeAttr('onclick');
},
success: function(data) {
let res = JSON.parse(data);
if(res.error) {
$('#'+btn_id).attr('disabled',false);
$('#'+btn_id).html('<i class="fa fa-cloud-upload" style="color:#ddd;"></i>');
$('#'+btn_id).attr('onclick','window.app.prospective_caregiver.upload.onClick(this);');

alert(res.message.join("\n"));
} else {
// Reload flexigrid
$("#"+flex_grid_blk).flexReload();
}
},
error: function(jqXHR, exception) {
$('#'+btn_id).attr('disabled',false);
$('#'+btn_id).html('<i class="fa fa-cloud-upload" style="color:#ddd;"></i>');
$('#'+btn_id).attr('onclick','window.app.prospective_caregiver.upload.onClick(this);');

alert('<b>ERROR:</b> '+jqXHR.status+': '+jqXHR.responseText+': '+exception);
}
});
}
}