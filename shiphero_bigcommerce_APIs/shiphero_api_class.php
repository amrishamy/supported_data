<?php
class Shiphero_api{
	    
        private $ship_hero_token;
       
        
        


        function __construct($ship_hero_token,$ship_hero_shop_name){
             $this->ship_hero_token = $ship_hero_token;
             $this->ship_hero_shop_name = $ship_hero_shop_name;

        }




		function get_product_by_sku($sku)
		{
			$ship_hero_token 		= $this->ship_hero_token;	
			//echo "\n https://api-gateway.shiphero.com/v1/general-api/get-product?token=$ship_hero_token&sku=".urlencode($sku)."";
			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://api-gateway.shiphero.com/v2/products?token=$ship_hero_token&sku=".urlencode($sku)."",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
			    "cache-control: no-cache",
			    
			  ),
			));

			$response = curl_exec($curl);
			

			$err = curl_error($curl);

			curl_close($curl);

			if ($err) { 
			  return $err;exit;
			} else {
				//echo "\n<pre>"; print_r(json_decode($response)); echo "</pre>"; die;
			  return $response;
			}
						




		}



		function update_inventory_warehouse($sku)
		{
			$ship_hero_token 		= $this->ship_hero_token;	
			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api-gateway.shiphero.com/v1/general-api/update-inventory/",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "{\n    \"token\": \"$ship_hero_token\",\n    \"products\": [\n        {\n            \"sku\": \"$sku\",\n            \"title\": \"Test Product updated\",\n            \"warehouse\": \"Primary\"\n        }\n        \n    ]\n}",
			CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"content-type: application/json"
			
			),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			echo "cURL Error #:" . $err;
			} else {
			echo $response;
			}
						




		}



		function update_order_warehouse($order_number,$partner_line_item_id,$line_item_sku,$warehouse)
		{
			$ship_hero_token 			= $this->ship_hero_token;
			$ship_hero_shop_name 		= $this->ship_hero_shop_name;
			


			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api-gateway.shiphero.com/v1.2/general-api/order-update",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "{\n    \"token\": \"$ship_hero_token\",\n    \"shop_name\":\"$ship_hero_shop_name\",\n    \"order_id\":$order_number,\n    \"line_items\": [\n    {\n      \"id\" :  $partner_line_item_id,\n      \"default_warehouse\":\"$warehouse\"\n    }\n  ]\n}\n\n",
			CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"content-type: application/json",
			"postman-token: 45bb9bf2-3f0a-e9ed-57c3-13a2ddc5b05c",
			"x-api-key: 9d42eff64f65866edf42ac2440ece015f88d1547"
			),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			return $err;exit;
			} else {
			return $response;
			}



		}


		function update_order_item_backorder_quantity($order_number,$partner_line_item_id,$line_item_sku)
		{
			$ship_hero_token 			= $this->ship_hero_token;
			$ship_hero_shop_name 		= $this->ship_hero_shop_name;
			


			$curl = curl_init();

			curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api-gateway.shiphero.com/v1.2/general-api/order-update",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => "{\n    \"token\": \"$ship_hero_token\",\n    \"shop_name\":\"$ship_hero_shop_name\",\n    \"order_id\":$order_number,\n    \"line_items\": [\n    {\n      \"id\" :  $partner_line_item_id,\n      \"backorder_quantity\":1\n    }\n  ]\n}\n\n",
			CURLOPT_HTTPHEADER => array(
			"cache-control: no-cache",
			"content-type: application/json",
			"postman-token: 45bb9bf2-3f0a-e9ed-57c3-13a2ddc5b05c",
			"x-api-key: 9d42eff64f65866edf42ac2440ece015f88d1547"
			),
			));


			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			return $err;exit;
			} else {
			return $response;
			}



		}






}

?>