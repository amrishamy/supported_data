<?php
class Bigcommerce_api{
	    
        private $bigcommerce_store_id;
        private $x_auth_client;
        private $x_auth_token;
        
        


        function __construct($bigcommerce_store_id,$x_auth_client,$x_auth_token){
             $this->bigcommerce_store_id = $bigcommerce_store_id;
             $this->x_auth_client = $x_auth_client;
             $this->x_auth_token = $x_auth_token;
            

        }




function get_products($page){


			$bigcommerce_store_id 		= $this->bigcommerce_store_id;		
			$x_auth_client			 	= $this->x_auth_client;		
			$x_auth_token 				= $this->x_auth_token;	

			$http_header_array = array('X-Auth-Client: '.$x_auth_client.' ','X-Auth-Token: '.$x_auth_token.' ','Accept: application/json','Content-Type: application/json');

			//$http_header_array = array('X-Auth-Client: '.$x_auth_client.' ','X-Auth-Token: '.$x_auth_token.' ','Accept: application/json','Content-Type: application/json');

			//print_r($http_header_array);die();

			$ch_big = curl_init();
			curl_setopt($ch_big, CURLOPT_URL, "https://api.bigcommerce.com/stores/$bigcommerce_store_id/v3/catalog/products?page=$page");// for our store


			curl_setopt($ch_big, CURLOPT_HTTPHEADER, $http_header_array); // For our store products



			curl_setopt($ch_big, CURLOPT_RETURNTRANSFER, TRUE);

		//curl_setopt($ch_big, CURLOPT_HTTPHEADER, 1);


		 $response = curl_exec($ch_big); 
		
		$err = curl_error($ch_big);

		//curl_close($curl);

		if ($err) {
		  return $err;exit;
		} else {
		  return $response;
		}




}






function add_custom_fields($req_method,$pro_id,$field_value){


			$bigcommerce_store_id 		= $this->bigcommerce_store_id;		
			$x_auth_client			 	= $this->x_auth_client;		
			$x_auth_token 				= $this->x_auth_token;		


				$http_header_array = array('X-Auth-Client: '.$x_auth_client.' ','X-Auth-Token: '.$x_auth_token.' ','Accept: application/json','Content-Type: application/json');


			$curl = curl_init();
			curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.bigcommerce.com/stores/$bigcommerce_store_id/v3/catalog/products/$pro_id/custom-fields",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  //CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => $req_method,
		  CURLOPT_POSTFIELDS => "{\n  \"name\": \"Expected Date\",\n  \"value\": \"$field_value\"\n}",
		  CURLOPT_HTTPHEADER => $http_header_array,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		//curl_close($curl);

		if ($err) {
		  return $err;exit;
		} else {
		  return $response;
		}




}



function update_custom_fields($req_method,$pro_id,$field_id,$field_value){
						
			$bigcommerce_store_id 		= $this->bigcommerce_store_id;		
			$x_auth_client			 	= $this->x_auth_client;		
			$x_auth_token 				= $this->x_auth_token;


				$http_header_array = array('X-Auth-Client: '.$x_auth_client.' ','X-Auth-Token: '.$x_auth_token.' ','Accept: application/json','Content-Type: application/json');


			$curl = curl_init();
			curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.bigcommerce.com/stores/$bigcommerce_store_id/v3/catalog/products/$pro_id/custom-fields/$field_id",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  //CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => $req_method,
		  CURLOPT_POSTFIELDS => "{\n  \"name\": \"Expected Date\",\n  \"value\": \"$field_value\"\n}",
		  CURLOPT_HTTPHEADER => $http_header_array,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		//curl_close($curl);

		if ($err) {
		  return $err;exit;
		} else {
		  return $response;
		}




}




function delete_custom_fields($req_method,$pro_id,$field_id,$field_value){
						
			$bigcommerce_store_id 		= $this->bigcommerce_store_id;		
			$x_auth_client			 	= $this->x_auth_client;		
			$x_auth_token 				= $this->x_auth_token;


				$http_header_array = array('X-Auth-Client: '.$x_auth_client.' ','X-Auth-Token: '.$x_auth_token.' ','Accept: application/json','Content-Type: application/json');


			$curl = curl_init();
			curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.bigcommerce.com/stores/$bigcommerce_store_id/v3/catalog/products/$pro_id/custom-fields/$field_id",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  //CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => $req_method,
		  CURLOPT_HTTPHEADER => $http_header_array,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		//curl_close($curl);

		if ($err) {
		  return $err;exit;
		} else {
		  return $response;
		}




}





function get_all_variants($page){


			$bigcommerce_store_id 		= $this->bigcommerce_store_id;		
			$x_auth_client			 	= $this->x_auth_client;		
			$x_auth_token 				= $this->x_auth_token;	

			$http_header_array = array('X-Auth-Client: '.$x_auth_client.' ','X-Auth-Token: '.$x_auth_token.' ','Accept: application/json','Content-Type: application/json');

			//$http_header_array = array('X-Auth-Client: '.$x_auth_client.' ','X-Auth-Token: '.$x_auth_token.' ','Accept: application/json','Content-Type: application/json');

			//print_r($http_header_array);die();

			$ch_big = curl_init();
			curl_setopt($ch_big, CURLOPT_URL, "https://api.bigcommerce.com/stores/$bigcommerce_store_id/v3/catalog/variants?page=$page");// for our store


			curl_setopt($ch_big, CURLOPT_HTTPHEADER, $http_header_array); // For our store products



			curl_setopt($ch_big, CURLOPT_RETURNTRANSFER, TRUE);

		//curl_setopt($ch_big, CURLOPT_HTTPHEADER, 1);


		 $response = curl_exec($ch_big); 
		
		$err = curl_error($ch_big);

		//curl_close($curl);

		if ($err) {
		  return $err;exit;
		} else {
		  return $response;
		}




}


function update_products_variant($req_method,$pro_id,$variant_id,$variant_expected_date){
						
			$bigcommerce_store_id 		= $this->bigcommerce_store_id;		
			$x_auth_client			 	= $this->x_auth_client;		
			$x_auth_token 				= $this->x_auth_token;


				$http_header_array = array('X-Auth-Client: '.$x_auth_client.' ','X-Auth-Token: '.$x_auth_token.' ','Accept: application/json','Content-Type: application/json');


			$curl = curl_init();
			curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.bigcommerce.com/stores/$bigcommerce_store_id/v3/catalog/products/$pro_id/variants/$variant_id",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  //CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => $req_method,
		  CURLOPT_POSTFIELDS => "{\n  \n  \"bin_picking_number\": \"$variant_expected_date\"\n \n}",
		  CURLOPT_HTTPHEADER => $http_header_array,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		//curl_close($curl);

		if ($err) {
		  return $err;exit;
		} else {
		  return $response;
		}




}



function add_custom_fields_by_variants($req_method,$pro_id,$field_name,$field_value){


			$bigcommerce_store_id 		= $this->bigcommerce_store_id;		
			$x_auth_client			 	= $this->x_auth_client;		
			$x_auth_token 				= $this->x_auth_token;		


				$http_header_array = array('X-Auth-Client: '.$x_auth_client.' ','X-Auth-Token: '.$x_auth_token.' ','Accept: application/json','Content-Type: application/json');


			$curl = curl_init();
			curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.bigcommerce.com/stores/$bigcommerce_store_id/v3/catalog/products/$pro_id/custom-fields",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  //CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => $req_method,
		  CURLOPT_POSTFIELDS => "{\n  \"name\": \"$field_name\",\n  \"value\": \"$field_value\"\n}",
		  CURLOPT_HTTPHEADER => $http_header_array,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		//curl_close($curl);

		if ($err) {
		  return $err;exit;
		} else {
		  return $response;
		}




}


function update_custom_fields_by_variants($req_method,$pro_id,$field_id,$field_name,$field_value){
						
			$bigcommerce_store_id 		= $this->bigcommerce_store_id;		
			$x_auth_client			 	= $this->x_auth_client;		
			$x_auth_token 				= $this->x_auth_token;


				$http_header_array = array('X-Auth-Client: '.$x_auth_client.' ','X-Auth-Token: '.$x_auth_token.' ','Accept: application/json','Content-Type: application/json');


			$curl = curl_init();
			curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.bigcommerce.com/stores/$bigcommerce_store_id/v3/catalog/products/$pro_id/custom-fields/$field_id",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  //CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => $req_method,
		  CURLOPT_POSTFIELDS => "{\n  \"name\": \"$field_name\",\n  \"value\": \"$field_value\"\n}",
		  CURLOPT_HTTPHEADER => $http_header_array,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		//curl_close($curl);

		if ($err) {
		  return $err;exit;
		} else {
		  return $response;
		}




}

/******************now functions start for update order_comments using the v2 API *************************/


public function get_all_orders()
{
		$bigcommerce_store_id 		= $this->bigcommerce_store_id;		
		$x_auth_client			 	= $this->x_auth_client;		
		$x_auth_token 				= $this->x_auth_token;	

		$http_header_array = array('X-Auth-Client: '.$x_auth_client.' ','X-Auth-Token: '.$x_auth_token.' ','Accept: application/json','Content-Type: application/json');

		$curl = curl_init();

		  curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.bigcommerce.com/stores/$bigcommerce_store_id/v2/orders",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => $http_header_array,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) 
		{
		  return $err;exit;
		} 
		else 
		{
		  return $response;
		}


}


public function get_all_products_by_order_id($order_id)
{
		$bigcommerce_store_id 		= $this->bigcommerce_store_id;		
		$x_auth_client			 	= $this->x_auth_client;		
		$x_auth_token 				= $this->x_auth_token;	

		$http_header_array = array('X-Auth-Client: '.$x_auth_client.' ','X-Auth-Token: '.$x_auth_token.' ','Accept: application/json','Content-Type: application/json');

		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.bigcommerce.com/stores/$bigcommerce_store_id/v2/orders/$order_id/products",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => $http_header_array,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) 
		{
		return $err;exit;
		} 
		else 
		{
		return $response;
		}


}


public function get_all_custom_fields_by_product_id($product_id)
{
		$bigcommerce_store_id 		= $this->bigcommerce_store_id;		
		$x_auth_client			 	= $this->x_auth_client;		
		$x_auth_token 				= $this->x_auth_token;	

		$http_header_array = array('X-Auth-Client: '.$x_auth_client.' ','X-Auth-Token: '.$x_auth_token.' ','Accept: application/json','Content-Type: application/json');

		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.bigcommerce.com/stores/$bigcommerce_store_id/v2/products/$product_id/customfields",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => $http_header_array,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) 
		{
		return $err;exit;
		} 
		else
		{
		return $response;
		}


}


public function get_product_info_by_product_id($product_id)
{
		$bigcommerce_store_id 		= $this->bigcommerce_store_id;		
		$x_auth_client			 	= $this->x_auth_client;		
		$x_auth_token 				= $this->x_auth_token;	

		$http_header_array = array('X-Auth-Client: '.$x_auth_client.' ','X-Auth-Token: '.$x_auth_token.' ','Accept: application/json','Content-Type: application/json');

		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.bigcommerce.com/stores/$bigcommerce_store_id/v2/products/$product_id",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => $http_header_array,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) 
		{
		return $err;exit;
		} 
		else
		{
		return $response;
		}


}


public function update_order_comments($order_id,$single_order_comments)
{
		$bigcommerce_store_id 		= $this->bigcommerce_store_id;		
		$x_auth_client			 	= $this->x_auth_client;		
		$x_auth_token 				= $this->x_auth_token;	

		$http_header_array = array('X-Auth-Client: '.$x_auth_client.' ','X-Auth-Token: '.$x_auth_token.' ','Accept: application/json','Content-Type: application/json');

		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.bigcommerce.com/stores/$bigcommerce_store_id/v2/orders/$order_id",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "PUT",
		CURLOPT_POSTFIELDS => "{\n\t\"customer_message\":\"$single_order_comments\"\n}",
		CURLOPT_HTTPHEADER => $http_header_array,
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) 
		{
		return $err;exit;
		}
		else
		{
		return $response;
		}


}


public function send_order_message_email($customer_email,$customer_fname,$order_id,$shipped_products)
{
			// multiple recipients
			
			$to = $customer_email;

			// subject
			$subject = 'Order message for order #'.$order_id;

			// message
			$message ="";
			$message .= 'Hi $customer_fname, \n';
			$message .= '\n This is regarding your order number #'.$order_id;
			$message .= '\n We would like to bring into your attention that following products in this order will be shipped after following dates -';
			$message .=  '
			<table>
			'.$shipped_products.'
			</table>
			';
			$message .= 'Thank you, \n'; 
			$message .= 'Pinks and Greens';


			// To send HTML mail, the Content-type header must be set
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

			// Additional headers
			$headers .= 'From: Birthday Reminder <birthday@example.com>' . "\r\n";

			// Mail it
			mail($to, $subject, $message, $headers);


}

function smtpmailer($to, $from, $from_name, $customer_fname, $order_id, $shipped_products)  
{ 
			
			$subject = 'Order message for order #'.$order_id;

			$message ="";
			$message .=  '
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td height="25">

        </td>
    </tr>
    <tr>
        <td align="center">
            <table width="600" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td align="center"><a href="https://www.pinksandgreens.com" target="_blank"><img src="https://cdn3.bigcommerce.com/s-3uqkp2wrwr/product_images/uploaded_images/logo-pg.jpg" alt="" width="300" border="0" /></a></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="50"><hr style="border-top: none;" /></td>
    </tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td height="30">

<p style="font-size: 16px; font-weight: 400;  font-family: Helvetica, Arial, sans-serif; color:#000000; ">Hi <strong>'.$customer_fname.', </strong><p>
<h2 style="font-size: 16px; font-weight: 400;  font-family: Helvetica, Arial, sans-serif; color:#000000; " align="left">
This is regarding your order number # <strong>'.$order_id.'</strong></h2>

<p style="font-size: 14px; font-weight: 400;  font-family: Helvetica, Arial, sans-serif; color:#666666;" align="left">We would like to bring into your attention that following products in this order will be shipped after following dates</p>
<br/><br/>


        </td>
    </tr>
    <tr>
        <td align="left">
            <table width="600" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse; border:1px solid #e3e3e3; border-top:0px">
            <thead>
             <tr>
            <td colspan="3" height="1" style="font-size:1px; line-height:1px; background-color:#000" bgcolor="#000"></td>
            </tr>
            <tr>
            <th width="182" align="left" style="font-size: 16px; font-weight: 700;  font-family: Helvetica, Arial, sans-serif; background:#666; color:#fff; padding:10px;border-left:1px solid #666; ">Image</th>
            <th width="275" align="left" style="font-size: 16px; font-weight: 700;  font-family: Helvetica, Arial, sans-serif; background:#666; color:#fff; padding:10px;  border-left:1px solid #000;">Name</th>
            <th width="143" align="left" style="font-size: 16px; font-weight: 700;  font-family: Helvetica, Arial, sans-serif; background:#666; color:#fff; padding:10px;  border-left:1px solid #000; border-right:1px solid #666;">Ship Date</th>
            </tr>
            </thead>
            <tbody>

            '.$shipped_products.'
   
            
            </tbody>
            
            
                
            </table>
        </td>
    </tr>
    <tr>
        <td height="60">
<br><br><p style="font-size: 14px; font-weight: 400;  font-family: Helvetica, Arial, sans-serif; color:#666; ">Thank you,</p>
<p style="font-size: 14px; font-weight: 700;  font-family: Helvetica, Arial, sans-serif; color:#000000;">Pinks and Greens</p>
<br><br>
        </td>
    </tr>
   
</table>
<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#f8bbd6">
    <tr>
        <td align="center">
            <table width="600" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td height="30"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <p style="margin: 0 0 12px"><span style="font-size: 14px; color: #3e484d; font-family: Helvetica, Arial, sans-serif;">Pinks and Greens</span></p>
                                                    <p style="margin: 0 0 7px"><span style="font-size: 14px; color: #3e484d; font-family: Helvetica, Arial, sans-serif;"><span style="color: #eb0966; font-weight: bold; font-family: Helvetica, Arial, sans-serif;">Call Us:</span> 877-421-2916</span></p>
                                                    <p style="margin: 0 0 7px"><small style="font-size: 14px; color: #3e484d; font-family: Helvetica, Arial, sans-serif;"> 2<span>248</span> Columbia Turnpike <span>#304</span> Florham Park, NJ 07932 </small></p>
                                                    <p style="margin:0">
                                                        <a href="https://www.pinksandgreens.com/golf/" style="text-decoration: underline; line-height: inherit; color: #3e484d;" target="_blank"><span style="text-decoration: underline; color: #3e484d; font-family: Helvetica, Arial, sans-serif; font-size: 14px; ">Products</span></a>
                                                        <span style="color: #3e484d">&nbsp; • &nbsp;</span>
                                                        <a href="https://www.pinksandgreens.com/pgactive/" style="text-decoration: underline; line-height: inherit; color: #3e484d;" target="_blank"><span style="text-decoration: underline; color: #3e484d; font-family: Helvetica, Arial, sans-serif; font-size: 14px; ">Blog</span></a>
                                                        <span style="line-height: inherit; color: #3e484d">&nbsp; • &nbsp;</span>
                                                        <a href="mailto:help@pinksandgreens.com?subject=Subscription%20Update%20Request&body=Please%20unsubscribe%20me%20from%20all%20mailing%20lists" style="text-decoration: underline; color: #3e484d;" target="_blank"><span style="text-decoration: underline; line-height: inherit; color: #3e484d; font-family: Helvetica, Arial, sans-serif; font-size: 14px; ">Unsubscribe</span></a>
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>




                                    </td>
                                    <td valign="top" align="right">
                                        <a href="#" target="_blank"><img src="https://cdn3.bigcommerce.com/s-3uqkp2wrwr/product_images/uploaded_images/social-facebook-white.png" width="24" height="24" alt="facebook" border="0" /></a>&nbsp;&nbsp;
                                        <a href="#" target="_blank"><img src="https://cdn3.bigcommerce.com/s-3uqkp2wrwr/product_images/uploaded_images/social-twitter-white.png" width="24" height="24" alt="twitter" border="0" /></a>&nbsp;&nbsp;
                                        <a href="#" target="_blank"><img src="https://cdn3.bigcommerce.com/s-3uqkp2wrwr/product_images/uploaded_images/social-instagram-white.png" width="24" height="24" alt="linkedin" border="0" /></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>

</table>
<table width="100%">
<tr><td height="20"></td></tr>
     <tr style="color: #a7b1b6; font-family: Helvetica,Arial,sans-serif; font-size: 12px;"><td align="center">Please reply to this email if you have any questions or give us a call at <span style="color: #eb0966;">877-421-2916</span></td></tr>
    <tr><td height="20"></td></tr>
</table>';

			global $error;
			$mail = new PHPMailer();  // create a new object
			$mail->IsSMTP(); // enable SMTP
			$mail->SMTPDebug = 0;  // debugging: 1 = errors and messages, 2 = messages only
			$mail->SMTPAuth = true;  // authentication enabled
			//$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
			//$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
			//$mail->Host = 'smtp.gmail.com';
			$mail->Host = 'email-smtp.us-west-2.amazonaws.com';
			$mail->Port = 587;   // 25, 465 or 587
			//$mail->Username = "gagand.impinge@gmail.com";  
			$mail->Username = "AKIAJEKZIRK2XR3VOBYQ";  
			//$mail->Password = "!mp5ol^gsga";           
			$mail->Password = "AqKxeQZ/lC1DAAaYq5WrkDybONXc+W00Aps/FJnS7gTZ";           
			$mail->SetFrom($from, $from_name);
			$mail->Subject = $subject;
			$mail->IsHTML(true);
			$mail->Body = $message;
			$mail->AddAddress($to);
			if(!$mail->Send()) {
			$error = 'Mail error: '.$mail->ErrorInfo; 
			return "failed".$mail->ErrorInfo;
			//return false;
			} else {
			$error = 'Message sent!';
			return "Mail sent successfully";
			//return true;
			}
}


}

?>