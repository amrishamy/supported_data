<?php
require_once('connection.php');
if(isset($_REQUEST['page']))
$page  = $_REQUEST['page'];
else
$page  = '';
if(isset($_REQUEST['limit']))
$limit = $_REQUEST['limit'];
else
$limit  = '';
$start = ( $page -1 ) * $limit;

if(!empty($page) && !empty($limit))
{
    get_tweets($con,$page,$start,$limit);  
}
else
{
    echo "Incomplete Data Input";
}


function get_tweets($con,$page,$start,$limit)
{   
    $response = array();
    $media = array();
    
    //$sql = "SELECT * FROM all_tweets order by tweet_id desc ";

    $sql = "SELECT * FROM amy_test order by tweet_id desc LIMIT $start , $limit ";
//echo $sql;
    $query = mysqli_query($con, $sql);
    while($row = mysqli_fetch_assoc($query))
    {   
        $media = array();
        $get_media_sql = "SELECT * FROM twitter_media where tweet_id = ".$row['tweet_id']." ";
        $query_media = mysqli_query($con, $get_media_sql);
        while($row_media = mysqli_fetch_assoc($query_media))
        {
            if($row_media['type'] == 'video'){
            $media[] = array(
                'media_url' => $row_media['media_url'],
                'media_url_https' => $row_media['media_url_https'],
                'type'  =>  $row_media['type'],
                'expanded_url'  =>  $row_media['expanded_url'],
                'video_url' => $row_media['video_url']
                );
            }
            else{
                $media[] = array(
                'media_url' => $row_media['media_url'],
                'media_url_https' => $row_media['media_url_https'],
                'type'  =>  $row_media['type'],
                'expanded_url'  =>  $row_media['expanded_url']
                );
            }
        }

        $pattern = "/https:\/\/t.co\/([a-zA-Z0-9]*)/i";
        $replacement = "";
        $tweet_text = preg_replace($pattern, $replacement, $row['tweet_text']);
        
        $response[] = array(
            'tweet_id'      => $row['tweet_id'],
            //'tweet_text'    => html_entity_decode(html_entity_decode($tweet_text, ENT_QUOTES | ENT_HTML5), ENT_QUOTES | ENT_HTML5).$row['expanded_urls'],
            'tweet_text'    => html_entity_decode(html_entity_decode($tweet_text, ENT_QUOTES | ENT_HTML5), ENT_QUOTES | ENT_HTML5),
            'media'         => $media,
            'datetime'      => date("Y-m-d H:i:s",$row['time_date_of_tweet']),
            'elapsed_time'  => get_friendly_time_ago($row['time_date_of_tweet']),
            'expanded_urls' => $row['expanded_urls']
            );
    }

    //echo "<pre>";print_r($response);die;
    if(!empty($response))
    echo json_encode(array("message"=>"records found","response"=>$response));
    else
        echo json_encode(array("message"=>"records Not found","response"=>$response));

}



function get_friendly_time_ago($distant_timestamp, $max_units = 1) {
    $i = 0;
    $time = time() - $distant_timestamp; // to get the time since that moment
    $tokens = [
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    ];

    $responses = [];
    while ($i < $max_units) {
        foreach ($tokens as $unit => $text) {
            if ($time < $unit) {
                continue;
            }
            $i++;
            $numberOfUnits = floor($time / $unit);

            $responses[] = $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
            $time -= ($unit * $numberOfUnits);
            break;
        }
    }

    if (!empty($responses)) {
        return implode(', ', $responses) . ' ago';
    }

    return 'Just now';
}
?>