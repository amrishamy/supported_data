<?php //echo $_SERVER['DOCUMENT_ROOT'];die;
ini_set('display_errors', 1);
require_once('connection.php');
require_once('TwitterAPIExchange.php');
require_once('functions.php');


$tweet_last_insert_id_query = "SELECT `tweet_id` FROM `all_tweets` ORDER BY `tweet_id` DESC LIMIT 0, 1 ";
$result_last_insert_id = mysqli_query($con, $tweet_last_insert_id_query);
if (mysqli_num_rows($result_last_insert_id) > 0 )
{
    $row = mysqli_fetch_assoc($result_last_insert_id);
    $last_insert_id = $row['tweet_id'];
}
else
{
    $last_insert_id = 0;
}
 
/** Set access tokens here - see: https://dev.twitter.com/apps/ **/
$since_id   = 0;//$last_insert_id;
$count      = 200;

/** URL for REST request, see: https://dev.twitter.com/docs/api/1.1/ **/
$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
$showURL = "https://api.twitter.com/1.1/statuses/show.json";

if($since_id > 0)
{
    //$getfield = '?screen_name='.$screen_name.'&tweet_mode=extended&count='.$count.'&since_id='.$since_id;   
	$getfield = '?screen_name='.$screen_name.'&tweet_mode=extended&count='.$count.'&since_id='.$since_id;	
}
else
{
	$getfield = '?screen_name='.$screen_name.'&tweet_mode=extended&count='.$count;	
}
$requestMethod = 'GET';
$twitter = new TwitterAPIExchange($settings);

$response = $twitter->setGetfield($getfield)->buildOauth($url,$requestMethod)->performRequest();

//echo $response;die;
$response_decoded = json_decode($response);
             
echo "<pre>";print_r(  $response_decoded); echo "</pre>"; die;
if(!empty($response_decoded))
{ 
	echo "Tweets insertion started<br>";
foreach($response_decoded as $status) {
        //echo "<pre>";print_r($status);echo "</pre>";
        // Convert links back into actual links, otherwise they're just output as text
        $enhancedStatus = htmlentities($status->full_text, ENT_QUOTES, 'UTF-8');
        $enhancedStatus = preg_replace('/http:\/\/t.co\/([a-zA-Z0-9]+)/i', '<a href="http://t.co/$1">http://$1</a>', $enhancedStatus);
        $enhancedStatus = preg_replace('/https:\/\/t.co\/([a-zA-Z0-9]+)/i', '<a href="https://t.co/$1">http://$1</a>', $enhancedStatus);

        $tweet_id           =   $status->id;
        $tweet_text         =   $status->full_text;
        $tweet_by           =   $status->user->name;
        $screen_name        =   $status->user->screen_name;
        $time_date_of_tweet =   strtotime($status->created_at) ;
        $followers_count    =   $status->user->followers_count;
        $expanded_urls      =   "";
        
        if(isset($status->entities->urls)){            
            foreach($status->entities->urls as $url_entity){
                $expanded_urls = " ".$url_entity->expanded_url;
            }
        }

        if(isset($status->entities->urls)){            
            foreach($status->entities->urls as $url_entity){
                //$expanded_urls = " ".$url_entity->expanded_url;
                $tweet_text = str_replace($url_entity->url, $url_entity->expanded_url, $tweet_text); 
            }
        }
        
        if(isset($status->extended_entities))
        {
        $media_extended_entities_array = $status->extended_entities->media;         
        }
        else
        {
           $media_extended_entities_array = array(); 
        }
        //echo "start array<pre>";print_r($media_extended_entities_array);echo "</pre>"; 
        
        $tweet_exist_query = "SELECT tweet_id FROM all_tweets where tweet_id = '".$tweet_id."'  ";
        $result = mysqli_query($con,$tweet_exist_query);
        if ( mysqli_num_rows($result) > 0)
        {

        }
        else
        {            
           
            $query = "INSERT INTO all_tweets SET tweet_id='".$tweet_id."', tweet_text='".htmlentities(htmlentities($tweet_text, ENT_QUOTES | ENT_HTML5), ENT_QUOTES | ENT_HTML5)."' , tweeted_by='".$tweet_by."', screen_name='".$screen_name."', time_date_of_tweet='".$time_date_of_tweet."', followers='".$followers_count ."', expanded_urls='".$expanded_urls."' ";
            //echo $query;die;
            mysqli_query($con,$query) ;
            // /$tweet_insert_id = mysqli_insert_id($con) ;
            if(!empty($media_extended_entities_array))
            {                
                foreach($media_extended_entities_array as $k=>$media)
                {
                    $video_url = '';
                    if(trim($media_extended_entities_array[$k]->type) == 'video' && is_array($media_extended_entities_array[$k]->video_info['variants'])){
                        foreach($media_extended_entities_array[$k]->video_info['variants'] as $n => $varArray){
                            if($varArray->content_type == 'video/mp4'){
                                $video_url = $varArray->url;
                            }
                        }                        
                    }
                    $query_media = "INSERT INTO twitter_media SET tweet_id='".$tweet_id."', media_url='".htmlentities($media_extended_entities_array[$k]->media_url)."' , media_url_https='".htmlentities($media_extended_entities_array[$k]->media_url_https)."' , type='".$media_extended_entities_array[$k]->type."' , expanded_url='".$media_extended_entities_array[$k]->expanded_url."' , video_url='".$video_url."'";
                    mysqli_query($con,$query_media) ;
                }
            }
        }
        
        
    }
	
	// Remove Duplicate Records
	$checkDuplicates = "SELECT count(*), tweet_text, GROUP_CONCAT(tweet_id) as tweet_ids FROM `all_tweets` group by tweet_text having count(*) > 1 order by tweet_ids desc";
	$duplicateRows = mysqli_query($con,$checkDuplicates);
	if ( mysqli_num_rows($duplicateRows) > 0){
		while($duplicateRow = mysqli_fetch_assoc($duplicateRows)){			
			if($duplicateRow['tweet_ids'] != ""){
				$arr_tweet_ids = explode(",", $duplicateRow['tweet_ids']);
				if(count($arr_tweet_ids) == 2){
					$deleteRecord = "DELETE FROM `all_tweets` WHERE tweet_id = '".$arr_tweet_ids[0]."'";				
					mysqli_query($con,$deleteRecord) ;
				}
			}
		}
	}

	
	
	
	
    // code start for send push Notifications to registred Device Token
    $stored_device_token = get_device_tokens($con);
    //echo "<pre>";print_r($stored_device_token);
    $message = "There are new updates";
    $passphrase = "";
    //$mode = "SANDBOX";
    $mode = "LIVE";
    foreach ($stored_device_token as $key => $value) {
        $DT = $value['device_token'];
        $notification_status = $value['notification_status'];
        if( $notification_status && !empty($notification_status) )
        send_ios_notification($DT,$message,$passphrase,$mode); 
    }
    //echo "<pre>";print_r($stored_device_token);
    echo "Tweets insertion Finished<br>";
    file_put_contents("/var/www/html/log.txt",json_encode(array("date"=>date("Y-m-d H:i:s"),"since_id"=>$since_id)));
    

}
else
{ 
	echo "No new Tweets found yet";
}


$all_tweets_query = "SELECT `tweet_id` FROM `all_tweets`";
$all_result = mysqli_query($con, $all_tweets_query);

if (mysqli_num_rows($all_result) > 0 ){
    while($row = mysqli_fetch_assoc($all_result)) {
        $check_tweet_id = $row['tweet_id'];       
        $getfield_id = '?screen_name='.$screen_name.'&tweet_mode=extended&count='.$count.'&id='.$check_tweet_id;	
        $tweet_response = $twitter->setGetfield($getfield_id)->buildOauth($showURL,$requestMethod)->performRequest();
        $tweet_decoded = json_decode($tweet_response);
        //echo "========<pre>";print_r($tweet_decoded);echo '</pre><br>========';
        if(isset($tweet_decoded->errors)){
            $errors = $tweet_decoded->errors;
        }
        if(!isset($tweet_decoded->id) && isset($errors) &&  $errors[0]->code == 144){    
            $deletequery = "DELETE FROM `all_tweets` WHERE tweet_id = '".$check_tweet_id."'";
            if(mysqli_query($con,$deletequery)){
                echo "<br>".$check_tweet_id." deleted<br>";
            }		   
        }
        else{
            //echo "exists<br>";
        }
    }    
}



//$getfieldtest = '?screen_name='.$screen_name.'&tweet_mode=extended&count='.$count.'&id=930178992094752768';	
//$testresponse = $twitter->setGetfield($getfieldtest)->buildOauth($showURL,$requestMethod)->performRequest();
//$testdecoded = json_decode($testresponse);
//echo "========<pre>";print_r($testdecoded);echo '</pre><br>========';
//die('here');


?>
