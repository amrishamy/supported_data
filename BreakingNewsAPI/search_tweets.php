<?php
	require_once('connection.php');
	
if(isset($_REQUEST['page']))
	$page  = $_REQUEST['page'];
else
	$page  = '';

if(isset($_REQUEST['limit']))
$limit = $_REQUEST['limit'];
else
$limit  = '';

if(isset($_REQUEST['q']))
$search_keyword = $_REQUEST['q'];
else
$search_keyword = '';

$start = ( $page -1 ) * $limit;

 

if(!empty($page) && !empty($limit) && !empty($search_keyword))
{
    search_get_tweets($con,$page,$start,$limit,$search_keyword);  
}
else
{
    echo "Incomplete Data Input";
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }


    if (!$full) $string = array_slice( $string , 0, 2);
    
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}


function search_get_tweets($con,$page,$start,$limit,$search_keyword)
{   
    $response = array();
    $media = array();
    
    //$sql = "SELECT * FROM all_tweets order by tweet_id desc ";

    $sql = "SELECT * FROM all_tweets where tweet_text LIKE '%$search_keyword%' order by tweet_id desc LIMIT $start , $limit ";
//echo $sql;
    $query = mysqli_query($con, $sql);
    while($row = mysqli_fetch_assoc($query))
    {   
        $media = array();
        $get_media_sql = "SELECT * FROM twitter_media where tweet_id = ".$row['tweet_id']." ";
        $query_media = mysqli_query($con, $get_media_sql);
        while($row_media = mysqli_fetch_assoc($query_media))
        {
            if($row_media['type'] == 'video'){
                $media[] = array(
                    'media_url' => $row_media['media_url'],
                    'media_url_https' => $row_media['media_url_https'],
                    'type'  =>  $row_media['type'],
                    'expanded_url'  =>  $row_media['expanded_url'],
                    'video_url' => $row_media['video_url']
                    );
            }
            else{
                $media[] = array(
                'media_url' => $row_media['media_url'],
                'media_url_https' => $row_media['media_url_https'],
                'type'  =>  $row_media['type'],
                'expanded_url'  =>  $row_media['expanded_url']
                );
            }
        }

        $tweet_text_decoded = html_entity_decode(html_entity_decode($row['tweet_text'], ENT_QUOTES | ENT_HTML5), ENT_QUOTES | ENT_HTML5);

        $pattern = "/https:\/\/t.co\/([a-zA-Z0-9]*)/i";
        $replacement = "";
        $tweet_text_final = preg_replace($pattern, $replacement, $tweet_text_decoded);
        
        $format_date_field = date("Y-m-d H:i:s",$row['time_date_of_tweet']);
        //$format_date_field = time_elapsed_string( date("Y-m-d H:i:s",$row['time_date_of_tweet']) , false );
		
        $response[] = array(
            'tweet_id'      => $row['tweet_id'],
            //'tweet_text'    => html_entity_decode(html_entity_decode($tweet_text, ENT_QUOTES | ENT_HTML5), ENT_QUOTES | ENT_HTML5).$row['expanded_urls'],
            'tweet_text'    => $tweet_text_final,
            'media'         => $media,
            'datetime'      => $format_date_field,
            'elapsed_time'      => get_friendly_time_ago($row['time_date_of_tweet']),
            'expanded_urls' => $row['expanded_urls']
            );
    }

    //echo "<pre>";print_r($response);die;
    if(!empty($response))
		echo json_encode(array("message"=>"records found","response"=>$response));
    else
        echo json_encode(array("message"=>"records Not found","response"=>$response));

}
    

function get_friendly_time_ago($distant_timestamp, $max_units = 1) {
    $i = 0;
    $time = time() - $distant_timestamp; // to get the time since that moment
    $tokens = [
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    ];

    $responses = [];
    while ($i < $max_units) {
        foreach ($tokens as $unit => $text) {
            if ($time < $unit) {
                continue;
            }
            $i++;
            $numberOfUnits = floor($time / $unit);

            $responses[] = $numberOfUnits . ' ' . $text . (($numberOfUnits > 1) ? 's' : '');
            $time -= ($unit * $numberOfUnits);
            break;
        }
    }

    if (!empty($responses)) {
        return implode(', ', $responses) . ' ago';
    }

    return 'Just now';
}
?>
