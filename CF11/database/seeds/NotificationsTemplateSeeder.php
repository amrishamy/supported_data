<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class NotificationsTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('emailtemplates')->insert(
        	[
        	'key' => 'new_gamer_request',
            'subject' => 'New Gamer Request',
			'email_template' => '&#x3C;!DOCTYPE html&#x3E;
&#x3C;html&#x3E;
&#x3C;head&#x3E;
  &#x3C;title&#x3E;&#x3C;/title&#x3E;
&#x3C;/head&#x3E;
&#x3C;body&#x3E;

  &#x3C;p&#x3E;&#x3C;b&#x3E;Hi ##name##,&#x3C;/b&#x3E; &#x3C;/p&#x3E;
  &#x3C;p&#x3E;A Gamer recently requested to play the game. Please response to Gamer request by Login into your ##appname## account. Click on below link to Login into your account&#x3C;/p&#x3E;

  &#x3C;p&#x3E;
    &#x3C;a style=&#x22;color:#f14e4e;font-family: Arial&#x22; href=&#x22;##loginUrl##&#x22;&#x3E;Login&#x3C;/a&#x3E;
  &#x3C;/p&#x3E;

  &#x3C;p&#x3E;Regards&#x3C;br /&#x3E;##appname##&#x3C;/p&#x3E;

&#x3C;/body&#x3E;
&#x3C;/html&#x3E;
',
            'status' => '1',
            ]);
            
        DB::table('emailtemplates')->insert(
            [
            'key' => 'gamer_request_acknowledgement',
            'subject' => 'Game request acknowledgement',
            'email_template' => '&#x3C;!DOCTYPE html&#x3E;
&#x3C;html&#x3E;
&#x3C;head&#x3E;
    &#x3C;title&#x3E;&#x3C;/title&#x3E;
&#x3C;/head&#x3E;
&#x3C;body&#x3E;

    &#x3C;p&#x3E;&#x3C;b&#x3E;Hi ##name##,&#x3C;/b&#x3E; &#x3C;/p&#x3E;
    &#x3C;p&#x3E;Please wait as we are waiting for the celebrity and you will be notified once the celebrity accepts or rejects your offer/request. Also there are many gamers may request to play in the one slot, so you may not be selected this time.

    &#x3C;p&#x3E;Regards&#x3C;br /&#x3E;##appname##&#x3C;/p&#x3E;

&#x3C;/body&#x3E;
&#x3C;/html&#x3E;
',
            'status' => '1',
            ]);

            DB::table('emailtemplates')->insert(
              [
              'key' => 'celebrity_registration_email',
                'subject' => 'Registration Email',
                'email_template' => '&#x3C;!DOCTYPE html&#x3E;
                            &#x3C;html&#x3E;
                            &#x3C;head&#x3E;
                            &#x3C;title&#x3E;&#x3C;/title&#x3E;
                            &#x3C;/head&#x3E;
                            &#x3C;body&#x3E;
    
                            &#x3C;p&#x3E;&#x3C;b&#x3E;Hi ##name##,&#x3C;/b&#x3E; &#x3C;/p&#x3E;
                            &#x3C;p&#x3E;Your account on website has been created successfully. someone from the Bast.gg team will be in touch in order to verify your identity.&#x3C;/p&#x3E;
    
                            
    
                            &#x3C;p&#x3E;Regards&#x3C;br /&#x3E;##appname##&#x3C;/p&#x3E;
    
                            &#x3C;/body&#x3E;
                            &#x3C;/html&#x3E;
                            ',
                'status' => '1',
              ]);

              DB::table('emailtemplates')->insert(
                [
                'key' => 'celebrity_approved_email',
                  'subject' => 'Account Approved Email',
                  'email_template' => '&#x3C;!DOCTYPE html&#x3E;
                              &#x3C;html&#x3E;
                              &#x3C;head&#x3E;
                              &#x3C;title&#x3E;&#x3C;/title&#x3E;
                              &#x3C;/head&#x3E;
                              &#x3C;body&#x3E;
      
                              &#x3C;p&#x3E;&#x3C;b&#x3E;Hi ##name##,&#x3C;/b&#x3E; &#x3C;/p&#x3E;
                              &#x3C;p&#x3E;Your account on website has been approved.
                              Click on below link to Login into your account&#x3C;/p&#x3E;

                              &#x3C;p&#x3E;
                                &#x3C;a style=&#x22;color:#f14e4e;font-family: Arial&#x22; href=&#x22;##loginUrl##&#x22;&#x3E;Login&#x3C;/a&#x3E;
                              &#x3C;/p&#x3E;
                              
      
                              &#x3C;p&#x3E;Regards&#x3C;br /&#x3E;##appname##&#x3C;/p&#x3E;
      
                              &#x3C;/body&#x3E;
                              &#x3C;/html&#x3E;
                              ',
                  'status' => '1',
                ]);
    }
}
