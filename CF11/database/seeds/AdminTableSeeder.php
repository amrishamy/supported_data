<?php
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin')->insert([
            'name' => 'Admin',
            'email' => 'admin@mailinator.com',
            'password' => \Hash::make('123456'),
            'role_id' => 1
        ]);
    }
}
