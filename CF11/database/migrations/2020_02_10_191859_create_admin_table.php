<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdminTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admin', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('name', 191);
			$table->string('email', 191)->unique();
			$table->string('gender', 191)->default('1')->comment('1 for male and 2 for female');
			$table->boolean('role_id')->default(2)->comment('1 for Admin and 2 for users');
			$table->dateTime('email_verified_at')->nullable();
			$table->string('password', 191);
			$table->string('forgotToken', 191)->nullable()->comment('new token generate when forgot password email send');
			$table->string('remember_token', 100)->nullable();
			$table->integer('status')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admin');
	}

}
