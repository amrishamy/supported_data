<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCelebritiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('celebrities', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('name', 191)->nullable();
			$table->string('surname', 191)->nullable();
			$table->string('email', 191)->unique();
			$table->string('image', 191)->nullable();
			$table->string('username', 191);
			$table->string('gamer_id', 191);
			$table->string('mobile', 191);
			$table->string('skype', 191);
			$table->string('twitter', 191);
			$table->string('termsnconditions', 191);
			$table->string('password', 191)->nullable();
			$table->string('forgotToken', 191)->nullable();
			$table->boolean('status')->default(0);
			$table->string('verify_token', 191)->nullable()->comment('verify link send when event manager signup');
			$table->integer('active_status')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('celebrities');
	}

}
