<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('first_name', 191)->nullable();
			$table->string('last_name', 191)->nullable();
			$table->string('mobile_number', 191)->nullable();
			$table->string('gamer_id', 191)->nullable();
			$table->string('email', 191)->unique();
			$table->string('gender', 191)->nullable();
			$table->string('password', 191)->nullable();
			$table->string('forgotToken', 191)->nullable();
			$table->boolean('status')->default(0);
			$table->string('verify_token', 191)->nullable()->comment('verify link send when event manager signup');
			$table->integer('country_id')->nullable()->comment('Manager country');
			$table->integer('state_id')->nullable()->comment('Manager state');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
