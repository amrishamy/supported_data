<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSlotsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('slots', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('title', 191)->default('');
			$table->integer('game_id');
			$table->integer('celebrity_id');
			$table->string('platform_type', 191);
			$table->float('slot_price', 15)->comment('price in dollars');
			$table->date('slot_date');
			$table->string('slot_start_time', 191);
			$table->string('slot_end_time', 191)->nullable();
			$table->integer('status')->default(1);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('slots');
	}

}
