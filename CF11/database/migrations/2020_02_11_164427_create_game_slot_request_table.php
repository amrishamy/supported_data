<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameSlotRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_slot_request', function (Blueprint $table) {
            $table->bigIncrements('id', true)->unsigned();
            $table->integer('user_id')->nullable()->default('0');
            $table->integer('requested_slot_id')->nullable()->default('0');
            $table->integer('requested_platform_id')->nullable()->default('0');
            $table->integer('transaction_id')->nullable()->default('0');
            $table->string('gamer_id')->nullable();
            $table->integer('status')->nullable()->default('0')->comment('0 => not taken any action, 1 => accepted, 2 => denied the request');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_slot_request');
    }
}
