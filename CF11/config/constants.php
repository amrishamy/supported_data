<?php
return [
    'countdowntime' => '+5 minutes',
    'from_email' => env('MAIL_FROM_ADDRESS'),
    'from_name' => env('MAIL_FROM_NAME'),
    'app_name' => env('APP_NAME'),
    'app_url' => env('APP_URL'),
    'mm_url' => env('MM_URL'),
    'socket_ip' => env('SOCKET_IP'),
    'platforms'=>array('1'=>'PS3','2'=>'PS4','3'=>'Xbox One','4'=>'Xbox One X','5'=>'Nintendo Switch','6'=>'PC')
];
