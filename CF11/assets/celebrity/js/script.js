(function( $ ) {
  $.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-z]+$/i.test(value);
  }, "Enter only alphanumeric values"); 

})(jQuery);

$(document).ready(function() {


  $('.alphabetsOnly').keydown(function (e) {
      // if (e.shiftKey || e.ctrlKey || e.altKey) {
        if ( e.altKey) {
          e.preventDefault();
      } else {
          var key = e.keyCode;
          if (!((key == 8) || (key == 9) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
              e.preventDefault();
          }
      }
  });
  


   jQuery.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[0-9a-zA-Z\s]+$/i.test(value);
  }, ""); 

   jQuery.validator.addMethod("minteamsize", function(value, element) {
     return this.optional(element) || value.length >= minTeamsForEvent;
   },"");

   jQuery.validator.addMethod("minteamMembers", function(value, element) {
     return this.optional(element) || value.split(',').length >= min_team_size;
   },"");
   jQuery.validator.addMethod("accept", function(value, element, param) {
     //return this.optional(element) || value.match(new RegExp("." + param + "$"));
     if( param ) { 
        var regex = /^[a-zA-Z]+$/;
        return this.optional(element) || ( regex.test( value ) );
      }
   });
   jQuery.validator.addMethod("isEmail", function(value, element, param) {
     if( param ) { 
       var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
       return ( regex.test( value ) );
     }
   });
   
        if( $('#add_slot_frm').length > 0 ) { 
          if( $( '.game:checked' ).length ) {
            $('#add_slot_frm input').not('input:checkbox,input:radio').css('pointer-events','unset');
          } else { 
            $('#add_slot_frm input').not('input:checkbox,input:radio').css('pointer-events','none');
          }
        }
        $('.game').on('change', function(){
            $('input').not('input:checkbox,input:radio').css('pointer-events','');
            game_value = $(this).val() ;
            $("input:checkbox").not('input[data-radio="'+game_value+'"]:checkbox').prop('checked', false );
            // alert(game_value);
            if(game_value !='' && game_value > 0)
            {
            //   $('.list-group-item').css('pointer-events','none');
              // $(this).closest('.columns').find('.list-group-item').css('pointer-events','');

            }
            if($("#starttime").val() != ''){ 
              var game_time = $('input[name=game]:checked').attr('game_time');
              result=addMinutesToTime($("#starttime").val(), game_time );
              // alert(result);
              $('#endtime').val( result );
            }else{
              $('#endtime').val( '' );
            }

        });

       

         $( '.platform_type' ).on('click',function(){
          
          if( $( 'input[name="platform_type[' + $( 'input[ name="game" ]:checked' ).val( ) + '][]"]:checked' ).length >= 1  )
          {
            $('span.error').remove();
          }
         })

        $('.form-check-input').on('change',function(){
            var game_value = $(this).attr('data-radio');
            $("input:checkbox").not('input[data-radio="'+game_value+'"]:checkbox').prop('checked', false );
            if($('input[data-radio="'+game_value+'"]:checkbox:checked').length > 0){
                $('input').not('input:checkbox,input:radio').css('pointer-events','');
                $('input:radio').prop('checked', false );
                $('input:radio[value="'+game_value+'"]').prop('checked', true );

            }
            if($("#starttime").val() != ''){ 
              var game_time = $('input[name=game]:checked').attr('game_time');
              result=addMinutesToTime($("#starttime").val(), game_time );
              // alert(result);
              $('#endtime').val( result );
            }else{
              $('#endtime').val( '' );
            }
        });


    $.fn.exists = function() { return this.length > 0; };

    if (typeof minTeamsForEvent === 'undefined' || minTeamsForEvent == null ){
      minTeamsForEvent = 2;
    }

    if (typeof min_team_size === 'undefined' || min_team_size == null ){
      min_team_size = 3;
    }

    $("#login_frm").validate({
      rules: {
         email: {
            required: true,
            // email: true,  //add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
            isEmail: true,
         },
         password: {required:true, minlength: 6}
        },
        messages: {
	    	email:{
	    		required: 'Please enter email',
	    		email: 'Please enter a valid email',
          maxlength: 'Email can not be greater than {0} characters',
          isEmail: 'Please enter a valid email'
	    	},
	    	password: {
	    		required: 'Please enter password',
	    		minlength: "Please enter at least {0} characters"	
	    	}
	    },
	    submitHandler: function(form) {
   		  form.submit();
	    }
   
   });

    $(".forget-form").validate({
      rules: {
        forgot_email: {
            required: true,
            email: true,  //add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
         }
        },
        messages: {
	    	forgot_email:{
	    		required: 'Please enter email',
	    		email: 'Please enter a valid email',
	    		maxlength: 'Email can not be greater than {0} characters'
	    	}
	    },
	   submitHandler: function(form) {
   		form.submit();
	   }
   
   });
    
    $("#add_eventmanager_frm, #edit_eventmanager_frm").validate({
      rules: {
         name: {
              required: {
                depends:function(){
                    $(this).val($.trim($(this).val()));
                    return true;
                }
              }, 
              // lettersonly: true,
              //accept: "[a-zA-Z]+",
              accept: true,
              maxlength: 50
         },
         //surname:{accept: "[a-zA-Z]+"},
         surname:{accept: true},
         email: {
            required: true,
            // email: true,  //add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
            isEmail: true
         },
         password: {
           required: function(element) {
              if($("#eventmanager_id").exists() && $("#eventmanager_id").val().length > 0) {
                  return false;
              } else {
                  return true;
              }
          }, 
          minlength: 6
        },
        username: {
          required: true, 
          // lettersonly: true,
          maxlength: 50
        },
        termsnconditions: {
          required: true,
        },
        gamer_id: {
          required: true,
        },
        skype: {
          require_from_group: [1, ".id_group"]
        },
        mobile: {
          require_from_group: [1, ".id_group"],
          number: true,
        }
    },
    messages: {
        name: {
          required: "Please enter name", 
          lettersonly: "Enter only alphanumeric values", 
          maxlength: "Full name can not be greater than {0} characters",
          accept: "Please enter name in alphabets only"
        },
        surname: {
          accept: "Please enter surname in alphabets only"
        },
        email:{
          required: 'Please enter email',
          isEmail: 'Please enter a valid email',
          // email: 'Please enter a valid email',
          maxlength: 'Email can not be greater than {0} characters'
        },
        password: {
          required: 'Please enter password',
          minlength: "Please enter at least {0} characters" 
        },
        username: {
          required: "Please enter username",
        },
        termsnconditions: {
          required: "Please accept terms and conditions",
        },
        gamer_id: {
          required: "Please enter gamer id",
        },
        skype: {
          require_from_group: "Please fill either skype username or mobile number.",
        },
        mobile: {
          require_from_group: "Please fill either skype username or mobile number.",
        }
    },
    errorPlacement:function( error, dom ){ 
      if( dom.closest( 'label' ).length > 0 ) dom.closest( 'label' ).after( error );
      else dom.closest( '.form-group' ).append( error );
    },
    submitHandler: function(form) {
      var signup_email  = $( 'input[ name="email" ]' ).val( ) ;
      $('span.error').remove();
      if(IsEmail(signup_email)==false){
        $( 'input[ name="email" ]' ).after('<span class="error" style="color:red;"> Please enter a valid email </span>');
        return false;
      }
      form.submit();
    }
   
  });
  

  /*$( 'input[ name="email" ]' ).on("change keyup",function() {
    var signup_email  = $( 'input[ name="email" ]' ).val( ) ;
    if( signup_email != '' ) return;
    $('span.error').remove();
    if(IsEmail(signup_email)==false){
      $( 'input[ name="email" ]' ).after('<span class="error" style="color:red;"> Please enter a valid email </span>');
      // return false;
    }
  }); */

  function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) {
      return false;
    }else{
      return true;
    }
  }

   $("#add_slot_frm, #edit_slot_frm").validate({
      rules: {
        game: { required: true, maxlength: 15 },
        slot_price: { required: true,number:true,maxlength: 10 },
        slot_date: { required: true },
        starttime: { required: true },
        // endtime: { required: true }
        },
        messages: {
          game: {required: "Please select the game"},
          slot_price:{required: "Please enter amount", number: "Please enter the valid amount."},
          slot_date: {required: "Please select date"},
          starttime: {required: "Please select time"},
          // endtime: {required: "Please select end time"}
      },
      errorPlacement:function( error, dom ){ 
        if(dom.attr("name") == "game") {
          $( 'input[name="platform_type[' + $( 'input[ name="game" ]' ).val( ) + '][]"]:last' ).closest('.list-group').append( error );
        } 

        else if( dom.closest( 'label' ).length > 0 ) dom.closest( 'label' ).after( error );
        else dom.closest( '.form-group' ).append( error );
      },
	    submitHandler: function(form) {
        // alert ($( 'input[ name="game" ]:checked' ).val( )) ;
        if( $( 'input[name="platform_type[' + $( 'input[ name="game" ]:checked' ).val( ) + '][]"]:checked' ).length <= 0 ) { 

          $('span.error').remove();

          $( 'input[name="platform_type[' + $( 'input[ name="game" ]' ).val( ) + '][]"]:last' ).closest('.list-group').after('<span class="error">Please select at least one platform type.</span>');
          return false;
        }
        // return false;
   		  form.submit();
	    }
   
   }); 

   $("#profile_update_frm").validate({
      rules: {
         fullname: {
              required: {
                depends:function(){
                    $(this).val($.trim($(this).val()));
                    return true;
                }
              }, 
              lettersonly: true,
              maxlength: 50
         },
         email: {
            required: true,
            email: true,  //add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
         }
        },
        messages: {
          fullname: {required: "Please enter full name", lettersonly: "Enter only alphanumeric values", maxlength: "Full name can not be greater than {0} characters"},
          email:{
          required: 'Please enter email',
          email: 'Please enter a valid email',
          maxlength: 'Email can not be greater than {0} characters'
        }
      },
      submitHandler: function(form) {
        form.submit();
      }
   
   });

   $("#update_password_frm").validate({
      rules: {
         old_password: {required:true, minlength: 6},
         password: {required:true, minlength: 6},
         cpassword: {required:true, minlength: 6, equalTo: "#password"}
        },
        messages: {
          old_password: {
            required: 'Please enter current password',
            minlength: "Please enter at least {0} characters"
          },
          password: {
            required: 'Please enter new password',
            minlength: "Please enter at least {0} characters" 
          },
          cpassword: {
            required: 'Please enter confirm password',
            minlength: "Please enter at least {0} characters",
            equalTo: "Passwords mismatch"

          }
      },
      submitHandler: function(form) {
        form.submit();
      }
   
   });


    var last_valid_selection = null;
    $('#selected,#items').change(function(event) {

      if ($(this).val().length > 5) {
        $(this).val(last_valid_selection);
      } else {
        last_valid_selection = $(this).val();
      }
    });

    // var dateToday = new Date();
    //var dateToday = new Date();
    var dateToday = new Date( serverDateTime );
    dateToday.setDate(dateToday.getDate() - 1);

    if( $('#eventStartDate').length > 0 ){ 
      $('#eventStartDate').datepicker({
          format: "yyyy-mm-dd",
          autoclose: true,
          todayHighlight: true,
          startDate: '-0m',
          minDate: dateToday
      });

    }
    
    if( $('#eventStartTime,#eventEndTime').length > 0 ) $('#eventStartTime,#eventEndTime').timepicker(); 

    dateToday.setDate(dateToday.getDate() + 1);

    diff_minutes = function (dt2, dt1) {
        var diff =(dt2.getTime() - dt1.getTime()) / 1000;
        diff /= 60;
        return Math.abs(Math.round(diff));
    }

    changeSlotTime = function( slot_date, todayDate ){
      slotDateObj = ( slot_date )?new Date( slot_date ):false;
      var bool = (slotDateObj && todayDate.toDateString() === slotDateObj.toDateString());
      var curr_hour = todayDate.getHours();
      var curr_min = todayDate.getMinutes();
      var curr_time = '00:00';
      if(bool)
      {
        var minutes = diff_minutes( clientDateTime, new Date( ) );
        curr_min = curr_min + minutes;
        if( curr_min % 5 > 0 )
        {
          curr_min =  +curr_min + +( 5 - (curr_min%5) )  ;
        }
        var curr_sec = todayDate.getSeconds();
          var curr_time = curr_hour + ":" + curr_min + ":" +curr_sec ;
      }
      return curr_time;
    };

    $('#slot_date').datepicker({
        dateFormat: "d M, yy",
        autoclose: true,
        todayHighlight: true,
        startDate: '-0m',
        /*minDate: new Date(),*/
        minDate: dateToday,
        onSelect: function( dateStr, obj ) {
          $('label#slot_date-error').hide(); 
          $('#starttime').val('');
          $('#starttime').timepicker( 'remove' );
          $('#starttime').timepicker({
            timeFormat: 'H:i',
            minTime: changeSlotTime( dateStr, dateToday )
          }); 
        }
    });
    
    var curr_time = changeSlotTime( $('#slot_date').val(), dateToday );
    
    $('#starttime').timepicker({
      timeFormat: 'H:i',
      minTime: curr_time,
      change: function(time) {
        $('.validation-errors.s-side').remove();
        
    }
    }); 

    $('#starttime').on('change', function(){
      if($(this).val() != ''){ 
        var game_time = $('input[name=game]:checked').attr('game_time');
        result=addMinutesToTime($(this).val(), game_time );
        // alert(result);
        $('#endtime').val( result );
      }else{
        $('#endtime').val( '' );
      }
    });

    function addMinutesToTime(time, minsAdd) {
      function z(n){ return (n<10? '0':'') + n;};
      var bits = time.split(':');
      var mins = bits[0]*60 + +bits[1] + +minsAdd;
      return z(mins%(24*60)/60 | 0) + ':' + z(mins%60);  
    } 
  // result=addMinutesToTime('5:31',40);
  // alert(result);

	$( '.columns-list-group .game, .columns-list-group .game_platform' ).on( 'change', function(){
		$( '.game' ).parents( '.columns-list-group' ).removeClass( 'active' );
		$( '.game:checked' ).parents( '.columns-list-group' ).addClass( 'active' );
  });
  
	$( '.columns-list-group .game' ).on( 'change', function(){
    var $html = '<div id="flds-wrap">' + $( '#flds-wrap' ).clone().html() + '</div>';
			$( '#flds-wrap' ).remove();
			$( this ).parents( 'form' ).append( $html );
  });
  
  $( '.columns-list-group .game_platform' ).on( 'change', function(){
    $("#add_slot_frm, #edit_slot_frm").validate().resetForm();
		var found =  $( 'input[name="' + $( this ).attr( 'name' ) + '"]:checked' ).length;
		if( found > 0 ) { 
			if( $( this ).parents( '.slot-detail-form' ).find( '#flds-wrap' ).length <= 0 ) { 
				var $clone = $( '#flds-wrap' ).clone();
				$clone.find( '.hasDatepicker' ).removeClass( 'hasDatepicker' );
				
				var $html = '<div class="form-details detail-form" id="flds-wrap">' + $clone.html() + '</div>';
				$( '#flds-wrap' ).remove();
				$( this ).parents( '.slot-detail-form' ).append( $html );
				$( this ).parents( '.slot-detail-form' ).find( 'input' ).not('input:checkbox').val( '' );
						
				var $currentStartTimeObj = $( this ).parents( '.slot-detail-form' );
				
				$( this ).parents( '.slot-detail-form' ).find('#slot_date').datepicker({
					dateFormat: "d M, yy",
					autoclose: true,
					todayHighlight: true,
					startDate: '-0m',
					minDate: dateToday,
					onSelect: function( dateStr, obj ) { 
						$('label#slot_date-error').hide(); 
						$currentStartTimeObj.find('#starttime').val('');
						$currentStartTimeObj.find('#starttime').timepicker( 'remove' );
						$currentStartTimeObj.find('#starttime').timepicker({
							timeFormat: 'H:i',
							minTime: changeSlotTime( dateStr, dateToday )
						}); 
					}
				});
				
				var curr_time = changeSlotTime( $currentStartTimeObj.find('#starttime').val(), dateToday );
	
				$currentStartTimeObj.find('#starttime').timepicker({
				  timeFormat: 'H:i',
				  minTime: curr_time
				});
		
				$( this ).parents( '.slot-detail-form' ).find( '.ui-timepicker-input' ).keypress(function (event) {
					return isNumber(event, this)
				});

				$( this ).parents( '.card-body' ).find( '.slot_price' ).focus();
				
			}
		} else { 
			var $html = '<div id="flds-wrap">' + $( '#flds-wrap' ).clone().html() + '</div>';
			$( '#flds-wrap' ).remove();
			$( this ).parents( 'form' ).append( $html );
		}
  });
  
  $('.slot_price' ).keypress(function (event) {
    // return isNumber(event, this)
});

});


function getTeamMembers(team_id){

    var getTeamMembersUrl = $("#getTeamMembersUrl").attr('url');var token = $('input[name="_token"]').val();
    $.ajax({
      type:'POST',
      url: getTeamMembersUrl,
      data: {"_token": token, "team_id":team_id},
      success:function(data){            
        $("#teamMembers").html(data);  
        $("#teamMembersModal").modal('show');
      }

    });
}

function activeInactiveState(affected_id, status){
    
    let affected_data_model = $("#data_model").val(); 
    let setActiveInactiveUrl = $("#setActiveInactiveUrl").attr('url');let token = $('input[name="_token"]').val();
    
    swal({
    title: "Are you sure you want to perform this action?",
    text: "",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Yes, update it!"
    }, function (isConfirm) {
      if (isConfirm) {
        $.ajax({
          type:'POST',
          url: setActiveInactiveUrl,
          data: {"_token": token, "affected_id":affected_id,"status": status, "affected_data_model":affected_data_model, "activeinactive":1},
          success:function(data){   
            let obj = JSON.parse(data);
            if(obj.code==200){        
              location.reload();
              swal("Done!", obj.message, "success");
            }else{
              swal("Error Occured!",obj.message, "error");
            }
          }

        });
      }

    });

// onslot select 


}

deleteImage = function ( url, $currentObj ) {
  swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
    function(){ 
      swal.close();
      $.ajax({
          url     : url,
          type    : 'POST',
          data    :{"_token": $('input[name="_token"]').val() },
          dataType: 'json',
          error:  function ( xhr, status, error ) { 
              swal( 'Error!', 'Unable to delete, please try again later.', 'error' );
          },
          success : function ( json ) { 
              if( json.status == 0 ){ 
                swal( 'Error!', json.msg, 'error' );
              } else { 
                swal( 'Deleted!', json.msg, 'success' );
                $( $currentObj ).closest( '.img-wrap' ).remove();
              }
              setTimeout(() => {
                $( 'body' ).removeClass( 'stop-scrolling' );
              }, 1000);
          }
      });
    });
    return false;
};

