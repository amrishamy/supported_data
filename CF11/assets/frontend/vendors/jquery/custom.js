(function( $ ){
	getCal = function( dateStr ) { 
		if( !dateStr ) return false;
		$.ajax({
			type:'POST',
			url: calUrl,
			data: {"_token": token, 'date': dateStr},
			success:function(data){   
				$( '#add-cal').html( data.html );
			}
		});
		return false;
	}

	// function to fetch the celebrity and games according to calendar date on Home page

	getCelebritiesAndGames = function( isDateSelected, dateStr,type ) { 
		if( typeof(isDateSelected)== typeof undefined  ) var isDateSelected = false; 
		/*if( !dateStr ) return false;*/
		if( typeof(dateStr)== typeof undefined  ) var dateStr = '';

		if( $( '.load_celeb_and_games' ).hasClass( 'running' ) ) return false;
		$( '.load_celeb_and_games' ).addClass( 'running' );

		var postedData = { "_token": token, 'date': dateStr, 'c_id': false, 'g_id': false, 'time': false };

		if( !isDateSelected ){ 
			var $wrap = $( '.load_celeb_and_games .nav-item.active' ).data( 'wrap' );
			if( $wrap ) { 
				if( $( $wrap ).find( '#g_id' ).length > 0 ) { 
					postedData[ 'g_id' ] = $( $wrap ).find( '#g_id' ).val();
				}
				if( $( $wrap ).find( '#c_id' ).length > 0 ) { 
					postedData[ 'c_id' ] = $( $wrap ).find( '#c_id' ).val();
				}
				if( $( $wrap ).find( '.slot_time' ).length > 0 ) { 
					postedData[ 'time' ] = $( $wrap ).find( '.slot_time' ).val();
				}
			}
		}
		var ajaxUrl;
		if(type == 'c'){
			ajaxUrl = calByCelebritysUrl;
		}else if(type == 'g'){
			ajaxUrl = calByGamesUrl;
		}else{
			ajaxUrl = calCelebGamesUrl;
		}
		console.log(ajaxUrl);
		$.ajax({
			type:'POST',
			url: ajaxUrl,
			data: postedData,
			success:function(data){ 
				if(type == 'c'){  
					$( '.load_celeb_and_games #nav-home #cel_wrap').html( data.html );
					$( '.load_celeb_and_games' ).removeClass( 'running' );
				}else if(type == 'g'){
					$( '.load_celeb_and_games #nav-profile #game_wrap').html( data.html );
					$( '.load_celeb_and_games' ).removeClass( 'running' );
				}else{
					$( '.load_celeb_and_games').html( data.html );
					$( '.load_celeb_and_games' ).removeClass( 'running' );
				}

				$( '.slot_time' ).timepicker({ timeFormat: 'H:i' });
				$('.ui-timepicker-input').keypress(function (event) {
					return isNumber(event, this)
				});
			
				$('.ui-timepicker-input').click(function (event) {
					$('.ui-timepicker-wrapper').css('width', $('.ui-timepicker-input').outerWidth() );
				});
			}
		});
		return false;
	};

	// For detail page
	getCelebritiesCalendarDetail = function( isDateSelected, dateStr,type=null ) { 
		if( typeof(isDateSelected)== typeof undefined  ) var isDateSelected = false; 
		if(type != 'detail'){
			if( !dateStr ) return false;
		}	
		
		var postedData = { "_token": token, 'date': dateStr, 'c_id': false, 'g_id': false, 'time': false, 'detail' : true };

		if( !isDateSelected ){ 
			var $wrap = $( '.filters .filter_row' );
			if( $wrap ) { 
				if( $( $wrap ).find( '#g_id' ).length > 0 ) { 
					postedData[ 'g_id' ] = $( $wrap ).find( '#g_id' ).val();
				}
				if( $( $wrap ).find( '#c_id' ).length > 0 ) { 
					postedData[ 'c_id' ] = $( $wrap ).find( '#c_id' ).val();
				}
				if( $( $wrap ).find( '.slot_time' ).length > 0 ) { 
					postedData[ 'time' ] = $( $wrap ).find( '.slot_time' ).val();
				}
				if( $( $wrap ).find( '.slot_date' ).length > 0 ) { 
					postedData[ 'date' ] = $( $wrap ).find( '.slot_date' ).val();
				}
			}
		}
			var ajaxUrl;
		
			ajaxUrl = celebrityCalendarDetailUrl;
		
		$.ajax({
			type:'POST',
			url: ajaxUrl,
			data: postedData,
			success:function(data){ 
				
					$( '.celeb_cal_detail').html( data.html );
					
			}
		});
		return false;
	};

	$( document ).on('click', '.cal-date', function(  ){
		$( '.cal-date' ).parent( ).removeClass( 'active' );
		$( this ).addClass( 'active' ).parent( ).addClass( 'active' );
		return false;
	});

	// load the celebrity and games on Home page 
	//getCelebritiesAndGames( '2020-01-30' );


})( jQuery );
$(document).ready(function () {
		
	$( '.slot_time' ).timepicker({ timeFormat: 'H:i' });

	// Game Section Owl Carousel
	$('.game-block').owlCarousel({
	  loop: true,
	  margin: 30,
	autoplay:true,
    autoplayTimeout:3000,
	  dots: false,
	  nav:true,
	  navText: [
		"<i class='fa fa-angle-left'></i>",
		"<i class='fa fa-angle-right'></i>"
	  ],
	  autoplayHoverPause: true,
	  responsive: {
		0: {
		  items: 1,
		  dots: true,
		   nav:false
		},
		600: {
		  items: 1,
		  dots: true,
		   nav:false
		},
		768: {
		  items: 2,
		   dots: true,
		   nav:false
		},
		1000: {
		  items: 3,
		   dots: false,
		   nav:true
		}
	  }
	});
	
	// I am online Owl Carousel
	$('.online-celebrity').owlCarousel({
	  loop: true,
	  margin: 20,
	autoplay:true,
    autoplayTimeout:3000,
	  dots: false,
	  nav:true,
	  navText: [
		"<i class='fa fa-angle-left'></i>",
		"<i class='fa fa-angle-right'></i>"
	  ],
	  autoplayHoverPause: true,
	  responsive: {
		  0: {
		  items: 1,
		  dots: true,
		   nav:false
		},
		600: {
		  items: 1,
		  dots: true,
		   nav:false
		},
		768: {
		  items: 2,
		   dots: true,
		   nav:false
		},
		991: {
		  items: 3,
		   dots: false,
		   nav:true
		},
		1279: {
		  items: 4,
		   dots: false,
		   nav:true
		}
	  }
	});
	
	// live games Owl Carousel
	$('.live-games').owlCarousel({
	  loop: true,
	  margin: 15,
	  dots: false,
	autoplay:true,
    autoplayTimeout:3000,
	  nav:true,
	  navText: [
		"<i class='fa fa-angle-left'></i>",
		"<i class='fa fa-angle-right'></i>"
	  ],
	  autoplayHoverPause: true,
	  responsive: {
		  0: {
		  items: 1,
		  dots: true,
		   nav:false
		},
		600: {
		  items: 1,
		  dots: true,
		   nav:false
		},
		768: {
		  items: 2,
		   dots: true,
		   nav:false
		},
		991: {
		  items: 3,
		   dots: false,
		   nav:true
		},
		1279: {
		  items: 4,
		   dots: false,
		   nav:true
		}
	  }
	});
	  // Back to top button
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('.back-to-top').fadeIn('slow');
    } else {
      $('.back-to-top').fadeOut('slow');
    }
  });
  $('.back-to-top').click(function(){
    $('html, body').animate({scrollTop : 0},1500, 'easeInOutExpo');
    return true;
  });
  
  // header fix

	$(window).scroll(function() {
	  var header = $(document).scrollTop();
	  var headerHeight = $(".header").outerHeight();
	  var firstSection = $(".main-container section:nth-of-type(1)").outerHeight();
	  if (header > headerHeight) {
	    $(".header").addClass("fixed");
	  } else {
	    $(".header").removeClass("fixed");
	  }	 
	
	});

	var date = new Date().toISOString().slice(0,10);
	$('input[type=date]').attr('min', date);

	$('.load_celeb_and_games .nav-item').click(function(){
		$( '.load_celeb_and_games' ).find(".nav-item").removeClass("active");
		$(this).addClass("active");
		var $obj = $( this ).attr( 'href' );
		if( $obj && $( $obj ).length > 0 ) {
			$( '#nav-tabContent .tab-pane' ).removeClass( 'show active' );
			$( $obj ).addClass( 'show active' )
		}
	});
/***********/

	/** active tab frontend menu **/
	$(document).ready(function() {

		var hash = window.location.hash;
		if( hash ) { 
			hash = hash.substring(1);
			if( hash ) { 
				var hashNavLink = $( '.navbar-nav' ).find('.'+hash);
				//if this link from home page only
				if (hashNavLink.length > 0) {
					$( '.navbar-nav' ).find('.nav-item').removeClass('active');
					hashNavLink.addClass('active');
				}
			}
		}
	});

	$('.modelChange').click(function(){
		var modalval = $(this).attr('data-target');
		var hideModal = $(this).attr('hide-modal');
		// alert(hideModal);
		$(hideModal).modal('hide');
		$(modalval).modal('show');
	})

	$('.ui-timepicker-input').keypress(function (event) {
        return isNumber(event, this)
    });

    $('.ui-timepicker-input').click(function (event) {
        $('.ui-timepicker-wrapper').css('width', $('.ui-timepicker-input').outerWidth() );
    });

    // $('.slot_date, .ui-corner-all').click(function (event) {
    //     $('#ui-datepicker-div').css('width', $('.slot_date').outerWidth() );
    // });

	// THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
function isNumber(evt, element) {
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (
	(charCode != 58 || $(element).val().indexOf(':') != -1) && // “.” CHECK DOT, AND ONLY ONE.
	(charCode < 48 || charCode > 57))
	return false;
	
	return true;
	}

    var dateToday = new Date();
	$('.slot_date').datepicker({
        dateFormat: "d M, yy",
        autoclose: true,
        todayHighlight: true,
        startDate: '-0m',
        /*minDate: new Date(),*/
        minDate: dateToday,
        
    });

});	



