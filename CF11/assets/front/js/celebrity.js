(function ( $ ) {
  celebrityListObj = $('#celebrity-list').DataTable({
      'processing': true,
      "searching": false, 
      "bLengthChange": false,
      'serverSide': true,
      'iDisplayLength': 10,
      "order": [ 1, "asc" ],
      'columnDefs': [ { orderable: false, targets: [0,1,2,3,4,5,6]}],
      'oLanguage': {'sProcessing': '<div class="datatable_loading" style="display:block;">Loading&#8230;</div>'},
      'ajax': {
        'url': listUrl,
        'data': function(data){
            var c_id = $('#c_id').val();
            data.c_id = c_id;
            var g_id = $('#g_id').val();
            data.g_id = g_id;
            var pltfrm_id = $('#pltfrm_id').val();
            data.pltfrm_id = pltfrm_id;
            var slot_date = $('.slot_date').val();
            data.slot_date = slot_date;
            var slot_time = $('.slot_time').val();
            data.slot_time = slot_time;
        }
      }
  });

  $(document).ready(function () {
    $( '.game-listing form' ).find( 'select, input' ).on( 'change', function(){
      celebrityListObj.draw();
    });
  });
})( jQuery );
