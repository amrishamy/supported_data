var loadFunFacturl = gameVars.loadFunFacturl;
var gameMinuts = gameVars.gameMinuts;
var gameSeconds = gameVars.gameSeconds;
var appurl = gameVars.appurl;
var csrf_token = gameVars.csrf_token;
var encId = gameVars.encId;
var event_id = gameVars.event_id;
var team_id = gameVars.team_id;

// alert(event_id+"  :  "+team_id);
var formurl = appurl + '/gamescreensave';
$(document).ready(function () {
  //load questions on screen.
  loadFunFact();
  //front check method
  formChechFunFact();
  //form submission and fetch next Fun Fact
  $("#submitForm").click(function (event) {
    event.preventDefault();

    var valueof = this.value;
    if (valueof == 1) {
      var selections = $('input[name="selected_option_userids[]"]:checked').length;
      if (selections >= 1) {
        $.ajax({
          type: "POST",
          url: formurl,
          data: $("#gameform").serialize(),
          success: function (result) {
            if ($('#lastq').val() == 1) {
              $('#submitForm').html('Finish');
              $('#submitForm').val(3);
            } else {
              $('#submitForm').html('Next');
              $('#submitForm').val(2);
              $('#submitForm').hide();
              $('.user_options').prop("disabled", true);
            }
          }
        });
      } else {
        alert('Choose atleast one option.');
      }
    } else {
      if (valueof == 2) {
        loadFunFact();
      } else {
        swal('Done!', 'Game Finished', 'success');
        // window.location.href = appurl+'/mmrulesscreen/'+encId;
        updateIBgameStatus(event_id, team_id);
      }
    }

  });
});

//Timer for game starts here..
$(function () {
  $('#countdowntimer').countdowntimer({
    minutes: gameMinuts,
    seconds: gameSeconds,
    displayFormat: "MS",
    timeUp: whenTimesUp,
    size: "lg"
  });
  function whenTimesUp() {
    swal('Done!', 'Game Time Over', 'warning');
    updateIBgameStatus(event_id, team_id);

  }
});
//Timer for game ends here.

//Load question/Funfact and start question timer
function loadFunFact() {
  $.ajax({
    url: loadFunFacturl,
    success: function (result) {
      let obj = JSON.parse(result);
      if (obj.gameminutes != 'x' || obj.gameseconds != 'x') {
        $("#getData").html(obj.html);
        $('#submitForm').html('Submit');
        $('#submitForm').val(1);
        $('#submitForm').show();
        questionTimer(obj.gameminutes, obj.gameseconds);
      } else {
        swal('Done!', 'Game Finished', 'success');
        updateIBgameStatus(event_id, team_id);
      }
    }
  });
}

//Load answer screen as per current funfact Id and start answer screen timer here..
function getAnswerScreen() {

  var funfactId = $('input[name="funfactId"]').val();
  $.ajax({
    type: "POST",
    url: appurl + "/getanswerdata",
    data: { "_token": csrf_token, 'funfactId': funfactId },
    success: function (result) {
      let obj = JSON.parse(result);
      if (result) {
        $("#getData").html(obj.html);
        answerTimer(obj.ansminuts, obj.anssecounds);
        if ($('#lastq').val() == 1) {
          $('#submitForm').html('Finish');
          $('#submitForm').val(3);
        } else {
          $('#submitForm').val(2);
          $('#submitForm').hide();
        }
      } else {
        swal('Done!', 'Game Finished', 'success');
        updateIBgameStatus(event_id, team_id);
      }
    }
  });
}

var tdestroy = function (id) {
  jQuery("#" + id).countdowntimer("pause", "pause");
}

function questionTimer(quesminuts, queseconds) {
  if (queseconds != 'x') {
    $(function () {
      $('.timer2').html('Question Time');
      $('#countdowntimerques').countdowntimer({
        minutes: parseInt(quesminuts),
        seconds: parseInt(queseconds),
        displayFormat: "MS",
        timeUp: whenTimesUpQus
      });

      function whenTimesUpQus() {
        getAnswerScreen();
      }
    });
  } else {
    swal('Done!', 'Game Finished', 'success');
    updateIBgameStatus(event_id, team_id);
  }
}

function answerTimer(ansminuts, anssecounds) {
  tdestroy('countdowntimerques');
  if (anssecounds != 'x') {
    $(function () {
      $('.timer2').html('Answer Time');
      $('#countdowntimerques').countdowntimer({
        minutes: parseInt(ansminuts),
        seconds: parseInt(anssecounds),
        displayFormat: "MS",
        timeUp: whenTimesUpAns
      });

      function whenTimesUpAns() {
        tdestroy('countdowntimerques');
        loadFunFact();
      }
    });
  } else {
    swal('Done!', 'Game Finished', 'success');
    updateIBgameStatus(event_id, team_id);
  }
}

function geteventgametime(event_id) {
  $.ajax({
    type: 'POST',
    url: appurl + "/geteventgametime",
    data: { "_token": csrf_token, 'event_id': event_id },
    success: function (result) {
      // alert('set apeared');
    }
  });
}

function formChechFunFact() {
  $(document).on('click', '.user_options', function (evt) {
    var choosedoptions = $('input[name="selected_option_userids[]"]:checked').length;
    if (choosedoptions > 2) {
      alert('You have already selected two options.');
      $(this).prop('checked', false);
    }
  });
}

function updateIBgameStatus(event_id, team_id) {
  var status = 2;
  $.ajax({
    type: "POST",
    url: appurl + "/update_ib_status",
    data: { "_token": csrf_token, 'event_id': event_id, 'team_id': team_id, 'status': status },
    success: function (result) {
      if (result) {
        window.location.href = appurl + '/mmrulesscreen/' + encId; // redirect to market madness rules screen when game finished
      }
    }
  });
}