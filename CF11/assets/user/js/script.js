$(document).ready(function() {

  $( document ). ajaxStart( function() {
      $('#preloader').show();
   });

   $( document ). ajaxComplete( function() {
    $('#preloader').hide();
   });

  $('.alphabetsOnly').keydown(function (e) {
    // if (e.shiftKey || e.ctrlKey || e.altKey) {
      if ( e.altKey) {
        e.preventDefault();
    } else {
        var key = e.keyCode;
        if (!((key == 8) || (key == 9) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
            e.preventDefault();
        }
    }
});

    $.fn.exists = function() { return this.length > 0; };

    if (typeof minTeamsForEvent === 'undefined' || minTeamsForEvent == null ){
      minTeamsForEvent = 2;
    }

    if (typeof min_team_size === 'undefined' || min_team_size == null ){
      min_team_size = 3;
    }

    $("#login_frm").validate({
      rules: {
         email: {
            required: true,
            email: true,  //add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
         },
         password: {required:true, minlength: 6}
        },
        messages: {
	    	email:{
	    		required: 'Please enter email',
	    		email: 'Please enter a valid email',
	    		maxlength: 'Email can not be greater than {0} characters'
	    	},
	    	password: {
	    		required: 'Please enter password',
	    		minlength: "Please enter at least {0} characters"	
	    	}
	    },
	    submitHandler: function(form) {
        var $formData = new FormData( form );

        $( form ).find( 'label.error, .notification-box' ).remove();
  
        $.ajax({
          type:'POST',
          url: $( form ).attr( 'action' ),
          data: $formData,
          contentType: "application/json",
          dataType: "json",
          processData: false,
          contentType: false,
          error:function( xhr, status){
            var extramsg = '';
            if( xhr.responseJSON.errors ) { 
              $.each(xhr.responseJSON.errors, function( fld, msg ){
                var $fld = $( form ).find( 'input[name="' + fld + '"]' );
                if( $fld.length > 0 ) { 
                  if( $fld.attr( 'type' ) == 'radio' ) { 
                    $fld = $( form ).find( '#Gender-1' ).closest( '.radio-inline' );
                  }
                  if( !$fld.next().hasClass( 'error' ) ) { 
                    $fld.after( '<label id="email-error" class="error" for="email">' + msg + '</label>' );
                  } else {
                    $fld.next().show().text( msg );
                  }
                } else { 
                  extramsg += '<span>' + msg + '</span>';
                }
              });
  
              if( extramsg ) { 
                $( form ).prepend( '<div class="login-box notification-box" ><div class="alert alert-danger alert-block" ><button type="button" class="close" data-dismiss="alert">×</button>' + extramsg + '</div></div>' )
              }
            }else { 
              $( form ).prepend( '<div class="login-box notification-box" ><div class="alert alert-danger alert-block"><button type="button" class="close" data-dismiss="alert">×</button>Something went wrong, please try again later.</div></div>' );
            }
          },
          success:function(data){
            
            $("#loginForm").find( 'form #slot_id' ).remove();

            if( data.success ) { 
              $( form ).find( 'input[ type="submit" ]' ).prop( 'disabled', true );
              $( form ).prepend( '<div class="login-box notification-box" ><div class="alert alert-success alert-block"><button type="button" class="close" data-dismiss="alert">×</button><span>' + data.success + '</span></div></div>' );
              
              $('#loginForm').modal('hide');
              if( data.redirectTo ) { 
                swal({
                  title: "" ,
                  text: data.success ,
                  type: "success",
                  showCancelButton: false,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Okay"
                  }, function (isConfirm) {
                    if (isConfirm) {
                      window.location.href = data.redirectTo;
                    }
                  });

                  setTimeout(function(){
                    window.location.href = data.redirectTo;
                  }, 3000);
              }

              if( data.links ) { 
                $( '#auth-links' ).html( data.links );
                $( '.book-btn a[slot_id="' + data.slot_id + '"]' ).trigger( 'click' );
              }

            } else if( data.error ) { 
              $( form ).prepend( '<div class="login-box notification-box" ><div class="alert alert-danger alert-block" ><button type="button" class="close" data-dismiss="alert">×</button><span>' + data.error + '</span></div></div>' );
            }
          }
        });
  
        //form.submit();
        return false;
	    }
   
   });

    $(".forget-form").validate({
      rules: {
        forgot_email: {
            required: true,
            email: true,  //add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
         }
        },
        messages: {
	    	forgot_email:{
	    		required: 'Please enter email',
	    		email: 'Please enter a valid email',
	    		maxlength: 'Email can not be greater than {0} characters'
	    	}
	    },
      submitHandler: function(form) {
        var $formData = new FormData( form );

        $( form ).find( 'label.error, .notification-box' ).remove();
  
        $.ajax({
          type:'POST',
          url: $( form ).attr( 'action' ),
          data: $formData,
          contentType: "application/json",
          dataType: "json",
          processData: false,
          contentType: false,
          error:function( xhr, status){
            var extramsg = '';
            if( xhr.responseJSON.errors ) { 
              $.each(xhr.responseJSON.errors, function( fld, msg ){
                var $fld = $( form ).find( 'input[name="' + fld + '"]' );
                if( $fld.length > 0 ) { 
                  if( $fld.attr( 'type' ) == 'radio' ) { 
                    $fld = $( form ).find( '#Gender-1' ).closest( '.radio-inline' );
                  }
                  if( !$fld.next().hasClass( 'error' ) ) { 
                    $fld.after( '<label id="email-error" class="error" for="email">' + msg + '</label>' );
                  } else {
                    $fld.next().show().text( msg );
                  }
                } else { 
                  extramsg += '<span>' + msg + '</span>';
                }
              });
  
              if( extramsg ) { 
                $( form ).prepend( '<div class="login-box notification-box" ><div class="alert alert-danger alert-block" ><button type="button" class="close" data-dismiss="alert">×</button>' + extramsg + '</div></div>' )
              }
            }else { 
              $( form ).prepend( '<div class="login-box notification-box" ><div class="alert alert-danger alert-block" ><button type="button" class="close" data-dismiss="alert">×</button>Something went wrong, please try again later.</div></div>' );
            }
          },
          success:function(data){
            if( data.success ) { 
              $( form ).find( 'input[ type="submit" ]' ).prop( 'disabled', true );
              $( form ).prepend( '<div class="login-box notification-box" ><div class="alert alert-success alert-block" ><button type="button" class="close" data-dismiss="alert">×</button><span>' + data.success + '</span></div></div>' );
              
              $('#forgotpassword').modal('hide');
              swal({
                title: "" ,
                text: data.success ,
                type: "success",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Okay"
                }, function (isConfirm) {
                  if (isConfirm) {
                    // window.location.href = data.redirectTo;
                  }
            
                });
                
            } else if( data.error ) { 
              $( form ).prepend( '<div class="login-box notification-box" ><div class="alert alert-danger alert-block" ><button type="button" class="close" data-dismiss="alert">×</button><span>' + data.error + '</span></div></div>' );
            }
          }
        });
  
        //form.submit();
        return false;
	    }
   
   });

   jQuery.validator.addMethod("lettersonly", function(value, element) {
     return this.optional(element) || /^[0-9a-zA-Z\s]+$/i.test(value);
   }, ""); 

    jQuery.validator.addMethod("minteamsize", function(value, element) {
      return this.optional(element) || value.length >= minTeamsForEvent;
    },"");

    jQuery.validator.addMethod("minteamMembers", function(value, element) {
      return this.optional(element) || value.split(',').length >= min_team_size;
    },"");

    jQuery.validator.addMethod("accept", function(value, element, param) {
      //return this.optional(element) || value.match(new RegExp("." + param + "$"));
      if( param ) { 
         var regex = /^[a-zA-Z]+$/;
         return this.optional(element) || ( regex.test( value ) );
       }
    });

    jQuery.validator.addMethod("isEmail", function(value, element, param) {
      if( param ) { 
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return ( regex.test( value ) );
      }
    });

    
    $("#add_user_frm").validate({
      rules: {
        first_name: {
              required: {
                depends:function(){
                    $(this).val($.trim($(this).val()));
                    return true;
                }
              }, 
              // lettersonly: true,
              //accept: "[a-zA-Z]+",
              accept: true,
              maxlength: 15
         },
         //surname:{accept: "[a-zA-Z]+"},
         last_name:{accept: true, maxlength: 15 },
         email: {
            required: true,
            // email: true,  //add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
            isEmail: true
         },
         gender:{
           required:true
         },
         password: {
            required: true , 
            minlength: 6
        },
        cpassword: {
          required:true, 
          minlength: 6, 
          equalTo: "#password"
        },
        mobile_number: {
          required: true, 
          // lettersonly: true,
          maxlength: 15
        },
        gamer_id: {
          required: true,
        }
        
    },
    messages: {
        first_name: {
          required: "Please enter first name", 
          lettersonly: "Enter only alphanumeric values", 
          maxlength: "First name can not be greater than {0} characters",
          accept: "Please enter first name in alphabets only"
        },
        last_name: {
          maxlength: "Last name can not be greater than {0} characters",
          accept: "Please enter last name in alphabets only"
        },
        email:{
          required: 'Please enter email',
          isEmail: 'Please enter a valid email',
          // email: 'Please enter a valid email',
          maxlength: 'Email can not be greater than {0} characters'
        },
        gender:{
          required: 'Please choose gender',
        },
        password: {
          required: 'Please enter password',
          minlength: "Please enter at least {0} characters" 
        },
        cpassword: {
          required: 'Please enter confirm password',
          minlength: "Please enter at least {0} characters",
          equalTo: "Password mismatch"

        },
        gamer_id: {
          required: "Please enter gamer id",
        },
        mobile_number: {
          required: "Please enter mobile number.",
        },
    },
    errorPlacement: function(error, element) {
      $( element ).closest( '.form-group' ).append( error );
   },
    submitHandler: function(form) {
      /*var signup_email  = $( 'input[ name="email" ]' ).val( );
      $('span.error').remove();
      if(IsEmail(signup_email)==false){
        $( 'input[ name="email" ]' ).after('<span class="error" style="color:red;"> Please enter a valid email </span>');
        return false;
      }*/

      var $formData = new FormData( form );

        $( form ).find( 'label.error, .notification-box' ).remove();
  
        $.ajax({
          type:'POST',
          url: $( form ).attr( 'action' ),
          data: $formData,
          contentType: "application/json",
          dataType: "json",
          processData: false,
          contentType: false,
          error:function( xhr, status){
            var extramsg = '';
            if( xhr.responseJSON.errors ) { 
              $.each(xhr.responseJSON.errors, function( fld, msg ){
                var $fld = $( form ).find( 'input[name="' + fld + '"]' );
                if( $fld.length > 0 ) { 
                  if( $fld.attr( 'type' ) == 'radio' ) { 
                    $fld = $( form ).find( '#Gender-1' ).closest( '.radio-inline' );
                  }
                  if( !$fld.next().hasClass( 'error' ) ) { 
                    $fld.after( '<label id="email-error" class="error" for="email">' + msg + '</label>' );
                  } else {
                    $fld.next().show().text( msg );
                  }
                } else { 
                  extramsg += '<span>' + msg + '</span>';
                }
              });
  
              if( extramsg ) { 
                $( form ).prepend( '<div class="login-box notification-box" ><div class="alert alert-danger alert-block" ><button type="button" class="close" data-dismiss="alert">×</button>' + extramsg + '</div></div>' );
              }
            } else { 
              $( form ).prepend( '<div class="login-box notification-box" ><div class="alert alert-danger alert-block" ><button type="button" class="close" data-dismiss="alert">×</button>Something went wrong, please try again later.</div></div>' );
            }
          },
          success:function(data){
            if( data.success ) { 
              $( form ).find( 'input[ type="submit" ]' ).prop( 'disabled', true );
              $( form ).prepend( '<div class="login-box notification-box"><div class="alert alert-success alert-block" ><button type="button" class="close" data-dismiss="alert">×</button><span>' + data.success + '</span></div></div>' );
              
              $('#signupForm').modal('hide');
              swal({
                title: "" ,
                text: data.success ,
                type: "success",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Okay"
                }, function (isConfirm) {
                  if (isConfirm) {
                    window.location.href = data.redirectTo;
                  }
            
                });

                  setTimeout(function(){
                  window.location.href = data.redirectTo;
                  }, 3000);

            } else if( data.error ) { 
              $( form ).prepend( '<div class="login-box notification-box"><div class="alert alert-danger alert-block" ><button type="button" class="close" data-dismiss="alert">×</button><span>' + data.error + '</span></div></div>' );
            }
          }
        });
  
        //form.submit();
        return false;
    }
   
  }); 

  $('#signupForm, #loginForm, #forgotpassword').on('hidden.bs.modal', function () {
      $( this ).find( 'form' )[0].reset();
      $( this ).find( 'label.error, label.errors, label.success' ).remove();
      $(".alert").alert('close');
  });



   $("#profile_update_frm").validate({
    rules: {
      first_name: {
        required: {
          depends:function(){
              $(this).val($.trim($(this).val()));
              return true;
          }
        },
        accept: true,
        maxlength: 15
      },
      last_name:{accept: true, maxlength: 15 },
      email: {
          required: true,
          maxlength: 255,
          isEmail: true
      },
      gender:{
        required:true
      },
      mobile_number: {
        required: true,
        maxlength: 15
      },
      gamer_id: {
        required: true,
      }
  
    },
  messages: {
    first_name: {
    required: "Please enter first name", 
    lettersonly: "Enter only alphanumeric values", 
    maxlength: "First name can not be greater than {0} characters",
    accept: "Please enter first name in alphabets only"
    },
    last_name: {
      maxlength: "Last name can not be greater than {0} characters",
      accept: "Please enter last name in alphabets only"
    },
    email:{
      required: 'Please enter email',
      isEmail: 'Please enter a valid email',
      // email: 'Please enter a valid email',
      maxlength: 'Email can not be greater than {0} characters'
    },
    gender:{
      required: 'Please choose gender',
    },
    gamer_id: {
      required: "Please enter gamer id",
    },
    mobile_number: {
      required: "Please enter mobile number.",
    },
  },
  errorPlacement: function(error, element) {
    $( element ).closest( '.form-group' ).append( error );
 },
    submitHandler: function(form) {
      form.submit();
    }
 
 });

   $("#update_password_frm").validate({
      rules: {
         old_password: {required:true, minlength: 6},
         password: {required:true, minlength: 6},
         cpassword: {required:true, minlength: 6, equalTo: "#password"}
        },
        messages: {
          old_password: {
            required: 'Please enter current password',
            minlength: "Please enter at least {0} characters"
          },
          password: {
            required: 'Please enter new password',
            minlength: "Please enter at least {0} characters" 
          },
          cpassword: {
            required: 'Please enter confirm password',
            minlength: "Please enter at least {0} characters",
            equalTo: "Password mismatch"

          }
      },
      submitHandler: function(form) {
        form.submit();
      }
   
   });


    var last_valid_selection = null;
    $('#selected,#items').change(function(event) {

      if ($(this).val().length > 5) {
        $(this).val(last_valid_selection);
      } else {
        last_valid_selection = $(this).val();
      }
    });

    // var dateToday = new Date();
    var dateToday = new Date();
    dateToday.setDate(dateToday.getDate() - 1);

    $('#eventStartDate').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true,
        startDate: '-0m',
        minDate: dateToday
    });

    $('#eventStartTime,#eventEndTime').timepicker(); 

    var $aSlotObj = false;
    var validateAndSubmitHandlerSRF = function( $srfObj ) {  
        if( !$srfObj ) return false;

        // send request on submit the request form
        $srfObj.validate({
          rules: {
            requested_slot_platform: {required: true},
            gamer_id: {required:true, maxlength: 25 }
          },
          messages: {
            requested_slot_platform:{required: 'Please select the platform'},
            gamer_id: {required: 'Please enter gamer id',maxlength: 'Gamer id can not be greater than {0} characters'}
          },
          submitHandler: function(form) {
            var $formData = new FormData( form );

            $( form ).find( 'label.error, .notification-box' ).remove();
      
            $.ajax({
              type:'POST',
              url: $( form ).attr( 'action' ),
              data: $formData,
              contentType: "application/json",
              dataType: "json",
              processData: false,
              contentType: false,
              error:function( xhr, status){
                var extramsg = '';
                if( xhr.responseJSON.errors ) { 
                  $.each(xhr.responseJSON.errors, function( fld, msg ){
                    var $fld = $( form ).find( 'input[name="' + fld + '"]' );
                    if( $fld.length > 0 ) { 
                      if( $fld.attr( 'type' ) == 'radio' ) { 
                        $fld = $( form ).find( '#Gender-1' ).closest( '.radio-inline' );
                      }
                      if( !$fld.next().hasClass( 'error' ) ) { 
                        $fld.after( '<label id="email-error" class="error" for="email">' + msg + '</label>' );
                      } else {
                        $fld.next().show().text( msg );
                      }
                    } else { 
                      extramsg += '<span>' + msg + '</span>';
                    }
                  });
      
                  if( extramsg ) { 
                    $( form ).prepend( '<div class="login-box notification-box"><div class="alert alert-danger alert-block" ><button type="button" class="close" data-dismiss="alert">×</button>' + extramsg + '</div></div>' )
                  }
                }else { 
                  $( form ).prepend( '<div class="login-box notification-box" ><div class="alert alert-danger alert-block" ><button type="button" class="close" data-dismiss="alert">×</button>Something went wrong, please try again later.</div></div>' );
                }
              },
              success:function(data){
                if( data.success ) { 
                  $( form ).find( 'input[ type="submit" ]' ).prop( 'disabled', true );
                  $( form ).prepend( '<div class="login-box notification-box"><div class="alert alert-success alert-block" ><button type="button" class="close" data-dismiss="alert">×</button><span>' + data.success + '</span></div></div>' );
                  
                  $('#requestnow').modal('hide');
                  
                  if( $aSlotObj ) { 
                    $aSlotObj.after( '<a href="javascript:void(0);" class="' + $aSlotObj.attr( 'class' ) + '">Requested</a>' );
                    $aSlotObj.next( ).removeClass( 'request_now_btn' );
                    $aSlotObj.remove();
                    $aSlotObj = false;
                  }

                  swal({
                    title: "" ,
                    text: data.success ,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Okay"
                    }, function (isConfirm) {
                      
                  });

                } else if( data.error ) { 
                  $( form ).prepend( '<div class="login-box notification-box"><div class="alert alert-danger alert-block" style="qmargin: 2px 2px;"><button type="button" class="close" data-dismiss="alert">×</button><span>' + data.error + '</span></div></div>' );
                }
              }
            });
      
            //form.submit();
            return false;
          }
      });
    };
    
    $(document).on('click', '.request_now_btn', function(e){
      $aSlotObj = $( this );
      e.preventDefault();
      var slot_id = $aSlotObj.attr( 'slot_id' ) ;
      var token = $('input[name="_token"]').val();
      $.ajax({
        type:'POST',
        url: $aSlotObj.attr( 'href' ),
        data: { "_token": token, "slot_id": slot_id },
        dataType:"json",
        success:function(response){
          if(response.status == 1 ) 
          {
            // $("#requestnow").modal('show');
            $("#requestnow").find('.modal-body').html( response.html );
            $("#requestnow").modal('show');
            validateAndSubmitHandlerSRF( $("#requestnow").find( '#send_request_frm' ) );
          }
          else if(response.status == 0 )
          {
            if( $("#loginForm").find( 'form #slot_id' ).length <= 0 )  
              $("#loginForm").find( 'form' ).append( '<input type="hidden" name="slot_id" id="slot_id" value="' + slot_id + '" />' );
            else $("#loginForm").find( 'form #slot_id' ).val( slot_id );
            $("#loginForm").modal('show');
          }
        }
  
      });
      
    });

});



function getTeamMembers(team_id){

    var getTeamMembersUrl = $("#getTeamMembersUrl").attr('url');var token = $('input[name="_token"]').val();
    $.ajax({
      type:'POST',
      url: getTeamMembersUrl,
      data: {"_token": token, "team_id":team_id},
      success:function(data){            
        $("#teamMembers").html(data);  
        $("#teamMembersModal").modal('show');
      }

    });
}

function activeInactiveState(affected_id, status){
    
    let affected_data_model = $("#data_model").val(); 
    let setActiveInactiveUrl = $("#setActiveInactiveUrl").attr('url');let token = $('input[name="_token"]').val();
    
    swal({
    title: "Are you sure you want to perform this action?",
    text: "",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Yes, update it!"
    }, function (isConfirm) {
      if (isConfirm) {
        $.ajax({
          type:'POST',
          url: setActiveInactiveUrl,
          data: {"_token": token, "affected_id":affected_id,"status": status, "affected_data_model":affected_data_model, "activeinactive":1},
          success:function(data){   
            let obj = JSON.parse(data);
            if(obj.code==200){        
              userTable.ajax.reload(); teamTable.ajax.reload(); eventTable.ajax.reload();
              swal("Done!", obj.message, "success");
            }else{
              swal("Error Occured!",obj.message, "error");
            }
          }

        });
      }

    });


}

$(".mobile_number").keyup(function(){
  // $(this).val($(this).val().replace(/^(\d{1})(\d{3})(\d{3})(\d)+$/, "$1-($2)-$3-$4"));
});

function isNumberOrDash(evt) {
  evt = (evt) ? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  //alert(charCode);

  if (charCode != 46 && charCode != 45 && charCode > 31
  && (charCode < 48 || charCode > 57)) {
    return false;
  }
  return true;
}


    