(function( $ ){ 

    load_booking_request = function ( form ) { 
        if( !form ) return false;

        $form = $( form );
        if( !bookingFor ) var bookingFor = 1;

        var $formData = new FormData( $form[0] );
        $.ajax({
            type:'POST',
            url: $( form ).attr( 'action' ),
            data: $formData,
            contentType: "application/json",
            dataType: "json",
            processData: false,
            contentType: false,
            success: function( data ) { 
                $("#teamMembers").html(data);
            }
        });

        return false;

    };

    $(document).ready(function() {
        //load_booking_request( '#filter_form' );
        $( '#filter_form select, #filter_form .slot_date, #filter_form .slot_time ' ).on('change', function(){
            //load_booking_request( '#filter_form' );
            requestedGamesTable.draw();
        });
    });
})( jQuery );


