<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    return "Cache is cleared";
});


Route::group(['prefix' => 'admin'/*,  'middleware' => 'auth:admin'*/], function(){

// Admin Authenticate & Forgot Password Routes
Route::get('/login', ['as' => 'admin.login', 'uses' => 'Admin\LoginController@loginForm']);
Route::post('/authenticate', ['as' => 'admin.authenticate', 'uses' => 'Admin\LoginController@authenticate']);
Route::post('/admin_forgot_password', ['as' => 'admin.forgot_password', 'uses' => 'Admin\LoginController@admin_forgot_password']);
Route::get('/resetPassword/{token}', ['as' => 'admin.resetpassword', 'uses' => 'Admin\LoginController@showResetPassword']);
Route::post('/updatePassword', ['as' => 'admin.update_password', 'uses' => 'Admin\LoginController@updatePassword']);


//Users CRUD Routes
Route::get('/', function () { return redirect()->action('Admin\LoginController@loginForm'); });

});
Route::group(['prefix' => 'admin',  'middleware' => 'auth:admin'], function(){
	Route::match(['get', 'post'],'/users', ['as' => 'admin.userlist', 'uses' => 'Admin\UsersController@userListing']);
	Route::get('/adduser', ['as' => 'admin.adduser', 'uses' => 'Admin\UsersController@addUser']);
	Route::post('/saveuser', ['as' => 'admin.saveuser', 'uses' => 'Admin\UsersController@saveUser']);
	Route::get('/edituser/{id}', ['as' => 'admin.edituser', 'uses' => 'Admin\UsersController@editUser']);
	Route::post('/updateuser', ['as' => 'admin.updateuser', 'uses' => 'Admin\UsersController@updateUser']);
	Route::get('/userajaxdata', ['as' => 'admin.userajaxdata', 'uses' => 'Admin\UsersController@ajaxDataLoad']);


	Route::match(['get', 'post'],'/celebrities', ['as' => 'admin.celebrityListing', 'uses' => 'Admin\CelebrityController@celebrityListing']);
	Route::get('/addcelebrity', ['as' => 'admin.addCelebrity', 'uses' => 'Admin\CelebrityController@addCelebrity']);
	Route::post('/savecelebrity', ['as' => 'admin.saveCelebrity', 'uses' => 'Admin\CelebrityController@saveCelebrity']);
	Route::get('/editcelebrity/{id}', ['as' => 'admin.editCelebrity', 'uses' => 'Admin\CelebrityController@editCelebrity']);
	Route::post('/updatecelebrity', ['as' => 'admin.updateCelebrity', 'uses' => 'Admin\CelebrityController@updateCelebrity']);
	Route::get('/celebrityajaxdata', ['as' => 'admin.celebrityajaxdata', 'uses' => 'Admin\CelebrityController@ajaxDataLoad']);



		// Games CRUD
	Route::get('/addgame', ['as' => 'admin.addgame', 'uses' => 'Admin\GamesController@add']);
	Route::get('/games', ['as' => 'admin.games', 'uses' => 'Admin\GamesController@index']);
	Route::post('/savegame', ['as' => 'admin.savegame', 'uses' => 'Admin\GamesController@store']);

	Route::get('/editgame/{id}', ['as' => 'admin.editgame', 'uses' => 'Admin\GamesController@editGame']);
	Route::post('/updategame', ['as' => 'admin.updategame', 'uses' => 'Admin\GamesController@updateGame']);

	Route::get('/gameajaxdata', ['as' => 'admin.gameajaxdata', 'uses' => 'Admin\GamesController@ajaxDataLoad']);

	Route::post( '/game/delete/image/{id}', [ 'as' => 'admin.delete_game_image', 'uses' => 'Admin\GamesController@deleteImage']);

	// Sliders CRUD
	Route::get('/addslide', ['as' => 'admin.addslide', 'uses' => 'Admin\SliderController@create']);
	Route::get('/sliders', ['as' => 'admin.sliders', 'uses' => 'Admin\SliderController@index']);
	Route::post('/saveslide', ['as' => 'admin.saveslide', 'uses' => 'Admin\SliderController@store']);

	Route::get('/editslide/{id}', ['as' => 'admin.editslide', 'uses' => 'Admin\SliderController@edit']);
	Route::post('/updateslide', ['as' => 'admin.updateslide', 'uses' => 'Admin\SliderController@update']);

	Route::get('/slidersajaxdata', ['as' => 'admin.slidersajaxdata', 'uses' => 'Admin\SliderController@ajaxDataLoad']);

	Route::post( '/slider/delete/image/{id}', [ 'as' => 'admin.delete_slider_image', 'uses' => 'Admin\SliderController@deleteImage']);

	Route::post('/slider/update/reorder', ['as' => 'admin.updateSlideImageReorder', 'uses' => 'Admin\SliderController@updateSlideImageReorder']);


	//Admin Profile Routes
	Route::get('/profile', ['as' => 'admin.profile', 'uses' => 'Admin\ProfileController@adminprofile']);
	Route::post('/profileupdate', ['as' => 'admin.profileupdate', 'uses' => 'Admin\ProfileController@profileupdate']);
	Route::post('/updatepassword', ['as' => 'admin.updatepassword', 'uses' => 'Admin\ProfileController@updatepassword']);


	//Single & Bulk Delete Routes
	Route::post('/deleterow', ['as' => 'admin.deleterow', 'uses' => 'Admin\CommonController@deleteRow']);
	Route::post('/updatebulkrows', ['as' => 'admin.updatebulkrows', 'uses' => 'Admin\CommonController@updateBulkRows']);


	//Email Template Routes
	Route::match(['get', 'post'],'/emails', ['as' => 'admin.emailslist', 'uses' => 'Admin\EmailsController@emailsListing']);
	Route::get('/addemail', ['as' => 'admin.addemail', 'uses' => 'Admin\EmailsController@addEmail']);
	Route::get('/emailajaxdata', ['as' => 'admin.emailajaxdata', 'uses' => 'Admin\EmailsController@ajaxDataLoad']);
	Route::post('/saveemail', ['as' => 'admin.saveemail', 'uses' => 'Admin\EmailsController@saveEmail']);
	Route::get('/editemail/{id}', ['as' => 'admin.editemail', 'uses' => 'Admin\EmailsController@editEmail']);
	Route::post('/updateemail', ['as' => 'admin.updateemail', 'uses' => 'Admin\EmailsController@updateEmail']);


	Route::get('/dashboard', ['as' => 'admin.dashboard', 'uses' => 'Admin\DashboardController@dashboard']);
	Route::get('/logout', ['as' => 'admin.logout', 'uses' => 'Admin\LoginController@logout']);
});

Route::group(['prefix' => 'celebrity'], function(){
	
	// Authenticate & Forgot Password Routes
	Route::get('/login', ['as' => 'celebrity.login', 'uses' => 'Celebrity\LoginController@loginForm']);
	Route::post('/authenticate', ['as' => 'celebrity.authenticate', 'uses' => 'Celebrity\LoginController@authenticate']);
	Route::get('/signup', ['as' => 'celebrity.signup', 'uses' => 'Celebrity\CelebrityController@signupForm']);
	Route::post('/register', ['as' => 'celebrity.register', 'uses' => 'Celebrity\CelebrityController@register']);
	Route::post('/forgotPassword', ['as' => 'celebrity.forgot_password', 'uses' => 'Celebrity\LoginController@forgotPassword']);
	Route::get('/resetPassword/{token}', ['as' => 'celebrity.resetpassword', 'uses' => 'Celebrity\LoginController@resetPassword']);
	Route::post('/updateForgotPassword', ['as' => 'celebrity.update_password', 'uses' => 'Celebrity\LoginController@updateForgotPassword']);

	Route::get('/', function () { return redirect()->action('Celebrity\LoginController@loginForm'); });


	Route::get('/accountVerify/{token}', ['as' => 'celebrity.verifyaccount', 'uses' => 'Celebrity\CelebrityController@verifyAccount']);
	
});
Route::group(['prefix' => 'celebrity',  'middleware' => 'guest:celebrity'], function(){
	//Profile Routes
	Route::get('/profile', ['as' => 'celebrity.profile', 'uses' => 'Celebrity\ProfileController@profile']);
	Route::post('/profileupdate', ['as' => 'celebrity.profileupdate', 'uses' => 'Celebrity\ProfileController@profileupdate']);
	Route::post('/updatepassword', ['as' => 'celebrity.updatepassword', 'uses' => 'Celebrity\ProfileController@updatepassword']);

	Route::post( '/celebrity/delete/image/{id}', [ 'as' => 'celebrity.delete_celebrity_image', 'uses' => 'Celebrity\ProfileController@deleteImage']);

	Route::get('/dashboard', ['as' => 'celebrity.dashboard', 'uses' => 'Celebrity\DashboardController@dashboard']);

	Route::get('/changeStatus', ['as' => 'celebrity.changeStatus', 'uses' => 'Celebrity\DashboardController@changeStatus']);


	Route::get('/logout', ['as' => 'celebrity.logout', 'uses' => 'Celebrity\LoginController@logout']);

	Route::get('/eventslistajax', ['as' => 'celebrity.eventslistajax', 'uses' => 'Celebrity\DashboardController@eventsListing']);




	// Events CRUD
	Route::match(['get', 'post'],'/events', ['as' => 'celebrity.eventlist', 'uses' => 'Celebrity\EventsController@eventListing']);
	
	Route::get('/eventajaxdata', ['as' => 'celebrity.eventajaxdata', 'uses' => 'Celebrity\EventsController@ajaxDataLoad']);

	Route::post('/setactiveinactive', ['as' => 'celebrity.setactiveinactive', 'uses' => 'Admin\CommonController@setActiveInactive']);

	Route::post('/emaillist', ['as' => 'celebrity.emaillist', 'uses' => 'Celebrity\UsersController@getEmailIdsAjax']);

	Route::post('/getemailIdata', ['as' => 'celebrity.getemailIdata', 'uses' => 'Celebrity\UsersController@getEmailIData']);

	//general
	Route::get('/loadstates/{countryId}/{stateId}', ['as' => 'celebrity.loadstates', 'uses' => 'Celebrity\CelebrityController@loadStates']);

	// Slots CRUD
	Route::get('/addslot', ['as' => 'celebrity.addslot', 'uses' => 'Celebrity\SlotController@create']);
	Route::get('/slots', ['as' => 'celebrity.slots', 'uses' => 'Celebrity\SlotController@index']);
	Route::post('/saveslot', ['as' => 'celebrity.saveslot', 'uses' => 'Celebrity\SlotController@store']);

	Route::get('/editslot/{id}', ['as' => 'celebrity.editslot', 'uses' => 'Celebrity\SlotController@edit']);
	Route::post('/updateslot', ['as' => 'celebrity.updateslot', 'uses' => 'Celebrity\SlotController@update']);

	Route::get('/slotjaxdata', ['as' => 'celebrity.slotajaxdata', 'uses' => 'Celebrity\SlotController@ajaxDataLoad']);

	Route::get('/slots/booking_requests/', ['as' => 'celebrity.booking_requests', 'uses' => 'Celebrity\SlotController@booking_requests']);


	//Single & Bulk Delete Routes
	Route::post('/deleterow', ['as' => 'celebrity.deleterow', 'uses' => 'Celebrity\CommonController@deleteRow']);
	Route::post('/updatebulkrows', ['as' => 'celebrity.updatebulkrows', 'uses' => 'Celebrity\CommonController@updateBulkRows']);

});



Route::group(['prefix' => 'user'], function(){
	
	// Authenticate & Forgot Password Routes
	Route::get('/login', ['as' => 'user.login', 'uses' => 'User\LoginController@loginForm']);
	Route::post('/authenticate', ['as' => 'user.authenticate', 'uses' => 'User\LoginController@authenticate']);
	Route::get('/signup', ['as' => 'user.signup', 'uses' => 'User\UserController@signupForm']);
	Route::post('/register', ['as' => 'user.register', 'uses' => 'User\UserController@register']);
	Route::post('/forgotPassword', ['as' => 'user.forgot_password', 'uses' => 'User\LoginController@forgotPassword']);
	Route::get('/resetPassword/{token}', ['as' => 'user.resetpassword', 'uses' => 'User\LoginController@resetPassword']);
	Route::post('/updateForgotPassword', ['as' => 'user.update_password', 'uses' => 'User\LoginController@updateForgotPassword']);

	Route::get('/', function () { return redirect()->action('User\LoginController@loginForm'); });


	Route::get('/accountVerify/{token}', ['as' => 'user.verifyaccount', 'uses' => 'User\UserController@verifyAccount']);

	// check user is logged in or not
	Route::post('/check_user_login', ['as' => 'user.check_user_login', 'uses' => 'User\LoginController@check_user_login']);
	
});
Route::group(['prefix' => 'user',  'middleware' => 'guest:web'], function(){
	//Profile Routes
	Route::get('/profile', ['as' => 'user.profile', 'uses' => 'User\ProfileController@profile']);
	Route::post('/profileupdate', ['as' => 'user.profileupdate', 'uses' => 'User\ProfileController@profileupdate']);
	Route::get('/changepassword', ['as' => 'user.changepassword', 'uses' => 'User\ProfileController@changepassword']);
	Route::post('/updatepassword', ['as' => 'user.updatepassword', 'uses' => 'User\ProfileController@updatepassword']);

	Route::get('/dashboard', ['as' => 'user.dashboard', 'uses' => 'User\DashboardController@dashboard']);

	Route::get('/logout', ['as' => 'user.logout', 'uses' => 'User\LoginController@logout']);

	Route::get('/eventslistajax', ['as' => 'user.eventslistajax', 'uses' => 'User\DashboardController@eventsListing']);

	// send request for slot booking
	Route::post('/send_booking_request', ['as' => 'user.send_booking_request', 'uses' => 'User\UserController@send_booking_request']);

	// Requested Games CRUD
	Route::get('/requested_games', ['as' => 'user.requested_games', 'uses' => 'User\RequestedGamesController@index']);
	Route::get('/bookings_list_ajax', ['as' => 'user.bookings_list_ajax', 'uses' => 'User\RequestedGamesController@bookingsListAjax']);

	// Upcoming Games CRUD
	Route::get('/upcoming_games', ['as' => 'user.upcoming_games', 'uses' => 'User\UpcomingGamesController@index']);

	// Games History CRUD
	Route::get('/games_history', ['as' => 'user.games_history', 'uses' => 'User\GamesHistoryController@index']);

	Route::match(['get', 'post'],'/events', ['as' => 'user.eventlist', 'uses' => 'User\EventsController@eventListing']);
	
	Route::get('/eventajaxdata', ['as' => 'user.eventajaxdata', 'uses' => 'User\EventsController@ajaxDataLoad']);

	Route::post('/setactiveinactive', ['as' => 'user.setactiveinactive', 'uses' => 'Admin\CommonController@setActiveInactive']);

	Route::post('/emaillist', ['as' => 'user.emaillist', 'uses' => 'Celebrity\UsersController@getEmailIdsAjax']);

	Route::post('/getemailIdata', ['as' => 'user.getemailIdata', 'uses' => 'User\UsersController@getEmailIData']);

	//general
	Route::get('/loadstates/{countryId}/{stateId}', ['as' => 'user.loadstates', 'uses' => 'User\UserController@loadStates']);

});

// Front-end Routes starts here

// Route::get('/landingpage', ['as' => 'landingpage', 'uses' => 'FrontendController@landingpage']);
Route::get('/', ['as' => 'homepage', 'uses' => 'HomeController@index']);
Route::post('/get-calendar', ['as' => 'get_calendar', 'uses' => 'HomeController@getCalendar']);
Route::post('/get-celebrities-games', ['as' => 'get_celebrities_games', 'uses' => 'HomeController@getCelebritiesAndGames']);

Route::post('/get-by-celebrities', ['as' => 'get_by_celebrities', 'uses' => 'HomeController@getByCelebrities']);

Route::post('/get-by-games', ['as' => 'get_by_games', 'uses' => 'HomeController@getByGames']);

Route::get('/get_celebrity_calendar_detail', ['as' => 'get_celebrity_calendar_detail', 'uses' => 'CelebritiesCalendarController@getCelebrityCalendarDetail']);

Route::get('/termnconditions', ['as' => 'termnconditions', 'uses' => 'FrontendController@termnconditions']);

Route::get('/games', ['as' => 'gamelist', 'uses' => 'GamesListController@index']);
Route::get('/celebrities', ['as' => 'celebritieslist', 'uses' => 'CelebritiessListController@index']);

Route::get('/celebritity-profile/{id}/', ['as' => 'celebritiyprofile', 'uses' => 'CelebritiessListController@profile']);
Route::get('/celebrities-calendar', ['as' => 'celebritiescalendar', 'uses' => 'CelebritiesCalendarController@index']);
Route::get('/live-videos', ['as' => 'livevideos', 'uses' => 'LiveVideosController@index']);


// Route::get('/', function () { return redirect('/landingpage'); });

/*Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');*/
