<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Slots extends Model
{
    protected $table = 'slots';

    public static function slotAlreadyExistSameTime($data=NULL,$startTime=NULL,$endTime=NULL,$edit=false){
        
        $celebrity_id = session('manager_id');
        $startTime = $data->starttime;
        $endTime = $data->endtime;
        $slotDate = date("yy-m-d", strtotime( $data->slot_date )) ;
       
        $obj = DB::table('slots as e')
                    ->select(DB::raw('e.id'))
                    ->where(['e.status' => 1]);
       if($edit) $obj->where('id','!=',$data->slot_id);

       $obj->whereRaw("((slot_start_time <= '$startTime' and slot_end_time >= '$startTime') or (slot_start_time <= '$endTime' and slot_end_time >= '$endTime' ) or (slot_start_time >= '$startTime' and slot_end_time <= '$endTime')) and e.celebrity_id = $celebrity_id and e.slot_date='$slotDate'");
       
        return $obj->get()->count();
        }

}
