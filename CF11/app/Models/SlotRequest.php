<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SlotRequest extends Model
{
    protected $table = 'game_slot_request';
}
