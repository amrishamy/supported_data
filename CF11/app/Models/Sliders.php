<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Sliders extends Model
{
    protected $table = 'sliders';

    public static function ResetOrder()
    {

        // "SET @pos := 0; update `sliders` set sort_order = ( SELECT @pos := @pos + 1 ) order by sort_order ASC"

        DB::select( 'call reset_slider_order();' );
    }
}
