<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Celebrities extends Authenticatable
{

    protected $guard = 'celebrities';


    protected $table = 'celebrities';

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status', 'verify_token', 'country_id', 'state_id', 'surname', 'username', 'gamer_id', 'mobile', 'skype', 'twitter', 'termsnconditions','image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

   
}
