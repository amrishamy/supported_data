<?php

namespace App\Http\Controllers\Celebrity;

use Auth;
use App\Http\Controllers\Controller;
use App\Models\Celebrities;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Middleware\RedirectIfAuthenticated;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Input;
use Config;
use xmlapi;
use Hash;
use Carbon\Carbon;
use App\Http\Controllers\Celebrity\CommonController;
use Illuminate\Support\Facades\URL;
use Vinkla\Hashids\Facades\Hashids;

use App\Models\Countries;
use App\Models\States;
use App\Models\Admin;
use App\Models\Games;
use App\Models\Slots;

class SlotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Game List";
        return view('celebrity.slots.slots', ['title' => $title, "breadcrumbItem" => "Game Management" , "breadcrumbTitle"=> "Game List"]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        // dd(date_default_timezone_get());
        $platforms = Config::get('constants.platforms');
        // var_dump($playstations );die;
        $title = "Add Game";
        return view('celebrity.slots.add_slot', ['title' => $title, "breadcrumbItem" => "Game Management" , "breadcrumbTitle"=> "Add Game",'games'=>$this->get_games(),'platforms'=>$platforms ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $this->validate($request, [
            'game' => 'required',
            'slot_price' => 'required',
            'slot_date' => 'required',
            'starttime' => 'required',
            // 'endtime' => 'required',
        ]);
        $platform_type = "";
        $postedData = $request->all();

        if( !( 
                isset( $postedData[ 'platform_type' ] ) && 
                isset( $postedData[ 'platform_type' ][ $postedData[ 'game' ] ] ) && 
                $postedData[ 'platform_type' ][ $postedData[ 'game' ] ] 
            ) ) { 
                return redirect()->route('celebrity.addslot')
                ->withInput($request->input())
                ->with(['error'=>'Please select at least one platform type.']);
                
        }
        else
        {
            $platform_type = $postedData[ 'platform_type' ][ $postedData[ 'game' ] ]  ;

        }

        $game = Games::find($request->game) ;
        $gameTime = '+'.$game->game_time.' minutes' ;
        // dd( $gameTime );
        $endTime = date("H:i", strtotime($gameTime, strtotime($request->starttime) ));
        // dd($endTime);
        
        $slot_occupied_count =  Slots::slotAlreadyExistSameTime($request,'','',false);
        // dd($slot_occupied_count);
       
        if($slot_occupied_count > 0)
        {
            return redirect()->route('celebrity.addslot')
                ->withInput($request->input())
                ->with(['error'=>'This game slot is already booked.']);
            // dd('Data Exist'); 
        }

        $enteredDateStartTime = strtotime( date("yy-m-d", strtotime( $request->slot_date )) . ' ' . $request->starttime . ':00' );
        // dd( date("yy-m-d", strtotime( $request->slot_date )) );
        if( time( ) >= $enteredDateStartTime ) { 
            return redirect()->route('celebrity.addslot')
                ->withInput($request->input())
                ->with(['slot_time_error'=>'Please select future game time']);
        }

        
        $slot = new Slots();
    
        $slot->game_id = $request->game;
        $slot->celebrity_id = $request->celebrity;
        $slot->platform_type = is_array($platform_type) ? implode(",",$platform_type) : '' ;
        
        $slot->slot_price  = $request->slot_price;
        $slot->slot_date  = date("yy-m-d", strtotime( $request->slot_date )) ;
        $slot->slot_start_time  = $request->starttime;
        
        $slot->slot_end_time  = $endTime;
        $slot->save();
    
        if($slot){
            return redirect()->route('celebrity.slots')->with(['success'=>'Game has been added successfully.']);
        }else{
            return redirect()->route('celebrity.slots')->with(['error'=>'Error occured while saving game details.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $platforms = Config::get('constants.platforms');
        $title = "Edit Game";

        $decryptSlotId = Hashids::decode($id);
        $slot_detail = Slots::find($decryptSlotId[0]);
        $game = Games::find($slot_detail->game_id);
        // echo "<pre>";
        // print_r($game); die;
        return view('celebrity.slots.edit_slot', ['title' => $title, "slot_detail"=> $slot_detail, "breadcrumbItem" => "Game Management" , "breadcrumbTitle"=> "Edit Game",'game'=>$game,'platforms'=>$platforms ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validatedData = $this->validate($request, [
            'game' => 'required',
            'slot_price' => 'required',
            'slot_date' => 'required',
            'starttime' => 'required',
            // 'endtime' => 'required',
        ]);

        $encryptId = Hashids::encode($request->slot_id);

        $platform_type = "";
        $postedData = $request->all();

        if( !( 
                isset( $postedData[ 'platform_type' ] ) && 
                isset( $postedData[ 'platform_type' ][ $postedData[ 'game' ] ] ) && 
                $postedData[ 'platform_type' ][ $postedData[ 'game' ] ] 
            ) ) { 
                return redirect()->back()
                ->withInput($request->input())
                ->with(['error'=>'Please select at least one platform type.']);
                
        }
        else
        {
            $platform_type = $postedData[ 'platform_type' ][ $postedData[ 'game' ] ]  ;

        }

        $game = Games::find($request->game) ;
        $gameTime = '+'.$game->game_time.' minutes' ;
        // dd( $gameTime );
        $endTime = date("H:i", strtotime($gameTime, strtotime($request->starttime) ));

        $slot_occupied_count =  Slots::slotAlreadyExistSameTime($request,'','',true);
        // dd($slot_occupied_count);
       
        if($slot_occupied_count > 0)
        {
            return redirect()->route('celebrity.editslot',$encryptId)
                ->withInput($request->input())
                ->with(['error'=>'This slot is already booked. Please select another slot time.']);
            // dd('Data Exist'); 
        }

        $enteredDateStartTime = strtotime( date("yy-m-d", strtotime( $request->slot_date )) . ' ' . $request->starttime . ':00' );
        // dd( date("yy-m-d", strtotime( $request->slot_date )) );
        if( time( ) >= $enteredDateStartTime ) { 
            return redirect()->route('celebrity.editslot',$encryptId)
                ->withInput($request->input())
                ->with(['slot_time_error'=>'Please select future game time']);
        }

        $slot = Slots::find($request->slot_id);
        
    
        $slot->game_id = $request->game;
        $slot->celebrity_id = $request->celebrity;
        $slot->platform_type = is_array($platform_type) ? implode(",",$platform_type) : '' ;
        
        $slot->slot_price  = $request->slot_price;
        $slot->slot_date  = date("yy-m-d", strtotime( $request->slot_date )) ;
        $slot->slot_start_time  = $request->starttime;
        $slot->slot_end_time  = $endTime;
        $slot->save();
    
        if($slot){
            return redirect()->route('celebrity.slots')->with(['success'=>'Game has been Updated successfully.']);
        }else{
            return redirect()->route('celebrity.slots')->with(['error'=>'Error occured while updating game details.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_games()
    {
        return Games::where('status', 1)->get();
    }

    public function ajaxDataLoad(Request $request){
        $draw = $_GET['draw'];
        $row = $_GET['start'];
        $rowperpage = $_GET['length']; // Rows display per page
        $columnIndex = $_GET['order'][0]['column']; // Column index
        $columnName = $_GET['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_GET['order'][0]['dir']; // asc or desc
        $searchValue = $_GET['search']['value']; // Search value
        
        $columns = [
            0 => '',
            1 => 'name',
            2 => 'slot_price',
            3 => 'slot_date',
            4 => 'slot_start_time',
            5 => 'slot_end_time',
            6 => 'status',
            7 => 'created_at',
            8 => 'action'

        ];
        $columnName = $columns[$columnIndex];

        ## Fetch records
        // $lists = Slots::whereRaw($searchQuery)->orderBy($columnName, $columnSortOrder)->skip($row)->take($rowperpage)->get();
        $slotTblName        = with( new Slots )->getTable( ); 
        $gameTblName        = with( new Games )->getTable( ); 
        ## Search 
        $searchQuery = " ";
        if($searchValue != ''){
           $searchQuery = " ( `$gameTblName`.`name` like '%".$searchValue."%') ";
        }else{ 
          $searchQuery = 1;
        }

        $listObj = Slots::select( [ $slotTblName.".*", $gameTblName.".name",$gameTblName.".game_time" ] ) 
        ->Join( $gameTblName, function( $query ) use( $slotTblName, $gameTblName ){
            $query->on( $slotTblName.".game_id", "=", $gameTblName.".id" );
            // $query->on( $slotTblName.".status", "=", \DB::raw( 1 ) );
        })
        ->where($slotTblName.'.celebrity_id','=',session('manager_id'))
        ->whereRaw($searchQuery);
        
        $totalRecordwithFilter = $listObj->count();

        $lists = $listObj->orderBy($columnName, $columnSortOrder)->skip($row)->take($rowperpage)->get();
        
        ## Total number of records
        $totalRecords = Slots::where($slotTblName.'.celebrity_id','=',session('manager_id'))->count();
        
        ## Total number of record with filtering
        //$totalRecordwithFilter = Slots::whereRaw($searchQuery)->count();
        

        $checkbox = ""; $data = array(); $action = "";
        if(!empty($lists)){
            foreach($lists as $list){

                $encryptId = Hashids::encode($list["id"]);  
                
                $checkbox = '<div class="animated-checkbox"><label style="margin-bottom:0px;"><input type="checkbox" name="ids[]" value="'.Hashids::encode($list['id']).'" /><span class="label-text"></span></label></div>';
                $action = '<a href="' . route('celebrity.booking_requests', [ 'slotId'=> Hashids::encode($list["id"])  ] ) . '" class="add" title="Show Booking Requests"><i class="fa fa-list"></i></a> &nbsp;&nbsp; 
                <a href="editslot/'.$encryptId.'" title="Edit"><i class="fa fa-pencil" aria-hidden="true"></i></a> &nbsp;&nbsp; 
                <a href="javascript:void(0);" onclick=delete_row("'.$encryptId.'")  title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a></i>';
                $data[] = array( 
                // ++$row,
                $checkbox,
                $list['name'],
                $list['slot_price'],
                date("d M, Y", strtotime($list['slot_date'])),
                $list['slot_start_time'],
                $list['slot_end_time'],
                $list['status'] =="1" ? "<span class='badge' style='background:green; color:#FFF; padding:5px;'>Active</span>" : "<span class='badge' style='background:#FF0000; color:#FFF; padding:5px;'>Inactive</span>",
                date("d M, Y", strtotime($list['created_at'])),
                $action
               );
            }
        }
        ## Response
        $response = array(
          "draw" => intval($draw),
          "iTotalRecords" => $totalRecords,
          "iTotalDisplayRecords" => $totalRecordwithFilter,
          "aaData" => $data
        );
        echo json_encode($response);
        exit;
      }


      public function booking_requests()
    {
        $title = "Booking Requests";
        return view('celebrity.booking_requests.booking_requests', ['title' => $title, "breadcrumbItem" => $title , "breadcrumbTitle"=> "Booking Requests List"]);
    }
}
