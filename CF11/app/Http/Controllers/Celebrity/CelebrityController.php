<?php

namespace App\Http\Controllers\Celebrity;

use Auth;
use App\Models\Celebrities;
use App\Models\States;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use Config;
use Mail;
use Hash;
use App\Http\Traits\CommonMethods;
use App\Models\Emails;
use File;
use Response;

class CelebrityController extends Controller
{
    use CommonMethods;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/login';
    public $folder_path = "uploads/celebrities";
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:celebrity')->except(['signupForm','register','verifyAccount','loadStates']);
    }



    /**
    Event manager signup form view
    **/
    public function signupForm(){
        $user_id = session('manager_id');$user_type = session('user_type');
        if(!empty($user_id) && $user_type=='celebrity'){
           return redirect()->route('celebrity.dashboard'); 
        }

        $title = 'Celebrity Signup';
        return view('celebrity.signup', [
            'title' => $title,
        ]);
    }


    /**
    Registeration process of celebrity
    **/
    public function register(Request $request){

      // echo $request->termsnconditions; die;

        $validatedData = $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:celebrities,email',
            'password' => 'required|min:6',
            'username' => 'required|unique:celebrities,username',
            'mobile' => 'required_without:skype',
            'skype' => 'required_without:mobile|unique:celebrities,skype',
            'twitter' => 'nullable|unique:celebrities,twitter',
            'gamer_id' => 'required|unique:celebrities,gamer_id',
            'termsnconditions' => 'required'
        ]);
        $image = "";
        $verifyToken = str_random(20);
        $file_name_with_time_prefix = upload_file($request, 'image' , $this->folder_path );

        if($file_name_with_time_prefix)
        $image = $this->folder_path."/".$file_name_with_time_prefix;

        $details = array(
          "name" => $request->name,
          "surname" => $request->surname,
          "username" => $request->username,
          "gamer_id" => $request->gamer_id,
          "mobile" => ($request->mobile)? $request->mobile : '',
          "skype" => ($request->skype)? $request->skype:'',
          "twitter" => $request->twitter?$request->twitter:'',
          "termsnconditions" => $request->termsnconditions,
          "email" => $request->email,
          "password" => Hash::make($request->password),
          "verify_token" => $verifyToken,
          "status"  =>  2,
          "image"  =>  $image
        );

        $verifyToken = route( 'celebrity.verifyaccount', [ $verifyToken ] );

        $user = Celebrities::create($details); 
        if($user){

            // send email to celebrity about their registration successfull
            $user_registration_email = Emails::where(['key' => 'celebrity_registration_email'])->first();

            $emailTemplateDecode = html_entity_decode($user_registration_email['email_template']);
            $email_body = str_replace("##name##", $request->name, $emailTemplateDecode);
            $email_body = str_replace("##appname##", \Config::get('constants.app_name'), $email_body);
            
            $emailParams = array("to"=>$request->email, "subject"=>$user_registration_email['subject'], "content"=>$email_body);
            //Method to send email
            $this->sendEmail($emailParams);
            
            /*** Email content here ****/
            $signup_email = Emails::where(['key' => 'verify_email'])->first();

            $emailTemplateDecode = html_entity_decode($signup_email['email_template']);
            $email_body = str_replace("##name##", $request->name, $emailTemplateDecode);
            $email_body = str_replace("##verification_code##", $verifyToken, $email_body);
            
            $emailParams = array("to"=>$request->email, "subject"=>$signup_email['subject'], "content"=>$email_body);
            //Method to send email
            $this->sendEmail($emailParams);

          return redirect()->route('celebrity.login')->with(['forgot_success'=>'Verification link has been sent to your email id.']);
        }else{
          return redirect()->route('celebrity.signup')->with(['error'=>'Error occured while register event manager.']);
        }
    }


    public function verifyAccount($verifyToken=NULL){
      $data = Celebrities::where(["verify_token" => $verifyToken])->first(); 
      if( $data['status']== 2 ){
          Celebrities::where(["verify_token" => $verifyToken])->update(["status"=>0]); 
          return redirect()->route('celebrity.login')->with(['forgot_success'=>'Your account is verified successfully. Please login.']);
      }else{
          return redirect()->route('celebrity.login')->with(['error'=>'Link has been expired.']);
      }

    }

    public function loadStates ($countryId, $stateId){

      $countryId = $this->decodeId($countryId);

      if($stateId){
        $stateId = $this->decodeId($stateId);
      }

      $data = States::where( ["country_id" => $countryId] )->get();

      $option = "";
      if($data){
        foreach ($data as $state) {
          $option .= '<option '.(($stateId == $state['id'])? 'selected':'').' value="'.$this->encodeId($state['id']).'">'.$state['name'].'</option>';
        }
      }
      return $option;
    }


}
