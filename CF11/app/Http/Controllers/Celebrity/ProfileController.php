<?php

namespace App\Http\Controllers\Celebrity;


use Auth;
use App\Http\Controllers\Controller;
use App\Models\Celebrities;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Middleware\RedirectIfAuthenticated;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Input;
use Config;
use xmlapi;
use Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;
use Vinkla\Hashids\Facades\Hashids;
use File;

class ProfileController extends Controller{

  public $folder_path = "uploads/celebrities";

  public function __construct() {
    $this->middleware('guest:celebrity');
  }

  /* Fetch profile of event manager */  
  public function profile(){
    $title = "Profile"; $eventmanagerid = session('manager_id');
    $profile = Celebrities::find($eventmanagerid);
    return view('celebrity.profile', ['title' => $title, "profile"=> $profile, "breadcrumbItem" => "Profile Settings" , "breadcrumbTitle"=> ""]);
  }


  /* Event manager profile information updated */
  public function profileupdate(Request $request){

    $eventmanagerid = session('manager_id');
    $rules = [
        'fullname' => 'required|regex:/^[0-9a-zA-Z\s]+$/u'
    ];

    // $validatedData = $this->validate($request, $rules);
    
    $file_name_with_time_prefix = upload_file($request, 'image' , $this->folder_path );

    $celebrity = Celebrities::find($eventmanagerid);

    if($file_name_with_time_prefix)
    $celebrity->image = $this->folder_path."/".$file_name_with_time_prefix;

    $celebrity->name = $request->name;
    $celebrity->surname = $request->surname ? $request->surname : '';
    $celebrity->username = $request->username;
    $celebrity->gamer_id = $request->gamer_id;
    $celebrity->mobile = ($request->mobile)? $request->mobile : '';
    $celebrity->skype = ($request->skype)? $request->skype:'';
    $celebrity->twitter = $request->twitter?$request->twitter:'';
    $celebrity->updated_at = Carbon::now();
    $celebrity->save();

    if($celebrity){
      $request->session()->put('managername', $request->name);
      return redirect()->route('celebrity.profile')->with(['success'=>'Profile has been updated successfully.']);
    }else{
      return redirect()->back()->with("error","Error occured while updating profile information.");
    }

  }


  /* Event manager update password request handled */
  public function updatepassword(Request $request){

      $eventmanagerid = session('manager_id');
      $obj_user = Celebrities::find($eventmanagerid);

      if (!(Hash::check($request->old_password, $obj_user->password))) {
          // The passwords matches
          return redirect()->back()->with("password_error","Current password is incorrect, please try again.");
      }else if(strcmp($request->old_password, $request->password) == 0){
          //Current password and new password are same
          return redirect()->back()->with("password_error","New Password cannot be same as your current password. Please choose a different password.");
      }else{
          $obj_user->password = \Hash::make($request->password);
          $obj_user->save();   
          return redirect()->back()->with("password_success","Password has been updated successfully.");
      }

  }

  public function deleteImage ( $celebrityId ) { 
    if( !$celebrityId ){
        return response()->json([
            'status' => 0,
            'msg' => 'Invalid request'
        ]); 
    }

    $celebrityId = Hashids::decode($celebrityId);
    if( !$celebrityId ){
        return response()->json([
            'status' => 0,
            'msg' => 'Invalid request'
        ]); 
    }

    $celebrityId = $celebrityId[0];

    $obj = Celebrities::find( $celebrityId );
    if( $obj->count() <= 0 ) { 
        return response()->json([
            'status' => 0,
            'msg' => 'Selected celebrity not found'
        ]);
    }

    if( !$obj->image ) { 
        return response()->json([
            'status' => 0,
            'msg' => 'Selected celebrity image not found'
        ]);
    }

    $isDeleted = File::delete(  'public/' . $obj->image );
    // File::delete($destinationPath.'/your_file');
    if( !$isDeleted ) { 
        return response()->json([
            'status' => 0,
            'msg' => 'Unable to delete image, please try again later!'
        ]);
    }

    $obj->image = '';
    
    if( !$obj->save( ) ) { 
        return response()->json([
            'status' => 0,
            'msg' => 'Unable to delete image, please try again later!'
        ]);
    }
    
    return response()->json([
        'status' => 1,
        'msg' => 'Celebrity image deleted!.'
    ]);

}



}
