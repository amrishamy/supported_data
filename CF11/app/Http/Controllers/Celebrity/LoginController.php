<?php

namespace App\Http\Controllers\Celebrity;

use Auth;
use App\Models\Celebrities;
use App\Models\Emails;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use Config;
use Mail;
use Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:celebrity')->except(['loginForm','authenticate','signupForm','forgotPassword','resetPassword','updateForgotPassword']);
    }


    /**
     * Show the application normal login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function loginForm()
    {
        $user_id = session('user_id');$user_type = session('user_type');
        if(!empty($user_id) && $user_type=='celebrity'){
           return redirect()->route('celebrity.dashboard'); 
        }

        $title = 'Sign In';
        return view('celebrity.login', [
            'title' => $title
        ]);
    }


    /**
     * Login user in the application
     *
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request){
        $request_data = $request->all();
        
        $validatedData = $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        if(Auth::guard('celebrity')->attempt(['email' => $request->email, 'password' => $request->password, 'status'=> 0])) {
            Auth::guard('celebrity')->logout();
            $request->session()->flush(); // this method should be called after we ensure that there is no logged in guards left
            $request->session()->regenerate();
            return redirect()->route('celebrity.login')->with('error','Your account is not approved by admin yet.');
        }elseif( Auth::guard('celebrity')->attempt(['email' => $request->email, 'password' => $request->password, 'status'=> 2]) ){
            Auth::guard('celebrity')->logout();
            $request->session()->flush(); // this method should be called after we ensure that there is no logged in guards left
            $request->session()->regenerate();
            return redirect()->route('celebrity.login')->with('error','Please verify your email address.');
        }else{
            /** $user */
            $user = Auth::guard('celebrity')->attempt(['email' => $request->email, 'password' => $request->password, 'status'=> 1]);
            // print_r($user); die;
            if($user) {
                $celebrityActiveStatusUpdate = Celebrities::where('email', $request->email)->update(['active_status' => 1, 'updated_at' => Carbon::now()]);
                $user = Celebrities::where(['email' => $request->email])->first();
                $request->session()->put('userData', $user);
                $request->session()->put('manager_id', $user->id);
                $request->session()->put('user_type', 'celebrity');
                $request->session()->put('managername', $user->name);
                return redirect()->route('celebrity.dashboard');
            }else {
                return redirect()->route('celebrity.login')->with('error','Invalid username or password.')->withInput();
            } 
        }
    }


    public function forgotPassword(Request $request){
        $user = Celebrities::where(['email' => $request->forgot_email])->first();
        $forgot_email_template = Emails::where(['key' => 'forgot_password'])->first();

        if($user){
            $forgotToken = str_random(20);
            $userUpdate = Celebrities::where('email', $request->forgot_email)->update(['forgotToken' => $forgotToken, 'updated_at' => Carbon::now()]);

            $forgotUrl = \Config::get('constants.app_url').'celebrity/resetPassword/'.$forgotToken;
           
            $emailTemplateDecode = html_entity_decode($forgot_email_template['email_template']);
            $email_body = str_replace("##name##", $user['name'], $emailTemplateDecode);
            $email_body = str_replace("##forgoturl##", $forgotUrl, $email_body);
            $email_body = str_replace("##appname##", \Config::get('constants.app_name'), $email_body);

            $to = $request->forgot_email; $subject = $forgot_email_template['subject'];
            Mail::send([], [], function($message) use($to, $subject, $email_body) {
                $message->setBody($email_body, 'text/html');
                $message->from(\Config::get('constants.from_email'), \Config::get('constants.from_name'));
                $message->to($to);
                $message->subject($subject);
            });

            return redirect()->route('celebrity.login')->with(['flipped_class'=>'flipped','forgot_success'=>'Reset password link has been sent to your email.']);
        }else{
            return redirect()->route('celebrity.login')->with(['flipped_class'=>'flipped','forgot_error'=>'Email id does not exist.']);
        }
    }


    //Hash::make($data['password'])
    /*
     * function to show reset password view
     */
    public function resetPassword(Request $request, $token)
    {
        $request->session()->put('token', $token);
        $title = 'Reset Password';
        return view('celebrity.resetPassword', [
            'title' => $title
        ]);
    }



    /*
     * function to reset password
     * parameters : new_password , token
     */
    public function updateForgotPassword(Request $request)
    {
        echo $token = $request->session()->get('token');

        $rules = [
            'password' => 'required|min:6|required_with:cpassword|same:cpassword',
            'cpassword' => 'required|min:6'
        ];
        
        $messages = [
            'password.required' => 'Password field is required.',
            'cpassword.required' => 'Confirm password field is required',
            'password.same' => 'Passwords mismatch.',
            'password.min' => 'Password must be at least 6 characters.',
            'cpassword.min' => 'Confirm password must be at least 6 characters.',

        ];

        $validatedData = $this->validate($request, $rules, $messages 
        );

        /*$validatedData = $this->validate($request, [
            'password' => 'required|min:6|required_with:cpassword|same:cpassword',
            'cpassword' => 'required|min:6'
        ]);*/


        $chkUser = Celebrities::where('forgotToken', $token)->first();
        //dd($chkUser);
        if($chkUser) {
            $passwordUpdate = Celebrities::where('id', $chkUser->id)->update(['password' => Hash::make($request->password), 'forgotToken' => '', 'updated_at' => Carbon::now()]);
            
            if ($passwordUpdate) {
               return redirect()->route('celebrity.resetpassword',[$token])->with(['success'=>'Success']);
            } else {
                return redirect()->route('celebrity.resetpassword',[$token])->with(['error'=>'Error occured while updating password.']);
            }

        } else {
            return redirect()->route('celebrity.resetpassword',[$token])->with(['error'=>'Reset password link has been expired.']);
        }

    }

    public function logout(Request $request){
        $eventmanager = session('manager_id');
        $celebrityActiveStatusUpdate = Celebrities::where('id', $eventmanager)->update(['active_status' => 0, 'updated_at' => Carbon::now()]);
        if($celebrityActiveStatusUpdate){
            Auth::guard('celebrity')->logout();
            $request->session()->forget('userData');
            $request->session()->forget('manager_id');
            $request->session()->forget('celebrity');
            $request->session()->forget('user_type');
            $request->session()->forget('managername');
            return Redirect::route('celebrity.login');
        }else{
            return Redirect::route('celebrity.dashboard');
        }
    }



}
