<?php

namespace App\Http\Controllers\Celebrity;

use Auth;
use App\Http\Controllers\Controller;
// use App\Models\Event;
use App\Models\Celebrities;
use App\Models\Team;
use App\Models\User;
use App\Models\Slots;
use App\Models\Games;
use App\Models\UserParents;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Middleware\RedirectIfAuthenticated;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Input;
use Config;
use xmlapi;


class DashboardController extends Controller{

  public function __construct() {
    $this->middleware('guest:celebrity');
  }

  
  /* get userlisting  */
  public function dashboard(Request $request){


    $title = "Celebrity Dashboard";
    // $events = Event::where(["event_manager" => session('manager_id')])->get();
    $eventCount = 1;

    // $teams = Team::where(["event_manager" => session('manager_id')])->get();
    $teamCount = 1;

    // $users = UserParents::where(["event_manager" => session('manager_id')])->get();
    $gamecount = Slots::where( "celebrity_id" , session('manager_id') )->get()->count();

    return view('celebrity.dashboard.dashboard', ['title' => $title, 'eventsCount' => $eventCount, "teamCount" => $teamCount, "gamecount" => $gamecount,"breadcrumbItem"=>"Dashboard" , "breadcrumbTitle"=>"Dashboard"]);
  }

  public function eventsListing ()
  {
    $curtime = date('Y-m-d');
    $eventmanager = session('manager_id');

    $slotTblName        = with( new Slots )->getTable( ); 
    $gameTblName        = with( new Games )->getTable( ); 

    $events = Slots::where(["celebrity_id" => session('manager_id')])->whereRaw('slot_date >= CURDATE()')
    ->leftJoin( $gameTblName, function( $query ) use( $slotTblName, $gameTblName ){
      $query->on( $slotTblName.".game_id", "=", $gameTblName.".id" );
      // $query->on( $slotTblName.".status", "=", \DB::raw( 1 ) );
  })
    ->get();

    $data = array();
    if($events){
      foreach ($events as $row) {
        $data[] = array(
          'id'   => $row["id"],
          'title'   => $row["name"],
          'start'   =>  $row['slot_date']."T".$row["slot_start_time"],
          // 'end'   => date('Y-m-d H:i:s', $row['end_time']),
         );
      }
    }

    echo json_encode($data);
  }


  public function changeStatus(Request $request){
        $celebrities = Celebrities::find($request->user_id);
        $celebrities->active_status = $request->status;
        $celebrities->updated_at = Carbon::now();
        $celebrities->save();
        if($celebrities){
          $user = Celebrities::where(['id' => $request->user_id])->first();
          $request->session()->put('userData', $user);
          return response()->json(['status'=>'success','message'=>'Status change successfully.']);
        }else{
          return response()->json(['status'=>'failed','message'=>'some error happened']);
        }
  }

}
