<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Traits\CommonMethods;

use App\Models\Sliders;
use App\Models\Games;
use App\Models\Slots;
use App\Models\Celebrities;

use Config;
use DB;

class HomeController extends Controller
{
    
    use CommonMethods;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $page = "Home";
        $celebList = $this->get_celebrities_list() ;
        $gameList = $this->get_games_list() ;
        
        $platforms = Config::get('constants.platforms');
        $sliders = Sliders::where('status',1)->orderBy('sort_order', 'asc')->get();
        $games = Games::where('status',1)->get();

        $topCelebrities = Celebrities::where('status',1)->get();

        /*$todayDate = date( 'Y-m-d' );*/
        $todayDate = '';
        $filteredCGRecords = $this->getCelebrityGamesRecords( [ 
                                /*'filter_for_date' => $todayDate,*/
                                'detailPage' => true,
                                'limit' => true,
                                'row' => 0,
                                'rowperpage' => 3,
                                'get_obj' => true
                             ] );
        return view( 'home', ['page'=>$page, 'sliders'=>$sliders, 'games'=>$games, 'platforms'=>$platforms, 'topCelebrities'=>$topCelebrities, 'filtered_cg_records'=>$filteredCGRecords, 'celebList'=>$celebList,'gamesList'=>$gameList, 'calObj' => $this->cal( ), 'postedData' => [ 'date' => $todayDate ] ] );
    }

    public function getCelebritiesAndGames( Request $request ) {

        $postedData = $request->all( );

        /*if( !$postedData[ 'date' ] ){
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]);
        }*/

        $data = [];
        $data[ 'filter_for_date' ] = $postedData[ 'date' ]?date( 'Y-m-d', strtotime( $postedData[ 'date' ] ) ):'';
        if( !$data[ 'filter_for_date' ] ) $data[ 'detailPage' ] = true;

        if( isset( $postedData[ 'time' ] ) && $postedData[ 'time' ] ) { 
            $data[ 'slot_time' ] = $postedData[ 'time' ];
        }

        if( isset( $postedData[ 'c_id' ] ) && $postedData[ 'c_id' ] ) { 
            $data[ 'celebrity_id' ] = $postedData[ 'c_id' ];
        }

        if( isset( $postedData[ 'g_id' ] ) && $postedData[ 'g_id' ] ) { 
            $data[ 'game_id' ] = $postedData[ 'g_id' ];
        }

        $data[ 'limit' ] = true;
        $data[ 'row' ] = 0;
        $data[ 'rowperpage' ] = 3;
        $data[ 'get_obj' ] = true;

        $celebList = $this->get_celebrities_list() ;
        $gameList = $this->get_games_list() ;
        $date = $request->get( 'date' );
        $filteredCGRecords = $this->getCelebrityGamesRecords( $data );

        return response()->json( ['status' => 1, 'html' => view( 'includes.complete_calendar_section', [ 'calObj' => $this->cal( $date ), 'filtered_cg_records'=>$filteredCGRecords, 'celebList'=>$celebList,'gamesList'=>$gameList, 'postedData' => $postedData ] )->render() ] );
    }

    public function getByGames( Request $request ) {

        $postedData = $request->all( );

        /*if( !$postedData[ 'date' ] ){
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]);
        }*/

        $data = [];
        $data[ 'filter_for_date' ] = $postedData[ 'date' ]?date( 'Y-m-d', strtotime( $postedData[ 'date' ] ) ):'';
        if( !$data[ 'filter_for_date' ] ) $data[ 'detailPage' ] = true;

        if( isset( $postedData[ 'time' ] ) && $postedData[ 'time' ] ) { 
            $data[ 'slot_time' ] = $postedData[ 'time' ];
        }

        if( isset( $postedData[ 'c_id' ] ) && $postedData[ 'c_id' ] ) { 
            $data[ 'celebrity_id' ] = $postedData[ 'c_id' ];
        }

        if( isset( $postedData[ 'g_id' ] ) && $postedData[ 'g_id' ] ) { 
            $data[ 'game_id' ] = $postedData[ 'g_id' ];
        }

        $data[ 'limit' ] = true;
        $data[ 'row' ] = 0;
        $data[ 'rowperpage' ] = 3;
        $data[ 'get_obj' ] = true;

        $celebList = $this->get_celebrities_list() ;
        $gameList = $this->get_games_list() ;
        $date = $request->get( 'date' );
        $filteredCGRecords = $this->getCelebrityGamesRecords( $data );

        return response()->json( ['status' => 1, 'html' => view( 'includes.celebrities_and_games', [ 'calObj' => $this->cal( $date ), 'filtered_cg_records'=>$filteredCGRecords, 'celebList'=>$celebList,'gamesList'=>$gameList, 'postedData' => $postedData ] )->render() ] );

    }


    public function getByCelebrities( Request $request ) {

        $postedData = $request->all( );

        /*if( !$postedData[ 'date' ] ){
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]);
        }*/

        $data = [];
        $data[ 'filter_for_date' ] = $postedData[ 'date' ]?date( 'Y-m-d', strtotime( $postedData[ 'date' ] ) ):'';
        if( !$data[ 'filter_for_date' ] ) $data[ 'detailPage' ] = true;

        if( isset( $postedData[ 'time' ] ) && $postedData[ 'time' ] ) { 
            $data[ 'slot_time' ] = $postedData[ 'time' ];
        }

        if( isset( $postedData[ 'c_id' ] ) && $postedData[ 'c_id' ] ) { 
            $data[ 'celebrity_id' ] = $postedData[ 'c_id' ];
        }

        if( isset( $postedData[ 'g_id' ] ) && $postedData[ 'g_id' ] ) { 
            $data[ 'game_id' ] = $postedData[ 'g_id' ];
        }

        $data[ 'limit' ] = true;
        $data[ 'row' ] = 0;
        $data[ 'rowperpage' ] = 3;
        $data[ 'get_obj' ] = true;

        $celebList = $this->get_celebrities_list() ;
        $gameList = $this->get_games_list() ;
        $date = $request->get( 'date' );
        $filteredCGRecords = $this->getCelebrityGamesRecords( $data );

        return response()->json( ['status' => 1, 'html' => view( 'includes.celebrities_and_games', [ 'calObj' => $this->cal( $date ), 'filtered_cg_records'=>$filteredCGRecords, 'celebList'=>$celebList,'gamesList'=>$gameList, 'postedData' => $postedData ] )->render() ] );

    }

    public function getCalendar( Request $request ) {
        $date = $request->get( 'date' );
        return response()->json( ['status' => 1, 'html' => view( 'includes.calendar', [ 'calObj' => $this->cal( $date ) ] )->render() ] );
    }

}
