<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Sliders;
use App\Models\Games;
use App\Models\Slots;
use App\Models\Celebrities;
use Vinkla\Hashids\Facades\Hashids;

use Config;
use DB;

class CelebritiessListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = "Celebrity";
        $topCelebrities = Celebrities::where('status',1)->get();
        $platforms = Config::get('constants.platforms');
        return view( 'frontend.celebritieslist', ['page'=>$page,'topCelebrities'=>$topCelebrities,'platforms'=>$platforms  ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile($id)
    {
        $page = "Celebrity Profile";
        $topCelebrities = Celebrities::where('status',1)->get();
        $platforms = Config::get('constants.platforms');

        $decryptedId = Hashids::decode($id);
        if( !$decryptedId ){
            return redirect()->route('frontend.celebritieslist')->with('error','Some information is missing!');
        }
        
        $info = Celebrities::find( $decryptedId[ 0 ] );
        if( !$info ){
            return redirect()->route('frontend.celebritieslist')->with('error','Record not found!');
        }
        

        return view( 'frontend.celebrity_profile', ['page'=>$page,'topCelebrities'=>$topCelebrities,'platforms'=>$platforms,'info'=>$info ] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
