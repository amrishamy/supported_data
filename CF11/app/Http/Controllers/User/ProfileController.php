<?php

namespace App\Http\Controllers\User;


use Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Middleware\RedirectIfAuthenticated;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Input;
use Config;
use xmlapi;
use Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;

class ProfileController extends Controller{

  public function __construct() {
    $this->middleware('guest:web');
  }

  /* Fetch profile of event manager */  
  public function profile(){
    $title = "Profile"; 
    $page = "Manage Profile"; 
    $userid = session('manager_id');
    $profile = User::find($userid);

    return view('user.profile', ['title' => $title, "profile"=> $profile, "breadcrumbItem" => "Profile Settings" , "breadcrumbTitle"=> "",'page'=>$page ]);
  }


  /* Event manager profile information updated */
  public function profileupdate(Request $request){

    $eventmanagerid = session('manager_id');
    $rules = [
        'first_name' => 'required|regex:/^[0-9a-zA-Z\s]+$/u',
        'email' => 'required|unique:users,email,'. $eventmanagerid,
        "mobile_number" => 'required',
        "gender" => 'required',
        "gamer_id" => 'required'
    ];

    $validatedData = $this->validate($request, $rules);
   
    $user = User::find($eventmanagerid);
    $user->first_name = $request->first_name;
    $user->last_name = $request->last_name;
    $user->email = $request->email;
    $user->mobile_number = $request->mobile_number;
    $user->mobile_number = $request->mobile_number;
    $user->updated_at = Carbon::now();
    $user->save();

    if($user){
      $request->session()->put('name', $request->first_name);
      return redirect()->route('user.profile')->with(['success'=>'Profile has been updated successfully.']);
    }else{
      return redirect()->back()->with("error","Error occured while updating profile information.");
    }

  }

  public function changepassword(){
    $title = "Change Password"; 
    $page = "Change Password"; 
    $userid = session('manager_id');
    $profile = User::find($userid);

    return view('user.changepassword', ['title' => $title, "profile"=> $profile, "breadcrumbItem" => "Change Password" , "breadcrumbTitle"=> "",'page'=>$page ]);
  }

  /* Event manager update password request handled */
  public function updatepassword(Request $request){

      $eventmanagerid = session('manager_id');
      $obj_user = User::find($eventmanagerid);

      if (!(Hash::check($request->old_password, $obj_user->password))) {
          // The passwords matches
          return redirect()->back()->with("password_error","Current password is incorrect, please try again.");
      }else if(strcmp($request->old_password, $request->password) == 0){
          //Current password and new password are same
          return redirect()->back()->with("password_error","New Password cannot be same as your current password. Please choose a different password.");
      }else{
          $obj_user->password = \Hash::make($request->password);
          $obj_user->save();   
          return redirect()->back()->with("password_success","Password has been updated successfully.");
      }

  }





}
