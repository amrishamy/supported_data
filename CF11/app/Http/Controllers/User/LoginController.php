<?php

namespace App\Http\Controllers\User;

use Auth;
use App\Models\User;
use App\Models\Emails;
use App\Models\Slots;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use Config;
use Mail;
use Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:web')->except(['loginForm','authenticate','signupForm','forgotPassword','resetPassword','updateForgotPassword','check_user_login']);
    }


    /**
     * Show the application normal login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function loginForm()
    {
        $user_id = session('user_id');$user_type = session('user_type');
        if(!empty($user_id) && $user_type=='user'){
           return redirect()->route('user.dashboard'); 
        }

        $title = 'Sign In';
        return view('user.login', [
            'title' => $title
        ]);
    }


    /**
     * Login user in the application
     *
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request){
        $validatedData = $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        $request_data = $request->all();
        /*if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'status'=> 0])) {
            Auth::guard('web')->logout();
            $request->session()->flush(); // this method should be called after we ensure that there is no logged in guards left
            $request->session()->regenerate();
            return redirect()->route('user.login')->with('error','Your account is not approved yet.');
        }else{*/
            /** $user */
            $isLoggedIn = Auth::attempt(['email' => $request->email, 'password' => $request->password ]);
            if(!$isLoggedIn) {
                return response()->json( [ 'error' => 'Username or password incorrect.' ] );
            }

            $user = User::where(['email' => $request->email])->first();
            $request->session()->put('manager_id', $user->id);
            $request->session()->put('user_type', 'user');
            $request->session()->put('managername', $user->first_name);

            $slot_id = ( isset( $request->slot_id ) && $request->slot_id )?$request->slot_id:false;
            if( $slot_id ) { 
                return response()->json( [ 'success' => 'You are logged in successfully.', 'links' => view( 'includes.user_auth_menu_links', [ 'page' => '' ] )->render(), 'slot_id' => $slot_id ] );
            }

            return response()->json( [ 'success' => 'You are logged in successfully.', 'redirectTo' => route( 'user.dashboard' ) ] );
        /*}*/
    }


    public function forgotPassword(Request $request){
        $user = User::where(['email' => $request->forgot_email])->first();
        $forgot_email_template = Emails::where(['key' => 'forgot_password'])->first(); // We are using slugs to get data of a paricular email content.

        if($user){
            $forgotToken = str_random(20);
            $userUpdate = User::where('email', $request->forgot_email)->update(['forgotToken' => $forgotToken, 'updated_at' => Carbon::now()]);
           
            $forgotUrl = \Config::get('constants.app_url').'user/resetPassword/'.$forgotToken;

            $emailTemplateDecode = html_entity_decode($forgot_email_template['email_template']);
            $email_body = str_replace("##name##", $user['first_name'], $emailTemplateDecode);
            $email_body = str_replace("##forgoturl##", $forgotUrl, $email_body);
            $email_body = str_replace("##appname##", \Config::get('constants.app_name'), $email_body);

            $to = $request->forgot_email; $subject = $forgot_email_template['subject'];
            Mail::send([], [], function($message) use($to, $subject, $email_body) {
                $message->setBody($email_body, 'text/html');
                $message->from(\Config::get('constants.from_email'), \Config::get('constants.from_name'));
                $message->to($to);
                $message->subject($subject);
            });

            return response()->json( [ 'success' => 'Reset password link sent on your registered email.' ] );
            
        }else{
            
            return response()->json( [ 'error' => 'Email id does not exist.' ] );
        }
    }


    //Hash::make($data['password'])
    /*
     * function to show reset password view
     */
    public function resetPassword(Request $request, $token)
    {
        $request->session()->put('token', $token);
        $title = 'Reset Password';
        $page = 'Reset Password';
        return view('user.resetPassword', [
            'title' => $title,
            'page' => $page
        ]);
    }

    // function to check User is Logged In or Not
    public function check_user_login(Request $request)
    {
        if (!Auth::user())
        {
            $status = 0 ;
            return response()->json( [ 'status' => $status ] );
        }
        else{
            $status = 1 ;
            $platforms = Config::get('constants.platforms');
            $slot_id = $request->slot_id ;
            $slot_detail = Slots::find( $slot_id ) ;
            $slot_platform = explode(',', $slot_detail->platform_type ) ;
            $platform_dropDown = '';
            foreach($platforms as $k=>$v){
                if(in_array($k,$slot_platform ) )
                {
                    $platform_dropDown .= '<option value="'.$k.'"> '.$v.' </option>';
                }

            }
            $html = '<button type="button" class="close" data-dismiss="modal">&times;</button>
            <h2 class="mb-4 text-center">Confirmation</h2>
            <form method="post" name="send_request_frm" id="send_request_frm" action="'. route("user.send_booking_request").'" class="needs-validation">
                '. csrf_field().' 
                <input type="hidden" name="requested_slot_id" value="'.$slot_id.'" >
                <div class="form-group">						 
                    <select class="form-control requested_slot_platform" name="requested_slot_platform"  >
                        <option value=""> Select platform </option>
                        '.$platform_dropDown.'
                    </select>
                </div>
                <div class="form-group">						 
                    <input type="text" class="form-control" id="gamer_id" placeholder="Gamer ID" name="gamer_id" maxlength="25" />
                </div>	
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-sm btn-success m-2">Submit</button>
                    <a class="modelChange btn btn-sm btn-red m-2" href="" data-dismiss="modal" >Cancel</a>
                </div>						
            </form>';
            return response()->json( [ 'status' => $status, 'html'=> $html ] );
        }

    }

    /*
     * function to reset password
     * parameters : new_password , token
     */
    public function updateForgotPassword(Request $request)
    {
        $token = $request->session()->get('token');

        $rules = [
            'password' => 'required|min:6|required_with:cpassword|same:cpassword',
            'cpassword' => 'required|min:6'
        ];
        
        $messages = [
            'password.required' => 'Password field is required.',
            'cpassword.required' => 'Confirm password field is required',
            'password.same' => 'Passwords mismatch.',
            'password.min' => 'Password must be at least 6 characters.',
            'cpassword.min' => 'Confirm password must be at least 6 characters.',

        ];

        $validatedData = $this->validate($request, $rules, $messages 
        );


        $chkUser = User::where('forgotToken', $token)->first();
        //dd($chkUser);

        if($chkUser) {
            $passwordUpdate = User::where('id', $chkUser->id)->update(['password' => Hash::make($request->password), 'forgotToken' => '', 'updated_at' => Carbon::now()]);
            if ($passwordUpdate) {
                // Redirect admin to dashboard page after successfully update password
                /*$user = Auth::guard('web')->attempt(['email' => $chkUser->email, 'password' => $request->password]); 
                $request->session()->put('manager_id', $chkUser->id);$request->session()->put('user_type', 'user');
                $request->session()->put('managername', $chkUser->name);$request->session()->put('companyname', $chkUser->company_name);
                return redirect()->route('user.dashboard');*/

               return redirect()->route('user.resetpassword',[$token])->with(['success'=>'Success']);
            } else {
                return redirect()->route('user.resetpassword',[$token])->with(['error'=>'Error occured while updating password.']);
            }
        } else {
            return redirect()->route('user.resetpassword',[$token])->with(['error'=>'Update password link has been expired.']);
        }

    }



    public function logout(Request $request){
        Auth::guard('web')->logout();
        $request->session()->forget('user');
        $request->session()->forget('user_type');
        $request->session()->forget('managername');
        return Redirect::route('homepage');
    }



}
