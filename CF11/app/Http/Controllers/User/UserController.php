<?php

namespace App\Http\Controllers\User;

use Auth;
use App\Models\User;
use App\Models\Countries;
use App\Models\States;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use Config;
use Mail;
use Hash;
use App\Http\Traits\CommonMethods;
use App\Models\Emails;
use App\Models\Slots;
use App\Models\SlotRequest;
use App\Models\Celebrities;

class UserController extends Controller
{
    use CommonMethods;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:web')->except(['signupForm','register','verifyAccount','loadStates']);
    }



    /**
    User signup form view
    **/
    public function signupForm(){
        $user_id = session('manager_id');$user_type = session('user_type');
        if(!empty($user_id) && $user_type=='user'){
           return redirect()->route('user.dashboard'); 
        }

        $title = 'User Signup';
        $countries = Countries::all();
        return view('user.signup', [
            'title' => $title,
            'countries' => $countries,
        ]);
    }


    /**
    Registeration process of User
    **/
    public function register(Request $request){ 
        $validatedData = $this->validate($request, [
            'first_name' => 'required|regex:/^[0-9a-zA-Z\s]+$/u',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:6',
            "mobile_number" => 'required',
            "gender" => 'required',
            "gamer_id" => 'required'
            // 'country' => 'required',
            // 'state' => 'required',
        ]);

        $verifyToken = str_random(20);

        $details = array(
          "first_name" => $request->first_name,
          "last_name" => $request->last_name,
          "mobile_number" => $request->mobile_number,
          "gender" => $request->gender,
          "email" => $request->email,
          "password" => Hash::make($request->password),
          "verify_token" => $verifyToken,
          "gamer_id" => $request->gamer_id,
          // "country_id" => $this->decodeId($request->country),
          // "state_id" => $this->decodeId($request->state)
        );

        $verifyToken = url('/user/accountVerify').'/'.$verifyToken;

        $user = User::create($details); 
        //  print_r($user);exit();
        if(!$user){
          return response()->json( [ 'error' => 'Error occured while creating you account.' ] );
        }

        /*** Email content here ****/
        /*$signup_email = Emails::where(['key' => 'verify_email'])->first();

        $emailTemplateDecode = html_entity_decode($signup_email['email_template']);
        $email_body = str_replace("##name##", $request->fullname, $emailTemplateDecode);
        $email_body = str_replace("##verification_code##", $verifyToken, $email_body);
        
        $emailParams = array("to"=>$request->email, "subject"=>$signup_email['subject'], "content"=>$email_body);
        //Method to send email
        $this->sendEmail($emailParams);

        return redirect()->route('user.login')->with(['forgot_success'=>'Verification link has been sent to your email id.']);*/

        $isLoggedIn = Auth::attempt(['email' => $request->email, 'password' => $request->password ]);
        if(!$isLoggedIn) {
          return response()->json( [ 'error' => 'Unable to logged in, Please try again.' ] );
        }

        $user = User::where(['email' => $request->email])->first();
        $request->session()->put('manager_id', $user->id);
        $request->session()->put('user_type', 'user');
        $request->session()->put('managername', $user->first_name);
        return response()->json( [ 'success' => 'Your account is created successfully.', 'redirectTo' => route( 'user.dashboard' ) ] );

    }

    public function verifyAccount($verifyToken=NULL){
      //die('asdjad');
      $data = User::where("verify_token",$verifyToken)->first(); 
      //print_r($data);exit();
      if($data['status']==0){
          
          User::where("verify_token" , $verifyToken)->update(["status"=>1]); 
          return redirect()->route('user.login')->with(['forgot_success'=>'Your account is verified successfully. Please login.']);
      }else{
          return redirect()->route('user.login')->with(['error'=>'Link has been expired.']);
      }

    }

    // 
    public function send_booking_request(Request $request)
    {
        // echo "<pre>";print_r( $request->all() );
        $validatedData = $this->validate($request, [
          'requested_slot_id' => 'required',
          'requested_slot_platform' => 'required',
          'gamer_id' => 'required',
        ]);

        $slot = Slots::find( $request->requested_slot_id );
        if( !$slot ) { 
          return response()->json([ 'error' => 'Selected slot not found' ]) ;
        }

        $celebrity = Celebrities::where( 'status', 1 )->find( $slot->celebrity_id );
        if( !$celebrity ) { 
          return response()->json([ 'error' => 'Selected celebrity not found' ]) ;
        }

        $gamer = User::where( 'status', 1 )->find( session('manager_id') );
        if( !$gamer ) { 
          return response()->json([ 'error' => 'Gamer not found' ]) ;
        }

        $slotRequest = SlotRequest::where( 'requested_platform_id', $request->requested_slot_platform )->where( 'requested_slot_id', $request->requested_slot_id )->where( 'user_id', session('manager_id') )->first();
        if( $slotRequest ) { 
          return response()->json( [ 'error' => 'Booking request already sent.' ] );
        }

        $SlotRequest =  new SlotRequest();
        $SlotRequest->user_id = session('manager_id');
        $SlotRequest->requested_slot_id = $request->requested_slot_id ;
        $SlotRequest->requested_platform_id = $request->requested_slot_platform ;
        $SlotRequest->gamer_id = $request->gamer_id ;
        
        if ( !$SlotRequest->save() ) { 
          return response()->json([ 'error' => 'Your request not sent' ]) ;
        }
        // Notify to celeberity user  
        $user = Celebrities::where(['email' => $celebrity->email])->first();
        $new_gamer_email_template = Emails::where(['key' => 'new_gamer_request'])->first(); // We are using slugs to get data of a paricular email content.

        if($user){
            
            $loginUrl = \Config::get('constants.app_url').'celebrity/login';

            $emailTemplateDecode = html_entity_decode($new_gamer_email_template['email_template']);
            $email_body = str_replace("##name##", $user['name'], $emailTemplateDecode);
            $email_body = str_replace("##loginUrl##", $loginUrl, $email_body);
            $email_body = str_replace("##appname##", \Config::get('constants.app_name'), $email_body);

            $to = $celebrity->email ; $subject = $new_gamer_email_template['subject'];
            Mail::send([], [], function($message) use($to, $subject, $email_body) {
                $message->setBody($email_body, 'text/html');
                $message->from(\Config::get('constants.from_email'), \Config::get('constants.from_name'));
                $message->to($to);
                $message->subject($subject);
            });
        }

        // Notify to Gamer user  
        $user = User::where(['email' => $gamer->email])->first();
        $new_gamer_email_template = Emails::where(['key' => 'gamer_request_acknowledgement'])->first(); // We are using slugs to get data of a paricular email content.

        if($user){
            
            $loginUrl = \Config::get('constants.app_url').'celebrity/login';

            $emailTemplateDecode = html_entity_decode($new_gamer_email_template['email_template']);
            $email_body = str_replace("##name##", $user['first_name'], $emailTemplateDecode);
            // $email_body = str_replace("##loginUrl##", $loginUrl, $email_body);
            $email_body = str_replace("##appname##", \Config::get('constants.app_name'), $email_body);

            $to = $gamer->email ; $subject = $new_gamer_email_template['subject'];
            Mail::send([], [], function($message) use($to, $subject, $email_body) {
                $message->setBody($email_body, 'text/html');
                $message->from(\Config::get('constants.from_email'), \Config::get('constants.from_name'));
                $message->to($to);
                $message->subject($subject);
            });
        }

        return response()->json([ 'success' => 'Your request sent successfully' ]) ;
      
    }

    public function loadStates ($countryId, $stateId){

      $countryId = $this->decodeId($countryId);

      if($stateId){
        $stateId = $this->decodeId($stateId);
      }

      $data = States::where( ["country_id" => $countryId] )->get();

      $option = "";
      if($data){
        foreach ($data as $state) {
          $option .= '<option '.(($stateId == $state['id'])? 'selected':'').' value="'.$this->encodeId($state['id']).'">'.$state['name'].'</option>';
        }
      }
      return $option;
    }


}
