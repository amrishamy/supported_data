<?php

namespace App\Http\Controllers\User;

use Auth;
use App\Http\Controllers\Controller;
use App\Models\Team;
use App\Models\User;
use App\Models\UserParents;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Middleware\RedirectIfAuthenticated;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Input;
use Config;
use xmlapi;
use App\Http\Traits\CommonMethods;


class DashboardController extends Controller{

  use CommonMethods;

  public function __construct() {
    $this->middleware('guest:web');
  }

  
  /* get userlisting  */
  public function dashboard(Request $request){

    $title = "Dashboard";
    $page = "Dashboard";
    $userid = session('manager_id');
    $count_booked_slots     = $this->getUserGamesRecords( [ 'booking_request_for' => 2, 'return_count' => true ] ) ;
    $count_requested_slots   = $this->getUserGamesRecords( [ 'booking_request_for' => 1, 'return_count' => true ] ) ;
    $count_played_games     = $this->getUserGamesRecords( [ 'booking_request_for' => 3, 'return_count' => true ] ) ;
    return view('user.dashboard.dashboard', ['title' => $title, "breadcrumbItem"=>"Dashboard" , "breadcrumbTitle"=>"",'page' => $page, 'count_booked_slots' => $count_booked_slots,'count_requested_slots' => $count_requested_slots, 'count_played_games' => $count_played_games ]);
  }

}
