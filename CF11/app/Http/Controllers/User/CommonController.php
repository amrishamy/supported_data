<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Vinkla\Hashids\Facades\Hashids;

use App\Models\Sliders;

use Mail;
use Hash;
use App\Http\Traits\CommonMethods;
use App\Models\Emails;
use App\Models\Celebrities;

class CommonController extends Controller
{
  use CommonMethods;
  private static $instance;

  public function __construct() {
    
  }

  public static function getInstance() {
      if (!isset(self::$instance)) {
          self::$instance = new static();
      }
      return self::$instance;
  }

  
  /* Delete the single row of a model  */
  public function deleteRow(Request $request){
    $rowid = Hashids::decode($request->rowid); $model = $request->affected_data_model;
    if(isset($rowid) && isset($model)){
      $delete = DB::table($model)->where(["id"=>$rowid[0]])->delete();
      if($model == "sliders")
      {
        Sliders::ResetOrder();
      }
      try {
        if($delete){
          echo json_encode(array("code"=>200, "message"=>"Row deleted successfully."));
        }else{
          echo json_encode(array("code"=>203, "message"=>"Error occured while deleting user."));
        }
      
      }catch (Exception $e) {
           echo json_encode(array("code"=>500, "message"=>$e->getMessage()));
      }
      
    }else{
      echo json_encode(array("code"=>500, "message"=>"Some information is missing!"));
    }
  }

  /* Delete bulk rows of a model  */
  public function updateBulkRows(Request $request){
    $idsArr = array();
    foreach ($request->rowids as $value) {
      $rowids = Hashids::decode($value); 
      $idsArr[] = $rowids[0];
    }   
    $actiontype = $request->actiontype; $model = $request->affected_data_model; $alertText = "updated";
    if(isset($actiontype) && isset($model)){
      if($actiontype==2){
        $alertText = "deleted";
        $delete = DB::table($model)->whereIn('id', $idsArr)->delete();
      }else{
        $delete = DB::table($model)->whereIn('id', $idsArr)->update(["status"=>$actiontype]);
        if($actiontype == 1 && $model == "celebrities")
        {
          // send email to celebrity that they are approved
          $celebrity_approved_email = Emails::where(['key' => 'celebrity_approved_email'])->first();
          foreach($idsArr as $c_id)
          {
            $celebrity = Celebrities::where( 'status', 1 )->find( $c_id );
            if($celebrity){
              $loginUrl = \Config::get('constants.app_url').'celebrity/login';
              $emailTemplateDecode = html_entity_decode($celebrity_approved_email['email_template']);
              $email_body = str_replace("##name##", $celebrity['name'], $emailTemplateDecode);
              $email_body = str_replace("##loginUrl##", $loginUrl, $email_body);
              $email_body = str_replace("##appname##", \Config::get('constants.app_name'), $email_body);
              
              $emailParams = array("to"=>$celebrity['email'], "subject"=>$celebrity_approved_email['subject'], "content"=>$email_body);
              //Method to send email
              $this->sendEmail($emailParams);
            }
          }
          


        }

      }
      try {
        if( $delete >= 0 ) { 
          echo json_encode(array("code"=>200, "message"=> (count($idsArr) > 1 ? "Records ":"Record ") .$alertText." successfully."));
        }else{
          echo json_encode(array("code"=>203, "message"=>"Error occured while updating data."));
        }
      }catch(Exception $e){
          echo json_encode(array("code"=>500, "message"=>$e->getMessage()));
      }

    }else{
        echo json_encode(array("code"=>500, "message"=>"Some information is missing!"));
    }

  }



  /* Common method to set Active/Inactive status  */
  public function setActiveInactive(Request $request){
    $rowid = Hashids::decode($request->affected_id); $model = $request->affected_data_model; $status = $request->status;
    if(isset($rowid) && isset($model)){
      $update = DB::table($model)->where(["id"=>$rowid[0]])->update(['status' => $status]);
      
      try {
        if($update){
          echo json_encode(array("code"=>200, "message"=>"Status updated successfully."));
        }else{
          echo json_encode(array("code"=>203, "message"=>"Error occured while updating user."));
        }
      
      }catch (Exception $e) {
           echo json_encode(array("code"=>500, "message"=>$e->getMessage()));
      }
      
    }else{
      echo json_encode(array("code"=>500, "message"=>"Some information is missing!"));
    }
  }

  



}
