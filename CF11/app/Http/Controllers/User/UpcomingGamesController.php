<?php

namespace App\Http\Controllers\User;

use Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Middleware\RedirectIfAuthenticated;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Input;
use Config;
use xmlapi;
use Hash;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;
use App\Http\Traits\CommonMethods;
use Vinkla\Hashids\Facades\Hashids;

use App\Models\Games;
use App\Models\Slots;
use App\Models\Celebrities;
use App\Models\SlotRequest;
use App\Models\Sliders;
use DB;
use Response;

class UpcomingGamesController extends Controller
{
    use CommonMethods;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('guest:web');
    }
    
    public function index()
    {
        $title = "Upcoming Games"; 
        $page = "Upcoming Games"; 
        $userid = session('manager_id');
        $celebList = $this->get_celebrities_list();
        $gameList = $this->get_games_list() ;
        $platforms = Config::get('constants.platforms');

        $data = [];
       
        $data[ 'request_status' ] = 0 ;


        $filteredRecords = []; //$this->getUserGamesRecords( $data );
    
        return view('user.upcoming_games', ['title' => $title, "filteredRecords"=> $filteredRecords, "breadcrumbItem" => $title , "breadcrumbTitle"=> "",'page'=>$page, 'platforms'=>$platforms, 'celebList'=>$celebList,'gamesList'=>$gameList ]);
    }


    public function bookingsListAjax(Request $request){

        $platforms = Config::get('constants.platforms');

        $draw = $_GET['draw'];
        
        $row = $_GET['start'];
        $rowperpage = $_GET['length']; // Rows display per page

        $columnIndex = $_GET['order'][0]['column']; // Column index
        $columnName = $_GET['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_GET['order'][0]['dir']; // asc or desc
        $searchValue = $_GET['search']['value']; // Search value
        
        $columns = [
            0 => 'checkbox',
            1 => 'cname',
            2 => 'gname',
            3 => 'cname',
            4 => 'cname',
            5 => 'cname',
            6 => 'cname',

        ];
        $columnName = $columns[$columnIndex];

        ## Search 
        $searchQuery = " ";
        if($searchValue != ''){
           $searchQuery = " (
               celebrities.name like '%".$searchValue."%'
               or games.name like '%".$searchValue."%'
               ) ";
        }else{ 
          $searchQuery = 1;
        }
        
        
        
        ## Fetch records
        $data = [] ;
        $data[ 'request_status' ] = 0 ;

        $data['celebrity_id'] = $request->celeb_id ;
        $data['game_id'] = $request->game_id ;
        $data['platform_id'] = $request->pltfrm_id ;
        $data['filter_for_date'] = $request->slot_date ;
        $data['slot_time'] = $request->slot_time ;
        $data['booking_request_for'] = $request->booking_request_for ;

        $data['columnName'] = $columnName ;
        $data['columnSortOrder'] = $columnSortOrder ;
        $data['row'] = $row ;
        $data['rowperpage'] = $rowperpage ;
        $data['searchQuery'] = $searchQuery ;

        $lists =  $this->getUserGamesRecords( $data );

        ## Total number of records
        $totalRecords = $lists['totalRecords'];
        
        ## Total number of record with filtering
        $totalRecordwithFilter = $lists['totalRecordwithFilter'];
        $listsData = $lists['dataObj'];
        $checkbox = ""; $data = array(); $action = "";
        if(!empty($listsData)){
            $startIndex = 1 * $row;
            foreach($listsData as $k=>$list){

                $encryptId = Hashids::encode($list["gr_id"]);  
               
                $data[] = array( 
                        // $checkbox,
                        ++$startIndex,
                        '<div class="celebrity-details-wrap text-center"><span class="cimg-pic"><img src="'.  ( $list['image'] ? asset("public")."/".$list['image'] : asset("assets/frontend/img/celbs.png") ) .'" width="70px" class="img-fluid rounded"/> </span> <span class="cb-nme">' . $list['cname'] . '</span>',
                        $list['gname'] . ' ( ' . $platforms[ $list['requested_platform_id'] ] . ' )',
                        $list['slot_date'],
                        $list['slot_start_time'],
                        $list['slot_end_time'],
                        '$' . $list['slot_price'],
                        /*'<td class="action-btn"> <a class="btn btn-success float-right" href="javascript:;">requested</a>	</td>',*/
                        
                        // $action,
                        $encryptId
                    );
            }
        }
        ## Response
        $response = array(
          "draw" => intval($draw),
          "iTotalRecords" => $totalRecords,
          "iTotalDisplayRecords" => $totalRecordwithFilter,
          "aaData" => $data
        );
        echo json_encode($response);
        exit;
    }
}
