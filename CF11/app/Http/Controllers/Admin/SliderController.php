<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sliders;
use DB;
use Vinkla\Hashids\Facades\Hashids;
use File;
use App\Http\Controllers\Admin\CommonController;
use Response;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $redirectTo = '/login';
    public $folder_path = "uploads/slider";
    public function index()
    {
        $title = "Slider List";
        return view('admin.sliders.sliders', ['title' => $title, "breadcrumbItem" => "Manage Slider" , "breadcrumbTitle"=> "Slider List"]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Add Slide";
        return view('admin.sliders.add_slide', ['title' => $title, "breadcrumbItem" => "Manage Slider" , "breadcrumbTitle"=> "Add Slide" ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png'
        ];
        
        $messages = [
            'title.required' => 'The title is required.',
            'image.required' => 'The image is required.',
            'image.mimes' => 'Please select valid image format.'
        ];

        $validatedData = $this->validate($request, $rules, $messages 
        );

        $admin_id = session('user_id');

        $file_name_with_time_prefix = upload_file($request, 'image' , $this->folder_path );

        $Sliders = new Sliders();
        
        $Sliders->title = $request->title ;
        $Sliders->created_by = $admin_id ;
       

        if($file_name_with_time_prefix)
        $Sliders->image= $this->folder_path."/".$file_name_with_time_prefix;
        
        if( $Sliders->save() ){
            $Sliders->sort_order = $maxOrder = Sliders::max('sort_order') + 1;
            $Sliders->save();
            Sliders::ResetOrder();
            return redirect()->route('admin.sliders')->with(['success'=>'Slide has been saved successfully.']);
        }else{
            return redirect()->route('admin.addslide')->with(['error'=>'Error occured while saving slide.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Edit Slide";
        $decryptId = Hashids::decode($id);
        $details = Sliders::find($decryptId[0]);
        return view('admin.sliders.edit_slide', ['title' => $title, "details"=> $details, "breadcrumbItem" => "Manage Slider" , "breadcrumbTitle"=> "Edit Slide" ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        
        $Sliders = Sliders::find($request->slide_id);
        if( !$Sliders ) { 
            return redirect()->route('admin.editslide')->with(['error'=>'Unable to update please try again later.']);
        }

        $rules = [
            'title' => 'required',
            'image' => 'mimes:jpeg,jpg,png',
            // 'image' => 'required',
        ];
        
        $messages = [
            'title.required' => 'The title is required.',
            'image.mimes' => 'Please select valid image format.'
            // 'image.required' => 'The image is required.',
        ];
        if( !$Sliders->image ) {
            $rules[ 'image' ] = 'required|mimes:jpeg,jpg,png';
            $messages[ 'image.required' ] = 'The image is required.';
        }

        $validatedData = $this->validate($request, $rules, $messages 
        );
        
        $admin_id = session('user_id');

        $file_name_with_time_prefix = upload_file($request, 'image' , $this->folder_path );
        
        $Sliders = Sliders::find($request->slide_id);

        $Sliders->title = $request->title ;
        $Sliders->created_by = $admin_id ;
       
        if($file_name_with_time_prefix)
        $Sliders->image= $this->folder_path."/".$file_name_with_time_prefix;
        
        $Sliders->save();
    
        if($Sliders){
            Sliders::ResetOrder();
            return redirect()->route('admin.sliders')->with(['success'=>'Slide has been updated successfully.']);
        }else{
            return redirect()->route('admin.editslide')->with(['error'=>'Error occured while saving slide details.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ajaxDataLoad(Request $request){
        $draw = $_GET['draw'];
        $row = $_GET['start'];
        $rowperpage = $_GET['length']; // Rows display per page
        $columnIndex = $_GET['order'][0]['column']; // Column index
        $columnName = $_GET['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_GET['order'][0]['dir']; // asc or desc
        $searchValue = $_GET['search']['value']; // Search value
        
        $columns = [
            0 => 'checkbox',
            1 => 'sort_order',
            2 => 'title',
            3 => 'image',
            4 => 'status',
            5 => 'action',
            6 => 'id',

        ];
        $columnName = $columns[$columnIndex];

        ## Search 
        $searchQuery = " ";
        if($searchValue != ''){
           $searchQuery = " (
               title like '%".$searchValue."%'
               or image like '%".$searchValue."%'
               ) ";
        }else{ 
          $searchQuery = 1;
        }
        
        ## Total number of records
        $totalRecords = Sliders::count();
        
        ## Total number of record with filtering
        $totalRecordwithFilter = Sliders::whereRaw($searchQuery)->count();
        
        ## Fetch records
        $lists = Sliders::whereRaw($searchQuery)->orderBy($columnName, $columnSortOrder)->skip($row)->take($rowperpage)->get();

        $checkbox = ""; $data = array(); $action = "";
        if(!empty($lists)){
            foreach($lists as $list){

                $encryptId = Hashids::encode($list["id"]);  
                
                $checkbox = '<div class="animated-checkbox"><label style="margin-bottom:0px;"><input type="checkbox" name="user_ids[]" value="'.Hashids::encode($list['id']).'" /><span class="label-text"></span></label></div>';
                $action = '<a href="editslide/'.$encryptId.'"><i class="fa fa-pencil" aria-hidden="true"></i></a> &nbsp;&nbsp; <a href="javascript:void(0);" onclick=delete_row("'.$encryptId.'")><i class="fa fa-trash" aria-hidden="true"></i></a></i>';
                $data[] = array( 
                        $checkbox,
                        $list['sort_order'],
                        $list['title'],
                        '<span><img src="' . asset("public/".$list['image'] ) .'" alt="" /></span>',
                        $list['status'] =="1" ? "<span class='badge' style='background:green; color:#FFF; padding:5px;'>Active</span>" : "<span class='badge' style='background:#FF0000; color:#FFF; padding:5px;'>Inactive</span>",
                        // date("d M, Y", strtotime($list['created_at'])),
                        $action,
                        $encryptId
                    );
            }
        }
        ## Response
        $response = array(
          "draw" => intval($draw),
          "iTotalRecords" => $totalRecords,
          "iTotalDisplayRecords" => $totalRecordwithFilter,
          "aaData" => $data
        );
        echo json_encode($response);
        exit;
    }

    public function deleteImage ( $slideId ) { 
        if( !$slideId ){
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        $slideId = Hashids::decode($slideId);
        if( !$slideId ){
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        $slideId = $slideId[0];

        $obj = Sliders::find( $slideId );
        if( $obj->count() <= 0 ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Selected slide not found'
            ]);
        }

        if( !$obj->image ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Selected slide image not found'
            ]);
        }

        $isDeleted = File::delete(  'public/' . $obj->image );
        // File::delete($destinationPath.'/your_file');
        if( !$isDeleted ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Unable to delete image, please try again later!'
            ]);
        }

        $obj->image = '';
        
        if( !$obj->save( ) ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Unable to delete image, please try again later!'
            ]);
        }
        
        return response()->json([
            'status' => 1,
            'msg' => 'Slide image deleted!.'
        ]);

    }

    public function updateSlideImageReorder(Request $request)
    {
        $request_data = $request->all();
        
        $validator = $request->validate([
                'replace_ids' => 'required'
            ]);

        if( count( $request_data['replace_ids'] ) <= 1 ) { 
            return response::json([ 'errors' => 'Unable to update order, please try again later.' ]);
        }

        $positions = $ids = [];
        foreach( array_keys( $request_data['replace_ids'] ) as $id ) {
            $ids[] = Hashids::decode( ( string )$id )[0];
            $positions[ Hashids::decode( ( string )$id )[0] ] = $request_data['replace_ids'][ $id ];
        }

        $items = Sliders::whereIn( 'id', $ids )->get();
        if( $items->count() !=  count( $request_data['replace_ids'] ) ) { 
            return response::json([ 'errors' => 'Unable to update order, please try again later.' ]);
        }

        foreach( $items as $item ){ 
            $item->sort_order = $positions[ $item->id ];
            $item->save();
        }
        Sliders::ResetOrder();
        return response::json([ 'success' => 'Order updated successfully' ]);

    }

}
