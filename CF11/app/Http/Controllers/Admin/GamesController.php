<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Models\Celebrities;
use App\Models\Countries;
use App\Models\States;
use App\Models\Admin;
use App\Models\Games;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use Config;
use Mail;
use Hash;
use App\Http\Traits\CommonMethods;
use App\Models\Emails;
use Vinkla\Hashids\Facades\Hashids;
use File;
use Response;


class GamesController extends Controller
{

    use CommonMethods;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/login';
    public $folder_path = "uploads/games";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Game List";
        return view('admin.games.games', ['title' => $title, "breadcrumbItem" => "Manage Games" , "breadcrumbTitle"=> "Game List"]);
    }

    public function add(){
        $platforms = Config::get('constants.platforms');
        $title = "Add Game";
        return view('admin.games.add_games', ['title' => $title, "breadcrumbItem" => "Manage Games" , "breadcrumbTitle"=> "Add Game",'platforms'=>$platforms ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
            'game_time' => 'required|numeric',
        ]);
        $eventmanager = session('user_id');
        $file_name_with_time_prefix = upload_file($request, 'image' , $this->folder_path );

        $games = new Games();
        $type = '';
        if(is_array($request->type)){
            $type = implode(',' , $request->type);
        }

        $games->type = $type ;
        $games->name = $request->name ;
        $games->game_time = $request->game_time ;
        $games->creater_id = $eventmanager ;
       

        if($file_name_with_time_prefix)
        $games->image= $this->folder_path."/".$file_name_with_time_prefix;

        /*$games_details = array(
          "type" => $type,
          "name" => $request->name,
          "game_time" => $request->game_time,
          "creater_id" => $eventmanager,
        );*/
        
        // echo $type;
        // print_r($games_details); die;
        // $games = Games::create($games_details);
        
        if( $games->save() ){
            return redirect()->route('admin.games')->with(['success'=>'Game has been saved successfully.']);
        }else{
            return redirect()->route('admin.addgame')->with(['error'=>'Error occured while saving game.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editGame($id)
    {
        $platforms = Config::get('constants.platforms');
        $title = "Edit Game";
        $decryptUserId = Hashids::decode($id);
        $user_detail = Games::find($decryptUserId[0]);
        return view('admin.games.edit_games', ['title' => $title, "game"=> $user_detail, "breadcrumbItem" => "Manage Games" , "breadcrumbTitle"=> "Edit Game", 'platforms'=>$platforms]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateGame(Request $request){

    $validatedData = $this->validate($request, [
        'name' => 'required',
        'type' => 'required',
        'game_time' => 'required|numeric',
    ]);
    // print_r($request); die;
    $file_name_with_time_prefix = upload_file($request, 'image' , $this->folder_path );
    $game = Games::find($request->game_id);

    $type = '';
    if(is_array($request->type)){
        $type = implode(',' , $request->type);
    }

    $game->name = $request->name;
    $game->type = $type;
    $game->game_time = $request->game_time;

    $game->updated_at = Carbon::now();

    if($file_name_with_time_prefix)
        $game->image= $this->folder_path."/".$file_name_with_time_prefix;

    if( $game->save() ){
        return redirect()->route('admin.games')->with(['success'=>'Game has been updated successfully.']);
    }else{
        return redirect()->route('admin.editgame')->with(['error'=>'Error occured while saving game details.']);
    }

  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function ajaxDataLoad(Request $request){
        $draw = $_GET['draw'];
        $row = $_GET['start'];
        $rowperpage = $_GET['length']; // Rows display per page
        $columnIndex = $_GET['order'][0]['column']; // Column index
        $columnName = $_GET['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_GET['order'][0]['dir']; // asc or desc
        $searchValue = $_GET['search']['value']; // Search value
        
        $columns = [
            0 => 'id',
            1 => 'name',
            2 => 'game_time',
            3 => 'status',
            4 => 'created_at',

        ];
        $columnName = $columns[$columnIndex];

        ## Search 
        $searchQuery = " ";
        if($searchValue != ''){
           $searchQuery = " (name like '%".$searchValue."%') ";
        }else{ 
          $searchQuery = 1;
        }
        
        ## Total number of records
        $totalRecords = Games::count();
        
        ## Total number of record with filtering
        $totalRecordwithFilter = Games::whereRaw($searchQuery)->count();
        
        ## Fetch records
        $gamelist = Games::whereRaw($searchQuery)->orderBy($columnName, $columnSortOrder)->skip($row)->take($rowperpage)->get();

        $checkbox = ""; $data = array(); $action = "";
        if(!empty($gamelist)){
            foreach($gamelist as $games){

                $encryptId = Hashids::encode($games["id"]);  
                
                $checkbox = '<div class="animated-checkbox"><label style="margin-bottom:0px;"><input type="checkbox" name="user_ids[]" value="'.Hashids::encode($games['id']).'" /><span class="label-text"></span></label></div>';
                $action = '<a href="editgame/'.$encryptId.'"><i class="fa fa-pencil" aria-hidden="true"></i></a> &nbsp;&nbsp; <a href="javascript:void(0);" onclick=delete_row("'.$encryptId.'")><i class="fa fa-trash" aria-hidden="true"></i></a></i>';
                $data[] = array( 
                $checkbox,
                $games['name'],
                // $games['type'],
                $games['game_time'] . ' Mins',
                $games['status'] =="1" ? "<span class='badge' style='background:green; color:#FFF; padding:5px;'>Active</span>" : "<span class='badge' style='background:#FF0000; color:#FFF; padding:5px;'>Inactive</span>",
                date("d M, Y", strtotime($games['created_at'])),
                $action
               );
            }
        }
        ## Response
        $response = array(
          "draw" => intval($draw),
          "iTotalRecords" => $totalRecords,
          "iTotalDisplayRecords" => $totalRecordwithFilter,
          "aaData" => $data
        );
        echo json_encode($response);
        exit;
    }

    public function deleteImage ( $gameId ) { 
        if( !$gameId ){
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        $gameId = Hashids::decode($gameId);
        if( !$gameId ){
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        $gameId = $gameId[0];

        $obj = Games::find( $gameId );
        if( $obj->count() <= 0 ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Selected game not found'
            ]);
        }

        if( !$obj->image ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Selected game image not found'
            ]);
        }

        $isDeleted = File::delete(  'public/' . $obj->image );
        // File::delete($destinationPath.'/your_file');
        if( !$isDeleted ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Unable to delete image, please try again later!'
            ]);
        }

        $obj->image = '';
        
        if( !$obj->save( ) ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Unable to delete image, please try again later!'
            ]);
        }
        
        return response()->json([
            'status' => 1,
            'msg' => 'Game image deleted!.'
        ]);

    }
}
