<?php

namespace App\Http\Controllers\Admin;


use Auth;
use App\Http\Controllers\Controller;
use App\Models\Celebrities;
use App\Models\User;
use App\Models\Admin;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Middleware\RedirectIfAuthenticated;
use Session;
use Illuminate\Support\Facades\Redirect;
use Mail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Input;
use Config;
use xmlapi;
use Hash;
use Carbon\Carbon;
use App\Http\Controllers\Admin\CommonController;
use Illuminate\Support\Facades\URL;
use Vinkla\Hashids\Facades\Hashids;

class CelebrityController extends Controller{

  public function __construct() {
    $this->middleware('guest');
  }

  /* get userlisting  */
  public function celebrityListing(Request $request){
    $title = "Celebrity Management";
    return view('admin.celebrity.celebrities', ['title' => $title, "breadcrumbItem" => "Manage Celebrities" , "breadcrumbTitle"=>"Celebrity List"]);
  }

  public function addCelebrity(){
    $title = "Add Celebrity";
    return view('admin.celebrity.add_celebrity', ['title' => $title, "breadcrumbItem" => "Manage Celebrities" , "breadcrumbTitle"=> "Add Celebrity"]);
  }


  public function saveCelebrity(Request $request){

    $validatedData = $this->validate($request, [
        'fullname' => 'required',
        'email' => 'required|email',
        'password' => 'required|min:6',
        'gender' => 'required'
    ]);

    $user_details = array(
      "name" => $request->fullname,
      "email" => $request->email,
      "password" => Hash::make($request->password),
      "gender" => $request->gender,
      "email_verified_at" => Carbon::now(),
      "role_id" => 2
    );    
    $user = Celebrities::create($user_details); 
    if($user){
      return redirect()->route('admin.celebrityListing')->with(['success'=>'Celebrity detail has been added successfully.']);
    }else{

    }

  }

  public function editCelebrity($id){
    $title = "Edit Celebrity";
    $decryptUserId = Hashids::decode($id);
    $user_detail = Celebrities::find($decryptUserId[0]);
    return view('admin.celebrity.edit_celebrity', ['title' => $title, "user"=> $user_detail, "breadcrumbItem" => "Manage Celebrity" , "breadcrumbTitle"=> "Edit Celebrity"]);
  }


  public function updateCelebrity(Request $request){

    $validatedData = $this->validate($request, [
        'name' => 'required',
        'surname' => 'required',
        'username' => 'required|unique:celebrities,username,' . $request->userid,
        'gamer_id' => 'required',
        'mobile' => 'required',
        'skype' => 'required',
        'twitter' => 'required',
        'email' => 'required|unique:celebrities,email,' . $request->userid,
        'password' => 'nullable|min:6',
    ]);

    $user = Celebrities::find($request->userid);

    $user->name = $request->name;
    $user->surname = $request->surname;
    $user->username = $request->username;
    $user->gamer_id = $request->gamer_id;
    $user->email = $request->email;
    $user->mobile = $request->mobile;
    $user->skype = $request->skype;
    $user->twitter = $request->twitter;
    if(!empty($request->password)){
      $user->password = Hash::make($request->password);
    }
    $user->updated_at = Carbon::now();
    $user->save();

    if($user){
      return redirect()->route('admin.celebrityListing')->with(['success'=>'Celebrity detail has been updated successfully.']);
    }else{

    }
  }


  public function ajaxDataLoad(Request $request){
    $draw = $_GET['draw'];
    $row = $_GET['start'];
    $rowperpage = $_GET['length']; // Rows display per page
    $columnIndex = $_GET['order'][0]['column']; // Column index
    $columnName = $_GET['columns'][$columnIndex]['data']; // Column name
    $columnSortOrder = $_GET['order'][0]['dir']; // asc or desc
    $searchValue = $_GET['search']['value']; // Search value
    
    $columns = [
        0 => 'id',
        1 => 'name',
        2 => 'email',
        3 => 'status',
        4 => 'created_at',
        5 => 'email'
    ];
    $columnName = $columns[$columnIndex];

    ## Search 
    $searchQuery = " ";
    if($searchValue != ''){
       $searchQuery = " (
                name like '%".$searchValue."%' 
                or surname like '%".$searchValue."%' 
                or email like '%".$searchValue."%'                 
                or username like '%".$searchValue."%' 
                or mobile like '%".$searchValue."%' 
                or skype like '%".$searchValue."%' 
                or twitter like '%".$searchValue."%' 
                or gamer_id like '%".$searchValue."%' 
              ) ";
    }else{ 
      $searchQuery = 1;
    }
    
    ## Total number of records
    $totalRecords = Celebrities::count();
    
    ## Total number of record with filtering
    $totalRecordwithFilter = Celebrities::whereRaw($searchQuery)->count();
    
    ## Fetch records
    $userlist = Celebrities::whereRaw($searchQuery)->orderBy($columnName, $columnSortOrder)->skip($row)->take($rowperpage)->get();

    $checkbox = ""; $data = array(); $action = "";
    if(!empty($userlist)){
        foreach($userlist as $users){
        $encryptId = Hashids::encode($users["id"]);  
        
        $checkbox = '<div class="animated-checkbox check_ids"><label style="margin-bottom:0px;"><input type="checkbox" name="user_ids[]" value="'.Hashids::encode($users['id']).'" /><span class="label-text"></span></label></div>';
        $action = '<a href="javascript:void(0);" onclick=delete_row("'.$encryptId.'")><i class="fa fa-trash" aria-hidden="true"></i></a></i>';
        // $action = '<a href="editcelebrity/'.$encryptId.'"><i class="fa fa-pencil" aria-hidden="true"></i></a> &nbsp;&nbsp; <a href="javascript:void(0);" onclick=delete_row("'.$encryptId.'")><i class="fa fa-trash" aria-hidden="true"></i></a></i>';
           $data[] = array( 
              $checkbox,
              (( $users['name']) ? "<b>Full Name:</b> " . $users['name'].' '.$users['surname'].'<br/>' : '' ).
              (( $users['email']) ? "<b>Email:</b> " . $users['email'].'<br/>' : '' ).
              (($users['username']) ? "<b>Username:</b> " . $users['username'].'<br/>' : '') .
              (($users['mobile']) ? "<b>Mobile Number:</b> " . $users['mobile'] : '' ),
              
              (( $users['skype']) ? "<b>Skype:</b> " . $users['skype'].'<br/>' : '' ).
              (($users['twitter']) ? "<b>Twitter:</b> " . $users['twitter'].'<br/>' : '') .
              (($users['gamer_id']) ? "<b>Gamer Id:</b> " . $users['gamer_id'] : '' ),

              $users['status'] == "1" ? "<span class='badge' style='background:green; color:#FFF; padding:5px;'>Active</span>" : ( ($users['status'] == "0" ) ? "<span class='badge' style='background:#FF0000; color:#FFF; padding:5px;'>Inactive</span>" : "<span class='badge' style='background:#FF0000; color:#FFF; padding:5px;'>Email Not Verified</span>" ),
              date("d M, Y", strtotime($users['created_at'])),
              $action
           );
        }
    }
    ## Response
    $response = array(
      "draw" => intval($draw),
      "iTotalRecords" => $totalRecords,
      "iTotalDisplayRecords" => $totalRecordwithFilter,
      "aaData" => $data
    );
    echo json_encode($response);
    exit;
  }
    


}
