<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Games;
use App\Models\Admin;
use App\Models\Celebrities;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Middleware\RedirectIfAuthenticated;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Input;
use Config;
use xmlapi;


class DashboardController extends Controller{

  public function __construct() {
    $this->middleware('guest');
  }

  
  /* get userlisting  */
  public function dashboard(Request $request){
  	
    $title = "Admin Dashboard";
    $user_count = User::count();
    $celebrity_count = Celebrities::count();
    $games_count = Games::count();
    return view('admin.dashboard.dashboard', ['title' => $title, 'user_count' => $user_count,'celebrity_count' => $celebrity_count, 'games' => $games_count, "breadcrumbItem"=>"Dashboard" , "breadcrumbTitle"=>"Admin Dashboard"]);
  }

}
