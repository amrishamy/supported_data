<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Traits\CommonMethods;

use App\Models\Sliders;
use App\Models\Games;
use App\Models\Slots;
use App\Models\Celebrities;

use Vinkla\Hashids\Facades\Hashids;

use Config;
use DB;

class CelebritiesCalendarController extends Controller
{
    use CommonMethods;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = "Celebrity calendar";

        $celebList = $this->get_celebrities_list() ;
        $gameList = $this->get_games_list() ;
        $platforms = Config::get('constants.platforms');

        return view( 'frontend.celebritiescalendar', ['page'=>$page, 'platforms'=>$platforms, 'celebList'=>$celebList,'gamesList'=>$gameList, 'calObj' => $this->cal( ), 'postedData' => [ 'date' => '' ] ] );
    }

    public function getCelebrityCalendarDetail( Request $request ) {

        $platforms = Config::get('constants.platforms');

        $draw = $_GET['draw'];
        
        $row = $_GET['start'];
        $rowperpage = $_GET['length']; // Rows display per page

        $columnIndex = $_GET['order'][0]['column']; // Column index
        $columnName = $_GET['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_GET['order'][0]['dir']; // asc or desc
        $searchValue = $_GET['search']['value']; // Search value
        
        $columns = [
            0 => 'checkbox',
            1 => 'cname',
            2 => 'gname',
            3 => 'cname',
            4 => 'cname',
            5 => 'cname',
            6 => 'cname',

        ];
        $columnName = $columns[$columnIndex];

        ## Search 
        $searchQuery = " ";
        if($searchValue != ''){
           $searchQuery = " (
               celebrities.name like '%".$searchValue."%'
               or games.name like '%".$searchValue."%'
               ) ";
        }else{ 
          $searchQuery = 1;
        }
        
        
        
        ## Fetch records
        $data = [] ;
        $data[ 'request_status' ] = 0 ;

        $data['celebrity_id'] = $request->c_id ;
        $data['game_id'] = $request->g_id ;
        $data['platform_id'] = $request->pltfrm_id ;
        $data['filter_for_date'] = $request->slot_date ;
        $data['slot_time'] = $request->slot_time ;

        $data['columnName'] = $columnName ;
        $data['columnSortOrder'] = $columnSortOrder ;
        $data['row'] = $row ;
        $data['rowperpage'] = $rowperpage ;
        $data['searchQuery'] = $searchQuery ;

        $lists =  $this->getCelebrityGamesRecords( $data );

        ## Total number of records
        $totalRecords = $lists['totalRecords'];
        
        ## Total number of record with filtering
        $totalRecordwithFilter = $lists['totalRecordwithFilter'];
        $listsData = $lists['dataObj'];
        $checkbox = ""; $data = array(); $action = "";
        if(!empty($listsData)){
            $startIndex = 1 * $row;
            foreach($listsData as $k=>$list){

                $encryptId = 0; //Hashids::encode($list["gr_id"]);
                
                $action = '';
                if( $encryptId && $list->requested_slot_id ) {  
                    if($list->gsr_status == 0 ){
                        $action .= '<a href="javascript:void(0);" class="btn btn-red float-right">Requested</a>';
                    }elseif($list->gsr_status == 1 ){
                        $action .= '<a href="javascript:void(0);" class="btn btn-red float-right">Booked</a>';
                    }else{
                        $action .= '<a href="javascript:void(0);" class="btn btn-red float-right">Denied</a>';
                    }
                } else { 
                    $action .= '<a href="' . route('user.check_user_login') .'" data-toggle="modal" data-target="" class="btn btn-red float-right request_now_btn" slot_id="' . $list->id . '">Request Now</a>';
                }
                
                $data[] = array( 
                        // $checkbox,
                        ++$startIndex,
                        '<div class="celebrity-details-wrap text-center"><span class="cimg-pic"><img src="'.  ( $list['image'] ? asset("public")."/".$list['image'] : asset("assets/frontend/img/celbs.png") ) .'" width="70px" class="img-fluid rounded"/> </span> <span class="cb-nme">' . $list['cname'] . '</span>',
                        $list['gname'],
                        $list['slot_date'],
                        $list['slot_start_time'],
                        '$' . $list['slot_price'],
                        /*'<td class="action-btn"> <a class="btn btn-success float-right" href="javascript:;">requested</a>	</td>',*/
                        
                        // $action,
                        $action,
                        $encryptId
                    );
            }
        }
        ## Response
        $response = array(
          "draw" => intval($draw),
          "iTotalRecords" => $totalRecords,
          "iTotalDisplayRecords" => $totalRecordwithFilter,
          "aaData" => $data
        );
        echo json_encode($response);
        exit;
    }
   
}
