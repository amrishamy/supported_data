<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (!Auth::guard($guard)->check()) {
            // if($guard=="celebrity"){
            //     return redirect()->route('celebrity.login');
            // }else{
            //     return redirect()->route('admin.login');
            // }

            switch($guard){

                case 'admin':
                    return redirect()->route('admin.login');
                break;

                case 'celebrity':
                    return redirect()->route('celebrity.login');
                break;

                case 'web':
                    return redirect()->route('homepage');
                break;

                default:
                    return redirect('/');
                break;
            }
        }

        return $next($request);
    }
}