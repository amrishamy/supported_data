<?php
namespace App\Http\Traits;

use Vinkla\Hashids\Facades\Hashids;

use App\Models\User;
use App\Models\GameSettings;
use App\Models\States;
use Mail;
use Carbon\Carbon;
use DateTimeZone;
use DateTime;

use App\Models\Games;
use App\Models\Slots;
use App\Models\Celebrities;
use App\Models\SlotRequest;
use DB;

//use App\Brand;

trait CommonMethods { 

    public function defaultTimeZone (){
        return 'Asia/Kolkata';
    }

    public function getUser(){
        return $this->user;
    }

    public function getServerTime($eventId=NULL) {

        $timezone = Event::getTimezoneAgainstEventId($eventId)->timezone;
        $timestamp = Carbon::parse(date("Y-m-d H:i:s"))->timezone($timezone)->toDateTimeString();        
        $timestamp = strtotime($timestamp);
        return $timestamp;
    }

    public function getUTCTime(){
        $timestamp = Carbon::parse(date("Y-m-d H:i:s"))->timezone('UTC')->toDateTimeString();
        
        $timestamp = strtotime($timestamp);
        return $timestamp;
    }


    public function encodeId($id=NULL) {
        // get the server time and convert it into timestamp.
        $encId = Hashids::encode($id);
        return $encId;
    }

    public function decodeId($encodedId=NULL) {
        // get the server time and convert it into timestamp.
        $decodeId = Hashids::decode($encodedId);
        return $decodeId[0];
    }

    public function getTimeDifference($userJoinTime=NULL, $eventStartTime=NULL){

        // Formulate the Difference between two dates 
		$diff = abs($eventStartTime - $userJoinTime);  
		  
		  
		// To get the year divide the resultant date into 
		// total seconds in a year (365*60*60*24) 
		$years = floor($diff / (365*60*60*24));  
		  
		  
		// To get the month, subtract it with years and 
		// divide the resultant date into 
		// total seconds in a month (30*60*60*24) 
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));  
		  
		  
		// To get the day, subtract it with years and  
		// months and divide the resultant date into 
		// total seconds in a days (60*60*24) 
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24)); 
		  
		  
		// To get the hour, subtract it with years,  
		// months & seconds and divide the resultant 
		// date into total seconds in a hours (60*60) 
		$hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));  
		  
		  
		// To get the minutes, subtract it with years, 
		// months, seconds and hours and divide the  
		// resultant date into total seconds i.e. 60 
		$minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);  
		  
		  
		// To get the minutes, subtract it with years, 
		// months, seconds, hours and minutes  
		$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));  
		  
		// Print the result 
		//printf("%d years, %d months, %d days, %d hours, ". "%d minutes, %d seconds", $years, $months, $days, $hours, $minutes, $seconds);

        $minutesonly = $days * 24 * 60;
        $minutesonly += $hours * 60;

        $secondsOnly = $minutesonly;
        $secondsOnly += $minutes * 60;
        $secondsOnly += $seconds;

        $minutesonly += $minutes;

		$timeArr = array("minutesonly"=> $minutesonly, "minutes"=>$minutes , "seconds" =>$seconds, "secondsonly" => $secondsOnly);
		return $timeArr;

    }

    // common function to send email
    public function sendEmail($dataArry=NULL){
        $to = $dataArry['to']; $subject = $dataArry['subject'];$emailBody = $dataArry['content'];
        if(isset($dataArry['file']) && isset($dataArry['mime'])){
            $file = $dataArry['file'];
            $mime = $dataArry['mime'];
        }else{
            $file = "";
            $mime = "";
        }
        Mail::send([], [], function($message) use($to, $subject, $emailBody, $file, $mime) {
          $message->setBody($emailBody, 'text/html');
          $message->from(\Config::get('constants.from_email'), \Config::get('constants.from_name'));
          $message->to($to);
          $message->subject($subject);
          if($file){
            $message->attach($file, array('mime' => $mime));
          }
        });

    }

    public function getTimeFromTimestamp($timestamp=NULL){
        return date("H:i", $timestamp);
    }

    protected function get_celebrities_list( ) { 
        $celebrities = Celebrities::select( [ 'id', 'name' ] )->where( 'status', 1 )->pluck( 'name', 'id' );
        return $celebrities;
    }

    protected function get_games_list( ) { 
        $celebrities = Games::select( [ 'id', 'name' ] )->where( 'status', 1 )->pluck( 'name', 'id' );
        return $celebrities;
    }

    protected function cal( $needCalFor = '' ){
        if( !$needCalFor ) $needCalFor = date( 'Y-m-d' );
        
        $calYear = date( 'Y', strtotime( $needCalFor ) );
        $calMonth = date( 'F', strtotime( $needCalFor ) );
        $calNumericMonth = date( 'm', strtotime( $needCalFor ) );
        
        $calDay = ( strtotime( date( 'Y-m', strtotime( $needCalFor ) ) ) == strtotime( date( 'Y-m' ) ) )?date( 'd' ):false;

        $calOverallDays = cal_days_in_month( CAL_GREGORIAN, $calNumericMonth, $calYear );
        $firstDayName = date( 'D', mktime( 0, 0, 0, $calNumericMonth, 1, $calYear ) );

        $prevCal = date( 'Y-m', strtotime( $calYear . '-' . $calNumericMonth . ' - 1 Months' ) );
        $prevCal = ( strtotime( $prevCal ) < strtotime( date( 'Y-m' ) ) )?'':$prevCal;
        $nextCal = date( 'Y-m', strtotime( $calYear . '-' . $calNumericMonth . ' + 1 Months' ) );
        
        return [ 'year' => $calYear, 'month' => $calMonth, 'numeric_month' => $calNumericMonth, 'current_date' => $calDay, 'overall_days' => $calOverallDays, 'first_day' => $firstDayName, 'previous_cal' => $prevCal, 'next_cal' => $nextCal, 'week_days' => [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ] ];
    }

    // get User games status wise like requested, accepted games
    protected function getUserGamesRecords( $attrs = [] )
    {
        extract( $attrs );

        $listFor = isset($booking_request_for) ? $booking_request_for : 1 ;
        
        if( isset($filter_for_date) && $filter_for_date ){
            $filter_for_date = date( 'Y-m-d', strtotime( $filter_for_date ) );
        }
        // print_r($filter_for_date);die;
        //DB::enableQueryLog();
        $dataObj = Slots::join('celebrities', 'slots.celebrity_id', '=', 'celebrities.id')
                ->join('games', 'slots.game_id', '=', 'games.id')
                ->select('celebrities.name as cname','celebrities.image', 'games.name as gname', 'slots.id', 'slots.slot_date','slots.slot_start_time','slots.slot_end_time','slots.slot_price', 'game_slot_request.id as gr_id', 'game_slot_request.requested_slot_id', 'game_slot_request.status as gsr_status','game_slot_request.requested_platform_id')
                ->join('game_slot_request', function( $customJoinQuery ) use ( $listFor ){
                    $customJoinQuery->on( 'slots.id', '=', 'game_slot_request.requested_slot_id' );
                    if( in_array( $listFor, [ 2, 3 ] ) ) { 
                        $customJoinQuery->where( 'game_slot_request.status', '=', 1 );
                    } else { 
                        $customJoinQuery->where( 'game_slot_request.status', '=', 0 );
                    }
                    $customJoinQuery->where( 'game_slot_request.user_id', '=', session( 'manager_id' ) );
                });

        if( $listFor == 2 || $listFor == 1 ) { 
            $filterDateTime = date( 'Y-m-d H:i:s' );

            $dataObj->where(function($query) use ( $filterDateTime ){
                $query->where( \DB::raw( 'CONCAT( slots.slot_date, \' \', slots.slot_start_time )' ), '>=', $filterDateTime );
            });
        }

        if( $listFor == 3 ) { 
            $filterDateTime = date( 'Y-m-d H:i:s' );

            $dataObj->where(function($query) use ( $filterDateTime ){
                $query->where( \DB::raw( 'CONCAT( slots.slot_date, \' \', slots.slot_start_time )' ), '<', $filterDateTime );
            });
        }

        $totalRecords =  $dataObj->count() ;
        if( isset( $return_count ) && $return_count === true ) { 
            return $totalRecords;
        }

        if( isset( $filter_for_date ) && $filter_for_date && isset ( $slot_time ) && $slot_time ) { 
            $filterDateTime = date( 'Y-m-d H:i:s', strtotime( $filter_for_date . ' ' . $slot_time ) );
            $dataObj->where(function($query) use ( $filterDateTime ){
                $query->where( \DB::raw( 'CONCAT( slots.slot_date, \' \', slots.slot_start_time )' ), '<=', $filterDateTime );
                $query->where( \DB::raw( 'CONCAT( slots.slot_date, \' \', slots.slot_end_time )' ), '>', $filterDateTime );
                $query->orWhere( \DB::raw( 'CONCAT( slots.slot_date, \' \', slots.slot_start_time )' ), '>=', $filterDateTime );
            });
        }elseif( isset($filter_for_date) && $filter_for_date ){
            $dataObj->where( 'slots.slot_date', $filter_for_date );
        }elseif( isset($slot_time) && $slot_time ) {
            $slot_time = date( 'H:i', strtotime( $slot_time ) );
            $dataObj->where(function($query) use ( $slot_time ){
                $query->where( 'slots.slot_start_time','<=', $slot_time );
                $query->where( 'slots.slot_end_time','>', $slot_time );
                $query->orWhere( 'slots.slot_start_time','>=', $slot_time );
            });
        }

        if( isset( $celebrity_id ) && intval( $celebrity_id ) > 0 ) { 
            $dataObj->where( 'celebrities.id', intval( $celebrity_id ) );
        } 
        if( isset( $game_id ) && intval( $game_id ) > 0 ) { 
            $dataObj->where( 'games.id', intval( $game_id ) );
        }
        if( isset( $platform_id ) && intval ($platform_id) > 0 ) { 
            $dataObj->where( 'game_slot_request.requested_platform_id', $platform_id );
        }

        $totalRecordwithFilter =  $dataObj->whereRaw($searchQuery)->count() ; 
        /*$dataObj = $dataObj->orderBy($columnName, $columnSortOrder)->skip($row)->take($rowperpage)->get();*/
        $dataObj = $dataObj->orderBy( DB::raw( 'CONCAT( slots.slot_date, \' \', slots.slot_start_time )' ), 'ASC')->skip($row)->take($rowperpage)->get();
        // die('comes here');
        //dd(DB::getQueryLog());

        return [ 'totalRecords' => $totalRecords, 'totalRecordwithFilter' => $totalRecordwithFilter, 'dataObj' => $dataObj ];
    }

    // get User games status wise like requested, accepted games
    protected function getCelebrityGamesRecords( $attrs = [] )
    {
        extract( $attrs );

        if( isset($filter_for_date) && $filter_for_date ){
            $filter_for_date = date( 'Y-m-d', strtotime( $filter_for_date ) );
        }
        // print_r($filter_for_date);die;
        //DB::enableQueryLog();
        $dataObj = Slots::join('celebrities', 'slots.celebrity_id', '=', 'celebrities.id')
                ->join('games', 'slots.game_id', '=', 'games.id');
        
        if( session( 'manager_id' ) ) { 
            $dataObj->select('celebrities.name as cname','celebrities.image', 'games.name as gname', 'slots.id', 'slots.slot_date','slots.slot_start_time','slots.slot_end_time','slots.slot_price', 'game_slot_request.id as gr_id', 'game_slot_request.requested_slot_id', 'game_slot_request.status as gsr_status','game_slot_request.requested_platform_id');
            $dataObj->leftJoin('game_slot_request', function( $customJoinQuery ){
                $customJoinQuery->on( 'slots.id', '=', 'game_slot_request.requested_slot_id' );
                $customJoinQuery->where( 'game_slot_request.user_id', '=', session( 'manager_id' ) );
            });
        } else { 
            $dataObj->select('celebrities.name as cname','celebrities.image', 'games.name as gname', 'slots.id', 'slots.slot_date','slots.slot_start_time','slots.slot_end_time','slots.slot_price' );
        }
        
        $currentDateTime = date( 'Y-m-d H:i:s' );

        $dataObj->where(function($query) use ( $currentDateTime ){
            $query->where( \DB::raw( 'CONCAT( slots.slot_date, \' \', slots.slot_start_time )' ), '>=', $currentDateTime );
        });

        $totalRecords =  $dataObj->count() ;

        if( isset( $filter_for_date ) && $filter_for_date && isset ( $slot_time ) && $slot_time ) { 
            $filterDateTime = date( 'Y-m-d H:i:s', strtotime( $filter_for_date . ' ' . $slot_time ) );
            $dataObj->where(function($query) use ( $filterDateTime ){
                $query->where( \DB::raw( 'CONCAT( slots.slot_date, \' \', slots.slot_start_time )' ), '<=', $filterDateTime );
                $query->where( \DB::raw( 'CONCAT( slots.slot_date, \' \', slots.slot_end_time )' ), '>', $filterDateTime );
                $query->orWhere( \DB::raw( 'CONCAT( slots.slot_date, \' \', slots.slot_start_time )' ), '>=', $filterDateTime );
            });
        }elseif( isset($filter_for_date) && $filter_for_date ){
            $dataObj->where( 'slots.slot_date', $filter_for_date );
        }elseif( isset($slot_time) && $slot_time ) {
            $slot_time = date( 'H:i', strtotime( $slot_time ) );
            $dataObj->where(function($query) use ( $slot_time ){
                $query->where( 'slots.slot_start_time','<=', $slot_time );
                $query->where( 'slots.slot_end_time','>', $slot_time );
                $query->orWhere( 'slots.slot_start_time','>=', $slot_time );
            });
        }

        if( isset( $celebrity_id ) && intval( $celebrity_id ) > 0 ) { 
            $dataObj->where( 'celebrities.id', intval( $celebrity_id ) );
        } 
        if( isset( $game_id ) && intval( $game_id ) > 0 ) { 
            $dataObj->where( 'games.id', intval( $game_id ) );
        }

        /*if( isset( $platform_id ) && intval ($platform_id) > 0 ) { 
            $dataObj->where( 'game_slot_request.requested_platform_id', $platform_id );
        }*/

        $totalRecordwithFilter =  isset( $searchQuery )?$dataObj->whereRaw($searchQuery)->count():0; 
        /*$dataObj = $dataObj->orderBy($columnName, $columnSortOrder)->skip($row)->take($rowperpage)->get();*/
        $dataObj = $dataObj->orderBy( DB::raw( 'CONCAT( slots.slot_date, \' \', slots.slot_start_time )' ), 'ASC')->skip($row)->take($rowperpage)->get();
        // die('comes here');
        //dd(DB::getQueryLog());

        if( isset( $get_obj ) && $get_obj === true ) { 
            return $dataObj;
        }

        return [ 'totalRecords' => $totalRecords, 'totalRecordwithFilter' => $totalRecordwithFilter, 'dataObj' => $dataObj ];
    }

}