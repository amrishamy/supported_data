@extends('celebrity.layouts.admin')
@section('content')
	
	<main class="app-content">
      @include('celebrity.includes.adminbreadcrumb')
      
      <div class="row">
        <div class="col-md-12">
          <form name="profile_frm" enctype="multipart/form-data" id="profile_update_frm" method="post" action="{{ route('celebrity.profileupdate') }}">
           {{ csrf_field() }}
          

          @if ($message = Session::get('success'))
          <div class="alert alert-success alert-block" style="margin-bottom: 10px;">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <span>{{ $message }}</span>
          </div>
          @endif
          @if ($message = Session::get('error'))
          <div class="alert alert-danger alert-block" style="margin-bottom: 10px;">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <span>{{ $message }}</span>
          </div>
          @endif
          <div class="tile">
            <!-- <h3 class="tile-title">Vertical Form</h3> -->
            <div class="tile-body">
                <h3 class="tile-title">Profile Information</h3>

               <!--  <div class="form-group">
                  <label class="control-label">Email</label><br />
                  {{$profile['email']}}
                </div> -->

                <!-- <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                  <label class="control-label">Email <span class="required-fields">*</span></label>
                  <input readonly class="form-control" type="text" value="{{ old('email')!='' ? old('email') : $profile['email'] }}" placeholder="E.g. john@abc.com" name="email" />
                  {!! $errors->first('email', '<p class="validation-errors">:message</p>') !!}
                </div>

                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                  <label class="control-label">Full Name <span class="required-fields">*</span></label>
                  <input class="form-control" type="text" value="{{ old('fullname')!='' ? old('fullname') : $profile['name'] }}" placeholder="e.g. John Doe" name="fullname" />
                  {!! $errors->first('fullname', '<p class="validation-errors">:message</p>') !!}
                </div>                
               
            </div>
            <div class="tile-footer">
              <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>&nbsp;&nbsp;&nbsp;<!-- <a class="btn btn-secondary" href="{{ route('celebrity.dashboard') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a> -->
            <!-- </div>
          </div> -->
          <!-- all sign up details -->
          <!-- <div class="tile">
          <div class="tile-body"> -->
                <div class="row">
                  <div class="col-md-6 col-12 form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                    <label class="control-label">Name <span class="required-fields">*</span></label>
                    <input class="form-control alphabetsOnly" type="text" value="{{ old('name')!='' ? old('name') : $profile['name'] }}" placeholder="e.g. John" name="name" maxlength="15" required />
                    {!! $errors->first('name', '<p class="validation-errors">:message</p>') !!}
                  </div>

                  <div class="col-md-6 col-12 form-group {{ $errors->has('surname') ? 'has-error' : ''}}">
                    <label class="control-label">Surname </label>
                    <input class="form-control alphabetsOnly" type="text" value="{{ old('surname')!='' ? old('surname') : $profile['surname'] }}" placeholder="e.g. Doe" name="surname" maxlength="15" />
                    {!! $errors->first('surname', '<p class="validation-errors">:message</p>') !!}
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6 col-12 form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                  <label class="control-label">Email <span class="required-fields">*</span></label>
                  <input class="form-control" type="text" value="{{ old('email')!='' ? old('email') : $profile['email'] }}" placeholder="e.g. john@abc.com" name="email" readonly maxlength="30" required />
                  {!! $errors->first('email', '<p class="validation-errors">:message</p>') !!}
                </div>
                  <div class="col-md-6 col-12 form-group {{ $errors->has('gamer_id') ? 'has-error' : ''}}">
                    <label class="control-label">Gamer ID <span class="required-fields">*</span></label>
                    <input class="form-control" type="text" value="{{ old('gamer_id')!='' ? old('gamer_id') : $profile['gamer_id'] }}" placeholder="e.g. xyz_123" name="gamer_id" maxlength="25" required />
                    {!! $errors->first('gamer_id', '<p class="validation-errors">:message</p>') !!}
                  </div> 
                </div> 
                <div class="row">
                  <div class="col-md-6 col-12 form-group {{ $errors->has('username') ? 'has-error' : ''}}">
                    <label class="control-label">Username <span class="required-fields">*</span></label>
                    <input class="form-control" type="text" value="{{ old('username')!='' ? old('username') : $profile['username'] }}" placeholder="e.g. john_doe" name="username" maxlength="20" required />
                    {!! $errors->first('username', '<p class="validation-errors">:message</p>') !!}
                  </div>

                  <div class="col-md-6 col-12 form-group">
                    <label class="control-label">Twitter Username</label>
                    <input class="form-control" type="text" value="{{ old('twitter')!='' ? old('twitter') : $profile['twitter'] }}" placeholder="e.g. twitter" name="twitter" maxlength="25" />
                    {!! $errors->first('twitter', '<p class="validation-errors">:message</p>') !!}
                  </div>
                  
                  
                </div>
                <div class="row">
                  <div class="col-md-6 col-12 form-group {{ $errors->has('skype') ? 'has-error' : ''}}">
                    <label class="control-label">Skype Username<span class="required-fields">*</span></label>
                    <input class="form-control id_group" type="text" value="{{ old('skype')!='' ? old('skype') : $profile['skype'] }}" placeholder="e.g. skype" name="skype" id="skype" maxlength="25" required />
                    {!! $errors->first('skype', '<p class="validation-errors">:message</p>') !!}
                  </div>

                  <div class="col-md-6 col-12 form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
                    <label class="control-label">Mobile Number <span class="required-fields">*</span></label>
                    <input class="form-control id_group" type="text" value="{{ old('mobile')!='' ? old('mobile') : $profile['mobile'] }}" placeholder="e.g. +123 456 789" name="mobile" id="mobile" minlength="7" maxlength="15" onkeypress="return isNumberOrDash(event);hide_error();" />
                    {!! $errors->first('mobile', '<p class="validation-errors">:message</p>') !!}
                  </div>
                  
                </div>

                <div class="row">
                <div class="col-md-{{ $profile['image']?8:12 }}">
                  <div class="form-group">    
                      <label for="first_name">Upload Profile Image</label>
                      <input name="image" type="file" class="form-control" >
                      {!! $errors->first('image', '<p class="validation-errors">:message</p>') !!}
                  </div>
                </div>
                @if( $profile['image'])
                <div class="col-md-4">
                  <div class="form-group">    
                      <span class="img-wrap">
                        <img src="{{ asset( "public/".$profile['image'] )  }}" alt="" width="100%" />
                        <a href="#" onclick="return deleteImage( '{{route( 'celebrity.delete_celebrity_image', Hashids::encode( $profile['id'] ) )}}', this );" title="Click here to delete image"><i class="fa fa-times" aria-hidden="true"></i></a>
                      </span>
                  </div>
                </div>
                @endif
              </div>
                
            </div>

              <div class="tile-footer">
              <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>&nbsp;&nbsp;&nbsp;<!-- <a class="btn btn-secondary" href="{{ route('celebrity.dashboard') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a> -->
            </div>
          </div>
          </form>

        </div>
        
        <div class="clearix"></div>        
      </div>



      <div class="row">
        <div class="col-md-12">
          <form name="password_frm" id="update_password_frm" method="post" action="{{ route('celebrity.updatepassword') }}">
           {{ csrf_field() }}

          @if ($message = Session::get('password_error'))
          <div class="alert alert-danger alert-block" style="margin-bottom: 10px;">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <span>{{ $message }}</span>
          </div>
          @endif
          @if ($message = Session::get('password_success'))
          <div class="alert alert-success alert-block" style="margin-bottom: 10px;">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <span>{{ $message }}</span>
          </div>
          @endif

          <div class="tile">
            <!-- <h3 class="tile-title">Vertical Form</h3> -->
            <div class="tile-body">

                <h3 class="tile-title">Change Password</h3>

                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                  <label class="control-label">Current Password <span class="required-fields">*</span></label>
                  <input class="form-control" type="password" placeholder="Enter password" name="old_password" maxlength="15" />
                  {!! $errors->first('password', '<p class="validation-errors">:message</p>') !!}
                </div>    


                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                  <label class="control-label">New Password <span class="required-fields">*</span></label>
                  <input class="form-control" type="password" placeholder="Enter password" name="password" id="password" maxlength="15" />
                  {!! $errors->first('password', '<p class="validation-errors">:message</p>') !!}
                </div> 

                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                  <label class="control-label">Confirm Password <span class="required-fields">*</span></label>
                  <input class="form-control" type="password" placeholder="Enter password" name="cpassword" maxlength="15" />
                  {!! $errors->first('password', '<p class="validation-errors">:message</p>') !!}
                </div> 

               
            </div>
            <div class="tile-footer">
              <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Change Password</button>&nbsp;&nbsp;&nbsp;<!-- <a class="btn btn-secondary" href="{{ route('celebrity.dashboard') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a> -->
            </div>
          </div>

          </form>

        </div>
        
        <div class="clearix"></div>        
      </div>


    </main>
 <!-- /.content-wrapper -->
@endsection