@extends('celebrity.layouts.admin')
@section('content')


	
	<main class="app-content">
      @include('celebrity.includes.adminbreadcrumb')
      <?php  ?>
       <div class="row">
        <div class="col-md-6 col-lg-3">
          <a href="{{ route('celebrity.slots') }}" class="widget-small primary coloured-icon"><i class="icon fa fa-gamepad fa-3x"></i>
            <div class="info">
              <h4>Games</h4>
              <p><b>{{ $gamecount }}</b></p>
            </div>
          </a>
        </div>
        <!-- <div class="col-md-6 col-lg-3">
          <div class="widget-small primary coloured-icon"><i class="icon fa fa-calendar-o fa-3x"></i>
            <div class="info">
              <h4>Played Games</h4>
              <p><b>45</b></p>
              <span style="float: right"><a href="#">View</a></span>
              <span style="float: right"><a href="javascript:void(0);">View</a></span> 
            </div>
          </div>
        </div> -->

      </div>
      <!-- <div id="calendar">
        <div class="row"></div>
      </div> -->
      <?php  ?>
    </main>

 <!-- /.content-wrapper -->
@endsection
