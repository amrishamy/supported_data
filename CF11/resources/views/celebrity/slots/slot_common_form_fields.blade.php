<div id="flds-wrap">
    <div class="row">
        <div class="col-md-2 form-group">
            <label for="exampleInputtext" class="font-weight-bold"> Game Amount ( $ )
            </label>
            <input type="number" step=".01" name="slot_price" value="{{ old('slot_price')}}" class="form-control slot_price" id="" aria-describedby="" placeholder="" min="1" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);">
            {!! $errors->first('slot_price', '<p class="validation-errors">:message</p>') !!}
        </div>
        <div class="col-md-2 form-group">
            <label for="exampleInputdate" class="font-weight-bold">Date</label>
            <input name="slot_date" class="form-control slot_date" type="text" value="{{ old('slot_date')}}" id="slot_date" readonly >
            {!! $errors->first('slot_date', '<p class="validation-errors">:message</p>') !!}
        </div>
        <div class="col-md-2 form-group">				
            <label for="exampleInputdate" class="font-weight-bold">Start Time</label>
            <input class="form-control abcd" type="text" name="starttime" value="{{ old('starttime')}}" id="starttime" placeholder="Select start time"  data-step="5" data-max-time="23:55" data-show-2400="true" maxlength="5" >
                
            {!! $errors->first('starttime', '<p class="validation-errors">:message</p>') !!}
            @if ($message = Session::get('slot_time_error'))
            <p class="validation-errors s-side">{{ $message }}</p>
            
            @endif
        </div>
        <div class="col-md-2 form-group">
			<label for="exampleInputtext">&nbsp;</label>	
            <input type="hidden" name="endtime" value="{{ old('endtime')}}" id="endtime" placeholder="Select end time" data-time-format="H:i" data-step="15" data-min-time="00:00" data-max-time="23:55" data-show-2400="true" readonly>
            <?php /* {!! $errors->first('endtime', '<p class="validation-errors">:message</p>') !!} */ ?>
            <button class="d-flex align-items-center justify-content-center btn btn-primary btn-lg form-control" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add</button>
			<!-- <a class="btn btn-secondary" href="{{ route('celebrity.slots') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a> -->
        </div>
    </div>
    <p><strong>Note:</strong> Time zone for this slot is <?php echo date_default_timezone_get(); ?> </p>
</div>