@extends('celebrity.layouts.admin')
@section('content')
@php 
	$commonFormShow = false;
@endphp
  <main class="app-content">
      @include('celebrity.includes.adminbreadcrumb')
      
        @if ($message = Session::get('error'))
          <div class="alert alert-danger alert-block" style="margin-bottom: 10px;">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <span>{{ $message }}</span>
          </div>
        @endif

      <div class="game-table">
      @if( empty( $games->count() ) )
        <h4> No Game available to create the slot. Please contact Administrator.</h4>
      @else
        <form name="slot_frm" id="add_slot_frm" method="post" action="{{ route('celebrity.saveslot') }}">
             
					{{ csrf_field() }}
					<input type="hidden" name="celebrity" value="{{session('manager_id')}}" />
					@if( !empty( $games ) )
						@foreach($games as $game)
						<div class="columns my-2 columns-list-group w-100">
							<div class="list-group text-left bg-alt">
								<div class="card">
									<div class="card-header">
										<label class="custom-radio-button">
											<input type="radio" name="game" class="game" value="{{ $game->id }}" @if(old('game') == $game->id ) checked @endif game_time="{{ $game->game_time }}" />
											<span class="checkmark"></span> 
											{{ $game->name }}
										</label>
										<span>Slot Time: {{ $game->game_time }} minutes</span>
										@php
											$AvailablePS = explode(',', $game->type ) ;
										@endphp
									</div>
									<div class="card-body">
										<div class="slot-detail-form">
											<ul class="slots-section d-flex align-items-center justify-content-start">
												@php 
													$formShow = false;
												@endphp
												@foreach($platforms as $k=>$v)
													@php 
														if( old( 'platform_type' ) && isset( old( 'platform_type' )[$game->id] ) && in_array($k,old( 'platform_type' )[$game->id] ) ) $commonFormShow = $formShow = true;
													@endphp
													@if(in_array($k,$AvailablePS ) )
													<li class="list-group-item">
														<label class="form-check-label font-weight-bold">
															<input type="checkbox"  data-radio="{{ $game->id }}" name="platform_type[{{ $game->id }}][]" class="form-check-input game_platform platform_type" value="{{ $k }}" @if( old( 'platform_type' ) && isset( old( 'platform_type' )[$game->id] ) && in_array($k,old( 'platform_type' )[$game->id] ) ) checked @endif @if(!in_array($k,$AvailablePS ) ) disabled @endif ><span>{{ $v }}</span>
														</label>
													</li>
													@endif
												@endforeach
											</ul>
											@if( $formShow )
												@include('celebrity.slots.slot_common_form_fields')
											@endif
										</div>
									</div>	
								</div>	

							</div>
						
						</div>
						@endforeach
					@endif
					{!! $errors->first('game', '<p class="validation-errors">:message</p>') !!}
				 
			@if( !$commonFormShow )
				@include('celebrity.slots.slot_common_form_fields')
			@endif
            <div class="clearix"></div>
        </form>
      @endif
        </div>
      <script>
    
      </script>	

    </main>
 <!-- /.content-wrapper -->
@endsection

