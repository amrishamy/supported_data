@extends('celebrity.layouts.admin')
@section('content')
  <?php //echo $game->id; die;?>
  <main class="app-content">
      @include('celebrity.includes.adminbreadcrumb')
      
        @if ($message = Session::get('error'))
          <div class="alert alert-danger alert-block" style="margin-bottom: 10px;">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <span>{{ $message }}</span>
          </div>
        @endif

      <div class="game-table">
      @if( empty( $game) )
        <h4> Something went wrong. Please contact Administrator.</h4>
      @else
        <form name="slot_frm" id="edit_slot_frm" method="post" action="{{ route('celebrity.updateslot') }}">
            <div class="row row-eq-height"> 
				{{ csrf_field() }}
				<input type="hidden" name="celebrity" value="{{session('manager_id')}}" />
				<input type="hidden" name="slot_id" value="{{ $slot_detail->id }}" />

				<div class="columns my-2 columns-list-group w-100">
					<div class="list-group text-left bg-alt">
						<div class="card">
							<div class="card-header">						
								<label class="custom-radio-button mb-0">
									<input type="radio" name="game" class="game" value="{{ $game->id }}" checked game_time="{{ $game->game_time }}" />
									
									<span class="checkmark"></span> 
									{{ $game->name }}
								</label>
								 
								<span>Slot Time: {{ $game->game_time }} minutes</span>
								
								@php
									$AvailablePS = explode(',', $game->type ) ;
									$selected_pf_type = explode(',', $slot_detail->platform_type );
									
								@endphp
							</div>
							<div class="card-body">
								<div class="slot-detail-form">
									<ul class="slots-section">
										@foreach($platforms as $k=>$v)
											@if(in_array($k,$AvailablePS ) )
											<li class="list-group-item">
												<label class="form-check-label font-weight-bold">
													<input type="checkbox" data-radio="{{ $game->id }}" name="platform_type[{{ $game->id }}][]" class="form-check-input game_platform1" value="{{ $k }}" @if( old( 'platform_type' ) && isset( old( 'platform_type' )[$game->id] ) && in_array($k,old( 'platform_type' )[$game->id] ) ) checked @endif @if(in_array($k,$selected_pf_type ) && $slot_detail->game_id == $game->id ) checked @endif @if(!in_array($k,$AvailablePS ) ) disabled @endif ><span>{{ $v }}</span>
												</label>
											</li>
											@endif
										@endforeach
									</ul>
							   </div>  
							</div>
						</div>
					</div>
				</div>
				{!! $errors->first('game', '<p class="validation-errors">:message</p>') !!}
			</div>
			<div class="form-details" id="flds-wrap1">
				<div class="row">
					<div class="col-md-4 col-12 form-group">
						<label for="exampleInputtext" class="font-weight-bold">Game Amount ( $ )
						</label>
						<input type="number" step=".01" name="slot_price" value="{{ $slot_detail->slot_price }}" class="form-control slot_price" id="" aria-describedby="" placeholder="" min="1" maxlength="10" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" >
						{!! $errors->first('slot_price', '<p class="validation-errors">:message</p>') !!}
					</div>
					<div class="col-md-4 col-12 form-group">
						<label for="exampleInputdate" class="font-weight-bold">Date</label>
						<input name="slot_date" class="form-control slot_date" type="text" value="{{ date('d M, Y', strtotime( $slot_detail->slot_date ))  }}" id="slot_date" readonly>
						{!! $errors->first('slot_date', '<p class="validation-errors">:message</p>') !!}
					</div>
					
					<div class="col-md-4 col-12 form-group">
						<label for="exampleInputdate" class="font-weight-bold">Start Time</label>
						<input class="form-control" type="text" name="starttime" value="{{ $slot_detail->slot_start_time }}" id="starttime" placeholder="Select start time"  data-step="5" data-max-time="23:55" data-show-2400="true" maxlength="5" >
						{!! $errors->first('starttime', '<p class="validation-errors">:message</p>') !!}
						@if ($message = Session::get('slot_time_error'))
						<span class="validation-errors">{{ $message }}</span>
						
						@endif
					</div>
					
						<!-- <label for="exampleInputdate" class="font-weight-bold">End Time</label> -->
						<input class="form-control" type="hidden" name="endtime" value="{{ $slot_detail->slot_end_time }}" id="endtime" placeholder="Select end time" data-time-format="H:i" data-step="15" data-min-time="00:00" data-max-time="23:55" data-show-2400="true" readonly>
						<!--{!! $errors->first('endtime', '<p class="validation-errors">:message</p>') !!}-->
					
				</div>

				<small style="position:relative; top:-4px; font-size:13px">(Note: Time zone for this slot is <?php echo date_default_timezone_get(); ?> )</small>
				<div class="tile-footer">
					<button class="btn btn-primary btn-lg" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>&nbsp;&nbsp;&nbsp;<!-- <a class="btn btn-secondary" href="{{ route('celebrity.slots') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a> -->
				</div>

			</div>
            <div class="clearix"></div>
            
        
        </form>
      @endif
        </div>
      <script>
    
      </script>	

    </main>
 <!-- /.content-wrapper -->
@endsection
