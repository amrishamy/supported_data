@extends('celebrity.layouts.admin')
@section('content')
  
  <main class="app-content">
      @include('celebrity.includes.adminbreadcrumb')
      
      <div class="row">
        <div class="col-md-12">
          <form name="user_frm" id="add_user_frm" method="post" action="{{ route('celebrity.savegame') }}">
           {{ csrf_field() }}
            <div class="tile">
              <!-- <h3 class="tile-title">Vertical Form</h3> -->
              <div class="form-group">    
                  <label for="first_name">Name:</label>
                  <input type="text" class="form-control" placeholder="Enter Name" name="name"/>
                  {!! $errors->first('name', '<p class="validation-errors">:message</p>') !!}
              </div>

              <div class="form-group">
                  <label for="last_name">Type:</label>
                  <select class="form-control" name="type">
                    <option value="">Select Game Type</option>
                    <option value="1">Type 1</option>
                    <option value="2">Type 2</option>
                    <option value="3">Type 3</option>
                    <option value="4">Type 4</option>
                  </select>
                   {!! $errors->first('type', '<p class="validation-errors">:message</p>') !!}
              </div>                     
            </div>
            <div class="tile-footer">
              <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="{{ route('admin.userlist') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
            </div>
          </div>

          </form>

        </div>
        
        <div class="clearix"></div>
        
      </div>


    </main>
 <!-- /.content-wrapper -->
@endsection
