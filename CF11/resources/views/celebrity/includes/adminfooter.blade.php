<!-- Essential javascripts for application to work-->

  
<!-- <link href="http://demos.codexworld.com/bootstrap-datetimepicker-add-date-time-picker-input-field/css/bootstrap-datetimepicker.css" rel="stylesheet"> -->
<script type="text/javascript" src="{{asset('assets/celebrity/js/plugins/bootstrap-datepicker.min.js')}}"></script>
<!-- <script src="{{asset('assets/celebrity/js/bootstrap-datetimepicker.min.js')}}"></script>  -->


<script src="{{asset('assets/celebrity/js/popper.min.js')}}"></script>
<script src="{{asset('assets/celebrity/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/celebrity/js/jquery-ui.min.js') }}"></script>
<script src="{{asset('assets/celebrity/js/script.js') }}"></script>

<!-- The javascript plugin to display page loading on top-->
<script src="{{asset('assets/celebrity/js/plugins/pace.min.js')}}"></script>
<!-- Page specific javascripts-->
<!-- Data table plugin-->
<script type="text/javascript" src="{{asset('assets/celebrity/js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/celebrity/js/plugins/dataTables.bootstrap.min.js')}}"></script>

<script src="{{asset('assets/celebrity/js/sweetalert.js')}}"></script>
<link rel="stylesheet" href="{{asset('assets/celebrity/css/sweetalert.css')}}">
<script src="{{asset('assets/celebrity/js/main.js')}}"></script>
  
<!-- Timepicker css & js lib -->  
<script src="{{asset('assets/celebrity/js/jquery.timepicker.min.js')}}"></script>

<link rel="stylesheet" href="{{asset('assets/celebrity/css/jquery.timepicker.min.css')}}">

<!-- Full celendar files -->
  <link rel="stylesheet" href="{{asset('assets/celebrity/css/fullcalendar.css')}}">
  <link rel="stylesheet" href="{{asset('assets/celebrity/css/jquery-ui.min.css')}}">
  <script src="{{asset('assets/celebrity/js/moment.min.js')}}"></script>
  <script src="{{asset('assets/celebrity/js/fullcalendar.min.js')}}"></script>
<!-- Ends here celendar -->


<script type="text/javascript">

// var eventTable = $('#eventTable').DataTable({
//     'processing': true,
//     'serverSide': true,
//     'iDisplayLength': 10,
//     'columnDefs': [ { orderable: false, targets: [0,4,5]}],
//     "order": [[2, 'desc']],
//     'oLanguage': {'sProcessing': '<div class="datatable_loading" style="display:block;">Loading&#8230;</div>'},
//     'ajax': {
//         'url':'{{ route("celebrity.eventajaxdata") }}'
//     },
//     "fnCreatedRow": function( nRow, aData, iDataIndex ) {
//         $(nRow).attr('id', "tr_"+iDataIndex);
//     },
//     "fnDrawCallback": function (o) {
//       $('html,body').animate({scrollTop: 0}, 500);
//     }
// });



var slotTable = $('#slotTable').DataTable({
    'processing': true,
    'serverSide': true,
    'iDisplayLength': 10,
    "order": [7, "desc" ],
    'columnDefs': [ { orderable: false, targets: [0,4,5,8]}],
    'oLanguage': {'sProcessing': '<div class="datatable_loading" style="display:block;">Loading&#8230;</div>'},
    'ajax': {
        'url':'{{ route("celebrity.slotajaxdata") }}'
    }
  });


function delete_row(rowid, name, index){
  $("#tr_"+index).css("background","#c1c1c1");
	let affected_data_model = $("#data_model").val();
	swal({
	  title: "Are you sure?",
	  text: "You will not be able to recover this record!",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonClass: "btn-danger",
	  confirmButtonText: "Yes, delete it!"
	}, function (isConfirm) {
		if (isConfirm) {
		  $.ajax({
	          type:'POST',
	          url:'{{ route("celebrity.deleterow") }}',
	          data:{"_token": "{{ csrf_token() }}", "rowid":rowid, "affected_data_model":affected_data_model},
	          success:function(data){
              let obj = JSON.parse(data);
              setTimeout(() => {
                $( 'body' ).removeClass( 'stop-scrolling' );
                if(obj.code==200){ 
                  swal("Deleted!", obj.message, "success");
                  slotTable.ajax.reload( null, false );
	            }else{
	                swal("Error Occured!",obj.message, "error");
              }
              
              }, 500);

	          }

	      });	
		}

	});
}

function delete_child(rowid, name, index){
  $("#tr_"+index).css("background","#c1c1c1");
  let affected_data_model = $("#parent_model").val();
  swal({
    title: "Are you sure?",
    text: "You will not be able to recover this record!",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Yes, delete it!"
  }, function (isConfirm) {
    if (isConfirm) {
      $.ajax({
            type:'POST',
            url:'{{ route("admin.deleterow") }}',
            data:{"_token": "{{ csrf_token() }}", "rowid":rowid, "affected_data_model":affected_data_model},
            success:function(data){
              let obj = JSON.parse(data);
              if(obj.code==200){
                slotTable.ajax.reload();
                swal("Deleted!", obj.message, "success");
              }else{
                  swal("Error Occured!",obj.message, "error");
              }
            }

        }); 
    }

  });
}


$("#goBtt").click(function(){

    var selectedRowIds = [];
    $.each($("input[name='ids[]']:checked"), function(){
        selectedRowIds.push($(this).val());
    });
    let actiontype = $("#actionDropdown").val(); let affected_data_model = $("#data_model").val();

    if(actiontype=="" || actiontype==null){
    	swal("Please select an action.");
    	return false;
    }else{
    	let checkboxLength = $("input[name='ids[]']:checked").length;
    	if(checkboxLength==0){
    	  swal("Please select at least one record.");
    	  return false;
    	}
    }
    swal({
      title: "Are you sure you want to perform this action?",
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, do it!"
    }, function (isConfirm) {
      if (isConfirm) {
        $.ajax({
              type:'POST',
              url:'{{ route("celebrity.updatebulkrows") }}',
              data:{"_token": "{{ csrf_token() }}", "rowids":selectedRowIds, "actiontype":actiontype, "affected_data_model":affected_data_model},
              success:function(data){
                let obj = JSON.parse(data);
                $('input[type="checkbox"]').prop('checked', false);
                $('#actionDropdown').prop('selectedIndex',"");
                setTimeout(() => {
                $( 'body' ).removeClass( 'stop-scrolling' );
                if(obj.code==200){ 
                // alert( obj.code );
	            	// location.reload();
                swal("Done!", obj.message, "success");
                slotTable.ajax.reload( null, false );
	            }else{
	                swal("Error Occured!",obj.message, "error");
              }
              
              }, 500);

              }

          });	
      }else{
        $('input[type="checkbox"]').prop('checked', false);
        $('#actionDropdown').prop('selectedIndex',"");
      }

    });

});


//var allPages = userTable.fnGetNodes();
      
$('body').on('click', '#checkAll', function () {
  if ($(this).hasClass('allChecked')) {
      $('input[type="checkbox"]').prop('checked', false);
  } else {
      $('input[type="checkbox"]').prop('checked', true);      
  }
  $(this).toggleClass('allChecked');
});

// Bind the single checkboxes with Bulk checkbox selection
$( document ).ready(function(){ 
  $(document).on('change', 'input[name="ids[]"]', function () {
        if ($(this).is(":checked")) { 
            var isAllChecked = 0;

            $('input[name="ids[]"]',document).each(function() {
              // alert( this.checked );
                if (!this.checked)
                    isAllChecked = 1;
            });
            
            if (isAllChecked == 0) {
                $("#checkAll").prop("checked", true);
            }     
        }
        else {
            $("#checkAll").prop("checked", false);
        }
    });

    $('.table').on( 'page.dt', function () { 
      $("#checkAll").prop("checked", false);
    } );
    $('.table').on( 'length.dt', function ( e, settings, len ) {
      $("#checkAll").prop("checked", false);
    } );

    $('.table').on( 'order.dt', function () {
      // This will show: "Ordering on column 1 (asc)", for example
      $("#checkAll").prop("checked", false);
    } );

});


</script>
<!-- <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>
<script>  CKEDITOR.replace( 'email_template' ); </script> -->

<script type="text/javascript" src="{{asset('assets/celebrity/js/plugins/chart.js')}}"></script>
    <script type="text/javascript">
      var data = {
      	labels: ["January", "February", "March", "April", "May"],
      	datasets: [
      		{
      			label: "My First dataset",
      			fillColor: "rgba(220,220,220,0.2)",
      			strokeColor: "rgba(220,220,220,1)",
      			pointColor: "rgba(220,220,220,1)",
      			pointStrokeColor: "#fff",
      			pointHighlightFill: "#fff",
      			pointHighlightStroke: "rgba(220,220,220,1)",
      			data: [65, 59, 80, 81, 56]
      		},
      		{
      			label: "My Second dataset",
      			fillColor: "rgba(151,187,205,0.2)",
      			strokeColor: "rgba(151,187,205,1)",
      			pointColor: "rgba(151,187,205,1)",
      			pointStrokeColor: "#fff",
      			pointHighlightFill: "#fff",
      			pointHighlightStroke: "rgba(151,187,205,1)",
      			data: [28, 48, 40, 19, 86]
      		}
      	]
      };
      var pdata = [
      	{
      		value: 300,
      		color: "#46BFBD",
      		highlight: "#5AD3D1",
      		label: "Complete"
      	},
      	{
      		value: 50,
      		color:"#F7464A",
      		highlight: "#FF5A5E",
      		label: "In-Progress"
      	}
      ]

      function fetchstates (country ,stateId = 0){
        var url = "<?=url('/').'/celebrity/loadstates/'?>"+country+"/"+stateId;
        $.ajax({
          type: "GET",
          url: url,
          success: function(result){
            $("#state_tab").html(result);
          }
        });
      }
    </script>


<script>
   
  $(document).ready(function() {
   var calendar = $('#calendar').fullCalendar({
    editable:true,
    header:{
      left:'prev,next today',
      center:'title',
      right:''
    },
    eventColor: 'green',
    timeFormat: 'HH:mm',
    events: "<?=url('/').'/celebrity/eventslistajax'?>",
    selectable:true,
    selectHelper:true,
    select: function(start, end, allDay)
    {
      // alert('clicked on blank date box');
    },
    editable:true,

    eventClick:function(event)
    {
      // alert("clickec on particular Event: "+event.title + " \nStart Time: "+event.start+" \nEnds on: "+event.end);
    },

   });

    $('.ui-timepicker-input').keypress(function (event) {
        return isNumber(event, this)
    });

    $( "#user_email" ).autocomplete({
          source: function( request, response ) {              
              $.ajax({
                  url: "{{ route('celebrity.emaillist') }}",
                  type: 'post',
                  dataType: "json",
                  data: {
                      "_token": "{{ csrf_token() }}",search: request.term, selectedIds: $("#blank-input").val()
                  },
                  success: function( data ) {
                      response( data );
                  }
              });
          },
          select: function (event, ui) {
              $('#user_email').val(ui.item.value); // display the selected text
              getEmailDataFilled( ui.item.value );
              return false;
          }
      });

  });

$( "#user_email" ).change(function() {
  var email = $( "#user_email" ).val();
  getEmailDataFilled( email );
});

  function getEmailDataFilled ( email ){
    $.ajax({
      url: "{{ route('celebrity.getemailIdata') }}",
      type: 'post',
      data: {
          "_token": "{{ csrf_token() }}",email: email,
      },
      success: function( data ) {
        if ( data.length != 0 ) {
          $("input[name='fullname']").val(data.name).prop('readonly', true);
          $("select[name=country]").val(data.coutry_id).prop('disabled', true);
          fetchstates(data.coutry_id, data.state_id);
          $("select[name=state]").prop('disabled', true);
        }else{
          $("input[name='fullname']").val('').prop('readonly', false);
          $("select[name=country]").val('').prop('disabled', false);
          $("select[name=state]").val('').prop('disabled', false);
        }
      }
    });
  }

    // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode
        if (
            (charCode != 58 || $(element).val().indexOf(':') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    }  
  </script>



  <script type="text/javascript">
          /* For user active status change*/

      $(function() {
        $('.toggle-class').change(function() {
          jQuery(".active-status").remove();
            var status = $(this).prop('checked') == true ? 2 : 1; 
            var user_id = $(this).data('id'); 
             
            $.ajax({
                type: "GET",
                url: "{{ route('celebrity.changeStatus') }}",
                data: {'status': status, 'user_id': user_id},
                success: function(data){
                  if(data.status == 'success'){
                    jQuery('.status-toggle.toggle-button-cover').prepend('<span class="active-status active-success-staus">'+data.message+'</div>');
                  }else{
                    jQuery('.status-toggle.toggle-button-cover').prepend('<span class="active-status active-failed-staus">'+data.message+'</div>');
                  }
                  setTimeout(function(){ jQuery(".active-status").remove(); }, 2000);
                }
            });
        })
      })

       $(".ph-number").keyup(function(){
    $(this).val($(this).val().replace(/^(\d{1})(\d{3})(\d{3})(\d)+$/, "$1-($2)-$3-$4"));
  });

  function isNumberOrDash(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    //alert(charCode);

    if (charCode != 46 && charCode != 45 && charCode > 31
    && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  </script>