<div class="app-title">
  <div>
    <h1><!-- <i class="fa fa-th-list"></i> --> <?php echo $breadcrumbTitle; ?></h1>
    <!-- <p>Table to display analytical data effectively</p> -->
  </div>
   <?php $userData = session('userData');?>
  <!--<div class="celebrity-active-btn">
    <div id="app-cover">
      <div class="row">
        <div class="toggle-button-cover status-toggle">
          <div class="button-cover">
            <div class="button r" id="button-1">
              <input type="checkbox" data-id="<?php echo $userData->id ?>" class="checkbox toggle-class" <?php echo $userData->active_status == 2 ? 'checked' : '' ?>>
              <div class="knobs"></div>
              <div class="layer"></div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>-->
  <ul class="app-breadcrumb breadcrumb side">
    <li class="breadcrumb-item"><a href='<?php echo route("celebrity.dashboard"); ?>'><i class="fa fa-home fa-lg"></i></a></li>
   	<?php
   	if(!empty($breadcrumbLink)){ ?>
   		<li class="breadcrumb-item"><a href='<?php echo route($breadcrumbLink); ?>'><?php echo $breadcrumbItem; ?></a></li>
   	<?php }else{ ?>
   		<li class="breadcrumb-item"><?php echo $breadcrumbItem; ?></li>
   	<?php } ?>
    
    <?php if(!empty($breadcrumbTitle)){ ?>
    	<li class="breadcrumb-item active"><?php echo $breadcrumbTitle; ?></li>
	<?php } ?>
  </ul>
</div>