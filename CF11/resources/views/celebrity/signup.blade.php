@extends('celebrity.layouts.default')
@section('content')
    <section class="material-half-bg">
      <div class="cover"></div>
    </section>
    <section class="login-content">
      <div class="logo">
        <h1>CF11</h1>
      </div>
      
        @if ($message = Session::get('error'))
            <div class="login-box" style="min-height:50px;max-height:50px;background:transparent;box-shadow: none;">
        <div class="alert alert-danger alert-block" >
          <button type="button" class="close" data-dismiss="alert">×</button> 
                <span>{{ $message }}</span>
        </div></div>
        @endif


        <div class="col-md-8 col-lg-8 col-xs-12">
          <div class="tile">
            <form name="eventmanager_frm" enctype="multipart/form-data" id="add_eventmanager_frm" method="post" action="{{ route('celebrity.register') }}">
            {{ csrf_field() }}
            <h3 class="tile-title text-center">Sign Up</h3>
            <div class="tile-body">
                <div class="row">
                  <div class="col-md-6 col-12 form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                    <label class="control-label">Name <span class="required-fields">*</span></label>
                    <input class="form-control alphabetsOnly" type="text" value="{{ old('name') }}" placeholder="e.g. John" name="name" maxlength="15" />
                    {!! $errors->first('name', '<p class="validation-errors">:message</p>') !!}
                  </div>

                  <div class="col-md-6 col-12 form-group {{ $errors->has('surname') ? 'has-error' : ''}}">
                    <label class="control-label">Surname </label>
                    <input class="form-control alphabetsOnly" type="text" value="{{ old('surname') }}" placeholder="e.g. Doe" name="surname" maxlength="15" />
                    {!! $errors->first('surname', '<p class="validation-errors">:message</p>') !!}
                  </div>
                </div>
                <div class="row">
                <div class="col-md-6 col-12 form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                  <label class="control-label">Email <span class="required-fields">*</span></label>
                  <input class="form-control" type="text" value="{{ old('email') }}" placeholder="e.g. john@abc.com" name="email" maxlength="30" />
                  {!! $errors->first('email', '<p class="validation-errors">:message</p>') !!}
                </div>
                <div class="col-md-6 col-12 form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                  <label class="control-label">Password <span class="required-fields">*</span></label>
                  <input class="form-control" type="password" placeholder="Enter password" name="password" maxlength="15" />
                  {!! $errors->first('password', '<p class="validation-errors">:message</p>') !!}
                </div>  
                </div> 
                <div class="row">
                  <div class="col-md-6 col-12 form-group {{ $errors->has('username') ? 'has-error' : ''}}">
                    <label class="control-label">Username <span class="required-fields">*</span></label>
                    <input class="form-control" type="text" value="{{ old('username') }}" placeholder="e.g. john_doe" name="username" maxlength="20" />
                    {!! $errors->first('username', '<p class="validation-errors">:message</p>') !!}
                  </div>

                  <div class="col-md-6 col-12 form-group">
                    <label class="control-label">Twitter Username</label>
                    <input class="form-control" type="text" value="{{ old('twitter') }}" placeholder="e.g. twitter" name="twitter" maxlength="25" />
                    {!! $errors->first('twitter', '<p class="validation-errors">:message</p>') !!}
                  </div>
                  
                  
                </div>
                <div class="row">
                  <div class="col-md-6 col-12 form-group {{ $errors->has('skype') ? 'has-error' : ''}}">
                    <label class="control-label">Skype Username<span class="required-fields">*</span></label>
                    <input class="form-control id_group" type="text" value="{{ old('skype') }}" placeholder="e.g. skype" name="skype" id="skype" maxlength="25" />
                    {!! $errors->first('skype', '<p class="validation-errors">:message</p>') !!}
                  </div>

                  <div class="col-md-6 col-12 form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
                    <label class="control-label">Mobile Number <span class="required-fields">*</span></label>
                    <input class="form-control id_group ph-number" type="number" minlength="7" maxlength="15" value="{{ old('mobile') }}" placeholder="e.g. +123 456 789" name="mobile" id="mobile" onkeypress="return isNumberOrDash(event);hide_error();" />
                    {!! $errors->first('mobile', '<p class="validation-errors">:message</p>') !!}
                  </div>
                  
                </div>
                <div class="row">
                  <div class="col-md-6 col-12 form-group {{ $errors->has('gamer_id') ? 'has-error' : ''}}">
                    <label class="control-label">Gamer ID <span class="required-fields">*</span></label>
                    <input class="form-control" type="text" value="{{ old('gamer_id') }}" placeholder="e.g. xyz_123" name="gamer_id" maxlength="25" />
                    {!! $errors->first('gamer_id', '<p class="validation-errors">:message</p>') !!}
                  </div>
                  <div class="col-md-6 col-12 form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                    <label class="control-label">Upload profile image <span class="required-fields"></span></label>
                    <input class="form-control" name="image" type="file" value="{{ old('image') }}" />
                    {!! $errors->first('image', '<p class="validation-errors">:message</p>') !!}
                  </div>

                </div>

                <div class="form-group term-conditions {{ $errors->has('termsnconditions') ? 'has-error' : ''}}">
               <label>
                    <input class="form-control" id="termsnconditions" type="checkbox" name="termsnconditions" value="1" />
                
                
                    <p>I agree to the <a target="_blank" href="{{ route('termnconditions') }}">Terms & Conditions</a>.</p>
                  </label>
                 
                  {!! $errors->first('termsnconditions', '<p class="validation-errors">:message</p>') !!}
                </div>

                
            </div>
            <div class="tile-footer p-0">
              <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Register</button>&nbsp;&nbsp;&nbsp;<a class="" href="{{ route('celebrity.login') }}"><!-- <i class="fa fa-fw fa-lg fa-times-circle"></i> -->Back To Login</a>
            </div>

            </form>
          </div>
        </div>

   </section>
  
@stop


<script type="text/javascript">
  
//   $(".ph-number").keyup(function(){
//     $(this).val($(this).val().replace(/^(\d{1})(\d{3})(\d{3})(\d)+$/, "$1-($2)-$3-$4"));
//   });

//   function isNumberOrDash(evt) {
// evt = (evt) ? evt : window.event;
// var charCode = (evt.which) ? evt.which : evt.keyCode;
// //alert(charCode);

// if (charCode != 46 && charCode != 45 && charCode > 31
// && (charCode < 48 || charCode > 57)) {
// return false;
// }
// return true;
// }

</script>