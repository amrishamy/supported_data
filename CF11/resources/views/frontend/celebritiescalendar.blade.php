@extends('layouts.frontend')

@section('content')

 <div class="inner-banner text-white">
			<img src="{{asset('assets/frontend/img/celebrity/calender_banner.jpg')}}" alt="E-Sport" class="img-fluid w-100 shadow-lg">            
            <div class="innerbanner_summary">
				<h1>celebrity <span>calendar</span></h1>
			</div>          
        </div>	  
      <div class="clearfix"></div>
      <!-- game-section start -->		
      <section class="game-listing">
      <div class="container">
         <div class="search-listing filters">
            <form>							  
               <div class="form-row justify-content-end filter_row">
                  <div class="col-md-3 col-12 form-group">
                     <select id="c_id" data-show-content="true" class="form-control border">
                        <option value="">All Celebrities</option>
                        @foreach( $celebList as $celebrityId => $celebrityName )
                           <option value="{{$celebrityId}}" data-content="{{$celebrityName}}">{{$celebrityName}}</option>
                        @endforeach
                        
                     </select>
                  </div>
                  <div class="col-md-3 col-12 form-group">
                     <select id="g_id" data-show-content="true" class="form-control border">
                        <option value="">All Games</option>
                        @foreach( $gamesList as $gameId => $gameName )
                           <option value="{{$gameId}}" data-content="{{$gameName}}">{{$gameName}}</option>
                        @endforeach
                     </select>
                  </div>
                  <?php /*
                  <div class="col-md-2 col-12 form-group">
                     <select id="pltfrm_id" data-show-content="true" class="form-control border">
                        <option value="">All Platforms</option>
                        @foreach( $platforms as $pId => $platformName )
                           <option value="{{$pId}}" data-content="{{$platformName}}">{{$platformName}}</option>
                        @endforeach
                     </select>
                  </div>
                  */ ?>
                  
                  
                  <div class="col-md-3 col-12 form-group">
                     <div class="input-group date bg-white" data-provide="">
                        <input type="text" name="slot_date" class="slot_date form-control" placeholder="Select date" readonly />
                        <div class="input-group-addon">
                              <!-- <span class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></span> -->
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3 col-12 form-group">
                    <input class="form-control slot_time" type="text" id="timepick" placeholder="Select start time" data-step="5" data-max-time="23:55" data-show-2400="true" maxlength="5" autocomplete="off" />
                  </div>
                  
               </div>
            </form>
         </div>
         <div class="table-border">
            <div class="table-responsive">
                <table id="celebrity-list" class="table text-left" id="" style="width:100%">
                        <thead>
                           <tr>
                              <th>S.No.</th>                         
                              <th>Celebrity</th>
                              <th>Game Name</th>
                              <!-- <th>platform</th>                          -->
                              <th>Date</th>                         
                              <th>Start time</th>                         
                              <!-- <th>End Time</th>                          -->
                              <th>Amount</th>
                              <th class="text-center">Action</th>
                           </tr>
                        </thead>
                        <tbody class="celeb_cal_detail">
                           <?php /* @include('includes.celebrity_calendar_detail') */ ?>
						   
                        </tbody>
                  </table>
            </div>
         </div>
      </div>
    </section><!-- #contact -->
      <!-- game-section End-->
      <div class="clearfix"></div>
	
@endsection

@push('scroll_login_pages')
   <script type="text/javascript">
      var listUrl = '{{route("get_celebrity_calendar_detail")}}';
   </script>
   <script type="text/javascript" src="{{asset('assets/front/js/celebrity.js')}}"></script>
@endpush
