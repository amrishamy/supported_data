@extends('layouts.frontend')

@section('content')

<div class="inner-banner text-white">
    <img src="{{asset('assets/frontend/img/players/players_banner.jpg')}}" alt="E-Sport" class="img-fluid w-100 shadow-lg">            
    <div class="innerbanner_summary">
        <h1>Celebrities <span></span></h1>
    </div>          
</div>      
    <div class="clearfix"></div>
      <!-- game-section start -->		
      <section class="game-section bg-pattern">
         <div class="container">          
            <div class="row row-eq-height">
            @foreach($topCelebrities as $celebrity)
              <div class="col-md-3 item my-3">
                  <div class="online-player bg-white rounded inner-game-page">
                     <a class="calender-section"  href="{{ route('celebritiyprofile', [ 'id'=> Hashids::encode($celebrity->id)  ]) }}">
                        <figure>
                        @if($celebrity->image)
                        <img src="{{ asset('public').'/'.$celebrity->image }}" alt="" class="img-fluid w-100 shadow-lg">
                        @else
                           <img src="{{ asset('assets/frontend/img/coming_soon.jpg')}}" alt="" class="img-fluid w-100 shadow-lg">
                        @endif
                        </figure>     
						<summary class="p-3">
                           <h3>{{ $celebrity->name." ".$celebrity->surname }}</h3>
						</summary>
       						
                     </a>
                    
                  </div>
               </div>
               @endforeach
            </div>
			  
         </div>
			
      </section>
      <!-- game-section End-->
      <div class="clearfix"></div>

@endsection
