@extends('layouts.frontend')

@section('content')

<div class="inner-banner text-white">
    <img src="{{asset('assets/frontend/img/games/game_banner.jpg')}}" alt="E-Sport" class="img-fluid w-100 shadow-lg">   <div class="innerbanner_summary">
        <h1>Games <span></span></h1>
    </div>          
</div>      
<div class="clearfix"></div>
      <!-- game-section start -->		
      <section class="game-page bg-pattern">
         <div class="container">          
            <div class="row row-eq-height">
            @foreach($games as $k=>$game)
              <div class="col-lg-4 col-md-6 col-12 item my-3">
                  <div class="select-game bg-white inner-game-page rounded">
                     <div class="calender-section">
                        <figure>
                        @if($game->image)
                        <img src="{{ asset('public').'/'.$game->image }}" alt="" class="img-fluid w-100 shadow-lg">
                        @else
                           <img src="{{ asset('assets/frontend/img/coming_soon.jpg')}}" alt="" class="img-fluid w-100 shadow-lg">
                        @endif
                        </figure>
                        
                     </div>
                     <summary class="p-3 caption">
					      <h3>{{ $game->name }}</h3>
                            <?php 
                            $pfs = array();
                            $AvailablePF = explode(',', $game->type ) ;
                            foreach($platforms as $k=>$v)
                            {
                                if(in_array($k,$AvailablePF ) )
                                    $pfs[] =  $v ;
                            }
                            ?>
                        <div class="game-detail border-bottom pb-2 mb-2">
                          <figure class="flex-text">
						  <!--img src="{{asset('assets/frontend/img/icons/game.png')}}" alt="" class="icon-img"--> 
						  @foreach( $pfs as $k=>$val)
						  <span class="badge badge-danger mt-2"> {{ $val }}</span>	
						  @endforeach
						  </figure>				
                        </div>
							
                        <div class="game-btn text-center my-2 my-md-3">		
                           <a href="{{ route('celebritiescalendar') }}" class="btn btn-red btn-sm w-100">Find Celebs To Play</a>
                        </div>
                     </summary>
                  </div>
            </div>
			    @endforeach
             </div>
         </div>
      </section>
      <!-- game-section End-->
      <div class="clearfix"></div>

@endsection
