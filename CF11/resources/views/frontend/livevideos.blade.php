@extends('layouts.frontend')

@section('content')

<div class="inner-banner text-white">
            <img src="{{asset('assets/frontend/img/contact-us/contact_banner.jpg')}}" alt="E-Sport" class="img-fluid w-100 shadow-lg">   
            <div class="innerbanner_summary">
               <h1>live <span>videos</span></h1>
            </div>
         </div>    
         <div class="clearfix"></div>
      <!-- game-section start -->		
      <section class="video-section bg-pattern">
         <!-- Grid row -->
         <div class="container">
            <div class="row">
               <!-- Grid column -->
               <div class="col-md-12 galary-video px-0">
                  <div class="embed-responsive embed-responsive-16by9">
                     <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/tgbNymZ7vqY" allowfullscreen></iframe>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <div class="clearfix"></div>
      <section class="live-section">
         <div class="container">
            <div class="text-left mb-5">
               <h2 class="text-white">Recent video</h2>
            </div>
            <div class="most-recent-cat">
               <div class="dropdown">
                  <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Most Recent
                  </button>
                  <div class="dropdown-menu mr-3" aria-labelledby="dropdownMenuButton">
                     <a class="dropdown-item" href="#">Action</a>
                     <a class="dropdown-item" href="#">Another action</a>
                     <a class="dropdown-item" href="#">Something else here</a>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-3 col-md-12 mb-4">
                  <a href="https://www.youtube.com/embed/tgbNymZ7vqY" data-fancybox="gallery" data-caption="Caption #1">
                     <div class="calender-section">
                        <figure>
                           <img src="{{asset('assets/frontend/img/live-games/game-3.jpg')}}" alt="" class="img-fluid"/>
                        </figure>
                        <span class="play_icon"><img src="{{asset('assets/frontend/img/icons/play_icon.svg')}}" alt=""/></span>                       
                     </div>
                     <summary class="py-3 text-white">
                        <h4 class="d-flex align-items-center mb-2">Jennifer Lopez</h4>
                        <p>FIFA LEAGUE 2019 TOURNAMENT</p>
                     </summary>
                  </a>
               </div>
               <!-- Grid column -->
               <div class="col-lg-3 col-md-12 mb-4">
                  <a href="https://www.youtube.com/embed/tgbNymZ7vqY" data-fancybox="gallery" data-caption="Caption #1">
                     <div class="calender-section">
                        <figure>
                           <img src="{{asset('assets/frontend/img/live-games/game-3.jpg')}}" alt="" class="img-fluid"/>
                        </figure>
                        <span class="play_icon"><img src="{{asset('assets/frontend/img/icons/play_icon.svg')}}" alt=""/></span>                       
                     </div>
                     <summary class="py-3 text-white">
                        <h4 class="d-flex align-items-center mb-2">Jennifer Lopez</h4>
                        <p>FIFA LEAGUE 2019 TOURNAMENT</p>
                     </summary>
                  </a>
               </div>
               <!-- Grid column -->
               <div class="col-lg-3 col-md-6 mb-4">
                  <a href="https://www.youtube.com/embed/tgbNymZ7vqY" data-fancybox="gallery" data-caption="Caption #1">
                     <div class="calender-section">
                        <figure>
                           <img src="{{asset('assets/frontend/img/live-games/game-3.jpg')}}" alt="" class="img-fluid"/>
                        </figure>
                        <span class="play_icon"><img src="{{asset('assets/frontend/img/icons/play_icon.svg')}}" alt=""/></span>                       
                     </div>
                     <summary class="py-3 text-white">
                        <h4 class="d-flex align-items-center mb-2">Jennifer Lopez</h4>
                        <p>FIFA LEAGUE 2019 TOURNAMENT</p>
                     </summary>
                  </a>
               </div>
               <!-- Grid column -->
               <!-- Grid column -->
               <div class="col-lg-3 col-md-6 mb-4">
                  <a href="https://www.youtube.com/embed/tgbNymZ7vqY" data-fancybox="gallery" data-caption="Caption #1">
                     <div class="calender-section">
                        <figure>
                           <img src="{{asset('assets/frontend/img/live-games/game-3.jpg')}}" alt="" class="img-fluid"/>
                        </figure>
                        <span class="play_icon"><img src="{{asset('assets/frontend/img/icons/play_icon.svg')}}" alt=""/></span>                       
                     </div>
                     <summary class="py-3 text-white">
                        <h4 class="d-flex align-items-center mb-2">Jennifer Lopez</h4>
                        <p>FIFA LEAGUE 2019 TOURNAMENT</p>
                     </summary>
                  </a>
               </div>
               <!-- Grid column -->
            </div>
         </div>
         <!-- Grid row -->
      </section>
      <!-- game-section End-->
      <div class="clearfix"></div>
      <section class="live-section bg-pattern">
         <div class="container">
            <div class="text-left mb-5">
               <h2 class="text-white">latest video</h2>
            </div>
            <div class="most-recent-cat">
               <div class="dropdown">
                  <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Most Recent
                  </button>
                  <div class="dropdown-menu mr-3" aria-labelledby="dropdownMenuButton">
                     <a class="dropdown-item" href="#">Action</a>
                     <a class="dropdown-item" href="#">Another action</a>
                     <a class="dropdown-item" href="#">Something else here</a>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-3 col-md-12 mb-4">
                  <a href="https://www.youtube.com/embed/tgbNymZ7vqY" data-fancybox="gallery" data-caption="Caption #1">
                     <div class="calender-section">
                        <figure>
                           <img src="{{asset('assets/frontend/img/live-games/game-3.jpg')}}" alt="" class="img-fluid"/>
                        </figure>
                        <span class="play_icon"><img src="{{asset('assets/frontend/img/icons/play_icon.svg')}}" alt=""/></span>                       
                     </div>
                     <summary class="py-3 text-white">
                        <h4 class="d-flex align-items-center mb-2">Jennifer Lopez</h4>
                        <p>FIFA LEAGUE 2019 TOURNAMENT</p>
                     </summary>
                  </a>
               </div>
               <!-- Grid column -->
               <div class="col-lg-3 col-md-12 mb-4">
                  <a href="https://www.youtube.com/embed/tgbNymZ7vqY" data-fancybox="gallery" data-caption="Caption #1">
                     <div class="calender-section">
                        <figure>
                           <img src="{{asset('assets/frontend/img/live-games/game-3.jpg')}}" alt="" class="img-fluid"/>
                        </figure>
                        <span class="play_icon"><img src="{{asset('assets/frontend/img/icons/play_icon.svg')}}" alt=""/></span>                       
                     </div>
                     <summary class="py-3 text-white">
                        <h4 class="d-flex align-items-center mb-2">Jennifer Lopez</h4>
                        <p>FIFA LEAGUE 2019 TOURNAMENT</p>
                     </summary>
                  </a>
               </div>
               <!-- Grid column -->
               <div class="col-lg-3 col-md-6 mb-4">
                  <a href="https://www.youtube.com/embed/tgbNymZ7vqY" data-fancybox="gallery" data-caption="Caption #1">
                     <div class="calender-section">
                        <figure>
                           <img src="{{asset('assets/frontend/img/live-games/game-3.jpg')}}" alt="" class="img-fluid"/>
                        </figure>
                        <span class="play_icon"><img src="{{asset('assets/frontend/img/icons/play_icon.svg')}}" alt=""/></span>                       
                     </div>
                     <summary class="py-3 text-white">
                        <h4 class="d-flex align-items-center mb-2">Jennifer Lopez</h4>
                        <p>FIFA LEAGUE 2019 TOURNAMENT</p>
                     </summary>
                  </a>
               </div>
               <!-- Grid column -->
               <!-- Grid column -->
               <div class="col-lg-3 col-md-6 mb-4">
                  <a href="https://www.youtube.com/embed/tgbNymZ7vqY" data-fancybox="gallery" data-caption="Caption #1">
                     <div class="calender-section">
                        <figure>
                           <img src="{{asset('assets/frontend/img/live-games/game-3.jpg')}}" alt="" class="img-fluid"/>
                        </figure>
                        <span class="play_icon"><img src="{{asset('assets/frontend/img/icons/play_icon.svg')}}" alt=""/></span>                       
                     </div>
                     <summary class="py-3 text-white">
                        <h4 class="d-flex align-items-center mb-2">Jennifer Lopez</h4>
                        <p>FIFA LEAGUE 2019 TOURNAMENT</p>
                     </summary>
                  </a>
               </div>
               <!-- Grid column -->
            </div>
         </div>
         <!-- Grid row -->
      </section>
      <!-- game-section End-->
      <div class="clearfix"></div>

@endsection
