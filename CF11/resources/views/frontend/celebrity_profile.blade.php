@extends('layouts.frontend')

@section('content')

<div class="inner-banner text-white">
    <img src="{{asset('assets/frontend/img/profile-banner.jpg') }}" alt="E-Sport" class="img-fluid w-100 shadow-lg">            
    <div class="innerbanner_summary">
        <h1>Detail <span></span></h1>
    </div>          
</div>

      <div class="clearfix"></div>
      <section class="profile_sec">
      <div class="text-white">
		
			<div class="container">
				<div class="row align-items-center flex-sm-row-reverse">
				<div class="col-md-4">
						<figure>
							@if($info->image)
							<img src="{{ asset('public').'/'.$info->image }}" alt="" class="img-fluid w-100">
							@else
                           <img src="{{ asset('assets/frontend/img/coming_soon.jpg')}}" alt="" class="img-fluid w-100 shadow-lg">
                        @endif
						</figure>
					</div>
					<div class="col-md-8">
						<h3 class="mb-3 title-text"> {{ $info->name." ".$info->surname }} </h3>
						<p>Sed gravida, urna quis tempus sollicitudin, tellus urna suscipit nisl, id rhoncus ligula elit condi Suspendisse lacinia, risus et porta dignissim</p>
						
						<blockquote>Sed gravida, urna quis tempus sollicitudin, tellus urna suscipit nisl, id rhoncus ligula elit condi Suspendisse lacinia, risus et porta dignissim, elit tellus iaculis tellus, eget efficitur elit magna ellus tempor consectetur magna.
						<span></span>
						</blockquote>
						<div class="clearfix"></div>
				<div class="sportsmagazine-tags">
					<a href="404.html">#pcs</a>
					<a href="404.html">#mario kart</a>
					<a href="404.html">#pc1</a>
					<a href="404.html">#pc2</a>
				</div>
				
			</div>
					<div class="container">
					<ul class="row align-items-center justify-content-start social-icons list-unstyled">
                     <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
                     <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
                     <li><a class="youtube" href="#"><i class="fa fa-youtube-play"></i></a></li>
                     <li><a class="pinterest" href="#"><i class="fa fa-pinterest-p"></i></a></li>
                  </ul>
				 </div>
				
				
				</div>
				
				<hr class="mt-4"/>
				
            </div>
            </div><!-- Start About page -->	
		</section>
			
		


      <div class="clearfix"></div>


@endsection
