<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>E-Sports</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <link rel="shortcut icon" href="{{asset('assets/frontend/img/favicon.png')}}">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="{{asset('assets/frontend/vendors/font-awesome/css/font-awesome.min.css')}}">
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="{{asset('assets/frontend/vendors/bootstrap/css/bootstrap.min.css')}}">
        <!-- Owl carousel CSS-->
        <link rel="stylesheet" href="{{asset('assets/frontend/vendors/owl-carousel/owl.carousel.min.css')}}">
        
        <!-- Stylesheet-->      
        <link rel="stylesheet" href= "{{asset('assets/frontend/css/reset.css')}}" />
        <link rel="stylesheet" href= "{{asset('assets/frontend/css/default.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/frontend/css/about.css')}}" />
        <link rel="stylesheet" href="{{asset('assets/frontend/css/jquery-ui.min.css')}}" />
        
		
		<!-- Timepicker css lib -->  
        <link rel="stylesheet" href="{{asset('assets/celebrity/css/jquery.timepicker.min.css' ) }}">
        
        <!-- Sweet alert css lib -->  
		<link rel="stylesheet" href="{{asset('assets/user/css/sweetalert.css' ) }}">

        <!-- Add fancyBox -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
        <!-- Favicon-->
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <script type="text/javascript">
            var calUrl = '{{ route( 'get_calendar' ) }}';
            var calCelebGamesUrl = '{{ route( 'get_celebrities_games' ) }}';
            var calByGamesUrl = '{{ route( 'get_by_games' ) }}';
            var calByCelebritysUrl = '{{ route( 'get_by_celebrities' ) }}';
            var celebrityCalendarDetailUrl = '{{ route( 'get_celebrity_calendar_detail' ) }}';
            var token = '{{ csrf_token() }}';
        </script>
    </head>

    <body data-spy="scroll" data-target="#myScrollspy" data-offset="1">

        <!-- LOADER -->
        <div id="preloader" style="display:none;">
            <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
         </div>
        <!-- END LOADER -->
        <div class="wrapper">
            @include('includes.frontendheader')
                @yield('content')
            @include('includes.frontendfooter')
        </div>
    </body>
</html>