<!doctype html>
<html lang="en">
<head>
	<title>{{$title}} - Admin Panel</title>
    <meta name="description" content="">
	<!-- Twitter meta-->
	<meta property="twitter:card" content="">
	<meta property="twitter:site" content="">
	<meta property="twitter:creator" content="">
	<!-- Open Graph Meta-->
	<meta property="og:type" content="website">
	<meta property="og:site_name" content="Admin Panel">
	<meta property="og:title" content="">
	<meta property="og:url" content="">
	<meta property="og:image" content="">
	<meta property="og:description" content="">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Main CSS-->
	<link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
	<!-- Font-icon css-->
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
	<script src="{{asset('js/jquery.validate.min.js')}}"></script>
	<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

</head>
<body  class="hold-transition sidebar-mini app">
<div class="wrapper">
    @include('includes.adminheader')
    	@yield('content')
    @include('includes.adminfooter')
</div>
</body>
</html>