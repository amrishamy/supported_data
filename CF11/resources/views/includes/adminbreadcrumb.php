<div class="app-title">
  <div>
    <h1><i class="fa fa-th-list"></i> <?php echo $breadcrumbTitle; ?></h1>
    <!-- <p>Table to display analytical data effectively</p> -->
  </div>
  <ul class="app-breadcrumb breadcrumb side">
    <li class="breadcrumb-item"><a href='<?php echo route("admin.dashboard"); ?>'><i class="fa fa-home fa-lg"></i></a></li>
    <li class="breadcrumb-item"><?php echo $breadcrumbItem; ?></li>
    <li class="breadcrumb-item active"><a href="#"><?php echo $breadcrumbTitle; ?></a></li>
  </ul>
</div>