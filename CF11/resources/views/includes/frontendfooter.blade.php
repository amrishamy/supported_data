      <!-- Start Footer -->
<footer id="footer" class="footer text-center text-white">

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="footer-logo">
					<a href="{{ route('homepage') }}"><img src="{{asset('assets/frontend/img/f-logo.png')}}" alt="ESport" class="img-fluid"/></a>
				</div>
			</div>
			<div class="col-md-12 py-3 py-md-4">
				<ul class="row align-items-center flex-md-row flex-column justify-content-center footer-menu list-unstyled">
					<li class="nav-item @if($page == 'Home') active @endif">
					<a class="nav-link" href="{{ route('homepage') }}">Home</a>
					</li>
					<li class="nav-item @if($page == 'Games') active @endif">
					<a class="nav-link" href="{{ route('gamelist') }}">Games</a>
					</li>
					<li class="nav-item @if($page == 'Celebrity') active @endif">
					<a class="nav-link" href="{{ route('celebritieslist') }}">Celebrities</a>
					</li>
					<li class="nav-item @if($page == 'Celebrity calendar') active @endif">
					<a class="nav-link" href="{{ route('celebritiescalendar') }}">Celebrities Calendars</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="{{ route('homepage') }}#iamonline">I am Online</a>
					</li>
					<li class="nav-item @if($page == 'Live video') active @endif">
					<a class="nav-link" href="{{ route('livevideos') }}">Live Videos</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="{{ route('homepage') }}#topcelebrity">Top Celebrities</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="#">about</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="#">Contact us</a>
					</li>
				</ul>
			</div>
			<div class="container">
			<div class="row align-items-center justify-content-center flex-column flex-md-row pb-3 pb-md-4">
				<p class="pr-0 pb-2 pb-md-0 pr-md-5 text-uppercase mb-0">FOLLOW US ON</p>
				<ul class="row align-items-center justify-content-center social-icons list-unstyled">
					<li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a class="youtube" href="#"><i class="fa fa-youtube-play"></i></a></li>
					<li><a class="vimeo" href="#"><i class="fa fa-vimeo"></i></a></li>
					<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
				</ul>
			</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom py-4 text-white border-top">
		<div class="container">
			<p>Copyright © 2020 E-Sports. All Rights Reserved.</p>
		</div>
	</div>
</footer>

 <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
	  
	<!-- Start  Login Modal -->	  
	
<div class="modal fade modal_forms" id="loginForm">
	<div class="modal-dialog modal-md modal-dialog-centered">
		<div class="modal-content">
				<!-- Modal body -->
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h2 class="mb-4 text-center">Login</h2>
				<form id="login_frm" name="login_frm" action="{{ route('user.authenticate') }}" class="needs-validation" method="POST">
					{{ csrf_field() }}
					<div class="form-group">						 
						<input type="text" class="form-control" id="email" placeholder="Enter email" name="email" maxlength="50">
					</div>
					<div class="form-group">						 
						<input type="password" class="form-control" id="password" placeholder="Enter password" name="password" maxlength="50" />
					</div>
					<div class="form-group">
						<div class="row">
						<?php /*
							<div class="col">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="customCheck1">
									<label class="custom-control-label" for="customCheck1">Remember password</label>
									</div>
							</div>
						*/ ?>
							<div class="col">
									<a href="#forgotpassword"  class="modelChange float-right" data-target="#forgotpassword" hide-modal="#loginForm"><em>Forgot Password</em> <i class="fa fa-question-circle"></i></a>
							</div>
						</div>			  
					</div>
					<div class="form-group text-center">
						<button type="submit" class="btn btn-lg btn-red">Submit</button>
					</div>
					<div class="text-center">Don't have account? 
						<a class="modelChange" data-target="#signupForm" hide-modal="#loginForm" href="#loginForm"><strong>Create Account</strong></a>
					</div>	
				</form>	
			</div>	
		</div>
	</div>
</div>
    <!-- End  Login Modal -->
		<!-- Start  forgot screen Modal -->	  
	
	<div class="modal fade modal_forms" id="forgotpassword">
		<div class="modal-dialog modal-md modal-dialog-centered">
			<div class="modal-content">
				 <!-- Modal body -->
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="form-head mb-3">
						<h2 class="mb-2 text-left">forgot password</h2>
						<span class="text-muted">Enter your registered email to reset password.</span>
					</div>
					<form class="forget-form" id="forgotPasswordFrm" method="post" action="{{route('user.forgot_password') }}">
        				{{ csrf_field() }}
						<div class="form-group">						 
							<input type="text" class="form-control" id="forgot_email" placeholder="E.g. john@abc.com" name="forgot_email" value="" maxlength="50" >
							
						</div>
							
						<div class="text-center">
							<button type="submit" class="btn btn-lg btn-red">send reset instruction</button>
						</div>
					</form>	
				</div>	
			</div>
		</div>
	</div>
    <!-- End  forgot screen Modal -->
	<!-- Start  Login Modal -->	  
<div class="modal fade modal_forms" id="signupForm">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content">
				<!-- Modal body -->
			<div class="modal-body p-4 p-md-5">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h2 class="mb-4 text-center">Register Now!</h2>
				<form class="needs-validation" name="add_user_frm" id="add_user_frm" method="post" action="{{ route('user.register') }}" >
					{{ csrf_field() }}
					<div class="form-row">
						<div class="col-md-6">
							<div class="form-group">						 
								<input type="text" class="form-control alphabetsOnly" id="first_name" placeholder="First Name" name="first_name" value="{{ old('first_name') }}" maxlength="15" >
							</div>
						</div>
						<div class="col-md-6">							
							<div class="form-group">						 
								<input type="text" class="form-control alphabetsOnly" id="last_name" placeholder="Last Name" name="last_name" value="{{ old('last_name') }}" maxlength="15" >
							</div>
						</div>
					</div>

					<div class="form-row">
						<div class="col-md-6">
							<div class="form-group">						 
								<input type="text" class="form-control mobile_number" id="mobile_number" placeholder="Enter your mobile number" name="mobile_number" value="" minlength="7" maxlength="15" onkeypress="return isNumberOrDash(event);hide_error();" >
							</div>
						</div>
						<div class="col-md-6 gender-option">
							<div class="form-group align-items-center">		
								<label class="control-label" for="Gender">Gender: </label>							
								<label class="radio-inline" for="Gender-0">
								<input type="radio" name="gender" id="Gender-0" value="1" >
								Male
								</label> 
								<label class="radio-inline" for="Gender-1">
								<input type="radio" name="gender" id="Gender-1" value="2">
								Female
								</label>				  
							</div>
						</div>
					</div>	

					<div class="form-row">
						<div class="col-md-6">
							<div class="form-group">						 
								<input id="gamer_id" name="gamer_id" class="form-control" type="text" placeholder="Enter your gamer ID" >	
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">						 
								<input type="email" class="form-control" id="email" placeholder="Enter your email" name="email" value="{{ old('email') }}" maxlength="50" >
							</div>
						</div>
					</div>	
					
					<div class="form-row">
						<div class="col-md-6">
							<div class="form-group">						 
								<input type="password" class="form-control" id="password" placeholder="Password" name="password" maxlength="50">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">						 
								<input type="password" class="form-control" id="cpassword" placeholder="Confirm Password" name="cpassword" maxlength="15" >								  
							</div>
						</div>
					</div>						
					
					<div class="form-group">
						<div class="text-center my-3">
							<button type="submit" class="btn btn-lg btn-red">Submit</button>
						</div>
					</div>	
					
					<div class="text-center">Already have an account? 
						<a class="modelChange" data-target="#loginForm" hide-modal="#signupForm" href="#loginForm"><strong>Login</strong></a>
					</div>		
				</form>	
			</div>	
		</div>
	</div>
</div>

<!-- End  Login Modal -->

<!-- rate us Modal -->	  
<div class="modal fade modal_forms" id="rate-us">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
				<!-- Modal body -->
			<div class="modal-body p-4 p-md-5">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h2 class="mb-4 text-center">Rate us!!</h2>
				<form id="smileys">
					<input type="radio" name="smiley" value="sad" class="sad">
					<input type="radio" name="smiley" value="neutral" class="neutral">
					<input type="radio" name="smiley" value="happy" class="happy" checked="checked">
					
					<div class="pt-3">It looks like you're feeling <span id="result">happy</span></div>
				</form>
				
			</div>	
		</div>
	</div>
</div>

<!-- End  rate us Modal -->
<!-- Start  request Modal -->	  
	
<div class="modal fade modal_forms" id="requestnow">
	<div class="modal-dialog modal-md modal-dialog-centered">
		<div class="modal-content">
				<!-- Modal body -->
			<div class="modal-body">
				
			</div>	
		</div>
	</div>
</div>
<!-- End  request Modal -->

	<script src="{{asset('assets/frontend/vendors/jquery/jquery.min.js')}}"></script>
	<!-- <script src="{{asset('assets/frontend/vendors/bootstrap/js/popper.min.js')}}"></script>
	<script src="{{asset('assets/frontend/vendors/bootstrap/js/bootstrap.bundle.min.js')}}"></script> -->
	<script src="{{asset('js/bootstrap.min.js')}}"></script>
	<!-- Timepicker js lib -->  
	<script src="{{asset('assets/celebrity/js/jquery.timepicker.min.js' ) }}"></script>
	<!-- Add fancyBox -->
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
	<!-- Owl carousel js-->
	<script src="{{asset('assets/frontend/vendors/owl-carousel/owl.carousel.min.js')}}"> </script>
	<!-- Custom Js-->
	<script src="{{asset('assets/frontend/vendors/jquery/custom.js')}}"></script>
	<script src="{{asset('assets/frontend/vendors/jquery/bootstrap-datepicker.min.js')}}"></script>
	<script src="{{asset('assets/frontend/vendors/jquery/jquery-ui.min.js')}}"></script>
	<script src="{{asset('assets/user/js/jquery.validate.min.js')}}"></script>
	<script src="{{asset('assets/user/js/script.js')}}"></script>
	<script src="{{asset('assets/celebrity/js/additional-methods.min.js')}}"></script>

	<!-- Data table plugin-->
	<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
	<link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.dataTables.min.css">
	<script src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>

	<!-- Sweet alert css lib -->  
	<script src="{{asset('assets/user/js/sweetalert.js')}}"></script>
	<script src="{{asset('assets/user/js/booking_request.js')}}"></script>
	  

	  <script>
		  // Very simple JS for updating the text when a radio button is clicked
const INPUTS = document.querySelectorAll('#smileys input');

function updateValue(e) {
	document.querySelector('#result').innerHTML = e.target.value;
}

INPUTS.forEach(el => el.addEventListener('click', (e) => updateValue(e)));
		  </script>
		  @stack('scroll_login_pages')

