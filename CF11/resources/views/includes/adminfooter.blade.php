<!-- Essential javascripts for application to work-->

<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/script.js') }}"></script>

<!-- The javascript plugin to display page loading on top-->
<script src="{{asset('js/plugins/pace.min.js')}}"></script>
<!-- Page specific javascripts-->
<!-- Data table plugin-->
<script type="text/javascript" src="{{asset('js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/dataTables.bootstrap.min.js')}}"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.dataTables.min.css">
<script src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script> 

<script src="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.js"></script>
<link rel="stylesheet" href="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.css">
<script src="{{asset('js/main.js')}}"></script>

<script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script>

<script type="text/javascript">

                
  var emailDataTables = $('#emailDataTables').DataTable({
    'processing': true,
    'serverSide': true,
    'iDisplayLength': 10,
    'columnDefs': [ { orderable: false, targets: [3]}],
    'oLanguage': {'sProcessing': '<div class="datatable_loading" style="display:block;">Loading&#8230;</div>'},
    'ajax': {
        'url':'{{ route("admin.emailajaxdata") }}'
    }
  });
	var userTable = $('#userTable').DataTable({
    // "aaSorting": [],
	  'processing': true,
    'serverSide': true,
    'iDisplayLength': 10,
    "order": [ 4, "desc" ],
    'columnDefs': [ { orderable: false, targets: [0,1,2,3,4,5]}],
	  'oLanguage': {'sProcessing': '<div class="datatable_loading" style="display:block;">Loading&#8230;</div>'},
    'ajax': {
        'url':'{{ route("admin.userajaxdata") }}'
    }
});


  var celebrityTable = $('#celebrityTable').DataTable({

    'processing': true,
    'serverSide': true,
  	'iDisplayLength': 10,
  	"order": [[ 4, "desc" ]],
    'columnDefs': [ 
        { orderable: false, targets: [0,1,2,5]},
        { className: "text-center", targets: [5]}
        ],
    'oLanguage': {'sProcessing': '<div class="datatable_loading" style="display:block;">Loading&#8230;</div>'},
    'ajax': {
      'url':'{{ route("admin.celebrityajaxdata") }}'
    }
  });


  var gameTable = $('#gameTable').DataTable({
    'processing': true,
    'serverSide': true,
    'iDisplayLength': 10,
    "order": [ 4, "desc" ],
    'columnDefs': [ { orderable: false, targets: [0,5]}],
    'oLanguage': {'sProcessing': '<div class="datatable_loading" style="display:block;">Loading&#8230;</div>'},
    'ajax': {
        'url':'{{ route("admin.gameajaxdata") }}'
    }
});

var sliderTable = $('#sliderTable').DataTable({
    'processing': true,
    'serverSide': true,
    'iDisplayLength': 10,
    "order": [ 1, "asc" ],
	'columnDefs': [ { orderable: false, targets: [0,3,5]}],
    'oLanguage': {'sProcessing': '<div class="datatable_loading" style="display:block;">Loading&#8230;</div>'},
    'ajax': {
        'url':'{{ route("admin.slidersajaxdata") }}'
	},
	rowReorder: {
		enable: true,
		selector: 'td:nth-child(2)'
    },
});

sliderTable.on('row-reorder', function ( e, diff, edit ) { 
            
			var reOrderData = { "_token": "{{ csrf_token() }}", 'replace_ids':{} };
			
            if( diff.length > 0 ) { 
              $upOrDown = diff[0].oldPosition - diff[0].newPosition == 1?'down':'up';
              for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
                  var rowData = sliderTable.row( diff[i].node ).data( );

                  if( $upOrDown == 'down' ) {  
                    var $newIndex = ( i == 0 )?ien-1:i-1;
                  } else { 
                    var $newIndex = ( i == ien-1 )?0:i+1;
                  }

                  var replacedData = sliderTable.row( diff[$newIndex].node ).data( );
				//   console.log(replacedData);
                  reOrderData.replace_ids[ rowData[ 6 ] ] = replacedData[ 1 ];
              }

              $.ajax({
                  url     : '{{ route("admin.updateSlideImageReorder") }}',
                  type    : 'POST',
                  data    : reOrderData,
                  dataType: 'json',
                  error:  function ( xhr, status, error ) { 
                      swal( 'Error!', 'Unable to update order, please try again later.', 'error' );
                  },
                  success : function ( json ) { 
                      if( json.errors ){ 
                        swal( 'Error!', json.errors, 'error' );
                      } else { 
                        swal( 'Updated!', json.success, 'success' );
                        sliderTable.ajax.reload( null, false );
                      }
                  }
              });

            }

        });



function delete_row(rowid){
	let affected_data_model = $("#data_model").val();
	swal({
	  title: "Are you sure?",
	  text: "You will not be able to recover this record!",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonClass: "btn-danger",
	  confirmButtonText: "Yes, delete it!"
	}, function (isConfirm) {
		if (isConfirm) {
		  $.ajax({
	          type:'POST',
	          url:'{{ route("admin.deleterow") }}',
	          data:{"_token": "{{ csrf_token() }}", "rowid":rowid, "affected_data_model":affected_data_model},
	          success:function(data){
              let obj = JSON.parse(data);
              setTimeout(() => {
                $( 'body' ).removeClass( 'stop-scrolling' );
                if(obj.code==200){ 
                swal("Deleted!", obj.message, "success");
                emailDataTables.ajax.reload( null, false );
                userTable.ajax.reload( null, false );
                celebrityTable.ajax.reload( null, false );
                gameTable.ajax.reload( null, false );
                sliderTable.ajax.reload( null, false );
	            }else{
	                swal("Error Occured!",obj.message, "error");
              }
              
              }, 500);

	            
	          }

	      });	
		}

	});
}


$("#goBtt").click(function(){

    var selectedRowIds = [];
    $.each($("input[name='user_ids[]']:checked"), function(){            
        selectedRowIds.push($(this).val());
    });
    let actiontype = $("#actionDropdown").val(); let affected_data_model = $("#data_model").val();

    if(actiontype=="" || actiontype==null){
    	swal("Please select an action.");
    	return false;
    }else{
    	let checkboxLength = $("input[name='user_ids[]']:checked").length;
    	if(checkboxLength==0){
    	  swal("Please select at least one record.");
    	  return false;
    	}
    }
    swal({
	  title: "Are you sure you want to perform this action?",
	  text: "",
	  type: "warning",
	  confirmButtonClass: "btn-danger",
	  confirmButtonText: "Yes, do it!",
    showCancelButton: true,
    reverseButtons : false,
	}, function (isConfirm) {
		if (isConfirm) {
		  $.ajax({
	          type:'POST',
	          url:'{{ route("admin.updatebulkrows") }}',
	          data:{"_token": "{{ csrf_token() }}", "rowids":selectedRowIds, "actiontype":actiontype, "affected_data_model":affected_data_model},
	          success:function(data){
	            let obj = JSON.parse(data);
              $('input[type="checkbox"]').prop('checked', false);
              $('#actionDropdown').prop('selectedIndex',"");
              setTimeout(() => {
                $( 'body' ).removeClass( 'stop-scrolling' );
                if(obj.code==200){ 
                // alert( obj.code );
	            	// location.reload();
                swal("Done!", obj.message, "success");
                emailDataTables.ajax.reload( null, false );
                userTable.ajax.reload( null, false );
                celebrityTable.ajax.reload( null, false );
                gameTable.ajax.reload( null, false );
                sliderTable.ajax.reload( null, false );
	            }else{
	                swal("Error Occured!",obj.message, "error");
              }
              
              }, 500);
	            
	          }

	      });	
		}else{
      $('input[type="checkbox"]').prop('checked', false);
      $('#actionDropdown').prop('selectedIndex',"");
    }

	});


});


//var allPages = userTable.fnGetNodes();
      
$('body').on('click', '#checkAll', function () {
  if ($(this).hasClass('allChecked')) {
      $('input[type="checkbox"]').prop('checked', false);
  } else {
      $('input[type="checkbox"]').prop('checked', true);      
  }
  $(this).toggleClass('allChecked');
});

// Bind the single checkboxes with Bulk checkbox selection
$( document ).ready(function(){ 
  $(document).on('change', 'input[name="user_ids[]"]', function () {
        if ($(this).is(":checked")) { 
            var isAllChecked = 0;

            $('input[name="user_ids[]"]',document).each(function() {
              // alert( this.checked );
                if (!this.checked)
                    isAllChecked = 1;
            });
            
            if (isAllChecked == 0) {
                $("#checkAll").prop("checked", true);
            }     
        }
        else {
            $("#checkAll").prop("checked", false);
        }
    });

    $('.table').on( 'page.dt', function () { 
      $("#checkAll").prop("checked", false);
    } );

    $('.table').on( 'length.dt', function ( e, settings, len ) {
      $("#checkAll").prop("checked", false);
    } );

    $('.table').on( 'order.dt', function () {
      // This will show: "Ordering on column 1 (asc)", for example
      $("#checkAll").prop("checked", false);
    } );

});

CKEDITOR.replace( 'email_template' );

</script>
<!-- <script src="{{asset('js/script.js')}}"></script> -->

 <script type="text/javascript" src="{{asset('js/plugins/chart.js')}}"></script>
    <script type="text/javascript">
      var data = {
      	labels: ["January", "February", "March", "April", "May"],
      	datasets: [
      		{
      			label: "My First dataset",
      			fillColor: "rgba(220,220,220,0.2)",
      			strokeColor: "rgba(220,220,220,1)",
      			pointColor: "rgba(220,220,220,1)",
      			pointStrokeColor: "#fff",
      			pointHighlightFill: "#fff",
      			pointHighlightStroke: "rgba(220,220,220,1)",
      			data: [65, 59, 80, 81, 56]
      		},
      		{
      			label: "My Second dataset",
      			fillColor: "rgba(151,187,205,0.2)",
      			strokeColor: "rgba(151,187,205,1)",
      			pointColor: "rgba(151,187,205,1)",
      			pointStrokeColor: "#fff",
      			pointHighlightFill: "#fff",
      			pointHighlightStroke: "rgba(151,187,205,1)",
      			data: [28, 48, 40, 19, 86]
      		}
      	]
      };
      var pdata = [
      	{
      		value: 300,
      		color: "#46BFBD",
      		highlight: "#5AD3D1",
      		label: "Complete"
      	},
      	{
      		value: 50,
      		color:"#F7464A",
      		highlight: "#FF5A5E",
      		label: "In-Progress"
      	}
      ]
      
      var ctxl = $("#lineChartDemo").get(0).getContext("2d");
      var lineChart = new Chart(ctxl).Line(data);
      
      var ctxp = $("#pieChartDemo").get(0).getContext("2d");
      var pieChart = new Chart(ctxp).Pie(pdata);

    </script>