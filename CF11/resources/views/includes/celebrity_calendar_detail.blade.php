@if($filtered_cg_records->count() > 0 )
@foreach( $filtered_cg_records as $k=>$celebrityCalendar)
    <tr>
        <td>{{$k+1}}</td>
        <td class="img-icon">
        <img src="@if($celebrityCalendar->image) {{ asset('public').'/'.$celebrityCalendar->image }} @else {{ asset('assets/frontend/img/celbs.png')}} @endif" alt="" class="img-fluid rounded"/>
        </td>
        <td> {{ $celebrityCalendar->cname }} </td>
        <td> {{ $celebrityCalendar->gname }} </td>
        <!-- <td>pc1 </td> -->
        <td> {{ date('d M,Y', strtotime($celebrityCalendar->slot_date)) }} </td>
        <td> {{ $celebrityCalendar->slot_start_time }} </td>
        <!-- <td> {{ $celebrityCalendar->slot_end_time }} </td> -->
        <td>${{ $celebrityCalendar->slot_price }} </td>
        <td class="action-btn">
            @if($celebrityCalendar->requested_slot_id)
               @if($celebrityCalendar->gsr_status == 0 )
                  <a href="javascript:void(0);" class="btn btn-red float-right">Requested</a>
               @elseif($celebrityCalendar->gsr_status == 1 )
                  <a href="javascript:void(0);" class="btn btn-red float-right">Booked</a>
               @else
                  <a href="javascript:void(0);" class="btn btn-red float-right">Denied</a>
               @endif
            @else
               <a href="{{ route('user.check_user_login') }}" data-toggle="modal" data-target="" class="btn btn-red float-right request_now_btn" slot_id="{{ $celebrityCalendar->id }}">Request Now</a>
            @endif
        </td>                             
    </tr>
@endforeach
@endif
    