@if($filtered_cg_records->count() > 0 )
   @foreach( $filtered_cg_records as $k=>$celebrityCalendar)
   <li class="celebrity-block d-flex align-items-center p-2 rounded">
      <figure>
         <img src="@if($celebrityCalendar->image) {{ asset('public').'/'.$celebrityCalendar->image }} @else {{ asset('assets/frontend/img/celbs.png')}} @endif" alt="" class="img-fluid shadow-lg rounded"/>
      </figure>	
      <div class="d-flex justify-content-between align-items-center">
         <div class="celebrity-details">									
            <h5>{{ $celebrityCalendar->gname }}</h5>
            <small class="w-100">{{ $celebrityCalendar->cname }}</small>
            <span><strong>Slot Time: </strong>{{ date('d M,Y', strtotime($celebrityCalendar->slot_date))}}  {{ $celebrityCalendar->slot_start_time }} </span>
         </div>																	
         <div class="book-btn">
            @if($celebrityCalendar->requested_slot_id)
               @if($celebrityCalendar->gsr_status == 0 )
                  <a href="javascript:void(0);" class="btn btn-red btn-sm">Requested</a>
               @elseif($celebrityCalendar->gsr_status == 1 )
                  <a href="javascript:void(0);" class="btn btn-red btn-sm">Booked</a>
               @else
                  <a href="javascript:void(0);" class="btn btn-red btn-sm">Denied</a>
               @endif
            @else
               <a href="{{ route('user.check_user_login') }}" data-toggle="modal" data-target="" class="btn btn-red btn-sm request_now_btn" slot_id="{{ $celebrityCalendar->id }}">Request Now</a>
            @endif
         </div>
      </div>
   </li>
   @endforeach

<div class="game-btn text-right mt-4">		
   <a href="{{ route('celebritiescalendar') }}" class="btn btn-red btn-sm w-100">see more</a>
</div>
@endif