<style>
                     .load_celeb_and_games.running{opacity:0.3;}
                     .cal-date{ cursor:pointer; }
                  </style>
                  <nav>
                     <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true" data-wrap="#cel_form">Celebrities</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false" data-wrap="#game_form">Games</a>
                     </div>
                  </nav>
                  <div class="tab-content py-3 list-unstyled" id="nav-tabContent">
                     <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <form id="cel_form">							  
                              <div class="form-row">
                                 <div class="col-md-6 form-group">
                                    <select id="c_id" data-show-content="true" class="form-control border" onchange="getCelebritiesAndGames( false, '{{ $postedData[ 'date' ]}}','c' );">
                                          <option value="">All Celebrities</option>
                                          @foreach( $celebList as $celebrityId => $celebrityName )
                                             <option value="{{$celebrityId}}" data-content="{{$celebrityName}}" @if( isset( $postedData[ 'c_id' ] ) && $postedData[ 'c_id' ] && $postedData[ 'c_id' ] == $celebrityId ) selected @endif>{{$celebrityName}}</option>
                                          @endforeach
                                    </select>
                                 </div>
                                 
                                 <div class="col-md-6 form-group">
                                    <div class="input-group date" data-provide="">
										<?php /*
                                          <input type="time" class="form-control slot_time" id="timepick" placeholder="HH:MM" onblur="getCelebritiesAndGames( false, '{{ $postedData[ 'date' ]}}','c' );" @if( isset( $postedData[ 'time' ] ) && $postedData[ 'time' ] ) value="{{$postedData[ 'time' ]}}"@endif>
                                          <div class="input-group-addon">
                                             <span class="glyphicon glyphicon-th"></span>
                                          </div>
										*/ ?>
										<input class="form-control slot_time" onkeypress="return isNumberOrDash(event);hide_error();" type="text" id="timepick" placeholder="Select start time" data-step="5" data-max-time="23:55" data-show-2400="true" maxlength="5" autocomplete="off" onchange="getCelebritiesAndGames( false, '{{ $postedData[ 'date' ]}}','c' );" @if( isset( $postedData[ 'time' ] ) && $postedData[ 'time' ] ) value="{{$postedData[ 'time' ]}}"@endif>
                                    </div>
                                 </div>
                                 <!-- <div class="col-md-2 form-group">
                                    <div class="input-group date" data-provide="">
                                       <button class="btn-primary btn btn-reset" onclick="$( this ).parents('form')[0].reset(); getCelebritiesAndGames( false, '','c' ); return false;">Reset Filter</button>
                                    </div>
                                 </div> -->
                              </div>
                        </form>
                        <ul id="cel_wrap">
                        @include( 'includes.celebrities_and_games' )
                        </ul>
                     </div>
                     <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <form id="game_form">
                              <div class="form-row">
                                 <div class="col-md-6 form-group">
                                 <select id="g_id" data-show-content="true" class="form-control border" onchange="getCelebritiesAndGames( false, '{{ $postedData[ 'date' ]}}','g' );">
                                    <option value="">All Games</option>
                                    @foreach( $gamesList as $gameId => $gameName )
                                    <option value="{{$gameId}}" data-content="{{$gameName}}" @if( isset( $postedData[ 'g_id' ] ) && $postedData[ 'g_id' ] && $postedData[ 'g_id' ] == $gameId ) selected @endif>{{$gameName}}</option>
                                    @endforeach
                                 </select>
                                 </div>
                                 <div class="col-md-6 form-group">
                                 <div class="input-group date" data-provide="">
									<?php /*
										<input type="time" class="form-control slot_time" id="timepick" placeholder="HH:MM" onblur="getCelebritiesAndGames( false, '{{ $postedData[ 'date' ]}}','g' );" @if( isset( $postedData[ 'time' ] ) && $postedData[ 'time' ] ) value="{{$postedData[ 'time' ]}}"@endif>
											<div class="input-group-addon">
                                          <span class="glyphicon glyphicon-th"></span>
                                    </div>
									*/
									?>
									<input class="form-control slot_time" type="text" id="timepick" placeholder="Select start time" data-step="5" data-max-time="23:55" data-show-2400="true" maxlength="5" autocomplete="off" onchange="getCelebritiesAndGames( false, '{{ $postedData[ 'date' ]}}','g' );" @if( isset( $postedData[ 'time' ] ) && $postedData[ 'time' ] ) value="{{$postedData[ 'time' ]}}"@endif>
                                   
                                 </div>
                                 </div>
                              </div>
                        </form>
                        <ul id="game_wrap">
                           @include( 'includes.celebrities_and_games' )
                        </ul>
                     </div>	
                  </div>