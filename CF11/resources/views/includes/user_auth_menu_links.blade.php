@if (!Auth::guest( 'web' ))
@php
    $user = auth()->user();
@endphp
<li class="login-profile">
    <div class="dropdown">
    <!-- <a href="{{ route( 'user.dashboard' ) }}" class="d-flex"> -->
            <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="pr-2">Welcome {{$user->first_name}}</span>
        <i class="fa fa-user"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item {{ ($page=='Dashboard') ? 'active':''}}" href="{{ route('user.dashboard') }}">Dashboard</a>
                    <a class="dropdown-item {{ ($page=='Manage Profile') ? 'active':''}}" href="{{ route('user.profile') }}">My profile</a>
                    <a class="dropdown-item {{ ($page=='Change Password') ? 'active':''}}" href="{{ route('user.changepassword') }}">Change Password</a>
                    <a class="dropdown-item {{ ($page=='Requested Games') ? 'active':''}}" href="{{ route('user.requested_games') }}">Requested Games</a>
                    <a class="dropdown-item {{ ($page=='Upcoming Games') ? 'active':''}}" href="{{ route('user.upcoming_games') }}">Upcoming Games</a>
                    <a class="dropdown-item {{ ($page=='Games history') ? 'active':''}}" href="{{ route('user.games_history') }}">Games History</a>
                    <a class="dropdown-item" href="{{ route('user.logout') }}"> Logout</a>
                </div>
            <!-- </a> -->
        </div>
</li>
@else
<li class="login-modal">
<a href="#loginForm" data-toggle="modal" data-target="#loginForm" class="d-flex"><i class="fa fa-user"></i><span>Login</span></a>
</li>
<li class="cart-option d-flex">
<a href="#signupForm" data-toggle="modal" data-target="#signupForm" class="d-flex"><i class="fa fa fa-user-plus"></i><span>Register</span></a>
</li>
@endif