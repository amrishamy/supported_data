<div id="top">
    <header class="header">
        <nav class="navbar navbar-expand-lg bg-voilet title p-0 shadow-lg" id="myScrollspy">
            <a class="navbar-brand p-3" href="{{ route('homepage') }}"><img src="{{asset('assets/frontend/img/logo.png')}}" alt="ESport"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse p-3" id="navbarSupportedContent">
                <ul class="navbar-nav justify-content-around w-100">
                    <li class="home nav-item">
                    <a class="nav-link @if($page == 'Home') active @endif" href="{{ route('homepage') }}">Home</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link @if($page == 'Games') active @endif" href="@if($page == 'Home') #Gamesblock @else {{route( 'homepage' )}}#Gamesblock @endif">Games</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link @if($page == 'Celebrity') active @endif" href="{{ route('celebritieslist') }}">Celebrities</a>
                    </li>
                    <li class="Celebritycalendar nav-item">
                    <!--a class="nav-link" href="{{ route('celebritiescalendar') }}">Celebrities Calendars</a-->
                    <a class="nav-link @if($page == 'Celebrity calendar') active @endif" href="@if($page == 'Home') #Celebritycalendar @else {{route( 'homepage' )}}#Celebritycalendar @endif"> celebrity Calendar</a>
                    </li>
                    <li class="iamonline nav-item">
                    <a class="nav-link" href="@if($page == 'Home') #iamonline @else {{route( 'homepage' )}}#iamonline @endif">I am Online</a>
                    </li>

                    <li class="livevideos nav-item">
                    <a class="nav-link" href="@if($page == 'Home') #livevideos @else {{route( 'homepage' )}}#livevideos @endif">live Videos</a>
                    </li>

                    <li class="topcelebrity nav-item">
                    <a class="nav-link" href="@if($page == 'Home') #topcelebrity @else {{route( 'homepage' )}}#topcelebrity @endif">Top celebrity</a>
                    </li>
                </ul>
            </div>

            
            <ul class="list-unstyled login-cart pr-2" id="auth-links">
                @include('includes.user_auth_menu_links')
            </ul>
        
        </nav>
    </header>
</div>