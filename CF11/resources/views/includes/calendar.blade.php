<div class="calendar shadow bg-white text-center p-md-5 p-2">
    <div class="d-flex justify-content-between align-items-center mb-4">
        @if( $calObj[ 'previous_cal' ] )
            <a href="javascript:void( 0 );" onclick="getCal( '{{$calObj[ 'previous_cal' ]}}' ); return false;"><i class="fa fa-chevron-left"></i></a>
        @endif
        <h3 class="month mb-0 text-uppercase align-self-center w-100">{{$calObj[ 'month' ] . ' ' . $calObj[ 'year' ]}}</h3>
        @if( $calObj[ 'next_cal' ] )
            <a href="javascript:void( 0 );" onclick="getCal( '{{$calObj[ 'next_cal' ]}}' ); return false;"><i class="fa fa-chevron-right"></i></a>
        @endif
    </div>
    <ol class="day-names list-unstyled">
        @foreach( $calObj[ 'week_days' ] as $dayName )
            <li class="text-uppercase">{{$dayName}}</li>
        @endforeach
    </ol>

    <ol class="days list-unstyled">
        @for( $dayNumber = 1 - array_search( $calObj[ 'first_day' ], $calObj[ 'week_days' ] ); $dayNumber <= $calObj[ 'overall_days' ]; $dayNumber++ )
            @if( $dayNumber >= 1 )
                <li @if( $dayNumber < $calObj[ 'current_date' ] ) class="disable" @endif><div onclick="getCelebritiesAndGames( false, '{{ $calObj[ 'year' ] . '-' . $calObj[ 'numeric_month' ] . '-' . $dayNumber }}' );" class="date cal-date @if( $calObj[ 'current_date' ] && $dayNumber == $calObj[ 'current_date' ] ) active @endif">{{$dayNumber}}</div></li>
            @else
                <li class="outside"><div>&nbsp;</div></li>
            @endif
        @endfor
    </ol>
</div>	