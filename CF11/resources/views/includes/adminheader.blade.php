<!-- Navbar-->
  <header class="app-header">
    <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
	<a class="app-header__logo text-left" href="javascript:void(0);">Admin Panel</a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
      <!-- User Menu-->
      <li class="dropdown"><a class="app-nav__item" href="javascript:void(0);" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
        <ul class="dropdown-menu settings-menu dropdown-menu-right">
          <li><a class="dropdown-item" href="{{ route('admin.profile') }}"><i class="fa fa-user fa-lg"></i> Profile</a></li>
          <li><a class="dropdown-item" href="{{ route('admin.logout') }}"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
        </ul>
      </li>
    </ul>
  </header>



    <!-- Sidebar menu-->
  <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
  <aside class="app-sidebar">
    <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/48.jpg" alt="User Image">
      <div>
        <p class="app-sidebar__user-name">{{session('name')}}</p>
        <p class="app-sidebar__user-designation">Administrator</p>
      </div>
    </div>
    <ul class="app-menu">
      <li><a class="app-menu__item {{$breadcrumbItem == 'Dashboard' ? 'active' : ''}}" href="{{ route('admin.dashboard') }}"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
     <!-- <li><a class="app-menu__item" href="{{ route('admin.emailslist') }}"><i class="app-menu__icon fa fa-envelope"></i><span class="app-menu__label">Email Templates</span></a></li> -->
     <li><a class="app-menu__item {{$breadcrumbItem == 'Manage Celebrities' ? 'active' : ''}}" href="{{ route('admin.celebrityListing') }}"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Manage Celebrities</span></a></li>
     <li><a class="app-menu__item {{$breadcrumbItem == 'Manage Users' ? 'active' : ''}}" href="{{ route('admin.userlist') }}"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Manage Users</span></a></li>     
     <li><a class="app-menu__item {{$breadcrumbItem == 'Manage Games' ? 'active' : ''}}" href="{{ route('admin.games') }}"><i class="app-menu__icon fa fa-handshake-o"></i><span class="app-menu__label">Manage Games</span></a></li>
     <li><a class="app-menu__item {{$breadcrumbItem == 'Manage Slider' ? 'active' : ''}}" href="{{ route('admin.sliders') }}"><i class="app-menu__icon fa fa-gear"></i><span class="app-menu__label">Manage Slider</span></a></li>
     <!--<li><a class="app-menu__item" href="{{ route('admin.userlist') }}"><i class="app-menu__icon fa fa-gear"></i><span class="app-menu__label">Settings</span></a></li>-->
    </ul>
  </aside>