@extends('layouts.default')
@section('content')
 <section class="material-half-bg">
      <div class="cover"></div>
    </section>
    <section class="login-content">
      <div class="logo">
        <h1>Logo</h1>
      </div>
      	<?php
      	$flipped_class = "";
      	if(Session::get('flipped_class')){ $flipped_class = "flipped"; } ?>

        @if ($message = Session::get('error'))
        <div class="login-box" style="min-height:50px;max-height:50px;background:transparent;box-shadow: none;">
        <div class="alert alert-danger alert-block" >
          <button type="button" class="close" data-dismiss="alert">×</button> 
                <span>{{ $message }}</span>
        </div></div>
        @endif

        @if ($message = Session::get('forgot_error'))
        <div class="login-box" style="min-height:50px;max-height:50px;background:transparent;box-shadow: none;">
        <div class="alert alert-danger alert-block" >
          <button type="button" class="close" data-dismiss="alert">×</button> 
                <span>{{ $message }}</span>
        </div></div>
        @endif

        @if ($message = Session::get('forgot_success'))
        <div class="login-box" style="min-height:50px;max-height:50px;background:transparent;box-shadow: none;">
        <div class="alert alert-success alert-block" >
          <button type="button" class="close" data-dismiss="alert">×</button> 
                <span>{{ $message }}</span>
        </div></div>
        @endif


        <div class="login-box {{ $flipped_class }}">

        <form class="login-form" id="login_frm" method="post" action="{{ route('admin.authenticate') }}">
        	{{ csrf_field() }}
          <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>SIGN IN</h3>
          <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            <label class="control-label">EMAIL</label>
            <input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off" maxlength="50" />
            {!! $errors->first('email', '<p class="validation-errors">:message</p>') !!}
          </div>
          <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            <label class="control-label">PASSWORD</label>
            <input class="form-control" type="password" name="password" placeholder="Password" maxlength="50" />
            {!! $errors->first('password', '<p class="validation-errors">:message</p>') !!}
          </div>
          <div class="form-group">
            <div class="utility">
              <div class="animated-checkbox">
                <label>
                  <input type="checkbox"><span class="label-text">Stay Signed in</span>
                </label>
              </div>
              <p class="semibold-text mb-2"><a href="#" data-toggle="flip">Forgot Password ?</a></p>
            </div>
          </div>
          <div class="form-group btn-container">
            <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN IN</button>
          </div>
        </form>

        <form class="forget-form" method="post" action="{{route('admin.forgot_password') }}">
        	{{ csrf_field() }}
          <h3 class="login-head"><i class="fa fa-lg fa-fw fa-lock"></i>Forgot Password ?</h3>
          <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            <label class="control-label">EMAIL</label>
            <input class="form-control" type="text" name="forgot_email" value="" placeholder="Email" maxlength="50" />
            {!! $errors->first('email', '<p class="validation-errors">:message</p>') !!}
          </div>
          <div class="form-group btn-container">
            <button class="btn btn-primary btn-block"><i class="fa fa-unlock fa-lg fa-fw"></i>RESET</button>
          </div>
          <div class="form-group mt-3">
            <p class="semibold-text mb-0"><a href="#" data-toggle="flip"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
          </div>
        </form>

      </div>
   </section>
   <script type="text/javascript">

   </script>
@stop
