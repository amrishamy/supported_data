@extends('layouts.admin')
@section('content')
	<main class="app-content">
      @include('includes.adminbreadcrumb')
      
      <?php
        $types = explode(",", $game['type']);
      ?>
      
      <div class="row">
        <div class="col-md-12">
          <form name="user_frm" enctype="multipart/form-data" id="edit_game_frm" method="post" action="{{ route('admin.updategame') }}">
           {{ csrf_field() }}

           <input type="hidden" name="game_id" id="game_id" value="{{$game['id']}}" />

            <div class="tile">
              <!-- <h3 class="tile-title">Vertical Form</h3> -->
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">    
                      <label for="first_name">Name:</label>
                      <input type="text" class="form-control" placeholder="Enter Name" name="name" value="{{$game['name']}}" maxlength="60" />
                      {!! $errors->first('name', '<p class="validation-errors">:message</p>') !!}
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-{{ $game['image']?8:12 }}">
                  <div class="form-group">    
                      <label for="first_name">Upload Slide Image</label>
                      <input name="image" type="file" class="form-control" >
                      {!! $errors->first('image', '<p class="validation-errors">:message</p>') !!}
                  </div>
                </div>
                @if( $game['image'])
                <div class="col-md-4">
                  <div class="form-group">    
                      <span class="img-wrap">
                        <img src="{{ asset( "public/".$game['image'] )  }}" alt="" width="100%" />
                        <a href="#" onclick="return deleteImage( '{{route( 'admin.delete_game_image', Hashids::encode( $game['id'] ) )}}', this );" title="Click here to delete image"><i class="fa fa-times" aria-hidden="true"></i></a>
                      </span>
                  </div>
                </div>
                @endif
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">    
                  <label for="game_time">Game Time (In minutes):</label>
                  <input type="text" class="form-control" placeholder="Enter Game Time" name="game_time" value="{{ $game['game_time'] }}"/>
                  {!! $errors->first('game_time', '<p class="validation-errors">:message</p>') !!}
                  </div>
                </div>
              </div>
              
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                  <label for="last_name">Platform Type:</label>
                  <select class="form-control" name="type[]" multiple>
                    @foreach($platforms as $k=>$v)
                      <option value={{$k}} <?php if(in_array($k, $types)) { echo 'selected'; } ?> > {{ $v }} </option>
                    @endforeach
                  </select>
                   {!! $errors->first('type', '<p class="validation-errors">:message</p>') !!}
                  </div>
                </div>
              </div>


            </div>
            <div class="tile-footer">
              <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>&nbsp;&nbsp;&nbsp;
              <!-- <a class="btn btn-secondary" href="{{ route('admin.games') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a> -->
            </div>
          </div>

          </form>

        </div>
        
        <div class="clearix"></div>
        
      </div>
    </main>
  
@endsection