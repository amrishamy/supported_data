@extends('layouts.admin')
@section('content')
  
  <main class="app-content">
      @include('includes.adminbreadcrumb')
      
      <div class="row">
        <div class="col-md-12">
          <form name="user_frm" enctype="multipart/form-data" id="add_user_frm" method="post" action="{{ route('admin.savegame') }}">
           {{ csrf_field() }}
            <div class="tile">
              <!-- <h3 class="tile-title">Vertical Form</h3> -->
              <div class="form-group">    
                  <label for="first_name">Name:</label>
                  <input type="text" class="form-control" placeholder="Enter Name" name="name" maxlength="60" />
                  {!! $errors->first('name', '<p class="validation-errors">:message</p>') !!}
              </div>

              <div class="form-group">    
                  <label for="first_name">Upload Game Image</label>
                  <input name="image" type="file" class="form-control" >
                  {!! $errors->first('image', '<p class="validation-errors">:message</p>') !!}
              </div>

              <div class="form-group">    
                  <label for="game_time">Game Time (In minutes):</label>
                  <input type="number" class="form-control" placeholder="Enter Game Time" name="game_time"  />
                  {!! $errors->first('game_time', '<p class="validation-errors">:message</p>') !!}
              </div>

              <div class="form-group">
                  <label for="last_name">Platform Type:</label>
                  <select class="form-control" name="type[]" multiple>
                    @foreach($platforms as $k=>$v)
                    <option value={{$k}}> {{ $v }} </option>
                    
                    @endforeach
                  </select>
                   {!! $errors->first('type', '<p class="validation-errors">:message</p>') !!}
              </div>                     
            </div>
            <div class="tile-footer">
              <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add</button>&nbsp;&nbsp;&nbsp;<!-- <a class="btn btn-secondary" href="{{ route('admin.userlist') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a> -->
            </div>
          </div>

          </form>

        </div>
        
        <div class="clearix"></div>
        
      </div>


    </main>
 <!-- /.content-wrapper -->
@endsection
