@extends('layouts.admin')
@section('content')
  
  <main class="app-content">
      @include('includes.adminbreadcrumb')
      
      <div class="row">
        <div class="col-md-12">
          <form name="user_frm" enctype="multipart/form-data" id="add_slide_frm" method="post" action="{{ route('admin.saveslide') }}">
           {{ csrf_field() }}
            <div class="tile">
              <!-- <h3 class="tile-title">Vertical Form</h3> -->
              <div class="form-group">    
                  <label for="first_name">Title:</label>
                  <input type="text" class="form-control" placeholder="Enter slide title" name="title" maxlength="60" value="{{ old('title') }}" />
                  {!! $errors->first('title', '<p class="validation-errors">:message</p>') !!}
              </div>

              <div class="form-group">    
                  <label for="first_name">Upload Slide Image</label>
                  <input name="image" type="file" class="form-control" >
                  {!! $errors->first('image', '<p class="validation-errors">:message</p>') !!}
                  <br/>
                  <strong><i>Valid format ( jpeg, png )</i></strong>
              </div>

                                   
            </div>
            <div class="tile-footer">
              <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>
            </div>
          </div>

          </form>

        </div>
        
        <div class="clearix"></div>
        
      </div>


    </main>
 <!-- /.content-wrapper -->
@endsection
