@extends('layouts.admin')
@section('content')
	<main class="app-content">
      @include('includes.adminbreadcrumb')
      
      
      <div class="row">
        <div class="col-md-12">
          <form name="user_frm" enctype="multipart/form-data" id="edit_slide_frm" method="post" action="{{ route('admin.updateslide') }}">
           {{ csrf_field() }}

           <input type="hidden" name="slide_id" id="slide_id" value="{{$details['id']}}" />

            <div class="tile">
              <!-- <h3 class="tile-title">Vertical Form</h3> -->
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">    
                      <label for="first_name">Title:</label>
                      <input type="text" class="form-control" placeholder="Enter slide title" name="title" value="{{$details['title']}}" maxlength="60" />
                      {!! $errors->first('title', '<p class="validation-errors">:message</p>') !!}
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-{{ $details['image']?8:12 }} upload-edit-image">
                  <div class="form-group">    
                      <label for="first_name">Upload Slider Image <i>(Note: Click on image to edit/replace slider image)<i></label>
					<div class="edit-slide-image">  
                      <input name="image" type="file" class="form-control file-upload" >
                      {!! $errors->first('image', '<p class="validation-errors">:message</p>') !!}
             
					</div>
                      <strong><i>Valid format ( jpeg, png )</i></strong>
                  </div>
  @if( $details['image'])
                      <span class="img-wrap">
                        <img src="{{ asset( "public/".$details['image'] )  }}" alt="" width="100%" />
                        <a href="#" onclick="return deleteImage( '{{route( 'admin.delete_slider_image', Hashids::encode( $details['id'] ) )}}', this );" title="Click here to delete image"><i class="fa fa-times" aria-hidden="true"></i></a>
                      </span>
                @endif
                </div>
              
              </div>
              

            </div>
            <div class="tile-footer">
              <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>
            </div>
          </div>

          </form>

        </div>
        
        <div class="clearix"></div>
        
      </div>
    </main>
  
@endsection