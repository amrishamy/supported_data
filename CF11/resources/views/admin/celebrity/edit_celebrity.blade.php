@extends('layouts.admin')
@section('content')
	
	<main class="app-content">
      @include('includes.adminbreadcrumb')
      
      <div class="row">
        <div class="col-md-12">
          <form name="user_frm" id="edit_user_frm" method="post" action="{{ route('admin.updateCelebrity') }}">
           {{ csrf_field() }}
          <div class="tile">
            <!-- <h3 class="tile-title">Vertical Form</h3> -->
            <div class="tile-body">
                <input type="hidden" name="userid" value="{{$user['id']}}" />

                <div class="row">
                  <div class="col-md-6 col-12 form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                    <label class="control-label">Name <span class="required-fields">*</span></label>
                    <input class="form-control" type="text" value="{{ old('name')!='' ? old('name') : $user['name'] }}" placeholder="e.g. John" name="name" />
                    {!! $errors->first('name', '<p class="validation-errors">:message</p>') !!}
                  </div>

                  <div class="col-md-6 col-12 form-group {{ $errors->has('surname') ? 'has-error' : ''}}">
                    <label class="control-label">Surname </label>
                    <input class="form-control" type="text" value="{{ old('surname')!='' ? old('surname') : $user['surname'] }}" placeholder="e.g. Doe" name="surname" />
                    {!! $errors->first('surname', '<p class="validation-errors">:message</p>') !!}
                  </div>
                </div>

                <div class="row">
                <div class="col-md-6 col-12 form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                  <label class="control-label">Email <span class="required-fields">*</span></label>
                  <input class="form-control" type="text" value="{{ old('email')!='' ? old('email') : $user['email'] }}" placeholder="e.g. john@abc.com" name="email" />
                  {!! $errors->first('email', '<p class="validation-errors">:message</p>') !!}
                </div>  
                <!-- add gamer id -->
                  <div class="col-md-6 col-12 form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                  <label class="control-label">Password <span class="required-fields">*</span></label>
                  <input class="form-control" type="password" placeholder="Enter password" name="password" />
                  {!! $errors->first('password', '<p class="validation-errors">:message</p>') !!}
                </div>  
                </div> 

                <div class="row">
                  <div class="col-md-6 col-12 form-group {{ $errors->has('username') ? 'has-error' : ''}}">
                    <label class="control-label">Username <span class="required-fields">*</span></label>
                    <input class="form-control" type="text" value="{{ old('username')!='' ? old('username') : $user['username'] }}" placeholder="e.g. john_doe" name="username" />
                    {!! $errors->first('username', '<p class="validation-errors">:message</p>') !!}
                  </div>


                  <div class="col-md-6 col-12 form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
                    <label class="control-label">Mobile Number <span class="required-fields">*</span></label>
                    <input class="form-control" type="number" value="{{ old('mobile')!='' ? old('mobile') : $user['mobile'] }}" placeholder="e.g. +123 456 789" name="mobile" />
                    {!! $errors->first('mobile', '<p class="validation-errors">:message</p>') !!}
                  </div>
                </div>

                <div class="row">
                   <div class="col-md-6 col-12 form-group {{ $errors->has('twitter') ? 'has-error' : ''}}">
                    <label class="control-label">Twitter Username<span class="required-fields">*</span></label>
                    <input class="form-control" type="text" value="{{ old('twitter')!='' ? old('twitter') : $user['twitter'] }}" placeholder="e.g. twitter" name="twitter" />
                    {!! $errors->first('twitter', '<p class="validation-errors">:message</p>') !!}
                  </div>

                  <div class="col-md-6 col-12 form-group {{ $errors->has('skype') ? 'has-error' : ''}}">
                    <label class="control-label">Skype Username</label>
                    <input class="form-control" type="text" value="{{ old('skype')!='' ? old('skype') : $user['skype'] }}" placeholder="e.g. skype" name="skype" />
                    {!! $errors->first('skype', '<p class="validation-errors">:message</p>') !!}
                  </div>
                </div>

                <div class="form-group {{ $errors->has('gamer_id') ? 'has-error' : ''}}">
                  <label class="control-label">Gamer ID <span class="required-fields">*</span></label>
                  <input class="form-control" type="text" value="{{ old('gamer_id')!='' ? old('gamer_id') : $user['gamer_id'] }}" placeholder="e.g. xyz_123" name="gamer_id" />
                  {!! $errors->first('gamer_id', '<p class="validation-errors">:message</p>') !!}
                </div>
               
            </div>
            <div class="tile-footer">
              <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>&nbsp;&nbsp;&nbsp;<!-- <a class="btn btn-secondary" href="{{ route('admin.celebrityListing') }}"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a> -->
            </div>
          </div>

          </form>

        </div>
        
        <div class="clearix"></div>
        
      </div>


    </main>
 <!-- /.content-wrapper -->
@endsection