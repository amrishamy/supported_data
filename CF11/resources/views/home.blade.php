@extends('layouts.frontend')

@section('content')
<!--Carousel Wrapper-->
@if( $sliders->count() > 0 )
<div id="carousel-example-2" class="carousel slide carousel-fade slider-banner text-white" data-ride="carousel">
  <!--Indicators-->
  <ol class="carousel-indicators">
    @foreach($sliders as $k=>$slider)
    <li data-target="#carousel-example-2" data-slide-to="{{ $k }}" class="{{ $k == 0 ? 'active':'' }}"></li>

    @endforeach
  </ol>
  <!--/.Indicators-->
  <!--Slides-->
  <div class="carousel-inner" role="listbox">
    
    @foreach($sliders as $k=>$slider)
    <div class="carousel-item {{ $k == 0 ? 'active':'' }}">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="{{ asset('public').'/'.$slider->image }}"
          alt="Third slide">
        <div class="mask rgba-black-slight"></div>
      </div>
      <div class="carousel-caption">
         <div class="slider-text">
               <div class="d-inline-block title-bg position-relative p-3 p-md-4 ml-auto">
                  <h1> {{ $slider->title }} </h1>
               </div>
            </div>
      </div>
    </div>
    @endforeach
  </div>
  <!--/.Slides-->
    <!--Controls-->
<a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
<i class="fa fa-angle-left"></i>
<span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
<i class="fa fa-angle-right"></i>
<span class="sr-only">Next</span>
</a>
                <!--/.Controls-->
</div>
@else
    
  <div id="carousel-example-2" class="carousel slide carousel-fade slider-banner text-white" data-ride="carousel">
  <!--Indicators-->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-2" data-slide-to="" class="active"></li>
  </ol>
  <!--/.Indicators-->
  <!--Slides-->
  <div class="carousel-inner" role="listbox">
    
    <div class="carousel-item active">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="{{ asset('assets/frontend/img/banner-img.jpg')}}"
          alt="Third slide">
        <div class="mask rgba-black-slight"></div>
      </div>
      <div class="carousel-caption">
         <div class="slider-text">
               <div class="d-inline-block title-bg position-relative p-3 p-md-4 ml-auto">
                  <h1> CF11 E-sports </h1>
               </div>
            </div>
      </div>
    </div>
  </div>
  <!--/.Slides-->

</div>

@endif
<!--/.Carousel Wrapper-->

<div class="clearfix"></div>
      <!-- game-section start -->	
      @if( $games->count() > 0 )	
      <section class="game-section bg-pattern" id="Gamesblock">
         <div class="container">
            <div class="text-center mb-5">
               <h2 class="text-white text-title">Games</h2>
            </div>
           <div class="owl-carousel game-block">
                @foreach($games as $k=>$game)
                <div class="item w-100">
                  <div class="select-game bg-white rounded thumbnail">
                     <div class="calender-section">
                        <figure>
                           @if($game->image)
                           <img src="{{ asset('public').'/'.$game->image }}" alt="" class="img-fluid w-100 shadow-lg">
                           @else
                           <img src="{{ asset('assets/frontend/img/coming_soon.jpg')}}" alt="" class="img-fluid w-100 shadow-lg">
                           @endif
                        </figure>
                        <!--<div class="d-flex bg-red align-items-center calender-block">
                           <img src="{{asset('assets/frontend/img/icons/calender-white.png')}}" alt="" class="icon-img img-fluid ">10 Jul 2019
                        </div>-->
                     </div>
                     <summary class="p-3 caption">
					      <h3>{{ $game->name }}</h3>
                            <?php 
                            $pfs = array();
                            $AvailablePF = explode(',', $game->type ) ;
                            foreach($platforms as $k=>$v)
                            {
                                if(in_array($k,$AvailablePF ) )
                                    $pfs[] =  $v ;
                            }
                            ?>
                        <div class="game-detail border-bottom pb-2 mb-2 mb-2">
                          <figure class="flex-text">
						  <!--img src="{{asset('assets/frontend/img/icons/game.svg')}}" alt="" class="icon-img"--> 
						  @foreach( $pfs as $k=>$val)
						  <span class="badge badge-danger mt-2 mr-2"> {{ $val }}</span>	
						  @endforeach
						  </figure>				
                        </div>
							
                        <div class="game-btn text-center my-2 my-md-3">		
                           <a href="{{ route('celebritiescalendar') }}" class="btn btn-red btn-sm w-100">Find Celebs To Play</a>
                        </div>
                     </summary>
                  </div>
                </div>
                @endforeach
               
            </div>
			<div class="game-btn text-center mt-5">		
               <a href="{{ route('gamelist') }}" class="btn btn-red btn-sm w-100">see more</a>
            </div>
         </div>
      </section>
      @endif
      <!-- game-section End-->
      <div class="clearfix"></div>
	  <!-- celebrity calendar start-->
      <section class="celebrity-calender" id="Celebritycalendar">
         <div class="container">
			<div class="text-center mb-5">
               <h2 class="text-white text-title">celebrity calendar</h2>
            </div>
			 <div class="row">		
          <div class="col-lg-5 col-md-12 left-section" id="add-cal">
						@include( 'includes.calendar' )
				  </div>				 
				  <div class="col-lg-7 col-md-12 right-section load_celeb_and_games" id="tabs">
               @include( 'includes.complete_calendar_section' )
             </div>
			   </div>
		 </div>
	   </section>
	   <!-- game-section End-->
	    <div class="clearfix"></div>
      <!-- I am online start -->
      <section class="celebrity-section bg-pattern" id="iamonline">
         <div class="container">
            <div class="text-center mb-5">
               <h2 class="text-white text-title">i am online</h2>
            </div>
            <div class="owl-carousel online-celebrity">
               <div class="item w-100">
                  <div class="online-player bg-white rounded">
                     <div class="calender-section">
                        <figure>
                           <img src="{{asset('assets/frontend/img/online-people/img-1.jpg')}}" alt="" class="img-fluid w-100 shadow-lg">
                        </figure>
                        <div class="d-flex align-items-center bg-red calender-block">
                           $50
                        </div>
                        <span class="dot bg-success shadow-lg"></span>
                     </div>
                     <summary class="p-3 bg-white">
                        <h3>Julia Roberts</h3>
                         <span>Fifa League</span>
                        <div class="game-detail border-bottom pb-2 mb-2">
						    <span class="badge badge-danger mt-2 mr-2">
						   <!--img src="{{asset('assets/frontend/img/icons/game.svg')}}" alt="" class="icon-img"-->
						   PS4</span>
                        </div>
                      <!--<div class="slot-details text-center">Game hours:<span>30 minutes</span></div>-->
                        <div class="game-btn text-center mt-2">		
                           <a href="#" class="btn btn-red btn-sm w-100">Pay now</a>
                        </div>
                     </summary>	
                  </div>
               </div>
               <div class="item w-100">
                  <div class="online-player bg-white rounded">
                     <div class="calender-section">
                        <figure>
                           <img src="{{asset('assets/frontend/img/online-people/img-2.jpg')}}" alt="" class="img-fluid w-100 shadow-lg">
                        </figure>
                        <div class="d-flex align-items-center bg-red calender-block">
                           $50
                        </div>
                        <span class="dot bg-success shadow-lg"></span>
                     </div>
                     <summary class="p-3 bg-white">
                        <h3>Julia Roberts</h3> 
                         <span>Fifa League</span>
                        <div class="game-detail border-bottom pb-2 mb-2">
						    <span class="badge badge-danger mt-2 mr-2">
						   <!--img src="{{asset('assets/frontend/img/icons/game.svg')}}" alt="" class="icon-img"-->
						   PS4</span>
                        </div>
                      <!--<div class="slot-details text-center">Game hours:<span>30 minutes</span></div>-->
                        <div class="game-btn text-center mt-2">		
                           <a href="#" class="btn btn-red btn-sm w-100">Pay now</a>
                        </div>
                     </summary>	
                  </div>
               </div>
               <div class="item w-100">
                  <div class="online-player bg-white rounded">
                     <div class="calender-section">
                        <figure>
                           <img src="{{asset('assets/frontend/img/online-people/img-3.jpg')}}" alt="" class="img-fluid w-100 shadow-lg">
                        </figure>
                        <div class="d-flex align-items-center bg-red calender-block">
                           $50
                        </div>
                        <span class="dot bg-secondary ml-2 shadow-lg"></span>
                     </div>
                     <summary class="p-3 bg-white">
                      <h3>Tom Hardy</h3>
                         <span>Fifa League</span>
                        <div class="game-detail border-bottom pb-2 mb-2">
						    <span class="badge badge-danger mt-2 mr-2">
						   <!--img src="{{asset('assets/frontend/img/icons/game.svg')}}" alt="" class="icon-img"-->
						   PS4</span>
                        </div>
                <!--<div class="slot-details text-center">Game hours:<span>30 minutes</span></div>-->
                        <div class="game-btn text-center mt-2">		
                           <a href="#" class="btn btn-red btn-sm w-100">Pay now</a>
                        </div>
                     </summary>	
                  </div>
               </div>
               <div class="item w-100">
                  <div class="online-player bg-white rounded">
                     <div class="calender-section">
                        <figure>
                           <img src="{{asset('assets/frontend/img/online-people/img-4.jpg')}}" alt="" class="img-fluid w-100 shadow-lg">
                        </figure>
                        <div class="d-flex align-items-center bg-red calender-block">
                           $50
                        </div>
                        <span class="dot bg-secondary ml-2 shadow-lg"></span>
                     </div>
                     <summary class="p-3 bg-white">
                        <h3>Jennifer Lopez</h3>
                         <span>Fifa League</span>
                        <div class="game-detail border-bottom pb-2 mb-2">
						    <span class="badge badge-danger mt-2 mr-2">
						   <!--img src="{{asset('assets/frontend/img/icons/game.svg')}}" alt="" class="icon-img"-->
						   PS4</span>
                        </div>
                   <!--<div class="slot-details text-center">Game hours:<span>30 minutes</span></div>-->
                        <div class="game-btn text-center mt-2">		
                           <a href="#" class="btn btn-red btn-sm w-100">Pay now</a>
                        </div>
                     </summary>	
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- I am online End-->
      <div class="clearfix"></div>
      <!-- live games start -->
      <section class="live-videos" id="livevideos">
         <div class="container">
            <div class="text-center mb-5">
               <h2 class="text-white text-title">Live videos</h2>
            </div>
            <div class="owl-carousel live-games">
               <div class="item w-100">
                  <a href="https://www.youtube.com/embed/tgbNymZ7vqY" data-fancybox="gallery" data-caption="Caption #1" class="bg-white">
                     <div class="calender-section rounded">
                        <figure>
                           <img src="{{asset('assets/frontend/img/live-games/game-1.jpg')}}" alt="" class="img-fluid"/>
                        </figure>
                        <span class="play_icon"><img src="{{asset('assets/frontend/img/icons/play_icon.svg')}}" alt=""/></span>							              
                     </div>
                     <summary class="py-3 text-white">
                        <h4 class="d-flex align-items-center mb-2">Julia Roberts</h4>
                        <p>FIFA LEAGUE 2019 TOURNAMENT</p>
                     </summary>
                  </a>
               </div>
               <div class="item w-100">
                  <a href="https://www.youtube.com/embed/tgbNymZ7vqY" data-fancybox="gallery" data-caption="Caption #1" class="bg-white">
                     <div class="calender-section rounded">
                        <figure>
                           <img src="{{asset('assets/frontend/img/live-games/game-2.jpg')}}" alt="" class="img-fluid"/>
                        </figure>
                        <span class="play_icon"><img src="{{asset('assets/frontend/img/icons/play_icon.svg')}}" alt=""/></span>	                    
                     </div>
                     <summary class="py-3 text-white">
                        <h4 class="d-flex align-items-center mb-2">Tom Hardy</h4>
                        <p>FIFA LEAGUE 2019 TOURNAMENT</p>
                     </summary>
                  </a>
               </div>
               <div class="item w-100">
                  <a href="https://www.youtube.com/embed/tgbNymZ7vqY" data-fancybox="gallery" data-caption="Caption #1" class="bg-white">
                     <div class="calender-section rounded">
                        <figure>
                           <img src="{{asset('assets/frontend/img/live-games/game-3.jpg')}}" alt="" class="img-fluid"/>
                        </figure>
                        <span class="play_icon"><img src="{{asset('assets/frontend/img/icons/play_icon.svg')}}" alt=""/></span>	                     
                     </div>
                     <summary class="py-3 text-white">
                        <h4 class="d-flex align-items-center mb-2">Jennifer Lopez</h4>
                        <p>FIFA LEAGUE 2019 TOURNAMENT</p>
                     </summary>
                  </a>
               </div>
               <div class="item w-100">
                  <a href="https://www.youtube.com/embed/tgbNymZ7vqY" data-fancybox="gallery" data-caption="Caption #1" class="bg-white">
                     <div class="calender-section rounded">
                        <figure>
                           <img src="{{asset('assets/frontend/img/live-games/game-4.jpg')}}" alt="" class="img-fluid"/>
                        </figure>
                        <span class="play_icon"><img src="{{asset('assets/frontend/img/icons/play_icon.svg')}}" alt=""/></span>	                    
                     </div>
                     <summary class="py-3 text-white">
                        <h4 class="d-flex align-items-center mb-2">Julia Roberts</h4>
                        <p>FIFA LEAGUE 2019 TOURNAMENT</p>
                     </summary>
                  </a>
               </div>
              
            </div>
			 <div class="game-btn text-center mt-5">		
               <a href="live-videos" class="btn btn-red btn-sm w-100">see more</a>
            </div>
         </div>
         </div>
      </section>
      <!-- live game End-->
      <div class="clearfix"></div>
      <!-- past games start -->
      <section class="live-videos bg-pattern">
         <div class="container">
            <div class="text-center mb-5">
               <h2 class="text-white text-title">Recent Games</h2>
            </div>
            <div class="most-recent-cat">
			  <div class="form-group dropdown">
				<select class="form-control">
					<option>Most Recent</option>
					<option>Action</option>
					<option>Another action</option>
					<option>Something else here</option>
				</select>
				</div>              
            </div>
            <div class="owl-carousel live-games">
               <div class="item w-100">
                  <a href="https://www.youtube.com/embed/tgbNymZ7vqY" data-fancybox="gallery" data-caption="Caption #1" class="bg-white">
                     <div class="calender-section rounded">
                        <figure>
                           <img src="{{asset('assets/frontend/img/live-games/game-1.jpg')}}" alt="" class="img-fluid"/>
                        </figure>
                        <span class="play_icon"><img src="{{asset('assets/frontend/img/icons/play_icon.svg')}}" alt=""/></span>							              
                     </div>
                  </a>
               </div>
               <div class="item w-100">
                  <a href="https://www.youtube.com/embed/tgbNymZ7vqY" data-fancybox="gallery" data-caption="Caption #1" class="bg-white">
                     <div class="calender-section rounded">
                        <figure>
                           <img src="{{asset('assets/frontend/img/live-games/game-2.jpg')}}" alt="" class="img-fluid"/>
                        </figure>
                        <span class="play_icon"><img src="{{asset('assets/frontend/img/icons/play_icon.svg')}}" alt=""/></span>	                    
                     </div>
                  </a>
               </div>
               <div class="item w-100">
                  <a href="https://www.youtube.com/embed/tgbNymZ7vqY" data-fancybox="gallery" data-caption="Caption #1" class="bg-white">
                     <div class="calender-section rounded">
                        <figure>
                           <img src="{{asset('assets/frontend/img/live-games/game-3.jpg')}}" alt="" class="img-fluid"/>
                        </figure>
                        <span class="play_icon"><img src="{{asset('assets/frontend/img/icons/play_icon.svg')}}" alt=""/></span>	                     
                     </div>
                  </a>
               </div>
               <div class="item w-100">
                  <a href="https://www.youtube.com/embed/tgbNymZ7vqY" data-fancybox="gallery" data-caption="Caption #1" class="bg-white">
                     <div class="calender-section rounded">
                        <figure>
                           <img src="{{asset('assets/frontend/img/live-games/game-4.jpg')}}" alt="" class="img-fluid"/>
                        </figure>
                        <span class="play_icon"><img src="{{asset('assets/frontend/img/icons/play_icon.svg')}}" alt=""/></span>	                    
                     </div>
                  </a>
               </div>
               <div class="item w-100 ">
                  <a href="https://www.youtube.com/embed/tgbNymZ7vqY" data-fancybox="gallery" data-caption="Caption #1" class="bg-white">
                     <div class="calender-section rounded">
                        <figure>
                           <img src="{{asset('assets/frontend/img/live-games/game-5.jpg')}}" alt="" class="img-fluid"/>
                        </figure>
                        <span class="play_icon"><img src="{{asset('assets/frontend/img/icons/play_icon.svg')}}" alt=""/></span>	                  
                     </div>
                  </a>
               </div>
               <div class="item w-100">
                  <a href="https://www.youtube.com/embed/tgbNymZ7vqY" data-fancybox="gallery" data-caption="Caption #1" class="bg-white">
                     <div class="calender-section rounded">
                        <figure>
                           <img src="{{asset('assets/frontend/img/live-games/game-6.jpg')}}" alt="" class="img-fluid"/>
                        </figure>
                        <span class="play_icon"><img src="{{asset('assets/frontend/img/icons/play_icon.svg')}}" alt=""/></span>	                   
                     </div>
                  </a>
               </div>
            </div>
           
         </div>
         </div>
      </section>
      <!-- past game End-->
      <div class="clearfix"></div>
      <!-- top-celebrity start -->
      <section class="top-celebrity" id="topcelebrity">
         <div class="container">
            <div class="text-center mb-5">
               <h2 class="text-white text-title">Top celebrity</h2>
            </div>
            <div class="owl-carousel live-games">
                @foreach($topCelebrities as $celebrity)
               <div class="item w-100">
                  <div class="fame-people bg-white rounded">
                     <div class="calender-section">
                        <figure>
                           @if($celebrity->image)
                           <img src="{{ asset('public').'/'.$celebrity->image }}" alt="" class="img-fluid w-100 shadow-lg">
                           @else
                           <img src="{{ asset('assets/frontend/img/coming_soon.jpg')}}" alt="" class="img-fluid w-100 shadow-lg">
                           @endif
                        </figure>
                        <div class="d-flex align-items-center bg-red calender-block">
                           <img src="{{asset('assets/frontend/img/icons/game.svg')}}" alt="" class="icon-img">PS4
                        </div>
                        <!-- <span class="award-icon"> -->
                        <!-- <img src="img/icons/award.png" alt="" class="img-fluid"> -->
                        <!-- </span>                       -->
                     </div>
                     <summary class="p-3 bg-white">
                        <h3>{{ $celebrity->name." ".$celebrity->surname }}</h3>
                      
                     </summary>
                  </div>
               </div>
                @endforeach
            </div>
         </div>
         </div>
      </section>
      <!-- top-celebrity End-->
      <div class="clearfix"></div>


@endsection
