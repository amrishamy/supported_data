@extends('layouts.frontend')

@section('content')

<div class="inner-banner text-white">
  <img src="img/contact-us/contact_banner.jpg" alt="E-Sport" class="img-fluid w-100 shadow-lg">            
  <div class="innerbanner_summary">
    <h1>Reset <span>password</span></h1>
  </div>          
</div>

<div class="clearfix"></div>

<section class="forms_screens">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-12 m-auto">
        <div class="bg-white p-3 p-md-5">
          <div class="form-head mb-3">
            <h2 class="mb-2 text-left">Reset password</h2>
            <span class="text-muted">Reset your password by entering your new password and confirm password in below fields. </span>
          </div>

          @if ($message = Session::get('success'))
          <div class="login-box" style="min-height:50px;max-height:50px;background:transparent;">
            <div class="alert alert-success alert-block" >
          <button type="button" class="close" data-dismiss="alert">×</button>	
                <strong>Password updated successfully. Go to <a class="modelChange" data-target="#loginForm" hide-modal="#signupForm" href="#loginForm" >Login</a></strong>
          </div></div>
          @endif

          @if ($message = Session::get('error'))
            <div class="login-box" style="min-height:50px;max-height:50px;background:transparent;">
            <div class="alert alert-danger alert-block" >
            <button type="button" class="close" data-dismiss="alert">×</button>	
                  <span>{{ $message }}</span>
            </div></div>
          @endif
    
          <form class="login-form" method="post" action="{{ route('user.update_password') }}" >
            {{ csrf_field() }}
            <div class="form-row">
              <div class="col-md-12">
                <div class="form-group">						 
                  <input type="password" class="form-control" id="password" placeholder="Password" name="password" >	
                  {!! $errors->first('password', '<p class="validation-errors">:message</p>') !!}							  
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">						 
                  <input type="password" class="form-control" id="cpassword" placeholder="Confirm Password" name="cpassword" >
                  {!! $errors->first('cpassword', '<p class="validation-errors">:message</p>') !!}								  
                </div>
              </div>
            </div>	
            <div class="text-center">
              <button type="submit" class="btn btn-lg btn-red">Submit</button>
            </div>
          </form>	
        </div>
      </div>
    </div>
  </div>
</section>
<!-- forgot password End-->
<div class="clearfix"></div>


@endsection
