@extends('layouts.frontend')

@section('content')
<div class="inner-banner text-white">
			<img src="{{asset('assets/img/contact-us/contact_banner.jpg')}}" alt="E-Sport" class="img-fluid w-100 shadow-lg">            
            <div class="innerbanner_summary">
				<h1> <span>{{ $page }}</span></h1>
			</div>          
        </div>
<div class="clearfix"></div>
<section class="form-screen">
	<div class="container">
		<div class="border p-1">  
			<div class="bg-white">
				<div class="row no-gutters">
					@include('user.includes.left_menu')
					<div class="col-md-8 profile-right-tab">
						<div id="" class="">
							<div class="tab-pane active" id="account" role="tabpanel">							
								<div class="account-list">
									<div class="container">
										<div class="row">
											<div class="col-md-6">
												<a href="{{route( 'user.upcoming_games' )}}" class="account-detail bg-white shadow-lg p-2  p-md-4">
													<i class="icon bg-danger">
														<i class="fa fa-gamepad"></i>
													</i>
													<div class="text-right">
														<h2>{{ $count_booked_slots }}</h2>
														<p>Booked Slots</p>
													</div>													
												</a>
											</div>
											<div class="col-md-6">
												<a href="{{route( 'user.requested_games' )}}" class="account-detail bg-white shadow-lg p-2  p-md-4">
													<i class="icon bg-dark">
														<i class="fa fa-gamepad"></i>
													</i>
													<div class="text-right">
														<h2>{{ $count_requested_slots }}</h2>
														<p>Requested Slots</p>
													</div>													
												</a>
											</div>
											<div class="col-md-6">
												<a href="{{route( 'user.games_history' )}}" class="account-detail bg-white shadow-lg p-2  p-md-4">
													<i class="icon bg-secondary">
														<i class="fa fa-gamepad"></i>
													</i>
													<div class="text-right">
														<h2>{{ $count_played_games }}</h2>
														<p>Games Played</p>
													</div>													
												</a>
											</div>
											
										</div>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
        
    <div class="clearfix"></div>

  @endsection
  @push('scroll_login_pages')
		<script type="text/javascript" src="{{asset('assets/user/js/scroll_page_custom.js')}}"></script>
	@endpush