<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/user/css/main.css') }}">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>{{$title}} - Panel</title>
    <script src="{{ asset('assets/user/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('assets/user/js/jquery.validate.min.js') }}"></script>

  </head>
  <body>

	@yield('content')

	<!-- Essential javascripts for application to work-->

    <script src="{{ asset('assets/user/js/popper.min.js') }}"></script>
    <script src="{{ asset('assets/user/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/user/js/main.js') }}"></script>
    <script src="{{ asset('assets/user/js/script.js') }}"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="{{ asset('assets/user/js/plugins/pace.min.js') }}"></script>
    <script type="text/javascript">
      // Login Page Flipbox control
      $('.login-content [data-toggle="flip"]').click(function() {
      	$('.login-box').toggleClass('flipped');
      	return false;
      });

      function fetchstates (country ,stateId=0){
        //alert();
        var url = "<?=url('/').'/user/loadstates/'?>"+country+"/"+stateId;
        $.ajax({
          type: "GET",
          url: url,
          success: function(result){
            $("#state_tab").html(result);
          }
        });
      }
    </script>
  </body>
</html>