@extends('layouts.frontend')

@section('content')
<div class="inner-banner text-white">
	<img src="{{asset('assets/img/contact-us/contact_banner.jpg')}}" alt="E-Sport" class="img-fluid w-100 shadow-lg">            
        <div class="innerbanner_summary">
			<h1> <span>{{ $page }}</span></h1>
		</div>          
</div>
<div class="clearfix"></div>
<section class="form-screen">
	<div class="container">
    	<div class="border p-1">  
			<div class="bg-white">
				<div class="row no-gutters">
					@include('user.includes.left_menu')
					<div class="col-md-8 profile-right-tab">
						<div id="" class="">
							<div class="tab-pane" id="profile" role="tabpanel">
								
                                <form class="form" name="password_frm" id="update_password_frm" method="post" action="{{ route('user.updatepassword') }}">
                                {{ csrf_field() }}
                                @if ($message = Session::get('password_error'))
                                <div class="alert alert-danger alert-block" style="margin-bottom: 10px;">
                                    <button type="button" class="close" data-dismiss="alert">×</button> 
                                    <span>{{ $message }}</span>
                                </div>
                                @endif
                                @if ($message = Session::get('password_success'))
                                <div class="alert alert-success alert-block" style="margin-bottom: 10px;">
                                    <button type="button" class="close" data-dismiss="alert">×</button> 
                                    <span>{{ $message }}</span>
                                </div>
                                @endif
		  
									<div class="form-row">								
										<div class="col-md-12">
											<div class="form-group">								  
												<label for="password">Current Password</label>
                                                <input type="password" placeholder="Enter password" name="old_password" maxlength="15" class="form-control" id="old_password" >
                                                {!! $errors->first('password', '<p class="validation-errors">:message</p>') !!}
											</div>
										</div>
                                    </div>
                                    <div class="form-row">	
										<div class="col-md-12">
											<div class="form-group">								  
												<label for="password2">New Password</label>
                                                <input type="password" placeholder="Enter password" name="password" id="password" maxlength="15" class="form-control" >
                                                {!! $errors->first('password', '<p class="validation-errors">:message</p>') !!}
											</div>
										</div>
                                    </div>
                                    <div class="form-row">	
										<div class="col-md-12">
											<div class="form-group">								  
												<label for="password2">Confirm Password</label>
                                                <input type="password" placeholder="Enter password" name="cpassword" maxlength="15"  class="form-control" >
                                                {!! $errors->first('password', '<p class="validation-errors">:message</p>') !!}
											</div>
										</div>
									</div>
									
									<div class="form-row">
									   <div class="col-md-12 pl-3">										 
											<div class="form-group">
												<button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Update Password</button>
												
											</div>
										</div>	
									</div>

								</form>					  
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
	</div>
</section>
        
    <div class="clearfix"></div>

  @endsection
  @push('scroll_login_pages')
		<script type="text/javascript" src="{{asset('assets/user/js/scroll_page_custom.js')}}"></script>
	@endpush