<form action="{{route('user.bookings_list_ajax')}}" class="srch-frm" id="filter_form" name="filter_form" method="POST">							  
    <div class="form-row justify-content-end filter_row">
        {{csrf_field()}}
        <input type="hidden" name="booking_request_for" value="{{$booking_request_for}}" />
        <div class="col-md-3 col-12 form-group">
            <select id="celeb_id" data-show-content="true" class="form-control border" name="celeb_id">
            <option value="">All Celebrities</option>
            @foreach( $celebList as $celebrityId => $celebrityName )
                <option value="{{$celebrityId}}" data-content="{{$celebrityName}}" >{{$celebrityName}}</option>
            @endforeach
                
                
            </select>
        </div>
        <div class="col-md-2 col-12 form-group">
            <select id="game_id" data-show-content="true" class="form-control border" name="game_id">
            <option value="">All Games</option>
            @foreach( $gamesList as $gameId => $gameName )
            <option value="{{$gameId}}" data-content="{{$gameName}}" >{{$gameName}}</option>
            @endforeach
            
            </select>
        </div>
        <div class="col-md-2 col-12 form-group">
            <select id="pltfrm_id" class="form-control border" name="pltfrm_id">
            <option value="">All Platforms</option>
            @foreach($platforms as $k=>$v)
                <option value="{{ $k }}">{{$v}}</option>
            @endforeach   
            </select>
        </div>
        <div class="col-md-3 col-12 form-group">
            <div class="input-group date bg-white" data-provide="">
                <input type="text" name="slot_date" class="slot_date form-control" placeholder="Select date" readonly />
            </div>
        </div>
        <div class="col-md-2 col-12 form-group">
            
            <input class="form-control slot_time" type="text" id="timepick" placeholder="Select start time" data-step="5" data-max-time="23:55" data-show-2400="true" maxlength="5" autocomplete="off" name="start_time" />
        </div>
    </div>
</form>