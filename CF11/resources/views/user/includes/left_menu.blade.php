<div class="col-md-2 profile-left-tab">					
    <ul class="tab nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="{{ ($page=='Dashboard') ? 'active':''}}"  href="{{ route('user.dashboard') }}" role="tab" aria-controls="account">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="{{ ($page=='Manage Profile') ? 'active':''}}" href="{{ route('user.profile') }}" role="tab" aria-controls="profile">My profile  </a>
        </li>
        <li class="nav-item">
            <a class="{{ ($page=='Change Password') ? 'active':''}}" href="{{ route('user.changepassword') }}" role="tab" aria-controls="profile"> Change Password  </a>
        </li>						 
        <li class="nav-item">
            <a class="{{ ($page=='Requested Games') ? 'active':''}}"  href="{{ route('user.requested_games') }}" role="tab" aria-controls="Requested">Requested Games</a>
        </li>
        <li class="nav-item">
            <a class="{{ ($page=='Upcoming Games') ? 'active':''}}"  href="{{ route('user.upcoming_games') }}" role="tab" aria-controls="Upcoming">Upcoming Games</a>
        </li>
        <li class="nav-item">
            <a class="{{ ($page=='Games History') ? 'active':''}}" href="{{ route('user.games_history') }}" role="tab" aria-controls="history">Games History</a>
        </li>
    </ul>
</div>