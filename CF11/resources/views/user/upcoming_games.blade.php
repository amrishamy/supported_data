@extends('layouts.frontend')

@section('content')
<div class="inner-banner text-white">
	<img src="{{asset('assets/img/contact-us/contact_banner.jpg')}}" alt="E-Sport" class="img-fluid w-100 shadow-lg">            
        <div class="innerbanner_summary">
			<h1> <span>{{ $page }}</span></h1>
		</div>          
</div>
<div class="clearfix"></div>
<section class="form-screen">
	<div class="container">
    	<div class="border p-1">  
			<div class="bg-white">
				<div class="row no-gutters">
					@include('user.includes.left_menu')
					<div class="col-md-10 profile-right-tab">
						<div id="" class="">
                            <div class="tab-pane" id="Upcoming" role="tabpanel"><div class="game-listing">
                                <div class="search-listing filters">
                                    @include('user.user_filters', ['booking_request_for' => 2 ] ) 
                                </div>
								<div class="table-border">
								    <div class="table-responsive">
                                    <table class="table table-hover table-bordered text-left border" id="upcomingGamesTable" style="width:100%">
                                            <thead>
                                                <tr>
                                                <th>S.No.</th>                         
                                                    <th>Celebrity</th> 
      
                                                    <th>Game Name ( plateform )</th>                         
                                                    <th>Date</th>                         
                                                    <th>Start time</th>                         
                                                    <th>End Time</th>                         
                                                    <th>Amount</th>
                                                    <!--th class="text-center">Status</th-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                                
                                            </tbody>
                                        </table>
								    </div>
								</div>
							</div>
					    </div>
				    </div>
			    </div>
            </div>
        </div>
    </div>
</section>
        
    <div class="clearfix"></div>

  @endsection
  @push('scroll_login_pages')
        <script type="text/javascript">
            var upcomingGamesTable = $('#upcomingGamesTable').DataTable({
                'processing': true,
                "searching": false, 
                "bLengthChange": false,
                'serverSide': true,
                'iDisplayLength': 10,
                "order": [ 1, "asc" ],
                'columnDefs': [ { orderable: false, targets: [0,1,2,3,4,5,6]}],
                'oLanguage': {'sProcessing': '<div class="datatable_loading" style="display:block;">Loading&#8230;</div>'},
                'ajax': {
                    'url':'{{route("user.bookings_list_ajax")}}',
                    'data': function(data){
                        var celeb_id = $('#celeb_id').val();
                        data.celeb_id = celeb_id;
                        var game_id = $('#game_id').val();
                        data.game_id = game_id;
                        var pltfrm_id = $('#pltfrm_id').val();
                        data.pltfrm_id = pltfrm_id;
                        var slot_date = $('.slot_date').val();
                        data.slot_date = slot_date;
                        var slot_time = $('.slot_time').val();
                        data.slot_time = slot_time;
                        var booking_request_for = $('input[name="booking_request_for"]').val();
                        data.booking_request_for = booking_request_for;
                    }
                }
            });
        </script>
        <script type="text/javascript" src="{{asset('assets/user/js/scroll_page_custom.js')}}"></script>
	@endpush