@extends('layouts.frontend')

@section('content')
<div class="inner-banner text-white">
	<img src="{{asset('assets/img/contact-us/contact_banner.jpg')}}" alt="E-Sport" class="img-fluid w-100 shadow-lg">            
        <div class="innerbanner_summary">
			<h1> <span>{{ $page }}</span></h1>
		</div>          
</div>
<div class="clearfix"></div>
<section class="form-screen">
	<div class="container">
    	<div class="border p-1">  
			<div class="bg-white">
				<div class="row no-gutters">
					@include('user.includes.left_menu')
					<div class="col-md-8 profile-right-tab">
						<div id="" class="">
							<div class="tab-pane" id="profile" role="tabpanel">
								<form class="form" name="profile_frm" enctype="multipart/form-data" id="profile_update_frm" method="post" action="{{ route('user.profileupdate') }}" >
									{{ csrf_field() }}
									@if ($message = Session::get('success'))
									<div class="alert alert-success alert-block" style="margin-bottom: 10px;">
										<button type="button" class="close" data-dismiss="alert">×</button> 
										<span>{{ $message }}</span>
									</div>
									@endif
									@if ($message = Session::get('error'))
									<div class="alert alert-danger alert-block" style="margin-bottom: 10px;">
										<button type="button" class="close" data-dismiss="alert">×</button> 
										<span>{{ $message }}</span>
									</div>
									@endif
		  
									<div class="form-row">
										<div class="col-md-6">
											<div class="form-group">							  
											  <label for="first_name">First name</label>
											  <input type="text" class="form-control alphabetsOnly" name="first_name" id="first_name" placeholder="first name" title="enter your first name if any." maxlength="15" value="{{ old('first_name')!='' ? old('first_name') : $profile['first_name'] }}" required >
											  {!! $errors->first('first_name', '<p class="validation-errors">:message</p>') !!}
											</div>
										</div>
										
										<div class="col-md-6">
											<div class="form-group">									  
												<label for="last_name">Last name</label>
												<input type="text" class="form-control alphabetsOnly" name="last_name" id="last_name" placeholder="last name" title="enter your last name if any." maxlength="15" value="{{ old('last_name')!='' ? old('last_name') : $profile['last_name'] }}" >
										  	</div>
										</div>
									</div>	
									
									<div class="form-row align-items-center">
										<div class="col-md-6">
											<div class="form-group">
											 <label for="mobile">Mobile</label>
											  <input type="text" class="form-control" name="mobile_number" id="mobile_number" placeholder="enter mobile number" title="enter your mobile number if any." minlength="7" maxlength="15" value="{{ old('mobile_number')!='' ? old('mobile_number') : $profile['mobile_number'] }}" onkeypress="return isNumberOrDash(event);hide_error();" >
										  </div>
										</div>

										<div class="col-md-6 gender-option">
											<div class="form-group mb-0">		
												<label class="control-label" for="Gender">Gender: </label>							
												<label class="radio-inline" for="Gender-0">
												<input type="radio" name="gender" id="Gender-0" value="1" @if ($profile->gender == 1 )  checked @endif >
												Male
												</label> 
												<label class="radio-inline" for="Gender-1">
												<input type="radio" name="gender" id="Gender-1" value="2" @if ($profile->gender == 2 )  checked @endif >
												Female
												</label>				  
											</div>
										</div>
					  
										
									</div>
									
									<div class="form-row">
										<div class="col-md-6">
											<div class="form-group">								  
											  <label for="email">Email</label>
											  <input type="email" class="form-control" name="email" id="email" placeholder="you@email.com" title="enter your email." maxlength="30" value="{{ old('email')!='' ? old('email') : $profile['email'] }}" readonly required >
											</div>
										</div>
										 
										<div class="col-md-6">
											<div class="form-group">								  
											  <label for="email">Gamer ID</label>
											  <input type="text" class="form-control" id="gamer_id" name="gamer_id" placeholder="Gamer ID" title="enter gamer id" maxlength="25" value="{{ old('gamer_id')!='' ? old('gamer_id') : $profile['gamer_id'] }}" >
											</div>
										</div>
									</div>
									
									<!-- <div class="form-row">								
										<div class="col-md-6">
											<div class="form-group">								  
												<label for="password">Password</label>
												<input type="password" class="form-control" name="password" id="password" placeholder="password" title="enter your password.">
											</div>
										</div>
										
										<div class="col-md-6">
											<div class="form-group">								  
												<label for="password2">Confirm Password</label>
												<input type="password" class="form-control" name="password2" id="password2" placeholder="password2" title="enter your password2.">
											</div>
										</div>
									</div> -->
									
									<div class="form-row">
									   <div class="col-md-12 pl-3">										 
											<div class="form-group">
												<button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Update</button>
												
											</div>
										</div>	
									</div>

								</form>					  
							</div>
						</div>
					</div>
				</div>
            </div>
        </div>
	</div>
</section>
        
    <div class="clearfix"></div>

  @endsection
  @push('scroll_login_pages')
		<script type="text/javascript" src="{{asset('assets/user/js/scroll_page_custom.js')}}"></script>
	@endpush
