$(document).ready(function() {

   $('.alphabetsOnly').keydown(function (e) {
      // if (e.shiftKey || e.ctrlKey || e.altKey) {
         if ( e.altKey) {
          e.preventDefault();
      } else {
          var key = e.keyCode;
          if (!((key == 8) || (key == 9) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
              e.preventDefault();
          }
      }
    });

    $("#add_slide_frm").validate({
      rules: {
          title: {required:true},
          image: {
              required:true,
              extension: "jpg|png|jpeg|gif"
            }
        },
        messages: {
          
          title: 'The title is required',
          image: {
              required: 'The image is required',
              extension: 'Please select valid image format.'
            }
          
        },
      
      submitHandler: function(form) {
        form.submit();
      }
    });

    $("#edit_slide_frm").validate({
      rules: {
          title: {required:true},
          image: { 
              extension: "jpg|png|jpeg|gif"
            }
        },
        messages: {
          title: 'The title is required',
          image: {
            extension: 'Please select valid image format.'
          }
      },
      
      submitHandler: function(form) {
        form.submit();
      }
    });

    $("#login_frm").validate({
      rules: {
         email: {
            required: true,
            email: true,  //add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
         },
         password: {required:true, minlength: 6}
        },
        messages: {
	    	email:{
	    		required: 'Please enter email',
	    		email: 'Please enter a valid email',
	    		maxlength: 'Email can not be greater than {0} characters'
	    	},
	    	password: {
	    		required: 'Please enter password',
	    		minlength: "Please enter at least {0} characters"	
	    	}
	    },
	    submitHandler: function(form) {
   		  form.submit();
	    }
   
   });

    $(".forget-form").validate({
      rules: {
        forgot_email: {
            required: true,
            email: true,  //add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
         }
       },
        messages: {
	    	email:{
	    		required: 'Please enter email',
	    		email: 'Please enter a valid email',
	    		maxlength: 'Email can not be greater than {0} characters'
	    	}
	    },
	 submitHandler: function(form) {
   		form.submit();
	 }
   
   });

    jQuery.validator.addMethod("lettersonly", function(value, element) {
	  return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
	}, ""); 

   $("#add_user_frm").validate({
      rules: {
      	 fullname: {required:true, lettersonly: true},
         email: {
            required: true,
            email: true,  //add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
         },
         password: {required:true, minlength: 6},
         gender: { required: true }
        },
        messages: {
        	fullname: {required: "Please enter fullname", lettersonly: "Enter only alphabets"},
        	email:{
	    		required: 'Please enter email',
	    		email: 'Please enter a valid email',
	    		maxlength: 'Email can not be greater than {0} characters'
	    	},
	    	password: {
	    		required: 'Please enter password',
	    		minlength: "Please enter at least {0} characters"	
	    	},
	    	gender : 'Please select gender type'
	    },
	    submitHandler: function(form) {
   		  form.submit();
	    }
   
   }); 

   $("#edit_user_frm").validate({
      rules: {
      	 fullname: {required:true, lettersonly: true},
         email: {
            required: true,
            email: true,  //add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
         },
         // password: {required:true, minlength: 6},
         gender: { required: true }
        },
        messages: {
        	fullname: {required: "Please enter fullname", lettersonly: "Enter only alphabets"},
        	email:{
	    		required: 'Please enter email',
	    		email: 'Please enter a valid email',
	    		maxlength: 'Email can not be greater than {0} characters'
	    	},
	    	password: {
	    		required: 'Please enter password',
	    		minlength: "Please enter at least {0} characters"	
	    	},
	    	gender : 'Please select gender type'
	    },
	    submitHandler: function(form) {
   		  form.submit();
	    }
   
   });
    
   $("#profile_update_frm").validate({
      rules: {
         fullname: {required:true, lettersonly: true},
         email: {
            required: true,
            email: true,  //add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
         }
        },
        messages: {
          fullname: {required: "Please enter fullname", lettersonly: "Enter only alphabets"},
          email:{
          required: 'Please enter email',
          email: 'Please enter a valid email',
          maxlength: 'Email can not be greater than {0} characters'
        }
      },
      submitHandler: function(form) {
        form.submit();
      }
   
   });

   $("#update_password_frm").validate({
      rules: {
         old_password: {required:true, minlength: 6},
         password: {required:true, minlength: 6},
         cpassword: {required:true, minlength: 6, equalTo: "#password"}
        },
        messages: {
          old_password: {
            required: 'Please enter current password',
            minlength: "Please enter at least {0} characters"
          },
          password: {
            required: 'Please enter new password',
            minlength: "Please enter at least {0} characters" 
          },
          cpassword: {
            required: 'Please enter confirm password',
            minlength: "Please enter at least {0} characters",
            equalTo: "Passwords mismatch"

          }
      },
      submitHandler: function(form) {
        form.submit();
      }
   
   });


});

deleteImage = function ( url, $currentObj ) {
   swal({
       title: "Are you sure?",
       type: "warning",
       showCancelButton: true,
       confirmButtonClass: "btn-danger",
       confirmButtonText: "Yes, delete it!",
       closeOnConfirm: false
     },
     function(){ 
       swal.close();
       $.ajax({
           url     : url,
           type    : 'POST',
           data    :{"_token": $('input[name="_token"]').val() },
           dataType: 'json',
           error:  function ( xhr, status, error ) { 
               swal( 'Error!', 'Unable to delete, please try again later.', 'error' );
           },
           success : function ( json ) { 
               if( json.status == 0 ){ 
                 swal( 'Error!', json.msg, 'error' );
               } else { 
                 swal( 'Deleted!', json.msg, 'success' );
                 $( $currentObj ).closest( '.img-wrap' ).remove();
               }
           }
       });
     });
 };
 
 $(document).ready(function() {
	
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.img-wrap img').attr('src', e.target.result);
            }    
            reader.readAsDataURL(input.files[0]);
			}
    }   
    $(".edit-slide-image .file-upload").on('change', function(){
        readURL(this);
    }); 
});