<?php
function get_device_tokens($con)
{   
    $response = array();

    $sql = "SELECT * FROM register_device_token order by id desc ";
//echo $sql;
    $query = mysqli_query($con, $sql);
    while($row = mysqli_fetch_assoc($query))
    {   
        
        
        
        $response[] = array(
            //'id'      => $row['id'],
            'device_token'    => $row['device_token'],
            //'datetime'      => date("Y-m-d H:i:s",strtotime($row['timestamp']))
            );
    }
    return $response;
       
}

function send_ios_notification($deviceToken,$message,$passphrase,$mode)
{
	
    $passphrase = '';
    $ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', dirname( __FILE__ ) . '/Certificatesbreaking-2.pem');
	stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
	// Open a connection to the APNS server
	if($mode == 'SANDBOX')
	{
		$fp = stream_socket_client(
		'ssl://gateway.sandbox.push.apple.com:2195', $err,  $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
	}
	else
	{
		$fp = stream_socket_client(
		'ssl://gateway.push.apple.com:2195', $err, 	$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
	}
	
	//var_dump($fp);
	if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);

	$body['aps'] = array(
	'alert' => trim($message),
	'sound' => 'default'
	);

	// Encode the payload as JSON
	$payload = json_encode($body);
	// Build the binary notification
	$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
	// Send it to the server
	$result = fwrite($fp, $msg, strlen($msg));
	if (!$result){
	echo 'Message not delivered' . PHP_EOL;
	}
	else
	{
	echo 'Message successfully delivered' . PHP_EOL;
	return $result;
	}
	// Close the connection to the server
	fclose($fp);

}


?>