<?php //echo $_SERVER['DOCUMENT_ROOT'];die;
ini_set('display_errors', 1);
require_once('connection.php');
require_once('TwitterAPIExchange.php');
require_once('functions.php');

$tweet_last_insert_id_query = "SELECT `tweet_id` FROM `all_tweets` ORDER BY `tweet_id` DESC LIMIT 0, 1 ";
$result_last_insert_id = mysqli_query($con, $tweet_last_insert_id_query);
if (mysqli_num_rows($result_last_insert_id) > 0 )
{
    $row = mysqli_fetch_assoc($result_last_insert_id);
    $last_insert_id = $row['tweet_id'];
}
else
{
    $last_insert_id = 0;
}
 
/** Set access tokens here - see: https://dev.twitter.com/apps/ **/
$since_id   = $last_insert_id;
$count      = 200;

/** URL for REST request, see: https://dev.twitter.com/docs/api/1.1/ **/
$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
if($since_id > 0)
{
    //$getfield = '?screen_name='.$screen_name.'&tweet_mode=extended&count='.$count.'&since_id='.$since_id;   
	$getfield = '?screen_name='.$screen_name.'&tweet_mode=extended&count='.$count.'&since_id='.$since_id;	
}
else
{
	$getfield = '?screen_name='.$screen_name.'&tweet_mode=extended&count='.$count;	
}
$requestMethod = 'GET';
$twitter = new TwitterAPIExchange($settings);
$response = $twitter->setGetfield($getfield)->buildOauth($url,$requestMethod)->performRequest();
//echo $response;die;
$response_decoded = json_decode($response);
             //echo "<pre>";print_r(  $response_decoded);
if(!empty($response_decoded))
{ 
	echo "Tweets insertion started<br>";
foreach($response_decoded as $status) {
        //echo "<pre>";print_r($status);echo "</pre>";
        // Convert links back into actual links, otherwise they're just output as text
        $enhancedStatus = htmlentities($status->full_text, ENT_QUOTES, 'UTF-8');
        $enhancedStatus = preg_replace('/http:\/\/t.co\/([a-zA-Z0-9]+)/i', '<a href="http://t.co/$1">http://$1</a>', $enhancedStatus);
        $enhancedStatus = preg_replace('/https:\/\/t.co\/([a-zA-Z0-9]+)/i', '<a href="https://t.co/$1">http://$1</a>', $enhancedStatus);

        $tweet_id           =   $status->id;
        $tweet_text         =   $status->full_text;
        $tweet_by           =   $status->user->name;
        $screen_name        =   $status->user->screen_name;
        $time_date_of_tweet =   strtotime($status->created_at) ;
        $followers_count    =   $status->user->followers_count;
        if(isset($status->extended_entities))
        {
        $media_extended_entities_array = $status->extended_entities->media;
         
        }
        else
        {
           $media_extended_entities_array = array(); 
        }
        //echo "start array<pre>";print_r($media_extended_entities_array);echo "</pre>"; 
        
        $tweet_exist_query = "SELECT tweet_id FROM all_tweets where tweet_id = '".$tweet_id."'  ";
        $result = mysqli_query($con,$tweet_exist_query);
        if ( mysqli_num_rows($result) > 0)
        {

        }
        else
        {
            $query = "INSERT INTO all_tweets SET tweet_id='".$tweet_id."', tweet_text='".htmlentities($tweet_text)."' , tweeted_by='".$tweet_by."', screen_name='".$screen_name."', time_date_of_tweet='".$time_date_of_tweet."', followers='".$followers_count ."' ";
            //echo $query;
            mysqli_query($con,$query) ;
            // /$tweet_insert_id = mysqli_insert_id($con) ;
            if(!empty($media_extended_entities_array))
            {
                foreach($media_extended_entities_array as $k=>$media)
                {
                    $query_media = "INSERT INTO twitter_media SET tweet_id='".$tweet_id."', media_url='".htmlentities($media_extended_entities_array[$k]->media_url)."' , media_url_https='".htmlentities($media_extended_entities_array[$k]->media_url_https)."' ";
                    mysqli_query($con,$query_media) ;
                }
            }
        }
        
        
    }

    // code start for send push Notifications to registred Device Token
    $stored_device_token = get_device_tokens($con);
    echo "<pre>";print_r($stored_device_token);
    $message = "There are new updates";
    $passphrase = "";
    $mode = "SANDBOX";
    foreach ($stored_device_token as $key => $value) {
        $DT = $value['device_token'];
        send_ios_notification($DT,$message,$passphrase,$mode); 
    }
    //echo "<pre>";print_r($stored_device_token);
    echo "Tweets insertion Finished<br>";
    file_put_contents("/var/www/html/log.txt",json_encode(array("date"=>date("Y-m-d H:i:s"),"since_id"=>$since_id)));
    

}
else
{ 
	echo "No new Tweets found yet";
}

?>
