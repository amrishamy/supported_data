<?php

	class MyPayPal {
		
		function GetItemTotalPrice($item){
		
			//(Item Price x Quantity = Total) Get total amount of product;
			return $item['ItemPrice'] * $item['ItemQty']; 
		}
		
		function GetProductsTotalAmount($products){
		
			$ProductsTotalAmount=0;

			foreach($products as $p => $item){
				
				$ProductsTotalAmount = $ProductsTotalAmount + $this -> GetItemTotalPrice($item);	
			}
			
			return $ProductsTotalAmount;
		}
		
		function GetGrandTotal($products, $charges){
			
			//Grand total including all tax, insurance, shipping cost and discount
			
			$GrandTotal = $this -> GetProductsTotalAmount($products);
			
			foreach($charges as $charge){
				
				$GrandTotal = $GrandTotal + $charge;
			}
			
			return $GrandTotal;
		}
		
		function SetExpressCheckout(){
			
			//Parameters for SetExpressCheckout, which will be sent to PayPal
			
			$padata  = 	'&METHOD=SetExpressCheckout';
			
			$padata .= 	'&RETURNURL='.urlencode(PPL_RETURN_URL);
			$padata .=	'&CANCELURL='.urlencode(PPL_CANCEL_URL);
			//$padata .=	'&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("SALE");
			$padata .=	'&PAYMENTREQUEST_0_PAYMENTACTION='.urlencode("AUTHORIZATION");
			
		

			$padata .=	'&PAYMENTREQUEST_0_AMT=0';
			$padata .=	'&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode(PPL_CURRENCY_CODE);


			/********custom******/
			$padata .=	'&L_BILLINGTYPE0=MerchantInitiatedBillingSingleAgreement'; 
			$padata .=	'&L_BILLINGAGREEMENTDESCRIPTION0=AddFundsFromPaypal'; 


			/********custom******/

			
			
			//paypal custom template
			
			$padata .=	'&LOCALECODE='.PPL_LANG; //PayPal pages to match the language on your website;
			$padata .=	'&LOGOIMG='.PPL_LOGO_IMG; //site logo
			$padata .=	'&CARTBORDERCOLOR=FFFFFF'; //border color of cart
			$padata .=	'&ALLOWNOTE=1';
						
			############# set session variable we need later for "DoExpressCheckoutPayment" #######
			
			/*$_SESSION['ppl_products'] =  $products;
			$_SESSION['ppl_charges'] 	=  $charges;*/
			
			$httpParsedResponseAr = $this->PPHttpPost('SetExpressCheckout', $padata);

			//echo "<pre>SetExpressCheckout Array==";print_r($httpParsedResponseAr);echo "</pre>";die();
			
			//Respond according to message we receive from Paypal
			if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])){

				$paypalmode = (PPL_MODE=='sandbox') ? '.sandbox' : '';
			
				//Redirect user to PayPal store with Token received.
				
				$paypalurl ='https://www'.$paypalmode.'.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$httpParsedResponseAr["TOKEN"].'';
				
				header('Location: '.$paypalurl);
			}
			else{
				
				//Show error message
				
				echo '<div style="color:red"><b>Error : </b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]).'</div>';
				
				echo '<pre>';
					
					print_r($httpParsedResponseAr);
				
				echo '</pre>';
			}	
		}		
		


/********mi CreateBillingAgreement function starts******************/

function CreateBillingAgreement($token){


			$padata  = 	'&METHOD=CreateBillingAgreement';
			
			$padata .= 	'&RETURNURL='.urlencode(PPL_RETURN_URL);
			$padata .=	'&CANCELURL='.urlencode(PPL_CANCEL_URL);
			
			


			$padata .=	'&L_BILLINGTYPE0=MerchantInitiatedBillingSingleAgreement'; 
			$padata .=	'&L_BILLINGAGREEMENTDESCRIPTION0=AddFundsFromPaypal'; 
			$padata .=	'&TOKEN='.$token; 


			
			
			
			$httpParsedResponseAr_billingAgg = $this->PPHttpPost('CreateBillingAgreement', $padata);

			//echo "<pre>";print_r($httpParsedResponseAr_billingAgg);echo "</pre>";

			return $httpParsedResponseAr_billingAgg;





}
function DoReferenceTransaction($BILLINGAGREEMENTID_decoded,$amount){


			$padata  = 	'&METHOD=DoReferenceTransaction';
			
			$padata .= 	'&RETURNURL='.urlencode(PPL_RETURN_URL);
			$padata .=	'&CANCELURL='.urlencode(PPL_CANCEL_URL);
			
			


			$padata .=	'&AMT='.$amount; 
			$padata .=	'&CURRENCYCODE=USD'; 
			$padata .=	'&PAYMENTACTION=SALE'; 
			$padata .=	'&REFERENCEID='.$BILLINGAGREEMENTID_decoded; 


			
			
			
			$ref_trans_arr = $this->PPHttpPost('DoReferenceTransaction', $padata);


			//echo "<pre>this is final array";print_r($ref_trans_arr);echo "</pre>";
			return $ref_trans_arr;







}

/********mi CreateBillingAgreement function ends******************/

			
		


		function DoExpressCheckoutPayment($token, $PAYERID){
			
			//echo $token."====".$PAYERID;die();
				
				$padata  = 	'&METHOD=DoExpressCheckoutPayment';
			
			$padata .= 	'&RETURNURL='.urlencode(PPL_RETURN_URL);
			$padata .=	'&CANCELURL='.urlencode(PPL_CANCEL_URL);
			
			


			$padata .=	'&TOKEN='.$token; 
			$padata .=	'&PAYMENTREQUEST_0_PAYMENTACTION=SALE'; 
			$padata .=	'&PAYERID='.$PAYERID; 
			$padata .=	'&PAYMENTREQUEST_0_AMT=39'; 


			
			
			
			$DoExpressCheckoutPayment_arr = $this->PPHttpPost('DoExpressCheckoutPayment', $padata);

			return $DoExpressCheckoutPayment_arr;
				

			
		}
				
		function GetTransactionDetails(){
		
			// we can retrive transection details using either GetTransactionDetails or GetExpressCheckoutDetails
			// GetTransactionDetails requires a Transaction ID, and GetExpressCheckoutDetails requires Token returned by SetExpressCheckOut
			
			$padata = 	'&TOKEN='.urlencode(_GET('token'));
			
			$httpParsedResponseAr = $this->PPHttpPost('GetExpressCheckoutDetails', $padata, PPL_API_USER, PPL_API_PASSWORD, PPL_API_SIGNATURE, PPL_MODE);

			if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])){
				
				echo '<br /><b>Stuff to store in database :</b><br /><pre>';
				/*
				#### SAVE BUYER INFORMATION IN DATABASE ###
				//see (http://www.sanwebe.com/2013/03/basic-php-mysqli-usage) for mysqli usage
				
				$buyerName = $httpParsedResponseAr["FIRSTNAME"].' '.$httpParsedResponseAr["LASTNAME"];
				$buyerEmail = $httpParsedResponseAr["EMAIL"];
				
				//Open a new connection to the MySQL server
				$mysqli = new mysqli('host','username','password','database_name');
				
				//Output any connection error
				if ($mysqli->connect_error) {
					die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
				}		
				
				$insert_row = $mysqli->query("INSERT INTO BuyerTable 
				(BuyerName,BuyerEmail,TransactionID,ItemName,ItemNumber, ItemAmount,ItemQTY)
				VALUES ('$buyerName','$buyerEmail','$transactionID','$products[0]['ItemName']',$products[0]['ItemNumber'], $products[0]['ItemTotalPrice'],$ItemQTY)");
				
				if($insert_row){
					print 'Success! ID of last inserted record is : ' .$mysqli->insert_id .'<br />'; 
				}else{
					die('Error : ('. $mysqli->errno .') '. $mysqli->error);
				}
				
				*/
				
				echo '<pre>';
				
					print_r($httpParsedResponseAr);
					
				echo '</pre>';
			} 
			else  {
				
				echo '<div style="color:red"><b>GetTransactionDetails failed:</b>'.urldecode($httpParsedResponseAr["L_LONGMESSAGE0"]).'</div>';
				
				echo '<pre>';
				
					print_r($httpParsedResponseAr);
					
				echo '</pre>';

			}
		}
		
		function PPHttpPost($methodName_, $nvpStr_) {
				
				// Set up your API credentials, PayPal end point, and API version.
				$API_UserName = urlencode(PPL_API_USER);
				$API_Password = urlencode(PPL_API_PASSWORD);
				$API_Signature = urlencode(PPL_API_SIGNATURE);
				
				$paypalmode = (PPL_MODE=='sandbox') ? '.sandbox' : '';
		
				$API_Endpoint = "https://api-3t".$paypalmode.".paypal.com/nvp";
				//$version = urlencode('109.0');
				$version = urlencode('86.0');
			
				// Set the curl parameters.
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
				curl_setopt($ch, CURLOPT_VERBOSE, 1);
				//curl_setopt($ch, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
				
				// Turn off the server and peer verification (TrustManager Concept).
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_POST, 1);
			
				// Set the API operation, version, and API signature in the request.
				$nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";
			
				// Set the request as a POST FIELD for curl.
				curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
			
				// Get response from the server.
				$httpResponse = curl_exec($ch);
			
				if(!$httpResponse) {
					exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');
				}
			
				// Extract the response details.
				$httpResponseAr = explode("&", $httpResponse);
			
				$httpParsedResponseAr = array();
				foreach ($httpResponseAr as $i => $value) {
					
					$tmpAr = explode("=", $value);
					
					if(sizeof($tmpAr) > 1) {
						
						$httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];
					}
				}
			
				if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {
					
					exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");
				}
			
			return $httpParsedResponseAr;
		}
	}
