<?php
 function gen_getUploadFileNameWithUniqPrefix($id, $fileName) {
 	$rand_val = getRandLetters(5);
  	return $id."-".$rand_val."-".$fileName;
 }

// ----------------------------------------------------------------------------------------------------------------------------
// Created By Piyush Nayee as on 4 Dec 2010...
// Function checks for prefix on website url...
// ----------------------------------------------------------------------------------------------------------------------------
function gen_getTargetURL($vURL, $default_url="")
{
	global $site_url;
	$ret_url = "";
	if($default_url == "")
		$default_url = $site_url;

	if($vURL != "")
	{
		if(strstr($vURL, "http://") || strstr($vURL, "https://"))
			$ret_url = $vURL;
		else
			$ret_url = "http://".$vURL;
	}
	else
		$ret_url = $default_url;

	return $ret_url;
}
#=======================================================
# THIS FUNCTION WILL REDIRECT USER TO LOGIN PAGE IF HE 
# HAS NOT PROVIDED ACCESSKEY
#=======================================================
function _do_Check_User_Login_With_Accesskey()// User
{
	global $_SESSION, $site_url;
	if($_SESSION['sess_vAccessKey'] == '')
	{
		header("Location:".$site_url."service-user-login.html");
		exit;
	}
}
#=======================================================

function make_seed()
{
    list($usec, $sec) = explode(' ', microtime());
    return (float) $sec + ((float) $usec * 100000);
}

function gen_convertIntoTitleCase($text)
{
	return ucfirst(strtolower($text));
}
##-------------
## Function to get Session Id 
 function gen_get_session_id($sessid = '') {
    if (!empty($sessid)) {
      return session_id($sessid);
    } else {
      return session_id();
    }
  }
# ---------------------------------------------------------------------------------
# Function will Replace slashes from URL
# ---------------------------------------------------------------------------------
function gen_currentURL()
{
	$current_url_domain = $_SERVER['HTTP_HOST'];
	$current_url_path = $_SERVER['SCRIPT_NAME'];
	$current_url_querystring = $_SERVER['QUERY_STRING'];
	$current_url = "http://".$current_url_domain.$current_url_path;
	if($current_url_querystring != "") {  $current_url .= "?".$current_url_querystring; }
	$current_url = urlencode($current_url);
	return $current_url;
}

# --------------------------------------------------------
# --------------------------------------------------------
# SEO Title / Keyword / Description SETTINGS
# Type = Title / Keyword / Description
function getSEOData($vPageName)
{
	global $sqlObj;
	$sql = "select vMetaTitle ,vMetaKeyword, tMetaDescription from seo_settings where vPageName='".$vPageName."'";
	$db_sql = $sqlObj->select($sql);
	return $db_sql[0];
}
# --------------------------------------------------------

## Get total Login IP address of user 
function getLoginHistoryOfUser($id, $type, $hr)
{
	global $sqlObj;
	if($type=='Customer')
	{
		$type_id = 2;
	}
	else if($type=='Operator')
	{
		$type_id = 3;
	}
	else if($type=='WebsiteOwner')
	{
		$type_id = 4;
	}
	$last_login = date_addDate(date("Y-m-d H:i:s"), 0, 0, 0, $hr*-1);
	$sql = "select lh.vIP, lh.dLoginDate, lh.dLogoutDate from login_history lh where iID='$id' and eType='$type_id' and dLoginDate>='$last_login' order by iLHId desc";
	$db_sql = $sqlObj->select($sql);
	return $db_sql;
}

## Get total Login IP address of user 
function gen_getLoginCountFromDifferentIP($id, $type, $hr)
{
	global $sqlObj;
	if($type=='Customer')
	{
		$eType = 'C';
	}
	else if($type=='Operator')
	{
		$eType = 'O';
	}
	else if($type=='GroupUser')
	{
		$eType = 'G';
	}
	$last_login = date_addDate(date("Y-m-d H:i:s"), 0, 0, 0, $hr*-1);
	$sql = "select count(iLHId) as tot from login_history where iID='$id' and eType='$eType' and dLoginDate>='$last_login' group by vIP";
	$db_sql = $sqlObj->select($sql);
	return count($db_sql);
}

## Created By Jayesh on 1-may-09
## Function close the connection and then redirect to passed url
function hc_header($urldata)
{
	global $sqlObj;
	$sqlObj->closeConnection();
	header("location:$urldata");
	exit;
}
#----------------------------------------------
# GET PAYMENT METHOD NAME
#----------------------------------------------
function gen_getPayMethodName($method_name)
{
	if ($method_name=="Payflow_Pro" || $method_name=="Payflow Pro" || $method_name == "CC" || $method_name == "Link Point" || $method_name == "Net Billing")
	  $payMName = "Credit Card";
	else if($method_name=="UA")
		$payMName = "User Account";
	else
	  $payMName = $method_name;
	  
	  return $payMName; 
}
#----------------------------------------------

#----------------------------------------------
# GET DISPLAY CREDIT CARD
#----------------------------------------------
function getDisplayCreditCard($cc_num)
{
	if(strlen($cc_num)>12)
	{
		return str_repeat("*",strlen($cc_num)-4).substr($cc_num, -4);
	}
	else
	{
		return "";
	}
}

#----------------------------------------------
# Encrypt CREDIT CARD
#----------------------------------------------
function encryptCreditCard($str)
{
	return encrypt($str);
}

#----------------------------------------------
# Decrypt CREDIT CARD
#----------------------------------------------
function decryptCreditCard($str)
{
	return decrypt($str);
}



#----------------------------------------------
# GENERATE QUERY STRING
#----------------------------------------------
function gen_getQueryString()
{
	global $_POST;
	foreach ( $_POST as $key => $value ) {
	$queryString.=$key."=".rawurlencode($value)."&";
	}
	return $queryString = substr($queryString,0,strlen($queryString)-1);
}	
#----------------------------------------------

#-----------------------------------------------------------------------
# MANUPULATE QUERY STRING AND GENERATE SMARTY ASSIGN VALUES DYNAMICALLY.
#-----------------------------------------------------------------------
function autoAssignSmartyVars()
{
	global $smarty,$_SERVER;
	$geturl = explode("?",$_SERVER[REQUEST_URI]);
	$getQString = explode("&",$geturl[1]); 
	for ($qs=0;$qs<count($getQString);$qs++) {
		$getQStringval = explode("=",$getQString[$qs]); 
		$PutqueryString.="$"."smarty->assign('$getQStringval[0]','".rawurldecode($getQStringval[1])."');\n";
	}
	eval($PutqueryString);
}	
#-----------------------------------------------------------------------

function duplicateValueCheck($field_name, $field_value, $table_name)
{
	global $sqlObj;
	$field_value = trim($field_value);
	if($field_value == '')
		$exist = "-1";
	else
	{
		if($table_name != "")
		{	
			$sql_uname = "select ".$field_name." from ".$table_name." where ".$field_name." = '".$field_value."' LIMIT 1";
			$rs_sql = $sqlObj->select($sql_uname);
			if(count($rs_sql)>0)
				$exist = "1";
			else 
				$exist = "0";
		}
		else
			$exist = "1";
	}
	return trim($exist);
}

/*function duplicateValueCheck($field_name, $field_value, $table_name)
{
	global $sqlObj;
	$field_value = trim($field_value);
	if($field_value == '')
		$exist = "-1";
	else
	{
		if($table_name != "")
		{	
			$sql_uname = "select ".$field_name." from ".$table_name." where ".$field_name." = '".$field_value."' LIMIT 1";
			$rs_sql = $sqlObj->select($sql_uname);
			if(count($rs_sql)>0)
				$exist = "1";
			else 
				$exist = "0";
		}
		else
			$exist = "1";
	}
	return trim($exist);
}*/

function getRandomValue($cid)
{
	srand(make_seed());
	$randuniqueId = rand(10000,99999);
	return $randuniqueId;
}

function _do_User_Authentication($type) //"Customer", "Operator", "WebsiteOwner", "Affiliate"
{	
	global $_SESSION, $main_site_url,$smarty;
	
	$smarty->assign('is_authenticate',"yes");
	
	
	if($type == 'GroupOperator')
		$type_name = 'Group Operator';
	else
		$type_name = $type;
	if (isset($_SESSION['sess_Login_Id']) && !empty($_SESSION['sess_Login_Id']) && $_GET['page'] != 'login.html')
	{
		//echo $_SESSION['sess_Login_Type']; echo $type; exit; 
		if($_SESSION['sess_Login_Type'] != '' && $_SESSION['sess_Login_Type'] != $type)
		{				
			$_erroMsg = "Unauthorized Access. Only $type_name can access this module";
			header("location:".$main_site_url."login.html/1/".$_erroMsg);
			exit;
		}
		
	} else {
		
		if($type !='')
			$_erroMsg = "Unauthorized Access. Only $type_name can access this module.";
		else
			$_erroMsg = "~~~~ Unauthorized Access ~~~~";
			header("location:".$main_site_url."login.html/1/".$_erroMsg);
		exit;
	}
	
	
}

#=======================================================
# THIS FUNCTION WILL REDIRECT CLIENT TO MY PAY LOG IF
# HIS STATUS IS EXPIRED...
#=======================================================
function _do_Check_User_Status()// Client
{
	global $_SESSION,$site_url;
	if($_SESSION['sess_Login_Status'] == 'Expired')
	{
		header("Location:".$site_url."client-pay-log.html");
		exit;
	}
}
#=======================================================
function _do_Check_Manager_User_Status()// WebsiteOwner
{
	global $_SESSION,$site_url;
	if($_SESSION['sess_Login_Status'] == 'Expired')
	{
		header("Location:".$site_url."manager-pay-log.html");
		exit;
	}
}

function getImageURL($imgName, $imgPath, $imgURL, $imgNamePrefix='', $imgDefaultURL='')
{
	
	$fina_img_url = '';
    if($imgName !='' && file_exists($imgPath.$imgNamePrefix.$imgName))
		$fina_img_url = $imgURL.$imgNamePrefix.rawurlencode($imgName);
	else
		$fina_img_url = $imgDefaultURL;
//	echo "<pre>".$fina_img_url;
	return $fina_img_url;
}

if (!function_exists("file_put_contents")){
	function file_put_contents($filename, $content)
	{
		if (!$handle = fopen($filename, 'w+')) {
			 echo "Cannot open file ($filename)";
			 exit;
		}
	
		// Write $somecontent to our opened file.
		if (fwrite($handle, $content) === FALSE) {
			echo "Cannot write to file ($filename)";
			exit;
		}
		fclose($handle);
		return 1;
	}
}

function ob_clean_all () { 
    $ob_active = ob_get_length () !== false; 
    while($ob_active) { 
        ob_end_clean(); 
        $ob_active = ob_get_length () !== false; 
    } 

    return true; 
}

function getSSLPageURL()
{
	$pageURL = 'http';
	if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	 $pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80"){
 		 $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 	}else{
 		 $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	 }
	 return $pageURL;
}

function createJavaScriptArray($array, $devidbytwo = "")
{
	if(is_array($array) && count($array) > 0)
	{
		if(!is_array($array[0]))
			$java_array = " ['".$array[0];
		else
		{
			$java_array = " ['".$array[0][0];
			$firstclount = ($devidbytwo == "No") ? count($array[0]) : count($array[0])/2;
			for($j = 1; $j < $firstclount; $j++)
				$java_array .= "', '".$array[0][$j]."";
		}
		 $java_array .= "']";
		for($i = 1; $i < count($array); $i++)
		{
			if(!is_array($array[$i]))
				$java_array .= " ,['".$array[$i];
			else
			{
				$java_array .= " ,['".$array[$i][0];
				$secondclount = ($devidbytwo == "No") ? count($array[$i]) : count($array[$i])/2;
				for($j = 1; $j < $secondclount; $j++)
					$java_array .= "', '". $array[$i][$j] ."";
			}
			$java_array .= "']";
		}
		
	}
	return $java_array;
}

function getIP()
{
	if (getenv('HTTP_CLIENT_IP')) {
			$ip = getenv('HTTP_CLIENT_IP');
		}
		elseif (getenv('HTTP_X_FORWARDED_FOR')) {
			$ip = getenv('HTTP_X_FORWARDED_FOR');
		}
		elseif (getenv('HTTP_X_FORWARDED')) {
			$ip = getenv('HTTP_X_FORWARDED');
		}
		elseif (getenv('HTTP_FORWARDED_FOR')) {
			$ip = getenv('HTTP_FORWARDED_FOR');
		}
		elseif (getenv('HTTP_FORWARDED')) {
			$ip = getenv('HTTP_FORWARDED');
		}
		else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
}

function visitors()
{
	global $obj;
	$sql = "insert into visitors (vIP,dDate) values ('".$_SERVER[REMOTE_ADDR]."','".date("Y-m-j H-i-s")."')";
	$db_insert = $obj->insert($sql);
}

function getSessionId($sessid = '') {
    if (!empty($sessid)) {
      return session_id($sessid);
    } else {
      return session_id();
    }
}
  

function float1($val)
{
	$x = intval(10*$val)/10;
	if ($x == intval($x)) return "".$x.".0";
	else return intval(10*$val)/10;
}

function float2($val)
{
	$x = intval(10*$val)/10;
	if ($x == intval($x)) return "".$x.".00";
	else return intval(10*$val)/10;
}


function getExpiry($CardExpiry)
{
		$month=substr($CardExpiry,0,2);
		$year=substr($CardExpiry,3,4);
		return $vCardExpiry=date("F, Y",mktime(0,0,0,$month,1,$year));
}


function generateRandRecordset($rec)
{
	global $obj;
	srand ((float)microtime()*1000000);
	if(count($rec) > 0)
		return shuffle($rec);
}
#-----------------------------------------------------------------
# FUNCTION THAT ALLOW SEO IN THIS PROJECT
#-----------------------------------------------------------------
function showSEOlink($generalLink,$SEOLink)
{
	global $allowSEOval;

	if ($allowSEOval=='Yes')
		return $SEOLink;
	else
		return $generalLink;
}
#-----------------------------------------------------------------

function transferDoubleToSingle($db_array,$substractArr = "")
{
	for($i=0, $n=count($db_array) ; $i<$n ; $i++)
	{
		  $single[] = $db_array[$i][0];
	}
	return $single;
}

function displayPagingTop($showLetter="Y", $showPaging="Y", $width="100%",$TotalRecords,$REC_LIMIT)
{
	

	global $recmsg, $page_link;
	if($showPaging=="N")
		$style = "style=display:none";
	else
		$style = "style=display:''";
	echo '<table width="'.$width.'" cellpadding="0" cellspacing="0" border="0" class="CustomerListView">
		<tr style="height:20px">
			<td align="left" width="18%" class="disprecordmsg"><span '.$style.' class="disprecordmsg"  id="RECMSG_BOTTOM">'.(($showPaging=="Y")?$recmsg:"").'&nbsp;</span></td>
			<td width="10px" align="left" style="color:#333"> | </td>
			<td width="160px" align="left" class="SelectRecordLimit">						
							<span>Record per page : </span>&nbsp;
							<select id="TotalRecords" name="TotalRecords" onchange="return checkStatus();">
							'.getDropDowm($TotalRecords,$REC_LIMIT).'	
							</td>
								
			<td width="100px" style="text-align:Center" nowrap><span class="JumpTo">'.(($showLetter=="Y")?"Jump to":"").' </span><span '.$style_alpha.' class="disprecordmsg"  id="ALPHA_PAGING_BOTTOM">'.(($showLetter=="Y")?getSearchByLetter():"").'&nbsp;</span></td>
			<td width="250px" style="text-align:right"><span '.$style.' class="errormsg"   id="PAGING-BOTTOM">'.(($showPaging=="Y")?$page_link:"").'&nbsp;</span></td>
		</tr>
		</table>';
}

function getDropDowm($TotalRecords,$REC_LIMIT)
{

	$link ='<option value="'.$REC_LIMIT.'" '.(($TotalRecords==$REC_LIMIT)?"selected":"").'> Default Limit ('.$REC_LIMIT.') </option>';
	for($i=0;$i<10;$i++)	{
		$v= ($i+1)*10;
		if($TotalRecords==$v)
		{
			$c = 'selected';
		}
		else
		{
			$c='';
		}
		$link .= "<option value='".$v."'  ".$c.">".$v."</option>";
								
	}
	return $link;
}

function displayPagingBottom($showLetter="Y", $showPaging="Y", $width="100%")
{
	global $recmsg, $page_link;
	if($showPaging=="N")
		$style = "style=display:none";
	else
		$style = "style=display:''";
	echo '<table width="'.$width.'" cellpadding="0" cellspacing="0" border="0">
		<tr style="height:20px">
			<td width="32%" class="disprecordmsg"><span '.$style.' class="disprecordmsg"  id="RECMSG_BOTTOM">'.(($showPaging=="Y")?$recmsg:"").'&nbsp;</span></td>
			<td style="text-align:Center" nowrap><span '.$style_alpha.' class="disprecordmsg"  id="ALPHA_PAGING_BOTTOM">'.(($showLetter=="Y")?getSearchByLetter():"").'&nbsp;</span></td>
			<td width="32%" style="text-align:right"><span '.$style.' class="errormsg"   id="PAGING-BOTTOM">'.(($showPaging=="Y")?$page_link:"").'&nbsp;</span></td>
		</tr>
		</table>';
}

function getSearchByLetter()
{
	$link = "<select onchange =checkStatus('Search',this.value,'');>";
	for($i=65;$i<=90;$i++)
	{
		
			if($_GET['iOperatorId']!="" || $_GET['TabId']!="" || $_GET['iOGallery']!="")
				{
					if($page == "operator_gallery_image_list")
					{
					$extra_param = "&iOperatorId=".$_GET['iOperatorId']."&TabId=".$_GET['TabId']."&iOGalleryId=".$_GET['iOGalleryId'];
					}
					else
					{
						$extra_param = "&iOperatorId=".$_GET['iOperatorId']."&TabId=".$_GET['TabId'];
					}
				}
			
			if($_GET['keyword'])
			{
				if($_GET['keyword'] == chr($i))
				{
					$check = 'selected';
				}
				else
				{
					$check = '';		
				}
			}
			
		//	$link .= '<a class="bluetext" href="javascript:checkStatus(\'Search\', \''.chr($i).'\',\''.$extra_param.'\')" title="Search with '.chr($i).'">'.chr($i).'</a> ';
			$link .= "<option ".$check." value='".chr($i)."'>".chr($i)."</option>";
			
			
			
	}
	if($_GET[keyword]=="")
	//	$link .= '<a href="javascript:checkStatus(\'Search\', \'\')"><font color="ff6600" size="+1">'.ALL.'</font></a>';
		$link .= "<option value=''>".All."</option> ";
	else 
		{
		if($_GET['iOperatorId']!="" || $_GET['TabId']!="")
					$extra_param = "&iOperatorId=".$_GET['iOperatorId']."&TabId=".$_GET['TabId'];
		//$link .= '<a class="bluetext" href="javascript:checkStatus(\'Search\', \'\',\''.$extra_param.'\')" title="Show All">'.ALL.'</a>';
		//$link .= "<option value=''>".All."</option>";

		}
		$link.="</select>";
	return $link;
}
/*
function getSearchByLetter()
{
	$link = "";
	for($i=65;$i<=90;$i++)
	{
		if($_GET[keyword]==chr($i))
			$link .= '<font color="ff6600" size="+1">'.chr($i).'</font> ';
		else
			{
			if($_GET['iOperatorId']!="" || $_GET['TabId']!="" || $_GET['iOGallery']!="")
				{
					if($page == "operator_gallery_image_list")
					{
					$extra_param = "&iOperatorId=".$_GET['iOperatorId']."&TabId=".$_GET['TabId']."&iOGalleryId=".$_GET['iOGalleryId'];
					}
					else
					{
						$extra_param = "&iOperatorId=".$_GET['iOperatorId']."&TabId=".$_GET['TabId'];
					}
				}
			
			$link .= '<a class="bluetext" href="javascript:checkStatus(\'Search\', \''.chr($i).'\',\''.$extra_param.'\')" title="Search with '.chr($i).'">'.chr($i).'</a> ';
			}
	}
	if($_GET[keyword]=="")
		$link .= '<a href="javascript:checkStatus(\'Search\', \'\')"><font color="ff6600" size="+1">'.ALL.'</font></a>';
	else 
		{
		if($_GET['iOperatorId']!="" || $_GET['TabId']!="")
					$extra_param = "&iOperatorId=".$_GET['iOperatorId']."&TabId=".$_GET['TabId'];
		$link .= '<a class="bluetext" href="javascript:checkStatus(\'Search\', \'\',\''.$extra_param.'\')" title="Show All">'.ALL.'</a>';

		}
	return $link;
}*/
function getTopInListAdd($TOP_HEADER,$BACK_LABEL='',$BACK_LINK='',$HEADING='')
{
	$html='<div class="screenTitle">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
      		<tr>
				<td><img src="images/on.gif">&nbsp;'.$TOP_HEADER.'</td>';
		if($BACK_LABEL!='')
			$html .= '<td align="right"><a href="'.$BACK_LINK.'">'.$BACK_LABEL.'</a>&nbsp;&nbsp;</td>';
	$html .= '</tr>
			</table>
		</div>';

	return $html;
}

$vstatus = array('Active','Inactive',);

function displayStatusNoDelete($selstatus,$fieldId)
{
	global $vstatus;

	if(empty($selstatus))
		$selstatus = "Active";

	$statuscombo = "";
	$statuscombo .= "<select name=\"$fieldId\">";

	for($i=0;$i<count($vstatus);$i++) {

		if (trim($selstatus) == trim($vstatus[$i])) {
			$statuscombo .= "<option value=".$vstatus[$i]." selected>".$vstatus[$i];
		} else {
			$statuscombo .= "<option value=".$vstatus[$i].">".$vstatus[$i];
		}
	}
	$statuscombo .= "</select>";
	return $statuscombo;
}

function generateLink($link)
{
	global $obj;
$link_arr=array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","Other");

for($i=0;$i<count($link_arr);$i++)
{
	$link_data[$i]["link_url"] = $link."&term=".$link_arr[$i];
	if($i==(count($link_arr)-1))
		$link_data[$i]["link"] = $link_arr[$i];
	else
		$link_data[$i]["link"] = $link_arr[$i]."&nbsp;|";
}
return $link_data;
}

###### Password Encrypt #############
function encrypt($data)
{
	for($i = 0, $key = 27, $c = 48; $i <= 255; $i++)
	{
		$c = 255 & ($key ^ ($c << 1));
		$table[$key] = $c;
		$key = 255 & ($key + 1);
	}
	$len = strlen($data);
	for($i = 0; $i < $len; $i++)
	{
		$data[$i] = chr($table[ord($data[$i])]);
	}
	return base64_encode($data);
}

#########Password Decrypt ##########

function decrypt($data)
{
	$data = base64_decode($data);
	for($i = 0, $key = 27, $c = 48; $i <= 255; $i++)
	{
		$c = 255 & ($key ^ ($c << 1));
		$table[$c] = $key;
		$key = 255 & ($key + 1);
	}
	$len = strlen($data);
	for($i = 0; $i < $len; $i++)
	{
		$data[$i] = chr($table[ord($data[$i])]);
	}
	return $data;
}


# replaces duplicate Tags by Anurag.
function chktags($text)
{
	$find_arr=array('<html>','<head>','<meta content=\"MSHTML 6.00.2600.0\" name=\"GENERATOR\"/>','</head>','<body>','</body>','</html>');
	$rep_arr=array("","","","","","","");

	$val=str_replace($find_arr,$rep_arr,$text);
	return $val;
}

//This function is used in Admin side
//------------------------------------------------
function mailme($to, $subject, $vBody, $from, $format, $cc, $bcc="")
{
	global $site_path,$debug_mode,$local_siteip;
	if(strlen($format)==0)
		$format="text/html";
	$headers  = "MIME-Version: 1.0\r\n"; 
	$headers .= "Content-type: ". $format ."; charset=iso-8859-1\r\n"; 
	
	/* additional headers */ 
	$headers .= "From: $from\r\n"; 
	if(strlen($cc)>5)
		$headers .= "Cc: $cc\r\n"; 
	if(strlen($bcc)>5)
		$headers .= "Bcc: $bcc\r\n"; 
	$cnt = "";
	$cnt = "Headers : \n".$headers."\n";
	$cnt .= "<br>Date Time :". date("d M, Y  h:i:s")."\r\n";
	$cnt .= "To : ".$to."\n";
	$cnt .= "Subject : ".$subject."\n";
	$cnt .= "Body : \n".$vBody."\n";
	$cnt .= "====================================================================\n\n";
	//echo $cnt;exit;
	$debug_mode = "true";
	if($debug_mode == "true")
	{
		$filename = $site_path."mail_log.html"; 
	   	if (!$fp = fopen($filename, 'a')) { 
	        print "Cannot open file ($filename)"; 
	        exit; 
	   	}
	   	if (!fwrite($fp, $cnt)) { 
	       print "Cannot write to file ($filename)"; 
	       exit; 
		}
	   	fclose($fp);
		$status = true;
	}
	
	//echo $to."<br>";echo $subject."<br>";echo $vBody."<br>";echo $headers."<br>";
	$status = @mail($to, $subject, $vBody, $headers);
	//echo "<pre>";print_r($_SERVER);exit;
	if($_SERVER['HTTP_HOST'] == $local_siteip)
		$status = true;

	return $status;
}


#--------------------------
# Strings of only string
#--------------------------

function getRandLetters($length)
{
  if($length>0)
  {
  $rand_id="";
   for($i=1; $i<=$length; $i++)
   {
	   mt_srand((double)microtime() * 1000000);
	   $num = mt_rand(1,26);
	   $rand_id .= assign_rand_value($num);
   }
  }
return $rand_id;
}
#--------------------------
#--------------------------
# Strings of only numbers/string mix
#--------------------------
function getRandId($length,$table_name,$field_name)
{
	global $sqlObj;
  if($length>0)
  {
  $rand_id="";
   for($i=1; $i<=$length; $i++)
   {
   mt_srand((double)microtime() * 1000000);
   $num = mt_rand(1,36);
   $rand_id .= assign_rand_value($num);
   }
  }
   $SNumber_query = "select $field_name from $table_name where $field_name= '".$rand_id."'";
   $SNumber_rec = $sqlObj->select($SNumber_query);
	if (count($SNumber_rec)>0) 	{
		$SNumber=getRandId($length);
		}
	return $rand_id;
}


#-----------------------------------------------------------------
#-----------------------------------------------------------------------------------------
# Active/Inactive Static Combobox : (Usage : echo DisplayStatus($eGender,eGender)
#----------------------------------------------------------------------------------------
$vstatus = array('Active','Inactive');

function getStatusDropDown($selstatus,$fieldId){

	global $vstatus;

	if(empty($selstatus))
	$selstatus = "Active";

	$statuscombo = "";
	$statuscombo .= "<select name=\"$fieldId\">";

	for($i=0;$i<count($vstatus);$i++) {

		if (trim($selstatus) == trim($vstatus[$i])) {
			$statuscombo .= "<option value=".$vstatus[$i]." selected>".$vstatus[$i];
		} else {
			$statuscombo .= "<option value=".$vstatus[$i].">".$vstatus[$i];
		}
	}
	$statuscombo .= "</select>";
	return $statuscombo;
}
  
#----------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------------
# Yes/No Static Combobox : (Usage : echo DisplayYesNo($eFeatured,eFeatured)
#----------------------------------------------------------------------------------------
$byesno = array('Yes','No');

function getYesNoDropDown($selyesno,$fieldId){

	global $byesno;

	if(empty($selyesno))
		$selyesno = "No";

	$yesnocombo = "";
	$yesnocombo .= "<select name=\"$fieldId\">";

	for($i=0;$i< count($byesno);$i++) {

		if (trim($selyesno)==trim($byesno[$i])) {
			$yesnocombo .= "<option value=".$byesno[$i]." selected>".$byesno[$i];
		} else {
			$yesnocombo .= "<option value=".$byesno[$i].">".$byesno[$i];
		}
	}
	$yesnocombo .= "</select>";
	return $yesnocombo;
}
#-----------------------------------------------------------------------------------------


function removeSlashes($keyword)
{
	return stripslashes(stripslashes(stripslashes(trim($keyword))));
}

#-----------------------------------------------------------------
# THIS FUNCTION GENERATE COMBO OF NUMERIC VALUE IN GENERAL SETTING SECTION
# CAREATED ON 12:25 PM 7/5/2005 BY NIRAV
#-----------------------------------------------------------------
function generalSettingCombo($selday="",$fieldId="",$limitStart="",$limitEnd="") {

	$daycombo = "";
	$daycombo .= "<select name=\"$fieldId\">";
	for($i=$limitStart;$i<=$limitEnd;$i++) {

		if (trim($selday)==trim($i))
		 {
			$daycombo .= "<option value=".$i." selected>".$i;
		} else {
			$daycombo .= "<option value=".$i.">".$i;
		}

	}
	$daycombo .= "</select>";
	return $daycombo;
}

function generateDropDownofArray($arr, $field_id, $sel_value="", $sel_attributes="")
{
	$str = "<select name='$field_id' $sel_attributes>";
	foreach($arr as $key=>$val)
	{
		$str .='<option value="'.htmlentities($val).'" '. ($val==$sel_value?"selected":"") .'>'.$val."</option>";
	}
	$str .="</select>";
	return $str;
}

function displayData($text)
{
	return stripslashes($text);
}

function dataEncode($text)
{
	return urlencode($text);
}
function dataDecode($text)
{
	return urldecode($text);
}

##GET URL USING PHP
function curPageURL()
{
	$pageURL = 'http';
	if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80"){
 		 $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 	}else{
 		 $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}
##


function changeKey($tobjCart)
{
	if(is_array($tobjCart) && count($tobjCart) > 0)
	{
		$elem =0;
		foreach($tobjCart as $key=>$val)
		{
		  	if($tobjCart[$key]->iProductId != "")
		  	{
		  	$TempobjCart[$elem] = $val;
		  	$elem++;
		    }
		}
	}
	return $TempobjCart;
}
function getPostForm($POST_Arr,$msg="",$action="", $exclude_var)
{
	$exclude_arr = explode(",", $exclude_var);
	
	$str ='
	<html>
	<form name="frm1" action="'.$action.'" method=post>';
		foreach($POST_Arr as $key => $value){
			if(!in_array($key, $exclude_arr)){
				if(is_array($value)){
					for($i=0;$i<count($value);$i++)
						$str .='<br><input type="Hidden" name="'.$key.'[]" value="'. stripslashes($value[$i]) . '">';
				}else{
					$str .='<br><input type="Hidden" name="'.$key.'" value="'. stripslashes($value) . '">';
				}
			}
		}
		$str .='<input type="Hidden" name=var_msg value="'. $msg . '">
	</form>
	<script>
		document.frm1.submit();
	</script>
	</html>';
	echo $str ;
	exit;
}

function replace_content($vTitle)
{
	$find_arr = array("/", "'", "(", ")", "?", "-", "#", ",", ";", ":", "'", "\\", " ", "&", "!","%");
	$repl_arr = array("",  "",   "",  "",  "", "_",  "",  "",  "",  "",  "",  "", "-", "and", "", "");
	$final_str = str_replace($find_arr, $repl_arr, $vTitle);
	return $final_str;
}

// ***\\
if(!function_exists(timeBetween))   
{   
    function timeBetween($start_date,$end_date)   
    {   
	
        $diff = $end_date-$start_date;   
        $seconds = 0;   
        $hours   = 0;   
        $minutes = 0;   
        if($diff % 86400 <= 0){$days = $diff / 86400;}  // 86,400 seconds in a day   
		
        if($diff % 86400 > 0)   
        {   
            $rest = ($diff % 86400);   
            $days = ($diff - $rest) / 86400;   
            if($rest % 3600 > 0)   
            {   
                $rest1 = ($rest % 3600);   
                $hours = ($rest - $rest1) / 3600;   
                if($rest1 % 60 > 0)   
                {   
                    $rest2 = ($rest1 % 60);   
                $minutes = ($rest1 - $rest2) / 60;   
                $seconds = $rest2;   
                }   
                else{$minutes = $rest1 / 60;}   
            }   
            else{$hours = $rest / 3600;}   
        }   
  
        if($days > 0){$days = $days.' days, ';}   
        else{$days = false;}   
        if($hours > 0){$hours = $hours.' hours, ';}   
        else{$hours = false;}   
        if($minutes > 0){$minutes = $minutes.' minutes, ';}   
        else{$minutes = false;}   
        $seconds = $seconds.' seconds';
  
        return $days.''.$hours.''.$minutes.''.$seconds;   
    }   
}
function displayPagingBottom1($showLetter="N", $showPaging="Y", $width="100%")
{
	global $recmsg, $page_link;
	if($showPaging=="N")
		$style = "style=display:none";
	else
		$style = "style=display:''";
	echo '<table width="'.$width.'" cellpadding="0" cellspacing="0" border="0">
		<tr style="height:20px">
			<td width="32%" class="disprecordmsg"><span '.$style.' class="disprecordmsg"  id="RECMSG_BOTTOM">'.(($showPaging=="Y")?$recmsg:"").'&nbsp;</span></td>
			<td style="text-align:Center" nowrap><span '.$style_alpha.' class="disprecordmsg"  id="ALPHA_PAGING_BOTTOM">'.(($showLetter=="N")?getSearchByLetter():"").'&nbsp;</span></td>
			<td width="32%" style="text-align:right"><span '.$style.' class="errormsg"   id="PAGING-BOTTOM">'.(($showPaging=="Y")?$page_link:"").'&nbsp;</span></td>
		</tr>
		</table>';
}


function CC_Masking($str)
{
   $sRes = $str;
   $iLen = strlen($str);
   if ( $iLen>4 )
   	$sRes = str_repeat('x', $iLen-4 ).substr($str, -4);
	return $sRes;
}
function CVVV_Masking($str)
{
   $sRes = $str;
   $iLen = strlen($str);
   if ( $iLen>1 )
   	$sRes = str_repeat('x', $iLen-0 ).substr($str, 3);
	return $sRes;
}
function getCreditCardName($code){
	$cardname = "";
	if($code == "VI"){
		$cardname = "Visa";
	}
	else if($code == "MC"){
		$cardname = "Mastercard";
	}
	else if($code == "AX"){
		$cardname = "American Express";
	}
	else if($code == "DI"){
		$cardname = "Discover";
	}
	else if($code == "DC"){
		$cardname = "Diners Club";
	}
	else if($code == "W3"){
		$cardname = "Tiger Preferred Gold";
	}
	return $cardname;
}

## Generate Ranomly Number
function gen_getUniqueId($len="")
{
	//if($len =="")$len=15;
	$better_token = strtoupper(md5(uniqid(rand(), true)));
	if($len!="")
		$better_token = substr($better_token, 1, $len);
	return $better_token;
}


function gen_autoLogin($module, $id, $load_msg='')
{
	global $site_url, $sqlObj, $SITE_TITLE;
	$str = '';
	if($module=='Customer'){
		$sql = "select vUsername as user, vPassword as pass from customer where iCustomerId='$id' limit 1";
		$form_action = $site_url."index.php?page=c-login";
	}
	else	if($module=='Operator'){
		$sql = "select vUsername as user, vPassword as pass from operator where iOperatorId='$id' limit 1";
		$form_action = $site_url."index.php?page=c-login";
	}
	else if($module=='GroupOperator'){	
		$sql = "select vUsername as user, vPassword as pass, iWOwnerId from website_owner_group where iWOGroupId = '$id' limit 1"; 
		$form_action = $site_url."index.php?page=c-login";
	}
	else if($module=='GroupUser'){	
		$sql = "select vUsername as user, vPassword as pass, iGUserId from group_user where iGUserId = '$id' limit 1"; 
		$form_action = $site_url."index.php?page=c-login";
	}
	//echo $sql;exit;
	if($sql !=''){
		$rs_sql = $sqlObj->select($sql);
	}
	if(count($rs_sql)>0){
		$user = $rs_sql[0]['user'];
		$pass = decrypt($rs_sql[0]['pass']);
		
		$str = '<html>';
		$str .= '<head><title>'. $SITE_TITLE .'</title></head>';
		$str .= '<style>
body{margin:0; background:#F2F2F2; font-family:Tahoma, Arial, Helvetica, sans-serif; padding:0; font-size:12px; color:#000000}
</style>';
		$str .= '<body>';
		$str .= '<br><br><br><br><br><br><br><br><br>';
		$str .= '<center><strong>Logging in</strong><br><br>Please wait your page is loading....</center>';
		if($load_msg !='')
		{
			$str .= '<script>alert("'. str_replace('"', '\"', $load_msg) .'");</script>';
		}
		if($module=='Customer' || $module=='Operator' || $module = 'GroupUser')
		{
			$str .= '<form name="frmlogin" method="post" action="'. $form_action .'">';
			$str .= '<input type="hidden" name="vUsername" value="'. addslashes(htmlentities($user)) .'">';
			$str .= '<input type="hidden" name="vPassword" value="'. addslashes(htmlentities($pass)) .'">';
			$str .= '<input type="hidden" name="btnlogin" value="Login">';
			$str .= '</form>';
			$str .= '<script>document.frmlogin.submit();</script>';
		}
		$str .= '</body></html>';
	}
	echo $str;
	exit;
}

#--------------------------------------------
#   array2json
#---------------------------------------------
function array2json($arr) { 
    if(function_exists('json_encode')) return json_encode($arr); //Lastest versions of PHP already has this functionality. 
    $parts = array(); 
    $is_list = false; 

    //Find out if the given array is a numerical array 
    $keys = array_keys($arr); 
    $max_length = count($arr)-1; 
    if(($keys[0] == 0) and ($keys[$max_length] == $max_length)) {//See if the first key is 0 and last key is length - 1 
        $is_list = true; 
        for($i=0; $i<count($keys); $i++) { //See if each key correspondes to its position 
            if($i != $keys[$i]) { //A key fails at position check. 
                $is_list = false; //It is an associative array. 
                break; 
            } 
        } 
    } 

    foreach($arr as $key=>$value) { 
        if(is_array($value)) { //Custom handling for arrays 
            if($is_list) $parts[] = array2json($value); /* :RECURSION: */ 
            else $parts[] = '"' . $key . '":' . array2json($value); /* :RECURSION: */ 
        } else { 
            $str = ''; 
            if(!$is_list) $str = '"' . $key . '":'; 

            //Custom handling for multiple data types 
            if(is_numeric($value)) $str .= $value; //Numbers 
            elseif($value === false) $str .= 'false'; //The booleans 
            elseif($value === true) $str .= 'true'; 
            else $str .= '"' . addslashes($value) . '"'; //All other things 
            // :TODO: Is there any more datatype we should be in the lookout for? (Object?) 

            $parts[] = $str; 
        } 
    } 
    $json = implode(',',$parts); 
     
    if($is_list) return '[' . $json . ']';//Return numerical JSON 
    return '{' . $json . '}';//Return associative JSON 
}

function gen_removeDir($fpath, $path1)
{
	$path= rtrim($fpath, '/').'/';
	$handle = opendir($path);
	for (;false !== ($file = readdir($handle));){
		if($file != "." and $file != ".." )
        	{
			$fullpath = $path.$file;
			if(is_dir($fullpath)){
				gen_removeDir($fullpath, $path1);
				echo "<br>D:".$fullpath;
                	@rmdir($fullpath);
            	}
            	else{
				echo "<br>".$fullpath;
              		unlink($fullpath);
			}
        	}
	}
    	closedir($handle);
	if(is_dir($path1))
    	{
		echo "<br>LD:".$path1;
        	@rmdir($path1);
    	}
}
# --------------------------------------------------------
#  Insert data in tranasction table if client_deposit status = Finished
#---------------------------------------------------------
/*function gen_addTransactionFees($iRefId, $eRefType, $fAmount, $vReason, $eCrDb, $eStatus, $dTranDate)
{
	global $sqlObj;
	$sql_chk = "select iTFeesId from transaction_fees where iRefId='$iRefId' and eRefType='$eRefType' limit 1";
	$db_chck = $sqlObj->select($sql_chk);
	
	if(count($db_chck)==0)
	{
		 $sql_tran = "INSERT INTO transaction_fees (iRefId, eRefType, fAmount, vReason, eCrDb, eStatus, dTranDate) VALUES ('".$iRefId."','".$eRefType."','".$fAmount."','".$vReason."','".$eCrDb."','".$eStatus."','".$dTranDate."') ";
	
		$db_sql = $sqlObj->insert($sql_tran);
		$iTFeesId = $db_sql;
		
		if($iTFeesId)
		{ 
			$upd_tran = "UPDATE transaction_fees SET  vInvoiceId= 'FEE-".$iTFeesId."' WHERE iTFeesId = '".$iTFeesId."' limit 1";
			$db_upd = $sqlObj->execute($upd_tran);
			return $db_upd;
		}else{
			return 0;
		}
	}
		return 2;
}*/
function gen_detailListing($value)
{
	$send_val="";
	
	$tDetail =$value;
	$val_tdetail=explode("|||",$tDetail);
	for($i=0;$i<count($val_tdetail);$i++)
	{
		$final_detail[$i][]=explode("::",$val_tdetail[$i]);
	}
	
	for($i=0;$i<count($final_detail);$i++)
	{
		if($final_detail[$i][0][1] != ""  )
			$send_val.=$final_detail[$i][0][0]." : ".$final_detail[$i][0][1]."<br>";
		elseif($final_detail[$i][0][0] != ""  ) 
			$send_val=$final_detail[$i][0][0]."<br>";
	}
	echo $send_val;
}
function gen_checkBlankValidation($data)
{
	if($data == "" || $data=="NULL")
	{
		return "----";
	}
	else
	{
		return $data;
	}
}

function gen_getUserNameByUserIdUserType($iUserId, $eUserType)
{
	global $sqlObj;
	
	$name = "";
	$tablename = "";
	$fieldname = "";
	$pId = "";
	switch($eUserType)
	{	
		case "C":
			$tablename = "customer";
			$fieldname = "concat(vFirstName, ' ', vLastName)";
			$pId = "iCustomerId";
			break;
		case "O":
			$tablename = "operator";
			$fieldname = "vName";
			$pId = "iOperatorId";
			break;
	}

	if($tablename != "" && $fieldname != "" && $pId != "" && $iUserId != "" && $eUserType != "")
	{
		$sql_name = "select ".$fieldname." as Name from ".$tablename." where ".$pId." = '".$iUserId."' limit 1";
		$db_name = $sqlObj->select($sql_name);
		
		if($db_name)
			$name = $db_name[0]['Name'];
	}

	return $name ;
}
//created by veerendra raghuvanshi//
//function to convert 1 & 0 into Yes & No
function gen_status($data)
{
	$status_arr = array("1"=>"Yes", "0"=>"No");
	$short_status_arr = array("1","0");
	if(in_array($data, $short_status_arr))
		return $status_arr[$data];
	else
		return "---";
}
#-----------------------------------------------------------------
# Random Alphanumeric String Generator
#-----------------------------------------------------------------
function assign_rand_value($num)
{

  switch($num)
  {
    case "1":
     $rand_value = "a";
    break;
    case "2":
     $rand_value = "b";
    break;
    case "3":
     $rand_value = "c";
    break;
    case "4":
     $rand_value = "d";
    break;
    case "5":
     $rand_value = "e";
    break;
    case "6":
     $rand_value = "f";
    break;
    case "7":
     $rand_value = "g";
    break;
    case "8":
     $rand_value = "h";
    break;
    case "9":
     $rand_value = "i";
    break;
    case "10":
     $rand_value = "j";
    break;
    case "11":
     $rand_value = "k";
    break;
    case "12":
     $rand_value = "l";
    break;
    case "13":
     $rand_value = "m";
    break;
    case "14":
     $rand_value = "n";
    break;
    case "15":
     $rand_value = "o";
    break;
    case "16":
     $rand_value = "p";
    break;
    case "17":
     $rand_value = "q";
    break;
    case "18":
     $rand_value = "r";
    break;
    case "19":
     $rand_value = "s";
    break;
    case "20":
     $rand_value = "t";
    break;
    case "21":
     $rand_value = "u";
    break;
    case "22":
     $rand_value = "v";
    break;
    case "23":
     $rand_value = "w";
    break;
    case "24":
     $rand_value = "x";
    break;
    case "25":
     $rand_value = "y";
    break;
    case "26":
     $rand_value = "z";
    break;
    case "27":
     $rand_value = "0";
    break;
    case "28":
     $rand_value = "1";
    break;
    case "29":
     $rand_value = "2";
    break;
    case "30":
     $rand_value = "3";
    break;
    case "31":
     $rand_value = "4";
    break;
    case "32":
     $rand_value = "5";
    break;
    case "33":
     $rand_value = "6";
    break;
    case "34":
     $rand_value = "7";
    break;
    case "35":
     $rand_value = "8";
    break;
    case "36":
     $rand_value = "9";
    break;
  }
return $rand_value;
}
# GET PAYMENT METHOD NAME
#----------------------------------------------
function gen_getPayProcessor($type)
{
	if ($type=="AN")
	  $payMName = "Authorized.net";
	else if($type=="LP")
		$payMName = "Link Pointer";
	else
	  $payMName = $type;
	  
	  return $payMName; 
}
#----------------------------------------------
function getRandValues($length)
{
  if($length>0)
  {
  $rand_id="";
   for($i=1; $i<=$length; $i++)
   {
	   mt_srand((double)microtime() * 1000000);
	   $num = mt_rand(27,36);
	   $rand_id .= assign_rand_value($num);
   }
  }
return $rand_id;
}

/*function gen_status($data)
{
	$status_arr = array("A"=>"Active", "I"=>"Inactive", "P"=>"Pending", "B"=>"Block","U"=>"Unpaid","PA"=>"Paid","Y"=>"Yes","N"=>"No","CL"=>"Closed","C"=>"Cancelled","S"=>"Salary","H"=>"Hourly");
	$short_status_arr = array("A","I","P","B","U","PA","Y","N","CL","C","S","H");
	if(in_array($data, $short_status_arr))
		return $status_arr[$data];
	else
		return "---";
}*/

function avtivate_themes(){
	mysql_connect("localhost","mpsbeta_talk2","T7T?d1K#4mQy");
	mysql_select_db("mpsbeta_talk1");
	$sql = "select activate_theme as theme from admin";   
	$rs_sql = mysql_query($sql);
	$rs_sql_fetch = mysql_fetch_assoc($rs_sql);
	$act_theme = $rs_sql_fetch['theme'];
	return $act_theme;
}

/*********Start Function For IVR to add funds from credit cards************/
function add_funds_using_IVR($amount, $credit_card_id, $user_type, $user_id)
{ 
	global $sqlObj;
    $response_data = array();
    
    if($user_type=="Customer"){
    	$ivr_user_type = "C";
    	$user_table = "customer";
    	$where_id = "iCustomerId";
    }else if($user_type=="Operator"){
    	$ivr_user_type = "C";
    	$user_table = "operator";
    	$where_id = "iOperatorId";
    }

    $cust_ip = getIP();


    $sql_save_cards = "select * from credit_card_information where id = '".$credit_card_id."' AND user_id = '".$user_id."' and user_type ='".$user_type."' ";
    
    
    $save_cards = $sqlObj->select($sql_save_cards);
    
    $cc_number=decryptCreditCard($save_cards[0]['cc_number']);
    $cc_cvv=decryptCreditCard($save_cards[0]['cc_cvv']);

    $cc_expiry=explode("/",decryptCreditCard($save_cards[0]['cc_expiry']));
    $cc_expiry_month=$cc_expiry[0];
    $cc_expiry_year=$cc_expiry[1];

    $year_last_2_digits = substr( $cc_expiry_year, -2);

    $cc_type =$save_cards[0]['cc_type'];
     $name_on_card =$save_cards[0]['name_on_card'];
      $address1 =$save_cards[0]['address1'];
      $address2 =$save_cards[0]['address2'];
      $city =$save_cards[0]['city']; 
      $country =$save_cards[0]['country'];
       $state =$save_cards[0]['state']; 
       $zip_code =$save_cards[0]['zip_code'];




        $card_info = array(

            'card_number'       => $cc_number,
            'card_exp_date'     => $cc_expiry_month.'/'.$year_last_2_digits,
            'Card_cvvCode'      => $cc_cvv,
            'Address1'          => $address1,
            'Address2'          => $address2,
            'City'              => $city,
            'State'             => $state,
            'zip_code'          => $zip_code,
            'country'          => $country

            );

    $customer_information_sql = "select vEmail from ".$user_table."  where  ".$where_id." = '".$user_id."' ";
    
    
    $customer_information = $sqlObj->select($customer_information_sql);
    $cust_email =$customer_information[0]['vEmail'];
    $cust_phone ="";

$op_new =  new OrbitalPayNew("C","S","120313912537",$cust_email,$cust_phone,$cust_ip,$amount,"");

 $st_response = $op_new->addcc($card_info);

$decoded_response = json_decode($st_response);
//print_r($decoded_response);


$res_return = '';
        if(isset($decoded_response->status_code) && $decoded_response->status_code=="1" )
        {
            
            $cust_ip = getIP();
			$today_date = date('Y-m-d');
			$entry_date = date('Y-m-d H:i:s');
            //$entry_fund = "INSERT INTO advisor_minutes set offerId =0, type ='Credit Card', senderId=0, senderType='', listId='0', recieverId ='".$user_id."', Offertype ='eMoney', free_minutes ='".$amount."', addedDate ='".$today_date ."', used =1 ";

            $entry_fund = "INSERT INTO balance set payment_user_id =0, iUserId ='".$user_id."', eUserType='".$ivr_user_type."', eBalType='C', fCreditAmount='".$amount."', fDebitAmount ='0', fAdminCommision ='0', fAdminCommisionPercentage ='0', Total ='0', dEntryDate ='".$entry_date."', dValueDate ='".$today_date."', vDescritption ='Credit card', vBalTypeCode ='D', iAddedUserId ='".$user_id."', eAddedUserType ='".$ivr_user_type."', eBalanceStatus ='A', eCallId ='0' ";

                $sqlObj->insert($entry_fund);

			

		   
			$res_return = json_encode(array("status" => 1 ,"data"=> 'Fund is added successfully' ) );

        }elseif (isset($decoded_response->status_code) && $decoded_response->status_code=="0") {
            $res_return = json_encode(array("status" => 0 ,"data"=> "Entered Card is Declined." ) );
            
        }elseif(isset($decoded_response->transaction_error) && !empty($decoded_response->transaction_error) ){
            $res_return = json_encode(array("status" => "error" ,"data"=> $decoded_response->transaction_error ) );
        }
        else{
        	$res_return = json_encode(array("status" => "error_unknown" ,"data"=> "Something went wrong" ) );
        }

    return $res_return;

}


/*********Ends Function For IVR to add funds from credit cards************/



/*********Starts Function For IVR to add credit cards ************/

function add_credit_cards_using_IVR($user_id, $user_type, $cc_number, $cc_cvv, $cc_exp_date, $cc_type)
{
    global $sqlObj;
    $response_data = array();
    
    if($user_type=="Customer"){
    	$user_table = "customer";
    	$where_id = "iCustomerId";
    }else if($user_type=="Operator"){
    	$user_table = "operator";
    	$where_id = "iOperatorId";
    }

    $cust_ip = getIP();

    $sql_save_cards = "select * from credit_card_information where user_id = '".$user_id."' and user_type ='".$user_type."' and cc_number='".encryptCreditCard($cc_number)."' AND enable=1 ";
    
    $save_cards = $sqlObj->select($sql_save_cards);
    $num_rows = count($save_cards);
    if($num_rows > 0 ){

        echo json_encode(array("status" => "exist" ,"data"=> "Entered Card is Already Exists.Please add another card." ) );

    }
    else{

    		$cc_expiry=explode("/",$cc_exp_date);
    		$cc_expiry_month=$cc_expiry[0];
    		$cc_expiry_year=$cc_expiry[1];

    		$year_last_2_digits = substr( $cc_expiry_year, -2);

    	        $card_info = array(

            'card_number'       => $cc_number,
            'card_exp_date'     => $cc_expiry_month.'/'.$year_last_2_digits,
            'Card_cvvCode'      => $cc_cvv,
            'Address1'          => '',
            'Address2'          => '',
            'City'              => '',
            'State'             => '',
            'zip_code'          => '',
            'country'          => ''

            );

        $customer_information_sql = "select vEmail from ".$user_table."  where  ".$where_id." = '".$user_id."' ";
    
    
    $customer_information = $sqlObj->select($customer_information_sql);
    $cust_email =$customer_information[0]['vEmail'];
    $cust_phone = ""; 

$op_new =  new OrbitalPayNew("C","S","120313912537",$cust_email,$cust_phone,$cust_ip,1,"");

 $st_response = $op_new->addcc($card_info);

$decoded_response = json_decode($st_response);
//print_r($decoded_response);
        if(isset($decoded_response->status_code) && $decoded_response->status_code=="1" )
        {
            
                $enc_card_num = encryptCreditCard($cc_number);
                $enc_Card_cvvCode = encryptCreditCard($cc_cvv);
                $enc_card_exp_date_full_year = encryptCreditCard($cc_exp_date);

                $delete_exist_card = " DELETE FROM credit_card_information  WHERE cc_number='".$enc_card_num."' AND enable = 0 ";

                $sqlObj->execute($delete_exist_card);

    $save_card = "INSERT INTO credit_card_information set user_id ='".$user_id."', user_type ='".$user_type."', cc_number='".$enc_card_num."', cc_cvv='".$enc_Card_cvvCode."', cc_expiry='".$enc_card_exp_date_full_year."', cc_type ='".$cc_type."', name_on_card ='', address1 ='', address2 ='', city ='', country ='', state ='', zip_code ='' ";

                $sqlObj->insert($save_card);

                

$op_new_refund =  new OrbitalPayNew("C","R","120313912537",$cust_email,$cust_phone,$cust_ip,1,$decoded_response->trans_id);

 $st_response_refund = $op_new_refund->addcc($card_info);
//echo $st_response_refund;
$decoded_response_refund = json_decode($st_response_refund);
//print_r($decoded_response);die();

                echo json_encode(array("status" => 1 ,"data"=> "Credit card is added successfully" ) );
                            



        }elseif (isset($decoded_response->status_code) && $decoded_response->status_code=="0") {
            echo json_encode(array("status" => 0 ,"data"=> "Entered Card is Declined." ) );
            
        }elseif(isset($decoded_response->transaction_error) && !empty($decoded_response->transaction_error) ){
            echo json_encode(array("status" => "error" ,"data"=> $decoded_response->transaction_error ) );
        }
        else{
        	echo json_encode(array("status" => "error_unknown" ,"data"=> "Something went wrong" ) );
        }

    }


}

/*********Ends Function For IVR to add credit cards ************/

/********* Starts Function to get the customer's favourite Advisors (Operators) by customer ID  ************/

function get_favourite_advisors_by_customer_id($customer_id)
{	global $sqlObj;
	$final_array = array();
	 $favourite_advisors_information_sql = "select iOperatorId, listid from customer_operator_favorite where iCustomerId  = '".$customer_id."' AND iFavouriteBy ='Customer' ";
    
    $favourite_advisors_information = $sqlObj->select($favourite_advisors_information_sql);
    //echo "kkkk<pre>";print_r($favourite_advisors_information); 

    foreach($favourite_advisors_information as $fav_adv){

		//echo "<pre>";print_r($fav_adv);echo "</pre>";
		$favourite_advisors = "select iOperatorId, vUsername, iExtensionkey, oPhoneStatus from operator where iOperatorId  = '".$fav_adv['iOperatorId']."' ";
		
		$favourite_advisors_row = $sqlObj->select($favourite_advisors);
		$single_arr = array("oid"=>$favourite_advisors_row[0]['iOperatorId'], "oname"=>$favourite_advisors_row[0]['vUsername'], "extension_key"=>$favourite_advisors_row[0]['iExtensionkey'], "listid"=>$fav_adv['listid'], "oPhoneStatus" => $favourite_advisors_row[0]['oPhoneStatus'] );
		array_push($final_array, $single_arr);

    }
    if(!$final_array)
		array_push($final_array, array("data" =>"No result found"));	
    //print_r($final_array);
	return $final_array;
    //echo json_encode($final_array);
   

}

/********* Ends Function to get the customer's favourite Advisors (Operators) by customer ID  ************/
?>
