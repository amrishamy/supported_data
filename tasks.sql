-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 30, 2019 at 06:47 AM
-- Server version: 5.7.27-0ubuntu0.16.04.1
-- PHP Version: 7.0.33-0ubuntu0.16.04.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tasks`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `folder_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `notify` int(11) DEFAULT '0',
  `is_read` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `user_id`, `folder_id`, `task_id`, `notify`, `is_read`, `created_at`, `updated_at`) VALUES
(10, 'This Task is Done.Please check and let me know.', 60, 19, 23, NULL, 0, '2019-05-13 07:17:39', '2019-05-13 07:17:39'),
(11, 'This Task is Done. Please check and mark complete.', 60, 19, 22, NULL, 0, '2019-05-13 07:18:39', '2019-05-13 07:18:39'),
(12, 'This Task is Done. Please check and Mark complete.', 60, 19, 19, NULL, 0, '2019-05-13 07:30:48', '2019-05-13 07:30:48'),
(13, 'This Task is Done. Please check and Mark complete.', 60, 19, 18, NULL, 0, '2019-05-13 07:31:57', '2019-05-13 07:31:57'),
(14, 'This Task is Done. Please check and Mark complete.', 60, 19, 21, NULL, 0, '2019-05-13 07:32:36', '2019-05-13 07:32:36'),
(15, 'Need to work on this', 60, 19, 20, NULL, 0, '2019-05-13 07:33:19', '2019-05-13 07:33:19'),
(16, 'This Task is Done.', 60, 19, 31, NULL, 0, '2019-05-13 10:43:44', '2019-05-13 10:43:44'),
(17, 'Okay', 1, 19, 31, 0, 0, '2019-05-13 11:01:13', '2019-05-13 11:01:13'),
(18, 'This Task is Done.', 60, 19, 35, 0, 0, '2019-05-13 11:03:11', '2019-05-13 11:03:11'),
(19, 'This Task is Done.', 60, 19, 33, 0, 0, '2019-05-13 11:19:18', '2019-05-13 11:19:18'),
(20, 'Because On Desktop view there will be "two folder view" scenario. Means on Mobile screen there will be appear one folder and related tasks and on Desktop screen there will be appear two folder (Left/Right) and related tasks.', 60, 19, 30, 0, 0, '2019-05-13 11:34:41', '2019-05-13 11:34:41'),
(21, 'There are two "Delete" buttons. The "Delete" button in the end of all Tasks is for Folder Delete and second one is for Delete the specific task and appears when you open the task by clicking the title.\r\n\r\nSo task delete button is already there to delete the specific task', 60, 19, 29, 0, 0, '2019-05-13 12:56:18', '2019-05-13 12:56:18'),
(22, 'As It is the Task Title with input field so It will display in the input field like displaying currently.\r\nIf you want it in Multiple Lines then I need to use the Textarea field for saving the task title. Please let me know your views on same.', 60, 19, 34, 0, 0, '2019-05-13 13:27:50', '2019-05-13 13:27:50'),
(23, 'As It is the Task Title with input field so It will display in the input field like displaying currently.\r\nIf you want it in Multiple Lines then I need to use the Textarea field for saving the task title. Please let me know your views on same.', 60, 19, 34, 0, 0, '2019-05-13 13:27:50', '2019-05-13 13:27:50'),
(24, 'Now all the icon are hidden initially and when you will select the text/words then toolbar will show so that you can format accordingly. Please check it and let me know', 60, 19, 32, 0, 0, '2019-05-14 06:03:05', '2019-05-14 06:03:05'),
(25, 'This is Done.', 60, 19, 32, 0, 0, '2019-05-14 14:35:22', '2019-05-14 14:35:22'),
(26, 'Testing', 1, 19, 20, 0, 0, '2019-05-15 05:06:07', '2019-05-15 05:06:07'),
(27, 'hi', 1, 19, 42, 0, 0, '2019-05-17 05:31:06', '2019-05-17 05:31:06'),
(28, 'Please advise', 1, 22, 97, 0, 0, '2019-07-10 06:46:01', '2019-07-10 06:46:01'),
(30, 'Test comment', 1, 19, 126, 0, 1, '2019-07-16 01:57:32', '2019-07-16 01:57:54');

-- --------------------------------------------------------

--
-- Table structure for table `folders`
--

CREATE TABLE `folders` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `children_ids` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `root` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0-root, 1-child',
  `order` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `folders`
--

INSERT INTO `folders` (`id`, `name`, `children_ids`, `root`, `order`, `created_at`, `updated_at`, `created_by`, `is_active`) VALUES
(19, 'Tasks Project', '', 0, 3, '2019-05-10 05:29:50', '2019-07-02 07:46:55', 1, 0),
(21, 'Tyler', '', 0, 7, '2019-05-11 00:39:53', '2019-07-02 07:47:16', 1, 0),
(22, 'German', '', 0, 6, '2019-05-11 00:43:52', '2019-07-02 07:47:16', 1, 0),
(23, 'OTM', '', 0, 2, '2019-05-14 11:11:04', '2019-07-02 07:46:55', 1, 0),
(24, 'Joe', '', 0, 1, '2019-06-04 16:05:19', '2019-07-02 07:46:55', 1, 0),
(25, 'Manufacturing - MOKO', '', 0, 5, '2019-06-04 16:07:49', '2019-07-02 07:46:55', 1, 0),
(26, 'Zeal', '', 0, 4, '2019-06-04 16:11:48', '2019-07-02 07:46:55', 1, 0),
(27, 'Kenny', '', 0, 8, '2019-06-24 20:51:57', '2019-07-02 07:47:16', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2019_04_02_080207_add_status_to_users', 2),
(6, '2019_04_09_170408_create_roles_users_table', 3),
(7, '2019_04_09_170417_create_roles_table', 3),
(9, '2019_04_12_171723_create_jobs_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('pawanvir.impinge@gmail.com', '$2y$10$qVusQGg5x2GLpzuFgClLuOFjleGXHmr..hy44RorRvb.RjVLmAX4C', '2019-07-16 09:28:18'),
('german@silentbeacon.com', '$2y$10$ZVjBUmaYhPuqi3IzLuM9XetkpGyfBb3jOvQsmsVqCg9a3MMQLNJIK', '2019-07-16 09:52:43');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles_users`
--

CREATE TABLE `roles_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `folder` int(11) NOT NULL DEFAULT '0',
  `assigned_to` int(11) DEFAULT '0',
  `due_date` timestamp NULL DEFAULT NULL,
  `reminders` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` longtext COLLATE utf8mb4_unicode_ci,
  `uploads` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notify` int(11) DEFAULT '0',
  `admin_approval` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `name`, `created_by`, `status`, `folder`, `assigned_to`, `due_date`, `reminders`, `notes`, `uploads`, `notify`, `admin_approval`, `created_at`, `updated_at`) VALUES
(18, 'To verify HTML code beautiful', 1, 1, 19, 60, '2019-06-12 10:00:00', NULL, '<p>BEAUTIFUL</p>', NULL, 1, 0, '2019-05-10 09:19:23', '2019-05-27 06:19:00'),
(19, 'Assigned tasks - Notes and due date fields, choose file are editable', 1, 1, 19, 60, '2019-05-10 10:00:00', NULL, '<p>Assigned tasks - Notes and due date fields, choose file are\r\n        editable<br></p>', NULL, 1, 0, '2019-05-10 09:50:15', '2019-05-15 12:58:26'),
(20, 'Forgot Password- Reset password link does not receive in email', 1, 1, 19, 60, '2019-06-20 10:00:00', NULL, '<p>Forgot Password- Reset password link does not receive in\r\n        email<br></p>', NULL, NULL, 0, '2019-05-10 09:51:58', '2019-05-27 06:20:45'),
(21, 'Unnecessary EDIT button on assigned task shows for the user- which behave like EDIT TASK', 1, 1, 19, 60, '2019-05-11 10:00:00', NULL, '<p>Unnecessary EDIT button on assigned task shows for the\r\n        user- which behave like EDIT TASK&nbsp;<br></p>', NULL, 1, 0, '2019-05-10 09:52:41', '2019-05-27 06:16:59'),
(22, 'Admin- Active users- Reset Password- Alert message- Successfully spelling mistake  COMMENTS- Correct it from everywhere where it shows', 1, 1, 19, 60, '2019-06-27 10:00:00', NULL, '<p>Admin- Active users- Reset Password- Alert message-\r\n        Successfully spelling mistake  COMMENTS- Correct it from\r\n        everywhere where it shows<br></p>', NULL, 1, 0, '2019-05-10 09:53:41', '2019-06-07 00:44:18'),
(23, 'Admin- Active users/Pending users- UI issue- email of users are not viewing properly', 1, 1, 19, 60, '2019-05-10 10:00:00', NULL, '<p>Admin- Active users/Pending users- UI issue- email of\r\n        users are not viewing properly<br></p>', NULL, NULL, 0, '2019-05-10 09:54:33', '2019-05-27 06:19:13'),
(28, 'Task App', 1, 1, 22, 62, '2019-06-10 10:00:00', NULL, '<p>Learn how to use and navigate through this app and let me know any suggestions you may have with making this better for your needs.</p>', NULL, 1, 0, '2019-05-11 00:45:20', '2019-05-27 06:20:10'),
(29, 'Hitting DELETE deletes the entire folder... you should be able to delete a task with the check mark... come on', 1, 1, 19, 60, '2019-06-19 10:00:00', NULL, 'Hitting DELETE deletes the entire folder... you should be able to delete a task with the check mark... come on<br>', NULL, NULL, 0, '2019-05-13 05:35:19', '2019-06-07 00:43:50'),
(30, 'Why doesn\'t it open up large on desktop?', 1, 1, 19, 60, '2019-05-31 10:00:00', NULL, 'why doenst it open up large on desktop?<br>', NULL, NULL, 0, '2019-05-13 05:35:39', '2019-05-27 06:17:03'),
(31, 'I keep asking that this is one line.   Kenny made for ________', 1, 1, 19, 60, '2019-06-28 10:00:00', NULL, 'I keep asking that this is one line.   Kenny made for<br>', NULL, NULL, 0, '2019-05-13 05:35:58', '2019-06-07 00:44:24'),
(32, 'Tuck this stuff under one icon to expand. I dont like all that stuff.', 1, 1, 19, 60, '2019-05-15 10:00:00', NULL, 'Tuck this stuff under one icon to expand. I dont like all that stuff.<br>', NULL, NULL, 0, '2019-05-13 05:36:12', '2019-06-07 00:39:03'),
(33, 'You press the title to open the text, you shoudl be able to press it again to close it. You should not have to hit SAVE / DISCARD / DELETE, in fact DISCARD should be removed.', 1, 1, 19, 60, '2019-06-05 10:00:00', NULL, 'You press the title to open the text, you shoudl be able to press it again to close it. You should not have to hit SAVE / DISCARD / DELETE, in fact DISCARD should be removed.<br>', NULL, NULL, 0, '2019-05-13 05:36:32', '2019-06-07 00:43:41'),
(34, 'The text should take up as many lines as needed, not cut off like this title....assigned task, a is cut off, and so is the end, TASK', 1, 1, 19, 60, '2019-06-04 10:00:00', NULL, 'The text should take up as many lines as needed, not cut off like this title....assigned task, a is cut off, and so is the end, TASK<br>', NULL, NULL, 0, '2019-05-13 05:36:49', '2019-06-07 00:43:31'),
(35, 'Notify user by email is mandatory so just remove that as well, user should always be notified', 1, 1, 19, 60, '2019-05-15 10:00:00', NULL, 'Notify user by email is mandatory so just rmeove that as well, user should always be notified<br>', NULL, NULL, 0, '2019-05-13 05:37:07', '2019-06-07 00:39:40'),
(36, 'Are you familiar with Gannt charts?, I want an overall gant chart page that will show all the completion dates and tasks, kinda like these', 1, 1, 19, 60, '2019-06-20 10:00:00', NULL, 'Are you familiar with Gannt charts?, I want an overall gant chart page that will show all the completion dates and tasks, kinda like these<br>', NULL, NULL, 0, '2019-05-13 05:37:25', '2019-05-27 06:16:50'),
(37, 'Get mic from OTM', 1, 1, 22, 62, '2019-06-25 10:00:00', NULL, '<p>For our shoot today </p>', NULL, 0, 0, '2019-05-13 12:22:24', '2019-06-25 11:02:08'),
(38, 'Is the return form on the website and linked ?', 1, 1, 22, 62, '2019-05-13 10:00:00', NULL, '<p>Website needs the RMA form updated.  Add “important” if you have used the device you need to remove from your app.  A fee of 10 dollars may be added to your return if the Silent Beacon is still paired to your phone”</p>', NULL, 0, 0, '2019-05-13 12:26:59', '2019-06-04 16:05:03'),
(42, 'Test Task2_5-17', 1, 1, 19, 60, '2019-05-29 10:00:00', NULL, '<p>Test 2 5 17<br></p>', NULL, 0, 0, '2019-05-17 05:17:39', '2019-05-27 06:16:42'),
(86, '$35 fee on Wells Fargo', 1, 1, 22, 62, '2019-06-05 10:00:00', NULL, NULL, NULL, 0, 0, '2019-06-04 15:58:53', '2019-06-05 12:32:16'),
(87, 'North Eastern Redesign', 1, 1, 22, 62, '2019-06-11 10:00:00', NULL, '<p>Get a meeting with the people in Boston</p><p class="MsoPlainText">1) making enclosure work without needing all this glue.</p>\r\n\r\n<p class="MsoPlainText">2) real buttons that are simple to push <o:p></o:p></p>\r\n\r\n<p class="MsoPlainText"><o:p>&nbsp;</o:p><span style="font-size: 1rem;">3) a way to make the speaker sound louder like our\r\ncompetition (send them a Polk speaker like we did joe)</span></p>\r\n\r\n<p class="MsoPlainText"><o:p></o:p></p><p><span style="font-size: 1rem;">4) See if the clip can be made stronger.</span>&nbsp;</p>', NULL, 0, 0, '2019-06-04 16:00:47', '2019-06-07 11:19:37'),
(88, 'Order a 3D Printer', 1, 1, 22, 62, '2019-06-07 10:00:00', NULL, 'On stand by', NULL, 0, 0, '2019-06-04 16:04:13', '2019-06-25 11:40:18'),
(89, 'Plastic Navi', 1, 0, 22, 62, '2019-06-12 10:00:00', NULL, 'Conversations are in progress', NULL, 0, 0, '2019-06-04 16:04:56', '2019-06-25 11:51:39'),
(90, '1) Finish the senior button design—', 1, 0, 24, 62, NULL, NULL, NULL, NULL, 0, 0, '2019-06-04 16:06:32', '2019-06-04 16:06:32'),
(91, '2) get samples made with zeal and our 3d printer', 1, 0, 24, 62, '2019-06-12 10:00:00', NULL, NULL, NULL, 0, 0, '2019-06-04 16:07:28', '2019-06-04 16:07:28'),
(92, 'Get a firm quote from Nic for 10,000 units', 1, 0, 25, 62, '2019-06-05 10:00:00', NULL, NULL, NULL, 0, 0, '2019-06-04 16:08:16', '2019-06-04 16:08:16'),
(93, 'Pricing', 1, 0, 25, 62, '2019-06-10 10:00:00', NULL, '<p class="MsoPlainText">Lock in the correct BOM and pricing for every single\r\ncomponent and plastic and so forth.<o:p></o:p></p>', NULL, 0, 0, '2019-06-04 16:08:51', '2019-06-04 16:08:51'),
(94, 'Samples', 1, 0, 25, 62, '2019-06-10 10:00:00', NULL, '<p class="MsoPlainText">3) Ask to prepare final samples with new parts, 10.&nbsp; 2 for Northeastern 2 for joe 2 for Gustavo\r\nand the rest for us.&nbsp;<o:p></o:p></p>', NULL, 0, 0, '2019-06-04 16:10:11', '2019-06-04 16:10:11'),
(95, 'Finish Firmware', 1, 0, 26, 63, '2019-06-17 10:00:00', NULL, NULL, NULL, 0, 0, '2019-06-04 16:12:13', '2019-06-04 16:12:13'),
(96, 'Boxes', 1, 1, 22, 62, '2019-06-10 10:00:00', NULL, '<p class="MsoPlainText">Stand-up boxes for 7 devices with perforated edges like we\r\nsaw at Home Depot.&nbsp;<o:p></o:p></p>', NULL, 0, 0, '2019-06-04 16:14:45', '2019-06-25 11:42:53'),
(97, 'Order Sample Boxes', 1, 0, 22, 62, '2019-06-07 10:00:00', NULL, '<p>Where are we wit this?</p>', NULL, 0, 0, '2019-06-04 16:15:18', '2019-07-10 06:30:25'),
(98, 'Inner Outer Boxes', 1, 1, 22, 62, '2019-06-07 10:00:00', NULL, '<p class="MsoPlainText">Get&nbsp; samples of new inner and outer with the new\r\ndimensions.<o:p></o:p></p>', NULL, 0, 0, '2019-06-04 16:17:02', '2019-06-25 11:43:16'),
(99, 'Spenser’s mock up sticker for boxes', 1, 1, 22, 62, '2019-06-07 10:00:00', NULL, '<p><span style="font-size:11.0pt;font-family:"Calibri",sans-serif;\r\nmso-ascii-theme-font:minor-latin;mso-fareast-font-family:Calibri;mso-fareast-theme-font:\r\nminor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:"Times New Roman";\r\nmso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:\r\nEN-US;mso-bidi-language:AR-SA">Get a sample of Spenser’s mock up sticker for\r\nboxes </span><br></p>', NULL, 0, 0, '2019-06-04 16:18:03', '2019-06-18 16:39:54'),
(100, 'Update actual retail box with new info.', 1, 0, 22, NULL, '2019-06-12 10:00:00', NULL, 'What info exaclty?', NULL, 0, 0, '2019-06-04 16:19:08', '2019-06-25 11:46:56'),
(101, 'Report on what page people leave on prior to sales', 1, 1, 22, 62, '2019-06-06 10:00:00', NULL, NULL, NULL, 0, 0, '2019-06-04 16:22:44', '2019-06-07 10:17:22'),
(102, 'How many sales from Logic vs Organic ?', 1, 1, 22, 62, '2019-06-06 10:00:00', NULL, NULL, NULL, 0, 0, '2019-06-04 16:30:31', '2019-06-25 11:39:57'),
(103, 'Link or A', 1, 1, 22, 62, '2019-06-05 10:00:00', NULL, '<p class="MsoPlainText">what is our top link or ad that is getting people to our\r\npage <o:p></o:p></p>\r\nPanic Button', NULL, 0, 0, '2019-06-04 16:31:08', '2019-06-25 11:52:29'),
(104, 'Finalize a list of events', 1, 1, 21, 61, '2019-06-05 10:00:00', NULL, '<p><span style="font-size:11.0pt;font-family:&quot;Calibri&quot;,sans-serif;\r\nmso-ascii-theme-font:minor-latin;mso-fareast-font-family:Calibri;mso-fareast-theme-font:\r\nminor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:\r\nEN-US;mso-bidi-language:AR-SA">Please make sure that the events are in the Editorial Calendar from On The Marc&nbsp;</span><br></p>', NULL, 0, 0, '2019-06-04 16:32:36', '2019-06-11 08:25:00'),
(105, 'Yellow Alert', 1, 0, 26, 63, '2019-06-17 10:00:00', NULL, '<p class="MsoPlainText">1) Must FULLY COMPLETE the yellow alert mode that is our\r\npatent.<o:p></o:p></p><p>\r\n\r\n</p><p class="MsoPlainText"><o:p> </o:p></p>', NULL, 0, 0, '2019-06-04 16:33:28', '2019-06-04 16:33:49'),
(106, '“Beep Beep Beep”', 1, 0, 26, NULL, '2019-06-17 10:00:00', NULL, '<p class="MsoPlainText">2) MUST FULLY COMPLETE the “beep beep beep” task which\r\nallows users to find their device when “find me” is hit.&nbsp;<o:p></o:p></p>', NULL, 0, 0, '2019-06-04 16:34:29', '2019-06-04 16:34:29'),
(107, 'Cellular Chips', 1, 0, 26, 63, '2019-06-17 10:00:00', NULL, '<p class="MsoPlainText">Start researching cellular chips.<o:p></o:p></p>', NULL, 0, 0, '2019-06-04 16:35:03', '2019-06-04 16:35:03'),
(108, 'Get PNC to give you a debt card.', 1, 1, 22, 62, '2019-06-07 10:00:00', NULL, '<p>In Progress. Lucie is checking with the Bank</p>', NULL, 0, 0, '2019-06-04 17:02:43', '2019-06-05 16:01:46'),
(109, 'Close Up May and All receipts', 1, 1, 22, 62, '2019-06-05 10:00:00', NULL, '<p><span style="font-size:11.0pt;font-family:&quot;Calibri&quot;,sans-serif;\r\nmso-ascii-theme-font:minor-latin;mso-fareast-font-family:Calibri;mso-fareast-theme-font:\r\nminor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:\r\nEN-US;mso-bidi-language:AR-SA">set a meeting with William to go over closing\r\nout May and have all receipts</span><br></p>', NULL, 0, 0, '2019-06-04 17:03:25', '2019-06-05 12:32:34'),
(110, 'Reimbursements for Kenny', 1, 1, 22, 62, '2019-06-05 10:00:00', NULL, '<p><span style="font-size:11.0pt;font-family:&quot;Calibri&quot;,sans-serif;\r\nmso-ascii-theme-font:minor-latin;mso-fareast-font-family:Calibri;mso-fareast-theme-font:\r\nminor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;;\r\nmso-bidi-theme-font:minor-bidi;mso-ansi-language:EN-US;mso-fareast-language:\r\nEN-US;mso-bidi-language:AR-SA">get my two separate reimbursements to Lucie to\r\npayout please.&nbsp;</span><br></p>', NULL, 0, 0, '2019-06-04 17:04:01', '2019-06-05 12:32:39'),
(111, 'Business Solutions Lead', 1, 0, 22, 61, '2019-06-05 10:00:00', NULL, '<p class="MsoNormal"><span style="font-size:12.0pt">Hi All,<o:p></o:p></span></p><p class="MsoNormal"><span style="font-size:12.0pt"> </span></p><p class="MsoNormal"><span style="font-size:12.0pt">I have this contact that is\r\ninterested in a business solutions package. <o:p></o:p></span></p><p class="MsoNormal"><span style="font-size:12.0pt"> </span></p><p class="MsoNormal"><span style="font-size:12.0pt">I am reaching out to see if\r\nGerman can get with our team and create a portal for this individual and do a\r\nfull walkthrough of the system so I can than coach them in how to test and use\r\nit. <o:p></o:p></span></p><p class="MsoNormal"><span style="font-size:12.0pt"> </span></p><p class="MsoNormal"><span style="font-size:12.0pt">Can we talk with the team in\r\nIndia and get this done? <o:p></o:p></span></p><p class="MsoNormal"><span style="font-size:12.0pt"> </span></p><p class="MsoNormal"><span style="font-size:12.0pt">Let me know. <br>\r\n<br>\r\nhere is the contact: <o:p></o:p></span></p><p class="MsoNormal"><b><span style="font-size:10.5pt;font-family:"Verdana",sans-serif"><o:p> </o:p></span></b></p><p class="MsoNormal"><b><span style="font-size:10.5pt;font-family:"Verdana",sans-serif">Stefan\r\nDodd<br>\r\n</span></b><i><span style="font-size:9.0pt;font-family:"Verdana",sans-serif">Director,\r\nTelecommunications and OneCard <br>\r\n</span></i><span style="font-size:9.0pt;font-family:"Verdana",sans-serif">Fitchburg\r\nState University<br>\r\n160 Pearl Street<br>\r\nFitchburg, MA   01420<br>\r\n(978) 665-3374<br>\r\n<a href="http://www.fitchburgstate.edu/IT">www.fitchburgstate.edu/IT</a></span><u><span style="color:blue"><o:p></o:p></span></u></p><p class="MsoNormal"><o:p> </o:p></p><p class="MsoNormal"><o:p> </o:p></p><p>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class="MsoNormal"><span style="font-size:12.0pt;font-family:"Cambria",serif">Tyler\r\nCharuhas<o:p></o:p></span></p>', NULL, 0, 0, '2019-06-04 17:11:00', '2019-06-25 11:51:50'),
(113, 'July 8th Expo Realtor', 1, 0, 21, 61, '2019-06-12 10:00:00', NULL, 'Finalize July 8ths Realtor Expo with German', NULL, 0, 0, '2019-06-11 08:24:43', '2019-06-11 08:24:43'),
(114, 'Investor Event July', 1, 1, 22, NULL, '2019-06-21 10:00:00', NULL, 'http://youngstartup.com/newyork2019/?utm_source=IPs&utm_campaign=NYVS19\r\n\r\nplease see if this event is worth it.', NULL, 0, 0, '2019-06-11 08:25:43', '2019-06-18 16:40:09'),
(115, 'Big event in Chicago', 1, 1, 22, 62, NULL, NULL, 'https://ecrm.marketgate.com/Sessions/2019/07/ConsumerTechnologyEPPS\r\n\r\nThis is the big one, setup everything with me and Tyler ASAP so costs don\'t get bigger', NULL, 0, 0, '2019-06-11 08:29:51', '2019-06-25 11:40:47'),
(116, 'Testing Task for admin assignment', 1, 1, 19, 60, '2019-06-11 10:00:00', NULL, NULL, NULL, 0, 0, '2019-06-11 09:27:47', '2019-06-23 21:40:46'),
(117, 'Get Realtor\'s Emails', 1, 1, 21, 61, '2019-06-14 10:00:00', NULL, 'Email with the names was already sent', NULL, 0, 0, '2019-06-12 13:55:04', '2019-06-23 21:39:58'),
(118, 'Contact International Companies on Spencer\'s List', 1, 1, 21, 61, '2019-06-17 10:00:00', NULL, NULL, NULL, 0, 0, '2019-06-12 13:55:37', '2019-06-23 21:40:07'),
(119, 'Testing Task2', 60, 0, 19, 58, NULL, NULL, NULL, NULL, 0, 1, '2019-06-14 05:23:58', '2019-06-26 01:01:25'),
(120, 'EAN number', 1, 0, 22, 62, '2019-07-05 10:00:00', NULL, 'We need EAN numbers for our products instead of the UPC codes as UPC is just for USA and EAN us for All over the world', NULL, 0, 0, '2019-06-20 09:39:11', '2019-06-25 11:46:30'),
(121, 'July San Fran Trip', 1, 1, 22, 62, '2019-06-28 10:00:00', NULL, 'Get the booth and info for San Fran July 9-11th\r\n\r\nMake sure Zeal is OK with going and explain why - that we can get companies who can help us. make these items.', NULL, 0, 1, '2019-06-24 15:35:36', '2019-06-25 11:40:35'),
(122, 'SEO', 1, 0, 22, 62, '2019-07-05 10:00:00', NULL, 'Merge "Com keywords Used in the Pages" with "Updated Keyword List with Current rank" and "Silent Beacon Competitors Analysis" into "SB Master File" ', NULL, 0, 0, '2019-06-24 19:36:48', '2019-07-01 10:54:45'),
(123, '500 units', 1, 1, 27, NULL, '2019-06-25 10:00:00', NULL, 'Set aside proper amount of units for SBL from TGT in RnD room', NULL, 0, 0, '2019-06-24 20:52:27', '2019-06-29 09:31:44'),
(124, 'Runners', 1, 0, 22, 62, NULL, NULL, 'See if Julie from the Fox News would let us take some running photos.  Ask OTM', NULL, 0, 0, '2019-06-26 16:57:14', '2019-06-26 16:57:14'),
(125, 'Start Planning CES 2020', 1, 0, 22, NULL, '2019-07-26 10:00:00', NULL, NULL, NULL, 0, 0, '2019-06-27 09:59:42', '2019-06-27 09:59:42'),
(126, 'Testing1', 1, 0, 19, 0, '2019-07-15 10:00:00', NULL, NULL, NULL, 0, 0, '2019-07-01 02:15:44', '2019-07-01 02:15:44'),
(127, 'Upwork new system', 1, 0, 22, 0, '2019-07-17 10:00:00', NULL, '<p>Figure out what plan we need with Kenny for upwork</p>', '1562580118_Screen Shot 2019-07-08 at 6.00.48 AM.png', 0, 0, '2019-07-08 05:02:06', '2019-07-08 05:02:06'),
(128, 'icon creation', 1, 0, 22, 0, '2019-07-15 10:00:00', NULL, 'Create an upwork project for Silent Beacon (not TGT) for icon creation.<div><br></div><div>Find the type of job within the Upwork dropdown.</div><div><br></div><div>description should be "Need custom icons for website"</div><div><br></div><div>send them this link - and say we need icons for each of the feature icons. &nbsp;We need them to showcase what each one really means. &nbsp;Example - USB port, show a USB port.</div><div><br></div><div><a href="https://silent3.silentbeacon.com/#!/features" target="_blank">https://silent3.silentbeacon.com/#!/features</a></div><div><br></div><div>Hire them ASAP and let me know what bids people ask to pay.</div>', NULL, 0, 0, '2019-07-13 11:25:35', '2019-07-13 11:25:35'),
(129, 'Speaker and Mic', 1, 0, 22, 0, '2019-08-01 10:00:00', NULL, '<p>This needs to be completed. &nbsp;Get with North Eastern as they may be able to help</p>', NULL, 0, 0, '2019-07-16 21:12:33', '2019-07-16 21:12:33'),
(130, 'Mold', 1, 0, 22, 0, '2019-08-09 10:00:00', NULL, NULL, NULL, 0, 0, '2019-07-25 13:05:19', '2019-07-25 13:05:19'),
(131, 'Follow up with logical with next steps', 1, 0, 22, 0, '2019-07-31 10:00:00', NULL, NULL, NULL, 0, 0, '2019-07-29 14:57:02', '2019-07-29 14:57:02');

-- --------------------------------------------------------

--
-- Table structure for table `task_assignments`
--

CREATE TABLE `task_assignments` (
  `id` int(10) UNSIGNED NOT NULL,
  `task_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `assigned_by` int(11) DEFAULT '0',
  `assigned_to` int(11) DEFAULT '0',
  `status` int(11) NOT NULL COMMENT '0->pending,1->complete',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `task_assignments`
--

INSERT INTO `task_assignments` (`id`, `task_id`, `created_by`, `assigned_by`, `assigned_to`, `status`, `created_at`, `updated_at`) VALUES
(1, 18, 1, 1, 60, 1, '2019-05-10 09:19:23', '2019-06-28 04:35:08'),
(2, 19, 1, 1, 60, 1, '2019-05-10 09:50:15', '2019-06-28 04:35:08'),
(3, 20, 1, 1, 60, 1, '2019-05-10 09:51:58', '2019-06-28 04:35:08'),
(4, 21, 1, 1, 60, 1, '2019-05-10 09:52:41', '2019-06-28 04:35:08'),
(5, 22, 1, 1, 60, 1, '2019-05-10 09:53:41', '2019-06-28 04:35:08'),
(6, 23, 1, 1, 60, 1, '2019-05-10 09:54:33', '2019-06-28 04:35:08'),
(7, 28, 1, 1, 62, 1, '2019-05-11 00:45:20', '2019-06-28 04:35:08'),
(8, 29, 1, 1, 60, 1, '2019-05-13 05:35:19', '2019-06-28 04:35:08'),
(9, 30, 1, 1, 60, 1, '2019-05-13 05:35:39', '2019-06-28 04:35:08'),
(10, 31, 1, 1, 60, 1, '2019-05-13 05:35:58', '2019-06-28 04:35:08'),
(11, 32, 1, 1, 60, 1, '2019-05-13 05:36:12', '2019-06-28 04:35:08'),
(12, 33, 1, 1, 60, 1, '2019-05-13 05:36:32', '2019-06-28 04:35:08'),
(13, 34, 1, 1, 60, 1, '2019-05-13 05:36:49', '2019-06-28 04:35:08'),
(14, 35, 1, 1, 60, 1, '2019-05-13 05:37:07', '2019-06-28 04:35:08'),
(15, 36, 1, 1, 60, 1, '2019-05-13 05:37:25', '2019-06-28 04:35:08'),
(16, 37, 1, 1, 62, 1, '2019-05-13 12:22:24', '2019-06-28 04:35:08'),
(17, 38, 1, 1, 62, 1, '2019-05-13 12:26:59', '2019-06-28 04:35:08'),
(19, 42, 1, 1, 60, 1, '2019-05-17 05:17:39', '2019-06-28 04:35:08'),
(20, 86, 1, 1, 62, 1, '2019-06-04 15:58:53', '2019-06-28 04:35:08'),
(21, 87, 1, 1, 62, 1, '2019-06-04 16:00:47', '2019-06-28 04:35:08'),
(22, 88, 1, 1, 62, 1, '2019-06-04 16:04:13', '2019-06-28 04:35:08'),
(23, 89, 1, 1, 62, 0, '2019-06-04 16:04:56', '2019-07-17 08:37:49'),
(24, 90, 1, 1, 62, 0, '2019-06-04 16:06:32', '2019-06-28 04:35:08'),
(25, 91, 1, 1, 62, 0, '2019-06-04 16:07:28', '2019-06-28 04:35:08'),
(26, 92, 1, 1, 62, 0, '2019-06-04 16:08:16', '2019-06-28 04:35:08'),
(27, 93, 1, 1, 62, 0, '2019-06-04 16:08:51', '2019-06-28 04:35:08'),
(28, 94, 1, 1, 62, 0, '2019-06-04 16:10:11', '2019-06-28 04:35:08'),
(29, 95, 1, 1, 63, 0, '2019-06-04 16:12:13', '2019-06-28 04:35:08'),
(30, 96, 1, 1, 62, 1, '2019-06-04 16:14:45', '2019-06-28 04:35:08'),
(31, 97, 1, 1, 62, 0, '2019-06-04 16:15:18', '2019-06-28 04:35:08'),
(32, 98, 1, 1, 62, 1, '2019-06-04 16:17:02', '2019-06-28 04:35:08'),
(33, 99, 1, 1, 62, 1, '2019-06-04 16:18:03', '2019-06-28 04:35:08'),
(34, 122, 1, 1, 62, 1, '2019-06-04 16:19:08', '2019-07-05 06:08:24'),
(35, 101, 1, 1, 62, 1, '2019-06-04 16:22:44', '2019-06-28 04:35:08'),
(36, 102, 1, 1, 62, 1, '2019-06-04 16:30:31', '2019-06-28 04:35:08'),
(37, 103, 1, 1, 62, 1, '2019-06-04 16:31:08', '2019-06-28 04:35:08'),
(38, 104, 1, 1, 61, 1, '2019-06-04 16:32:36', '2019-06-28 04:35:08'),
(39, 105, 1, 1, 63, 0, '2019-06-04 16:33:28', '2019-06-28 04:35:08'),
(40, 120, 1, 1, 62, 1, '2019-06-04 16:34:29', '2019-07-05 05:56:33'),
(41, 107, 1, 1, 63, 0, '2019-06-04 16:35:03', '2019-06-28 04:35:08'),
(42, 108, 1, 1, 62, 1, '2019-06-04 17:02:43', '2019-06-28 04:35:08'),
(43, 109, 1, 1, 62, 1, '2019-06-04 17:03:25', '2019-06-28 04:35:08'),
(44, 110, 1, 1, 62, 1, '2019-06-04 17:04:01', '2019-06-28 04:35:08'),
(45, 111, 1, 1, 61, 1, '2019-06-04 17:11:00', '2019-07-17 08:37:13'),
(46, 113, 1, 1, 61, 0, '2019-06-11 08:24:43', '2019-06-28 04:35:08'),
(47, 114, 1, 0, 0, 1, '2019-06-11 08:25:43', '2019-06-28 04:35:08'),
(48, 115, 1, 1, 62, 1, '2019-06-11 08:29:51', '2019-06-28 04:35:08'),
(49, 116, 1, 1, 60, 1, '2019-06-11 09:27:47', '2019-06-28 04:35:08'),
(50, 117, 1, 1, 61, 1, '2019-06-12 13:55:04', '2019-06-28 04:35:08'),
(51, 118, 1, 1, 61, 1, '2019-06-12 13:55:37', '2019-06-28 04:35:08'),
(52, 119, 60, 60, 58, 0, '2019-06-14 05:23:58', '2019-06-28 04:35:08'),
(54, 121, 1, 1, 62, 1, '2019-06-24 15:35:36', '2019-06-28 04:35:08'),
(56, 123, 1, 0, 0, 0, '2019-06-24 20:52:27', '2019-06-28 04:35:08'),
(57, 124, 1, 1, 62, 1, '2019-06-26 16:57:14', '2019-07-01 11:44:35'),
(58, 125, 1, 0, 0, 0, '2019-06-27 09:59:42', '2019-06-28 04:35:08'),
(59, 126, 1, 1, 60, 0, '2019-07-01 02:15:44', '2019-07-01 02:15:44'),
(60, 126, 1, 60, 58, 0, '2019-07-01 04:25:37', '2019-07-01 04:25:37'),
(61, 127, 1, 1, 62, 1, '2019-07-08 05:02:06', '2019-07-15 10:04:16'),
(62, 128, 1, 1, 62, 1, '2019-07-13 11:25:35', '2019-07-15 10:04:26'),
(63, 129, 1, 1, 62, 0, '2019-07-16 21:12:33', '2019-07-16 21:12:33'),
(64, 130, 1, 1, 62, 0, '2019-07-25 13:05:19', '2019-07-25 13:05:19'),
(65, 131, 1, 1, 62, 0, '2019-07-29 14:57:02', '2019-07-29 14:57:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` enum('Active','Pending') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Kenny', 'kenny@silentbeacon.com', 'admin', NULL, '$2y$10$wc5h0EH.FR2D9U9jOUdy6O7QeFWZaxkyqYkp2jkPkoKeo3Xute9xy', 'ztXYG3yiCQnm4oWkOn86BUvs2fw9bo5O39EIvBUmZudvWvI6coJRUgCbeC2U', '2019-03-29 02:14:51', '2019-04-10 08:28:43', 'Active'),
(56, 'Anmol', 'anmol.impinge@gmail.com', 'customer', NULL, '$2y$10$rLzviy95v9B5QQ1YdgpsA.XjcVRUF8YRQO6c1Ex6nXisQCz8yEihi', 'jSgMR3JHgyUgLGOzHoObTeG9ni7RAC0UAVFnOf2zl6kLa3FcjACsQ8gZPuiS', '2019-05-09 04:10:56', '2019-05-09 06:52:49', 'Active'),
(57, 'Sonika', 'sonikak@impingeonline.com', 'customer', NULL, '$2y$10$W17y5chlXk6rlE/D5hGu3ubTcT6FIUP4djHaeZCWFhsG7zCgP1IQ6', 'fopZCQNJNz2NaDesLsJSxmfUPvENkF84axWZ0NuK2O5EydZM1RVfHJZu2WT2', '2019-05-09 07:23:36', '2019-05-10 10:52:57', 'Active'),
(58, 'Pawan', 'pawanvir.impinge@gmail.com', 'customer', NULL, '$2y$10$Z/SS1nVM1oI66kugZF7.z.DzbGJme0nFHznMKby0IBua.bpT79U.6', 'BEpnESCFo5SididCcJUbT4HQxrmaJpr9yXIDHhGJ6p7fljSF4lzdIOuMqH36', '2019-05-09 07:47:26', '2019-07-16 09:29:04', 'Active'),
(59, 'Davinder Kumar', 'davinder@impingesolutions.com', 'admin', NULL, '$2y$10$HKIzVQh5L/WrErEUIyGTeO0AynuH.9ofsXrlL2X.U9Ro8HSlTnywW', 'xTToqPyOZAKbOEqxoE57V4FHvVF4KxQgFWUmSlzd7y3KUZw8GbzeudGRsbZz', '2019-05-10 05:36:25', '2019-05-13 06:54:40', 'Active'),
(60, 'Amrish K', 'amrishk@impingeonline.com', 'customer', NULL, '$2y$10$MZWMojVRPV7mtQjYHFHMxuv6nBSVhQ8cJgS5pN5RMly93trRdCiQy', 'EzDNHmdyzLYTnBeg9QN2FAHGpUcwL0vMlsOXmgmO7VWB7Z6tZUYiYQ9J6Xre', '2019-05-10 05:36:47', '2019-05-13 05:48:47', 'Active'),
(61, 'Tyler', 'tyler@silentbeacon.com', 'customer', NULL, '$2y$10$VHyRyrATlqibrCCvFBYh4ecp2iunCK4dhc9DX2aFbBaHZrYGXem0e', 'IbZEg94vTuH8nDIUxYNKUoOd835SolT1wg531ew3axD2Pd5kQ07mWya9OHtV', '2019-05-11 00:41:13', '2019-05-14 11:14:46', 'Active'),
(62, 'German', 'german@silentbeacon.com', 'admin', NULL, '$2y$10$bQQ5G6d3XtmhfjfTQcwVju9XP/S2XHMVyIcsQRiItHicyUWvNxhh2', 'VnIP6SoalOFtHL1QlIduOhY5Hb9tdqRVxiv9tpUzoWy2JeA0GKg9ah8LTaB4', '2019-05-11 00:44:32', '2019-07-16 09:54:15', 'Active'),
(63, 'Zeal', 'zeal@silentbeacon.com', 'customer', NULL, '$2y$10$qB2jVfdWlPvFqgUTTyeM4OGkvCiWcv4/ZQt4WvV.hiyNR91CLN8YK', 'QJV5SpSL80HaJFVd6UmOD1eve0UHH7RWAtM4andZX5RwlSQBi3bQ43DX8Ltg', '2019-06-04 16:11:20', '2019-06-04 17:03:08', 'Active'),
(64, 'Karim', 'karim@silentbeacon.com', 'customer', NULL, '$2y$10$YP90MdCuKRjvqH1AMnxVDe3Rar8N7RbG9hwIofyx9eMkpLd/49Kx6', NULL, '2019-06-20 09:39:46', '2019-06-20 09:39:46', 'Pending');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `folders`
--
ALTER TABLE `folders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles_users`
--
ALTER TABLE `roles_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task_assignments`
--
ALTER TABLE `task_assignments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `folders`
--
ALTER TABLE `folders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles_users`
--
ALTER TABLE `roles_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;
--
-- AUTO_INCREMENT for table `task_assignments`
--
ALTER TABLE `task_assignments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
