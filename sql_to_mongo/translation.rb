table "action_has_params" do
	column "id", :key, :as => :integer
	column "action_id", :integer, :references => "actions"
	column "name", :text
	column "value", :text
end

table "actions" do
	column "id", :key, :as => :integer
	column "project_id", :integer, :references => "projects"
	column "event_name", :text
	column "action_name", :text
end

table "column_has_move_restrictions" do
	column "restriction_id", :integer, :references => "restrictions"
	column "project_id", :integer, :references => "projects"
	column "role_id", :integer, :references => "roles"
	column "src_column_id", :integer, :references => "src_columns"
	column "dst_column_id", :integer, :references => "dst_columns"
	column "only_assigned", :boolean
end

table "column_has_restrictions" do
	column "restriction_id", :integer, :references => "restrictions"
	column "project_id", :integer, :references => "projects"
	column "role_id", :integer, :references => "roles"
	column "column_id", :integer, :references => "columns"
	column "rule", :string
end

table "columns" do
	column "id", :key, :as => :integer
	column "title", :string
	column "position", :integer
	column "project_id", :integer, :references => "projects"
	column "task_limit", :integer
	column "description", :text
	column "hide_in_dashboard", :integer
end

table "comments" do
	column "id", :key, :as => :integer
	column "task_id", :integer, :references => "tasks"
	column "user_id", :integer, :references => "users"
	column "date_creation", :integer
	column "comment", :text
	column "reference", :string
	column "date_modification", :integer
end

table "currencies" do
	column "currency", :string
	column "rate", :float
end

table "custom_filters" do
	column "id", :key, :as => :integer
	column "filter", :text
	column "project_id", :integer, :references => "projects"
	column "user_id", :integer, :references => "users"
	column "name", :text
	column "is_shared", :boolean
	column "append", :boolean
end

table "group_has_users" do
	column "group_id", :integer, :references => "groups"
	column "user_id", :integer, :references => "users"
end

table "groups" do
	column "id", :key, :as => :integer
	column "external_id", :string, :references => "externals"
	column "name", :string
end

table "invites" do
	column "email", :string
	column "project_id", :integer, :references => "projects"
	column "token", :string
end

table "last_logins" do
	column "id", :key, :as => :integer
	column "auth_type", :string
	column "user_id", :integer, :references => "users"
	column "ip", :string
	column "user_agent", :string
	column "date_creation", :integer
end

table "links" do
	column "id", :key, :as => :integer
	column "label", :string
	column "opposite_id", :integer, :references => "opposites"
end

table "password_reset" do
	column "token", :string
	column "user_id", :integer, :references => "users"
	column "date_expiration", :integer
	column "date_creation", :integer
	column "ip", :string
	column "user_agent", :string
	column "is_active", :boolean
end

table "plugin_schema_versions" do
	column "plugin", :string
	column "version", :integer
end

table "predefined_task_descriptions" do
	column "id", :key, :as => :integer
	column "project_id", :integer, :references => "projects"
	column "title", :text
	column "description", :text
end

table "project_activities" do
	column "id", :key, :as => :integer
	column "date_creation", :integer
	column "event_name", :text
	column "creator_id", :integer, :references => "creators"
	column "project_id", :integer, :references => "projects"
	column "task_id", :integer, :references => "tasks"
	column "data", :text
end

table "project_daily_column_stats" do
	column "id", :key, :as => :integer
	column "day", :string
	column "project_id", :integer, :references => "projects"
	column "column_id", :integer, :references => "columns"
	column "total", :integer
	column "score", :integer
end

table "project_daily_stats" do
	column "id", :key, :as => :integer
	column "day", :string
	column "project_id", :integer, :references => "projects"
	column "avg_lead_time", :integer
	column "avg_cycle_time", :integer
end

table "project_has_categories" do
	column "id", :key, :as => :integer
	column "name", :string
	column "project_id", :integer, :references => "projects"
	column "description", :text
	column "color_id", :string, :references => "colors"
end

table "project_has_files" do
	column "id", :key, :as => :integer
	column "project_id", :integer, :references => "projects"
	column "name", :text
	column "path", :text
	column "is_image", :boolean
	column "size", :integer
	column "user_id", :integer, :references => "users"
	column "date", :integer
end

table "project_has_groups" do
	column "group_id", :integer, :references => "groups"
	column "project_id", :integer, :references => "projects"
	column "role", :string
end

table "project_has_metadata" do
	column "project_id", :integer, :references => "projects"
	column "name", :string
	column "value", :string
	column "changed_by", :integer
	column "changed_on", :integer
end

table "project_has_notification_types" do
	column "id", :key, :as => :integer
	column "project_id", :integer, :references => "projects"
	column "notification_type", :string
end

table "project_has_roles" do
	column "role_id", :integer, :references => "roles"
	column "role", :string
	column "project_id", :integer, :references => "projects"
end

table "project_has_users" do
	column "project_id", :integer, :references => "projects"
	column "user_id", :integer, :references => "users"
	column "role", :string
end

table "project_role_has_restrictions" do
	column "restriction_id", :integer, :references => "restrictions"
	column "project_id", :integer, :references => "projects"
	column "role_id", :integer, :references => "roles"
	column "rule", :string
end

table "projects" do
	column "id", :key, :as => :integer
	column "name", :text
	column "is_active", :integer
	column "token", :string
	column "last_modified", :integer
	column "is_public", :boolean
	column "is_private", :boolean
	column "description", :text
	column "identifier", :string
	column "start_date", :string
	column "end_date", :string
	column "owner_id", :integer, :references => "owners"
	column "priority_default", :integer
	column "priority_start", :integer
	column "priority_end", :integer
	column "email", :text
	column "predefined_email_subjects", :text
end

table "remember_me" do
	column "id", :key, :as => :integer
	column "user_id", :integer, :references => "users"
	column "ip", :string
	column "user_agent", :string
	column "token", :string
	column "sequence", :string
	column "expiration", :integer
	column "date_creation", :integer
end

table "schema_version" do
	column "version", :integer
end

table "sessions" do
	column "id", :key, :as => :string
	column "expire_at", :string
	column "data", :text
end

table "settings" do
	column "option", :string
	column "value", :text
	column "changed_by", :integer
	column "changed_on", :integer
end

table "subtask_time_tracking" do
	column "id", :key, :as => :integer
	column "user_id", :integer, :references => "users"
	column "subtask_id", :integer, :references => "subtasks"
	column "start", :integer
	column "end", :integer
	column "time_spent", :float
end

table "subtasks" do
	column "id", :key, :as => :integer
	column "title", :text
	column "status", :integer
	column "time_estimated", :float
	column "time_spent", :float
	column "task_id", :integer, :references => "tasks"
	column "user_id", :integer, :references => "users"
	column "position", :integer
end

table "swimlanes" do
	column "id", :key, :as => :integer
	column "name", :string
	column "position", :integer
	column "is_active", :integer
	column "project_id", :integer, :references => "projects"
	column "description", :text
end

table "tags" do
	column "id", :key, :as => :integer
	column "name", :string
	column "project_id", :integer, :references => "projects"
	column "color_id", :string, :references => "colors"
end

table "task_has_external_links" do
	column "id", :key, :as => :integer
	column "link_type", :string
	column "dependency", :string
	column "title", :text
	column "url", :text
	column "date_creation", :integer
	column "date_modification", :integer
	column "task_id", :integer, :references => "tasks"
	column "creator_id", :integer, :references => "creators"
end

table "task_has_files" do
	column "id", :key, :as => :integer
	column "name", :text
	column "path", :text
	column "is_image", :boolean
	column "task_id", :integer, :references => "tasks"
	column "date", :integer
	column "user_id", :integer, :references => "users"
	column "size", :integer
end

table "task_has_links" do
	column "id", :key, :as => :integer
	column "link_id", :integer, :references => "links"
	column "task_id", :integer, :references => "tasks"
	column "opposite_task_id", :integer, :references => "opposite_tasks"
end

table "task_has_metadata" do
	column "task_id", :integer, :references => "tasks"
	column "name", :string
	column "value", :string
	column "changed_by", :integer
	column "changed_on", :integer
end

table "task_has_tags" do
	column "task_id", :integer, :references => "tasks"
	column "tag_id", :integer, :references => "tags"
end

table "tasks" do
	column "id", :key, :as => :integer
	column "title", :text
	column "description", :text
	column "date_creation", :integer
	column "date_completed", :integer
	column "date_due", :integer
	column "color_id", :string, :references => "colors"
	column "project_id", :integer, :references => "projects"
	column "column_id", :integer, :references => "columns"
	column "owner_id", :integer, :references => "owners"
	column "position", :integer
	column "score", :integer
	column "is_active", :integer
	column "category_id", :integer, :references => "categories"
	column "creator_id", :integer, :references => "creators"
	column "date_modification", :integer
	column "reference", :string
	column "date_started", :integer
	column "time_spent", :float
	column "time_estimated", :float
	column "swimlane_id", :integer, :references => "swimlanes"
	column "date_moved", :integer
	column "recurrence_status", :integer
	column "recurrence_trigger", :integer
	column "recurrence_factor", :integer
	column "recurrence_timeframe", :integer
	column "recurrence_basedate", :integer
	column "recurrence_parent", :integer
	column "recurrence_child", :integer
	column "priority", :integer
	column "external_provider", :string
	column "external_uri", :string
end

table "transitions" do
	column "id", :key, :as => :integer
	column "user_id", :integer, :references => "users"
	column "project_id", :integer, :references => "projects"
	column "task_id", :integer, :references => "tasks"
	column "src_column_id", :integer, :references => "src_columns"
	column "dst_column_id", :integer, :references => "dst_columns"
	column "date", :integer
	column "time_spent", :integer
end

table "user_has_metadata" do
	column "user_id", :integer, :references => "users"
	column "name", :string
	column "value", :string
	column "changed_by", :integer
	column "changed_on", :integer
end

table "user_has_notification_types" do
	column "id", :key, :as => :integer
	column "user_id", :integer, :references => "users"
	column "notification_type", :string
end

table "user_has_notifications" do
	column "user_id", :integer, :references => "users"
	column "project_id", :integer, :references => "projects"
end

table "user_has_unread_notifications" do
	column "id", :key, :as => :integer
	column "user_id", :integer, :references => "users"
	column "date_creation", :integer
	column "event_name", :text
	column "event_data", :text
end

table "users" do
	column "id", :key, :as => :integer
	column "username", :string
	column "password", :string
	column "is_ldap_user", :boolean
	column "name", :string
	column "email", :string
	column "google_id", :string, :references => "googles"
	column "github_id", :string, :references => "githubs"
	column "notifications_enabled", :boolean
	column "timezone", :string
	column "language", :string
	column "disable_login_form", :boolean
	column "twofactor_activated", :boolean
	column "twofactor_secret", :string
	column "token", :string
	column "notifications_filter", :integer
	column "nb_failed_login", :integer
	column "lock_expiration_date", :integer
	column "gitlab_id", :integer, :references => "gitlabs"
	column "role", :string
	column "is_active", :boolean
	column "avatar_path", :string
	column "api_access_token", :string
	column "filter", :text
end

