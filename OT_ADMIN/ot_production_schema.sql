-- Adminer 4.7.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DELIMITER ;;

DROP PROCEDURE IF EXISTS `additional_exercise`;;
CREATE PROCEDURE `additional_exercise`()
BEGIN
   DECLARE count INT DEFAULT 0;
   WHILE count < 10 DO
      INSERT INTO `fitness_plan` ( `name`, `calories`, `leaderId`, `weekNumber`, `plan`, `dayNumber`, `code`, `exercise`, `dataType`, `data`, `video_links`, `additional_exercise`, `createdDate`) VALUES ('AdditionalExercise1', '200', '0', '0', '', '-1', '', '', '0,1', '<html><style>p{font-size:16px;}</style><p></p><ul></ul></html>', 'https://www.youtube.com/watch?v=cgJpZJQ5eMU&feature=youtu.be', '1', CURRENT_TIMESTAMP);
      SET count = count + 1;
   END WHILE;
END;;

DELIMITER ;

DROP TABLE IF EXISTS `app_api_key`;
CREATE TABLE `app_api_key` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apiKey` varchar(100) NOT NULL,
  `platform` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


SET NAMES utf8mb4;

DROP TABLE IF EXISTS `app_users`;
CREATE TABLE `app_users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `leaderId` int(10) NOT NULL,
  `fullName` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `emailId` varchar(100) NOT NULL,
  `gender` int(1) NOT NULL COMMENT 'gender 0 for Male and 1 for female',
  `age` int(4) NOT NULL,
  `weight` varchar(50) NOT NULL COMMENT 'in pound',
  `weightType` int(1) NOT NULL COMMENT 'case lb = 0, case st = 1, case kg = 2',
  `height` varchar(50) NOT NULL COMMENT 'in centimeters',
  `heightType` int(1) NOT NULL COMMENT 'case ft = 0, case cm = 1',
  `createdDate` datetime NOT NULL,
  `sessionStartDate` datetime NOT NULL,
  `userCurrentSessionToken` varchar(150) NOT NULL COMMENT 'Recently logged in device session token of user',
  `timeOffset` varchar(10) NOT NULL,
  `notification` int(11) NOT NULL DEFAULT '1' COMMENT '0-notification off,1-notification on',
  `currentPlanType` int(11) NOT NULL DEFAULT '0',
  `planSelectionDay` int(11) NOT NULL DEFAULT '0',
  `vegOnly` int(11) NOT NULL DEFAULT '0' COMMENT '0-vegOnly false,1-vegOnly true',
  PRIMARY KEY (`userId`),
  KEY `emailId` (`emailId`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `app_users_dump`;
CREATE TABLE `app_users_dump` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `leaderId` int(10) NOT NULL,
  `fullName` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `emailId` varchar(100) NOT NULL,
  `gender` int(1) NOT NULL COMMENT 'gender 0 for Male and 1 for female',
  `age` int(4) NOT NULL,
  `weight` varchar(50) NOT NULL COMMENT 'in pound',
  `weightType` int(1) NOT NULL COMMENT 'case lb = 0, case st = 1, case kg = 2',
  `height` varchar(50) NOT NULL COMMENT 'in centimeters',
  `heightType` int(1) NOT NULL COMMENT 'case ft = 0, case cm = 1',
  `createdDate` datetime NOT NULL,
  `sessionStartDate` datetime NOT NULL,
  `userCurrentSessionToken` varchar(150) NOT NULL COMMENT 'Recently logged in device session token of user',
  `timeOffset` varchar(10) NOT NULL,
  `notification` int(11) NOT NULL DEFAULT '1' COMMENT '0-notification off,1-notification on',
  PRIMARY KEY (`userId`),
  KEY `emailId` (`emailId`),
  KEY `password` (`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `blogs`;
CREATE TABLE `blogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `week` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isPromotionalVideo` int(1) NOT NULL COMMENT '1 if promotional video , else 0',
  `title` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `category_items`;
CREATE TABLE `category_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryId` int(11) NOT NULL,
  `itemId` int(11) NOT NULL,
  `orderByOld` int(5) NOT NULL,
  `orderBy` int(5) NOT NULL,
  `duplicate` int(11) NOT NULL DEFAULT '0' COMMENT '0-no,1-yes',
  PRIMARY KEY (`id`),
  KEY `categoryId` (`categoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `daily_meal`;
CREATE TABLE `daily_meal` (
  `dayNumber` int(3) NOT NULL,
  `weekNumber` int(3) NOT NULL,
  `itemId` int(5) NOT NULL,
  `createdDate` datetime NOT NULL,
  `updatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`dayNumber`),
  KEY `dayNumber` (`dayNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `daily_progress_report`;
CREATE TABLE `daily_progress_report` (
  `userId` int(11) NOT NULL,
  `stepCount` bigint(20) NOT NULL,
  `calorieCount` int(11) NOT NULL,
  `dayNumber` int(5) NOT NULL,
  `backupDayNumber` int(11) NOT NULL,
  `createdDate` datetime NOT NULL,
  PRIMARY KEY (`userId`,`dayNumber`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `daily_progress_report_dump`;
CREATE TABLE `daily_progress_report_dump` (
  `userId` int(11) NOT NULL,
  `stepCount` bigint(20) NOT NULL,
  `calorieCount` int(5) NOT NULL,
  `dayNumber` int(5) NOT NULL COMMENT 'daynumber 0 for day 1',
  `createdDate` datetime NOT NULL,
  PRIMARY KEY (`userId`,`dayNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `emailtemplates`;
CREATE TABLE `emailtemplates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_template` longtext COLLATE utf8mb4_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `event_list`;
CREATE TABLE `event_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventTitle` varchar(255) NOT NULL,
  `eventDesc` text NOT NULL,
  `eventBannerImage` varchar(255) NOT NULL,
  `eventPDFFile` varchar(255) NOT NULL,
  `eventDate` date NOT NULL,
  `postedDate` date NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `event_locations`;
CREATE TABLE `event_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventId` int(11) NOT NULL,
  `locationId` int(11) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `eventId` (`eventId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `fitness_diary`;
CREATE TABLE `fitness_diary` (
  `userId` int(11) NOT NULL,
  `weekNumber` int(11) NOT NULL,
  `dayNumber` int(11) NOT NULL,
  `excercise_id` int(11) NOT NULL DEFAULT '0',
  `backupDayNumber` int(11) NOT NULL,
  `backupWeekNumber` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdDate` datetime NOT NULL,
  PRIMARY KEY (`userId`,`weekNumber`,`dayNumber`,`excercise_id`) USING BTREE,
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `fitness_plan`;
CREATE TABLE `fitness_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `calories` int(11) NOT NULL DEFAULT '0',
  `leaderId` int(11) NOT NULL,
  `weekNumber` int(11) NOT NULL,
  `plan` text NOT NULL,
  `dayNumber` int(11) NOT NULL,
  `code` text NOT NULL,
  `exercise` text NOT NULL,
  `dataType` text NOT NULL COMMENT '0-text,1-links',
  `data` text NOT NULL,
  `video_links` text NOT NULL,
  `additional_exercise` int(11) NOT NULL DEFAULT '0',
  `order_by` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 => In-active, 1 => Active',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0',
  `createdDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `leaderId` (`leaderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `food_categories`;
CREATE TABLE `food_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(100) NOT NULL,
  `categoryImage` varchar(150) NOT NULL,
  `activeStatus` int(1) NOT NULL DEFAULT '1' COMMENT '1 if active , 0 if inactive',
  `isCombo` int(1) NOT NULL DEFAULT '0' COMMENT '1 if combo category, 0 if not',
  `isMealOfTheDay` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 => Will show meals of the day food items',
  `isArchive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 if data is in archive mode, else 0',
  `createdDate` datetime NOT NULL COMMENT 'utc time',
  `updatedDate` datetime NOT NULL COMMENT 'utc date',
  `orderBy` int(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `isCombo` (`isCombo`),
  KEY `isArchive` (`isArchive`),
  KEY `activeStatus` (`activeStatus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `food_items`;
CREATE TABLE `food_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemName` varchar(100) NOT NULL,
  `serveName` varchar(255) NOT NULL,
  `itemImage` varchar(100) NOT NULL,
  `itemIngredients` text NOT NULL,
  `itemMethod` text NOT NULL,
  `instructionalVideo` varchar(255) NOT NULL,
  `prepareAhead` text NOT NULL,
  `additionalInfo` text NOT NULL,
  `calories` float NOT NULL COMMENT 'calories count',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `isArchive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 if data is in archive mode, else 0',
  `orderBy` int(11) NOT NULL DEFAULT '0',
  `createdDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updatedDate` datetime NOT NULL COMMENT 'utc time',
  `userType` enum('0','1','2') NOT NULL,
  `food_type` int(11) NOT NULL DEFAULT '1' COMMENT '1 for veg, 0 for nonveg',
  `veg_item_id` int(11) DEFAULT '0',
  `vegOption` text NOT NULL,
  `vegCalories` int(11) NOT NULL,
  `isVegAvailable` int(11) NOT NULL,
  `isShutDownWeek` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `isArchive` (`isArchive`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `forgot_password`;
CREATE TABLE `forgot_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(10) NOT NULL,
  `forgotOTP` int(8) NOT NULL,
  `requestCount` int(1) NOT NULL,
  `forgotRequestTime` datetime NOT NULL COMMENT 'ut timestamp',
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `forgot_password_dump`;
CREATE TABLE `forgot_password_dump` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(10) NOT NULL,
  `forgotOTP` int(8) NOT NULL,
  `requestCount` int(1) NOT NULL,
  `forgotRequestTime` datetime NOT NULL COMMENT 'ut timestamp',
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `is_updated`;
CREATE TABLE `is_updated` (
  `id` int(1) NOT NULL,
  `updatedDate` datetime NOT NULL,
  `shopingListUpdatedDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `leaders`;
CREATE TABLE `leaders` (
  `leaderId` int(11) NOT NULL AUTO_INCREMENT,
  `leaderName` varchar(255) NOT NULL,
  `age` int(5) NOT NULL,
  `weight` float NOT NULL COMMENT 'in pound',
  `convertedWeight` text NOT NULL,
  `location` text NOT NULL,
  `image` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 => In-Active, 1 => Active',
  `is_archive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 => Enable, 1 => Archived',
  `createdDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`leaderId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `locations`;
CREATE TABLE `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `county` text NOT NULL,
  `route` text NOT NULL,
  `location` varchar(255) NOT NULL,
  `contact` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requestType` varchar(10) NOT NULL,
  `url` text NOT NULL,
  `request` text NOT NULL,
  `response` text NOT NULL,
  `createdDate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `log_with_raw_data`;
CREATE TABLE `log_with_raw_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `rawData` text NOT NULL,
  `createdDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `week` text NOT NULL,
  `day` int(11) NOT NULL,
  `message` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT 'Notification activation status 0-inactive,1-Active',
  `type` int(11) NOT NULL COMMENT '1-food,2-event,3-rest,4-walk,5clip',
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `datetime` (`datetime`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `partners`;
CREATE TABLE `partners` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1 for active true and 0 for active false',
  `isArchive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 if data is in archive mode, else 0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `reminder_settings`;
CREATE TABLE `reminder_settings` (
  `userId` int(11) NOT NULL COMMENT 'user primary key',
  `dayNumber` int(3) NOT NULL COMMENT '-1 if all 7 days reminder set, else 0 to 6 dayNumber',
  `reminderType` int(1) NOT NULL COMMENT '1 for workout reminder, 2 for weekly weight reminder & 3 for weekly progress photo reminder',
  `reminderTime` varchar(5) NOT NULL COMMENT '24 hour format',
  `createdDate` datetime NOT NULL COMMENT 'ut timestamp',
  PRIMARY KEY (`userId`,`dayNumber`,`reminderType`),
  KEY `userId` (`userId`),
  KEY `dayNumber` (`dayNumber`),
  KEY `reminderType` (`reminderType`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `shopping_list_pdf`;
CREATE TABLE `shopping_list_pdf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weekNumber` int(2) NOT NULL,
  `pdfFile` varchar(255) NOT NULL,
  `pdfFile_veg` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 => In-Active, 1 => Active',
  `isArchive` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 => Enable, 1 => Archived',
  `orderBy` int(11) NOT NULL,
  `createdDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` tinyint(4) NOT NULL DEFAULT '2' COMMENT '1 for Admin and 2 for users',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `forgotToken` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'new token generate when forgot password email send',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `user_devices`;
CREATE TABLE `user_devices` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `userId` int(100) NOT NULL,
  `deviceType` tinyint(4) NOT NULL DEFAULT '0',
  `deviceToken` text NOT NULL,
  `sessionToken` varchar(255) NOT NULL,
  `loginTime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user_devices_dump`;
CREATE TABLE `user_devices_dump` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `userId` int(100) NOT NULL,
  `deviceType` tinyint(4) NOT NULL DEFAULT '0',
  `deviceToken` varchar(255) NOT NULL,
  `sessionToken` varchar(255) NOT NULL,
  `loginTime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user_item_consumptions`;
CREATE TABLE `user_item_consumptions` (
  `userId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `itemId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `dayNumber` int(11) NOT NULL COMMENT 'daynumber 0 for day 1',
  `backupDayNumber` int(11) NOT NULL,
  `createdDate` datetime NOT NULL,
  PRIMARY KEY (`userId`,`categoryId`,`itemId`,`dayNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `user_item_consumptions_dump`;
CREATE TABLE `user_item_consumptions_dump` (
  `userId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `itemId` int(11) NOT NULL,
  `dayNumber` int(11) NOT NULL COMMENT 'daynumber 0 for day 1',
  `createdDate` datetime NOT NULL,
  PRIMARY KEY (`userId`,`categoryId`,`itemId`,`dayNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `weekly_photo_progress_report`;
CREATE TABLE `weekly_photo_progress_report` (
  `weeklyProgressId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `userWeeklyPhoto` text NOT NULL,
  `userWeeklyWeight` float NOT NULL,
  `weekNumber` int(5) NOT NULL COMMENT 'Start from 0 to 7 Week, 0 equal to Week Number 1',
  `backupWeekNumber` int(11) NOT NULL,
  `dayNumber` int(5) NOT NULL COMMENT 'Start from 0, 0 equal to Day 1',
  `backupDayNumber` int(11) NOT NULL,
  `createdDate` datetime NOT NULL,
  PRIMARY KEY (`weeklyProgressId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `weekly_photo_progress_report_dump`;
CREATE TABLE `weekly_photo_progress_report_dump` (
  `weeklyProgressId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `userWeeklyPhoto` varchar(255) NOT NULL,
  `userWeeklyWeight` float NOT NULL,
  `weekNumber` int(5) NOT NULL COMMENT 'Start from 0 to 7 Week, 0 equal to Week Number 1',
  `dayNumber` int(5) NOT NULL COMMENT 'Start from 0, 0 equal to Day 1',
  `createdDate` datetime NOT NULL,
  PRIMARY KEY (`weeklyProgressId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- 2020-01-02 10:35:40
