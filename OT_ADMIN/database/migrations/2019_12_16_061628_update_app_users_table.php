<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAppUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('app_users', function (Blueprint $table) {
            //

            DB::statement("ALTER TABLE app_users ADD vegOnly INT(11) NOT NULL COMMENT '0-vegOnly false,1-vegOnly true' AFTER notification");

            DB::statement("UPDATE app_users SET vegOnly = 0");

            DB::statement("ALTER TABLE app_users CHANGE vegOnly vegOnly INT(11) NOT NULL DEFAULT '0' COMMENT '0-vegOnly false,1-vegOnly true'");

            DB::statement("ALTER TABLE app_users ADD currentPlanType INT(11) NOT NULL DEFAULT '0' AFTER notification, ADD planSelectionDay INT(11) NOT NULL DEFAULT '0' AFTER currentPlanType");

            /*$table->integer('vegOnly')->after('notification')->default('0')->comment='0-vegOnly false,1-vegOnly true';
            $table->integer('currentPlanType')->after('notification')->default('0');
            $table->integer('planSelectionDay')->after('currentPlanType')->default('0');*/

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('app_users', function (Blueprint $table) {
            //
            $table->dropColumn('vegOnly');
            $table->dropColumn('currentPlanType');
            $table->dropColumn('planSelectionDay');
        });
    }
}
