<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFitnessPlanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fitness_plan', function (Blueprint $table) {
            //
            DB::statement("ALTER TABLE fitness_plan ADD name VARCHAR(255) NOT NULL AFTER id, ADD calories INT(11) NOT NULL DEFAULT '0' AFTER name");

            DB::statement("ALTER TABLE fitness_plan ADD additional_exercise INT(11) NOT NULL DEFAULT '0' AFTER video_links");

            DB::statement("ALTER TABLE fitness_plan ADD updatedDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER createdDate");

            DB::statement("ALTER TABLE fitness_plan ADD order_by INT NOT NULL AFTER additional_exercise, ADD status BOOLEAN NOT NULL DEFAULT TRUE COMMENT '0 => In-active, 1 => Active' AFTER order_by");

            DB::statement("ALTER TABLE fitness_plan ADD is_archive BOOLEAN NOT NULL DEFAULT FALSE AFTER status");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fitness_plan', function (Blueprint $table) {
            //
            $table->dropColumn('name');
            $table->dropColumn('calories');
            $table->dropColumn('additional_exercise');
            $table->dropColumn('updatedDate');
            $table->dropColumn('order_by');
            $table->dropColumn('status');
            $table->dropColumn('is_archive');
        });
    }
}
