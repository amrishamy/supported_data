<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leaders', function (Blueprint $table) {
            //
            DB::statement("ALTER TABLE leaders ADD status BOOLEAN NOT NULL DEFAULT TRUE COMMENT '0 => In-Active, 1 => Active' AFTER image, ADD is_archive BOOLEAN NOT NULL DEFAULT FALSE COMMENT '0 => Enable, 1 => Archived' AFTER status, ADD createdDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER is_archive, ADD updatedDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER createdDate");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leaders', function (Blueprint $table) {
            //
            $table->dropColumn('status');
            $table->dropColumn('is_archive');
            $table->dropColumn('createdDate');
            $table->dropColumn('updatedDate');
        });
    }
}
