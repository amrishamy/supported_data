<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFitnessDiaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fitness_diary', function (Blueprint $table) {
            //
            DB::statement("ALTER TABLE fitness_diary ADD excercise_id INT(11) NOT NULL DEFAULT '0' AFTER dayNumber");

            DB::statement("ALTER TABLE fitness_diary DROP PRIMARY KEY, ADD PRIMARY KEY (userId, weekNumber, dayNumber, excercise_id) USING BTREE");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fitness_diary', function (Blueprint $table) {
            //
            $table->dropColumn('excercise_id');
        });
    }
}
