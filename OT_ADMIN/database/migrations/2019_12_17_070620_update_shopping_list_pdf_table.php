<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateShoppingListPdfTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shopping_list_pdf', function (Blueprint $table) {
            //
            DB::statement("ALTER TABLE shopping_list_pdf ADD status BOOLEAN NOT NULL DEFAULT TRUE COMMENT '0 => In-Active, 1 => Active' AFTER pdfFile, ADD isArchive BOOLEAN NOT NULL DEFAULT FALSE COMMENT '0 => Enable, 1 => Archived' AFTER status, ADD createdDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER isArchive, ADD updatedDate DATETIME on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER createdDate");

            DB::statement("ALTER TABLE shopping_list_pdf ADD orderBy INT(11) NOT NULL AFTER isArchive");

            DB::statement("ALTER TABLE shopping_list_pdf ADD pdfFile_veg VARCHAR(255) NOT NULL AFTER pdfFile");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shopping_list_pdf', function (Blueprint $table) {
            //
            $table->dropColumn('status');
            $table->dropColumn('isArchive');
            $table->dropColumn('createdDate');
            $table->dropColumn('updatedDate');
            $table->dropColumn('pdfFile_veg');
        });
    }
}
