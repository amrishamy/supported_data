<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFoodCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('food_categories', function (Blueprint $table) {
            //
            DB::statement("ALTER TABLE food_categories ADD isMealOfTheDay BOOLEAN NOT NULL DEFAULT FALSE COMMENT '1 => Will show meals of the day food items' AFTER isCombo");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('food_categories', function (Blueprint $table) {
            //
            $table->dropColumn('isMealOfTheDay');
            
        });
    }
}
