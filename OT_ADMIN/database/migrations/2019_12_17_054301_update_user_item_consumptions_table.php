<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUserItemConsumptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_item_consumptions', function (Blueprint $table) {
            //
            DB::statement("ALTER TABLE user_item_consumptions ADD quantity INT(11) NOT NULL DEFAULT '1' AFTER itemId");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_item_consumptions', function (Blueprint $table) {
            //
            $table->dropColumn('quantity');
        });
    }
}
