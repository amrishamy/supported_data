<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partners', function (Blueprint $table) {
            //
            DB::statement("ALTER TABLE partners ADD isArchive BOOLEAN NOT NULL DEFAULT FALSE COMMENT '1 if data is in archive mode, else 0' AFTER active");

            DB::statement("ALTER TABLE partners ADD image_link VARCHAR(255) NOT NULL AFTER image");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('partners', function (Blueprint $table) {
            //
            $table->dropColumn('isArchive');
            $table->dropColumn('image_link');
        });
    }
}
