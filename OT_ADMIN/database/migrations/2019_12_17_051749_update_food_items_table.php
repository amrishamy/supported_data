<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFoodItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('food_items', function (Blueprint $table) {
            //
            DB::statement("ALTER TABLE food_items ADD veg_item_id INT(11) NULL DEFAULT '0' AFTER userType");

            DB::statement("ALTER TABLE food_items ADD orderBy INT(11) NOT NULL DEFAULT '0' AFTER isArchive");
            DB::statement("ALTER TABLE food_items ADD food_type INT(11) NOT NULL DEFAULT '1' COMMENT '1 for veg, 0 for nonveg' AFTER userType");
            DB::statement("ALTER TABLE food_items ADD status BOOLEAN NOT NULL DEFAULT TRUE AFTER calories");
            DB::statement("ALTER TABLE food_items ADD isShutDownWeek BOOLEAN NOT NULL DEFAULT FALSE AFTER isVegAvailable");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('food_items', function (Blueprint $table) {
            //
            $table->dropColumn('veg_item_id');
            $table->dropColumn('orderBy');
            $table->dropColumn('food_type');
            $table->dropColumn('status');
            $table->dropColumn('isShutDownWeek');
        });
    }
}
