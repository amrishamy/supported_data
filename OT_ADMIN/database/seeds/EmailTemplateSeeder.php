<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class EmailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('emailtemplates')->insert(
        	[
        	'key' => 'birthday_template',
            'subject' => '!!Happy Birthday!!',
            'email_template' => 'Email Template',
            'status' => '1',
        	]);
        DB::table('emailtemplates')->insert(
        	[
        	'key' => 'forgot_password',
            'subject' => 'Forgot Password',
			'email_template' => '&#x3C;!DOCTYPE html PUBLIC &#x22;-//W3C//DTD XHTML 1.0 Transitional//EN&#x22; &#x22;http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd&#x22;&#x3E;
				&#x3C;html xmlns=&#x22;http://www.w3.org/1999/xhtml&#x22;&#x3E;
				    &#x3C;head&#x3E;
				        &#x3C;meta name=&#x22;viewport&#x22; content=&#x22;width=device-width&#x22; /&#x3E;
				        &#x3C;meta http-equiv=&#x22;Content-Type&#x22; content=&#x22;text/html; charset=UTF-8&#x22; /&#x3E;
				        &#x3C;title&#x3E;Forgot Password&#x3C;/title&#x3E;
				        &#x3C;link href=&#x22;http://mailgun.github.io/transactional-email-templates/styles.css&#x22; media=&#x22;all&#x22; rel=&#x22;stylesheet&#x22; type=&#x22;text/css&#x22; /&#x3E;
				    &#x3C;/head&#x3E;
				    &#x3C;body&#x3E;

				        &#x3C;table class=&#x22;body-wrap&#x22;&#x3E;
				            &#x3C;tr&#x3E;
				                &#x3C;td&#x3E;&#x3C;/td&#x3E;
				                &#x3C;td class=&#x22;container&#x22; width=&#x22;700&#x22; style=&#x22;background-color:#f5f5f5;color:#333;padding:15px;&#x22;&#x3E;
				                    &#x3C;div class=&#x22;content&#x22;&#x3E;
				                        &#x3C;table class=&#x22;main&#x22; width=&#x22;100%&#x22; cellpadding=&#x22;0&#x22; cellspacing=&#x22;0&#x22;&#x3E;
				                            &#x3C;tr&#x3E;
				                                &#x3C;td class=&#x22;content-wrap&#x22;&#x3E;
				                                    &#x3C;table width=&#x22;100%&#x22; cellpadding=&#x22;0&#x22; cellspacing=&#x22;0&#x22;&#x3E;
				                                        &#x3C;tr&#x3E;
				                                            &#x3C;td style=&#x22;background: #ffffff;text-align: center;padding: 22px;line-height: 0;font-size: 16px;margin: 0 auto;&#x22;&#x3E;
				                                                &#x3C;a style=&#x22;text-decoration: none; color: #fff;font-size: 28px;padding-left: 37px; position: relative;&#x22; href=&#x22;javascrip:void(0);&#x22;&#x3E;&#x9;
				                                                    &#x3C;img style=&#x22;width: 80px;&#x22; src=&#x22;{{asset(&#x27;images/logo.png&#x27;)}}&#x22; alt=&#x22;Work in Take&#x22;/&#x3E;
				                                                &#x3C;/a&#x3E;&#x3C;br/&#x3E;
				                                            &#x3C;/td&#x3E; 
				                                        &#x3C;/tr&#x3E;
				                                        &#x3C;tr&#x3E;
				                                            &#x3C;td style=&#x22;padding: 20px 0;line-height: 27px;font-size: 16px;margin-top: 20px;&#x22;&#x3E;
				                                                Hi {name},
				                                                &#x3C;br/&#x3E;
				                                                &#x3C;!--&#x3C;p&#x3E;&#x3C;/p&#x3E;Thank you for signup with Pawmoji.&#x3C;p&#x3E;&#x3C;/p&#x3E;--&#x3E;
				                                                You have requested to reset your password. Please click on below link to reset : &#x3C;br/&#x3E;&#x3C;br/&#x3E;
				                                                
				                                                &#x3C;strong&#x3E;Email:&#x3C;/strong&#x3E;{email}                                 
				                                        
				                                                &#x3C;br/&#x3E;
				                                                {URL}
				                                            &#x3C;/td&#x3E;
				                                        &#x3C;/tr&#x3E;

				                                    &#x3C;/table&#x3E;
				                                &#x3C;/td&#x3E;
				                            &#x3C;/tr&#x3E;
				                        &#x3C;/table&#x3E;
				                        &#x3C;div class=&#x22;footer&#x22;&#x3E;&#x3C;/div&#x3E;
				                    &#x3C;/div&#x3E;
				                &#x3C;/td&#x3E;
				                &#x3C;td&#x3E;&#x3C;/td&#x3E;
				            &#x3C;/tr&#x3E;
				        &#x3C;/table&#x3E;
				    &#x3C;/body&#x3E;
				&#x3C;/html&#x3E;',
            'status' => '1',
        	]);
		
		DB::table('emailtemplates')->insert(
        	[
        	'key' => 'user_registration_email',
            'subject' => 'Registration Email',
            'email_template' => '&#x3C;!DOCTYPE html PUBLIC &#x22;-//W3C//DTD XHTML 1.0 Transitional//EN&#x22; &#x22;http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd&#x22;&#x3E;
				&#x3C;html xmlns=&#x22;http://www.w3.org/1999/xhtml&#x22;&#x3E;
				    &#x3C;head&#x3E;
				        &#x3C;meta name=&#x22;viewport&#x22; content=&#x22;width=device-width&#x22; /&#x3E;
				        &#x3C;meta http-equiv=&#x22;Content-Type&#x22; content=&#x22;text/html; charset=UTF-8&#x22; /&#x3E;
				        &#x3C;title&#x3E;Welcome to In Take&#x3C;/title&#x3E;
				        &#x3C;link href=&#x22;http://mailgun.github.io/transactional-email-templates/styles.css&#x22; media=&#x22;all&#x22; rel=&#x22;stylesheet&#x22; type=&#x22;text/css&#x22; /&#x3E;
				    &#x3C;/head&#x3E;
				    &#x3C;body&#x3E;

				        &#x3C;table class=&#x22;body-wrap&#x22;&#x3E;
				            &#x3C;tr&#x3E;
				                &#x3C;td&#x3E;&#x3C;/td&#x3E;
				                &#x3C;td class=&#x22;container&#x22; width=&#x22;700&#x22; style=&#x22;background-color:#f5f5f5;color:#333;padding:15px;&#x22;&#x3E;
				                    &#x3C;div class=&#x22;content&#x22;&#x3E;
				                        &#x3C;table class=&#x22;main&#x22; width=&#x22;100%&#x22; cellpadding=&#x22;0&#x22; cellspacing=&#x22;0&#x22;&#x3E;
				                            &#x3C;tr&#x3E;
				                                &#x3C;td class=&#x22;content-wrap&#x22;&#x3E;
				                                    &#x3C;table width=&#x22;100%&#x22; cellpadding=&#x22;0&#x22; cellspacing=&#x22;0&#x22;&#x3E;
				                                        &#x3C;tr&#x3E;
				                                            &#x3C;td style=&#x22;background: #ffffff;text-align: center;padding: 22px;line-height: 0;font-size: 16px;margin: 0 auto;&#x22;&#x3E;
				                                                &#x3C;a style=&#x22;text-decoration: none; color: #fff;font-size: 28px;padding-left: 37px; position: relative;&#x22; href=&#x22;javascrip:void(0);&#x22;&#x3E;&#x9;
				                                                    &#x3C;img style=&#x22;width: 80px;&#x22; src=&#x22;{{asset(&#x27;images/logo.png&#x27;)}}&#x22; alt=&#x22;Work in Take&#x22;/&#x3E;
				                                                &#x3C;/a&#x3E;&#x3C;br/&#x3E;
				                                            &#x3C;/td&#x3E; 
				                                        &#x3C;/tr&#x3E;
				                                        &#x3C;tr&#x3E;
				                                            &#x3C;td style=&#x22;padding: 20px 0;line-height: 27px;font-size: 16px;margin-top: 20px;&#x22;&#x3E;
				                                                Hi {name},
				                                                &#x3C;br/&#x3E;
				                                                &#x3C;!--&#x3C;p&#x3E;&#x3C;/p&#x3E;Thank you for signup with Pawmoji.&#x3C;p&#x3E;&#x3C;/p&#x3E;--&#x3E;
				                                                Your account on InTake website has been created. Please click on below link to login: &#x3C;br/&#x3E;&#x3C;br/&#x3E;
				                                                
				                                                &#x3C;strong&#x3E;Email:&#x3C;/strong&#x3E;{email}                                 
				                                        
				                                                &#x3C;br/&#x3E;
				                                                {URL}
				                                            &#x3C;/td&#x3E;
				                                        &#x3C;/tr&#x3E;

				                                    &#x3C;/table&#x3E;
				                                &#x3C;/td&#x3E;
				                            &#x3C;/tr&#x3E;
				                        &#x3C;/table&#x3E;
				                        &#x3C;div class=&#x22;footer&#x22;&#x3E;&#x3C;/div&#x3E;
				                    &#x3C;/div&#x3E;
				                &#x3C;/td&#x3E;
				                &#x3C;td&#x3E;&#x3C;/td&#x3E;
				            &#x3C;/tr&#x3E;
				        &#x3C;/table&#x3E;
				    &#x3C;/body&#x3E;
				&#x3C;/html&#x3E;',
            'status' => '1',
        	]
    	);

DB::table('emailtemplates')->insert(
        	[
        	'key' => 'project_added',
            'subject' => 'A new project has been added.',
            'email_template' => '&#x3C;!DOCTYPE html PUBLIC &#x22;-//W3C//DTD XHTML 1.0 Transitional//EN&#x22; &#x22;http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd&#x22;&#x3E;
            &#x9;&#x9;&#x9;&#x9;&#x3C;html xmlns=&#x22;http://www.w3.org/1999/xhtml&#x22;&#x3E;
            &#x9;&#x9;&#x9;&#x9;    &#x3C;head&#x3E;
            &#x9;&#x9;&#x9;&#x9;        &#x3C;meta name=&#x22;viewport&#x22; content=&#x22;width=device-width&#x22; /&#x3E;
            &#x9;&#x9;&#x9;&#x9;        &#x3C;meta http-equiv=&#x22;Content-Type&#x22; content=&#x22;text/html; charset=UTF-8&#x22; /&#x3E;
            &#x9;&#x9;&#x9;&#x9;        &#x3C;title&#x3E;New Project Added to Work In Take Directory&#x3C;/title&#x3E;
            &#x9;&#x9;&#x9;&#x9;        &#x3C;link href=&#x22;http://mailgun.github.io/transactional-email-templates/styles.css&#x22; media=&#x22;all&#x22; rel=&#x22;stylesheet&#x22; type=&#x22;text/css&#x22; /&#x3E;
            &#x9;&#x9;&#x9;&#x9;    &#x3C;/head&#x3E;
            &#x9;&#x9;&#x9;&#x9;    &#x3C;body&#x3E;

            &#x9;&#x9;&#x9;&#x9;        &#x3C;table class=&#x22;body-wrap&#x22;&#x3E;
            &#x9;&#x9;&#x9;&#x9;            &#x3C;tr&#x3E;
            &#x9;&#x9;&#x9;&#x9;                &#x3C;td&#x3E;&#x3C;/td&#x3E;
            &#x9;&#x9;&#x9;&#x9;                &#x3C;td class=&#x22;container&#x22; width=&#x22;700&#x22; style=&#x22;background-color:#f5f5f5;color:#333;padding:15px;&#x22;&#x3E;
            &#x9;&#x9;&#x9;&#x9;                    &#x3C;div class=&#x22;content&#x22;&#x3E;
            &#x9;&#x9;&#x9;&#x9;                        &#x3C;table class=&#x22;main&#x22; width=&#x22;100%&#x22; cellpadding=&#x22;0&#x22; cellspacing=&#x22;0&#x22;&#x3E;
            &#x9;&#x9;&#x9;&#x9;                            &#x3C;tr&#x3E;
            &#x9;&#x9;&#x9;&#x9;                                &#x3C;td class=&#x22;content-wrap&#x22;&#x3E;
            &#x9;&#x9;&#x9;&#x9;                                    &#x3C;table width=&#x22;100%&#x22; cellpadding=&#x22;0&#x22; cellspacing=&#x22;0&#x22;&#x3E;
            &#x9;&#x9;&#x9;&#x9;                                        &#x3C;tr&#x3E;
            &#x9;&#x9;&#x9;&#x9;                                            &#x3C;td style=&#x22;background: #ffffff;text-align: center;padding: 22px;line-height: 0;font-size: 16px;margin: 0 auto;&#x22;&#x3E;
            &#x9;&#x9;&#x9;&#x9;                                                &#x3C;a style=&#x22;text-decoration: none; color: #fff;font-size: 28px;padding-left: 37px; position: relative;&#x22; href=&#x22;javascrip:void(0);&#x22;&#x3E;&#x9;
            &#x9;&#x9;&#x9;&#x9;                                                    &#x3C;img style=&#x22;width: 80px;&#x22; src=&#x22;{{asset(&#x26;#x27;images/logo.png&#x26;#x27;)}}&#x22; alt=&#x22;Work in Take&#x22;/&#x3E;
            &#x9;&#x9;&#x9;&#x9;                                                &#x3C;/a&#x3E;&#x3C;br/&#x3E;
            &#x9;&#x9;&#x9;&#x9;                                            &#x3C;/td&#x3E; 
            &#x9;&#x9;&#x9;&#x9;                                        &#x3C;/tr&#x3E;
            &#x9;&#x9;&#x9;&#x9;                                        &#x3C;tr&#x3E;
            &#x9;&#x9;&#x9;&#x9;                                            &#x3C;td style=&#x22;padding: 20px 0;line-height: 27px;font-size: 16px;margin-top: 20px;&#x22;&#x3E;
            &#x9;&#x9;&#x9;&#x9;                                                Hi Admin,
            &#x9;&#x9;&#x9;&#x9;                                                &#x3C;br/&#x3E;
            &#x9;&#x9;&#x9;&#x9;                                                &#x3C;!--&#x3C;p&#x3E;&#x3C;/p&#x3E;Thank you for signup with Pawmoji.&#x3C;p&#x3E;&#x3C;/p&#x3E;--&#x3E;
            &#x9;&#x9;&#x9;&#x9;                                                A new project {project_name} has been added to Work in take diectory by user {name}.
            &#x9;&#x9;&#x9;&#x9;                                                &#x3C;br/&#x3E;&#x3C;br/&#x3E;
            &#x9;&#x9;&#x9;&#x9;                                                                               
            &#x9;&#x9;&#x9;&#x9;                                        
            &#x9;&#x9;&#x9;&#x9;                                            &#x3C;/td&#x3E;
            &#x9;&#x9;&#x9;&#x9;                                        &#x3C;/tr&#x3E;

            &#x9;&#x9;&#x9;&#x9;                                    &#x3C;/table&#x3E;
            &#x9;&#x9;&#x9;&#x9;                                &#x3C;/td&#x3E;
            &#x9;&#x9;&#x9;&#x9;                            &#x3C;/tr&#x3E;
            &#x9;&#x9;&#x9;&#x9;                        &#x3C;/table&#x3E;
            &#x9;&#x9;&#x9;&#x9;                        &#x3C;div class=&#x22;footer&#x22;&#x3E;&#x3C;/div&#x3E;
            &#x9;&#x9;&#x9;&#x9;                    &#x3C;/div&#x3E;
            &#x9;&#x9;&#x9;&#x9;                &#x3C;/td&#x3E;
            &#x9;&#x9;&#x9;&#x9;                &#x3C;td&#x3E;&#x3C;/td&#x3E;
            &#x9;&#x9;&#x9;&#x9;            &#x3C;/tr&#x3E;
            &#x9;&#x9;&#x9;&#x9;        &#x3C;/table&#x3E;
            &#x9;&#x9;&#x9;&#x9;    &#x3C;/body&#x3E;
            &#x9;&#x9;&#x9;&#x9;&#x3C;/html&#x3E;',
            'status' => '1',
        	]
    	);
    }
}
