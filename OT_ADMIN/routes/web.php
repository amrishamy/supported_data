<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*Route::group(['prefix' => 'admin'/*,  'middleware' => 'auth'*//*], function(){*/

// Admin Authenticate & Forgot Password Routes
//Users CRUD Routes
Route::get('/', function () { return redirect()->action('Admin\LoginController@loginForm'); });

Route::get('/login', ['as' => 'admin.login', 'uses' => 'Admin\LoginController@loginForm']);
Route::get('/forgot', ['as' => 'admin.forgot', 'uses' => 'Admin\LoginController@forgotForm']);
Route::post('/authenticate', ['as' => 'admin.authenticate', 'uses' => 'Admin\LoginController@authenticate']);
Route::post('/admin_forgot_password', ['as' => 'admin.forgot_password', 'uses' => 'Admin\LoginController@admin_forgot_password']);
Route::get('/resetPassword/{token}', ['as' => 'admin.resetpassword', 'uses' => 'Admin\LoginController@showResetPassword']);
Route::post('/updatePassword', ['as' => 'admin.update_password', 'uses' => 'Admin\LoginController@updatePassword']);
Route::get('/logout', ['as' => 'admin.logout', 'uses' => 'Admin\LoginController@logout']);


Route::group(['middleware' => 'auth'], function(){

	Route::get('/dashboard', ['as' => 'admin.dashboard', 'uses' => 'Admin\DashboardController@dashboard']);
	
	//Food Items CRUD Routes
	Route::get( '/food_items', ['as' => 'admin.food_items', 'uses' => 'Admin\FoodItemController@index'] );
	Route::get( '/category/{category_id}/food_items', ['as' => 'admin.category_food_items', 'uses' => 'Admin\FoodItemController@index'] );
	Route::post( '/food_items_list/{category_id}', ['as' => 'admin.food_items_list', 'uses' => 'Admin\FoodItemController@list']);
	Route::post( '/get_food_items', ['as' => 'admin.get_food_items', 'uses' => 'Admin\FoodItemController@getFoodItems']);
	Route::get('/food/add', ['as' => 'admin.addfood', 'uses' => 'Admin\FoodItemController@create']);
	Route::post('/savefood', ['as' => 'admin.savefood', 'uses' => 'Admin\FoodItemController@store']);
	Route::get('/food/edit/{id}', ['as' => 'admin.editfood', 'uses' => 'Admin\FoodItemController@edit']);
	Route::post('/food/update', ['as' => 'admin.updatefood', 'uses' => 'Admin\FoodItemController@update']);
	Route::post('/food/update/reorder', ['as' => 'admin.updateFoodItemReorder', 'uses' => 'Admin\FoodItemController@updateFoodItemReorder']);
	Route::post('/category/{category_id}/food/update/reorder', ['as' => 'admin.updateCategoryWiseFoodItemReorder', 'uses' => 'Admin\FoodItemController@updateCategoryWiseFoodItemReorder']);
	Route::get('/food/delete/{id}', ['as' => 'admin.deletefood', 'uses' => 'Admin\FoodItemController@destroy']);
	Route::post( '/food/delete/image/{id}', [ 'as' => 'admin.delete_food_item_image', 'uses' => 'Admin\FoodItemController@deleteImage']);
	Route::post('/food_status_update', ['as' => 'admin.food_status_update', 'uses' => 'Admin\FoodItemController@food_status_update']);

	//Food Category CRUD Routes
	Route::get( '/category', [ 'as' => 'admin.categorylist', 'uses' => 'Admin\CategoryController@index' ] );
	Route::post( '/catgories_list', ['as' => 'admin.catgories_list', 'uses' => 'Admin\CategoryController@list']);
	Route::get('/category/add', ['as' => 'admin.addcategory', 'uses' => 'Admin\CategoryController@create']);
	Route::post('/savecategory', ['as' => 'admin.savecategory', 'uses' => 'Admin\CategoryController@store']);
	Route::get('/category/edit/{id}', ['as' => 'admin.editcategory', 'uses' => 'Admin\CategoryController@edit']);
	Route::post('/category/update', ['as' => 'admin.updatecategory', 'uses' => 'Admin\CategoryController@update']);
	Route::post('/category/update/reorder', ['as' => 'admin.updateCategoryItemReorder', 'uses' => 'Admin\CategoryController@updateCategoryItemReorder']);
	Route::get('/category/delete/{id}', ['as' => 'admin.deletecategory', 'uses' => 'Admin\CategoryController@destroy']);
	Route::post( '/category/delete/image/{id}', [ 'as' => 'admin.delete_category_image', 'uses' => 'Admin\CategoryController@deleteImage']);
	Route::post('/category_status_update', ['as' => 'admin.category_status_update', 'uses' => 'Admin\CategoryController@category_status_update']);

	//Leaders CRUD Routes
	Route::get( '/leaders', [ 'as' => 'admin.leaders', 'uses' => 'Admin\LeadersController@index' ] );
	Route::post( '/leaders_list', ['as' => 'admin.leaders_list', 'uses' => 'Admin\LeadersController@list']);
	Route::post( '/get_leaders', ['as' => 'admin.get_leaders', 'uses' => 'Admin\LeadersController@getLeaders']);
	Route::get( '/leader/add', [ 'as' => 'admin.add_leader', 'uses' => 'Admin\LeadersController@create']);
	Route::post( '/saveleader', [ 'as' => 'admin.save_leader', 'uses' => 'Admin\LeadersController@store']);
	Route::get( '/leader/edit/{id}', [ 'as' => 'admin.edit_leader', 'uses' => 'Admin\LeadersController@edit']);
	Route::post( '/leader/update/{id}', [ 'as' => 'admin.update_leader', 'uses' => 'Admin\LeadersController@update']);
	Route::post( '/leader/delete/image/{id}', [ 'as' => 'admin.delete_leader_image', 'uses' => 'Admin\LeadersController@deleteImage']);
	Route::get( '/leader/delete/{id}', [ 'as' => 'admin.delete_leader', 'uses' => 'Admin\LeadersController@destroy']); 
	Route::post('/leader/status/update', ['as' => 'admin.leader_status_update', 'uses' => 'Admin\LeadersController@leaderStatusUpdate']);

	//Exercise CRUD Routes
	Route::get( '/leader/{leaderId}/exercises', [ 'as' => 'admin.exercises', 'uses' => 'Admin\ExerciseController@index' ] );
	Route::post( '/leader/{leaderId}/exercise_list', [ 'as' => 'admin.exercise_list', 'uses' => 'Admin\ExerciseController@list']);
	Route::get( '/leader/{leaderId}/exercise/add', [ 'as' => 'admin.add_exercise', 'uses' => 'Admin\ExerciseController@create']);
	Route::post( '/leader/{leaderId}/save_exercise', [ 'as' => 'admin.save_exercise', 'uses' => 'Admin\ExerciseController@store']);
	Route::get( '/leader/{leaderId}/exercise/edit/{id}', [ 'as' => 'admin.edit_exercise', 'uses' => 'Admin\ExerciseController@edit']);
	Route::post( '/leader/{leaderId}/exercise/update/{id}', [ 'as' => 'admin.update_exercise', 'uses' => 'Admin\ExerciseController@update']);
	Route::post('/leader/{leaderId}/exercise/update_reorder', ['as' => 'admin.updateExerciseReorder', 'uses' => 'Admin\ExerciseController@updateReorder']);
	Route::get( '/leader/{leaderId}/exercise/delete/{id}', [ 'as' => 'admin.delete_exercise', 'uses' => 'Admin\ExerciseController@destroy']); 
	Route::post('/leader/{leaderId}/exercise/status/update', ['as' => 'admin.exercise_status_update', 'uses' => 'Admin\ExerciseController@statusUpdate']);

	//Users CRUD Routes
	Route::match(['get', 'post'],'/users', ['as' => 'admin.userlist', 'uses' => 'Admin\UsersController@userListing']);
	Route::get('/adduser', ['as' => 'admin.adduser', 'uses' => 'Admin\UsersController@addUser']);
	Route::post('/saveuser', ['as' => 'admin.saveuser', 'uses' => 'Admin\UsersController@saveUser']);
	Route::get('/edituser/{id}', ['as' => 'admin.edituser', 'uses' => 'Admin\UsersController@editUser']);
	Route::post('/updateuser', ['as' => 'admin.updateuser', 'uses' => 'Admin\UsersController@updateUser']);
	Route::get('/userajaxdata', ['as' => 'admin.userajaxdata', 'uses' => 'Admin\UsersController@ajaxDataLoad']);

	//Admin Profile Routes
	Route::get('/profile', ['as' => 'admin.profile', 'uses' => 'Admin\ProfileController@adminprofile']);
	Route::post('/profileupdate', ['as' => 'admin.profileupdate', 'uses' => 'Admin\ProfileController@profileupdate']);
	Route::post('/updatepassword', ['as' => 'admin.updatepassword', 'uses' => 'Admin\ProfileController@updatepassword']);

	//Single & Bulk Delete Routes
	Route::post('/deleterow', ['as' => 'admin.deleterow', 'uses' => 'Admin\CommonController@deleteRow']);
	Route::post('/updatebulkrows', ['as' => 'admin.updatebulkrows', 'uses' => 'Admin\CommonController@updateBulkRows']);

	//Email Template Routes
	Route::match(['get', 'post'],'/emails', ['as' => 'admin.emailslist', 'uses' => 'Admin\EmailsController@emailsListing']);
	Route::get('/addemail', ['as' => 'admin.addemail', 'uses' => 'Admin\EmailsController@addEmail']);
	Route::get('/emailajaxdata', ['as' => 'admin.emailajaxdata', 'uses' => 'Admin\EmailsController@ajaxDataLoad']);
	Route::post('/saveemail', ['as' => 'admin.saveemail', 'uses' => 'Admin\EmailsController@saveEmail']);
	Route::get('/editemail/{id}', ['as' => 'admin.editemail', 'uses' => 'Admin\EmailsController@editEmail']);
	Route::post('/updateemail', ['as' => 'admin.updateemail', 'uses' => 'Admin\EmailsController@updateEmail']);

	//Partners CRUD Routes
	Route::get( '/partner', [ 'as' => 'admin.partnerlist', 'uses' => 'Admin\PartnerController@index' ] );
    Route::post( '/partners_list', ['as' => 'admin.partners_list', 'uses' => 'Admin\PartnerController@list']);
	Route::get('/partner/add', ['as' => 'admin.addpartner', 'uses' => 'Admin\PartnerController@create']);
	Route::post('/savepartner', ['as' => 'admin.savepartner', 'uses' => 'Admin\PartnerController@store']);
	Route::get('/partner/edit/{id}', ['as' => 'admin.editpartner', 'uses' => 'Admin\PartnerController@edit']);
    Route::post('/partner/update', ['as' => 'admin.updatepartner', 'uses' => 'Admin\PartnerController@update']);
    
    Route::post('/partner/update/reorder', ['as' => 'admin.updatePartnerItemReorder', 'uses' => 'Admin\PartnerController@updatePartnerItemReorder']);
	Route::get('/partner/delete/{id}', ['as' => 'admin.deletepartner', 'uses' => 'Admin\PartnerController@destroy']);
	Route::post( '/partner/delete/image/{id}', [ 'as' => 'admin.delete_partner_image', 'uses' => 'Admin\PartnerController@deleteImage']);
	Route::post('/partner_status_update', ['as' => 'admin.partner_status_update', 'uses' => 'Admin\PartnerController@partner_status_update']);

    //Shopping List CRUD Routes
    Route::get( '/shopping', [ 'as' => 'admin.shoppinglist', 'uses' => 'Admin\ShoppingListController@index' ] );
    Route::post( '/shopping_list', ['as' => 'admin.shopping_list', 'uses' => 'Admin\ShoppingListController@list']);
	Route::get('/shopping/add', ['as' => 'admin.addshopping', 'uses' => 'Admin\ShoppingListController@create']);
	Route::post('/saveshopping', ['as' => 'admin.saveshopping', 'uses' => 'Admin\ShoppingListController@store']);
	Route::get('/shopping/edit/{id}', ['as' => 'admin.editshopping', 'uses' => 'Admin\ShoppingListController@edit']);
    Route::post('/shopping/update', ['as' => 'admin.updateshopping', 'uses' => 'Admin\ShoppingListController@update']);
    Route::post('/shopping/update/reorder', ['as' => 'admin.updateShoppingItemReorder', 'uses' => 'Admin\ShoppingListController@updateShoppingItemReorder']);
	Route::get('/shopping/delete/{id}', ['as' => 'admin.deleteshopping', 'uses' => 'Admin\ShoppingListController@destroy']);
    Route::post('/shopping_status_update', ['as' => 'admin.shopping_status_update', 'uses' => 'Admin\ShoppingListController@shopping_status_update']);
    
    //Daily Meal Plan Routes
    Route::get( '/dailymealplan', [ 'as' => 'admin.dailymealplanlist', 'uses' => 'Admin\DailyMealPlanController@index' ] );
    Route::post( '/dailymealplan_list', ['as' => 'admin.dailymealplan_list', 'uses' => 'Admin\DailyMealPlanController@list']);
	Route::get('/dailymealplan/edit/{id}', ['as' => 'admin.editdailymealplan', 'uses' => 'Admin\DailyMealPlanController@edit']);
    Route::post('/dailymealplan/update/{id}', ['as' => 'admin.updatedailymealplan', 'uses' => 'Admin\DailyMealPlanController@update']);
    /*
    Route::post('/shopping/update/reorder', ['as' => 'admin.updateShoppingItemReorder', 'uses' => 'Admin\ShoppingListController@updateShoppingItemReorder']);

	Route::get('/shopping/delete/{id}', ['as' => 'admin.deleteshopping', 'uses' => 'Admin\ShoppingListController@destroy']);
	Route::post('/shopping_status_update', ['as' => 'admin.shopping_status_update', 'uses' => 'Admin\ShoppingListController@shopping_status_update']);*/

});

/*});*/


/*Route::get('/landingpage', ['as' => 'landingpage', 'uses' => 'FrontendController@landingpage']);

Route::get('/', function () { return redirect('/landingpage'); });*/

/*Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');*/

Route::get('test', function () {
    Mail::to('sam@yopmail.com')->send(new App\Mail\TestAmazonSes('It works!'));
});
