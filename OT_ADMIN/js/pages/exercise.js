(function( $ ){
  // var getDaysList = function( $leaderId, $selected ){ 
  //   if( !$leaderId ) { 
  //     $( '.days-wrap select' ).html( '<option value="">Select Day</option>' );
  //   } else { 
  //     $.ajax({
  //           url     : exercise.days_available_for_leader,
  //           type    : 'POST',
  //           data    : { 'leaderId':$leaderId },
  //           dataType: 'json',
  //           error:  function ( xhr, status, error ) { 
  //               swal( 'Error!', 'Unable to get available days, please try again later.', 'error' );
  //           },
  //           success : function ( json ) { 
  //               if( json.status == 0 ){ 
  //                 $( '.days-wrap select' ).html( '<option value="">Select Day</option>' );
  //                 swal( 'Error!', json.msg, 'error' );
  //               } else { 
  //                 if( json.error ){ 
  //                   $( '.days-wrap select' ).html( '<option value="">Select Day</option>' );

  //                   swal({
  //                       title: "Error!",
  //                       text: json.error,
  //                       type: "error",
  //                       showCancelButton: true,
  //                       confirmButtonClass: "btn-danger",
  //                       confirmButtonText: "All Exercises!",
  //                       cancelButtonText: "Change Leader!",
  //                       closeOnConfirm: true,
  //                       closeOnCancel: true
  //                     },
  //                     function(isConfirm) { 
  //                       if (isConfirm) {
  //                         window.location.href = exercise.list_url;
  //                       } else {
  //                         $( '.days-wrap select' ).html( '<option value="">Select Day</option>' );
  //                       }
  //                     });
  //                 } else { 
  //                     $( '.days-wrap select' ).html( json.success );
  //                 }
  //               }
  //           }
  //       });
  //   }
  // };

  $( document ).ready(function(){
    // $( "#leaders-list" ).autocomplete({
    //     source: function( request, response ) {
    //      // Fetch data
    //      $.ajax({
    //       url: exercise.category_url,
    //       type: 'post',
    //       dataType: "json",
    //       data: {
    //        search: request.term
    //       },
    //       success: function( data ) {
    //         response( data );
    //       }
    //      });
    //     },
    //     select: function (event, ui) {
    //      $('#leaders-list').val(ui.item.label); 
    //      $('#leaderId').val(ui.item.value);
    //      return false;
    //     }
    //  });

    if( $("#add_exercise_frm").length > 0 ){ 
        $("#add_exercise_frm").validate({
            rules: {
               name: { required: true },
               leaderId: { required:true },
               calories: {
                    required: true,
                    number: true
                  },
               dayNumber: { required:true }
              },
              messages: {
                  name:{
                    required: 'Please enter exercise name'
                  },
                  leaderId:{
                    required: 'Please select leader'
                  },
                  calories:{
                    required: 'Please enter calories burned'
                  },
                  dayNumber:{
                    required: 'Please select day number'
                  },
              },
              submitHandler: function(form) {
                tinymce.triggerSave();
                form.submit();
              }
         });
    }
    if( $("#edit_exercise_frm").length > 0 ){ 
        $("#edit_exercise_frm").validate({
            rules: {
               name: { required: true },
               calories: {
                  required: true,
                  number: true
                }
              },
              messages: {
                  name:{
                    required: 'Please enter exercise name'
                  },
                  calories:{
                    required: 'Please enter calories burned'
                  }
              },
              submitHandler: function(form) {
                tinymce.triggerSave();
                form.submit();
              }
         });
    }

    // if( $("#add_exercise_frm").length > 0 ){ 
    //   getDaysList( $( '#leaderId' ).val(), $( '#leaderId' ).data( 'selected' ) );
    //   $( '#leaderId' ).on('change', function(){
    //     $leaderId = $( this ).val();
    //     getDaysList( $leaderId, $( this ).data( 'selected' ) );
    //   });
    // }

    // if( typeof tab != typeof undefined ){ 
    //   $( document ).on('change', '#leader-options', function(){ 
    //     tab.ajax.reload( );
    //   });
    // }

  });
 })( jQuery );