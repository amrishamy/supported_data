(function( $ ){
  $( document ).ready(function(){
    $("#edit_leader_frm, #add_leader_frm").validate({
        rules: {
           leaderName: { required: true },
           age: { required:true },
           weight: { required:true },
           location: { required:true }
          },
          messages: {
              leaderName:{
                required: 'Please enter name'
              },
              age:{
                required: 'Please enter age'
              },
              weight:{
                required: 'Please enter weights'
              },
              location:{
                required: 'Please enter location'
              },
          },
          submitHandler: function(form) {
            form.submit();
          },
          errorPlacement: function(error, element) {
             $( element ).closest( '.form-group' ).append( error );
          }
     });
  });
 })( jQuery );