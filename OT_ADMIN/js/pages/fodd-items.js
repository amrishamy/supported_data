(function( $ ){
  var getFilteredFoodItems = function( $search, $selected ){ 
      $( '#fi-loader' ).show();
      $.ajax({
            url     : fiObj.get_food_items,
            type    : 'POST',
            data    : { 'search':$search, 'selected':$selected, 'veg': 'only' },
            dataType: 'json',
            error:  function ( xhr, status, error ) { 
                $( '#fi-loader' ).hide();
                swal( 'Error!', 'Unable to get food items, please try again later.', 'error' );
            },
            success : function ( json ) { 
                $( '#fi-loader' ).hide();
                $( '#fi-list-wrap' ).html( json.html );
            }
        });
  };

  $( document ).ready(function(){ 
      $('#show-exist-veg-items').on('shown.bs.modal', function ( e ) { 
        getFilteredFoodItems( $( '#fi-srch' ).val( ), fiObj.selected_fi );
      });

      $('#fi-srch').on('keyup keydown', function ( e ) { 
        getFilteredFoodItems( $( this ).val( ), fiObj.selected_fi );
      });

      $( document).on( 'click', ".select_meal_plan_day_btn", function( e ) { 
         var itemId = $( "input[name='daily_meal_selection' ]:checked").val( );
         var title = $( "input[name='daily_meal_selection' ]:checked").attr( 'title' );
         $( '#itemId' ).val( itemId );
         $( '#fi-title' ).text( title );
         $('#show-exist-veg-items').modal('hide');
         $( '#fi-list-wrap' ).html( '' );
         $( '#fi-srch' ).val( '' );
         return false;
      });
  });
})( jQuery );