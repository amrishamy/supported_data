(function( $ ){
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  
   $('#image_link').blur(function(e) {
       var ini = $(this).val().substring(0, 4);
       console.log(ini);
       if (ini !== '' && ini !== 'http'){
            // get value from field
           var cur_val = $(this).val(); 
           // do with cur_val
           $(this).val('http://' + cur_val);
       }        
   });
   

  deleteImage = function ( url, $currentObj ) { 
    swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
      function(){ 
        swal.close();
        $.ajax({
            url     : url,
            type    : 'POST',
            data    : {},
            dataType: 'json',
            error:  function ( xhr, status, error ) { 
                swal( 'Error!', 'Unable to delete, please try again later.', 'error' );
            },
            success : function ( json ) { 
                if( json.status == 0 ){ 
                  swal( 'Error!', json.msg, 'error' );
                } else { 
                  swal( 'Deleted!', json.msg, 'success' );
                  $( $currentObj ).closest( '.img-wrap' ).remove();
                }
            }
        });
      });
  };

  list = function ( obj ) { 

      if( typeof obj == typeof undefined || !( typeof obj.listing_url != typeof undefined && obj.listing_url ) ) return false;
      if( !( typeof obj.reorder != typeof undefined && obj.reorder ) ) obj.reorder = false;

      $ajaxObj = {
                "url": obj.listing_url,
                "type": "POST",
                "error": function(){
                  swal( 'Error!', 'Unable to load, please try again later.', 'error' );
                }
            };
      if( typeof obj.ajaxData != typeof undefined ) { 
          $ajaxObj.data = obj.ajaxData;
      }

      var params = {
                    "processing": true,
                    "serverSide": true,
                    "ajax": $ajaxObj,
                    "drawCallback": function( settings ) {
                        var pagination = $( this ).closest( '.dataTables_wrapper' ).find( '.dataTables_paginate' );
                        pagination.toggle( this.api( ).page.info( ).pages > 1 );
                        if( typeof obj.drawCallback != typeof undefined ) { 
                            obj.drawCallback( settings, this );
                        }
                    },
                    
                };
      if( obj.reorder ) { 
        params.rowReorder = { update: false };
      }
      if( typeof obj.orderable != typeof undefined && obj.orderable ) { 
        params.columnDefs = [ obj.orderable ];
      } else { 
        params.ordering = false;
      }

      var table = $( '#datatable' ).DataTable( params );

      if( obj.reorder && typeof obj.reorder_url != typeof undefined && obj.reorder_url ) { 
        table.on('row-reorder', function ( e, diff, edit ) { 
            
            var reOrderData = { 'replace_ids':{} };
            if( diff.length > 0 ) { 
              $upOrDown = diff[0].oldPosition - diff[0].newPosition == 1?'down':'up';
              for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
                  var rowData = table.row( diff[i].node ).data( );

                  if( $upOrDown == 'down' ) {  
                    var $newIndex = ( i == 0 )?ien-1:i-1;
                  } else { 
                    var $newIndex = ( i == ien-1 )?0:i+1;
                  }

                  var replacedData = table.row( diff[$newIndex].node ).data( );

                  reOrderData.replace_ids[ rowData[ obj.idIndex ] ] = replacedData[ obj.orderIndex ];
              }

              $.ajax({
                  url     : obj.reorder_url,
                  type    : 'POST',
                  data    : reOrderData,
                  dataType: 'json',
                  error:  function ( xhr, status, error ) { 
                      swal( 'Error!', 'Unable to update order, please try again later.', 'error' );
                  },
                  success : function ( json ) { 
                      if( json.errors ){ 
                        swal( 'Error!', json.errors, 'error' );
                      } else { 
                        swal( 'Updated!', json.success, 'success' );
                        table.ajax.reload( null, false );
                      }
                  }
              });

            }

        });

      }
      
      return table;
  };

})( jQuery );

$(document).ready(function() {

   $( '.file-upload-default' ).on('change', function(e){
      $(this).parent('.form-group').find( '.file-upload-info' ).val( e.target.files[0].name );
   });
   
    $("#login_frm").validate({
      rules: {
         email: {
            required: true,
            email: true,  //add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
         },
         password: {required:true, minlength: 6}
        },
        messages: {
	    	email:{
	    		required: 'Please enter email',
	    		email: 'Please enter a valid email',
	    		maxlength: 'Email can not be greater than {0} characters'
	    	},
	    	password: {
	    		required: 'Please enter password',
	    		minlength: "Please enter at least {0} characters"	
	    	}
	    },
	    submitHandler: function(form) {
   		  form.submit();
	    }
   
   });

    $(".forget-form").validate({
      rules: {
        forgot_email: {
            required: true,
            email: true,  //add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
         }
       },
        messages: {
	    	email:{
	    		required: 'Please enter email',
	    		email: 'Please enter a valid email',
	    		maxlength: 'Email can not be greater than {0} characters'
	    	}
	    },
	 submitHandler: function(form) {
   		form.submit();
	 }
   
   });

    jQuery.validator.addMethod("lettersonly", function(value, element) {
	     return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
	   }, ""); 

   $("#add_user_frm").validate({
      rules: {
      	 fullname: {required:true, lettersonly: true},
         email: {
            required: true,
            email: true,  //add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
         },
         password: {required:true, minlength: 6},
         gender: { required: true }
        },
        messages: {
        	fullname: {required: "Please enter fullname", lettersonly: "Enter only alphabets"},
        	email:{
	    		required: 'Please enter email',
	    		email: 'Please enter a valid email',
	    		maxlength: 'Email can not be greater than {0} characters'
	    	},
	    	password: {
	    		required: 'Please enter password',
	    		minlength: "Please enter at least {0} characters"	
	    	},
	    	gender : 'Please select gender type'
	    },
	    submitHandler: function(form) {
   		  form.submit();
	    }
   
   }); 

   $("#edit_user_frm").validate({
      rules: {
      	 fullname: {required:true, lettersonly: true},
         email: {
            required: true,
            email: true,  //add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
         },
         password: {required:true, minlength: 6},
         gender: { required: true }
        },
        messages: {
        	fullname: {required: "Please enter fullname", lettersonly: "Enter only alphabets"},
        	email:{
	    		required: 'Please enter email',
	    		email: 'Please enter a valid email',
	    		maxlength: 'Email can not be greater than {0} characters'
	    	},
	    	password: {
	    		required: 'Please enter password',
	    		minlength: "Please enter at least {0} characters"	
	    	},
	    	gender : 'Please select gender type'
	    },
	    submitHandler: function(form) {
   		  form.submit();
	    }
   
   });
    
   $("#profile_update_frm").validate({
      rules: {
         fullname: {required:true, lettersonly: true},
         email: {
            required: true,
            email: true,  //add an email rule that will ensure the value entered is valid email id.
            maxlength: 255,
         }
        },
        messages: {
          fullname: {required: "Please enter fullname", lettersonly: "Enter only alphabets"},
          email:{
          required: 'Please enter email',
          email: 'Please enter a valid email',
          maxlength: 'Email can not be greater than {0} characters'
        }
      },
      submitHandler: function(form) {
        form.submit();
      }
   
   });

   $("#update_password_frm").validate({
      rules: {
         old_password: {required:true, minlength: 6},
         password: {required:true, minlength: 6},
         cpassword: {required:true, minlength: 6, equalTo: "#password"}
        },
        messages: {
          old_password: {
            required: 'Please enter old password',
            minlength: "Please enter at least {0} characters"
          },
          password: {
            required: 'Please enter new password',
            minlength: "Please enter at least {0} characters" 
          },
          cpassword: {
            required: 'Please enter confirm password',
            minlength: "Please enter at least {0} characters",
            equalTo: "Please enter confirm password same as password"

          }
      },
      submitHandler: function(form) {
        form.submit();
      }
   
   });

      
      // Function for delete the Record on Listing page using model box open
      

        $('body').on("click", ".delete_record", function (e) {
            e.preventDefault();
            
            var del_route = $(this).attr('href');
         
            $(".modal-footer .model_action_confirm").attr( 'href' ,del_route );
            $('.modal').modal('show');
        });

        toggleIsVegAvailableCheckbox = function( el ){ 
           if( $( el ).val( ) == 'nonveg' ){ 
               $('.is_veg_available').show();
           } else { 
               $( '.isVegAvailable, .add_veg' ).prop( 'checked', false );
               $( '.optional_veg_item, .add_veg_section, .is_veg_available, #existing-wrap, #new-item-wrap' ).hide();
           }
        };

        // check food type during edit food item
        toggleIsVegAvailableCheckbox( "input[name='food_type']:checked" );

         // check food type (veg , non veg ) during aading food item
         $(".food_type").change(function(){
            toggleIsVegAvailableCheckbox( "input[name='food_type']:checked" );
         });

         toggleIsVegAvailable = function( el ){
            if( $(el).prop('checked') == true ) { 
               $('.optional_veg_item, .add_veg_section').show();
            } else {
               $( '.add_veg' ).prop( 'checked', false );
               $('.optional_veg_item, .add_veg_section').hide();
               $( '#existing-wrap, #new-item-wrap' ).hide();
            }
         };

         toggleIsVegAvailable( ".isVegAvailable" );
         $(".isVegAvailable").change(function(){
            toggleIsVegAvailable( this );
         });

         // check add veg (exist or new) during edit food item
         toggleExistingNewVegForm = function( el ){ 
            var add_veg = $( el ).val( );
            $('.optional_veg_item').show();
            $( '#existing-wrap, #new-item-wrap' ).hide();
            if(add_veg == 'existing' ) {
               $( '#existing-wrap' ).show();
            } else if(add_veg == 'new') { 
               $( '#new-item-wrap' ).show();
            }
         }

         toggleExistingNewVegForm( ".add_veg:checked" );
         $(".add_veg").on( 'change', function(){
            toggleExistingNewVegForm( ".add_veg:checked" );
         });

  
         var foodFormValidate = $( "#add_food_item_frm" ).validate({
            ignore: "",
            rules: {
               itemName: {required:true},
               "food_category[]": {required:true},
               serveName: {required:true},
               calories: {required:true,number: true},
               userType: {required:true},
               itemIngredients: {required:true},
               itemMethod: {required:true},
               food_type: { required: true },
               add_veg: {required:function(){
                     return $('.isVegAvailable').prop('checked');
                  }
               },
               /*isVegAvailable: { required: function(){
                     if( $("input[name='food_type']:checked").val() == 'nonveg' )
                     return true;
                  }
               },*/

               itemName_veg: {required:function(){
                    return ( $('.isVegAvailable').prop('checked') && $( 'input[ name="add_veg" ]:checked' ).val() == 'new' );
                  }
               },
               serveName_veg: {required:function(){
                    return ( $('.isVegAvailable').prop('checked') && $( 'input[ name="add_veg" ]:checked' ).val() == 'new' );
                  }
               },
               calories_veg: {number: true,required:function(){
                    return ( $('.isVegAvailable').prop('checked') && $( 'input[ name="add_veg" ]:checked' ).val() == 'new' );
                  }
               },
               userType_veg: {required:function(){
                    return ( $('.isVegAvailable').prop('checked') && $( 'input[ name="add_veg" ]:checked' ).val() == 'new' );
                  }
               },
               itemIngredients_veg: {required:function(){
                  return ( $('.isVegAvailable').prop('checked') && $( 'input[ name="add_veg" ]:checked' ).val() == 'new' );
                  }
               },
               itemMethod_veg: {required:function(){
                  return ( $('.isVegAvailable').prop('checked') && $( 'input[ name="add_veg" ]:checked' ).val() == 'new' );
                  }
               },
               itemId: {required:function(){
                  return ( $('.isVegAvailable').prop('checked') && $( 'input[ name="add_veg" ]:checked' ).val() == 'existing' );
                  }
               }
            },
            messages: {
               food_type: "Select any one of food type",
            },
            errorPlacement: function(error, element) {
               $( element ).closest( '.form-group' ).append( error );
            }
         });

         $("#edit_food_item_frm").validate({
            ignore: "",
            rules: {
               itemName: {required:true},
               "food_category[]": {required:true},
               serveName: {required:true},
               calories: {required:true,number: true},
               userType: {required:true},
               itemIngredients: {required:true},
               itemMethod: {required:true},
               
               food_type: { required: true },
               add_veg: {required:function(){
                     return $('.isVegAvailable').prop('checked');
                  }
               },
               /*isVegAvailable: { required: function(){
                     if( $("input[name='food_type']:checked").val() == 'nonveg' )
                     return true;
                  }
               },*/

               itemName_veg: {required:function(){
                  return ( $('.isVegAvailable').prop('checked') && $( 'input[ name="add_veg" ]:checked' ).val() == 'new' );
                  }
               },
               serveName_veg: {required:function(){
                  return ( $('.isVegAvailable').prop('checked') && $( 'input[ name="add_veg" ]:checked' ).val() == 'new' );
                  }
               },
               calories_veg: {number: true,required:function(){
                  return ( $('.isVegAvailable').prop('checked') && $( 'input[ name="add_veg" ]:checked' ).val() == 'new' );
                  }
               },
               userType_veg: {required:function(){
                  return ( $('.isVegAvailable').prop('checked') && $( 'input[ name="add_veg" ]:checked' ).val() == 'new' );
                  }
               },
               itemIngredients_veg: {required:function(){
                  return ( $('.isVegAvailable').prop('checked') && $( 'input[ name="add_veg" ]:checked' ).val() == 'new' );
                  }
               },
               itemMethod_veg: {required:function(){
                  return ( $('.isVegAvailable').prop('checked') && $( 'input[ name="add_veg" ]:checked' ).val() == 'new' );
                  }
               },
               itemId: {required:function(){
                  return ( $('.isVegAvailable').prop('checked') && $( 'input[ name="add_veg" ]:checked' ).val() == 'existing' );
                  }
               }
               
              },
              messages: {
               food_type: "Select any one of food type",
           },
            errorPlacement: function(error, element) {
               $( element ).closest( '.form-group' ).append( error );
             },
              
             submitHandler: function(form) {
               tinymce.triggerSave();
                 form.submit();
             }
         
         });

      // Jquery validation for category add/edit
      $("#add_category_frm").validate({
         ignore: "",
         rules: {
            category_name: {required:true},
            
           },
           
          submitHandler: function(form) {
            tinymce.triggerSave();
              form.submit();
          }
      
      });

      $("#edit_category_frm").validate({
         ignore: "",
         rules: {
            category_name: {required:true},
            
           },
           
          submitHandler: function(form) {
            tinymce.triggerSave();
              form.submit();
          }
      
      });

      // Jquery validation for Partner add/edit
      $("#add_partner_frm").validate({
         ignore: "",
         rules: {
            partner_name: {required:true},
           },
           
          submitHandler: function(form) {
              form.submit();
          }
      
      });

      $("#edit_partner_frm").validate({
         ignore: "",
         rules: {
            partner_name: {required:true},
           },
           
          submitHandler: function(form) {
              form.submit();
          }
      
      });

      // Jquery validation for Shopping add/edit
      $("#add_shopping_frm").validate({
         ignore: "",
         rules: {
            weekNumber: {required:true},
            pdfFile: {required:true},
           },
           errorPlacement: function(error, element) {
            $( element ).closest( '.form-group' ).append( error );
          },
           
          submitHandler: function(form) {
              form.submit();
          }
      
      });

      $("#edit_shopping_frm").validate({
         ignore: "",
         rules: {
            weekNumber: {required:true},
           },
           errorPlacement: function(error, element) {
            $( element ).closest( '.form-group' ).append( error );
          },
           
          submitHandler: function(form) {
              form.submit();
          }
      
      });

      $('.add_more').click(function(e){
         e.preventDefault();
         var index_length = $(".repeated_video_link_section").length;
         var added_index = index_length  ;
         // alert(added_index);
         // $(".repeated_video_link_section:first").clone().insertBefore(".add_more_div");
         $('.add_more_div').before(`<div class="row repeated_video_link_section">
         <div class="col-md-6">
             <div class="form-group">
                 <label for="video_links">Video Link</label>
                 <input name="video_links[`+added_index+`][link]" type="text" class="form-control" id="video_links" placeholder="Video Link" value="">
             </div>
         </div>
         <div class="col-md-4">
             <div class="form-group">
                 <label for="video_links">Video Title</label>
                 <input name="video_links[`+added_index+`][title]" type="text" class="form-control" id="video_links" placeholder="Video Title" value="">
             </div>
         </div>
         <div class="col-md-2">
               <div class="form-group">
                  <a href="#" class="btn btn-primary remove_more" onclick="removeVideoLink($(this),event);"> <i class="fa fa-times" aria-hidden="true"></i> </a>
               </div>
         </div> 
     </div>`);
      })

      // $('.remove_more').click(function(e){
      //    e.preventDefault();
      //    alert('jhjh');
      //    $(this).closest(".repeated_video_link_section").remove();
         
      // })

});

// Function for delete the Record on Listing page using model box open
function updateStatus(id, route_url, value)
{  
   // console.log(thisObj );
   $.ajax({
      url: route_url,
      type: "POST",
      data: { id: id },
      dataType: "json",
      success: function (data) {
         if( data.status == 0 ) {  
            $( '.record_active_' + id ).prop( 'checked', false );
            $( '.record_active_' + id + '[value='+ ( value == 1?0:1 ) +']' ).prop( 'checked', true );
         }
         swal("", data.msg, ( ( data.status == 1 ) ? "success" : "error" ) ) ;
      }
   });
}

function removeVideoLink(current,e)
{  
   e.preventDefault();
   current.closest(".repeated_video_link_section").remove();
}