<!--  navbar.html -->
<nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
  <div class="navbar-brand-wrapper d-flex align-items-center">
    <a class="navbar-brand brand-logo" href="{{ route('admin.food_items') }}">
      <img src="{{ asset('images/ot_logo.png') }}" alt="logo" class="logo-dark" />
    </a>
    <a class="navbar-brand brand-logo-mini" href="{{ route('admin.food_items') }}"><img src="{{ asset('images/ot_logo.png') }}" alt="logo" /></a>
  </div>
  <div class="navbar-menu-wrapper d-flex align-items-center flex-grow-1">
    <h5 class="mb-0 font-weight-medium d-none d-lg-flex">Welcome! {{ session('name') }}</h5>
    <ul class="navbar-nav navbar-nav-right ml-auto">
        
      <li class="nav-item dropdown d-none d-xl-inline-flex user-dropdown">
        <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
          <!-- <img class="img-xs rounded-circle ml-2" src="{{ asset('images/faces/face8.jpg') }}" alt="Profile image">  -->
          <span class="font-weight-normal"> {{ session('name') }}</span></a>
        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
            
          
          <a class="dropdown-item" href="{{ route('admin.logout') }}"><i class="dropdown-item-icon icon-power text-primary"></i>Sign Out</a>
        </div>
      </li>
    </ul>
    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
      <span class="icon-menu"></span>
    </button>
  </div>
</nav>

    <!-- Sidebar menu-->

         
<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item {{ ( $current_route_name == 'admin.food_items' || $current_route_name == 'admin.category_food_items' || $current_route_name == 'admin.addfood' || $current_route_name == 'admin.editfood' || $current_route_name == 'admin.categorylist' || $current_route_name == 'admin.addcategory' || $current_route_name == 'admin.editcategory' ) ? 'active':'' }}">
      <a class="nav-link {{ ( $current_route_name == 'admin.food_items' || $current_route_name == 'admin.category_food_items' || $current_route_name == 'admin.addfood' || $current_route_name == 'admin.editfood' || $current_route_name == 'admin.categorylist' || $current_route_name == 'admin.addcategory' || $current_route_name == 'admin.editcategory' ) ? 'active':'' }}" data-toggle="collapse" href="#ui-basic" aria-expanded="true" aria-controls="ui-basic">
        <span class="menu-title">Food</span>
        <i class="fas fa-hamburger menu-icon"></i>
      </a>
      <div class="collapse {{ ( $current_route_name == 'admin.food_items' || $current_route_name == 'admin.category_food_items' || $current_route_name == 'admin.addfood' || $current_route_name == 'admin.editfood' || $current_route_name == 'admin.categorylist' || $current_route_name == 'admin.addcategory' || $current_route_name == 'admin.editcategory' ) ? 'show':'' }}" id="ui-basic">
        <ul class="nav flex-column sub-menu">

          <li class="nav-item"><a class="nav-link {{ ( $current_route_name == 'admin.food_items' || $current_route_name == 'admin.addfood' || $current_route_name == 'admin.editfood' ) ? 'active':'' }}" href="{{ route('admin.food_items') }}">Manage Food Items</a></li>
          
          <!--li class="nav-item" style="display:none;"><a class="nav-link" href="{{ route('admin.addfood') }}">Add Product</a></li>
          <li class="nav-item" style="display:none;"><a class="nav-link" href="{{ route('admin.editfood', 'ND') }}">Edit Product</a></li-->

          <li class="nav-item"><a class="nav-link {{ ( $current_route_name == 'admin.categorylist' || $current_route_name == 'admin.category_food_items' || $current_route_name == 'admin.addcategory' || $current_route_name == 'admin.editcategory' ) ? 'active':'' }}" href="{{ route('admin.categorylist') }}">Manage Categories</a></li>
          <!--li class="nav-item" style="display:none;"><a class="nav-link" href="{{ route('admin.addcategory') }}">Add Category</a></li-->
        </ul>
      </div>
    </li>

    <li class="nav-item {{ ( $current_route_name == 'admin.exercises' || $current_route_name == 'admin.add_exercise' || $current_route_name == 'admin.edit_exercise' || $current_route_name == 'admin.dailymealplanlist' || $current_route_name == 'admin.editdailymealplan' || $current_route_name == 'admin.leaders' || $current_route_name == 'admin.add_leader' || $current_route_name == 'admin.edit_leader' ) ? ' active':'' }}">
      <a class="nav-link {{ ( $current_route_name == 'admin.exercises' || $current_route_name == 'admin.add_exercise' || $current_route_name == 'admin.edit_exercise' || $current_route_name == 'admin.dailymealplanlist' || $current_route_name == 'admin.editdailymealplan' || $current_route_name == 'admin.leaders' || $current_route_name == 'admin.add_leader' || $current_route_name == 'admin.edit_leader' ) ? ' active':'' }}" data-toggle="collapse" href="#ui-98-days" aria-expanded="true" aria-controls="ui-98-days">
          <span class="menu-title">98 days Plan</span>
        <i class="icon-book-open menu-icon"></i>
      </a>

      <div class="collapse {{ ( $current_route_name == 'admin.exercises' || $current_route_name == 'admin.add_exercise' || $current_route_name == 'admin.edit_exercise' || $current_route_name == 'admin.dailymealplanlist' || $current_route_name == 'admin.editdailymealplan' || $current_route_name == 'admin.leaders' || $current_route_name == 'admin.add_leader' || $current_route_name == 'admin.edit_leader' ) ? ' show':'' }}" id="ui-98-days">
        <ul class="nav flex-column sub-menu">

          <li class="nav-item {{ ( $current_route_name == 'admin.leaders' || $current_route_name == 'admin.add_leader' || $current_route_name == 'admin.edit_leader' || $current_route_name == 'admin.exercises' || $current_route_name == 'admin.add_exercise' || $current_route_name == 'admin.edit_exercise' ) ? ' active':'' }}">
            <a class="nav-link {{ ( $current_route_name == 'admin.leaders' || $current_route_name == 'admin.add_leader' || $current_route_name == 'admin.edit_leader' || $current_route_name == 'admin.exercises' || $current_route_name == 'admin.add_exercise' || $current_route_name == 'admin.edit_exercise' ) ? ' active':'' }}" href="{{ route('admin.leaders') }}">
              <span class="menu-title">Leaders</span>
            </a>
          </li>

          <li class="nav-item {{ ( $current_route_name == 'admin.dailymealplanlist' || $current_route_name == 'admin.editdailymealplan' ) ? ' active':'' }}">
            <a class="nav-link {{ ( $current_route_name == 'admin.dailymealplanlist' || $current_route_name == 'admin.editdailymealplan' ) ? ' active':'' }}" href="{{ route('admin.dailymealplanlist') }}">
              <span class="menu-title">Meal plans</span>
            </a>
          </li>

        </ul>
      </div>

    </li>
    
    
    <li class="nav-item {{ ( $current_route_name == 'admin.partnerlist' || $current_route_name == 'admin.addpartner' || $current_route_name == 'admin.editpartner' ) ? 'active':'' }}">
      <a class="nav-link {{ ( $current_route_name == 'admin.partnerlist' || $current_route_name == 'admin.addpartner' || $current_route_name == 'admin.editpartner' ) ? 'active':'' }}" href="{{ route('admin.partnerlist') }}">
        <span class="menu-title">Partner Management</span>
        <i class="icon-chart menu-icon"></i>
      </a>
    </li>      
    <li class="nav-item {{ ( $current_route_name == 'admin.shoppinglist' || $current_route_name == 'admin.addshopping' || $current_route_name == 'admin.editshopping' ) ? ' active':'' }}">
      <a class="nav-link {{ ( $current_route_name == 'admin.shoppinglist' || $current_route_name == 'admin.addshopping' || $current_route_name == 'admin.editshopping' ) ? ' active':'' }}" href="{{ route('admin.shoppinglist') }}">
        <span class="menu-title">Shopping Lists</span>
        <i class="icon-doc menu-icon"></i>
      </a>
    </li>
  </ul>
</nav>


 