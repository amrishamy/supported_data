<footer class="footer">
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © <?php echo date('Y'); ?> <a href="javascript:;" >OT</a>. All rights reserved. </span>              
    </div>
</footer>

<!-- Delete Confirmation Modal -->
	
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
    <h4 class="modal-title" id="exampleModalLabel">DELETE</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    </div>
    <div class="modal-body">
        Are you sure ? You will not be able to recover this data !
    </div>
    <div class="modal-footer">
      <a href="#" class="model_action_confirm btn btn-secondary"> YES </a>
      <button type="button" class="btn btn-primary" data-dismiss="modal">NO</button>
    </div>
  </div>
  </div>
</div>