<!-- Essential javascripts for application to work-->

	<script src="{{ asset('vendors/js/vendor.bundle.base.js') }}"></script> <!-- having jquery,bootstrap, theme -->

	<!-- Plugin js for this page -->
    <script src="{{ asset('vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('vendors/moment/moment.min.js') }}"></script>
    <script src="{{ asset('vendors/daterangepicker/daterangepicker.js') }}"></script>
	<script src="{{ asset('vendors/chartist/chartist.min.js') }}"></script>
	<!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('js/off-canvas.js') }}"></script>
    <script src="{{ asset('js/misc.js') }}"></script>
	<!-- endinject -->
	
	<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> 
	<script src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script> 
	
    <!-- Custom js for this page -->
	<script src="{{ asset('js/dashboard.js') }}"></script>
	<script src="{{ asset('js/jquery.validate.min.js') }}"></script>

	<script src="{{ asset('js/script.js') }}"></script>

	<script src="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.js"></script>
	<link rel="stylesheet" href="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.css">
<!-- <script src="{{asset('js/main.js')}}"></script> -->

<!-- <script src="https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js"></script> -->
@stack('after_scripts')
