

<div class="page-header">
  <h3 class="page-title"> {{ $breadcrumbTitle }}</h3>
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
      <li class="breadcrumb-item"><a href="{{ $breadcrumbItemLink }}">{{  $breadcrumbItem }}</a></li>
      <li class="breadcrumb-item active" aria-current="page">{{ $breadcrumbTitle }} </li>
    </ol>
  </nav>
</div>
@include('includes.flash-message')