@extends('layouts.admin')
@section('content')

<div class="main-panel">
  <div class="content-wrapper">
    @include('includes.adminbreadcrumb')
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">	
                    <div class="row align-items-center">
                        <div class="col-md-5 text-left">
                            <h4 class="mb-0">Shopping List</h4>
                        </div>
                        <div class="col-md-7 text-right">
                            <a href="{{ route('admin.addshopping') }}" class="btn btn-md btn-primary "><i class="fa fa-plus mr-1"></i> Add Shopping List Item</a>
                        </div>
                    </div>
                    <hr/>
                    <div class="table-responsive">
                        <table class="table table-bordered tbl-reorder" id="datatable" style="width:100%;">
                            <thead class="thead-light">
                            <tr>
                            <th width="80"> S.No. </th>
                            <th width="140">Week Number </th>
                            <th>PDF Link </th>	
                            <th>PDF Link for veg </th>	
                            <th>Active</th>								   
                            <th width="120">Action </th>
                            </tr>
                            </thead>
                            <tbody>
                                
                                
                            </tbody>
                        </table> 
                    </div>
                
                </div>
            </div>
        </div>
    </div> 
</div>
  <!-- content-wrapper ends -->

  <!-- footer Start -->
  @include('includes.admincopyrightfooter')
  <!-- partial -->
        @push('after_scripts')
            <script type="text/javascript">
                list({ 
                listing_url: '{{ route( 'admin.shopping_list' ) }}', //for listing
                orderable: { orderable: false, targets: [ 2,3,4 ] }, // for sorting
                /* Row Reorder [ */
                reorder: true, 
                reorder_url: '{{ route( 'admin.updateShoppingItemReorder' ) }}', 
                idIndex: 6, // primary auto inc id
                orderIndex: 7 // sort order index
                /* ] */
                });
            </script>
        @endpush
</div> <!-- main-panel ends -->

 <!-- /.content-wrapper -->
@endsection
