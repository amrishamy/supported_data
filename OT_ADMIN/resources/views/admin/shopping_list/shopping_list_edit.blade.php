@extends('layouts.admin')
@section('content')

<div class="main-panel">
  <div class="content-wrapper">
    @include('includes.adminbreadcrumb')
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">	
                    <form method="POST" enctype="multipart/form-data" class="exercise-list" action="{{ route('admin.updateshopping') }}" id="edit_shopping_frm" >
                        {{ csrf_field() }}
                        <input type="hidden" name="shoppingid" value="{{$shopping_detail->id}}" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputName1">Week Number</label>
                                    <input type="text" class="form-control" value="{{ $shopping_detail->weekNumber + 1 }}" placeholder="Week Number" disabled />
                                </div>
                            </div> 								  
                        </div>
                    
                        <div class="row">
                            <div class="col-md-6"> 								 
                                <div class="form-group">
                                <label>PDF Link</label>
                                <input type="file" name="pdfFile" class="file-upload-default">
                                <div class="input-group">
                                    <input type="text" class="form-control file-upload-info" disabled="" value="{{ $shopping_detail->pdfFile }}" placeholder="Upload Pdf">
                                    <span class="input-group-append">
                                    <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                                </div>
                            </div> 							
                        </div>

                        <div class="row">
                            <div class="col-md-6"> 								 
                                <div class="form-group">
                                <label>PDF Link for veg</label>
                                <input type="file" name="pdfFile_veg" class="file-upload-default">
                                <div class="input-group">
                                    <input type="text" class="form-control file-upload-info" disabled="" value="{{ $shopping_detail->pdfFile_veg }}" placeholder="Upload Pdf">
                                    <span class="input-group-append">
                                    <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                                </div>
                            </div> 							
                        </div>
                            
                        <div class="row">
                            <div class="col-md-6"> 
                                <input type="submit" class="btn btn-primary mr-2" value="Update"> 
                                <a href="{{ route('admin.shoppinglist') }}" class="btn btn-light">Cancel</a>
                            </div>  
                        </div>	
                    </form>
                
                </div>
            </div>
        </div>
    </div> 
</div>
  <!-- content-wrapper ends -->

  <!-- footer Start -->
  @include('includes.admincopyrightfooter')
  <!-- partial -->
</div> <!-- main-panel ends -->

 <!-- /.content-wrapper -->
@endsection
