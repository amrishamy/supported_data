@extends('layouts.admin')
@section('content')
	
<div class="main-panel">
  <div class="content-wrapper">
  @include('includes.adminbreadcrumb')
    Main page content here
  </div>
  <!-- content-wrapper ends -->

  <!-- footer Start -->
  @include('includes.admincopyrightfooter')
  <!-- partial -->
</div>

 <!-- /.content-wrapper -->
@endsection
