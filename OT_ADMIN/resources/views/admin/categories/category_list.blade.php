@extends('layouts.admin')
@section('content')

<div class="main-panel">
  <div class="content-wrapper">
    @include('includes.adminbreadcrumb')
    <div class="row">
      <div class="col-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">	
              <div class="row align-items-center">

                <div class="col-md-5 text-left">
                  <h4 class="mb-0">Category List</h4>
                </div>
                <div class="col-md-7 text-right">
                  <a href="{{ route('admin.addcategory') }}" class="btn btn-md btn-primary "><i class="fa fa-plus mr-1"></i> Add Category</a>
                </div>
              </div>
              <hr/>
              <div class="table-responsive">
                <table class="table table-bordered tbl-reorder" id="datatable">
                  <thead class="thead-light">
                    <tr>
                      <th width="100"> S.No. </th>
                      <th> Category Name </th>
                      <th width="140"> Thumbnail </th>
                      <th>Active</th>
                      <th width="120"> Action </th>
                    </tr>
                  </thead>

                  </tbody>
                </table>
                  
              </div>
            </div>
          </div>
      </div>
    </div> 
  </div>
  <!-- content-wrapper ends -->

  <!-- footer Start -->
  @include('includes.admincopyrightfooter')
  <!-- partial -->
    @push('after_scripts')
        <script type="text/javascript">
          list({ 
            listing_url: '{{ route( 'admin.catgories_list' ) }}', //for listing
            orderable: { orderable: false, targets: [ 2,3,4 ] }, // for sorting
            /* Row Reorder [ */
            reorder: true, 
            reorder_url: '{{ route( 'admin.updateCategoryItemReorder' ) }}', 
            idIndex: 5,
            orderIndex: 6
            /* ] */
          });
      </script>
    @endpush
</div> <!-- main-panel ends -->

 <!-- /.content-wrapper -->
@endsection
