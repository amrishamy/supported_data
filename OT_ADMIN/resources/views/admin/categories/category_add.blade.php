@extends('layouts.admin')
@section('content')
	
<div class="main-panel">
  <div class="content-wrapper">
    @include('includes.adminbreadcrumb')
    
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">	
                    <form method="POST" enctype="multipart/form-data" class="exercise-list" id="add_category_frm" action="{{ route('admin.savecategory') }}" >
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="exampleInputName1">Category Name</label>
                                <input name="category_name" type="text" class="form-control" id="exampleInputName1" placeholder="Category Name">
                                <!-- @if ($errors->has('category_name'))
                                    <div class="error">{{ $errors->first('category_name') }}</div>
                                @endif -->
                                </div>
                            </div> 								  
                        </div>
                            
                        <div class="row">
                            <div class="col-md-6"> 								 
                                <div class="form-group">
                                <label>Category Image</label>
                                <input type="file" name="category_image" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                                    <span class="input-group-append">
                                    <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                                </div>
                            </div> 			 
                                                            
                        </div> 
                                        
                        <div class="row">
                            <div class="col-md-6"> 
                                <input type="submit" class="btn btn-primary mr-2" value="Add Category"> 
                                <a href="{{ route('admin.categorylist') }}" class="btn btn-light">Cancel</a>
                            </div>  
                        </div>	
                    </form>
                    
                </div>
            </div>
        </div>
    </div> 

  </div>
  <!-- content-wrapper ends -->

  <!-- footer Start -->
  @include('includes.admincopyrightfooter')
  <!-- partial -->
</div> <!-- main-panel ends -->

 <!-- /.content-wrapper -->
@endsection
