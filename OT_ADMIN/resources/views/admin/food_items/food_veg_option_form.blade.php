
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
            <h4 class="card-title optional_veg_item_form">Optional Veg food item</h4>
            </div>
        </div> 
        	 
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
            <label for="itemName_veg">Food Item Name</label>
            <input name="itemName_veg" type="text" class="form-control" id="itemName_veg" placeholder="Ex:-Porridge Bread,Vegetable Fried Rice etc.">
            </div>
        </div> 
        	 
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
            <label for="serveName_veg">Serve Details</label>
            
            <textarea name="serveName_veg" class="form-control"  placeholder="Serve Details" ></textarea>
            </div>
        </div> 

        <div class="col-md-6"> 								 
            <div class="form-group">
            <label>Upload Product Image</label>
            <input name="itemImage_veg" type="file" class="file-upload-default">
            <div class="input-group col-xs-12">
                <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                <span class="input-group-append">
                <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                </span>
            </div>
            </div>
        </div>

                
    </div>

    <div class="row">

        <div class="col-md-6">
            <div class="form-group">
            <label for="instructionalVideo_veg">Video URL</label>
            <input name="instructionalVideo_veg" type="text" class="form-control" id="instructionalVideo_veg" placeholder="Video URL">
            </div>	
        </div>
        <div class="col-md-6">
            <div class="form-group">
            <label for="prepareAhead_veg">Prepare Ahead</label>
            <input name="prepareAhead_veg" type="text" class="form-control" id="prepareAhead_veg" placeholder="Prepare Ahead">
            </div>
        </div> 
                
    </div>
    
    <div class="row">
        
        <div class="col-md-6"> 
            <div class="form-group">
            <label for="calories_veg">Calories</label>
            <input name="calories_veg" type="text" class="form-control" id="calories_veg" placeholder="Instructional Video">
            </div>
        </div> 
        <div class="col-md-6"> 
            <div class="form-group">
            <label for="userType">User Type</label>
            <select class="form-control" name="userType_veg" id="userType_veg">
                    
                        <option value="0">Both</option>
                        <option value="1">Men's</option>
                        <option value="2">Women's</option>
                    
                </select>
            </div>
        </div>                            
    </div>	
    
    <div class="row">								
        <div class="col-md-12">
            <div class="form-group">
                <label>Ingredients</label>
                <textarea name="itemIngredients_veg" class="tinymceeditor"></textarea>
            </div>	
        </div>
    </div>

    <div class="row">								
        <div class="col-md-12">
            <div class="form-group">
                <label>Method</label>
                <textarea name="itemMethod_veg" class="tinymceeditor"></textarea>
            </div>	
        </div>
    </div>

    <div class="row">								
        <div class="col-md-12">
            <div class="form-group">
                <label>Additional Info</label>
                <textarea name="additionalInfo_veg" class="tinymceeditor"></textarea>
            </div>	
        </div>
    </div>
    