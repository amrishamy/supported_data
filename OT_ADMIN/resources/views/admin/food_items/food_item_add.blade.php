@extends('layouts.admin')

@push('after_scripts')
    <script type="text/javascript">
        var fiObj = {
            selected_fi: 0,
            get_food_items: '{{ route( 'admin.get_food_items' ) }}'
        };
    </script>
    <script src="{{ asset('js/pages/fodd-items.js') }}"></script>
@endpush

@section('content')
	
<div class="main-panel">
  <div class="content-wrapper">
    @include('includes.adminbreadcrumb')

    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                <form method="POST" enctype="multipart/form-data" class="exercise-list" action="{{ route('admin.savefood') }}" id="add_food_item_frm" >
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                  <label for="itemName">Food Item Name</label>
                                <input name="itemName" type="text" class="form-control" id="itemName" placeholder="Ex:-Porridge Bread,Vegetable Fried Rice etc." value="{{ old('itemName') }}">
                            </div>
                        </div> 
                        <div class="col-md-6"> 								 
                            <div class="form-group">
                            <label>Upload Product Image</label>
                            <input name="itemImage" type="file" class="file-upload-default">
                            <div class="input-group col-xs-12">
                                <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                                <span class="input-group-append">
                                <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                </span>
                            </div>
                            </div>
                        </div>                        
                    </div>

                    <div class='row'>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="food_category">Select Category</label>
                                <select multiple class="form-control" name="food_category[]" id="food_category">
                                    @foreach($all_categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->categoryName }}</option>
                                    @endforeach
                                </select>
                            </div>	
                        </div>	
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="serveName">Serve Details</label>
                            
                            <textarea name="serveName" class="form-control"  placeholder="Serve Details" rows="4">{{ old('serveName') }}</textarea>
                            </div>
                        </div> 
                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="instructionalVideo">Video URL</label>
                            <input name="instructionalVideo" type="text" class="form-control" id="instructionalVideo" placeholder="Video URL">
                            </div>	
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="prepareAhead">Prepare Ahead</label>
                            <input name="prepareAhead" type="text" class="form-control" id="prepareAhead" placeholder="Prepare Ahead">
                            </div>
                        </div> 
                        	 
                    </div>
                    
                    <div class="row">
                        
                        <div class="col-md-6"> 
                            <div class="form-group">
                            <label for="calories">Calories</label>
                            <input name="calories" type="text" class="form-control" id="calories" placeholder="Calories">
                            </div>
                        </div>  

                        <div class="col-md-6"> 
                            <div class="form-group">
                            <label for="userType">User Type</label>
                            <select class="form-control" name="userType" id="userType">
                                    
                                        <option value="0">Both</option>
                                        <option value="1">Men's</option>
                                        <option value="2">Women's</option>
                                    
                                </select>
                            </div>
                        </div>
                                                   
                    </div>	
                    
                    <div class="row">								
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Ingredients</label>
                                <textarea name="itemIngredients" id="itemIngredients" class="tinymceeditor"></textarea>
                            </div>	
                        </div>
                    </div>

                    <div class="row">								
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Method</label>
                                <textarea name="itemMethod" class="tinymceeditor"></textarea>
                            </div>	
                        </div>
                    </div>

                    <div class="row">								
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Additional Info</label>
                                <textarea name="additionalInfo" class="tinymceeditor"></textarea>
                            </div>	
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6"> 
                            <div class="form-group isShutDownWeek">
                                <div class="form-check">
                                <label class="form-check-label">
                                    <input name="isShutDownWeek" type="checkbox" class="form-check-input isShutDownWeek" > Is Shut Down Week ? </label>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row align-items-center">
                            <label class="col-sm-3 col-form-label">Food Type</label>
                            <div class="col-sm-4">
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input food_type" name="food_type" id="membershipRadios1" value="veg" > Veg </label>
                              </div>
                            </div>
                            <div class="col-sm-5">
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input food_type" name="food_type" id="membershipRadios2" value="nonveg"> Non Veg </label>
                              </div>
                            </div>
                            {!! $errors->first('food_type', '<p class="validation-errors">:message</p>') !!}
                          </div>
                        </div>

                        <div class="col-md-6"> 
                            <div class="form-group is_veg_available" style="display:none;">
                                <div class="form-check">
                                <label class="form-check-label">
                                    <input name="isVegAvailable" type="checkbox" class="form-check-input isVegAvailable" > Is the same food item available in veg ? </label>
                                </div>

                            <!-- <label for="exampleInputEmail3">Is Veg Available ?</label>
                            <input type="checkbox" class="form-check-input" checked> -->
                            <!-- <select name="isVegAvailable" class="form-control isVegAvailable">
                                <option value="">---Select---</option>
                                <option value="yes">Yes</option>
                                <option value="no">No </option>
                            </select> -->
                            </div>
                        </div>                             
                    </div>

                    <div class='row'>
                        <div class="col-md-6 add_veg_section"  style="display:none;">
                          <div class="form-group row align-items-center">
                            <label class="col-sm-3 col-form-label">Add Veg</label>
                            <div class="col-sm-4">
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input add_veg" name="add_veg" id="add_veg1" value="existing" > Existing </label>
                              </div>
                            </div>
                            <div class="col-sm-5">
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input add_veg" name="add_veg" id="add_veg2" value="new"> New </label>
                              </div>
                            </div>
                          </div>
                        </div>

                         
                    </div>

                    <span class="optional_veg_item" style="display:none;"> 
                        <span id="existing-wrap" style="display:none;">
                            <div class="row">
                                <div class="col-md-6"> 								 
                                    <div class="form-group">
                                    <label>Select Existing Veg items</label>
                                    <div class="input-group">
                                        <div class="form-control">
                                            <input type="hidden" name="itemId" id="itemId" value="" />
                                            <span class="badge badge-info" id="fi-title">
                                            </span> 
                                        </div>
                                        <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-primary" data-toggle="modal" data-target="#show-exist-veg-items" type="button"><i class="menu-icon icon-list"></i></button>
                                        </span>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </span>
                        <span id="new-item-wrap" style="display:none;">
                            @include('admin.food_items.food_veg_option_form') 
                        </span>
                    </span>
                                            
                    <div class="row">
                        <div class="col-md-6"> 
                            <input type="submit" class="btn btn-primary mr-2" value="Add Food"> 
                            <a href="{{ route('admin.food_items') }}" class="btn btn-light">Cancel</a>
                        </div>  
                    </div>	
                    
                </form>
                </div>
            </div>
        </div>
    </div> 
   
    
  </div>
  <!-- content-wrapper ends -->

  <!-- footer Start -->
  @include('includes.admincopyrightfooter')
  <!-- partial -->
    @push( 'after_scripts' )
        <!--script type="text/javascript">
          var $allRecipesCatId = "{{ $comboCatId?$comboCatId:0 }}";
          (function( $ ) { 
            $( document ).ready(function( ) { 
                $('#food_category').change(function( ) { 
                    if( $allRecipesCatId && $.inArray( $allRecipesCatId, $( this ).val( ) ) > -1 ) { 
                        $(this).find( 'option' ).prop('selected', true);
                    }
                });
            });
          })(  jQuery );
        </script-->
        <script src="https://cdn.tiny.cloud/1/qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc/tinymce/5/tinymce.min.js"></script>
        <script type="text/javascript">

            // tinymce editor
            tinymce.init({
                selector: 'textarea.tinymceeditor',
                height: 300,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste code help wordcount'
                ],
                toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tiny.cloud/css/codepen.min.css'
                ],
                onchange_callback: function(editor) {
                    tinymce.triggerSave();
                    $(".tinymceeditor").valid();
                }

            });
        </script>
    @endpush
</div> <!-- main-panel ends -->

<div class="modal fade" id="show-exist-veg-items">
  <div class="modal-dialog modal-lg modal-dialog-scrollable">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Select Veg Item</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <div class="fi-srch-wrap content m-2">
         <div class="row">
            <div class="col-md-12">
                <input type="text" class="form-control" placeholder="Type here to search food items" id="fi-srch">
            </div>
         </div>
    </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="check_select">
            <div class="container">
                <div id="fi-list-wrap" class="row">
               
                </div>
            </div>
            <div class="text-center hide" id="fi-loader">
                <img src="{{ asset('images/loader.gif') }}" width="50px" />
            </div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <a href="#" class="btn btn-primary select_meal_plan_day_btn" >Select</a>
      </div>

    </div>
  </div>
</div>

 <!-- /.content-wrapper -->
@endsection