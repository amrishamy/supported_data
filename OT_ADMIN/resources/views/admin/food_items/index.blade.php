@extends('layouts.admin')

@section('content')
	
<div class="main-panel">
  <div class="content-wrapper">
    @include('includes.adminbreadcrumb')

    <div class="row">
		<div class="col-12 grid-margin stretch-card">
			<div class="card">
				<div class="card-body">	
					<div class="row align-items-center">
						<div class="col-md-5 text-left">
							@if( $category )
							<h4 class="mb-0">Category: {{$category->categoryName}}</h4>
							<h4 class="mb-0">Food Items List</h4>
							@else
							<h4 class="mb-0">Food Items List</h4>
							@endif
						</div>
						@if( !$category )
						<div class="col-md-7 text-right">
							<a href="{{ route('admin.addfood') }}" class="btn btn-md btn-primary"><i class="fa fa-plus mr-1"></i> Add Item</a>
						</div>
						@endif
					</div>
					<hr/>
					<div class="table-responsive" id="listing">
						<table class="table table-bordered display @if( $category ) tbl-reorder @endif" id="datatable" style="width:100%;">
							<thead class="thead-light">
								<tr>
									<th width="40">S.No. </th>
									<th>Name</th>
									<th>Food Image </th>
									@if( !$category )
										<th>Category </th>
									@endif
									<th>Calorie</th>
									<th>Active</th>
									<th>Action </th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div> 
    
  </div>
  <!-- content-wrapper ends -->

	<style>
		table#datatable {
			table-layout:fixed;
		}
		table#datatable td:nth-child(2), table#datatable td:nth-child(4) {
			white-space: normal;
			word-break: break-word;
		}
		table#datatable th:nth-child(2),
		table#datatable th:nth-child(4) {
			width: 150px!important;			
		}
	</style>

  <!-- footer Start -->
  @include('includes.admincopyrightfooter')
  <!-- partial -->

	@push('after_scripts')
	  	<script type="text/javascript">
	  		list({ 
	  			listing_url: '{{ route( 'admin.food_items_list', ( $category )?Hashids::encode( $category->id ):0 ) }}', 
	  			@if( $category )
		  			orderable: { orderable: false, targets: [ 2,3,4,5 ] },
		  			reorder: true, 
		  			/*reorder_url: '{{ route( 'admin.updateFoodItemReorder' ) }}', */
		  			reorder_url: '{{ route( 'admin.updateCategoryWiseFoodItemReorder', Hashids::encode( $category->id ) ) }}', 
		  			idIndex: 6,
		  			orderIndex: 7
		  		@else
		  			orderable: { orderable: false, targets: [ 2,3,4,5,6 ] },
	  			@endif
  			});
		</script>
	@endpush
</div> <!-- main-panel ends -->

 <!-- /.content-wrapper -->
@endsection