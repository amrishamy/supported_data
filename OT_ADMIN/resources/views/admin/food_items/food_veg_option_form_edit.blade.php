
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
            <h4 class="card-title optional_veg_item_form">Optional Veg food item</h4>
            </div>
        </div> 
        	 
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
            <label for="itemName_veg">Food Name</label>
            <input name="itemName_veg" type="text" class="form-control" id="itemName_veg" placeholder="Food Name" value="{{ @$food_detail_veg->itemName }}">
            </div>
        </div> 
        	 
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
            <label for="serveName_veg">Serve Name</label>
            
            <textarea name="serveName_veg" class="form-control"  placeholder="Serve Name" >{{ @$food_detail_veg->serveName }}</textarea>
            </div>
        </div> 

        <div class="col-md-{{ @$food_detail_veg->itemImage?4:6 }}"> 								 
            <div class="form-group">
            <label>Upload Product Image</label>
            <input name="itemImage_veg" type="file" class="file-upload-default">
            <div class="input-group col-xs-12">
                <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                <span class="input-group-append">
                <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                </span>
            </div>
            </div>
        </div>

        @if( @$food_detail_veg->itemImage )
        <div class="col-md-2">                               
            <div class="form-group">
             <span class="img-wrap">
                <img src="{{ env('AWS_S3_BUCKET_URL').'/'.env('AWS_S3_FOOD_ITEMIMAGE_BASEURL').'/'.$food_detail_veg->itemImage . '?' . time() }}" alt="" width="100%" />
                <a href="#" onclick="return deleteImage( '{{route( 'admin.delete_food_item_image', Hashids::encode( $food_detail_veg->id ) )}}', this );" title="Click here to delete image"><i class="fa fa-times" aria-hidden="true"></i></a>
             </span>
            </div>
        </div>
        @endif
                
    </div>

    <div class="row">

        <div class="col-md-6">
            <div class="form-group">
            <label for="instructionalVideo_veg">Video URL</label>
            <input name="instructionalVideo_veg" type="text" class="form-control" id="instructionalVideo_veg" placeholder="Video URL" value="{{ @$food_detail_veg->instructionalVideo }}">
            </div>	
        </div>
        <div class="col-md-6">
            <div class="form-group">
            <label for="prepareAhead_veg">Prepare Ahead</label>
            <input name="prepareAhead_veg" type="text" class="form-control" id="prepareAhead_veg" placeholder="Prepare Ahead" value="{{ @$food_detail_veg->prepareAhead }}" >
            </div>
        </div> 
                
    </div>
    
    <div class="row">
        
        <div class="col-md-6"> 
            <div class="form-group">
            <label for="calories_veg">Calories</label>
            <input name="calories_veg" type="text" class="form-control" id="calories_veg" placeholder="Calories" value="{{ @$food_detail_veg->calories }}">
            </div>
        </div>                             
    </div>	
    
    <div class="row">								
        <div class="col-md-12">
            <div class="form-group">
                <label>Ingredients</label>
                <textarea name="itemIngredients_veg" class="tinymceeditor"> {{ @$food_detail_veg->itemIngredients }} </textarea>
            </div>	
        </div>
    </div>

    <div class="row">								
        <div class="col-md-12">
            <div class="form-group">
                <label>Method</label>
                <textarea name="itemMethod_veg" class="tinymceeditor"> {{ @$food_detail_veg->itemMethod }} </textarea>
            </div>	
        </div>
    </div>

    <div class="row">								
        <div class="col-md-12">
            <div class="form-group">
                <label>Additional Info</label>
                <textarea name="additionalInfo_veg" class="tinymceeditor"> {{ @$food_detail_veg->additionalInfo }} </textarea>
            </div>	
        </div>
    </div>
    