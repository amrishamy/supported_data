@extends('layouts.admin')

@php 
    $food_detail_veg = ( !empty($food_detail->vegItem) )?$food_detail->vegItem:false;
@endphp
@push('after_scripts')
    <script type="text/javascript">
        var fiObj = {
            selected_fi: {{$food_detail_veg && $food_detail_veg->id?$food_detail_veg->id:0}},
            get_food_items: '{{ route( 'admin.get_food_items' ) }}'
        };
    </script>
    <script src="{{ asset('js/pages/fodd-items.js') }}"></script>
@endpush

@section('content')
	
<div class="main-panel">
  <div class="content-wrapper">
    @include('includes.adminbreadcrumb')

    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                <form method="POST" enctype="multipart/form-data" class="exercise-list" action="{{ route('admin.updatefood') }}" id="edit_food_item_frm" >
                    {{ csrf_field() }}
                    <input type="hidden" name="foodid" value="{{$food_detail->id}}" />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="itemName">Food Item Name</label>
                            <input name="itemName" type="text" class="form-control" id="itemName" placeholder="Ex:-Porridge Bread,Vegetable Fried Rice etc." value="{{ $food_detail->itemName }}">
                            </div>
                        </div> 

                        <div class="col-md-{{ $food_detail->itemImage?4:6 }}"> 								 
                            <div class="form-group">
                            <label>Upload Product Image</label>
                            <input name="itemImage" type="file" class="file-upload-default">
                            <div class="input-group col-xs-12">
                                <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                                <span class="input-group-append">
                                <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                </span>
                            </div>
                            </div>
                        </div>

                        @if( $food_detail->itemImage )
                        <div class="col-md-2">                               
                            <div class="form-group">
                             <span class="img-wrap">
                                <img src="{{ env('AWS_S3_BUCKET_URL').'/'.env('AWS_S3_FOOD_ITEMIMAGE_BASEURL').'/'.$food_detail->itemImage . '?' . time() }}" alt="" width="100%" />
                                <a href="#" onclick="return deleteImage( '{{route( 'admin.delete_food_item_image', Hashids::encode( $food_detail->id ) )}}', this );" title="Click here to delete image"><i class="fa fa-times" aria-hidden="true"></i></a>
                             </span>
                            </div>
                        </div>
                        @endif

                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="food_category">Select Category</label>
                            <select multiple class="form-control" name="food_category[]" id="food_category">
                                    @foreach($all_categories as $category)
                                        @if( in_array( $category->id , $item_selected_categories) )
                                        <option value="{{ $category->id }}" selected>{{ $category->categoryName }}</option>
                                        @else
                                        <option value="{{ $category->id }}">{{ $category->categoryName }}</option>
                                        @endif
                                    @endforeach
                            </select>
                            </div>	
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="serveName">Serve Details</label>
                            
                            <textarea name="serveName" class="form-control"  placeholder="Serve Details" rows="4">{{ $food_detail->serveName }}</textarea>
                            </div>
                        </div> 

                        

                        	 
                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="instructionalVideo">Video URL</label>
                            <input name="instructionalVideo" type="text" class="form-control" id="instructionalVideo" placeholder="Video URL" value="{{ $food_detail->instructionalVideo }}">
                            </div>	
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label for="prepareAhead">Prepare Ahead</label>
                            <input name="prepareAhead" type="text" class="form-control" id="prepareAhead" placeholder="Prepare Ahead" value="{{ $food_detail->prepareAhead }}">
                            </div>
                        </div> 
                        	 
                    </div>
                    
                    <div class="row">
                        
                        <div class="col-md-6"> 
                            <div class="form-group">
                            <label for="calories">Calories</label>
                            <input name="calories" type="text" class="form-control" id="calories" placeholder="Calories" value="{{ $food_detail->calories }}">
                            </div>
                        </div> 

                        <div class="col-md-6"> 
                            <div class="form-group">
                                <label for="userType">User Type</label>
                                <select class="form-control" name="userType" id="userType">
                                    
                                <option value="0"  @if($food_detail->userType == 0 ) selected @endif >Both</option>
                                <option value="1" @if($food_detail->userType == 1 ) selected @endif >Men's</option>
                                <option value="2" @if($food_detail->userType == 2 ) selected @endif >Women's</option>
                                    
                                </select>
                            </div>
                        </div>

                    </div>	
                    
                    <div class="row">								
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Ingredients</label>
                                <textarea name="itemIngredients" id="itemIngredients" class="tinymceeditor"> {{ $food_detail->itemIngredients }} </textarea>
                            </div>	
                        </div>
                    </div>

                    <div class="row">								
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Method</label>
                                <textarea name="itemMethod" class="tinymceeditor"> {{ $food_detail->itemMethod }} </textarea>
                            </div>	
                        </div>
                    </div>

                    <div class="row">								
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Additional Info</label>
                                <textarea name="additionalInfo" class="tinymceeditor"> {{ $food_detail->additionalInfo }} </textarea>
                            </div>	
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6"> 
                            <div class="form-group isShutDownWeek">
                                <div class="form-check">
                                <label class="form-check-label">
                                    <input name="isShutDownWeek" type="checkbox" class="form-check-input isShutDownWeek" {{ $food_detail->isShutDownWeek ? 'checked' : '' }} > Is Shut Down Week ? </label>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                          <div class="form-group row align-items-center">
                            <label class="col-sm-3 col-form-label">Food Type</label>
                            <div class="col-sm-4">
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input food_type" name="food_type" id="membershipRadios1" value="veg" @if ($food_detail->food_type)  checked @endif > Veg </label>
                              </div>
                            </div>
                            <div class="col-sm-5">
                              <div class="form-check">
                                <label class="form-check-label">
                                  <input type="radio" class="form-check-input food_type" name="food_type" id="membershipRadios2" value="nonveg" @if (!$food_detail->food_type)  checked @endif @if ($isAssociatedVegItem)  disabled @endif> Non Veg </label>
                              </div>
                            </div>
                            {!! $errors->first('food_type', '<p class="validation-errors">:message</p>') !!}
                          </div>
                        </div>

                        @if( !$isAssociatedVegItem )
                            <div class="col-md-6"> 
                                <div class="form-group is_veg_available" style="display:none;">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                        <input name="isVegAvailable" type="checkbox" class="form-check-input isVegAvailable" {{ $food_detail->isVegAvailable ? 'checked' : '' }} > Is Veg Available ? </label>
                                    </div>
                                    <!-- <label for="exampleInputEmail3">Is Veg Available ?</label>
                                    <select name="isVegAvailable" class="form-control isVegAvailable">
                                        <option value="">---Select---</option>
                                        <option value="yes">Yes</option>
                                        <option value="no">No </option>
                                    </select> -->
                                </div>
                            </div>
                        @endif
                    </div>

                    @if( !$isAssociatedVegItem )
                        <div class='row'>
                            <div class="col-md-6 add_veg_section"  style="display:none;">
                                <div class="form-group row align-items-center">
                                    <label class="col-sm-3 col-form-label">Add Veg</label>
                                    <div class="col-sm-4">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input add_veg" name="add_veg" id="add_veg1" value="existing" {{ $food_detail->isVegAvailable ? 'checked' : '' }} /> Existing </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input add_veg" name="add_veg" id="add_veg2" value="new"> New </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <span class="optional_veg_item" @if( !$food_detail_veg ) style="display:none;" @endif> 
                            <span id="existing-wrap" @if( !$food_detail_veg ) style="display:none;" @endif>
                                <div class="row">
                                    <div class="col-md-6"> 								 
                                        <div class="form-group">
                                        <label>Select Existing Veg items</label>
                                        <div class="input-group">
                                            <div class="form-control">
                                                <input type="hidden" name="itemId" id="itemId" value="{{ $food_detail_veg?$food_detail_veg->id:'' }}" />
                                                <span class="badge badge-info" id="fi-title">{{$food_detail_veg?$food_detail_veg->itemName:''}}
                                                </span> 
                                            </div>
                                            <span class="input-group-append">
                                                <button class="file-upload-browse btn btn-primary" data-toggle="modal" data-target="#show-exist-veg-items" type="button"><i class="menu-icon icon-list"></i></button>
                                            </span>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </span>
                            <span id="new-item-wrap" style="display:none;">
                                @include('admin.food_items.food_veg_option_form') 
                            </span>
                        </span>
                    @endif

                    <?php /*
                    <span class="optional_veg_item" style="display:none;">
                    <?php 
                    if( !empty($food_detail->vegItem) )
                    $food_detail_veg = $food_detail->vegItem ;
                    else
                    $food_detail_veg = '{}' ;
                    
                    ?>
                    
                    @include('admin.food_items.food_veg_option_form_edit',['food_detail_veg' => $food_detail_veg]) 
                    
                    </span>
                    */ ?>  
                                            
                    <div class="row">
                        <div class="col-md-6"> 
                            <input type="submit" class="btn btn-primary mr-2" value="Update Food"> 
                            <a href="{{ route('admin.food_items') }}" class="btn btn-light">Cancel</a>
                        </div>  
                    </div>	
                    
                </form>
                </div>
            </div>
        </div>
    </div> 
   
    
  </div>
  <!-- content-wrapper ends -->

  <!-- footer Start -->
  @include('includes.admincopyrightfooter')
  <!-- partial -->

    @push( 'after_scripts' )
        <!--script type="text/javascript">
          var $allRecipesCatId = "{{ $comboCatId?$comboCatId:0 }}";
          (function( $ ) { 
            $( document ).ready(function( ) { 
                $('#food_category').change(function( ) { 
                    if( $allRecipesCatId && $.inArray( $allRecipesCatId, $( this ).val( ) ) > -1 ) { 
                        $(this).find( 'option' ).prop('selected', true);
                    }
                });
            });
          })(  jQuery );
        </script-->
        <script src="https://cdn.tiny.cloud/1/qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc/tinymce/5/tinymce.min.js"></script>
        <script type="text/javascript">

            // tinymce editor
            tinymce.init({
                selector: 'textarea.tinymceeditor',
                height: 300,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste code help wordcount'
                ],
                toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tiny.cloud/css/codepen.min.css'
                ],
                onchange_callback: function(editor) {
                    tinymce.triggerSave();
                    $(".tinymceeditor").valid();
                }

            });
        </script>
    @endpush

</div> <!-- main-panel ends -->

<div class="modal fade" id="show-exist-veg-items">
  <div class="modal-dialog modal-lg modal-dialog-scrollable">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Select Veg Item</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <div class="fi-srch-wrap content m-2">
         <div class="row">
            <div class="col-md-12">
                <input type="text" class="form-control" placeholder="Type here to search food items" id="fi-srch">
            </div>
         </div>
    </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="check_select">
            <div class="container">
                <div id="fi-list-wrap" class="row">
               
                </div>
            </div>
            <div class="text-center hide" id="fi-loader">
                <img src="{{ asset('images/loader.gif') }}" width="50px" />
            </div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <a href="#" class="btn btn-primary select_meal_plan_day_btn" >Select</a>
      </div>

    </div>
  </div>
</div>

 <!-- /.content-wrapper -->
@endsection