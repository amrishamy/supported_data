@extends('layouts.admin')

@section('content')
	
<div class="main-panel">
  <div class="content-wrapper">
    @include('includes.adminbreadcrumb')

    <div class="row">
		<div class="col-12 grid-margin stretch-card">
			<div class="card">
				<div class="card-body">	
					<div class="row align-items-center">
						<div class="col-md-5 text-left">
							<h4 class="mb-0">Leader: {{ $leader->leaderName }}</h4>
						</div>
						@if( $totaldaysAddded < 98 )
						<div class="col-md-7 text-right">
							<a href="{{ route('admin.add_exercise', [ Hashids::encode( $leaderId ) ]) }}" class="btn btn-md btn-primary"><i class="fa fa-plus mr-1"></i> Add Exercise</a>
						</div>
						@endif
					</div>
					<hr/>
					<div class="table-responsive" id="listing">
						<table class="table table-bordered display tbl-reorder" id="datatable" style="width:100%;">
							<thead class="thead-light">
								<tr>
									<th width="40">S.No. </th>
									<th>Name</th>
									<th>Calories</th>	
									<th>Day Number</th>
									<th>Week Number</th>
									<th>Active</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div> 
    
  </div>
  <!-- content-wrapper ends -->

  <!-- footer Start -->
  @include('includes.admincopyrightfooter')
  <!-- partial -->

	@push('after_scripts')
	  	<script type="text/javascript">
	  		var tab = list({ 
			  			listing_url: '{{ route( 'admin.exercise_list', [ Hashids::encode( $leaderId ) ] ) }}', 
			  			orderable: { orderable: false, targets: [ 5,6 ] },
			  			/* Row Reorder [ */
			            reorder: true, 
			            reorder_url: '{{ route( 'admin.updateExerciseReorder', [ Hashids::encode( $leaderId ) ] ) }}', 
			            idIndex: 7,
			            orderIndex: 8,
			            /* ] */
			            /*drawCallback: function( settings, obj ) { 
			            	if( $( '#leader-options' ).length <= 0 ) { 

			            		$( '#datatable_filter' ).prepend( '<select id="leader-options"></select> &nbsp; &nbsp;' );
			            	}
			            },
			            ajaxData: function ( d ) {
			                d.leader = $( '#leader-options' ).val( );
			            }*/
		  			});

        var exercise = {
            list_url: '{{ route( 'admin.exercises', [ Hashids::encode( $leaderId ) ] ) }}'
        };
	</script>
    <script src="{{ asset('js/pages/exercise.js') }}"></script>
	@endpush
</div> <!-- main-panel ends -->

 <!-- /.content-wrapper -->
@endsection