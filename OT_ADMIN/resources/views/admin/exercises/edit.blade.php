@extends('layouts.admin')
@push( 'after_scripts' )
    <!--link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script-->
    <!--script type="text/javascript">
        var exercise = {
                list_url: '{{ route( 'admin.exercises', [ Hashids::encode( $leader->leaderId ) ] ) }}'
            };
    </script-->
    <!-- Tiny MCA Editor -->
	<script src="https://cdn.tiny.cloud/1/qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc/tinymce/5/tinymce.min.js"></script>
    <script type="text/javascript">

        // tinymce editor
        tinymce.init({
            selector: 'textarea.tinymceeditor',
            height: 300,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
            ],
            toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tiny.cloud/css/codepen.min.css'
            ],
            onchange_callback: function(editor) {
                tinymce.triggerSave();
                $(".tinymceeditor").valid();
            }

        });
    </script>
    <script src="{{ asset('js/pages/exercise.js') }}"></script>
@endpush
@section('content')
    
<div class="main-panel">
  <div class="content-wrapper">
    @include('includes.adminbreadcrumb')

    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                <form method="POST" enctype="multipart/form-data" class="exercise-list" action="{{ route('admin.update_exercise', [ Hashids::encode( $leader->leaderId ), Hashids::encode( $exerciseInfo->id ) ]) }}" id="edit_exercise_frm" >
                    {{ csrf_field() }}
                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                                  <label for="name">Exercise Name<sup>*</sup></label>
                                <input name="name" type="text" class="form-control" id="name" placeholder="Exercise Name" value="{{ $exerciseInfo->name }}">
                            </div>
                        </div>


                    </div>

                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="dayNumber">Day Number<sup>*</sup></label>
                                <select class="form-control" id="dayNumber" disabled>
                                    @for( $start = 0; $start <= 97; $start++ )
                                        <option value="{{$start}}"{{ $start == $exerciseInfo->dayNumber?' selected':'' }}>{{$start + 1 }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div> 

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="calories">Calories burned<sup>*</sup></label>
                                <input name="calories" type="text" class="form-control" id="calories" placeholder="Calories burned" value="{{ $exerciseInfo->calories }}">
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="data">Exercise Detail<sup>*</sup></label>
                                <textarea name="data" type="text" class="form-control tinymceeditor" id="data" placeholder="Exercise Detail">{{ $exerciseInfo->data }}</textarea>
                            </div>  
                        </div>
                    </div>

                    <?php 
                     $isJson  = is_string( $exerciseInfo->video_links ) && is_array(json_decode($exerciseInfo->video_links, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
                    if($isJson)
                    {
                        $video_links_decode = json_decode($exerciseInfo->video_links, true );
                    }
                    else{
                        $video_links_decode = [ [ "link"=>$exerciseInfo->video_links,"title"=>"" ] ];
                    }
                    //  echo "<pre>";print_r($video_links_decode);die;
                    $index = 0;
                    foreach($video_links_decode as $v_link)
                    { 
                    ?>
                    <div class="row repeated_video_link_section">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="video_links">Video Link</label>
                                <input name="video_links[{{$index}}][link]" type="text" class="form-control" id="video_links" placeholder="Video Link" value="{{ $v_link['link'] }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="video_links">Video Title</label>
                                <input name="video_links[{{$index}}][title]" type="text" class="form-control" id="video_links" placeholder="Video Title" value="{{ $v_link['title'] }}">
                            </div>
                        </div> 
                        <div class="col-md-2">
                            <div class="form-group">
                            <a href="#" class="btn btn-primary remove_more" onclick="removeVideoLink($(this),event);"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                            </div>
                        </div>
                    </div>
                     <?php 
                     $index++ ; 
                    }
                     ?>
                    
                    <div class="row add_more_div">
                        <div class="col-md-12 text-right">
                            <a href="#" class="btn btn-primary add_more">+ Add more video link</a>
                        </div>  
                    </div> 

                                            
                    <div class="row">
                        <div class="col-md-6"> 
                            <input type="submit" class="btn btn-primary mr-2" value="Update"> 
                            <a href="{{ route('admin.exercises', [ Hashids::encode( $leader->leaderId ) ]) }}" class="btn btn-light">Cancel</a>
                        </div>  
                    </div>  
                    
                </form>
                </div>
            </div>
        </div>
    </div> 
   
    
  </div>
  <!-- content-wrapper ends -->

  <!-- footer Start -->
  @include('includes.admincopyrightfooter')
  <!-- partial -->
</div> <!-- main-panel ends -->

 <!-- /.content-wrapper -->
@endsection