@extends('layouts.admin')
@section('content')

<div class="main-panel">
  <div class="content-wrapper">
    @include('includes.adminbreadcrumb')
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">	
                    <form method="POST" enctype="multipart/form-data" class="exercise-list" action="{{ route('admin.updatedailymealplan', Hashids::encode( $daily_meal_plan_detail->dayNumber ) ) }}" id="edit_daily_meal_frm" >
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="dayNumber">Day Number</label>
                                <input type="text" class="form-control" id="dayNumber" value="{{ $daily_meal_plan_detail->dayNumber +1  }}" disabled />
                                </div>
                            </div>
                        </div>
                    
                        <div class="row">
                            <div class="col-md-6"> 								 
                                <div class="form-group">
                                <label>Select Meal of the Day</label>
                                <div class="input-group">
                                    <div class="form-control">
                                        <input type="hidden" name="itemId" id="itemId" value="{{ old( 'itemId' ) OR $foodItem?$foodItem->id:'' }}" />
                                        <span class="badge badge-info" id="fi-title">{{$foodItem?$foodItem->itemName:''}}
                                            <!--<span data-role="remove"><i class="menu-icon icon-close ml-1"></i></span>-->
                                        </span> 
                                        </div>
                                    <span class="input-group-append">
                                    <button class="file-upload-browse btn btn-primary" data-toggle="modal" data-target="#show-foods-items" type="button"><i class="menu-icon icon-list"></i></button>
                                    </span>
                                </div>
                                </div>
                            </div>
                        </div>  
                                    
                        <div class="row">
                            <div class="col-md-6"> 
                                <input type="submit" class="btn btn-primary mr-2" value="Update Plan"> 
                                <a href="{{ route('admin.dailymealplanlist') }}" class="btn btn-light">Cancel</a>
                            </div>  
                        </div>	
                    </form>
                </div>
            </div>
        </div>
    </div> 
</div>
  <!-- content-wrapper ends -->

  <!-- footer Start -->
  @include('includes.admincopyrightfooter')
  <!-- partial -->

  
    @push('after_scripts')
        <script type="text/javascript">
            var fiObj = {
                selected_fi: {{$foodItem && $foodItem->id?$foodItem->id:0}},
                get_food_items: '{{ route( 'admin.get_food_items' ) }}'
            };
        </script>
        <script src="{{ asset('js/pages/daily-meal-plans.js') }}"></script>
    @endpush
</div> <!-- main-panel ends -->


<div class="modal fade" id="show-foods-items">
  <div class="modal-dialog modal-lg modal-dialog-scrollable">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Select Meal Of the Day</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <div class="fi-srch-wrap content m-2">
         <div class="row">
            <div class="col-md-12">
                <input type="text" class="form-control" placeholder="Type here to search food items" id="fi-srch">
            </div>
         </div>
    </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="check_select">
            <div class="container">
                <div id="fi-list-wrap" class="row">
               
                </div>
            </div>
            <div class="text-center hide" id="fi-loader">
                <img src="{{ asset('images/loader.gif') }}" width="50px" />
            </div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <a href="#" class="btn btn-primary select_meal_plan_day_btn" >Select</a>
      </div>

    </div>
  </div>
</div>


 <!-- /.content-wrapper -->
@endsection