@extends('layouts.default')
@section('content')
<div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-5">
                <div class="brand-logo text-center">
                   <img src="{{ asset('images/ot_logo.png') }}" class="img-fluid">
                </div>
                <h3>Sign In</h3>

                @if ($message = Session::get('error'))
                <div class="login-box" style="min-height:50px;max-height:50px;background:transparent;">
                <div class="alert alert-danger alert-block" style="padding: 7px;margin: 2px 2px;">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div></div>
                @endif

                @if ($message = Session::get('forgot_error'))
                <div class="login-box" style="min-height:50px;max-height:50px;background:transparent;">
                <div class="alert alert-danger alert-block" style="padding: 7px;margin: 2px 2px;">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div></div>
                @endif

                @if ($message = Session::get('forgot_success'))
                <div class="login-box" style="min-height:50px;max-height:50px;background:transparent;">
                <div class="alert alert-success alert-block" style="padding: 7px;margin: 2px 2px;">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div></div>
                @endif

                <form class="pt-3 login_frm" id="login_frm" method="post" action="{{ route('admin.authenticate') }}">
                  {{ csrf_field() }}
                  <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                    <input type="text" name="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Email">
                    {!! $errors->first('email', '<p class="validation-errors">:message</p>') !!}
                  </div>
                  <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                    <input type="password" name="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password">
                    {!! $errors->first('password', '<p class="validation-errors">:message</p>') !!}
                  </div>
                  <div class="form-group d-flex justify-content-between align-items-center">
                    <div class="form-check">
                      <label class="form-check-label text-muted">
                        <input type="checkbox" class="form-check-input"> Keep me signed in </label>
                    </div>
                    <a href="{{ route('admin.forgot') }}" class="auth-link text-black">Forgot password?</a>
                  </div>
                  <div class="mt-3">
                    <!-- <a class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" href="index.html">SIGN IN</a> -->
                    <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" type="submit"><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN IN</button>
                  </div>
                   
                  <!-- <div class="text-center mt-4"> Don't have an account? <a href="register.html" class="text-primary">Create</a>
                  </div> -->
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
@stop
