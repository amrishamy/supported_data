@extends('layouts.admin')

@push( 'after_scripts' )
    <script src="{{ asset('js/pages/leader-edit.js') }}"></script>
@endpush

@section('content')
    
<div class="main-panel">
  <div class="content-wrapper">
    @include('includes.adminbreadcrumb')

    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                <form method="POST" enctype="multipart/form-data" class="exercise-list" action="{{ route('admin.update_leader', Hashids::encode( $leaderInfo->leaderId ) ) }}" id="edit_leader_frm" >
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                  <label for="leaderName">Name<sup>*</sup></label>
                                <input name="leaderName" type="text" class="form-control" id="leaderName" placeholder="Name" value="{{ $leaderInfo->leaderName }}">
                            </div>
                        </div> 
                        <div class="col-md-{{ $leaderInfo->image?4:6 }}">                               
                            <div class="form-group">
                            <label>Upload Leader Image</label>
                            <input name="image" type="file" class="file-upload-default">
                            <div class="input-group col-xs-12">
                                <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                                <span class="input-group-append">
                                <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                </span>
                            </div>
                            </div>
                        </div>
                        @if( $leaderInfo->image )
                        <div class="col-md-2">                               
                            <div class="form-group">
                             <span class="img-wrap">
                                <img src="{{ env('AWS_S3_BUCKET_URL').'/'.env('AWS_S3_LEADERS_IMAGE_BASEURL').'/'.$leaderInfo->image . '?' . time() }}" alt="" width="100%" />
                                <a href="#" onclick="return deleteImage( '{{route( 'admin.delete_leader_image', Hashids::encode( $leaderInfo->leaderId ) )}}', this );" title="Click here to delete image"><i class="fa fa-times" aria-hidden="true"></i></a>
                             </span>
                            </div>
                        </div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                  <label for="age">Age<sup>*</sup></label>
                                <input name="age" type="number" class="form-control" id="age" placeholder="Age" value="{{ $leaderInfo->age }}">
                            </div>
                        </div> 
                    
                        <div class="col-md-6">
                            <div class="form-group">
                                  <label for="weight">Weight<sup>*</sup></label>
                                <input name="weight" type="text" class="form-control" id="weight" placeholder="Weight" value="{{ $leaderInfo->weight }}">
                                <strong style="display:block;"><i>( In pounds )</i></strong>
                            </div>
                        </div> 
                    </div>

                    <div class="row">

                        <div class="col-md-12">
                            <div class="form-group">
                            <label for="location">Location<sup>*</sup></label>
                            <input name="location" type="text" class="form-control" id="location" placeholder="Location" value="{{ $leaderInfo->location }}">
                            </div>  
                        </div>
                    </div>
                                            
                    <div class="row">
                        <div class="col-md-6"> 
                            <input type="submit" class="btn btn-primary mr-2" value="Update"> 
                            <a href="{{ route('admin.leaders') }}" class="btn btn-light">Cancel</a>
                        </div>  
                    </div>  
                    
                </form>
                </div>
            </div>
        </div>
    </div> 
   
    
  </div>
  <!-- content-wrapper ends -->

  <!-- footer Start -->
  @include('includes.admincopyrightfooter')
  <!-- partial -->
</div> <!-- main-panel ends -->

 <!-- /.content-wrapper -->
@endsection