@extends('layouts.admin')

@section('content')
	
<div class="main-panel">
  <div class="content-wrapper">
    @include('includes.adminbreadcrumb')

    <div class="row">
		<div class="col-12 grid-margin stretch-card">
			<div class="card">
				<div class="card-body">	
					<div class="row align-items-center">
						<div class="col-md-5 text-left">
							<h4 class="mb-0">Leaders</h4>
						</div>
						<div class="col-md-7 text-right">
							<a href="{{ route('admin.add_leader') }}" class="btn btn-md btn-primary"><i class="fa fa-plus mr-1"></i> Add Leader</a>
						</div>
					</div>
					<hr/>
					<div class="table-responsive" id="listing">
						<table class="table table-bordered display" id="datatable" style="width:100%;">
							<thead class="thead-light">
								<tr>
									<th width="40">S.No. </th>
									<th>Image</th>
									<th>Name</th>
									<th>Age</th>	
									<th>Weight</th>
									<th>Active</th>
									<th>Action</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div> 
    
  </div>
  <!-- content-wrapper ends -->

  <!-- footer Start -->
  @include('includes.admincopyrightfooter')
  <!-- partial -->

	@push('after_scripts')
	  	<script type="text/javascript">
	  		list({ 
	  			listing_url: '{{ route( 'admin.leaders_list' ) }}', 
	  			orderable: { orderable: false, targets: [ 1,5,6 ] },
  			});
		</script>
	@endpush
</div> <!-- main-panel ends -->

 <!-- /.content-wrapper -->
@endsection