@extends('layouts.admin')
@section('content')
	
<div class="main-panel">
  <div class="content-wrapper">
    @include('includes.adminbreadcrumb')

    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">	
                    <form method="POST" enctype="multipart/form-data" class="exercise-list" action="{{ route('admin.savepartner') }}" id="add_partner_frm" >
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="exampleInputName1">Partner Name</label>
                                <input name="partner_name" type="text" class="form-control" id="partner_name" value="{{ old('partner_name') }}" placeholder="Partner Name">
                                
                                </div>
                            </div> 								  
                        </div>
                            
                        <div class="row">
                            <div class="col-md-6"> 								 
                                <div class="form-group">
                                <label>Image</label>
                                <input type="file" name="partner_image" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image">
                                    <span class="input-group-append">
                                    <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                                </div>
                            </div> 			 
                                                            
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="exampleInputName1">Image link</label>
                                <input name="image_link" type="text" class="form-control" id="image_link" value="{{ old('image_link') }}" placeholder="Enter the Hyperlink for image">
                                
                                </div>
                            </div> 								  
                        </div> 
                                        
                        <div class="row">
                            <div class="col-md-6"> 
                                <input type="submit" class="btn btn-primary mr-2" value="Add Partner"> 
                                <a href="{{ route('admin.partnerlist') }}" class="btn btn-light">Cancel</a>
                            </div>  
                        </div>	
                    </form>
                    
                </div>
            </div>
        </div>
    </div> 
    
  </div>
  <!-- content-wrapper ends -->

  <!-- footer Start -->
  @include('includes.admincopyrightfooter')
  <!-- partial -->
</div> <!-- main-panel ends -->

 <!-- /.content-wrapper -->
@endsection
