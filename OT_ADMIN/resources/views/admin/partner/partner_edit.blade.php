@extends('layouts.admin')
@section('content')
	
<div class="main-panel">
  <div class="content-wrapper">
    @include('includes.adminbreadcrumb')
    
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">	
                    <form method="POST" enctype="multipart/form-data" class="exercise-list" action="{{ route('admin.updatepartner') }}" id="edit_partner_frm" >
                        {{ csrf_field() }}
                        <input type="hidden" name="partnerid" value="{{$partner_detail->id}}" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="exampleInputName1">Partner Name</label>
                                <input name="partner_name" type="text" class="form-control" id="exampleInputName1" placeholder="Partner Name" value="{{ $partner_detail->name }}" >
                                @if ($errors->has('name'))
                                    <div class="error">{{ $errors->first('name') }}</div>
                                @endif
                                </div>
                            </div> 								  
                        </div>
                            
                        <div class="row">
                            <div class="col-md-6"> 								 
                                <div class="form-group">
                                <label>Image</label>
                                <input type="file" name="partner_image" class="file-upload-default">
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled="" placeholder="Upload Image" />
                                    <span class="input-group-append">
                                    <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                    </span>
                                </div>
                                </div>
                            </div>

                            @if( $partner_detail->image )
                            <div class="col-md-2">                               
                                <div class="form-group">
                                 <span class="img-wrap">
                                    <img src="{{ env('AWS_S3_BUCKET_URL').'/'.env('AWS_S3_PARTNER_IMAGE_BASEURL').'/'.$partner_detail->image . '?' . time() }}" alt="{{ $partner_detail->image }}" title="{{ $partner_detail->image }}" width="100%" />
                                    <a href="#" onclick="return deleteImage( '{{route( 'admin.delete_partner_image', Hashids::encode( $partner_detail->id ) )}}', this );" title="Click here to delete image"><i class="fa fa-times" aria-hidden="true"></i></a>
                                 </span>
                                </div>
                            </div>
                            @endif 
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="exampleInputName1">Image link</label>
                                <input name="image_link" type="text" class="form-control" id="image_link" value="{{ $partner_detail->image_link }}" placeholder="Enter the Hyperlink for image">
                                
                                </div>
                            </div> 								  
                        </div> 
                                        
                        <div class="row">
                            <div class="col-md-6"> 
                                <input type="submit" class="btn btn-primary mr-2" value="Update Partner"> 
                                <a href="{{ route('admin.partnerlist') }}" class="btn btn-light">Cancel</a>
                            </div>  
                        </div>	
                    </form>
                    
                </div>
            </div>
        </div>
    </div> 

  </div>
  <!-- content-wrapper ends -->

  <!-- footer Start -->
  @include('includes.admincopyrightfooter')
  <!-- partial -->
</div> <!-- main-panel ends -->

 <!-- /.content-wrapper -->
@endsection
