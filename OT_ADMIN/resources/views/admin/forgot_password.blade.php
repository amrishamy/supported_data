@extends('layouts.default')
@section('content')
<div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-5">
                <div class="brand-logo text-center">
                   <img src="{{ asset('images/ot_logo.png') }}" class="img-fluid">
                </div>
                
                @if ($message = Session::get('error'))
                <div class="login-box" style="min-height:50px;max-height:50px;background:transparent;">
                <div class="alert alert-danger alert-block" style="padding: 7px;margin: 2px 2px;">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div></div>
                @endif

                @if ($message = Session::get('forgot_error'))
                <div class="login-box" style="min-height:50px;max-height:50px;background:transparent;">
                <div class="alert alert-danger alert-block" style="padding: 7px;margin: 2px 2px;">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div></div>
                @endif

                @if ($message = Session::get('forgot_success'))
                <div class="login-box" style="min-height:50px;max-height:50px;background:transparent;">
                <div class="alert alert-success alert-block" style="padding: 7px;margin: 2px 2px;">
                  <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                </div></div>
                @endif

                <div class="text-center mb-2">
                  <h3 class="text-center">Forgot your password?</h3>
                  <span class="text-muted d-block">Enter you email address below and we'll get you back on track.</span>
                </div>

                <form class="pt-3 forget-form" method="post" action="{{route('admin.forgot_password') }}">
                  {{ csrf_field() }}
                  
                  <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                    
                    <input type="text" class="form-control form-control-lg" name="forgot_email" placeholder="Enter your email">
                    {!! $errors->first('email', '<p class="validation-errors">:message</p>') !!}
                  </div>
                  <div class="mt-3">
                    <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn"><i class="fa fa-unlock fa-lg fa-fw"></i>RESET</button>
                  </div>

                  <div class="text-center mt-4"> Already have an account? <a href="{{ route('admin.login') }}" class="text-primary">Login</a></div>

                  
                </form>

              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
@stop
