@extends('layouts.default')
@section('content')

<div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-center p-5">
                <div class="brand-logo text-center">
                   <img src="{{ asset('images/ot_logo.png') }}" class="img-fluid">
                </div>
                <h3 class="text-center font-weight-bold">Reset password?</h3>
                <span>Enter your new password.</span>

                @if ($message = Session::get('success'))
                  <div class="login-box" style="min-height:50px;max-height:50px;background:transparent;">
                    <div class="alert alert-success alert-block" style="padding: 7px;margin: 2px 2px;">
                  <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong>Password updated successfully. Go to <a href="{{ route('admin.login') }}">Login</a></strong>
                  </div></div>
                @endif

                @if ($message = Session::get('error'))
                  <div class="login-box" style="min-height:50px;max-height:50px;background:transparent;">
                  <div class="alert alert-danger alert-block" style="padding: 7px;margin: 2px 2px;">
                  <button type="button" class="close" data-dismiss="alert">×</button>	
                        <strong>{{ $message }}</strong>
                  </div></div>
                @endif
                
                <form class="pt-3" method="post" action="{{ route('admin.update_password') }}">
                {{ csrf_field() }}
                  <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                    <input type="password" name="password"  class="form-control form-control-lg" id="" placeholder="New Password">
                    {!! $errors->first('password', '<p class="validation-errors">:message</p>') !!}
                  </div> 
				          <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                    <input type="password" name="cpassword" class="form-control form-control-lg" id="" placeholder="Confirm Password">
                    {!! $errors->first('cpassword', '<p class="validation-errors">:message</p>') !!}
                  </div>                  
                  <div class="mt-3">
                    
                    <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn"><i class="fa fa-sign-in fa-lg fa-fw"></i>Change password</button>
                  </div>
                   
                  <div class="text-center mt-4"> Already have an account? <a href="{{ route('admin.login') }}" class="text-primary">Login</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
 
@stop
