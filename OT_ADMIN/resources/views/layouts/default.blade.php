<!DOCTYPE html>
<html>

  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{ asset('vendors/simple-line-icons/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!-- <link rel="stylesheet" href="{{ asset('css/main.css') }}"> -->

    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" />
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>{{$title}} - Panel</title>
    <!-- <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script> -->
    <!-- <script src="{{ asset('js/jquery.validate.min.js') }}"></script> -->

  </head>
  <body class="login_bg">

	@yield('content')

	<!-- Essential javascripts for application to work-->

    <!-- <script src="{{ asset('js/popper.min.js') }}"></script> -->
    <!-- <script src="{{ asset('js/bootstrap.min.js') }}"></script> -->
    <!-- <script src="{{ asset('js/main.js') }}"></script> -->
    <!-- <script src="{{ asset('js/script.js') }}"></script> -->
    <!-- The javascript plugin to display page loading on top-->
    <!-- <script src="{{ asset('js/plugins/pace.min.js') }}"></script> -->
    <script type="text/javascript">
      // Login Page Flipbox control
      /*$('.login-content [data-toggle="flip"]').click(function() {
      	$('.login-box').toggleClass('flipped');
      	return false;
      });*/
    </script>


    <!-- plugins:js -->
    <script src="{{ asset('vendors/js/vendor.bundle.base.js') }}"></script> <!-- having jquery,bootstrap, theme -->
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>


    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('js/off-canvas.js') }}"></script>
    <script src="{{ asset('js/misc.js') }}"></script>
    <!-- endinject -->


  </body>
</html>