<!doctype html>
<html lang="en">
<head>
	
	<title>{{$title}} - Admin Panel</title>
	<link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" />
    
    <meta name="description" content="">
	<!-- Twitter meta-->
	<meta property="twitter:card" content="">
	<meta property="twitter:site" content="">
	<meta property="twitter:creator" content="">
	<!-- Open Graph Meta-->
	<meta property="og:type" content="website">
	<meta property="og:site_name" content="Admin Panel">
	<meta property="og:title" content="">
	<meta property="og:url" content="">
	<meta property="og:image" content="">
	<meta property="og:description" content="">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- plugins:css -->
	@stack('before_styles')
    <link rel="stylesheet" href="{{ asset('vendors/simple-line-icons/css/simple-line-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/flag-icon-css/css/flag-icon.min.css') }}">
	<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/fontawesome.min.css">
	<link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.base.css') }}">

    <link rel="stylesheet" href="{{ asset('vendors/daterangepicker/daterangepicker.css') }}">
	<link rel="stylesheet" href="{{ asset('vendors/chartist/chartist.min.css') }}">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.1/css/select.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/rowreorder/1.2.6/css/rowReorder.dataTables.min.css">
   
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!-- End layout styles -->
	@stack('after_styles')

	<script src='https://kit.fontawesome.com/a076d05399.js'></script>
	
	<style>
	.mce-notification {display: none !important;}
	.tox-notification {display: none !important;}
	</style>

</head>
<body  class="admin_body">
<div class="container-scroller">
    @include('includes.adminheader')
    	@yield('content')
    @include('includes.adminfooter')
</div>
</body>
</html>