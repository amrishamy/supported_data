# ChicMic-Component-PHP-Admin

# used artisan commands:-

php artisan db:seed --class=UsersTableSeeder

# Migrations for partner
php artisan make:controller Admin/PartnerController --resource

php artisan make:migration create_partners_table

php artisan migrate --path='/database/migrations/2019_11_21_094137_create_partners_table.php'

php artisan make:model Models/Partner

# Migrations for Category

php artisan make:model Models/Food_category -m      ( model with migrations )

php artisan make:controller Admin/CategoryController --resource

php artisan migrate --path=/database/migrations/2019_11_25_064642_create_food_categories_table.php


# Food Items =================

php artisan make:model Models/Food_item

php artisan make:controller Admin/FoodItemController --resource


# ==================== migration for tables updations===========

1) php artisan make:migration update_app_users_table --table=app_users
php artisan migrate --path='/database/migrations/2019_12_16_061628_update_app_users_table.php'



