<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Food_category extends Model
{
    public $table = "food_categories";

    const CREATED_AT = 'createdDate';

    const UPDATED_AT = 'updatedDate';
}
