<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Food_item extends Model
{
    public $table = "food_items";

    const CREATED_AT = 'createdDate';

    const UPDATED_AT = 'updatedDate';

    function vegItem() {
        return $this->belongsTo('App\Models\Food_item', 'veg_item_id'); 
      }

      function in_categories() {
        return $this->hasMany('App\Models\Category_item', 'itemId'); 
      }
}
