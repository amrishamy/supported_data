<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $table = 'users';

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'gender', 'email_verified_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public static function all_users($post_request = NULL){
        $records_per_page = 15;
        $result = User::where('role_id', '=', 2)
        ->whereRaw('(name LIKE "%' . $post_request->search_keyword . '%" or email LIKE "%' . $post_request->search_keyword . '%")')
        ->orderBy('created_at', 'desc')->paginate($records_per_page);
       /*var_dump(DB::getQueryLog());
        exit;*/
        return $result;
    }



}
