<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shopping_list extends Model
{
    public $table = "shopping_list_pdf";

    const CREATED_AT = 'createdDate';

    const UPDATED_AT = 'updatedDate';
}
