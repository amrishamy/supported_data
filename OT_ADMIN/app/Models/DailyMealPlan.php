<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DailyMealPlan extends Model
{
    public $table = "daily_meal";

    const CREATED_AT = 'createdDate';

    const UPDATED_AT = 'updatedDate';

    protected $primaryKey = 'dayNumber';
}
