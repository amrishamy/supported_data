<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LastUpdated extends Model
{
    public $table = "is_updated";

    const CREATED_AT = 'createdDate';

    const UPDATED_AT = 'updatedDate';

    public static function updateDateTime( $isShoppingUpdated = false ) { 
		$dateTimeStr = \Carbon\Carbon::now()->toDateTimeString();
    	$obj = (new static)::where( 'id', 1 )->first( );
    	$obj->updatedDate = $dateTimeStr;
    	if( $isShoppingUpdated ) $obj->shopingListUpdatedDate = $dateTimeStr;
    	return $obj->save();
    }
}
