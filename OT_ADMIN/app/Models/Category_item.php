<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category_item extends Model
{
    public $table = "category_items";

    public $timestamps = false ;

}
