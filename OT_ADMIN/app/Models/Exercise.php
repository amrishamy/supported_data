<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
    public $table = "fitness_plan";

    const CREATED_AT = 'createdDate';

    const UPDATED_AT = 'updatedDate';
}
