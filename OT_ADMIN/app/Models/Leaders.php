<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Leaders extends Model
{
    public $table = "leaders";

    const CREATED_AT = 'createdDate';

    const UPDATED_AT = 'updatedDate';

    protected $primaryKey = 'leaderId';
}
