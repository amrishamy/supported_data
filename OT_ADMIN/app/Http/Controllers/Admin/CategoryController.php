<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\CommonController;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Config;
use Mail;
use Hash;
use Vinkla\Hashids\Facades\Hashids;

use Auth;
use App\Models\User;
use App\Models\Emails;
use App\Models\Food_category;

use Response;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        //$all_categories = Food_category::orderBy('orderBy','asc')->get(); 
        return view('admin.categories.category_list',[ 'title'=>"Manage Categories",'breadcrumbItem'=>'Food' , 'breadcrumbItemLink'=>route('admin.categorylist'), 'breadcrumbTitle'=>'All Categories' ] );
    }

    public function list( Request $request) { 

        $requestedData  = $request->all();

        $draw           = ( isset( $requestedData[ 'draw' ] ) && $requestedData[ 'draw' ] > 0 )?$requestedData[ 'draw' ]:1;
        $start          = ( isset( $requestedData[ 'start' ] ) && $requestedData[ 'start' ] > 0 )?$requestedData[ 'start' ]:0;
        $perPage        = ( isset( $requestedData[ 'length' ] ) && $requestedData[ 'length' ] > 0 )?$requestedData[ 'length' ]:10;
        $search         = ( isset( $requestedData[ 'search' ] ) && isset( $requestedData[ 'search' ][ 'value' ] ) )?$requestedData[ 'search' ][ 'value' ]:'';
        
        $orderColumn    = ( isset( $requestedData[ 'order' ] ) && isset( $requestedData[ 'order' ][ 0 ] ) && isset( $requestedData[ 'order' ][ 0 ][ 'column' ] ) )?$requestedData[ 'order' ][ 0 ][ 'column' ]:0;
        $orderDir        = ( isset( $requestedData[ 'order' ] ) && isset( $requestedData[ 'order' ][ 0 ] ) && isset( $requestedData[ 'order' ][ 0 ][ 'dir' ] ) )?$requestedData[ 'order' ][ 0 ][ 'dir' ]:'ASC';

        $categoryTblName        = with( new Food_category )->getTable( ); 
        $orderByColumns = [ $categoryTblName . '.orderBy', $categoryTblName . '.categoryName' ];

        $catObj = Food_category::where( $categoryTblName . '.isArchive', 0 )->orderBy( $orderByColumns[ $orderColumn ], $orderDir );

        $totalItems = $catObj->count( );

        if( $search ) { 
            $catObj->where( $categoryTblName . '.categoryName', 'like', "%{$search}%" );
        }

        $recordsFiltered = $catObj->count( );

        $catItems = $catObj->offset( $start )->limit( $perPage )->get( );
        
        $data = [ "draw" => $draw, "recordsTotal" => $totalItems, "recordsFiltered" => $recordsFiltered, "data" => [] ];
        if( $catItems->count( ) > 0 ) { 
            foreach( $catItems as $category ) { 
                $data[ 'data' ][] = [  
                                    ++$start,
                                    $category->categoryName,
                                    '<span><img src="' . env('AWS_S3_BUCKET_URL').'/'.env('AWS_S3_FOOD_CATEGORYIMAGE_BASEURL').'/'.$category->categoryImage . '" alt="" /></span>',
                                    '<div class="form-group">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input active_status category_active_' . $category->id . '" name="category_active_' . $category->id . '" onchange="return updateStatus( ' . $category->id . ', \'' . route( 'admin.category_status_update' ) . '\', this.value );" id="yes" value="1" ' . ( $category->activeStatus == 1 ? "checked":"" ) . '> Yes <i class="input-helper"></i></label>
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input active_status category_active_' . $category->id . '" name="category_active_' . $category->id . '" onchange="return updateStatus( ' . $category->id . ', \'' . route( 'admin.category_status_update' ) . '\', this.value );" id="no" value="0" ' . ( $category->activeStatus == 0 ? "checked":"" ) . '> No <i class="input-helper"></i></label>
                                        </div>                                          
                                    </div>',
                                    '<a href="' . route('admin.category_food_items', [ 'id'=> Hashids::encode($category->id)  ] ) . '" class="add" title="View category products list and reorder"><i class="fa fa-list"></i></a>
                                    <a href="' . route('admin.editcategory', [ 'id'=> Hashids::encode($category->id)  ] ) . '" class="add" title=""><i class="fa fa-edit"></i></a>
                                    <a href="' . route('admin.deletecategory', [ 'id'=> Hashids::encode($category->id)  ] ) . '" class="delete_record" ><i class="fa fa-trash"></i></a>',
                                    Hashids::encode($category->id),
                                    $category->orderBy 
                                ];
            }
        }

        return response( )->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Add Category";
        return view('admin.categories.category_add',['title'=>$title,'breadcrumbItem'=>'Food' , 'breadcrumbItemLink'=>route('admin.categorylist'),  'breadcrumbTitle'=>'Add Category' ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request_data = $request->all();
        
        $request->validate([
            'category_name' => 'required',
        ]);
        
        $file_name_with_time_prefix = upload_file($request, 'category_image' , env('AWS_S3_FOOD_CATEGORYIMAGE_BASEURL') );
        

        $category = new Food_category();
        $category->categoryName= $request['category_name'];
        if($file_name_with_time_prefix)
            $category->categoryImage= $file_name_with_time_prefix;
        
        $category->save();

        $category->orderBy = $category->id;
        $category->save();
        
        \App\Models\LastUpdated::updateDateTime();
        return redirect()->route('admin.categorylist')->with('success','Category saved successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $decryptedId = Hashids::decode($id);
        
        $title = "Edit Category";
        $category_detail = Food_category::find($decryptedId[0]);     
        
        return view('admin.categories.category_edit',[ 'title'=>$title,'breadcrumbItem'=>'All categories' , 'breadcrumbItemLink'=>route('admin.categorylist'), 'breadcrumbTitle'=>'Edit Category','category_detail'=>$category_detail ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request_data = $request->all();
        
        $request->validate([
            'category_name' => 'required',
        ]);
        $category = Food_category::find($request->categoryid);

        $file_name_with_time_prefix = upload_file($request, 'category_image' , env('AWS_S3_FOOD_CATEGORYIMAGE_BASEURL') );

        $category->categoryName= $request['category_name'];
        if($file_name_with_time_prefix)
        $category->categoryImage= $file_name_with_time_prefix;
        
        $category->save();
        
        \App\Models\LastUpdated::updateDateTime();
        return redirect()->route('admin.categorylist')->with('success','Category updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decryptedId = Hashids::decode($id);
        if( !( isset($decryptedId[0]) && $decryptedId[0] > 0 ) )
        return redirect()->route('admin.categorylist')->with('error','Invalid Request id !');

        $catInfo = Food_category::find( $decryptedId[0] );
        if( $catInfo->count() <= 0 ) { 
            return redirect()->route('admin.categorylist')->with('error','Some information is missing!');
        }

        $catInfo->isArchive = 1 ;
        if( !$catInfo->save() ){ 
            return redirect()->route('admin.categorylist')->with('error','Some information is missing!');
        }

        \App\Models\LastUpdated::updateDateTime();
        return redirect()->route('admin.categorylist')->with('success','Category deleted successfully.');
        
    }
    // function to update the status (Active/Inactive)
    public function category_status_update(Request $request)
    {
        $request_data = $request->all();
        if( ! ( isset( $request->id ) && $request->id > 0 ) ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        // echo "<pre>";print_r($request_data);die;
        $category = Food_category::find($request->id);
        if( $category->count() <= 0 ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Selected Category Not Found'
            ]);
        }

        $category->activeStatus = $category->activeStatus ? 0 : 1 ;
        
        if( !$category->save( ) ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Unable to update status, please try again later.'
            ]);
        }
        
        \App\Models\LastUpdated::updateDateTime();
        return response()->json([
            'status' => 1,
            'msg' => 'Status updated successfully.'
        ]);
    }

    public function updateCategoryItemReorder(Request $request)
    {
        $request_data = $request->all();
        // echo "<pre>";print_r( $request_data );  die;
        
        $validator = $request->validate([
                'replace_ids' => 'required'
            ]);

        if( count( $request_data['replace_ids'] ) <= 1 ) { 
            return response::json([ 'errors' => 'Unable to update order, please try again later.' ]);
        }

        $positions = $ids = [];
        foreach( array_keys( $request_data['replace_ids'] ) as $id ) { 
            $ids[] = Hashids::decode( ( string )$id )[0];
            $positions[ Hashids::decode( ( string )$id )[0] ] = $request_data['replace_ids'][ $id ];
        }
            
        $items = Food_category::whereIn( 'id', $ids )->get();
        if( $items->count() !=  count( $request_data['replace_ids'] ) ) { 
            return response::json([ 'errors' => 'Unable to update order, please try again later.' ]);
        }

        foreach( $items as $item ){ 
            $item->orderBy = $positions[ $item->id ];
            $item->save();
        }

        \App\Models\LastUpdated::updateDateTime();
        return response::json([ 'success' => 'Order updated successfully' ]);

    }

    public function deleteImage ( $id ) { 
        if( !$id ){
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        $id = Hashids::decode($id);
        if( !$id ){
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        $id = $id[0];

        $obj = Food_category::find( $id );
        if( $obj->count() <= 0 ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Selected item not found'
            ]);
        }

        if( !$obj->categoryImage ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Selected leader image not found'
            ]);
        }

        $isDeleted = Storage::disk('s3')->delete( env( 'AWS_S3_FOOD_CATEGORYIMAGE_BASEURL' ) . '/' . $obj->categoryImage );
        if( !$isDeleted ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Unable to delete image, please try again later!'
            ]);
        }

        $obj->categoryImage = '';
        
        if( !$obj->save( ) ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Unable to delete image, please try again later!'
            ]);
        }
        
        \App\Models\LastUpdated::updateDateTime();
        return response()->json([
            'status' => 1,
            'msg' => 'Category image deleted!.'
        ]);

    }
}
