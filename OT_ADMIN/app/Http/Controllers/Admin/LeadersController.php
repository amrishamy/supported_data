<?php

namespace App\Http\Controllers\Admin;

use Session;
use Config;
use Mail;
use Hash;
use Auth;
use Response;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\CommonController;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Vinkla\Hashids\Facades\Hashids;

use App\Models\Leaders;

class LeadersController extends Controller
{
    
    public function index()
    {
         return view('admin.leaders.index',[ 'title'=>"Manage Leaders",'breadcrumbItem'=>'All Leaders' , 'breadcrumbItemLink'=>route('admin.leaders'), 'breadcrumbTitle'=>'All Leaders' ] );
    }

    public function list( Request $request) { 

        $requestedData  = $request->all();

        $draw           = ( isset( $requestedData[ 'draw' ] ) && $requestedData[ 'draw' ] > 0 )?$requestedData[ 'draw' ]:1;
        $start          = ( isset( $requestedData[ 'start' ] ) && $requestedData[ 'start' ] > 0 )?$requestedData[ 'start' ]:0;
        $perPage        = ( isset( $requestedData[ 'length' ] ) && $requestedData[ 'length' ] > 0 )?$requestedData[ 'length' ]:10;
        $search         = ( isset( $requestedData[ 'search' ] ) && isset( $requestedData[ 'search' ][ 'value' ] ) )?$requestedData[ 'search' ][ 'value' ]:'';
        
        $orderColumn    = ( isset( $requestedData[ 'order' ] ) && isset( $requestedData[ 'order' ][ 0 ] ) && isset( $requestedData[ 'order' ][ 0 ][ 'column' ] ) )?$requestedData[ 'order' ][ 0 ][ 'column' ]:0;
        $orderDir        = ( isset( $requestedData[ 'order' ] ) && isset( $requestedData[ 'order' ][ 0 ] ) && isset( $requestedData[ 'order' ][ 0 ][ 'dir' ] ) )?$requestedData[ 'order' ][ 0 ][ 'dir' ]:'ASC';

        $leaderTblName        = with( new Leaders )->getTable( ); 
        $orderByColumns = [ 0 => $leaderTblName . '.leaderId', 2 => $leaderTblName . '.leaderName', 3 => $leaderTblName . '.age', 4 => $leaderTblName . '.weight' ];

        $leaderObj = Leaders::where( 'is_archive', 0 )
                            ->orderBy( $orderByColumns[ $orderColumn ], $orderDir );

        $totalItems = $leaderObj->count( );

        if( $search ) { 
            $leaderObj->where( $leaderTblName . '.leaderName', 'like', "%{$search}%" );
        }

        $recordsFiltered = $leaderObj->count( );

        $leaders = $leaderObj->offset( $start )->limit( $perPage )->get( );
        
        $data = [ "draw" => $draw, "recordsTotal" => $totalItems, "recordsFiltered" => $recordsFiltered, "data" => [] ];
        if( $leaders->count( ) > 0 ) { 
            foreach( $leaders as $leader ) { 
                $data[ 'data' ][] = [ 
                                    ++$start,
                                    ( ( $leader->image ) ? '<span><img src="' . env('AWS_S3_BUCKET_URL').'/'.env('AWS_S3_LEADERS_IMAGE_BASEURL').'/'.$leader->image . '" alt="" /></span>' : 'NA' ),
                                    $leader->leaderName,
                                    $leader->age . ' Y',
                                    $leader->weight . ' pounds',
                                    '<div class="form-group">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input active_status category_active_' . Hashids::encode($leader->leaderId) . '" name="category_active_' . Hashids::encode($leader->leaderId) . '" onchange="return updateStatus( \'' . Hashids::encode($leader->leaderId) . '\', \'' . route( 'admin.leader_status_update' ) . '\', this.value );" id="yes" value="1" ' . ( $leader->status == 1 ? "checked":"" ) . '> Yes <i class="input-helper"></i></label>
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input active_status category_active_' . Hashids::encode($leader->leaderId) . '" name="category_active_' . Hashids::encode($leader->leaderId) . '" onchange="return updateStatus( \'' . Hashids::encode($leader->leaderId) . '\', \'' . route( 'admin.leader_status_update' ) . '\', this.value );" id="no" value="0" ' . ( $leader->status == 0 ? "checked":"" ) . '> No <i class="input-helper"></i></label>
                                        </div>                                          
                                    </div>',
                                    '<a href="' . route('admin.exercises', [ 'leaderId'=> Hashids::encode($leader->leaderId)  ] ) . '" class="add" title="Show Exercises"><i class="fa fa-list"></i></a>
                                    <a href="' . route('admin.edit_leader', [ 'id'=> Hashids::encode($leader->leaderId)  ] ) . '" class="add" title=""><i class="fa fa-edit"></i></a>
                                    <a href="' . route('admin.delete_leader', [ 'id'=> Hashids::encode($leader->leaderId)  ] ) . '" class="delete_record" ><i class="fa fa-trash"></i></a>'
                                ];
            }
        }

        return response( )->json($data);
    }

    public function create( ) { 
        $title = "Add Leader";
        return view('admin.leaders.add',['title'=>$title,'breadcrumbItem'=>'All Leaders' , 'breadcrumbItemLink'=>route('admin.leaders'),  'breadcrumbTitle'=>'Add Leader' ]);
    }

    public function store(Request $request)
    {
        $validator = $request->validate([
            'leaderName' => 'required',
            'age' => 'required|regex:/^[1-9][0-9]+/|not_in:0',
            'weight' => 'required',
            'location' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif'
        ]);
        
        $fileName = upload_file( $request, 'image' , env( 'AWS_S3_LEADERS_IMAGE_BASEURL' ) );
        
        $leader = new Leaders();
        $leader->leaderName= $request['leaderName'];
        $leader->age= $request['age'];
        $leader->weight= $request['weight'];

        /*$stone = number_format( $request['weight']/14, 2, '.', '' );
        $fractionStone = number_format( ( ( $stone*100 ) % 100 )/100, 2, '.', '' );
        $exactStone = $stone - $fractionStone;
        $lbs = number_format( $fractionStone * 14, 2, '.', '' );

        $kg = number_format( $request['weight']/2.2047244094, 2, '.', '' );*/
        $stone =  $request['weight']/14 ;
        $exactStone = ( int )$stone;
        $fractionStone = $stone - $exactStone;
        $lbs = round( number_format( $fractionStone * 14 ) );
        // $lbs = ( $lbs - ( int )$lbs > 0 )?$lbs:( int )$lbs;

        $kg = number_format( $request['weight']/2.2047244094, 1, '.', '' );
        $kg = ( $kg - ( int )$kg > 0 )?$kg:( int )$kg;

        $leader->convertedWeight= $exactStone . 'st ' . ( $lbs > 0?$lbs . 'lb':'' ) . " ( $kg kg )";

        $leader->location= $request['location'];
        if( $fileName )
            $leader->image= $fileName;

        if( !$leader->save() ) {
            return redirect()->route('admin.leaders')->with('error','Something went wrong, please try again later.');
        }
        
        \App\Models\LastUpdated::updateDateTime();
        return redirect()->route('admin.leaders')->with('success','Leader saved successfully.');
    }

    public function edit($id) { 

        if( !$id ){
            return redirect()->route('admin.leaders')->with('error','Some information is missing!');
        }

        $decryptedId = Hashids::decode($id);
        if( !$decryptedId ){
            return redirect()->route('admin.leaders')->with('error','Some information is missing!');
        }
        
        $title = "Edit Leader";
        $leaderInfo = Leaders::find( $decryptedId[ 0 ] );
        if( !$leaderInfo ){
            return redirect()->route('admin.leaders')->with('error','Leader not found!');
        }
        
        return view('admin.leaders.edit',[ 'title'=>$title,'breadcrumbItem'=>'All Leaders' , 'breadcrumbItemLink'=>route('admin.leaders'), 'breadcrumbTitle'=>'Edit Leader', 'leaderInfo'=>$leaderInfo ] );

    }

    public function update( $leaderId, Request $request)
    {
        if( !$leaderId ){
            return redirect()->route('admin.leaders')->with('error','Some information is missing!');
        }

        $leaderId = Hashids::decode($leaderId);
        if( !$leaderId ){
            return redirect()->route('admin.leaders')->with('error','Some information is missing!');
        }
        $leaderId = $leaderId[0];

        $validator = $request->validate([
            'leaderName' => 'required',
            'age' => 'required|regex:/^[1-9][0-9]+/|not_in:0',
            'weight' => 'required',
            'location' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif'
        ]);
            
        $leaderObj = Leaders::find($leaderId);
        if( $leaderObj->count() <= 0 ){ 
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }

        $fileName = upload_file( $request, 'image' , env( 'AWS_S3_LEADERS_IMAGE_BASEURL' ) );

        $leaderObj->leaderName= $request['leaderName'];
        $leaderObj->age= $request['age'];
        $leaderObj->weight= $request['weight'];

        /*$stone = number_format( $request['weight']/14, 2, '.', '' );
        $kg = number_format( $request['weight']/2.205, 2, '.', '' );

        $leaderObj->convertedWeight= $stone . 'st ' . $request['weight'] . "lb ( $kg kg )";*/

        $stone =  $request['weight']/14 ;
        $exactStone = ( int )$stone;
        $fractionStone = $stone - $exactStone;
        $lbs = round( number_format( $fractionStone * 14 ) );
        // $lbs = ( $lbs - ( int )$lbs > 0 )?$lbs:( int )$lbs;

        $kg = number_format( $request['weight']/2.2047244094, 1, '.', '' );
        $kg = ( $kg - ( int )$kg > 0 )?$kg:( int )$kg;

        $leaderObj->convertedWeight= $exactStone . 'st ' . ( $lbs > 0?$lbs . 'lb':'' ) . " ( $kg kg )";

        $leaderObj->location= $request['location'];
        if( $fileName ){ 

            if( $leaderObj->image ) Storage::disk('s3')->delete( env( 'AWS_S3_LEADERS_IMAGE_BASEURL' ) . '/' . $leaderObj->image );

            $leaderObj->image= $fileName;

        }

        if( !$leaderObj->save() ) {
            return redirect()->route('admin.edit_leader', Hashids::encode( $leaderId ) )->with('error','Something went wrong, please try again later.');
        }

        \App\Models\LastUpdated::updateDateTime();
        return redirect()->route('admin.leaders')->with('success','Leader updated successfully.');
    }

    public function getLeaders( Request $request ) { 

        $postedData = $request->all();
        $search = ( isset( $postedData->search ) && $postedData->search )?$postedData->search:'';
        $leaderObj = Leaders::where( 'is_archive', 0 )
                            ->where( 'leaderName', 'LIKE',  $search );
        $leaders = $leaderObj->offset( 0 )->limit( 2 )->get( );

        $returnData = [];
        foreach( $leaders as $leader ){
            $returnData[ ] = [ 'value' =>$leader->leaderId, 'label' => $leader->leaderName ];
        }

        return response()->json( $returnData );

    }

    public function destroy( $id ) { 
        $decryptedId = Hashids::decode($id);
        if( !$decryptedId ){
            return redirect()->route('admin.leaders')->with('error','Some information is missing!');
        }
            
        $leaderInfo = Leaders::find( $decryptedId[0] );
        if( $leaderInfo->count() <= 0 ) { 
            return redirect()->route('admin.leaders')->with('error','Some information is missing!');
        }

        $leaderInfo->is_archive = 1;
        if( !$leaderInfo->save( ) ) { 
            return redirect()->route('admin.leaders')->with('error','Some information is missing!');
        }

        \App\Models\LastUpdated::updateDateTime();
        return redirect()->route('admin.leaders')->with('success','Leader deleted successfully.');

    }

    public function deleteImage ( $leaderId ) { 
        if( !$leaderId ){
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        $leaderId = Hashids::decode($leaderId);
        if( !$leaderId ){
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        $leaderId = $leaderId[0];

        $obj = Leaders::find( $leaderId );
        if( $obj->count() <= 0 ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Selected leader not found'
            ]);
        }

        if( !$obj->image ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Selected leader image not found'
            ]);
        }

        $isDeleted = Storage::disk('s3')->delete( env( 'AWS_S3_LEADERS_IMAGE_BASEURL' ) . '/' . $obj->image );
        if( !$isDeleted ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Unable to delete image, please try again later!'
            ]);
        }

        $obj->image = '';
        
        if( !$obj->save( ) ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Unable to delete image, please try again later!'
            ]);
        }
        
        \App\Models\LastUpdated::updateDateTime();
        return response()->json([
            'status' => 1,
            'msg' => 'Leader image deleted!.'
        ]);

    }

    // function to update the status (Active/Inactive)
    public function leaderStatusUpdate(Request $request)
    {
        $request_data = $request->all();
        if( !$request->id ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        $request->id = Hashids::decode( $request->id )[0];

        if( ! ( isset( $request->id ) && $request->id > 0 ) ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        $obj = Leaders::find($request->id);
        if( $obj->count() <= 0 ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Selected leader not found'
            ]);
        }

        $obj->status = $obj->status ? 0 : 1 ;
        
        if( !$obj->save( ) ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Unable to update status, please try again later.'
            ]);
        }
        
        \App\Models\LastUpdated::updateDateTime();
        return response()->json([
            'status' => 1,
            'msg' => 'Status updated successfully.'
        ]);
    }
}
