<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\CommonController;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Config;
use Mail;
use Hash;
use Vinkla\Hashids\Facades\Hashids;

use Auth;
use App\Models\User;
use App\Models\Emails;
use App\Models\Shopping_list;
use Response;

class ShoppingListController extends Controller
{
    public function __construct() {
        $this->middleware('guest');
      }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        // $all_shopping = Shopping_list::all();  
        return view('admin.shopping_list.shopping_list',[ 'title'=>'Manage shopping list','breadcrumbItem'=>'Shopping List', 'breadcrumbItemLink'=>route('admin.shoppinglist'), 'breadcrumbTitle'=>'List Shopping' ] );
    }

    public function list( Request $request) { 

        $requestedData  = $request->all();

        $draw           = ( isset( $requestedData[ 'draw' ] ) && $requestedData[ 'draw' ] > 0 )?$requestedData[ 'draw' ]:1;
        $start          = ( isset( $requestedData[ 'start' ] ) && $requestedData[ 'start' ] > 0 )?$requestedData[ 'start' ]:0;
        $perPage        = ( isset( $requestedData[ 'length' ] ) && $requestedData[ 'length' ] > 0 )?$requestedData[ 'length' ]:10;
        $search         = ( isset( $requestedData[ 'search' ] ) && isset( $requestedData[ 'search' ][ 'value' ] ) )?$requestedData[ 'search' ][ 'value' ]:'';
        
        $orderColumn    = ( isset( $requestedData[ 'order' ] ) && isset( $requestedData[ 'order' ][ 0 ] ) && isset( $requestedData[ 'order' ][ 0 ][ 'column' ] ) )?$requestedData[ 'order' ][ 0 ][ 'column' ]:0;
        $orderDir        = ( isset( $requestedData[ 'order' ] ) && isset( $requestedData[ 'order' ][ 0 ] ) && isset( $requestedData[ 'order' ][ 0 ][ 'dir' ] ) )?$requestedData[ 'order' ][ 0 ][ 'dir' ]:'ASC';

        $shoppingTblName        = with( new Shopping_list )->getTable( ); 
        $orderByColumns = [ $shoppingTblName . '.orderBy', $shoppingTblName . '.weekNumber' ];

        $shoppingObj = Shopping_list::where( $shoppingTblName . '.isArchive', 0 )->orderBy( $orderByColumns[ $orderColumn ], $orderDir );

        $totalItems = $shoppingObj->count( );

        if( $search ) { 
            $shoppingObj->where( $shoppingTblName . '.pdfFile', 'like', "%{$search}%" );
        }

        $recordsFiltered = $shoppingObj->count( );

        $shoppingItems = $shoppingObj->offset( $start )->limit( $perPage )->get( );
        
        $data = [ "draw" => $draw, "recordsTotal" => $totalItems, "recordsFiltered" => $recordsFiltered, "data" => [] ];
        if( $shoppingItems->count( ) > 0 ) { 
            foreach( $shoppingItems as $shopping ) { 
                $data[ 'data' ][] = [  
                                    ++$start,
                                    $shopping->weekNumber + 1,
                                    
                                    ( $shopping->pdfFile )?'<i class="mr-2"><a target="_blank" href="' . env('AWS_S3_BUCKET_URL').'/'.env('AWS_S3_SHOPPING_LIST_BASEURL').'/'.$shopping->pdfFile . '"><img style="width:30px;" src="' . asset("images/PDF_icon.svg") . '" width="15px" alt="'.$shopping->pdfFile . '" title="'.$shopping->pdfFile . '"/></a></i>':'NA',
                                    
                                    ( $shopping->pdfFile_veg )?'<i class="mr-2"><a target="_blank" href="' . env('AWS_S3_BUCKET_URL').'/'.env('AWS_S3_SHOPPING_LIST_BASEURL').'/'.$shopping->pdfFile_veg . '"><img style="width:30px;" src="' . asset("images/PDF_icon.svg") . '" width="15px" alt="'.$shopping->pdfFile_veg . '" title="'.$shopping->pdfFile_veg . '"/></a></i>':'NA',
                                    
                                    '<div class="form-group">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input active_status shopping_active_' . $shopping->id . '" name="shopping_active_' . $shopping->id . '" onchange="return updateStatus( ' . $shopping->id . ', \'' . route( 'admin.shopping_status_update' ) . '\', this.value );" id="yes" value="1" ' . ( $shopping->status == 1 ? "checked":"" ) . '> Yes <i class="input-helper"></i></label>
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input active_status shopping_active_' . $shopping->id . '" name="shopping_active_' . $shopping->id . '" onchange="return updateStatus( ' . $shopping->id . ', \'' . route( 'admin.shopping_status_update' ) . '\', this.value );" id="no" value="0" ' . ( $shopping->status == 0 ? "checked":"" ) . '> No <i class="input-helper"></i></label>
                                        </div>                                          
                                    </div>',
                                    '<a href="' . route('admin.editshopping', [ 'id'=> Hashids::encode($shopping->id)  ] ) . '" class="add" title=""><i class="fa fa-edit"></i></a><a href="' . route('admin.deleteshopping', [ 'id'=> Hashids::encode($shopping->id)  ] ) . '" class="delete_record" ><i class="fa fa-trash"></i></a>',
                                    Hashids::encode($shopping->id),
                                    $shopping->orderBy 
                                ];
            }
        }

        return response( )->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Add Shopping";
        return view('admin.shopping_list.shopping_list_add',[ 'title'=>$title,'breadcrumbItem'=>'Shopping List' ,'breadcrumbItemLink'=>route('admin.shoppinglist'), 'breadcrumbTitle'=>'Add Shopping' ] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request_data = $request->all();
        // print_r($request_data);  die();

        $request->validate([
            'weekNumber' => 'required',
            'pdfFile' => 'required|mimes:pdf',
        ]);
        
        
        $file_name_with_time_prefix = upload_file($request, 'pdfFile' , env('AWS_S3_SHOPPING_LIST_BASEURL') );

        $file_name_with_time_prefix_veg = upload_file($request, 'pdfFile_veg' , env('AWS_S3_SHOPPING_LIST_BASEURL') );
        
        $shoppingExist = Shopping_list::where('weekNumber', $request['weekNumber'] )->first();
        if( $shoppingExist ) { 
            return redirect()->route('admin.shoppinglist')->with('warning','PDF file already uploaded for this day !');
        }
        $shopping = new Shopping_list();
        $shopping->weekNumber= $request['weekNumber'];
        if($file_name_with_time_prefix)
        $shopping->pdfFile = $file_name_with_time_prefix;

        if($file_name_with_time_prefix_veg)
        $shopping->pdfFile_veg = $file_name_with_time_prefix_veg;
        
        if( $shopping->save() )
        {
            $shopping->orderBy = $shopping->id;
            $shopping->save();
            \App\Models\LastUpdated::updateDateTime( true );
            return redirect()->route('admin.shoppinglist')->with('success','Shopping saved successfully.');
        }
        
        return redirect()->route('admin.shoppinglist')->with('error','Shopping not saved. Something went wrong !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        
        $decryptedId = Hashids::decode($id);

        if( !( isset($decryptedId[0]) && $decryptedId[0] > 0 ) )
        return redirect()->route('admin.shoppinglist')->with('error','Invalid Request id !');
        
        $title = "Edit Shopping";
        $shopping_detail = Shopping_list::find($decryptedId[0]);     
        // print_r($all_partners);die;
        return view('admin.shopping_list.shopping_list_edit',[ 'title'=>$title,'breadcrumbItem'=>'Shopping' , 'breadcrumbItemLink'=>route('admin.shoppinglist'), 'breadcrumbTitle'=>'Edit Shopping','shopping_detail'=>$shopping_detail ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request_data = $request->all();
        
        $request->validate([
            'pdfFile' => 'mimes:pdf',
        ]);
        if( !( isset($request->shoppingid) && $request->shoppingid > 0 ) )
        return redirect()->route('admin.shoppinglist')->with('error','Invalid Request id !!');

        $shopping = Shopping_list::find($request->shoppingid);
        $file_name_with_time_prefix = upload_file($request, 'pdfFile' , env('AWS_S3_SHOPPING_LIST_BASEURL') );
        $file_name_with_time_prefix_veg = upload_file($request, 'pdfFile_veg' , env('AWS_S3_SHOPPING_LIST_BASEURL') );
        if($file_name_with_time_prefix)
            $shopping->pdfFile= $file_name_with_time_prefix;

        if($file_name_with_time_prefix_veg)
        $shopping->pdfFile_veg = $file_name_with_time_prefix_veg;
        
        if( !$shopping->save() )
        return redirect()->route('admin.shoppinglist')->with('error','Shopping not updated. Something went wrong !!');
        
        \App\Models\LastUpdated::updateDateTime( true );
        return redirect()->route('admin.shoppinglist')->with('success','Shopping updated successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decryptedId = Hashids::decode($id);
        if( !( isset($decryptedId[0]) && $decryptedId[0] > 0 ) )
        return redirect()->route('admin.shoppinglist')->with('error','Invalid Request id !');
        
        $shoppingInfo = Shopping_list::find( $decryptedId[0] );
        if( $shoppingInfo->count() <= 0 ) { 
            return redirect()->route('admin.shoppinglist')->with('error','Some information is missing!');
        }
        
        $shoppingInfo->isArchive = 1 ;
        if( !$shoppingInfo->save() ){ 
            return redirect()->route('admin.shoppinglist')->with('error','Some information is missing!');
        }

        \App\Models\LastUpdated::updateDateTime( true );
        return redirect()->route('admin.shoppinglist')->with('success','Shopping deleted successfully.');
        
    }

    // function to update the status (Active/Inactive)
    public function shopping_status_update(Request $request)
    {
        $request_data = $request->all();
        if( ! ( isset( $request->id ) && $request->id > 0 ) ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        // echo "<pre>";print_r($request_data);die;
        $shopping = Shopping_list::find($request->id);
        if( $shopping->count() <= 0 ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Selected Shopping Not Found'
            ]);
        }

        $shopping->status = $shopping->status ? 0 : 1 ;
        
        if( !$shopping->save( ) ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Unable to update status, please try again later.'
            ]);
        }
        
        \App\Models\LastUpdated::updateDateTime( true );
        return response()->json([
            'status' => 1,
            'msg' => 'Status updated successfully.'
        ]);
    }

    public function updateShoppingItemReorder(Request $request)
    {
        $request_data = $request->all();
        // echo "<pre>";print_r( $request_data );  die;
        
        $validator = $request->validate([
                'replace_ids' => 'required'
            ]);

        if( count( $request_data['replace_ids'] ) <= 1 ) { 
            return response::json([ 'errors' => 'Unable to update order, please try again later.' ]);
        }

        $positions = $ids = [];
        foreach( array_keys( $request_data['replace_ids'] ) as $id ) { 
            $ids[] = Hashids::decode( ( string )$id )[0];
            $positions[ Hashids::decode( ( string )$id )[0] ] = $request_data['replace_ids'][ $id ];
        }

        $items = Shopping_list::whereIn( 'id', $ids )->get();
        if( $items->count() !=  count( $request_data['replace_ids'] ) ) { 
            return response::json([ 'errors' => 'Unable to update order, please try again later.' ]);
        }

        foreach( $items as $item ){ 
            $item->orderBy = $positions[ $item->id ];
            $item->save();
        }

        \App\Models\LastUpdated::updateDateTime( true );
        return response::json([ 'success' => 'Order updated successfully' ]);

    }
}
