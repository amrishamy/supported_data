<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Models\User;
use App\Models\Emails;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
//use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Redirect;
use Config;
use Mail;

use Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }


    /**
     * Show the application normal login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function loginForm()
    {
        $user_id = session('user_id');$user_type = session('user_type');
        if(!empty($user_id) && $user_type=='admin'){
        //    return redirect()->route('admin.dashboard'); 
           return redirect()->route('admin.food_items'); 
        }

        $title = 'Login';
        return view('admin.login', [
            'title' => $title
        ]);
    }

    public function forgotForm()
    {
        $user_id = session('user_id');$user_type = session('user_type');
        if(!empty($user_id) && $user_type=='admin'){
        //    return redirect()->route('admin.dashboard'); 
           return redirect()->route('admin.food_items'); 
        }

        $title = 'Forgot Password';
        return view('admin.forgot_password', [
            'title' => $title
        ]);
    }


    /**
     * Login user in the application
     *
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request)
    {
        $request_data = $request->all();

        //print_r($request_data);exit;

        $validatedData = $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        /** $user */
        $user = Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role_id' => 1]);  
        //print_r($user); die;
        if ($user) {
            $user = User::where(['email' => $request->email])->first();
            $request->session()->put('user_id', $user->id);$request->session()->put('user_type', 'admin');
            $request->session()->put('name', $user->name);
            // return redirect()->route('admin.dashboard');
            return redirect()->route('admin.food_items');
        }else {
            return redirect()->route('admin.login')->with('error','Invalid username or password.');
        } 
    }


    public function admin_forgot_password(Request $request)
    {
        $user = User::where(['email' => $request->forgot_email, "role_id" => 1])->first();

        $forgot_email_template = Emails::find('2')->toArray();
        /*echo '<pre>';
        print_R($user );
        die;*/
        

        if($user){
            $forgotToken = str_random(20);
            $userUpdate = User::where('email', $request->forgot_email)->update(['forgotToken' => $forgotToken, 'updated_at' => Carbon::now()]);
           

            $emailTemplateDecode = html_entity_decode($forgot_email_template['email_template']);
            $email_body = str_replace("##name##", $user['name'], $emailTemplateDecode);
            $email_body = str_replace("##verification_code##", $forgotToken, $email_body);
           
            $to = $request->forgot_email; 
            $subject = $forgot_email_template['subject'];
           
            $data['name'] =  $user->name;
            $data['verification_code'] =  $forgotToken;
            Mail::send('emails.admin_forgot_password',$data, function($message) use($to, $subject, $email_body) {
                $message->setBody($email_body);
                $message->from( env('MAIL_FROM_ADDRESS') , 'Operation Transformation Admin');
                $message->to($to);
                $message->subject($subject);
            });

            
           

            return redirect()->route('admin.login')->with(['flipped_class'=>'flipped','forgot_success'=>'Reset password link sent.']);
        }else{
            return redirect()->route('admin.login')->with(['flipped_class'=>'flipped','forgot_error'=>'Email id does not exist.']);
        }


    }


    //Hash::make($data['password'])
    /*
     * function to show reset password view
     */
    public function showResetPassword(Request $request, $token)
    {
        $request->session()->put('token', $token);
        $title = 'Reset Password';
        return view('admin.resetPassword', [
            'title' => $title
        ]);
    }



    /*
     * function to reset password
     * parameters : new_password , token
     */
    public function updatePassword(Request $request)
    {
        $token = $request->session()->get('token');

        $validatedData = $this->validate($request, [
            'password' => 'required|min:6|required_with:cpassword|same:cpassword',
            'cpassword' => 'min:6'
        ]);


        $chkUser = User::where('forgotToken', $token)->first();
        //dd($chkUser);

        if($chkUser) {
            $passwordUpdate = User::where('id', $chkUser->id)->update(['password' => Hash::make($request->password), 'forgotToken' => '', 'updated_at' => Carbon::now()]);
            if ($passwordUpdate) {
               return redirect()->route('admin.resetpassword',[$token])->with(['success'=>'Success']);
            } else {
                return redirect()->route('admin.resetpassword',[$token])->with(['error'=>'Error occured while updating password.']);
            }
        } else {
            return redirect()->route('admin.resetpassword',[$token])->with(['error'=>'Update password link has been expired.']);
        }

    }



    public function logout(Request $request){
        Session::flush();
        return Redirect::route('admin.login');
    }



}
