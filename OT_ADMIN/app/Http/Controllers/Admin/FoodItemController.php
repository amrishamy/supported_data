<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\CommonController;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Config;
use Mail;
use Hash;
use Vinkla\Hashids\Facades\Hashids;
use Illuminate\Support\Facades\Input;

use Auth;
use App\Models\User;
use App\Models\Emails;
use App\Models\Food_item;
use App\Models\Food_category;
use App\Models\Category_item;

use Response;

class FoodItemController extends Controller
{
    public $food_item_folder_path = "Recipes";
    public function index( $categoryId = 0 ) { 
        $categoryId = ( isset( $categoryId ) && $categoryId )?Hashids::decode( $categoryId )[0]:0;
        $category = [];
        if( $categoryId ) { 
            $category = Food_category::where( 'isArchive', 0 )->find( $categoryId );
            if( !$category ) return redirect()->route('admin.categorylist')->with('error','Category not found!');
        }

        $title = $category?'Manage Food Items For Catgory: ' . $category->categoryName:'Manage Food Items';
        $breadcrumbItem = $category?'All Categories':'All Food Items';
        $breadcrumbItemLink = $category?'admin.categorylist':'admin.food_items';

        return view('admin.food_items.index',['title' => $title,'breadcrumbItem' => $breadcrumbItem, 'breadcrumbItemLink'=>route( $breadcrumbItemLink ), 'breadcrumbTitle'=>'All Food items', 'category' => $category ]);
    }

    public function list( $categoryId, Request $request) { 

        $requestedData  = $request->all();

        $draw           = ( isset( $requestedData[ 'draw' ] ) && $requestedData[ 'draw' ] > 0 )?$requestedData[ 'draw' ]:1;
        $start          = ( isset( $requestedData[ 'start' ] ) && $requestedData[ 'start' ] > 0 )?$requestedData[ 'start' ]:0;
        $perPage        = ( isset( $requestedData[ 'length' ] ) && $requestedData[ 'length' ] > 0 )?$requestedData[ 'length' ]:10;
        $search         = ( isset( $requestedData[ 'search' ] ) && isset( $requestedData[ 'search' ][ 'value' ] ) )?$requestedData[ 'search' ][ 'value' ]:'';
        
        $orderColumn    = ( isset( $requestedData[ 'order' ] ) && isset( $requestedData[ 'order' ][ 0 ] ) && isset( $requestedData[ 'order' ][ 0 ][ 'column' ] ) )?$requestedData[ 'order' ][ 0 ][ 'column' ]:0;
        $orderDir        = ( isset( $requestedData[ 'order' ] ) && isset( $requestedData[ 'order' ][ 0 ] ) && isset( $requestedData[ 'order' ][ 0 ][ 'dir' ] ) )?$requestedData[ 'order' ][ 0 ][ 'dir' ]:'DESC';


        $categoryId = ( isset( $categoryId ) && $categoryId )?Hashids::decode( $categoryId )[0]:0;
        $category = [];
        if( $categoryId ) { 
            $category = Food_category::where( 'isArchive', 0 )->find( $categoryId );
            if( !$category ) {
                $data = [ "draw" => $draw, "recordsTotal" => 0, "recordsFiltered" => 0, "data" => [] ];
                return response( )->json($data);
            }
        }

        $foodItemTblName        = with( new Food_item )->getTable( ); 
        $categoryTblName        = with( new Food_category )->getTable( ); 
        $categoryLinkTblName    = with( new Category_item )->getTable( ); 

        $orderByColumns = [ $categoryLinkTblName . '.orderBy', $foodItemTblName . '.itemName' ];

        // \DB::enableQueryLog();
        $foodItemObj = Category_item::join( $categoryTblName, $categoryLinkTblName . '.categoryId', '=', $categoryTblName . '.id' )
                    ->join( $foodItemTblName, $categoryLinkTblName . '.itemId', '=', $foodItemTblName . '.id' )

                    ->leftJoin( $foodItemTblName . ' as fi2', function( $join ) use ( $foodItemTblName ) { 
                        $join->on( $foodItemTblName . '.veg_item_id', '=', 'fi2.id' );
                        $join->on( 'fi2.isArchive', '=', \DB::raw( '0' ) );
                    })

                    ->select( [ \DB::raw( 'DISTINCT ' . $foodItemTblName . '.id' ), $foodItemTblName . '.itemName', $foodItemTblName . '.food_type', $foodItemTblName . '.itemImage', $foodItemTblName . '.calories', $foodItemTblName . '.status', $foodItemTblName . '.orderBy', \DB::raw( $categoryLinkTblName . '.id as cat_link_id' ), \DB::raw( $categoryLinkTblName . '.orderBy as cat_orderBy' ), \DB::raw( 'GROUP_CONCAT( DISTINCT ' . $categoryTblName . '.categoryName SEPARATOR \', \' ) as cats_name' ), \DB::raw( 'fi2.itemName as veg_item_name' ) ] )
                    ->where( $categoryTblName . '.isArchive', 0 )
                    ->where( $foodItemTblName . '.isArchive', 0 )
                    /* ->whereNotIn( $categoryLinkTblName . '.itemId', function( $query ) use( $foodItemTblName ) { 
                                                $query->select( $foodItemTblName . '.veg_item_id')
                                                        ->from( $foodItemTblName )
                                                        ->where( $foodItemTblName . '.isVegAvailable', 1);
                                        } )
                    ->whereNotIn( $foodItemTblName . '.id', function( $query ) use( $foodItemTblName ) { 
                                                $query->select( $foodItemTblName . '.veg_item_id')
                                                        ->from( $foodItemTblName )
                                                        ->where( $foodItemTblName . '.isVegAvailable', 1);
                                        } ) */
                    ->orderBy( $orderByColumns[ $orderColumn ], $orderDir );

        if( $category ) $foodItemObj->where( $categoryTblName . '.id', $category->id );
        $totalItems = $foodItemObj->count( \DB::raw( 'DISTINCT ' . $foodItemTblName . '.id' ) );

        // dd( \DB::getQueryLog() );

        if( $search ) { 
            $foodItemObj->where( $foodItemTblName . '.itemName', 'like', "%{$search}%" );
            $foodItemObj->orWhere( 'fi2.itemName', 'like', "%{$search}%" );
        }

        $recordsFiltered = $foodItemObj->count( \DB::raw( 'DISTINCT ' . $foodItemTblName . '.id' ) );

        $foodItems = $foodItemObj->groupBy( $foodItemTblName . '.id' )->offset( $start )->limit( $perPage )->get( );
        
        $data = [ "draw" => $draw, "recordsTotal" => $totalItems, "recordsFiltered" => $recordsFiltered, "data" => [] ];
        if( $foodItems->count( ) > 0 ) { 
            foreach( $foodItems as $food ) { 
                $addData = [  
                            ++$start,
                            ( $food->food_type == 1 ?'':'Non veg:') . $food->itemName  . ( $food->veg_item_name?'<br/>Veg: ' . $food->veg_item_name:'' ),
                            '<span><img src="' . env('AWS_S3_BUCKET_URL').'/'.env('AWS_S3_FOOD_CATEGORYIMAGE_BASEURL').'/'.$food->itemImage . '" alt="" /></span>',
                            $food->cats_name,
                            $food->calories,
                            '<div class="form-group">
                                <div class="form-check">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input active_status food_active_' . $food->id . '" name="food_active_' . $food->id . '" onchange="return updateStatus( ' . $food->id . ', \'' . route( 'admin.food_status_update' ) . '\', this.value );" id="yes" value="1" ' . ( $food->status == 1 ? "checked":"" ) . '> Yes <i class="input-helper"></i></label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                    <input type="radio" class="form-check-input active_status food_active_' . $food->id . '" name="food_active_' . $food->id . '" onchange="return updateStatus( ' . $food->id . ', \'' . route( 'admin.food_status_update' ) . '\', this.value );" id="no" value="0" ' . ( $food->status == 0 ? "checked":"" ) . '> No <i class="input-helper"></i></label>
                                </div>                                          
                            </div>',
                            '<a href="' . route('admin.editfood', [ 'id'=> Hashids::encode($food->id)  ] ) . '" class="add" title=""><i class="fa fa-edit"></i></a><a href="' . route('admin.deletefood', [ 'id'=> Hashids::encode($food->id)  ] ) . '" class="delete_record" ><i class="fa fa-trash"></i></a>',
                            ( $category )?Hashids::encode($food->cat_link_id):Hashids::encode($food->id),
                            ( $category )?$food->cat_orderBy:$food->orderBy,
                        ];
                if( $category ) array_splice( $addData, 3, 1 );
                $data[ 'data' ][] = $addData;
            }
        }

        return response( )->json($data);
    }

    public function getFoodItems( Request $request ) { 

        $postedData = $request->all();
        $search = ( isset( $postedData['search'] ) && $postedData['search'] )?$postedData['search']:'';
        $selected = ( isset( $postedData['selected'] ) && $postedData['selected'] )?$postedData['selected']:false;

        $foodItemTblName        = with( new Food_item )->getTable( ); 
        $categoryTblName        = with( new Food_category )->getTable( ); 
        $categoryLinkTblName    = with( new Category_item )->getTable( ); 

        if( isset( $postedData[ 'veg' ] ) && $postedData[ 'veg' ] == 'only' ) { 
            $fiObj = Food_item::select( [ \DB::raw( 'DISTINCT ' . $foodItemTblName . '.id' ), $foodItemTblName . '.itemName' ] )
                    ->where( $foodItemTblName . '.food_type', 1 )
                    ->where( $foodItemTblName . '.isArchive', 0 )
                    ->orderBy( $foodItemTblName . '.itemName', 'DESC' );
        } else { 
            $fiObj = Category_item::join( $categoryTblName, $categoryLinkTblName . '.categoryId', '=', $categoryTblName . '.id' )
                        ->join( $foodItemTblName, $categoryLinkTblName . '.itemId', '=', $foodItemTblName . '.id' )
                        ->select( [ \DB::raw( 'DISTINCT ' . $foodItemTblName . '.id' ), $foodItemTblName . '.itemName' ] )
                        ->where( $categoryTblName . '.isMealOfTheDay', 1 )
                        ->where( $categoryTblName . '.isArchive', 0 )
                        ->where( $foodItemTblName . '.isArchive', 0 )
                        ->orderBy( $foodItemTblName . '.itemName', 'DESC' );
        }
        if( $search ) { 
            $fiObj->where( $foodItemTblName . '.itemName', 'LIKE',  "%$search%" );
        }
        if( $selected ) { 
            $fiObj->where( $foodItemTblName . '.id', '!=',  $selected );
        }

        $foodItems = $fiObj->orderBy( 'itemName' )->offset( 0 )->limit( 200 )->get( );

        $html = '<div class="text-center">Food Items Not Found</div>';
        if( $foodItems->count() > 0 ) { 
            $html = '';
            foreach( $foodItems as $foodItem ){
                $returnData[ ] = [ 'value' =>$foodItem->id, 'label' => $foodItem->itemName ];

                $html .= '<div class="col-md-4">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input daily_meal_selection" name="daily_meal_selection" value="' . $foodItem->id . '" title="' . $foodItem->itemName . '" />' . $foodItem->itemName . '<i class="input-helper"></i></label>
                        </div>
                    </div>';
            }
        }

        return response()->json( [ 'html' => $html ] );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Add Food item";
        $all_categories = $this->get_categories();
        $comboCatObj = $this->getComboCategory( );
        $comboCatId = $comboCatObj->id;
        return view('admin.food_items.food_item_add',['title'=>$title,'breadcrumbItem'=>'Food' , 'breadcrumbItemLink'=>route('admin.food_items'),  'breadcrumbTitle'=>'Add Food item', 'all_categories'=>$all_categories, 'comboCatId'=>$comboCatId ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) { 

        // $comboCatObj = $this->getComboCategory( );
        // $comboCatId = $comboCatObj->id;

        $request_data = $request->all();
        // echo "<pre>";print_r( $request_data );  die;
       
        $validator = $request->validate([
            'itemName' => 'required',
            'food_category' => 'required',
            'serveName' => 'required',
            'itemIngredients' => 'required',
            'itemMethod' => 'required',
            'calories' => 'required',
            'userType' => 'required',
            'food_type' => 'required'
        ]);

        // if( $comboCatId && in_array( ( string )$comboCatId, $request['food_category'] ) ) { 
        //     $request->merge( [ 'food_category' => [ $comboCatId ] ] );
        // }
        
        $file_name_with_time_prefix = upload_file($request, 'itemImage' , env('AWS_S3_FOOD_ITEMIMAGE_BASEURL')."/".$this->food_item_folder_path );
        
        $food = new Food_item();
        $food->itemName= $request['itemName'];
        $food->serveName= $request['serveName'];
        $food->instructionalVideo= $request['instructionalVideo'] ? $request['instructionalVideo'] : '';
        $food->prepareAhead= $request['prepareAhead'] ? $request['prepareAhead'] :'' ;
        $food->calories= $request['calories'];
        $food->userType= $request['userType'];

        $food->itemIngredients= $request['itemIngredients'];
        $food->itemMethod= $request['itemMethod'];
        $food->additionalInfo= $request['additionalInfo'] ? $request['additionalInfo'] : '';
        $food->isShutDownWeek = $request['isShutDownWeek'] ? 1:0 ;
        $food->food_type= ($request['food_type']=='veg') ? 1: 0 ;

        $food->veg_item_id= 0 ;

        if($file_name_with_time_prefix)
            $food->itemImage= $this->food_item_folder_path."/".$file_name_with_time_prefix;

        if( !$food->save() ) {
            return redirect()->route('admin.food_items')->with('error','Something went wrong, please try again later.');
        }

        $food->orderBy = $food->id;
        $food->save();

        foreach( $request['food_category'] as $cat )
        {
            $Category_item = new Category_item(); 
            $Category_item->categoryId= $cat ;
            $Category_item->itemId= $food->id ;
            $Category_item->orderBy= 0;
            $Category_item->duplicate = 0 ;
            $Category_item->save() ;

            $Category_item->orderBy = $Category_item->id;
            $Category_item->save();
        }
        
        if( $request['food_type'] == 'nonveg' && isset($request['isVegAvailable']) )
        {
            if( $request['add_veg'] == 'existing' )
            {
                $exist_veg_id = $request['itemId'] ;
                $food_item = Food_item::find( $food->id );
                $food_item->veg_item_id = $exist_veg_id ;
                $food_item->isVegAvailable = 1 ;
                $food_item->save();
            }else
            {
                $food_veg = new Food_item();

                $file_name_with_time_prefix = upload_file($request, 'itemImage_veg' , env('AWS_S3_FOOD_ITEMIMAGE_BASEURL') );

                $food_veg->itemName= $request['itemName_veg'];
                $food_veg->serveName= $request['serveName_veg'];
                $food_veg->instructionalVideo= $request['instructionalVideo_veg'] ? $request['instructionalVideo_veg'] : '';
                $food_veg->prepareAhead= $request['prepareAhead_veg'] ? $request['prepareAhead_veg']: '';
                $food_veg->calories= $request['calories_veg'];
                $food_veg->userType= $request['userType_veg'];

                $food_veg->itemIngredients= $request['itemIngredients_veg'];
                $food_veg->itemMethod= $request['itemMethod_veg'];
                $food_veg->additionalInfo= $request['additionalInfo_veg'] ? $request['additionalInfo_veg']: '';

                $food_veg->veg_item_id = 0;

                if($file_name_with_time_prefix)
                    $food_veg->itemImage= $file_name_with_time_prefix;

                if( $food_veg->save() ) { 

                    $food_veg->orderBy = $food_veg->id;
                    $food_veg->save();

                    $food_item = Food_item::find( $food->id );
                    $food_item->veg_item_id = $food_veg->id ;
                    $food_item->isVegAvailable = 1 ;
                    $food_item->save();

                    foreach( $request['food_category'] as $cat )
                    {
                        $Category_item = new Category_item(); 
                        $Category_item->categoryId = $cat ;
                        $Category_item->itemId = $food_veg->id ;
                        $Category_item->orderBy = $food_veg->id ;
                        $Category_item->duplicate = 0 ;
                        $Category_item->save() ;
                        
                        $Category_item->orderBy = $Category_item->id;
                        $Category_item->save();
                    }
                }   
            }
            

        }
        
        \App\Models\LastUpdated::updateDateTime();
        return redirect()->route('admin.food_items')->with('success','Food saved successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $decryptedId = Hashids::decode($id);
        
        $title = "Edit Food item";
        $all_categories = $this->get_categories();
        $food_detail = Food_item::find($decryptedId[0]);
        // echo "<pre>";print_r($food_detail->vegItem );die;
        $item_selected_categories = Category_item::select('categoryId')->where('itemId',$food_detail->id)->distinct()->pluck('categoryId')->toArray();

        $comboCatObj = $this->getComboCategory( );
        $comboCatId = $comboCatObj->id;

        $isAssociatedVegItem = Food_item::where( 'veg_item_id', $decryptedId[0] )->count( );
        
        // print_r($item_selected_categories);die;
        return view('admin.food_items.food_item_edit',[ 'title'=>$title,'breadcrumbItem'=>'All Foods' , 'breadcrumbItemLink'=>route('admin.food_items'), 'breadcrumbTitle'=>'Edit Food item','food_detail'=>$food_detail,'all_categories'=>$all_categories,'item_selected_categories'=>$item_selected_categories, 'comboCatId' => $comboCatId, 'isAssociatedVegItem' => $isAssociatedVegItem ] );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) { 

        // $comboCatObj = $this->getComboCategory( );
        // $comboCatId = $comboCatObj->id;

        $request_data = $request->all();
        // echo "<pre>";print_r( $request_data );  die;
        
        $validator = $request->validate([
            'itemName' => 'required',
            'food_category' => 'required',
            'serveName' => 'required',
            'itemIngredients' => 'required',
            'itemMethod' => 'required',
            'calories' => 'required',
            'userType' => 'required',
            'food_type' => 'required'
            ]);

        // if( $comboCatId && in_array( ( string )$comboCatId, $request['food_category'] ) ) { 
        //     $request->merge( [ 'food_category' => [ $comboCatId ] ] );
        // }
            
        $food = Food_item::find($request->foodid);
        $file_name_with_time_prefix = upload_file($request, 'itemImage' , env('AWS_S3_FOOD_ITEMIMAGE_BASEURL')."/".$this->food_item_folder_path );
        
        $food->itemName= $request['itemName'];
        $food->serveName= $request['serveName'];
        $food->instructionalVideo= $request['instructionalVideo'] ? $request['instructionalVideo'] : '';
        $food->prepareAhead= $request['prepareAhead'] ? $request['prepareAhead'] :'' ;
        $food->calories= $request['calories'];
        $food->userType= $request['userType'];

        $food->itemIngredients= $request['itemIngredients'];
        $food->itemMethod= $request['itemMethod'];
        $food->additionalInfo= $request['additionalInfo'] ? $request['additionalInfo'] : '';
        $food->isShutDownWeek = $request['isShutDownWeek'] ? 1:0 ;
        $food->food_type= ($request['food_type']=='veg') ? 1: 0 ;

        // $food->veg_item_id= 0 ;

        if($file_name_with_time_prefix)
            $food->itemImage= $this->food_item_folder_path."/".$file_name_with_time_prefix;

        if( $food->save() ) {  
            $existingCatIds = Category_item::where( 'itemId', $food->id )->pluck( 'categoryId' )->toArray();
            $needToAdd = $request['food_category'];
            if( $existingCatIds ) { 
                $needToDelete = array_diff( $existingCatIds, $request['food_category'] );
                $matched = array_intersect( $existingCatIds, $request['food_category'] );
                $needToAdd = array_diff( $request['food_category'], $matched );
                if( $needToDelete ) Category_item::whereIn('categoryId', $needToDelete )->where( 'itemId', $food->id )->delete( );
            }
            
            foreach( $needToAdd as $cat ) { 
                $Category_item = new Category_item(); 
                $Category_item->categoryId= $cat ;
                $Category_item->itemId= $food->id;
                $Category_item->orderBy= 0;
                $Category_item->duplicate = 0 ;
                $Category_item->save();
                    
                $Category_item->orderBy = $Category_item->id;
                $Category_item->save();
            }
        }
        
        if( $request['food_type'] == 'nonveg' && isset($request['isVegAvailable']) )
        {
            if( $request['add_veg'] == 'existing' )
            {
                $food->veg_item_id = $request['itemId'];
                $food->isVegAvailable = 1;
                $food->save();
            }else
            {
                $food_veg = new Food_item();

                $file_name_with_time_prefix = upload_file($request, 'itemImage_veg' , env('AWS_S3_FOOD_ITEMIMAGE_BASEURL') );

                $food_veg->itemName= $request['itemName_veg'];
                $food_veg->serveName= $request['serveName_veg'];
                $food_veg->instructionalVideo= $request['instructionalVideo_veg'] ? $request['instructionalVideo_veg'] : '';
                $food_veg->prepareAhead= $request['prepareAhead_veg'] ? $request['prepareAhead_veg']: '';
                $food_veg->calories= $request['calories_veg'];
                $food_veg->userType= $request['userType_veg'];

                $food_veg->itemIngredients= $request['itemIngredients_veg'];
                $food_veg->itemMethod= $request['itemMethod_veg'];
                $food_veg->additionalInfo= $request['additionalInfo_veg'] ? $request['additionalInfo_veg']: '';

                $food_veg->veg_item_id = 0;

                if($file_name_with_time_prefix)
                    $food_veg->itemImage= $file_name_with_time_prefix;

                if( $food_veg->save() ) { 

                    $food_veg->orderBy = $food_veg->id;
                    $food_veg->save();

                    $food_item = Food_item::find( $food->id );
                    $food_item->veg_item_id = $food_veg->id ;
                    $food_item->isVegAvailable = 1 ;
                    $food_item->save();

                    foreach( $request['food_category'] as $cat )
                    {
                        $Category_item = new Category_item(); 
                        $Category_item->categoryId = $cat ;
                        $Category_item->itemId = $food_veg->id ;
                        $Category_item->orderBy = $food_veg->id ;
                        $Category_item->duplicate = 0 ;
                        $Category_item->save() ;
                        
                        $Category_item->orderBy = $Category_item->id;
                        $Category_item->save();
                    }
                }   
            }
        }
        else{
            /* if( $food->veg_item_id && $food->isVegAvailable )
            {
                $food_veg = Food_item::find($food->veg_item_id);
                $food_veg->isArchive = 1;
                $food_veg->save() ;
                Category_item::where('itemId', $food_veg->id )->delete();
            } */
            $food = Food_item::find($food->id);
            $food->veg_item_id = 0;
            $food->isVegAvailable = 0 ;
            $food->save() ;
        }

        \App\Models\LastUpdated::updateDateTime();

        return redirect()->route('admin.food_items')->with('success','Food updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decryptedId = Hashids::decode($id);
        if( !$decryptedId ){
            return redirect()->route('admin.food_items')->with('error','Some information is missing!');
        }
            
        $foodInfo = Food_item::find( $decryptedId[0] );
        if( $foodInfo->count() <= 0 ) { 
            return redirect()->route('admin.food_items')->with('error','Some information is missing!');
        }

        $foodInfo->isArchive = 1;
        if( !$foodInfo->save( ) ) { 
            return redirect()->route('admin.food_items')->with('error','Some information is missing!');
        }

        if( $foodInfo->veg_item_id > 0 ) {  
            $foodInfo = Food_item::find( $foodInfo->veg_item_id );
            $foodInfo->isArchive = 1;
            $foodInfo->save( );
        }

        \App\Models\LastUpdated::updateDateTime();

        $redirectTo = \Request::server('HTTP_REFERER');

        if( $redirectTo ) { 
            return back()->with('success','Food deleted successfully.');
        } else { 
            return redirect()->route('admin.food_items')->with('success','Food deleted successfully.');
        }

    }

    public function get_categories()
    {
        return $all_categories = Food_category::where( 'isArchive', 0 )->orderBy('isCombo','DESC')->orderBy('orderBy','asc')->get();
    }

    public function getComboCategory()
    {
        return Food_category::where( 'isArchive', 0 )->where( 'isCombo', 1 )->first( );
    }

    // function to update the status (Active/Inactive)
    public function food_status_update(Request $request)
    {
        $request_data = $request->all();
        if( ! ( isset( $request->id ) && $request->id > 0 ) ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        // echo "<pre>";print_r($request_data);die;
        $food = Food_item::find($request->id);
        if( $food->count() <= 0 ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Selected Item Not Found'
            ]);
        }

        $food->status = $food->status ? 0 : 1 ;
        if( !$food->save( ) ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Unable to update status, please try again later.'
            ]);
        }
        
        \App\Models\LastUpdated::updateDateTime();
        return response()->json([
            'status' => 1,
            'msg' => 'Status updated successfully.'
        ]);
    }

    public function updateFoodItemReorder(Request $request)
    {
        $request_data = $request->all();
        // echo "<pre>";print_r( $request_data );  die;
        
        $validator = $request->validate([
                'replace_ids' => 'required'
            ]);

        if( count( $request_data['replace_ids'] ) <= 1 ) { 
            return response::json([ 'errors' => 'Unable to update order, please try again later.' ]);
        }

        $positions = $ids = [];
        foreach( array_keys( $request_data['replace_ids'] ) as $id ) { 
            $ids[] = Hashids::decode( ( string )$id )[0];
            $positions[ Hashids::decode( ( string )$id )[0] ] = $request_data['replace_ids'][ $id ];
        }
            
        $foodItems = Food_item::whereIn( 'id', $ids )->get();
        if( $foodItems->count() !=  count( $request_data['replace_ids'] ) ) { 
            return response::json([ 'errors' => 'Unable to update order, please try again later.' ]);
        }

        foreach( $foodItems as $foodItem ){ 
            
            $foodItem->orderBy = $positions[ $foodItem->id ];
            
            $foodItem->save();

            /*$catsObj = Category_item::where( 'itemId', $foodItem->id )->update( [ 'orderBy', $positions[ $foodItem->id ] ] );*/

        }

        \App\Models\LastUpdated::updateDateTime();

        return response::json([ 'success' => 'Order updated successfully' ]);

    }

    public function updateCategoryWiseFoodItemReorder( $categoryId, Request $request) { 

        $categoryId = ( isset( $categoryId ) && $categoryId )?Hashids::decode( $categoryId )[0]:0;
        $category = [];
        if( $categoryId ) { 
            $category = Food_category::where( 'isArchive', 0 )->find( $categoryId );
            if( !$category ) {
                $data = [ "draw" => $draw, "recordsTotal" => 0, "recordsFiltered" => 0, "data" => [] ];
                return response( )->json($data);
            }
        }
        if( !$category ){ 
            return response::json([ 'errors' => 'Unable to update order, please try again later.' ]);
        }

        $request_data = $request->all();
        // echo "<pre>";print_r( $request_data );  die;
        
        $validator = $request->validate([
                'replace_ids' => 'required'
            ]);

        if( count( $request_data['replace_ids'] ) <= 1 ) { 
            return response::json([ 'errors' => 'Unable to update order, please try again later.' ]);
        }

        $positions = $ids = [];
        foreach( array_keys( $request_data['replace_ids'] ) as $id ) { 
            $ids[] = Hashids::decode( (string)$id )[0];
            $positions[ Hashids::decode( (string)$id )[0] ] = $request_data['replace_ids'][ $id ];
        }
            
        $catItems = Category_item::whereIn( 'id', $ids )->where( 'categoryId', $categoryId )->get();
        if( $catItems->count() !=  count( $request_data['replace_ids'] ) ) { 
            return response::json([ 'errors' => 'Unable to update order, please try again later.' ]);
        }

        foreach( $catItems as $catItem ){ 
            
            $catItem->orderBy = $positions[ $catItem->id ];
            
            $catItem->save();

            /*$catsObj = Category_item::where( 'itemId', $foodItem->id )->update( [ 'orderBy', $positions[ $foodItem->id ] ] );*/

        }

        \App\Models\LastUpdated::updateDateTime();

        return response::json([ 'success' => 'Order updated successfully' ]);

    }

    public function deleteImage ( $id ) { 
        if( !$id ){
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        $id = Hashids::decode($id);
        if( !$id ){
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        $id = $id[0];

        $obj = Food_item::find( $id );
        if( $obj->count() <= 0 ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Selected item not found'
            ]);
        }

        if( !$obj->itemImage ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Selected leader image not found'
            ]);
        }

        $isDeleted = Storage::disk('s3')->delete( env( 'AWS_S3_FOOD_ITEMIMAGE_BASEURL' ) . '/' . $obj->itemImage );
        if( !$isDeleted ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Unable to delete image, please try again later!'
            ]);
        }

        $obj->itemImage = '';
        
        if( !$obj->save( ) ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Unable to delete image, please try again later!'
            ]);
        }
        
        \App\Models\LastUpdated::updateDateTime();
        return response()->json([
            'status' => 1,
            'msg' => 'Food item image deleted!.'
        ]);

    }
}
