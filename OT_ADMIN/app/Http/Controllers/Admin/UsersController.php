<?php

namespace App\Http\Controllers\Admin;


use Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Middleware\RedirectIfAuthenticated;
use Session;
use Illuminate\Support\Facades\Redirect;
use Mail;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Input;
use Config;
use xmlapi;
use Hash;
use Carbon\Carbon;
use App\Http\Controllers\Admin\CommonController;
use Illuminate\Support\Facades\URL;
use Vinkla\Hashids\Facades\Hashids;

class UsersController extends Controller{

  public function __construct() {
    $this->middleware('guest');
  }

  /* get userlisting  */
  public function userListing(Request $request){
    $title = "User Management";
    return view('admin.users.users', ['title' => $title, "breadcrumbItem" => "Manage Users" , "breadcrumbTitle"=>"Users List"]);
  }

  public function addUser(){
    $title = "Add User";
    return view('admin.users.add_user', ['title' => $title, "breadcrumbItem" => "Manage Users" , "breadcrumbTitle"=> "Add User"]);
  }


  public function saveUser(Request $request){

    $validatedData = $this->validate($request, [
        'fullname' => 'required|regex:/^[a-zA-Z]+$/u',
        'email' => 'required|email',
        'password' => 'required|min:6',
        'gender' => 'required'
    ]);

    $user_details = array(
      "name" => $request->fullname,
      "email" => $request->email,
      "password" => Hash::make($request->password),
      "gender" => $request->gender,
      "email_verified_at" => Carbon::now(),
      "role_id" => 2
    );    
    $user = User::create($user_details); 
    if($user){
      return redirect()->route('admin.userlist')->with(['success'=>'User detail has been added successfully.']);
    }else{

    }

  }
  


  public function editUser($id){
    $title = "Edit User";
    $decryptUserId = Hashids::decode($id);
    $user_detail = User::find($decryptUserId[0]);
    return view('admin.users.edit_user', ['title' => $title, "user"=> $user_detail, "breadcrumbItem" => "Manage Users" , "breadcrumbTitle"=> "Edit User"]);
  }


  public function updateUser(Request $request){

    $validatedData = $this->validate($request, [
        'fullname' => 'required|regex:/^[a-zA-Z]+$/u',
        'email' => 'required|unique:users,email,' . $request->userid,
        'password' => 'nullable|min:6',
        'gender' => 'required'
    ]);

    $user = User::find($request->userid);
    $user->name = $request->fullname;
    $user->email = $request->email;
    if(!empty($request->password)){
      $user->password = Hash::make($request->password);
    }
    $user->updated_at = Carbon::now();
    $user->save();

    if($user){
      return redirect()->route('admin.userlist')->with(['success'=>'User detail has been updated successfully.']);
    }else{

    }

  }


  public function ajaxDataLoad(Request $request){
    $draw = $_GET['draw'];
    $row = $_GET['start'];
    $rowperpage = $_GET['length']; // Rows display per page
    $columnIndex = $_GET['order'][0]['column']; // Column index
    $columnName = $_GET['columns'][$columnIndex]['data']; // Column name
    $columnSortOrder = $_GET['order'][0]['dir']; // asc or desc
    $searchValue = $_GET['search']['value']; // Search value
    
    $columns = [
        0 => 'id',
        1 => 'name',
        2 => 'email',
        3 => 'status',
        4 => 'created_at',
        5 => 'email'
    ];
    $columnName = $columns[$columnIndex];

    ## Search 
    $searchQuery = " ";
    if($searchValue != ''){
       $searchQuery = " and (name like '%".$searchValue."%' or email like '%".$searchValue."%' ) ";
    }
    
    ## Total number of records
    $totalRecords = User::whereRaw("role_id = '2'")->count();
    
    ## Total number of record with filtering
    $totalRecordwithFilter = User::whereRaw(" role_id = '2' ". $searchQuery)->count();
    
    ## Fetch records
    $userlist = User::whereRaw(" role_id = '2' ". $searchQuery)->orderBy($columnName, $columnSortOrder)->skip($row)->take($rowperpage)->get();

    $checkbox = ""; $data = array(); $action = "";
    if(!empty($userlist)){
        foreach($userlist as $users){
        $encryptId = Hashids::encode($users["id"]);  
        
        $checkbox = '<div class="animated-checkbox"><label style="margin-bottom:0px;"><input type="checkbox" name="user_ids[]" value="'.Hashids::encode($users['id']).'" /><span class="label-text"></span></label></div>';
        $action = '<a href="edituser/'.$encryptId.'"><i class="fa fa-pencil" aria-hidden="true"></i></a> &nbsp;&nbsp; <a href="javascript:void(0);" onclick=delete_row("'.$encryptId.'")><i class="fa fa-trash" aria-hidden="true"></i></a></i>';
           $data[] = array( 
              $checkbox,
              $users['name'],
              $users['email'],
              $users['status'] =="1" ? "<span class='badge' style='background:green; color:#FFF; padding:5px;'>Active</span>" : "<span class='badge' style='background:#FF0000; color:#FFF; padding:5px;'>Inactive</span>",
              date("d M, Y", strtotime($users['created_at'])),
              $action
           );
        }
    }
    ## Response
    $response = array(
      "draw" => intval($draw),
      "iTotalRecords" => $totalRecords,
      "iTotalDisplayRecords" => $totalRecordwithFilter,
      "aaData" => $data
    );
    echo json_encode($response);
    exit;
  }
    


}
