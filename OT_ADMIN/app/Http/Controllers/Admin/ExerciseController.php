<?php

namespace App\Http\Controllers\Admin;

use Session;
use Config;
use Mail;
use Hash;
use Auth;
use Response;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\CommonController;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Vinkla\Hashids\Facades\Hashids;
use App\Models\Exercise;
use App\Models\Leaders;

class ExerciseController extends Controller
{
    public function index( $leaderId ) { 

        if( !$leaderId ){
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }

        $leaderId = Hashids::decode($leaderId);
        if( !$leaderId ){
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }

        $leader = Leaders::where( 'is_archive', 0 )->find( $leaderId[0] );
        if( !$leader ){ 
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }

        $leaderObj = Exercise::select( \DB::raw( 'count( dayNumber ) as total_days_added' ) )
                            ->where( 'is_archive', 0 )
                            ->where( 'leaderId', $leaderId[0] )
                            ->first();
        $totaldaysAddded = $leaderObj && $leaderObj->total_days_added?$leaderObj->total_days_added:0;
                           

        return view('admin.exercises.index',[ 'title'=>"Manage Exercises For: " . $leader->leaderName,'breadcrumbItem'=>'All Leaders' , 'breadcrumbItemLink'=>route('admin.leaders' ), 'breadcrumbTitle'=>'All Exercises', 'totaldaysAddded' => $totaldaysAddded, 'leaderId' => $leaderId[0], 'leader' => $leader ] );
    }

    public function list( $leaderId, Request $request) { 

        $requestedData  = $request->all();

        $draw           = ( isset( $requestedData[ 'draw' ] ) && $requestedData[ 'draw' ] > 0 )?$requestedData[ 'draw' ]:1;
        
        if( !$leaderId ){
            $data = [ "draw" => $draw, "recordsTotal" => 0, "recordsFiltered" => 0, "data" => [] ];
            return response( )->json($data);
        }

        $leaderId = Hashids::decode($leaderId);
        if( !$leaderId ){
            $data = [ "draw" => $draw, "recordsTotal" => 0, "recordsFiltered" => 0, "data" => [] ];
            return response( )->json($data);
        }

        $leader = Leaders::where( 'is_archive', 0 )->find( $leaderId[0] );
        if( !$leader ){ 
            $data = [ "draw" => $draw, "recordsTotal" => 0, "recordsFiltered" => 0, "data" => [] ];
            return response( )->json($data);
        }

        $start          = ( isset( $requestedData[ 'start' ] ) && $requestedData[ 'start' ] > 0 )?$requestedData[ 'start' ]:0;
        $perPage        = ( isset( $requestedData[ 'length' ] ) && $requestedData[ 'length' ] > 0 )?$requestedData[ 'length' ]:10;
        $search         = ( isset( $requestedData[ 'search' ] ) && isset( $requestedData[ 'search' ][ 'value' ] ) )?$requestedData[ 'search' ][ 'value' ]:'';

        $leader         = ( isset( $requestedData[ 'leader' ] ) && $requestedData[ 'leader' ] )?$requestedData[ 'leader' ]:false;
        
        $orderColumn    = ( isset( $requestedData[ 'order' ] ) && isset( $requestedData[ 'order' ][ 0 ] ) && isset( $requestedData[ 'order' ][ 0 ][ 'column' ] ) )?$requestedData[ 'order' ][ 0 ][ 'column' ]:0;
        $orderDir        = ( isset( $requestedData[ 'order' ] ) && isset( $requestedData[ 'order' ][ 0 ] ) && isset( $requestedData[ 'order' ][ 0 ][ 'dir' ] ) )?$requestedData[ 'order' ][ 0 ][ 'dir' ]:'ASC';

        $exerciseTblName = with( new Exercise )->getTable( );
        $orderByColumns = [ 0 => $exerciseTblName . '.order_by', 1 => $exerciseTblName . '.name', 2 => $exerciseTblName . '.calories', 3 => $exerciseTblName . '.dayNumber', 4 => $exerciseTblName . '.weekNumber' ];

        $exerciseObj = Exercise::where( $exerciseTblName . '.is_archive', 0 )
                            ->where( $exerciseTblName . '.leaderId', $leaderId[0] )
                            ->orderBy( $orderByColumns[ $orderColumn ], $orderDir );

        $totalItems = $exerciseObj->count( );

        if( $search ) { 
            $exerciseObj->where( $exerciseTblName . '.name', 'like', "%{$search}%" );
        }

        if( $leader ) { 
            $exerciseObj->where( $exerciseTblName . '.leaderId', $leader );
        }

        $recordsFiltered = $exerciseObj->count( );

        $exercises = $exerciseObj->offset( $start )->limit( $perPage )->get( );
        
        $data = [ "draw" => $draw, "recordsTotal" => $totalItems, "recordsFiltered" => $recordsFiltered, "data" => [] ];
        if( $exercises->count( ) > 0 ) { 
            foreach( $exercises as $exercise ) { 
                $data[ 'data' ][] = [ 
                                    ++$start,
                                    $exercise->name,
                                    $exercise->calories,
                                    $exercise->dayNumber + 1,
                                    $exercise->weekNumber + 1,
                                    '<div class="form-group">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input active_status category_active_' . Hashids::encode($exercise->id) . '" name="category_active_' . Hashids::encode($exercise->id) . '" onchange="return updateStatus( \'' . Hashids::encode($exercise->id) . '\', \'' . route( 'admin.exercise_status_update', [ Hashids::encode( $leaderId[0] ) ] ) . '\', this.value );" id="yes" value="1" ' . ( $exercise->status == 1 ? "checked":"" ) . '> Yes <i class="input-helper"></i></label>
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input active_status category_active_' . Hashids::encode($exercise->id) . '" name="category_active_' . Hashids::encode($exercise->id) . '" onchange="return updateStatus( \'' . Hashids::encode($exercise->id) . '\', \'' . route( 'admin.exercise_status_update', [ Hashids::encode( $leaderId[0] ) ] ) . '\', this.value );" id="no" value="0" ' . ( $exercise->status == 0 ? "checked":"" ) . '> No <i class="input-helper"></i></label>
                                        </div>                                          
                                    </div>',
                                    '<a href="' . route('admin.edit_exercise', [ Hashids::encode( $leaderId[0] ), 'id'=> Hashids::encode($exercise->id)  ] ) . '" class="add" title=""><i class="fa fa-edit"></i></a><a href="' . route('admin.delete_exercise', [ Hashids::encode( $leaderId[0] ), 'id'=> Hashids::encode($exercise->id)  ] ) . '" class="delete_record" ><i class="fa fa-trash"></i></a>',
                                    Hashids::encode($exercise->id),
                                    $exercise->order_by 
                                ];
            }
        }

        return response( )->json($data);
    }

    public function create( $leaderId ) { 
        if( !$leaderId ){
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }

        $leaderId = Hashids::decode($leaderId);
        if( !$leaderId ){
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }

        $leader = Leaders::where( 'is_archive', 0 )->find( $leaderId[0] );
        if( !$leader ){ 
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }

        $leaderObj = Exercise::select( \DB::raw( 'count( dayNumber ) as total_days_added' ) )
                            ->where( 'is_archive', 0 )
                            ->where( 'leaderId', $leaderId[0] )
                            ->first();
        $totaldaysAddded = $leaderObj && $leaderObj->total_days_added?$leaderObj->total_days_added:0;
        if( $totaldaysAddded >= 98 ){
            return redirect()->route('admin.exercises', [ Hashids::encode( $leaderId[0] ) ])->with('error','98 days exercise plans already added!');
        }

        $usedDays = Exercise::where( 'leaderId', $leaderId[0])->where('is_archive',0)
                        ->pluck( 'dayNumber' )->toArray();
        $leftDays = array_diff( range( 0, 97 ), $usedDays );

        $title = "Add Exercise For: " . $leader->leaderName;

        return view('admin.exercises.add',[ 'title'=>$title, 'breadcrumbItem'=>'All Exercises' , 'breadcrumbItemLink'=>route('admin.exercises', [ Hashids::encode( $leaderId[0] ) ] ),  'breadcrumbTitle'=>'Add Exercise For: ' . $leader->leaderName, 'leftDays' => $leftDays, 'leader' => $leader ]);
    }

    public function store( $leaderId, Request $request ) { 

        if( !$leaderId ){
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }

        $leaderId = Hashids::decode($leaderId);
        if( !$leaderId ){
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }

        $leader = Leaders::where( 'is_archive', 0 )->find( $leaderId[0] );
        if( !$leader ){ 
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }

        $leaderObj = Exercise::select( \DB::raw( 'count( dayNumber ) as total_days_added' ) )
                            ->where( 'is_archive', 0 )
                            ->where( 'leaderId', $leaderId[0] )
                            ->first();

        $totaldaysAddded = $leaderObj && $leaderObj->total_days_added?$leaderObj->total_days_added:0;
        if( $totaldaysAddded >= 98 ){
            return redirect()->route('admin.exercises', [ Hashids::encode( $leaderId[0] ) ])->with('error','98 days exercise plans already added!');
        }

        $leadersTblName = with( new Leaders )->getTable( );

        $validator = $request->validate([
            'name' => 'required',
            'dayNumber' => 'required',
            'calories' => 'required',
            // 'video_links' => 'nullable|url'
        ],[   
            'name.required' => 'Please enter exercise name',
            'dayNumber.required' => 'Please select day number',
            'calories.required' => 'Please enter calories burned',
            // 'video_links.url' => 'Please enter vallid url'
        ]);

        $exerciseTblName = with( new Exercise )->getTable( );
        $leadersTblName  = with( new Leaders )->getTable( );

        $isDayTaken = Exercise::where( 'is_archive', 0 )->where( 'dayNumber', $request['dayNumber'] )->where( 'leaderId', $leaderId[0] )->first();
        if( $isDayTaken ) { 
            return Redirect::back()->with( 'error', 'Exercise plan already added for selected day.' )->withInput( );
        }

        $exercise = new Exercise();
        $exercise->name= $request['name'];
        $exercise->calories= $request['calories'];
        $exercise->leaderId= $leaderId[0];
        $exercise->weekNumber= floor($request['dayNumber']/7);
        $exercise->dayNumber= $request['dayNumber'];
        $exercise->data= $request['data'];
        $exercise->dataType= '0,1';
        $exercise->video_links= $request['video_links']?json_encode($request['video_links'], true):'';

        if( !$exercise->save() ) {
            return redirect()->route('admin.exercises', [ Hashids::encode( $leaderId[0] ) ])->with('error','Something went wrong, please try again later.');
        }

        $exercise->order_by = $exercise->id;
        $exercise->save();
        
        \App\Models\LastUpdated::updateDateTime();
        return redirect()->route('admin.exercises', [ Hashids::encode( $leaderId[0] ) ])->with('success','Exercise saved successfully.');
    }

    public function edit($leaderId, $id) { 

        if( !$leaderId ){
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }

        $leaderId = Hashids::decode($leaderId);
        if( !$leaderId ){
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }

        $leader = Leaders::where( 'is_archive', 0 )->find( $leaderId[0] );
        if( !$leader ){ 
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }
        
        if( !$id ){
            return redirect()->route('admin.exercises', [ Hashids::encode( $leaderId[0] ) ])->with('error','Some information is missing!');
        }

        $decryptedId = Hashids::decode($id);
        if( !$decryptedId ){
            return redirect()->route('admin.exercises', [ Hashids::encode( $leaderId[0] ) ])->with('error','Some information is missing!');
        }
        
        $title = "Edit Exercise For: " . $leader->leaderName;
        $exerciseInfo = Exercise::find( $decryptedId[ 0 ] );
        if( !$exerciseInfo ){
            return redirect()->route('admin.exercises', [ Hashids::encode( $leaderId[0] ) ])->with('error','Exercise not found!');
        }
        
        return view('admin.exercises.edit',[ 'title'=>$title,'breadcrumbItem'=>'All Exercises' , 'breadcrumbItemLink'=>route('admin.exercises', [ Hashids::encode( $leaderId[0] ) ]), 'breadcrumbTitle'=>'Edit Exercise For: ' . $leader->leaderName, 'exerciseInfo'=>$exerciseInfo, 'leader'=>$leader ] );

    }

    public function update( $leaderId, $exerciseId, Request $request)  { 

        if( !$leaderId ){
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }

        $leaderId = Hashids::decode($leaderId);
        if( !$leaderId ){
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }

        $leader = Leaders::where( 'is_archive', 0 )->find( $leaderId[0] );
        if( !$leader ){ 
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }

        $leaderObj = Exercise::select( \DB::raw( 'count( dayNumber ) as total_days_added' ) )
                            ->where( 'is_archive', 0 )
                            ->where( 'leaderId', $leaderId[0] )
                            ->first();

        $totaldaysAddded = $leaderObj && $leaderObj->total_days_added?$leaderObj->total_days_added:0;
        if( $totaldaysAddded >= 98 ){
            // return redirect()->route('admin.exercises', [ Hashids::encode( $leaderId[0] ) ])->with('error','98 days exercise plans already added!');
        }
        
        if( !$exerciseId ){
            return redirect()->route('admin.exercises')->with('error','Some information is missing!');
        }

        $exerciseId = Hashids::decode($exerciseId);
        if( !$exerciseId ){
            return redirect()->route('admin.exercises')->with('error','Some information is missing!');
        }

        $exerciseId = $exerciseId[0];

        $leadersTblName = with( new Leaders )->getTable( );
        
        $validator = $request->validate([
            'name' => 'required',
            'data' => 'required',
            'calories' => 'required',
            // 'video_links' => 'nullable|url'
        ],[   
            'name.required' => 'Please enter exercise name',
            'data.required' => 'Please enter exercise details',
            'calories.required' => 'Please enter calories burned',
            // 'video_links.url' => 'Please enter vallid url'
        ]);
            
        $exerciseObj = Exercise::find($exerciseId);
        if( $exerciseObj->count() <= 0 ){ 
            return redirect()->route('admin.exercises')->with('error','Selected exercise not found!');
        }

        $exerciseObj->name= $request['name'];
        $exerciseObj->calories= $request['calories'];
        $exerciseObj->data= $request['data'];
        // $exerciseObj->video_links= $request['video_links'];
        $exerciseObj->video_links= json_encode($request['video_links'], true);

        if( !$exerciseObj->save() ) {
            return redirect()->route('admin.edit_exercise', Hashids::encode( $exerciseId ) )->with('error','Something went wrong, please try again later.');
        }

        \App\Models\LastUpdated::updateDateTime();
        return redirect()->route('admin.exercises', [ Hashids::encode( $leaderId[0] ) ])->with('success','Exercise updated successfully.');
    }

    public function destroy( $leaderId, $id ) { 

        if( !$leaderId ){
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }

        $leaderId = Hashids::decode($leaderId);
        if( !$leaderId ){
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }

        $leader = Leaders::where( 'is_archive', 0 )->find( $leaderId[0] );
        if( !$leader ){ 
            return redirect()->route('admin.leaders')->with('error','Selected leader not found!');
        }

        $decryptedId = Hashids::decode($id);
        if( !$decryptedId ){
            return redirect()->route('admin.exercises', [ Hashids::encode( $leaderId[0] ) ])->with('error','Some information is missing!');
        }
            
        $exerciseInfo = Exercise::find( $decryptedId[0] );
        if( $exerciseInfo->count() <= 0 ) { 
            return redirect()->route('admin.exercises', [ Hashids::encode( $leaderId[0] ) ])->with('error','Some information is missing!');
        }

        $exerciseInfo->is_archive = 1;
        if( !$exerciseInfo->save( ) ) { 
            return redirect()->route('admin.exercises', [ Hashids::encode( $leaderId[0] ) ])->with('error','Some information is missing!');
        }

        \App\Models\LastUpdated::updateDateTime();
        return redirect()->route('admin.exercises', [ Hashids::encode( $leaderId[0] ) ])->with('success','Exercise deleted successfully.');

    }

    // function to update the status (Active/Inactive)
    public function statusUpdate(Request $request)
    {
        $request_data = $request->all();
        if( !$request->id ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        $request->id = Hashids::decode( $request->id )[0];

        if( ! ( isset( $request->id ) && $request->id > 0 ) ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        $obj = Exercise::find($request->id);
        if( $obj->count() <= 0 ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Selected exercise not found'
            ]);
        }

        $obj->status = $obj->status ? 0 : 1 ;
        
        if( !$obj->save( ) ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Unable to update status, please try again later.'
            ]);
        }
        
        \App\Models\LastUpdated::updateDateTime();
        return response()->json([
            'status' => 1,
            'msg' => 'Status updated successfully.'
        ]);
    }

    public function updateReorder(Request $request)
    {
        $request_data = $request->all();
        // echo "<pre>";print_r( $request_data );  die;
        
        $validator = $request->validate([
                'replace_ids' => 'required'
            ]);

        if( count( $request_data['replace_ids'] ) <= 1 ) { 
            return response::json([ 'errors' => 'Unable to update order, please try again later.' ]);
        }

        $positions = $ids = [];
        foreach( array_keys( $request_data['replace_ids'] ) as $id ) { 
            $ids[] = Hashids::decode( ( string )$id )[0];
            $positions[ Hashids::decode( ( string )$id )[0] ] = $request_data['replace_ids'][ $id ];
        }
            
        $items = Exercise::whereIn( 'id', $ids )->get();
        if( $items->count() !=  count( $request_data['replace_ids'] ) ) { 
            return response::json([ 'errors' => 'Unable to update order, please try again later.' ]);
        }

        foreach( $items as $item ){ 
            $item->order_by = $positions[ $item->id ];
            $item->save();
        }

        \App\Models\LastUpdated::updateDateTime();
        return response::json([ 'success' => 'Order updated successfully' ]);

    }
}