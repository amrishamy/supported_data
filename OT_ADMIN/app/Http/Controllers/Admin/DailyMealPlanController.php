<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\CommonController;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Config;
use Mail;
use Hash;
use Vinkla\Hashids\Facades\Hashids;

use Auth;
use App\Models\User;
use App\Models\Emails;
use App\Models\DailyMealPlan;
use App\Models\Food_item;
use Response;
use DB;

class DailyMealPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( ) { 
          
        return view('admin.daily_meal_plans.daily_meal_plans_list',[ 'title'=>'Manage meal plans','breadcrumbItem'=>'Daily meal plans', 'breadcrumbItemLink'=>route('admin.dailymealplanlist'), 'breadcrumbTitle'=>'Daily meal list' ] );
    }

    public function list( Request $request) { 

        $requestedData  = $request->all();

        $draw           = ( isset( $requestedData[ 'draw' ] ) && $requestedData[ 'draw' ] > 0 )?$requestedData[ 'draw' ]:1;
        $start          = ( isset( $requestedData[ 'start' ] ) && $requestedData[ 'start' ] > 0 )?$requestedData[ 'start' ]:0;
        $perPage        = ( isset( $requestedData[ 'length' ] ) && $requestedData[ 'length' ] > 0 )?$requestedData[ 'length' ]:10;
        $search         = ( isset( $requestedData[ 'search' ] ) && isset( $requestedData[ 'search' ][ 'value' ] ) )?$requestedData[ 'search' ][ 'value' ]:'';
        
        $orderColumn    = ( isset( $requestedData[ 'order' ] ) && isset( $requestedData[ 'order' ][ 0 ] ) && isset( $requestedData[ 'order' ][ 0 ][ 'column' ] ) )?$requestedData[ 'order' ][ 0 ][ 'column' ]:1;
        $orderDir        = ( isset( $requestedData[ 'order' ] ) && isset( $requestedData[ 'order' ][ 0 ] ) && isset( $requestedData[ 'order' ][ 0 ][ 'dir' ] ) )?$requestedData[ 'order' ][ 0 ][ 'dir' ]:'ASC';

        $foodTblName        = with( new Food_item )->getTable( ); 
        $mealPlanTblName        = with( new DailyMealPlan )->getTable( ); 
        $orderByColumns = [ $mealPlanTblName . '.dayNumber',1 => $mealPlanTblName . '.dayNumber', 3 => $foodTblName . '.itemName' ];
        // DB::enableQueryLog();
        $dailyMealObj = DailyMealPlan::select( [ $mealPlanTblName.".*", $foodTblName.".*" ] ) 
                ->orderBy( $orderByColumns[ $orderColumn ], $orderDir )
                ->leftJoin( $foodTblName, function( $query ) use( $mealPlanTblName, $foodTblName ){
                    $query->on( $mealPlanTblName.".itemId", "=", $foodTblName.".id" );
                    $query->on( $foodTblName.".isArchive", "=", \DB::raw( 0 ) );
                });

        $totalItems = $dailyMealObj->count( );

        if( $search ) { 
            $dailyMealObj->where( $foodTblName . '.itemName', 'like', "%{$search}%" );
        }

        $recordsFiltered = $dailyMealObj->count( );

        $dailyMealItems = $dailyMealObj->offset( $start )->limit( $perPage )->get( );
        // dd(DB::getQueryLog());
        $data = [ "draw" => $draw, "recordsTotal" => $totalItems, "recordsFiltered" => $recordsFiltered, "data" => [] ];
        if( $dailyMealItems->count( ) > 0 ) { 
            foreach( $dailyMealItems as $daily_meal ) { 
                $data[ 'data' ][] = [  
                                    ++$start,
                                    $daily_meal->dayNumber + 1,
                                    $daily_meal->weekNumber + 1,
                                    '<a target="_blank" href="' . route('admin.editfood', [ 'id'=> Hashids::encode($daily_meal->id)  ] ) . '" >'.$daily_meal->itemName.'</a>',
                                    
                                    '<a href="' . route('admin.editdailymealplan', [ 'id'=> Hashids::encode($daily_meal->dayNumber)  ] ) . '" class="add" title=""><i class="fa fa-edit"></i></a>',
                                    Hashids::encode($daily_meal->id),
                                    $daily_meal->orderBy 
                                ];
            }
        }

        return response( )->json($data);
    }

    public function edit( $id, Request $request ) { 
        $decryptedId = Hashids::decode( $id );
        $daily_meal_plan_detail = DailyMealPlan::where( 'dayNumber', $decryptedId[0] )->first();

        if( !$daily_meal_plan_detail ) { 
            return redirect()->route('admin.dailymealplanlist')->with('error','Selected Day Not Found!');
        }
        $itemId = $request->old('itemId')?$request->old('itemId'):$daily_meal_plan_detail->itemId;

        $foodItem = Food_item::where( 'isArchive', 0 )->find( $itemId );
        
        return view('admin.daily_meal_plans.daily_meal_plans_edit',[ 'title'=>'Edit daily meal plan','breadcrumbItem'=>'Daily plan list' , 'breadcrumbItemLink'=>route('admin.dailymealplanlist'), 'breadcrumbTitle'=>'Edit daily meal plan','daily_meal_plan_detail'=>$daily_meal_plan_detail, 'foodItem' => $foodItem ] );
    }

    public function update( $id, Request $request ) { 
        
        $validator = $request->validate([
            'itemId' => 'required'
        ], [
            'itemId.required' => 'Please select meal from list.'
        ]);

        $decryptedId = Hashids::decode( $id );
        $daily_meal_plan_detail = DailyMealPlan::where( 'dayNumber', $decryptedId[0] )->first();

        if( !$daily_meal_plan_detail ) { 
            return redirect()->route('admin.dailymealplanlist')->with('error','Selected Day Not Found!');
        }

        $foodItem = Food_item::where( 'isArchive', 0 )->find( $request['itemId'] );
        
        if( !$foodItem ) { 
            return redirect( )->back( )->with('error','Selected meal item not found!')->withInput( $request->input() );
        }

        $daily_meal_plan_detail->itemId = $foodItem->id;
        if( !$daily_meal_plan_detail->save() ) { 
            return redirect( )->back( )->with('error','Unable to update meal item, please try again later!')->withInput( $request->input() );
        }

        \App\Models\LastUpdated::updateDateTime();
        return redirect()->route('admin.dailymealplanlist')->with('success','Selected meal for day: ' . $decryptedId[0] . ' updated!');
    }

}
