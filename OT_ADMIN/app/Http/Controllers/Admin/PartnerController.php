<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\CommonController;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Config;
use Mail;
use Hash;
use Vinkla\Hashids\Facades\Hashids;

use Auth;
use App\Models\User;
use App\Models\Emails;
use App\Models\Partner;
use Response;

class PartnerController extends Controller
{
    public function __construct() {
        $this->middleware('guest');
      }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        // $all_partners = Partner::orderBy('sort_order','asc')->get();     
        // print_r($all_partners);die;
        return view('admin.partner.partner_list',[ 'title'=>'Manage Partners','breadcrumbItem'=>'Partners', 'breadcrumbItemLink'=>route('admin.partnerlist'), 'breadcrumbTitle'=>'List Partners' ] );
    }

    public function list( Request $request) { 

        $requestedData  = $request->all();

        $draw           = ( isset( $requestedData[ 'draw' ] ) && $requestedData[ 'draw' ] > 0 )?$requestedData[ 'draw' ]:1;
        $start          = ( isset( $requestedData[ 'start' ] ) && $requestedData[ 'start' ] > 0 )?$requestedData[ 'start' ]:0;
        $perPage        = ( isset( $requestedData[ 'length' ] ) && $requestedData[ 'length' ] > 0 )?$requestedData[ 'length' ]:10;
        $search         = ( isset( $requestedData[ 'search' ] ) && isset( $requestedData[ 'search' ][ 'value' ] ) )?$requestedData[ 'search' ][ 'value' ]:'';
        
        $orderColumn    = ( isset( $requestedData[ 'order' ] ) && isset( $requestedData[ 'order' ][ 0 ] ) && isset( $requestedData[ 'order' ][ 0 ][ 'column' ] ) )?$requestedData[ 'order' ][ 0 ][ 'column' ]:0;
        $orderDir        = ( isset( $requestedData[ 'order' ] ) && isset( $requestedData[ 'order' ][ 0 ] ) && isset( $requestedData[ 'order' ][ 0 ][ 'dir' ] ) )?$requestedData[ 'order' ][ 0 ][ 'dir' ]:'ASC';

        $partnerTblName        = with( new Partner )->getTable( ); 
        $orderByColumns = [ $partnerTblName . '.sort_order', $partnerTblName . '.name' ];

        $partnerObj = Partner::where( $partnerTblName . '.isArchive', 0 )->orderBy( $orderByColumns[ $orderColumn ], $orderDir );

        $totalItems = $partnerObj->count( );

        if( $search ) { 
            $partnerObj->where( $partnerTblName . '.name', 'like', "%{$search}%" );
        }

        $recordsFiltered = $partnerObj->count( );

        $partnerItems = $partnerObj->offset( $start )->limit( $perPage )->get( );
        
        $data = [ "draw" => $draw, "recordsTotal" => $totalItems, "recordsFiltered" => $recordsFiltered, "data" => [] ];
        if( $partnerItems->count( ) > 0 ) { 
            foreach( $partnerItems as $partner ) { 
                $data[ 'data' ][] = [  
                                    ++$start,
                                    $partner->name,
                                    '<span><img src="' . env('AWS_S3_BUCKET_URL').'/'.env('AWS_S3_PARTNER_IMAGE_BASEURL').'/'.$partner->image . '" alt="" /></span>',
                                    '<div class="form-group">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input active_status partner_active_' . $partner->id . '" name="partner_active_' . $partner->id . '" onchange="return updateStatus( ' . $partner->id . ', \'' . route( 'admin.partner_status_update' ) . '\', this.value );" id="yes" value="1" ' . ( $partner->active == 1 ? "checked":"" ) . '> Yes <i class="input-helper"></i></label>
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                            <input type="radio" class="form-check-input active_status partner_active_' . $partner->id . '" name="partner_active_' . $partner->id . '" onchange="return updateStatus( ' . $partner->id . ', \'' . route( 'admin.partner_status_update' ) . '\', this.value );" id="no" value="0" ' . ( $partner->active == 0 ? "checked":"" ) . '> No <i class="input-helper"></i></label>
                                        </div>                                          
                                    </div>',
                                    '<a href="' . route('admin.editpartner', [ 'id'=> Hashids::encode($partner->id)  ] ) . '" class="add" title=""><i class="fa fa-edit"></i></a><a href="' . route('admin.deletepartner', [ 'id'=> Hashids::encode($partner->id)  ] ) . '" class="delete_record" ><i class="fa fa-trash"></i></a>',
                                    Hashids::encode($partner->id),
                                    $partner->sort_order 
                                ];
            }
        }

        return response( )->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Add Partner";
        return view('admin.partner.partner_add',[ 'title'=>$title,'breadcrumbItem'=>'Partner' ,'breadcrumbItemLink'=>route('admin.partnerlist'), 'breadcrumbTitle'=>'Add Partner' ] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request_data = $request->all();
        // print_r($request_data);  die();

        $request->validate([
            'partner_name' => 'required',
            'image_link' => 'url',
        ]);
        
        $file_name_with_time_prefix = upload_file($request, 'partner_image' , env('AWS_S3_PARTNER_IMAGE_BASEURL') );
        
        $partner = new Partner();
        $partner->name= $request['partner_name'];
        if($file_name_with_time_prefix)
        $partner->image= $file_name_with_time_prefix;

        $partner->image_link= $request['image_link'];

        
        if( $partner->save() )
        {
            $partner->sort_order = $partner->id;
            $partner->save();
            \App\Models\LastUpdated::updateDateTime();
            return redirect()->route('admin.partnerlist')->with('success','Partner saved successfully.');
        }

        return redirect()->route('admin.partnerlist')->with('error','Partner not saved. Something went wrong !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        
        $decryptedId = Hashids::decode($id);

        if( !( isset($decryptedId[0]) && $decryptedId[0] > 0 ) )
        return redirect()->route('admin.partnerlist')->with('error','Invalid Request id !');
        
        $title = "Edit Partner";
        $partner_detail = Partner::find($decryptedId[0]);     
        // print_r($all_partners);die;
        return view('admin.partner.partner_edit',[ 'title'=>$title,'breadcrumbItem'=>'Partner' , 'breadcrumbItemLink'=>route('admin.partnerlist'), 'breadcrumbTitle'=>'Edit Partner','partner_detail'=>$partner_detail ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request_data = $request->all();
        
        $request->validate([
            'partner_name' => 'required',
            'image_link' => 'url',
        ]);
        if( !( isset($request->partnerid) && $request->partnerid > 0 ) )
        return redirect()->route('admin.partnerlist')->with('error','Invalid Request id !!');

        $partner = Partner::find($request->partnerid);
        $file_name_with_time_prefix = upload_file($request, 'partner_image' , env('AWS_S3_PARTNER_IMAGE_BASEURL') );
        $partner->name= $request['partner_name'];
        if($file_name_with_time_prefix)
        $partner->image= $file_name_with_time_prefix;

        $partner->image_link= $request['image_link'];
        
        if( $partner->save() ){ 
            \App\Models\LastUpdated::updateDateTime();
            return redirect()->route('admin.partnerlist')->with('success','Partner updated successfully.');
        }

        return redirect()->route('admin.partnerlist')->with('error','Partner not updated. Something went wrong !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $decryptedId = Hashids::decode($id);
        if( !( isset($decryptedId[0]) && $decryptedId[0] > 0 ) )
        return redirect()->route('admin.partnerlist')->with('error','Invalid Request id !');
        
        $partnerInfo = Partner::find( $decryptedId[0] );
        if( $partnerInfo->count() <= 0 ) { 
            return redirect()->route('admin.partnerlist')->with('error','Some information is missing!');
        }
        
        $partnerInfo->isArchive = 1 ;
        if( !$partnerInfo->save() ){ 
            return redirect()->route('admin.partnerlist')->with('error','Some information is missing!');
        }
        
        \App\Models\LastUpdated::updateDateTime();
        return redirect()->route('admin.partnerlist')->with('success','Partner deleted successfully.');
        
    }

    // function to update the status (Active/Inactive)
    public function partner_status_update(Request $request)
    {
        $request_data = $request->all();
        if( ! ( isset( $request->id ) && $request->id > 0 ) ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        // echo "<pre>";print_r($request_data);die;
        $Partner = Partner::find($request->id);
        if( $Partner->count() <= 0 ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Selected Partner Not Found'
            ]);
        }

        $Partner->active = $Partner->active ? 0 : 1 ;
        
        if( !$Partner->save( ) ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Unable to update status, please try again later.'
            ]);
        }
        
        \App\Models\LastUpdated::updateDateTime();
        return response()->json([
            'status' => 1,
            'msg' => 'Status updated successfully.'
        ]);
    }

    public function updatePartnerItemReorder(Request $request)
    {
        $request_data = $request->all();
        // echo "<pre>";print_r( $request_data );  die;
        
        $validator = $request->validate([
                'replace_ids' => 'required'
            ]);

        if( count( $request_data['replace_ids'] ) <= 1 ) { 
            return response::json([ 'errors' => 'Unable to update order, please try again later.' ]);
        }

        $positions = $ids = [];
        foreach( array_keys( $request_data['replace_ids'] ) as $id ) { 
            $ids[] = Hashids::decode( ( string )$id )[0];
            $positions[ Hashids::decode( ( string )$id )[0] ] = $request_data['replace_ids'][ $id ];
        }
            
        $items = Partner::whereIn( 'id', $ids )->get();
        if( $items->count() !=  count( $request_data['replace_ids'] ) ) { 
            return response::json([ 'errors' => 'Unable to update order, please try again later.' ]);
        }

        foreach( $items as $item ){ 
            $item->sort_order = $positions[ $item->id ];
            $item->save();
        }

        \App\Models\LastUpdated::updateDateTime();
        return response::json([ 'success' => 'Order updated successfully' ]);

    }

    public function deleteImage ( $id ) { 
        if( !$id ){
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        $id = Hashids::decode($id);
        if( !$id ){
            return response()->json([
                'status' => 0,
                'msg' => 'Invalid request'
            ]); 
        }

        $id = $id[0];

        $obj = Partner::find( $id );
        if( $obj->count() <= 0 ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Selected item not found'
            ]);
        }

        if( !$obj->image ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Selected leader image not found'
            ]);
        }

        $isDeleted = Storage::disk('s3')->delete( env( 'AWS_S3_PARTNER_IMAGE_BASEURL' ) . '/' . $obj->image );
        if( !$isDeleted ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Unable to delete image, please try again later!'
            ]);
        }

        $obj->image = '';
        
        if( !$obj->save( ) ) { 
            return response()->json([
                'status' => 0,
                'msg' => 'Unable to delete image, please try again later!'
            ]);
        }

        \App\Models\LastUpdated::updateDateTime();
        return response()->json([
            'status' => 1,
            'msg' => 'Partner image deleted!.'
        ]);

    }

}
