<?php

if (! function_exists('upload_file')) {
    function upload_file($request, $filename, $folder_path)
    {
        // die('helper file');
        $file_is_moved = 0 ; 
        if ($request->hasFile($filename))
        {
            
            $file =  $request->file($filename)  ;
            //Display File Name
            $File_Name = $file->getClientOriginalName();
            //Display File Extension
            $File_Extension  = $file->getClientOriginalExtension();
            //Display File Real Path
            $File_Real_Path = $file->getRealPath();
            //Display File Size
            $File_Size = $file->getSize();
            //Display File Mime Type
            $File_Mime_Type = $file->getMimeType();
        
            //Move Uploaded File
            $file_name_with_time_prefix = time().'_'.$File_Name ;

            /*$destinationPath = public_path( $folder_path );
            $file_is_moved = $file->move( $destinationPath , $file_name_with_time_prefix );
            if($file_is_moved)
            return $file_name_with_time_prefix ;*/

            $t = Storage::disk('s3')->put($folder_path."/".$file_name_with_time_prefix, file_get_contents($file), 'public');
            // die($t);
            $file_path_s3 = Storage::disk('s3')->url($folder_path."/".$file_name_with_time_prefix);
            //  die( $file_path_s3 );
            return $file_name_with_time_prefix ;
           
        }
            return false;

    }
}

?>